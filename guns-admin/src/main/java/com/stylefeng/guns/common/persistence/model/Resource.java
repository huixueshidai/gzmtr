package com.stylefeng.guns.common.persistence.model;

import cn.afterturn.easypoi.excel.annotation.Excel;
import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;
import com.baomidou.mybatisplus.enums.IdType;
import org.springframework.format.annotation.DateTimeFormat;

import java.io.Serializable;
import java.util.Date;

/**
 * <p>
 * 应急资源
 * </p>
 *
 * @author yinjc
 * @since 2018-02-23
 */
@TableName("gzmtr_resource")
public class Resource extends Model<Resource> {

    private static final long serialVersionUID = 1L;

	@TableId(value="id", type= IdType.AUTO)
//	@Excel(name = "id" ,width = 15)
	private Integer id;
    /**
     * 名称
     */
	@Excel(name = "名称" ,width = 20)
	private String name;
    /**
     * 是否是大型物资
     */
	@TableField("large_scale")
	@Excel(name = "是否是大型物资" ,width = 20)
	private String largeScale;
    /**
     * 型号
     */
	@TableField("model_num")
	@Excel(name = "型号" ,width = 15)
	private String modelNum;
    /**
     * 专业用途
     */
	@Excel(name = "专业用途" ,width = 20)
	private String fist;
    /**
     * 现有数量
     */
	@Excel(name = "现有数量" ,width = 15,type = 1)
	private Integer num;
    /**
     * 单位
     */
	@Excel(name = "单位" ,width = 15)
	private String unit;
    /**
     * 线别
     */
	@Excel(name = "线别" ,width = 15)
	private String line;
    /**
     * 放置地点
     */
	@Excel(name = "放置地点" ,width = 20)
	private String location;
    /**
     * 地点备注
     */
	@Excel(name = "地点备注" ,width = 20)
	private String remark;
    /**
     * 中心
     */
	@Excel(name = "中心" ,width = 20)
	private String center;
    /**
     * 使用部门
     */
	@Excel(name = "使用部门" ,width = 20)
	private String usedp;
    /**
     * 维护部门
     */
	@Excel(name = "维护部门" ,width = 20)
	private String maintaindp;
    /**
     * 更新时间
     */
	@TableField("update_time")
	@DateTimeFormat(pattern = "yyyy-MM-dd")
	@Excel(name = "更新时间" ,width = 20,importFormat = "yyyy-MM-dd")
	private Date updateTime;
    /**
     * 状态
     */
	@Excel(name = "状态" ,width = 15)
	private String state;


	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getLargeScale() {
		return largeScale;
	}

	public void setLargeScale(String largeScale) {
		this.largeScale = largeScale;
	}

	public String getModelNum() {
		return modelNum;
	}

	public void setModelNum(String modelNum) {
		this.modelNum = modelNum;
	}

	public String getFist() {
		return fist;
	}

	public void setFist(String fist) {
		this.fist = fist;
	}

	public Integer getNum() {
		return num;
	}

	public void convertSetNum(Integer num){
		System.out.println("==============================");
	}

	public void setNum(Integer num) {
		this.num = num;
	}

	public String getUnit() {
		return unit;
	}

	public void setUnit(String unit) {
		this.unit = unit;
	}

	public String getLine() {
		return line;
	}

	public void setLine(String line) {
		this.line = line;
	}

	public String getLocation() {
		return location;
	}

	public void setLocation(String location) {
		this.location = location;
	}

	public String getRemark() {
		return remark;
	}

	public void setRemark(String remark) {
		this.remark = remark;
	}

	public String getCenter() {
		return center;
	}

	public void setCenter(String center) {
		this.center = center;
	}

	public String getUsedp() {
		return usedp;
	}

	public void setUsedp(String usedp) {
		this.usedp = usedp;
	}

	public String getMaintaindp() {
		return maintaindp;
	}

	public void setMaintaindp(String maintaindp) {
		this.maintaindp = maintaindp;
	}

	public Date getUpdateTime() {
		return updateTime;
	}

	public void setUpdateTime(Date updateTime) {
		this.updateTime = updateTime;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	@Override
	protected Serializable pkVal() {
		return this.id;
	}

	@Override
	public String toString() {
		return "Resource{" +
			"id=" + id +
			", name=" + name +
			", largeScale=" + largeScale +
			", modelNum=" + modelNum +
			", fist=" + fist +
			", num=" + num +
			", unit=" + unit +
			", line=" + line +
			", location=" + location +
			", remark=" + remark +
			", center=" + center +
			", usedp=" + usedp +
			", maintaindp=" + maintaindp +
			", updateTime=" + updateTime +
			", state=" + state +
			"}";
	}
}
