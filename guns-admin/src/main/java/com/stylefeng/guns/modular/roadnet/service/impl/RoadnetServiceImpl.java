package com.stylefeng.guns.modular.roadnet.service.impl;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.stylefeng.guns.common.persistence.dao.RoadnetMapper;
import com.stylefeng.guns.common.persistence.model.Roadnet;
import com.stylefeng.guns.common.plugin.service.impl.BaseTreeableServiceImpl;
import com.stylefeng.guns.modular.roadnet.service.IRoadnetService;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author wangshuaikang
 * @since 2018-01-04
 */
@Service
public class RoadnetServiceImpl extends BaseTreeableServiceImpl<RoadnetMapper, Roadnet> implements IRoadnetService {

	@Override
	public List<Roadnet> selectByType(Integer type) {
		EntityWrapper<Roadnet> ew = new EntityWrapper<>();
		ew.eq("type", type);
		return selectList(ew);
	}
}