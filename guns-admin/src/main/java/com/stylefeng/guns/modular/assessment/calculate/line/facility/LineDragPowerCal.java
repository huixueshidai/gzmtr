package com.stylefeng.guns.modular.assessment.calculate.line.facility;

import com.stylefeng.guns.common.persistence.model.Assessment;
import com.stylefeng.guns.core.util.DoubleUtil;
import com.stylefeng.guns.modular.assessment.calculate.ThirdIndicationCalculator;
import com.stylefeng.guns.modular.assessment.calculate.aware.LineThirdIndicationAware;
import org.springframework.stereotype.Component;

import java.util.Date;

/**
 * 牵引供电系统影响运营风险指数
 */
@Component
public class LineDragPowerCal extends ThirdIndicationCalculator implements LineThirdIndicationAware {

    @Override
    protected Integer getIndicationId() {
        return 61;
    }

    @Override
    public Assessment calculate(Integer lineId, Date beginDate, Date endDate) {
        //区间强度
        Double intervalStrength = getDoubleValue(lineId, 201, beginDate,endDate);
        //区间供电故障的时间
        Double failureTime = getDoubleValue(lineId, 202, beginDate,endDate);
        //供电系统总运行时间
        Double totalTime = getDoubleValue(lineId, 203, beginDate,endDate);
        //受供电影响所耽误的列车正线运营里程
        Double reduceMilByfailure = getDoubleValue(lineId, 204, beginDate,endDate);
        //列车正线计划运营里程数(供电)
        Double normalMil = getDoubleValue(lineId, 205, beginDate,endDate);
        //牵引供电设备故障时间权重
        Double timeWeight = getDoubleValue(lineId, 206, beginDate,endDate);
        //影响范围权重
        Double milWeight = getDoubleValue(lineId, 207, beginDate,endDate);

        Double g = DoubleUtil.safeDiv(reduceMilByfailure, normalMil);
        Double res = DoubleUtil.mul(failureTime, intervalStrength);
        Double resVal = DoubleUtil.safeDiv(res, totalTime);
        Double m = DoubleUtil.mul(timeWeight, g);
        Double resa = DoubleUtil.mul(resVal, milWeight);
        Double resVal1 = DoubleUtil.add(resa, m);
        return genResult(resVal1, lineId, beginDate,endDate);
    }

}
