package com.stylefeng.guns.modular.yjya.dao;

import com.baomidou.mybatisplus.plugins.Page;
import com.stylefeng.guns.common.persistence.model.Resource;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

public interface ResourceDao {
    List<Map<String,Object>> getResources(@Param("page") Page<Resource> page, @Param("name") String name);
    List<Map<String,Object>> selectByName(@Param("condition")  String condition,@Param("location")  String location);
}
