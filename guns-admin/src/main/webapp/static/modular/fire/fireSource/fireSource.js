/**
 * 火灾情景路径选择管理初始化
 */
var FireSource = {
    id: "FireSourceTable",	//表格id
    seItem: null,		//选中的条目
    table: null,
    layerIndex: -1
};

/**
 * 初始化表格的列
 */
FireSource.initColumn = function () {
    return [
        {field: 'selectItem', radio: true},
          /*  {title: '', field: 'id', visible: true, align: 'center', valign: 'middle'},*/
            {title: '燃烧材料', field: 'material', visible: true, align: 'center', valign: 'middle'},
            {title: '燃烧场地', field: 'field', visible: true, align: 'center', valign: 'middle'},
            {title: '救援人员', field: 'rescuers', visible: true, align: 'center', valign: 'middle'},
           /* {title: '专家一', field: 'expert1', visible: true, align: 'center', valign: 'middle'},
            {title: '专家二', field: 'expert2', visible: true, align: 'center', valign: 'middle'},
            {title: '专家三', field: 'expert3', visible: true, align: 'center', valign: 'middle'},*/
        {title: '专家一熄灭概率', field: 'expert1T', visible: true, align: 'center', valign: 'middle'},
        {title: '专家一未熄灭概率', field: 'expert1F', visible: true, align: 'center', valign: 'middle'},
        {title: '专家二熄灭概率', field: 'expert2T', visible: true, align: 'center', valign: 'middle'},
        {title: '专家二未熄灭概率', field: 'expert2F', visible: true, align: 'center', valign: 'middle'},
        {title: '专家三熄灭概率', field: 'expert3T', visible: true, align: 'center', valign: 'middle'},
        {title: '专家三未熄灭概率', field: 'expert3F', visible: true, align: 'center', valign: 'middle'},
        {title: '结果', field: 'result', visible: true, align: 'center', valign: 'middle'}
    ];
};

/**
 * 检查是否选中
 */
FireSource.check = function () {
    var selected = $('#' + this.id).bootstrapTable('getSelections');
    if(selected.length == 0){
        Feng.info("请先选中表格中的某一记录！");
        return false;
    }else{
        FireSource.seItem = selected[0];
        return true;
    }
};

/**
 * 点击添加火灾情景路径选择
 */
FireSource.openAddFireSource = function () {
    var index = layer.open({
        type: 2,
        title: '添加火灾情景路径选择',
        area: ['800px', '420px'], //宽高
        fix: false, //不固定
        maxmin: true,
        content: Feng.ctxPath + '/fireSource/fireSource_add'
    });
    this.layerIndex = index;
};

/**
 * 打开查看火灾情景路径选择详情
 */
FireSource.openFireSourceDetail = function () {
    if (this.check()) {
        var index = layer.open({
            type: 2,
            title: '火灾情景路径选择详情',
            area: ['800px', '420px'], //宽高
            fix: false, //不固定
            maxmin: true,
            content: Feng.ctxPath + '/fireSource/fireSource_update/' + FireSource.seItem.id
        });
        this.layerIndex = index;
    }
};

/**
 * 删除火灾情景路径选择
 */
FireSource.delete = function () {
    if (this.check()) {
        var ajax = new $ax(Feng.ctxPath + "/fireSource/delete", function (data) {
            Feng.success("删除成功!");
            FireSource.table.refresh();
        }, function (data) {
            Feng.error("删除失败!" + data.responseJSON.message + "!");
        });
        ajax.set("fireSourceId",this.seItem.id);
        ajax.start();
    }
};

/**
 * 查询火灾情景路径选择列表
 */
FireSource.search = function () {
    var queryData = {};
    queryData['condition'] = $("#condition").val();
    FireSource.table.refresh({query: queryData});
};

$(function () {
    var defaultColunms = FireSource.initColumn();
    var table = new BSTable(FireSource.id, "/fireSource/list", defaultColunms);
    console.log(table);
    table.setPaginationType("server");
    FireSource.table = table.init();
});
