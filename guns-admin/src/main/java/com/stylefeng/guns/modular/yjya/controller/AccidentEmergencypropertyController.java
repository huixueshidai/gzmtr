package com.stylefeng.guns.modular.yjya.controller;

import com.stylefeng.guns.core.base.controller.BaseController;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.beans.factory.annotation.Autowired;
import com.stylefeng.guns.core.log.LogObjectHolder;
import org.springframework.web.bind.annotation.RequestParam;
import com.stylefeng.guns.common.persistence.model.AccidentEmergencyproperty;
import com.stylefeng.guns.modular.yjya.service.IAccidentEmergencypropertyService;

/**
 * 控制器
 *
 * @author wangshuaikang
 * @Date 2018-05-29 00:36:47
 */
@Controller
@RequestMapping("/accidentEmergencyproperty")
public class AccidentEmergencypropertyController extends BaseController {

    private String PREFIX = "/yjya/accidentEmergencyproperty/";

    private String PREFIXACCIDENT = "/yjya/accident/";

    @Autowired
    private IAccidentEmergencypropertyService accidentEmergencypropertyService;

    /**
     * 跳转到首页
     */
    @RequestMapping("")
    public String index() {
        return PREFIX + "accidentEmergencyproperty.html";
    }

    /**
     * 跳转到接警时添加突发事件属性
     */
    @RequestMapping("/accidentEmergencyproperty_add")
    public String accidentEmergencypropertyAdd() {
        return PREFIX + "accidentEmergencyproperty_add.html";
    }


    /**
     * 接警时跳转到突发事件属性修改
     */
    @RequestMapping("/accidentEmergencyproperty_update/{accidentEmergencypropertyId}")
    public String accidentEmergencypropertyUpdate(@PathVariable Integer accidentEmergencypropertyId, Model model) {
        AccidentEmergencyproperty accidentEmergencyproperty = accidentEmergencypropertyService.selectById(accidentEmergencypropertyId);
        model.addAttribute("item",accidentEmergencyproperty);
        LogObjectHolder.me().set(accidentEmergencyproperty);
        return PREFIX + "accidentEmergencyproperty_edit.html";
    }

    /**
     * 手动添加事故实例时添加突发事件属性
     */
    @RequestMapping("/add_accidentEmergencyproperty")
    public String addAccidentEmergencyproperty() {
        return PREFIXACCIDENT + "accidentEmergencypro_add.html";
    }

    /**
     * 收动添加十股势力突发事件属性修改
     */
    @RequestMapping("/edit_accidentEmergencyproperty_update/{accidentEmergencypropertyId}")
    public String editAccidentEmergencypropertyUpdate(@PathVariable Integer accidentEmergencypropertyId, Model model) {
        AccidentEmergencyproperty accidentEmergencyproperty = accidentEmergencypropertyService.selectById(accidentEmergencypropertyId);
        model.addAttribute("item",accidentEmergencyproperty);
        LogObjectHolder.me().set(accidentEmergencyproperty);
        return PREFIXACCIDENT + "accidentEmergencypro_edit.html";
    }

    /**
     * 获取列表
     */
    @RequestMapping(value = "/list")
    @ResponseBody
    public Object list(String condition) {
        return accidentEmergencypropertyService.selectList(null);
    }

    /**
     * 新增
     */
    @RequestMapping(value = "/add")
    @ResponseBody
    public Object add(AccidentEmergencyproperty accidentEmergencyproperty) {
        accidentEmergencypropertyService.insert(accidentEmergencyproperty);

        return accidentEmergencyproperty;
    }

    /**
     * 删除
     */
    @RequestMapping(value = "/delete")
    @ResponseBody
    public Object delete(@RequestParam Integer accidentEmergencypropertyId) {
        accidentEmergencypropertyService.deleteById(accidentEmergencypropertyId);
        return SUCCESS_TIP;
    }

    /**
     * 修改
     */
    @RequestMapping(value = "/update")
    @ResponseBody
    public Object update(AccidentEmergencyproperty accidentEmergencyproperty) {
        accidentEmergencypropertyService.updateById(accidentEmergencyproperty);
        return super.SUCCESS_TIP;
    }

    /**
     * 详情
     */
    @RequestMapping(value = "/detail/{accidentEmergencypropertyId}")
    @ResponseBody
    public Object detail(@PathVariable("accidentEmergencypropertyId") Integer accidentEmergencypropertyId) {
        return accidentEmergencypropertyService.selectById(accidentEmergencypropertyId);
    }
}
