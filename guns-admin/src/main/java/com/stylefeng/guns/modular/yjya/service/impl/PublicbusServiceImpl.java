package com.stylefeng.guns.modular.yjya.service.impl;

import com.stylefeng.guns.common.persistence.model.Publicbus;
import com.stylefeng.guns.common.persistence.dao.PublicbusMapper;
import com.stylefeng.guns.modular.yjya.service.IPublicbusService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author wangshuaikang
 * @since 2018-09-03
 */
@Service
public class PublicbusServiceImpl extends ServiceImpl<PublicbusMapper, Publicbus> implements IPublicbusService {
	
}
