package com.stylefeng.guns.common.persistence.model;

import java.io.Serializable;

import com.baomidou.mybatisplus.enums.IdType;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableName;
import java.io.Serializable;

/**
 * <p>
 * 车辆类型
 * </p>
 *
 * @author yinjc
 * @since 2018-03-21
 */
@TableName("gzmtr_public_bus_type")
public class PublicBusType extends Model<PublicBusType> {

    private static final long serialVersionUID = 1L;

	@TableId(value="id", type= IdType.AUTO)
	private Integer id;
    /**
     * 车辆类型
     */
	@TableField("bus_type_name")
	private String busTypeName;
    /**
     * 车辆定员
     */
	@TableField("bus_capacity")
	private Integer busCapacity;


	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getBusTypeName() {
		return busTypeName;
	}

	public void setBusTypeName(String busTypeName) {
		this.busTypeName = busTypeName;
	}

	public Integer getBusCapacity() {
		return busCapacity;
	}

	public void setBusCapacity(Integer busCapacity) {
		this.busCapacity = busCapacity;
	}

	@Override
	protected Serializable pkVal() {
		return this.id;
	}

	@Override
	public String toString() {
		return "PublicBusType{" +
			"id=" + id +
			", busTypeName=" + busTypeName +
			", busCapacity=" + busCapacity +
			"}";
	}
}
