/**
 * 管理初始化
 */
var AccidentEmergencyproperty = {
    id: "AccidentEmergencypropertyTable",	//表格id
    seItem: null,		//选中的条目
    table: null,
    layerIndex: -1
};

/**
 * 初始化表格的列
 */
AccidentEmergencyproperty.initColumn = function () {
    return [
        {field: 'selectItem', radio: true},
            {title: '', field: 'id', visible: true, align: 'center', valign: 'middle'},
            {title: '客流', field: 'ridership', visible: true, align: 'center', valign: 'middle'},
            {title: '同行能力', field: 'trafficCapacity', visible: true, align: 'center', valign: 'middle'},
            {title: '故障数量', field: 'fault', visible: true, align: 'center', valign: 'middle'},
            {title: '气体保护', field: 'gasShield', visible: true, align: 'center', valign: 'middle'},
            {title: '隧道长度', field: 'tunnelLength', visible: true, align: 'center', valign: 'middle'},
            {title: '状态', field: 'status', visible: true, align: 'center', valign: 'middle'},
            {title: '距离前一站距离', field: 'distanceOn', visible: true, align: 'center', valign: 'middle'},
            {title: '距离下一站距离', field: 'distanceNext', visible: true, align: 'center', valign: 'middle'},
            {title: '被困人员数量', field: 'trappedNum', visible: true, align: 'center', valign: 'middle'},
            {title: '被困人员行动能力', field: 'trappedNumPower', visible: true, align: 'center', valign: 'middle'},
            {title: '物理状态', field: 'physicalState', visible: true, align: 'center', valign: 'middle'},
            {title: '毒性', field: 'toxicity', visible: true, align: 'center', valign: 'middle'},
            {title: '燃爆性', field: 'bums', visible: true, align: 'center', valign: 'middle'},
            {title: '腐蚀性', field: 'causticity', visible: true, align: 'center', valign: 'middle'},
            {title: '挥发性', field: 'volatileness', visible: true, align: 'center', valign: 'middle'},
            {title: '温度', field: 'temperature', visible: true, align: 'center', valign: 'middle'},
            {title: '烟气浓度', field: 'smokescope', visible: true, align: 'center', valign: 'middle'},
            {title: '坍塌', field: 'collapse', visible: true, align: 'center', valign: 'middle'},
            {title: '', field: 'loseEfficacy', visible: true, align: 'center', valign: 'middle'},
            {title: '道路状况', field: 'reoad', visible: true, align: 'center', valign: 'middle'},
            {title: '119距离', field: 'distanceOneOneNine', visible: true, align: 'center', valign: 'middle'},
            {title: '110距离', field: 'distanceOneOneZero', visible: true, align: 'center', valign: 'middle'},
            {title: '120距离', field: 'distanceOneTwoZero', visible: true, align: 'center', valign: 'middle'},
            {title: '消防设施', field: 'fireControl', visible: true, align: 'center', valign: 'middle'}
    ];
};

/**
 * 检查是否选中
 */
AccidentEmergencyproperty.check = function () {
    var selected = $('#' + this.id).bootstrapTable('getSelections');
    if(selected.length == 0){
        Feng.info("请先选中表格中的某一记录！");
        return false;
    }else{
        AccidentEmergencyproperty.seItem = selected[0];
        return true;
    }
};

/**
 * 点击添加
 */
AccidentEmergencyproperty.openAddAccidentEmergencyproperty = function () {
    var index = layer.open({
        type: 2,
        title: '添加',
        area: ['800px', '420px'], //宽高
        fix: false, //不固定
        maxmin: true,
        content: Feng.ctxPath + '/accidentEmergencyproperty/accidentEmergencyproperty_add'
    });
    this.layerIndex = index;
};

/**
 * 打开查看详情
 */
AccidentEmergencyproperty.openAccidentEmergencypropertyDetail = function () {
    if (this.check()) {
        var index = layer.open({
            type: 2,
            title: '详情',
            area: ['800px', '420px'], //宽高
            fix: false, //不固定
            maxmin: true,
            content: Feng.ctxPath + '/accidentEmergencyproperty/accidentEmergencyproperty_update/' + AccidentEmergencyproperty.seItem.id
        });
        this.layerIndex = index;
    }
};

/**
 * 删除
 */
AccidentEmergencyproperty.delete = function () {
    if (this.check()) {
        var ajax = new $ax(Feng.ctxPath + "/accidentEmergencyproperty/delete", function (data) {
            Feng.success("删除成功!");
            AccidentEmergencyproperty.table.refresh();
        }, function (data) {
            Feng.error("删除失败!" + data.responseJSON.message + "!");
        });
        ajax.set("accidentEmergencypropertyId",this.seItem.id);
        ajax.start();
    }
};

/**
 * 查询列表
 */
AccidentEmergencyproperty.search = function () {
    var queryData = {};
    queryData['condition'] = $("#condition").val();
    AccidentEmergencyproperty.table.refresh({query: queryData});
};

$(function () {
    var defaultColunms = AccidentEmergencyproperty.initColumn();
    var table = new BSTable(AccidentEmergencyproperty.id, "/accidentEmergencyproperty/list", defaultColunms);
    table.setPaginationType("client");
    AccidentEmergencyproperty.table = table.init();
});
