package com.stylefeng.guns.modular.yjya.controller;

import com.baomidou.mybatisplus.plugins.Page;
import com.stylefeng.guns.common.constant.factory.PageFactory;
import com.stylefeng.guns.common.persistence.dao.EmergencypropertyMapper;
import com.stylefeng.guns.core.base.controller.BaseController;
import com.stylefeng.guns.modular.yjya.dao.EmergencypropertyDao;
import com.stylefeng.guns.modular.yjya.warpper.ContactWarpper;
import com.stylefeng.guns.modular.yjya.warpper.EmergencypropertyWarpper;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.beans.factory.annotation.Autowired;
import com.stylefeng.guns.core.log.LogObjectHolder;
import org.springframework.web.bind.annotation.RequestParam;
import com.stylefeng.guns.common.persistence.model.Emergencyproperty;
import com.stylefeng.guns.modular.yjya.service.IEmergencypropertyService;


import java.text.NumberFormat;
import java.util.List;
import java.util.Map;

/**
 * 控制器
 *
 * @author wangshuaikang
 * @Date 2018-05-24 21:05:37
 */
@Controller
@RequestMapping("/emergencyproperty")
public class EmergencypropertyController extends BaseController {

    private String PREFIX = "/yjya/emergencyproperty/";

    @Autowired
    private IEmergencypropertyService emergencypropertyService;

    @Autowired
    private EmergencypropertyDao emergencypropertyDao;

    @Autowired
    private EmergencypropertyMapper emergencypropertyMapper;
    /**
     * 跳转到首页
     */
    @RequestMapping("")
    public String index() {
        return PREFIX + "emergencyproperty.html";
    }

    /**
     * 跳转到添加
     */
    @RequestMapping("/emergencyproperty_add")
    public String emergencypropertyAdd() {
        return PREFIX + "emergencyproperty_add.html";
    }

    /**
     * 跳转到修改
     */
    @RequestMapping("/emergencyproperty_update/{emergencypropertyId}")
    public String emergencypropertyUpdate(@PathVariable Integer emergencypropertyId, Model model) {
        Emergencyproperty emergencyproperty = emergencypropertyService.selectById(emergencypropertyId);
        model.addAttribute("item",emergencyproperty);
        LogObjectHolder.me().set(emergencyproperty);
        return PREFIX + "emergencyproperty_edit.html";
    }

    /**
     * 获取列表
     */
    @RequestMapping(value = "/list")
    @ResponseBody
    public Object list(@RequestParam(value = "emergencyId",required = false) Integer emergencyId) {
        Page<Emergencyproperty> page = new PageFactory<Emergencyproperty>().defaultPage();
        List<Map<String,Object>> mapList = emergencypropertyDao.selectEmergencypropertys(page,emergencyId);
        page.setRecords((List<Emergencyproperty>) new EmergencypropertyWarpper(mapList).warp());
        return super.packForBT(page);
    }

    /**
     * 新增
     */
    @RequestMapping(value = "/add")
    @ResponseBody
    public Object add(Emergencyproperty emergencyproperty) {


        List<String> list = emergencyproperty.getPropertyList(emergencyproperty);
        for(int i =0;i<list.size();i++){
            double sum = 0;
            double sum1 = 0;
            double w = 0;
            double vj = 0;

            String[] property = list.get(i).split(";");
            for(int j = 0;j<property.length;j++){
                //每个属性专家打分和
                sum = sum + Double.parseDouble(property[j]);
                if(j==property.length-1){
                    //每个属性平均值
                    w = sum/3;
                }

                }
            for(int f = 0;f<property.length;f++){
                //每个属性专家打分和
                sum1 = sum1 + Math.pow(Double.parseDouble(property[f])-w,2);
                if(f == property.length-1){
                    //计算出每个属性的标准差
                    vj = Math.sqrt(sum1/3);
                }

            }

               //各属性标准差
                list.set(i,String.valueOf(vj));

        }
        double sumvj = 0;
        for(int n=0;n<list.size();n++){
             sumvj = sumvj + Double.parseDouble(list.get(n));
        }
        NumberFormat nf=NumberFormat.getNumberInstance() ;
        nf.setMaximumFractionDigits(4);
        for(int q = 0;q<list.size();q++){
            list.set(q,String.valueOf(nf.format(Double.parseDouble(list.get(q))/sumvj)));
        }
        emergencyproperty.setEmergency(list);
        Emergencyproperty emergencyproperty1 = emergencypropertyMapper.selectByemergency(emergencyproperty.getEmergencyId());
        if(emergencyproperty1!=null){
            emergencypropertyService.deleteById(emergencyproperty1.getId());
            emergencypropertyService.insert(emergencyproperty);
        }
        else{
            emergencypropertyService.insert(emergencyproperty);
        }
        return super.SUCCESS_TIP;
    }

    /**
     * 删除
     */
    @RequestMapping(value = "/delete")
    @ResponseBody
    public Object delete(@RequestParam Integer emergencypropertyId) {
        emergencypropertyService.deleteById(emergencypropertyId);
        return SUCCESS_TIP;
    }

    /**
     * 修改
     */
    @RequestMapping(value = "/update")
    @ResponseBody
    public Object update(Emergencyproperty emergencyproperty) {
        emergencypropertyService.updateById(emergencyproperty);
        return super.SUCCESS_TIP;
    }

    /**
     * 详情
     */
    @RequestMapping(value = "/detail/{emergencypropertyId}")
    @ResponseBody
    public Object detail(@PathVariable("emergencypropertyId") Integer emergencypropertyId) {
        return emergencypropertyService.selectById(emergencypropertyId);
    }
}
