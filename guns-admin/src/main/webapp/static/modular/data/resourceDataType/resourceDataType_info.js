/**
 * 初始化源数据类型详情对话框
 */
var ResourceDataTypeInfoDlg = {
    resourceDataTypeInfoData: {}
    , zTreeInstance: null
};

/**
 * 清除数据
 */
ResourceDataTypeInfoDlg.clearData = function () {
    this.resourceDataTypeInfoData = {};
}

/**
 * 设置对话框中的数据
 *
 * @param key 数据的名称
 * @param val 数据的具体值
 */
ResourceDataTypeInfoDlg.set = function (key, val) {
    this.resourceDataTypeInfoData[key] = (typeof val == "undefined") ? $("#" + key).val() : val;
    return this;
}

/**
 * 设置对话框中的数据
 *
 * @param key 数据的名称
 * @param val 数据的具体值
 */
ResourceDataTypeInfoDlg.get = function (key) {
    return $("#" + key).val();
}

/**
 * 关闭此对话框
 */
ResourceDataTypeInfoDlg.close = function () {
    parent.layer.close(window.parent.ResourceDataType.layerIndex);
}

/**
 * 收集数据
 */
ResourceDataTypeInfoDlg.collectData = function () {
    this
        .set('id')
        .set('name')
        .set('instruction')
        .set('needEntrance')
        .set('isFixed')
        .set('source')
        .set("indicationId")
        .set('defaultValue')
    ;
}

/**
 * 提交添加
 */
ResourceDataTypeInfoDlg.addSubmit = function () {

    this.clearData();
    this.collectData();

    //提交信息
    var ajax = new $ax(Feng.ctxPath + "/resourceDataType/add", function (data) {
        Feng.success("添加成功!");
        window.parent.ResourceDataType.table.refresh();
        ResourceDataTypeInfoDlg.close();
    }, function (data) {
        Feng.error("添加失败!" + data.responseJSON.message + "!");
    });
    ajax.set(this.resourceDataTypeInfoData);
    ajax.start();
}

/**
 * 提交修改
 */
ResourceDataTypeInfoDlg.editSubmit = function () {

    this.clearData();
    this.collectData();

    //提交信息
    var ajax = new $ax(Feng.ctxPath + "/resourceDataType/update", function (data) {
        Feng.success("修改成功!");
        window.parent.ResourceDataType.table.refresh();
        ResourceDataTypeInfoDlg.close();
    }, function (data) {
        Feng.error("修改失败!" + data.responseJSON.message + "!");
    });
    ajax.set(this.resourceDataTypeInfoData);
    ajax.start();
}

ResourceDataTypeInfoDlg.showIndicationSelectTree = function () {
    Feng.showInputTree("indicationName", "indicationTreeDiv", 15, 34);
}


ResourceDataTypeInfoDlg.onClickIndication = function (e, treeId, treeNode) {

    $("#indicationName").attr("value", ResourceDataTypeInfoDlg.zTreeInstance.getSelectedVal());
    $("#indicationId").attr("value", treeNode.id);
    $("#indicationTreeDiv").fadeOut("fast");
}

$(function () {
    var ztree = new $ZTree("indicationMenuTree", "/indication/tree");
    ztree.bindOnClick(ResourceDataTypeInfoDlg.onClickIndication);
    ztree.init();
    ResourceDataTypeInfoDlg.zTreeInstance = ztree;
});
