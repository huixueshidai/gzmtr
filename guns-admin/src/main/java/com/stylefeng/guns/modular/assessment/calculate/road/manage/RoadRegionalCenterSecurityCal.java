package com.stylefeng.guns.modular.assessment.calculate.road.manage;

import com.stylefeng.guns.common.persistence.model.Assessment;
import com.stylefeng.guns.modular.assessment.calculate.ThirdIndicationCalculator;
import com.stylefeng.guns.modular.assessment.calculate.aware.RoadnetThirdIndicationAware;
import org.springframework.stereotype.Component;

import java.util.Date;

/**
 * 路网区域中心安全管理指数
 *
 * @author yinjc
 * @time 2018/05/11
 */
@Component
public class RoadRegionalCenterSecurityCal extends ThirdIndicationCalculator implements RoadnetThirdIndicationAware {
    @Override
    protected Integer getIndicationId() {
        return 107;
    }

    @Override
    public Assessment calculate(Integer roadId, Date beginDate, Date endDate) {
        Double anQuan = getDoubleValue(roadId, 348, beginDate,endDate);
        return genResult(anQuan, roadId, beginDate,endDate);

    }
}
