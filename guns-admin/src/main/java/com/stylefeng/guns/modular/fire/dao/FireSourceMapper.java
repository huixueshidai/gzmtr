package com.stylefeng.guns.modular.fire.dao;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.stylefeng.guns.common.persistence.model.FireSource;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

/**
 * <p>
  * 火源情景预算 Mapper 接口
 * </p>
 *
 * @author xhq
 * @since 2018-09-03
 */
public interface FireSourceMapper extends BaseMapper<FireSource> {
    List<Map<String,Object>> selectMap(@Param("page") Page<FireSource> page);
}