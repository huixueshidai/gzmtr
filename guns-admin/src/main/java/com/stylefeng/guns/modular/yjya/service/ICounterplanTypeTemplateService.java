package com.stylefeng.guns.modular.yjya.service;

import com.stylefeng.guns.common.persistence.model.CounterplanTypeTemplate;
import com.baomidou.mybatisplus.service.IService;

/**
 * <p>
 * 预案类型模板 服务类
 * </p>
 *
 * @author xuziyang
 * @since 2018-01-18
 */
public interface ICounterplanTypeTemplateService extends IService<CounterplanTypeTemplate> {
	
}
