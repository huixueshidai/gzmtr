/**
 * 公交站枢纽信息管理初始化
 */
var PublicBusHub = {
    id: "PublicBusHubTable",	//表格id
    seItem: null,		//选中的条目
    table: null,
    layerIndex: -1
};

/**
 * 初始化表格的列
 */
PublicBusHub.initColumn = function () {
    return [
        {field: 'selectItem', radio: true},
        // {title: 'id', field: 'id', visible: true, align: 'center', valign: 'middle'},
        {title: '枢纽站名称', field: 'name', visible: true, align: 'center', valign: 'middle'},
        {title: '公交车辆类型', field: 'busType', visible: true, align: 'center', valign: 'middle'},
        {title: '车辆定员', field: 'busCapacity', visible: true, align: 'center', valign: 'middle'},
        {title: '车辆数量', field: 'driverNum', visible: true, align: 'center', valign: 'middle'},
        {title: '可用驾驶人员', field: 'driverNum', visible: true, align: 'center', valign: 'middle'},
        {title: '距轨道车站距离', field: 'distance', visible: true, align: 'center', valign: 'middle'},
        {title: '最大载客量', field: 'maxNum', visible: true, align: 'center', valign: 'middle'},
        {title: '满载率', field: 'loadFactor', visible: true, align: 'center', valign: 'middle'},

    ];
};

/**
 * 检查是否选中
 */
PublicBusHub.check = function () {
    var selected = $('#' + this.id).bootstrapTable('getSelections');
    if (selected.length == 0) {
        Feng.info("请先选中表格中的某一记录！");
        return false;
    } else {
        PublicBusHub.seItem = selected[0];
        return true;
    }
};

/**
 * 点击添加公交站枢纽信息
 */
PublicBusHub.openAddPublicBusHub = function () {
    var index = layer.open({
        type: 2,
        title: '添加公交站枢纽信息',
        area: ['800px', '420px'], //宽高
        fix: false, //不固定
        maxmin: true,
        content: Feng.ctxPath + '/publicBusHub/publicBusHub_add'
    });
    this.layerIndex = index;
};
/**
 * 点击获取地理位置信息
 */
PublicBusHub.openPublicBusHubLocation = function () {
    if (this.check()) {
        var index = layer.open({
            type: 2,
            title: '获取枢纽站位置',
            area: ['1000px', '600px'], //宽高
            fix: false, //不固定
            maxmin: true,
            content: Feng.ctxPath + '/publicBusHub/publicBusHub_location/' + PublicBusHub.seItem.id
        });
        this.layerIndex = index;
    }
};
/**
 * 打开查看公交站枢纽信息详情
 */
PublicBusHub.openPublicBusHubDetail = function () {
    if (this.check()) {
        var index = layer.open({
            type: 2,
            title: '公交站枢纽信息详情',
            area: ['800px', '420px'], //宽高
            fix: false, //不固定
            maxmin: true,
            content: Feng.ctxPath + '/publicBusHub/publicBusHub_update/' + PublicBusHub.seItem.id
        });
        this.layerIndex = index;
    }
};

/**
 * 删除公交站枢纽信息
 */
PublicBusHub.delete = function () {
    if (this.check()) {
        var ajax = new $ax(Feng.ctxPath + "/publicBusHub/delete", function (data) {
            Feng.success("删除成功!");
            PublicBusHub.table.refresh();
        }, function (data) {
            Feng.error("删除失败!" + data.responseJSON.message + "!");
        });
        ajax.set("publicBusHubId", this.seItem.id);
        ajax.start();
    }
};

/**
 * 重置
 */
PublicBusHub.resetSearch = function(){
    $("#condition").val("");
}

/**
 * 查询公交站枢纽信息列表
 */
PublicBusHub.search = function () {
    var queryData = {};
    queryData['condition'] = $("#condition").val();
    PublicBusHub.table.refresh({query: queryData});
};

$(function () {
    var defaultColunms = PublicBusHub.initColumn();
    var table = new BSTable(PublicBusHub.id, "/publicBusHub/list", defaultColunms);
    table.setPaginationType("client");
    PublicBusHub.table = table.init();
});
