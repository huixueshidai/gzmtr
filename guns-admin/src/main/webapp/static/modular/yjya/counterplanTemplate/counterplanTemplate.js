/**
 * 预案模板管理初始化
 */
var CounterplanTemplate = {
    id: "CounterplanTemplateTable",	//表格id
    seItem: null,		//选中的条目
    table: null,
    layerIndex: -1,
    uploadInstance:null,
    seItemIds:[],
    accidentTypeTreeInstance:null
};

/**
 * 初始化表格的列
 */
CounterplanTemplate.initColumn = function () {
    return [
        {field: 'selectItem', checkbox: true},
            {title: 'id', field: 'id', visible: true, align: 'center', valign: 'middle'},
            {title: '预案模板名称', field: 'name', visible: true, align: 'center', valign: 'middle'},
            {title: '类别', field: 'type', visible: true, align: 'center', valign: 'middle',sortable: true},
            {title: '事故类型', field: 'accidentTypeName', visible: true, align: 'center', valign: 'middle',sortable: true},
            {title: '响应等级', field: 'levelName', visible: true, align: 'center', valign: 'middle', sortable: true},
            {title: '地点', field: 'stationName', align: 'center', valign: 'middle', sortable: true},
            {title: '岗位', field: 'accidentMain', align: 'center', valign: 'middle', sortable: true},
    ];
};

/**
 * 检查是否选中
 */
CounterplanTemplate.check = function () {
    var selected = $('#' + this.id).bootstrapTable('getSelections');
    if(selected.length == 0){
        Feng.info("请先选中表格中的某一记录！");
        return false;
    }else{
        CounterplanTemplate.seItem = selected[0];
        CounterplanTemplate.seItems = selected;
        return true;
    }
};

/**
 * 点击添加预案模板
 */
CounterplanTemplate.openAddCounterplanTemplate = function () {
    var index = layer.open({
        type: 2,
        title: '添加预案模板',
        area: ['1200px', '600px'], //宽高
        fix: false, //不固定
        maxmin: true,
        content: Feng.ctxPath + '/counterplanTemplate/counterplanTemplate_add'
    });
    this.layerIndex = index;
    layer.full(index);
};

/**
 * 打开查看预案模板详情
 */
CounterplanTemplate.openCounterplanTemplateDetail = function () {
    if (this.check()) {
        if(CounterplanTemplate.seItems.length>1){
            Feng.info("仅可以选择一条预案模板修改！");
            return true;
        }
        var index = layer.open({
            type: 2,
            title: '修改预案模板',
            area: ['1200px', '600px'], //宽高
            fix: false, //不固定
            maxmin: true,
            content: Feng.ctxPath + '/counterplanTemplate/counterplanTemplate_update/' + CounterplanTemplate.seItem.id
        });
        this.layerIndex = index;
        layer.full(index);
    }
};

/**
 * 编制预案
 */
CounterplanTemplate.addCounterplan = function () {
    if (this.check()) {
        var index = layer.open({
            type: 2,
            title: '编制预案',
            area: ['1200px', '600px'], //宽高
            fix: true, //不固定
            maxmin: true,
            content: Feng.ctxPath + '/counterplan/counterplan_add?templateId=' + CounterplanTemplate.seItem.id
        });
        this.layerIndex = index;
        layer.full(index);
    }
};



CounterplanTemplate.detail = function () {
    if (this.check()) {
        if(CounterplanTemplate.seItems.length>1){
            Feng.info("仅可以选择一条预案模板预览！");
            return true;
        }
        var index = layer.open({
            type: 2,
            title: '预案模板详情',
            area: ['1200px', '600px'], //宽高
            fix: false, //不固定
            maxmin: true,
            content: Feng.ctxPath + '/counterplanTemplate/detail/' + CounterplanTemplate.seItem.id
        });
        this.layerIndex = index;
        layer.full(index);
    }
}

/**
 * 删除预案模板
 */
CounterplanTemplate.delete = function () {
    if (this.check()) {
        var seItems = CounterplanTemplate.seItems;
        var delIds = [];
        seItems.forEach(function(element){
            delIds.push(element.id);
        });
        var operation = function(){
            var ajax = new $ax(Feng.ctxPath + "/counterplanTemplate/delete", function () {
                Feng.success("删除成功!");
                CounterplanTemplate.table.refresh();
            }, function (data) {
                Feng.error("删除失败!" + data.responseJSON.message + "!");
            });
            ajax.set("ids",delIds);
            ajax.start();
        };
        Feng.confirm("是否删除选中预案模板?" ,operation);


    }
};

/**
 * 预案模板导入
 */
CounterplanTemplate.openAddCounterplanTemplateExport = function () {
    var index = layer.open({
        type: 2,
        title: '预案模板导入',
        area: ['1200px', '600px'], //宽高
        fix: false, //不固定
        maxmin: true,
        content: Feng.ctxPath + '/counterplanTemplate/counterplateTemplateExport'
    });
    this.layerIndex = index;
    layer.full(index);
};

CounterplanTemplate.queryParams = function () {
    var queryData = {};
    queryData['name'] = $("#q_name").val();
    queryData['type'] = $("#type").val();
    queryData['level'] = $("#level").val();
    queryData['accidentTypeId'] = $("#accidentTypeId").val();

    return queryData;
}


/**
 * 查询预案模板列表
 */
CounterplanTemplate.search = function () {
    var queryData = this.queryParams();
    CounterplanTemplate.table.refresh({query: queryData});
};

CounterplanTemplate.upload = function () {
    CounterplanTemplate.uploadInstance;
}

CounterplanTemplate.showAccidentTypeSelectTree = function () {

    Feng.showInputTree("accidentTypeName", "accidentTypeTreeDiv", 15, 34);
}

CounterplanTemplate.AutoMatchAccidentTypeSelectTree = function(txtObj){
    if (txtObj.value.length > 0) {
        var accidentTypeTree = new $ZTree("accidentTypeTree", "/accidentType/tree");
        accidentTypeTree.bindOnClick(CounterplanTemplate.onClickAccidentType);
        accidentTypeTree.init();
        accidentTypeTree.init(txtObj);
        CounterplanTemplate.accidentTypeTreeInstance = accidentTypeTree;
        Feng.showInputTree("accidentTypeName", "accidentTypeTreeDiv", 15, 34);
    }
    else{
        var accidentTypeTree = new $ZTree("accidentTypeTree", "/accidentType/tree");
        accidentTypeTree.bindOnClick(CounterplanTemplate.onClickAccidentType);
        accidentTypeTree.init();
        CounterplanTemplate.accidentTypeTreeInstance = accidentTypeTree;
        Feng.showInputTree("accidentTypeName", "accidentTypeTreeDiv", 15, 34);
    }
}

CounterplanTemplate.onClickAccidentType=function (e, treeId, treeNode) {
    $("#accidentTypeName").val(CounterplanTemplate.accidentTypeTreeInstance.getSelectedVals());
    $("#accidentTypeId").attr("value", treeNode.id);
    $("#accidentTypeTreeDiv").fadeOut("fast");

}

/**
 * 重置
 */
CounterplanTemplate.resetSearch = function(){
    $("#q_name").val("");
    $("#type").val("");
    $("#level").val("");
    $("#accidentTypeName").val("");
    $("#accidentTypeId").val("");

}


$(function () {
    var defaultColunms = CounterplanTemplate.initColumn();
    var table = new BSTable(CounterplanTemplate.id, "/counterplanTemplate/list", defaultColunms);
    table.setPaginationType("server");
    table.init();
    CounterplanTemplate.table = table;
        var upload = new $WebUpload({
        uploadId:"upload",
        mimeTypes:"application/msword,application/vnd.openxmlformats-officedocument.wordprocessingml.document",
        extensions:"doc,docx",
        url:"/counterplanTemplate/counterplateTemplateExport",
        multiple:true
    });
    upload.init();
    CounterplanTemplate.uploadInstance = upload;

    var accidentTypeTree = new $ZTree("accidentTypeTree", "/accidentType/tree");
    accidentTypeTree.bindOnClick(CounterplanTemplate.onClickAccidentType);
    accidentTypeTree.init();
    CounterplanTemplate.accidentTypeTreeInstance = accidentTypeTree;

});
