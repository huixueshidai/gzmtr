package com.stylefeng.guns.modular.assessment.calculate.line.facility;

import com.stylefeng.guns.common.persistence.model.Assessment;
import com.stylefeng.guns.core.util.DoubleUtil;
import com.stylefeng.guns.modular.assessment.calculate.ThirdIndicationCalculator;
import com.stylefeng.guns.modular.assessment.calculate.aware.LineThirdIndicationAware;
import org.springframework.stereotype.Component;

import java.util.Date;

/**
 * 土建系统影响运营风险指数
 *
 * @author yinjc
 * @since 2018/05/09
 */
@Component
public class LineCivilEngineeringImpactCal extends ThirdIndicationCalculator implements LineThirdIndicationAware {

    @Override
    protected Integer getIndicationId() {
        return 65;
    }

    @Override
    public Assessment calculate(Integer lineId, Date beginDate, Date endDate) {
        //土建系统故障率
        Double rateOfFailure = getDoubleValue(lineId, 229, beginDate,endDate);
        //土建系统平均故障修复时间
        Double fixAvgDuration = getDoubleValue(lineId, 230, beginDate,endDate);
        //土建故障修复时间
        //Double fixDuration = getDoubleValue(lineId, 231, beginDate,endDate));
        //土建故障导致的影响行车故障次数
        //Double numOfFailure = getDoubleValue(lineId, 232, beginDate,endDate));
        //受土建影响所耽误的列车正线运营里程
        Double loseMileageForFailure = getDoubleValue(lineId, 233, beginDate,endDate);
        //列车正线计划运营里程数(土建)
        Double mileageOfPlan = getDoubleValue(lineId, 234, beginDate,endDate);

        Double resultVal = DoubleUtil.add(rateOfFailure,
                DoubleUtil.safeDiv(DoubleUtil.sub(mileageOfPlan, loseMileageForFailure), mileageOfPlan),
                DoubleUtil.safeDiv(1, fixAvgDuration)
        );

        return genResult(resultVal, lineId, beginDate,endDate);
    }
}
