package com.stylefeng.guns.modular.data.transfer;

import cn.afterturn.easypoi.excel.entity.result.ExcelVerifyHandlerResult;
import cn.afterturn.easypoi.handler.inter.IExcelVerifyHandler;
import com.stylefeng.guns.common.constant.factory.ConstantFactory;
import com.stylefeng.guns.common.persistence.model.ResourceDataType;
import org.apache.commons.lang3.StringUtils;
import org.apache.ibatis.exceptions.TooManyResultsException;
import org.mybatis.spring.MyBatisSystemException;

public class ResourceDataXlsVerifyHandler implements IExcelVerifyHandler<ResourceDataXlsM> {

    @Override
    public ExcelVerifyHandlerResult verifyHandler(ResourceDataXlsM resourceDataXlsM) {
        StringBuffer sb = new StringBuffer();

		if (resourceDataXlsM.getResourceDataTypeName() != null) {
			ResourceDataType resourceDataType = ConstantFactory.me().getResourceDataTypeByName(resourceDataXlsM.getResourceDataTypeName());

			if (resourceDataType == null) {
				sb.append("该源数据'" + resourceDataXlsM.getResourceDataTypeName() + "不存在");
			}else{
				boolean needEntrance = resourceDataType.isNeedEntrance();
				if (needEntrance && StringUtils.isBlank(resourceDataXlsM.getEntrance())) {
					sb.append("请填写进站口");
				}
			}
		}

		if (resourceDataXlsM.getRoadnet() != null) {
			Integer roadnetId = null;
			try {
				roadnetId = ConstantFactory.me().getRoadnetIdByName(resourceDataXlsM.getRoadnet());
				if (roadnetId == null) {
					sb.append("该车站、线路或区域中心'" + resourceDataXlsM.getRoadnet() + "不存在");
				}
			} catch (MyBatisSystemException e) {
				sb.append("存在多个" + resourceDataXlsM.getRoadnet() + ",无法确定是哪一个");
			}
		}

        boolean success = sb.length() > 0 ? false : true;
        return new ExcelVerifyHandlerResult(success, sb.toString());
    }



}