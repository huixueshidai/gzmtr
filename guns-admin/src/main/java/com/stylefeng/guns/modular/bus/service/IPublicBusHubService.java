package com.stylefeng.guns.modular.bus.service;

import com.stylefeng.guns.common.persistence.model.PublicBusHub;
import com.baomidou.mybatisplus.service.IService;

/**
 * <p>
 * 公交枢纽站信息 服务类
 * </p>
 *
 * @author yinjc
 * @since 2018-03-21
 */
public interface IPublicBusHubService extends IService<PublicBusHub> {
	
}
