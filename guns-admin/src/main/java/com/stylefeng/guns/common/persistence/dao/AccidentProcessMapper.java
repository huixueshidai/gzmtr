package com.stylefeng.guns.common.persistence.dao;

import com.baomidou.mybatisplus.plugins.Page;
import com.stylefeng.guns.common.persistence.model.AccidentProcess;
import com.baomidou.mybatisplus.mapper.BaseMapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

/**
 * <p>
  *  Mapper 接口
 * </p>
 *
 * @author 殷佳成
 * @since 2018-01-17
 */
public interface AccidentProcessMapper extends BaseMapper<AccidentProcess> {
    List<Map<String,Object>> selectProcesses(@Param("page") Page<AccidentProcess> page , @Param("main")String  main, @Param("typeId")Integer typeId);
}