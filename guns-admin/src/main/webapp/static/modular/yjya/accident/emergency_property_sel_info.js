var EmergencyPropertyInfoDlg = {

}
EmergencyPropertyInfoDlg.close = function(){
    parent.layer.close(window.parent.AccidentInfoDlg.layerIndex);
}

/**
 * 突发事件属性选择提交
 */
EmergencyPropertyInfoDlg.emergencyPropertySubmit = function () {

    var list = document.getElementsByName("mm");
    var emergencyProperty = "";
    for(var i =0;i<list.length;i++){
        if(list[i].checked){
            emergencyProperty = emergencyProperty+list[i].value+";";
        }
    }
    window.parent.AlarmInfoDlg.emergencyPropertyAssign(emergencyProperty);
    EmergencyPropertyInfoDlg.close();

};