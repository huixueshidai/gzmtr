/**
 * 事故致因管理初始化
 */
var RiskFactor = {
    id: "RiskFactorTable",	//表格id
    seItem: null,		//选中的条目
    table: null,
    layerIndex: -1
};

/**
 * 初始化表格的列
 */
RiskFactor.initColumn = function () {
    return [
        {field: 'selectItem', radio: true},
        {title: '编码', field: 'id', visible: true, align: 'center', valign: 'middle'},
        {title: '名称', field: 'name', visible: true, align: 'center', valign: 'middle'},
        {title: '排序', field: 'num', visible: true, align: 'center', valign: 'middle'},
        {title: '中值', field: 'median', visible: true, align: 'center', valign: 'middle'},
        {title: '上限', field: 'upper', visible: true, align: 'center', valign: 'middle'},
        {title: '下限', field: 'flo', visible: true, align: 'center', valign: 'middle'},
        {title: '状态', field: 'status', visible: false, align: 'center', valign: 'middle'},
    ];
};

/**
 * 检查是否选中
 */
RiskFactor.check = function () {
    var selected = $('#' + this.id).bootstrapTreeTable('getSelections');
    if (selected.length == 0) {
        Feng.info("请先选中表格中的某一记录！");
        return false;
    } else {
        RiskFactor.seItem = selected[0];
        return true;
    }
};

/**
 * 点击添加事故致因
 */
RiskFactor.openAddRiskFactor = function () {
    var index = layer.open({
        type: 2,
        title: '添加事故致因',
        area: ['800px', '420px'], //宽高
        fix: false, //不固定
        maxmin: true,
        content: Feng.ctxPath + '/riskFactor/riskFactor_add'
    });
    this.layerIndex = index;
};

/**
 * 打开查看事故致因详情
 */
RiskFactor.openRiskFactorDetail = function () {
    if (this.check()) {
        var index = layer.open({
            type: 2,
            title: '事故致因详情',
            area: ['800px', '420px'], //宽高
            fix: false, //不固定
            maxmin: true,
            content: Feng.ctxPath + '/riskFactor/riskFactor_update/' + RiskFactor.seItem.id
        });
        this.layerIndex = index;
    }
};

/**
 * 删除事故致因
 */
RiskFactor.delete = function () {
    if (this.check()) {
        var ajax = new $ax(Feng.ctxPath + "/riskFactor/delete", function (data) {
            Feng.success("删除成功!");
            RiskFactor.table.refresh();
        }, function (data) {
            Feng.error("删除失败!" + data.responseJSON.message + "!");
        });
        ajax.set("riskFactorId", this.seItem.id);
        ajax.start();
    }
};


RiskFactor.setStatusDefault = function () {
    if (this.check()) {
        var ajax = new $ax(Feng.ctxPath + "/riskFactor/seDefaultStatus", function (data) {
            Feng.success("设置成功!");
            RiskFactor.table.refresh();
        }, function (data) {
            Feng.error("设置失败!" + data.responseJSON.message + "!");
        });
        ajax.set("id", this.seItem.id);
        ajax.start();
    }
};

/**
 * 查询事故致因列表
 */
RiskFactor.search = function () {
    var queryData = {};
    queryData['name'] = $("#name").val();
    RiskFactor.table.refresh({query: queryData});
};

$(function () {
    var defaultColunms = RiskFactor.initColumn();
    var table = new BSTreeTable(RiskFactor.id, "/riskFactor/list", defaultColunms);
    table.setExpandColumn(2);
    table.setIdField("id");
    table.setCodeField("id");
    table.setParentCodeField("pid");
    table.setExpandAll(false);
    table.init();
    RiskFactor.table = table;
});
