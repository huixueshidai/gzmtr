/**
 * 源数据管理初始化
 */
var ResourceData = {
    id: "ResourceDataTable",	//表格id
    seItem: null,		//选中的条目
    table: null,
    layerIndex: -1,
    uploadInstance: null
};

/**
 * 初始化表格的列
 */
ResourceData.initColumn = function () {
    return [
        {field: 'selectItem', radio: true},
        {title: 'id', field: 'id', visible: true, align: 'center', valign: 'middle'},
        {title: '源数据类型', field: 'resourceDataTypeName', visible: true, align: 'center', valign: 'middle'},
        {title: '所属', field: 'roadnetName', visible: true, align: 'center', valign: 'middle'},
        {title: '值', field: 'value', visible: true, align: 'center', valign: 'middle'},
        {title: '站口', field: 'entrance', visible: true, align: 'center', valign: 'middle'},
        {title: '开始时间', field: 'startTime', visible: true, align: 'center', valign: 'middle'},
        {title: '结束时间', field: 'endTime', visible: true, align: 'center', valign: 'middle'}
    ];
};

/**
 * 检查是否选中
 */
ResourceData.check = function () {
    var selected = $('#' + this.id).bootstrapTable('getSelections');
    if (selected.length == 0) {
        Feng.info("请先选中表格中的某一记录！");
        return false;
    } else {
        ResourceData.seItem = selected[0];
        return true;
    }
};

/**
 * 点击添加源数据
 */
ResourceData.openAddResourceData = function () {
    var index = layer.open({
        type: 2,
        title: '添加源数据',
        area: ['850px', '420px'], //宽高
        fix: false, //不固定
        maxmin: true,
        content: Feng.ctxPath + '/resourceData/resourceData_add'
    });
    this.layerIndex = index;
};

/**
 * 打开查看源数据详情
 */
ResourceData.openResourceDataDetail = function () {
    if (this.check()) {
        var index = layer.open({
            type: 2,
            title: '源数据详情',
            area: ['850px', '420px'], //宽高
            fix: false, //不固定
            maxmin: true,
            content: Feng.ctxPath + '/resourceData/resourceData_update/' + ResourceData.seItem.id
        });
        this.layerIndex = index;
    }
};

/**
 * 删除源数据
 */
ResourceData.delete = function () {
    if (this.check()) {
        var operation = function () {
            var ajax = new $ax(Feng.ctxPath + "/resourceData/delete", function (data) {
                Feng.success("删除成功!");
                ResourceData.table.refresh();
            }, function (data) {
                Feng.error("删除失败!" + data.responseJSON.message + "!");
            });
            ajax.set("resourceDataId", ResourceData.seItem.id);
            ajax.start();
        }
        Feng.confirm("是否删除数据", operation);
    }

};

/**
 * 清空源数据
 */
ResourceData.deleteAll = function () {
        var operation = function () {
            var ajax = new $ax(Feng.ctxPath + "/resourceData/delete/all", function (data) {
                Feng.success("清除成功!");
                ResourceData.table.refresh();
            }, function (data) {
                Feng.error("清除失败!" + data.responseJSON.message + "!");
            });
            ajax.start();

        }
        Feng.confirm("是否清除数据", operation);

}

ResourceData.exportXlsByT = function (pids) {
    window.location.href = Feng.ctxPath + "/resourceData/exportXlsByT?pids=" + pids;
}

/**
 * 重置
 */
ResourceData.resetSearch = function(){
    $("#resourceDataTypeName").val("");
    $("#roadnetName").val("");
    $("#startTime").val("");
    $("#endTime").val("");
}


/**
 * 查询源数据列表
 */
ResourceData.search = function () {
    var queryData = {};
    queryData['startTime'] = $("#startTime").val();
    queryData['endTime'] = $("#endTime").val();
    queryData['resourceDataTypeName'] = $("#resourceDataTypeName").val();
    queryData['roadnetName'] = $("#roadnetName").val();
    ResourceData.table.refresh({query: queryData});
};


$(function () {
    var defaultColunms = ResourceData.initColumn();
    var table = new BSTable(ResourceData.id, "/resourceData/list", defaultColunms);
    table.setPaginationType("server");
    ResourceData.table = table.init();


    var upload = new $WebUpload({
        uploadId: "upload",
        mimeTypes: "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet,application/vnd.ms-excel",
        extensions: "xlsx,xls",
        url: "/resourceData/importExcel",
        multiple: false
    });
    upload.successCallBack = function(){
        var state = arguments[1].code;
        var msg = arguments[1].msg;
        console.log(arguments[1]);
        if(state == 422){
            Feng.error("上传失败");
            window.location.href = Feng.ctxPath + "/excel/failFile?fileName="+msg;
        }else if(state == 200){
            Feng.success(msg);
        }else{
            Feng.error(msg);
        }
    };

    upload.init();
    ResourceData.uploadInstance = upload;
});
