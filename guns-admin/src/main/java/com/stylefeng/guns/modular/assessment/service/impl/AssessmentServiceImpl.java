package com.stylefeng.guns.modular.assessment.service.impl;

import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.stylefeng.guns.common.constant.cache.Cache;
import com.stylefeng.guns.common.exception.BussinessException;
import com.stylefeng.guns.common.persistence.dao.AssessmentMapper;
import com.stylefeng.guns.common.persistence.model.Assessment;
import com.stylefeng.guns.common.plugin.entity.Period;
import com.stylefeng.guns.modular.assessment.calculate.area.AreaCalculator;
import com.stylefeng.guns.modular.assessment.calculate.exception.CalculateException;
import com.stylefeng.guns.modular.assessment.calculate.line.LineCalculator;
import com.stylefeng.guns.modular.assessment.calculate.road.RoadCal;
import com.stylefeng.guns.modular.assessment.calculate.station.StationCalculate;
import com.stylefeng.guns.modular.assessment.service.IAssessmentService;
import com.stylefeng.guns.modular.system.dao.RoadnetDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Lookup;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.List;

import static com.stylefeng.guns.common.exception.BizExceptionEnum.CALCULATE_ERROR;
import static java.util.stream.Collectors.toList;

/**
 * <p>
 * 指标评估 服务实现类
 * </p>
 *
 * @author xuziyang
 * @since 2018-03-27
 */
@Service
public class AssessmentServiceImpl extends ServiceImpl<AssessmentMapper, Assessment> implements IAssessmentService {

	@Autowired
	private RoadnetDao roadnetDao;

	@Override
	public List assessmentMulPeriod(Integer roadnetId, List<Period> periodList) {
		return periodList.stream()
				.map(period -> assessmentOnePeriod(roadnetId, period.getBeginDate(), period.getEndDate()))
				.collect(toList());
	}


	@Override
	public Assessment assessmentOnePeriod(Integer roadnetId, Date beginDate, Date endDate) {
		try {
			if (roadnetId == 0) { //路网
				return getRoadCal().get(roadnetId, beginDate, endDate);
			}

			String type = roadnetDao.getTypeById(roadnetId);
			if (type.equals("1")) { //
				return getAreaCal().get(roadnetId, beginDate, endDate);
			}
			if (type.equals("3")) { //车站
				return getStationCalculate().get(roadnetId, beginDate, endDate);
			}
			if (type.equals("2")) { //线路
				return getLineCal().get(roadnetId, beginDate, endDate);
			}
		} catch (CalculateException e) {
			throw e;
		} catch (Exception e){
			e.printStackTrace();
			throw new BussinessException(CALCULATE_ERROR);
		}

		return null;
	}

	@Override
	public Assessment selectOne(Integer indicationId, Integer roadnetId, String beginTime, String endTime) {
		return this.baseMapper.selectAssessment(indicationId, roadnetId, beginTime, endTime);
	}


	@Lookup
	public LineCalculator getLineCal() {
		return null;
	}

	@Lookup
	public AreaCalculator getAreaCal() {
		return null;
	}

	@Lookup
	public RoadCal getRoadCal() {
		return null;
	}

	@Lookup
	public StationCalculate getStationCalculate() {
		return null;
	}


}
