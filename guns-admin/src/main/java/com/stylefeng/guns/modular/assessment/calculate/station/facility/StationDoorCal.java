package com.stylefeng.guns.modular.assessment.calculate.station.facility;

import com.stylefeng.guns.common.persistence.model.Assessment;
import com.stylefeng.guns.core.util.DoubleUtil;
import com.stylefeng.guns.modular.assessment.calculate.ThirdIndicationCalculator;
import com.stylefeng.guns.modular.assessment.calculate.aware.StationThirdIndicationAware;
import org.apache.commons.collections.CollectionUtils;
import org.springframework.stereotype.Component;

import java.util.Date;
import java.util.List;

/**
 * 屏蔽门安全指数
 */
@Component
public class StationDoorCal extends ThirdIndicationCalculator implements StationThirdIndicationAware {
    @Override
    protected Integer getIndicationId() {
        return 29;
    }

    @Override
    public Assessment calculate(Integer stationId, Date beginDate, Date endDate) {

        List<Double> ks = this.getMulDoubleValue(stationId, 50, beginDate,endDate);

        //车门总数
        Double n = this.getDoubleValue(stationId, 51, beginDate,endDate);
        if (n == 0 || CollectionUtils.isEmpty(ks)) {
            return genResult(1d, stationId, beginDate,endDate);
        }

        Long baoJingNum = ks.stream().filter(d -> d == 1d).count();

        Double resultValue = DoubleUtil.sub(1, DoubleUtil.safeDiv(baoJingNum, n));
        return genResult(resultValue, stationId, beginDate,endDate);
    }
}
