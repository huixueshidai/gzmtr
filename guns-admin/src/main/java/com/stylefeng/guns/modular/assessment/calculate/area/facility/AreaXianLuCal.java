package com.stylefeng.guns.modular.assessment.calculate.area.facility;

import com.stylefeng.guns.common.persistence.model.Assessment;
import com.stylefeng.guns.modular.assessment.calculate.ThirdIndicationCalculator;
import com.stylefeng.guns.modular.assessment.calculate.area.facility.util.AreaRoadFacilityCalculatorUtil;
import com.stylefeng.guns.modular.assessment.calculate.aware.AreaThirdIndicationAware;
import com.stylefeng.guns.modular.assessment.calculate.line.facility.LineTheWiringSystemAffectsTransportationCal;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Date;
import java.util.List;

/**
 * 线路系统影响运营风险指数
 *
 * @author yinjc
 * @since 2018/05/11
 */
@Component
public class AreaXianLuCal extends ThirdIndicationCalculator implements AreaThirdIndicationAware {

    @Autowired
    private LineTheWiringSystemAffectsTransportationCal lineTheWiringSystemAffectsTransportationCal;

    @Override
    protected Integer getIndicationId() {
        return 85;
    }

    @Override
    public Assessment calculate(Integer areaId, Date beginDate, Date endDate) {
        List<Double> w = getMulDoubleValue(areaId, 300, beginDate,endDate);
        Double resVal = AreaRoadFacilityCalculatorUtil.calculate(areaId, lineTheWiringSystemAffectsTransportationCal, w, beginDate,endDate);
        return genResult(resVal, areaId, beginDate,endDate);
    }
}
