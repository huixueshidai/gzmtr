var ProcessExecuteInfo = {
    flowInstance:null
    , jsondata: null
};

ProcessExecuteInfo.property = {
    toolBtns:[],
    haveHead:true,
    headBtns:[],//如果haveHead=true，则定义HEAD区的按钮
    haveTool:true,
    haveGroup:true,
    useOperStack:true
};


ProcessExecuteInfo.onItemDbClick = function (id,type) {
    AccidentInfoDlg.dispositionStep = [];
    var report = "";
    var curTime = new Date().toLocaleTimeString();
    var item = this.flowInstance.getItemInfo(id, type);
    this.flowInstance.markItem(id, type, true);
    report += curTime;
    if(item.type == 'fork'){
        var children = this.flowInstance.getChildren(id);
        for(var key in children) {
            var child = children[key];
            this.flowInstance.markItem(key, "node", true);
            report += ("\n" + child.name);
            AccidentInfoDlg.dispositionStep.push(key);
        }
        report += "\n";
    }else{
        AccidentInfoDlg.dispositionStep.push(id);
        report += ("\n " + item.name + "\n");
    }
    console.log(AccidentInfoDlg.dispositionStep);
    $("#report").append(report);
    AlarmInfoDlg.submitdispositionStep();
}

ProcessExecuteInfo.addSign = function(nodeIds){
    for(var i=0;i<nodeIds.length;i++){
        this.flowInstance.markItem(nodeIds[i], "node", true);
    }

}


ProcessExecuteInfo.getJson = function () {
    var json = this.flowInstance.exportData();
}


ProcessExecuteInfo.init = function (json) {
    if(this.flowInstance) {
        this.flowInstance.destrory();
    }
    var flow = $.createGooFlow($("#processExe"), this.property);
    flow.setTitle("处置流程");
    flow.loadData(JSON.parse(json));

    this.flowInstance = flow;

    flow.onBtnSaveClick = function () {
        ProcessExecuteInfo.save();
    }

    flow.onItemDbClick = function (id, type) {
        ProcessExecuteInfo.onItemDbClick(id, type);
        return false;//返回false可以阻止原组件自带的双击直接编辑事件
    }
    ProcessExecuteInfo.flowInstance = flow;
};
