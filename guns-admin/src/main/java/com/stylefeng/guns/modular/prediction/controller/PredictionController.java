package com.stylefeng.guns.modular.prediction.controller;

import com.github.abel533.echarts.json.GsonOption;
import com.stylefeng.guns.modular.prediction.service.impl.PredictionServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.swing.text.html.Option;
import java.util.Date;

@Controller
@RequestMapping("prediction")
public class PredictionController {

    private String PREFIX = "/prediction/prediction/";

    @Autowired
    private PredictionServiceImpl predictionService;

    @RequestMapping("prediction_chart")
    public String index() {
        return PREFIX + "prediction_chart.html";
    }

    @RequestMapping("analyse")
    @ResponseBody
    public Object analyse(@RequestParam("category")String category,
					     @RequestParam(value = "year",required = false)@DateTimeFormat(pattern = "yyyy") Date year,
			             @RequestParam(value = "month",required = false)@DateTimeFormat(pattern = "yyyy-MM") Date month) {

		String jsonResult = "";
		if (year != null) {
			jsonResult = predictionService.calculateByYear(category, year).toString();
		}else if(month != null){
			jsonResult = predictionService.calculateByMonth(category, month).toString();
		}

		return jsonResult;
    }
}
