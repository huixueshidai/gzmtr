package com.stylefeng.guns.common.persistence.model;

import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;
import com.baomidou.mybatisplus.enums.IdType;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.io.Serializable;

/**
 * <p>
 * 预案
 * </p>
 *
 * @author xuziyang
 * @since 2018-01-18
 */
@TableName("gzmtr_counterplan")
@ToString
@Getter
@Setter
public class Counterplan extends Model<Counterplan> {

    private static final long serialVersionUID = 1L;

	@TableId(value="id", type= IdType.AUTO)
	private Integer id;
    /**
     * 预案名称
     */
	private String name;
    /**
     * 类别
     */
	private String type;
    /**
     * 响应等级
     */
	private String level;
    /**
     * 内容
     */
	private String content;

	/**
	 * 岗位
	 */
	@TableField("accident_main")
	private String accidentMain;

    /**
     * 车站
     */
	@TableField("station_id")
	private String stationId;

	@TableField("accident_type_id")
	private Integer accidentTypeId;

	/**
	 *状态（0：未审核 1：审核通过 2：审核未通过）
	 */
	private Integer status;

	@Override
	protected Serializable pkVal() {
		return this.id;
	}

}
