package com.stylefeng.guns.modular.path.service;

import com.stylefeng.guns.common.persistence.model.PathEvolution;
import com.baomidou.mybatisplus.service.IService;

/**
 * <p>
 * 事故情景路径演练 服务类
 * </p>
 *
 * @author xhq
 * @since 2018-09-04
 */
public interface IPathEvolutionService extends IService<PathEvolution> {
	
}
