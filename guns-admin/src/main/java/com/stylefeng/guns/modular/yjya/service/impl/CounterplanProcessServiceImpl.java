package com.stylefeng.guns.modular.yjya.service.impl;

import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.stylefeng.guns.common.persistence.dao.AccidentProcessMapper;
import com.stylefeng.guns.common.persistence.dao.CounterplanProcessMapper;
import com.stylefeng.guns.common.persistence.model.AccidentProcess;
import com.stylefeng.guns.common.persistence.model.CounterplanProcess;
import com.stylefeng.guns.modular.yjya.dao.CounterplanProcessDao;
import com.stylefeng.guns.modular.yjya.service.ICounterplanProcessService;
import org.apache.commons.beanutils.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.lang.reflect.InvocationTargetException;
import java.util.*;

/**
 * <p>
 * 预案处置流程表 服务实现类
 * </p>
 *
 * @author xuziyang
 * @since 2018-03-01
 */
@Service
public class CounterplanProcessServiceImpl extends ServiceImpl<CounterplanProcessMapper, CounterplanProcess> implements ICounterplanProcessService {

    @Autowired
    private AccidentProcessMapper accidentProcessMapper;

    @Autowired
    private CounterplanProcessDao counterplanProcessDao;

    @Override
    @Transactional
    public void insert(Integer counterplanId, String[] accidentIds) {
        if (accidentIds.length < 1) {
            return;
        }

        for (int i = 0 ; i< accidentIds.length ; i ++) {
            String[] ids = accidentIds[i].split(",");
            for (int j = 0 ; j < ids.length ; j++) {
                CounterplanProcess counterplanAccident = new CounterplanProcess();
                AccidentProcess accidentProcess = accidentProcessMapper.selectById(ids[j]);
                try {
                    BeanUtils.copyProperties(counterplanAccident, accidentProcess);
                } catch (IllegalAccessException e) {
                    e.printStackTrace();
                } catch (InvocationTargetException e) {
                    e.printStackTrace();
                }
                counterplanAccident.setCounterplanId(counterplanId);
                counterplanAccident.setNum(i);
                insert(counterplanAccident);
            }

        }
    }

    @Override
    public void update(Integer counterplanId, String[] accidentIds) {
        deleteByCounterplanId(counterplanId);
        this.insert(counterplanId,accidentIds);
    }

    @Override
    public void deleteByCounterplanId(Integer counterplanId) {
        counterplanProcessDao.deleteBycounterplanId(counterplanId);
    }

    @Override
    public List<List<String>> selectProcesses(Integer counterplanId) {
        List<CounterplanProcess> counterplanProcessList = counterplanProcessDao.selectListByCounterplanId(counterplanId);
        List<List<String>> result = new ArrayList<>();
        for (CounterplanProcess counterplanProcess : counterplanProcessList) {
            Integer num = counterplanProcess.getNum();
            List<String> theSameNumStringList;
            if (result.size() > num ) {
                theSameNumStringList = result.get(num);
            }else {
                theSameNumStringList = new ArrayList<>();
                result.add(theSameNumStringList);
            }
            theSameNumStringList.add(counterplanProcess.toString());
        }
        return result;
    }
}
