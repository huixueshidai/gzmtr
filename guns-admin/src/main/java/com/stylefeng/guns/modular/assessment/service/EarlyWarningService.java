package com.stylefeng.guns.modular.assessment.service;

import com.stylefeng.guns.common.constant.factory.ConstantFactory;
import com.stylefeng.guns.common.persistence.model.Assessment;
import com.stylefeng.guns.common.persistence.model.Indication;
import com.stylefeng.guns.modular.assessment.calculate.area.AreaCalculator;
import com.stylefeng.guns.modular.assessment.calculate.line.LineCalculator;
import com.stylefeng.guns.modular.assessment.calculate.road.RoadCal;
import com.stylefeng.guns.modular.assessment.calculate.station.StationCalculate;
import com.stylefeng.guns.modular.indication.service.IIndicationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;

@Service
public class EarlyWarningService {

    @Autowired
    private StationCalculate stationCalculate;

    @Autowired
    private LineCalculator lineCalculator;

    @Autowired
    private AreaCalculator areaCalculator;

    @Autowired
    private RoadCal roadCal;

    @Autowired
    private IIndicationService indicationService;

    public void tt(Date beginDate, Date endDate) {
        //获取路网
        List<Integer> areaIdList = ConstantFactory.me().getIsUsedChildrenIdsByPid(0);

    }

    private void area(Integer areaId) {
        List<Integer> lineIdList = ConstantFactory.me().getIsUsedChildrenIdsByPid(areaId);
    }


    private void line(Integer lineId,Date beginDate,Date endDate) {
        List<Integer> stationIdList = ConstantFactory.me().getIsUsedChildrenIdsByPid(lineId);
//        for()
    }

    public void station(Integer stationId, Date beginDate , Date endDate) {
        Assessment assessment = stationCalculate.get(stationId, beginDate, endDate);
        int level =  waringLevel(assessment);

    }

    public int waringLevel(Assessment assessment) {
        for (Assessment secondAssessment : assessment.getNexLvlList()) {
            for (Assessment thirdAsessment : secondAssessment.getNexLvlList()) {
                Indication indication = indicationService.selectById(thirdAsessment.getIndicationId());
                double thirdIndicationValue = thirdAsessment.getAssessmentValue();
                double max = indication.getMaxVal();
                double min = indication.getMinVal();
                if (indication.getBigIsBetter() == 1) {//越大越优
                    if (thirdIndicationValue < min) {
                        return 4;
                    }
                }else{
                    if (thirdIndicationValue >= max) {
                        return 4;
                    }
                }
            }
        }
        double firstIndicationVal = assessment.getAssessmentValue();
        if (firstIndicationVal <= 0.3) {
            return 1;
        } else if (firstIndicationVal <= 0.9) {
            return 2;
        } else if (firstIndicationVal < 1.5) {
            return 3;
        }else{
            return 4;
        }
    }



}
