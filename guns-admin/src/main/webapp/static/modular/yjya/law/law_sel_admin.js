var LawSel = {
    table: null,
    id: "LawSelTable",
    seItems: null,
}


LawSel.initColumn = function () {
    return [
        {field: 'selectItem', checkbox: true},
        {title: 'id', field: 'id', visible: true, align: 'center', valign: 'middle'},
        {title: '名称', field: 'name', visible: true, align: 'center', valign: 'middle'},
    ];
};

/**
 * 检查是否选中
 */
LawSel.check = function () {
    var selected = $('#' + this.id).bootstrapTable('getSelections');
    if (selected.length == 0) {
        Feng.info("请先选中表格中的某一记录！");
        return false;
    } else {
        LawSel.seItems = selected;
        return true;
    }
};

LawSel.close = function () {
    parent.layer.close(window.parent.CounterplanInfoDlg.layerIndex);
}

LawSel.addLaws = function () {
    if (this.check()) {
        var items = this.seItems;
        var laws = "";
        for(i in items) {
            laws += "《"+items[i].name+"》<br/>";
        }
        window.parent.CounterplanInfoDlg.ue.execCommand('inserthtml', laws);
        LawSel.close();
    }

}

$(function () {
    var defaultColunms = LawSel.initColumn();
    var table = new BSTable(LawSel.id, "/law/list", defaultColunms);
    table.setPaginationType("client");
    LawSel.table = table.init();
});