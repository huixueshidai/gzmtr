package com.stylefeng.guns.modular.yjya.controller;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.mapper.Wrapper;
import com.stylefeng.guns.common.persistence.dao.PositionStatementMapper;
import com.stylefeng.guns.core.base.controller.BaseController;
import com.stylefeng.guns.core.node.ZTreeNode;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.beans.factory.annotation.Autowired;
import com.stylefeng.guns.core.log.LogObjectHolder;
import org.springframework.web.bind.annotation.RequestParam;
import com.stylefeng.guns.common.persistence.model.PositionStatement;
import com.stylefeng.guns.modular.yjya.service.IPositionStatementService;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

/**
 * 岗位职责控制器
 *
 * @author 殷佳成
 * @Date 2018-09-07 14:42:35
 */
@Controller
@RequestMapping("/positionStatement")
public class PositionStatementController extends BaseController {

    private String PREFIX = "/yjya/positionStatement/";

    @Autowired
    private IPositionStatementService positionStatementService;
   @Autowired
    private PositionStatementMapper positionStatementMapper;
    /**
     * 跳转到岗位职责首页
     */
    @RequestMapping("")
    public String index() {
        return PREFIX + "positionStatement.html";
    }

    /**
     * 获取tree列表
     */
    @RequestMapping(value = "/tree")
    @ResponseBody
    public List<ZTreeNode> tree() {
        List<ZTreeNode> tree = this.positionStatementService.tree();
        return tree;
    }


    /**
     * 跳转到添加岗位职责
     */
    @RequestMapping("/positionStatement_add")
    public String positionStatementAdd() {
        return PREFIX + "positionStatement_add.html";
    }

    /**
     * 跳转到修改岗位职责
     */
    @RequestMapping("/positionStatement_update/{positionStatementId}")
    public String positionStatementUpdate(@PathVariable Integer positionStatementId, Model model) {
        PositionStatement positionStatement = positionStatementService.selectById(positionStatementId);
        model.addAttribute("item", positionStatement);
        String pName;
        if (positionStatement.getPid() == 0) {
            pName = "顶级";
        } else {
            pName = positionStatementService.selectById(positionStatement.getPid()).getName();
        }
        model.addAttribute("pName", pName);
        LogObjectHolder.me().set(positionStatement);
        return PREFIX + "positionStatement_edit.html";
    }

    /**
     * 预案或预案模板修改岗位职责
     */
    @RequestMapping("/treeMain")
    @ResponseBody
    public List<ZTreeNode> positionStatementUpdateCounter(@RequestParam(value="accidentMain") String accidentMain) {
        if(accidentMain!=null){
            Wrapper<PositionStatement> wrapper = new EntityWrapper<>();
            wrapper.orderBy("num");
            List<String> accidentMainList = new ArrayList(Arrays.asList(accidentMain.split(",")));
            List<ZTreeNode> tree = positionStatementService.tree(wrapper);
            for(int i =0;i<accidentMainList.size();i++){
                for(int j = 0;j<tree.size();j++){
                    if((String.valueOf(accidentMainList.get(i))).equals(tree.get(j).getName())){
                        tree.get(j).setChecked(true);
                    }
                }
            }
            return tree;
        }
        else{
            List<ZTreeNode> tree = this.positionStatementService.tree();
            return tree;
        }
    }

    /**
     * 获取岗位职责列表
     */
    @RequestMapping(value = "/list")
    @ResponseBody
    public Object list(String condition) {

        return positionStatementMapper.list(condition);
    }

    /**
     * 新增岗位职责
     */
    @RequestMapping(value = "/add")
    @ResponseBody
    public Object add(PositionStatement positionStatement) {
        return positionStatementService.addNode(positionStatement);

    }

    /**
     * 删除岗位职责
     */
    @RequestMapping(value = "/delete")
    @ResponseBody
    public Object delete(@RequestParam Integer positionStatementId) {
        positionStatementService.deleteById(positionStatementId);
        return SUCCESS_TIP;
    }

    /**
     * 修改岗位职责
     */
    @RequestMapping(value = "/update")
    @ResponseBody
    public Object update(PositionStatement positionStatement) {
        positionStatementService.updateNode(positionStatement);
        return super.SUCCESS_TIP;
    }

    /**
     * 岗位职责详情
     */
    @RequestMapping(value = "/detail/{positionStatementId}")
    @ResponseBody
    public Object detail(@PathVariable("positionStatementId") Integer positionStatementId) {
        return positionStatementService.selectById(positionStatementId);
    }
}
