package com.stylefeng.guns.common.persistence.dao;

import com.stylefeng.guns.common.persistence.model.CounterplanProcess;
import com.baomidou.mybatisplus.mapper.BaseMapper;

/**
 * <p>
  * 预案处置流程表 Mapper 接口
 * </p>
 *
 * @author xuziyang
 * @since 2018-03-01
 */
public interface CounterplanProcessMapper extends BaseMapper<CounterplanProcess> {

}