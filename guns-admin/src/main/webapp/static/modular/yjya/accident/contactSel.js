/**
 * 通讯录管理初始化
 */
var Contact = {
    id: "ContactTable",	//表格id
    seItem: null,		//选中的条目
    table: null,
    layerIndex: -1,
    groupId:0,
    uploadInstance:null
};

/**
 * 初始化表格的列
 */
Contact.initColumn = function () {
    return [
        {field: 'selectItem', checkout: true},
            {title: 'id', field: 'id', visible: true, align: 'center', valign: 'middle'},
            {title: '姓名', field: 'name', visible: true, align: 'center', valign: 'middle'},
            {title: '职务/专业', field: 'duty', visible: true, align: 'center', valign: 'middle'},
            {title: '行政职务', field: 'administrationDuty', visible: true, align: 'center', valign: 'middle'},
            {title: '座机', field: 'phone', visible: true, align: 'center', valign: 'middle'},
            {title: '手机号', field: 'mobilePhone', visible: true, align: 'center', valign: 'middle'},
            {title: '邮箱', field: 'email', visible: true, align: 'center', valign: 'middle'},
            {title: '群组', field: 'groupName', visible: true, align: 'center', valign: 'middle'}
    ];
};

/**
 * 检查是否选中
 */
Contact.check = function () {
    var selected = $('#' + this.id).bootstrapTable('getSelections');
    var selects = new Array();
    if(selected.length == 0){
        Feng.info("请先选择通讯录人员！");
        return false;
    }else{
        for(var i=0;i<selected.length;i++){
            selects[i] = selected[i].id;
        }
        Contact.seItem = selects.join(",");
        return true;
    }

};

/**
 * 点击添加通讯录
 */
Contact.openAddContact = function () {
    var index = layer.open({
        type: 2,
        title: '添加通讯录',
        area: ['800px', '500px'], //宽高
        fix: false, //不固定
        maxmin: true,
        content: Feng.ctxPath + '/contact/contact_add'
    });
    this.layerIndex = index;
};

/**
 * 打开查看通讯录详情
 */
Contact.openContactDetail = function () {
    if (this.check()) {
        var index = layer.open({
            type: 2,
            title: '通讯录详情',
            area: ['800px', '500px'], //宽高
            fix: false, //不固定
            maxmin: true,
            content: Feng.ctxPath + '/contact/contact_update/' + Contact.seItem.id
        });
        this.layerIndex = index;
    }
};

/**
 * 删除通讯录
 */
Contact.delete = function () {
    if (this.check()) {
        var ajax = new $ax(Feng.ctxPath + "/contact/delete", function (data) {
            Feng.success("删除成功!");
            Contact.table.refresh();
        }, function (data) {
            Feng.error("删除失败!" + data.responseJSON.message + "!");
        });
        ajax.set("contactId",this.seItem.id);
        ajax.start();
    }
};

Contact.resetSearch = function () {
    $("#name").val("");
    // Contact.search();

}

/**
 * 查询通讯录列表
 */
Contact.search = function () {
    var queryData = {};

    queryData['groupId'] = Contact.groupId;
    queryData['name'] = $("#name").val();
    Contact.table.refresh({query: queryData});
};

Contact.onClickContactGroup = function (e, treeId, treeNode) {
    Contact.groupId = treeNode.id;
    Contact.search();
}

Contact.exportXlsByM = function () {
    window.location.href = Feng.ctxPath + "/contact/exportXlsByM";
}

Contact.upload = function () {
    this.uploadInstance
}

/**
 * 添加选择的通讯录人员
 */
Contact.addToReport = function(){
    if (this.check()) {
        var ajax = new $ax(Feng.ctxPath + "/contact/reportContactSelAdd", function (data) {
            Feng.success("添加成功!");
            for(var i=0;i<data.length;i++){
                window.parent.AccidentReport.addItemContactSelData(data[i].name,data[i].duty,data[i].mobilePhone);
            }
        }, function (data) {
            Feng.error("添加失败!" + data.responseJSON.message + "!");
        });
        ajax.set("contactIds",this.seItem);
        ajax.start();

        parent.layer.close(window.parent.AccidentReport.layerIndex);
    }
}

$(function () {
    var defaultColunms = Contact.initColumn();
    var table = new BSTable(Contact.id, "/contact/reportContactSel", defaultColunms);
    table.setPaginationType("server");
    Contact.table = table.init();

    var ztree = new $ZTree("contactGroupTree", "/contactGroup/tree");
    ztree.bindOnClick(Contact.onClickContactGroup);
    ztree.init();

    var upload = new $WebUpload({
        uploadId:"upload",
        mimeTypes:"application/vnd.ms-excel",
        extensions:"xls",
        url:"/contact/importExcel",
        multiple:true
    });
    upload.init();
    Contact.uploadInstance = upload;
});
