package com.stylefeng.guns.modular.yjya.service;

import com.stylefeng.guns.common.persistence.model.PositionStatement;
import com.baomidou.mybatisplus.service.IService;
import com.stylefeng.guns.common.plugin.service.IBaseTreeableService;
import com.stylefeng.guns.core.node.ZTreeNode;

import java.util.List;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author 殷佳成
 * @since 2018-09-07
 */
public interface IPositionStatementService extends IBaseTreeableService<PositionStatement> {

}
