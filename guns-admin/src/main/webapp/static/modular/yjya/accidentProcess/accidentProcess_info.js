/**
 * 初始化事故处理措施详情对话框
 */
var AccidentProcessInfoDlg = {
    accidentInfoData : {},
    zTreepositionStatementInstance:null,
    zTreeaccidentTypeInstance:null,
    validateFields: {
        main: {
            validators: {
                notEmpty: {
                    message: '主体不能为空'
                }
            }
        },
        mmotion: {
            validators: {
                notEmpty: {
                    message: '主体动作不能为空'
                }
            }
        },
        object: {
            validators: {
                notEmpty: {
                    message: '客体不能为空'
                }
            }
        },
        typeName: {
            validators: {
                notEmpty: {
                    message: '分组不能为空'
                }
            }
        }

    }
};

/**
 * 清除数据
 */
AccidentProcessInfoDlg.clearData = function() {
    this.accidentInfoData = {};
}

/**
 * 设置对话框中的数据
 *
 * @param key 数据的名称
 * @param val 数据的具体值
 */
AccidentProcessInfoDlg.set = function(key, val) {
    this.accidentInfoData[key] = (typeof val == "undefined") ? $("#" + key).val() : val;
    return this;
}

/**
 * 设置对话框中的数据
 *
 * @param key 数据的名称
 * @param val 数据的具体值
 */
AccidentProcessInfoDlg.get = function(key) {
    return $("#" + key).val();
}

/**
 * 关闭此对话框
 */
AccidentProcessInfoDlg.close = function() {
    parent.layer.close(window.parent.AccidentProcess.layerIndex);
}

/**
 * 收集数据
 */
AccidentProcessInfoDlg.collectData = function() {
    this
    .set('id')
    .set('typeId')
    .set('main')
    .set('mproperty')
    .set('mmotion')
    .set('object')
    .set('oproperty')
        .set('omotion')
        .set('typeName')
    ;
}
/**
 * 验证数据是否为空
 */
AccidentProcessInfoDlg.validate = function () {
    $('#accidentProcessInfoForm').data("bootstrapValidator").resetForm();
    $('#accidentProcessInfoForm').bootstrapValidator('validate');
    return $("#accidentProcessInfoForm").data('bootstrapValidator').isValid();
}


AccidentProcessInfoDlg.showAccidentGroupSelectTree = function () {
    var groupObj = $("#typeName");
    var groupOffset = $("#typeName").offset();
    $("#menuContent").css({
        left: groupOffset.left + "px",
        top: groupOffset.top + groupObj.outerHeight() + "px"
    }).slideDown("fast");

    $("body").bind("mousedown", onBodyDown);
}

AccidentProcessInfoDlg.hideAccidentGroupSelectTree = function () {
    $("#menuContent").fadeOut("fast");
    $("body").unbind("mousedown", onBodyDown);// mousedown当鼠标按下就可以触发，不用弹起
}

function onBodyDown(event) {
    if (!(event.target.id == "menuBtn" || event.target.id == "menuContent" || $(
            event.target).parents("#menuContent").length > 0)) {
        AccidentProcessInfoDlg.hideAccidentGroupSelectTree();
    }
}

/**
 * 提交添加
 */
AccidentProcessInfoDlg.addSubmit = function() {

    this.clearData();
    this.collectData();
    if (!this.validate()) {
        return;
    }

    //提交信息
    var ajax = new $ax(Feng.ctxPath + "/accidentProcess/add", function(data){
        Feng.success("添加成功!");
        window.parent.AccidentProcess.table.refresh();
        AccidentProcessInfoDlg.close();
    },function(data){
        Feng.error("添加失败!" + data.responseJSON.message + "!");
    });
    ajax.set(this.accidentInfoData);
    ajax.start();
}

/**
 * 提交修改
 */
AccidentProcessInfoDlg.editSubmit = function() {

    this.clearData();
    this.collectData();
    if (!this.validate()) {
        return;
    }
    //提交信息
    var ajax = new $ax(Feng.ctxPath + "/accidentProcess/update", function(data){
        Feng.success("修改成功!");
        window.parent.AccidentProcess.table.refresh();
        AccidentProcessInfoDlg.close();
    },function(data){
        Feng.error("修改失败!" + data.responseJSON.message + "!");
    });
    ajax.set(this.accidentInfoData);
    ajax.start();
}

AccidentProcessInfoDlg.onClickAccidentGroup = function (e, treeId, treeNode) {
    $("#typeName").attr("value", AccidentProcessInfoDlg.zTreeaccidentTypeInstance.getSelectedVal());
    $("#typeId").attr("value", treeNode.id);
    $("#menuContent").fadeOut("fast");

}

/**
 * 点击ztree列表的选项时
 *
 * @param e
 * @param treeId
 * @param treeNode
 * @returns
 *
 */
AccidentProcessInfoDlg.onClickPositionStatement = function (e, treeId, treeNode) {
    $("#main").attr("value", AccidentProcessInfoDlg.zTreepositionStatementInstance.getSelectedVal());
    $("#parentTreeDiv").fadeOut("fast");

}


/**
 * 显示tree
 */
AccidentProcessInfoDlg.showPositionStatementSelectTree = function () {
    Feng.showInputTree("main", "parentTreeDiv", 15, 34);
}


$(function() {
    Feng.initValidator("accidentProcessInfoForm", AccidentProcessInfoDlg.validateFields);
    //分组tree
    var accidentTypeztree = new $ZTree("treeDemo", "/accidentType/tree");
    accidentTypeztree.bindOnClick(AccidentProcessInfoDlg.onClickAccidentGroup);
    accidentTypeztree.init();
    AccidentProcessInfoDlg.zTreeaccidentTypeInstance = accidentTypeztree;
    //主体tree
    var positionStatementztree = new $ZTree("parentTree", "/positionStatement/tree");
    positionStatementztree.bindOnClick(AccidentProcessInfoDlg.onClickPositionStatement);
    positionStatementztree.init();
    AccidentProcessInfoDlg.zTreepositionStatementInstance = positionStatementztree;
});
