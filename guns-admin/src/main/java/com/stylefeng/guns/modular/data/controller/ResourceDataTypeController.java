package com.stylefeng.guns.modular.data.controller;

import com.stylefeng.guns.common.constant.cache.Cache;
import com.stylefeng.guns.common.exception.BizExceptionEnum;
import com.stylefeng.guns.common.exception.BussinessException;
import com.stylefeng.guns.common.persistence.model.Indication;
import com.stylefeng.guns.common.persistence.model.ResourceDataType;
import com.stylefeng.guns.core.base.controller.BaseController;
import com.stylefeng.guns.core.cache.CacheKit;
import com.stylefeng.guns.core.log.LogObjectHolder;
import com.stylefeng.guns.modular.data.dao.ResourceDataTypeDao;
import com.stylefeng.guns.modular.data.service.IResourceDataTypeService;
import com.stylefeng.guns.modular.data.warpper.ResourceDataTypeWrapper;
import com.stylefeng.guns.modular.indication.service.IIndicationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.validation.Valid;
import java.util.List;
import java.util.Map;

/**
 * 源数据类型控制器
 *
 * @author xuziyang
 * @Date 2018-01-15 17:48:02
 */
@Controller
@RequestMapping("/resourceDataType")
public class ResourceDataTypeController extends BaseController {

    private String PREFIX = "/data/resourceDataType/";

    @Autowired
    private IResourceDataTypeService resourceDataTypeService;

    @Autowired
    private IIndicationService indicationService;

    @Autowired
    private ResourceDataTypeDao resourceDataTypeDao;

    /**
     * 跳转到源数据类型首页
     */
    @RequestMapping("")
    public String index() {
        return PREFIX + "resourceDataType.html";
    }

    /**
     * 跳转到添加源数据类型
     */
    @RequestMapping("/resourceDataType_add")
    public String resourceDataTypeAdd(@RequestParam(value = "indicationId",required = false)Integer indicationId,Map map) {
        if (indicationId != null) {
            map.put("indication", indicationService.selectById(indicationId));
        }
        return PREFIX + "resourceDataType_add.html";
    }

    /**
     * 跳转到修改源数据类型
     */
    @RequestMapping("/resourceDataType_update/{resourceDataTypeId}")
    public String resourceDataTypeUpdate(@PathVariable Integer resourceDataTypeId, Model model) {
        ResourceDataType resourceDataType = resourceDataTypeService.selectById(resourceDataTypeId);
        model.addAttribute("item",resourceDataType);
        String indicationName = "";
        Indication indication = indicationService.selectById(resourceDataType.getIndicationId());
        indicationName = indication.getName();
        model.addAttribute("indicationName", indicationName);
        LogObjectHolder.me().set(resourceDataType);
        return PREFIX + "resourceDataType_edit.html";
    }

    /**
     * 获取源数据类型列表
     */
    @RequestMapping(value = "/list")
    @ResponseBody
    public Object list(@RequestParam(name = "indicationId",required = false) String indicationId,
                       @RequestParam(name = "name",required = false) String name) {
        List<Map<String,Object>> list = resourceDataTypeDao.selectResourceDataTypes(name,indicationId);
        return new ResourceDataTypeWrapper(list).warp();
    }

    /**
     * 新增源数据类型
     */
    @RequestMapping(value = "/add")
    @ResponseBody
    public Object add(@Valid ResourceDataType resourceDataType,BindingResult result ) {
        if (result.hasErrors()) {
            throw new BussinessException(BizExceptionEnum.REQUEST_NULL);
        }

        //判断是否存在该名称
        ResourceDataType resourceDataType1 = resourceDataTypeDao.getByAccount(resourceDataType.getName());
        if(resourceDataType1!=null){
            throw new BussinessException(BizExceptionEnum.EXISTED_THE_NAME);
        }
        resourceDataTypeService.insert(resourceDataType);
        return super.SUCCESS_TIP;
    }

    /**
     * 删除源数据类型
     */
    @RequestMapping(value = "/delete")
    @ResponseBody
    public Object delete(@RequestParam Integer resourceDataTypeId) {
        resourceDataTypeService.deleteById(resourceDataTypeId);
        CacheKit.removeAll(Cache.CONSTANT);
        return SUCCESS_TIP;
    }

    /**
     * 修改源数据类型
     */
    @RequestMapping(value = "/update")
    @ResponseBody
    public Object update(ResourceDataType resourceDataType) {
        resourceDataTypeService.updateById(resourceDataType);
        CacheKit.removeAll(Cache.CONSTANT);
        return super.SUCCESS_TIP;
    }

    /**
     * 源数据类型详情
     */
    @RequestMapping(value = "/detail/{resourceDataTypeId}")
    @ResponseBody
    public Object detail(@PathVariable("resourceDataTypeId") Integer resourceDataTypeId) {
        return resourceDataTypeService.selectById(resourceDataTypeId);
    }

    @RequestMapping(value = "tree")
    @ResponseBody
    public Object tree() {
        return resourceDataTypeService.tree();
    }
}
