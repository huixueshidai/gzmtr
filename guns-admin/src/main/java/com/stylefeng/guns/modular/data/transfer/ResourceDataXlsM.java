package com.stylefeng.guns.modular.data.transfer;

import cn.afterturn.easypoi.excel.annotation.Excel;
import cn.afterturn.easypoi.handler.inter.IExcelModel;
import lombok.*;
import javax.validation.constraints.NotNull;
import java.util.Date;


@Getter
@Setter
@ToString
@NoArgsConstructor
@Data
public class ResourceDataXlsM implements IExcelModel {

    private String errorMsg;

    @Excel(name = "源数据", width = 40)
    @NotNull(message = "源数据不能为空")
    private String resourceDataTypeName;

    @Excel(name = "值" ,width = 30)
    @NotNull(message = "值不能为空")
    private String value;

    @Excel(name = "开始时间",width = 25,format = "yyyy/MM/dd HH:mm:ss")
    @NotNull(message = "开始时间不能为空")
    private Date startTime;

    @Excel(name = "结束时间",width = 25,format = "yyyy/MM/dd HH:mm:ss")
    @NotNull(message = "结束时间不能为空")
    private Date endTime;

    @Excel(name = "所属车站、线路或区域中心",width = 22)
    @NotNull(message = "所属车站、线路或区域中心不能为空")
    private String roadnet;

    @Excel(name = "进站口",width = 10)
    private String entrance;

    @Excel(name = "说明",width = 20)
    private String instruction;

    public ResourceDataXlsM(String resourceDataTypeName, String instruction) {
        this.resourceDataTypeName = resourceDataTypeName;
        this.instruction = instruction;
    }

}

