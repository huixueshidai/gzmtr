/**
 * 初始化详情对话框
 */
var CounterplanInfoDlg = {
    counterplanInfoData: {}
    , ue: null
    , processIds: null
    ,contactSel:0
    , validateFields:{
        name: {
            validators: {
                notEmpty: {
                    message: '预案名称不能为空'
                }
            }
        }
        ,type:{
            validators: {
                notEmpty: {
                    message: '类型不能为空'
                }
            }
        }
        ,accidentTypeName:{
            validators: {
                notEmpty: {
                    message: '事故类型不能为空'
                }
            }
        }
        ,stationSel:{
            validators: {
                notEmpty: {
                    message: '地点不能为空'
                }
            }
        }
        ,level:{
            validators: {
                notEmpty: {
                    message: '响应等级不能为空'
                }
            }
        }
    }
};

/**
 * 清除数据
 */
CounterplanInfoDlg.clearData = function () {
    this.counterplanInfoData = {};
}

/**
 * 设置对话框中的数据
 *
 * @param key 数据的名称
 * @param val 数据的具体值
 */
CounterplanInfoDlg.set = function (key, val) {
    this.counterplanInfoData[key] = (typeof val == "undefined") ? $("#" + key).val() : val;
    return this;
}

/**
 * 设置对话框中的数据
 *
 * @param key 数据的名称
 * @param val 数据的具体值
 */
CounterplanInfoDlg.get = function (key) {
    return $("#" + key).val();
}

/**
 * 关闭此对话框
 */
CounterplanInfoDlg.close = function () {
    parent.layer.close(window.parent.Counterplan.layerIndex);
}
/**
 * 关闭添加预案对话框
 */
CounterplanInfoDlg.closeAddLayer = function () {
    parent.layer.close(window.parent.Counterplan.layerIndex);
}

/**
 * 收集数据
 */
CounterplanInfoDlg.collectData = function () {
    this
        .set('id')
        .set('name')
        .set('type')
        .set('level')
        .set('content',this.ue.getContent())
        .set('accidentTypeId')
        .set('stationId')
        .set('status')
        .set('processIds',this.processIds)
        .set('accidentMain')
    ;
}


/**
 * 提交添加
 */
CounterplanInfoDlg.addSubmit = function () {

    this.clearData();
    this.collectData();


    //提交信息
    var ajax = new $ax(Feng.ctxPath + "/counterplan/add", function (data) {
        Feng.success("添加成功!");
        CounterplanInfoDlg.close();
    }, function (data) {
        Feng.error("添加失败!" + data.responseJSON.message + "!");
    });
    ajax.set(this.counterplanInfoData);
    ajax.start();

}

/**
 * 插入通讯录
 */
CounterplanInfoDlg.contactSel = function () {
    var index = layer.open({
        type: 2,
        title: '通讯录',
        area: ['900px', '550px'], //宽高
        fix: false, //不固定
        maxmin: false,
        content: Feng.ctxPath + '/contact/contact_sel_admin'
    });
    this.layerIndex = index;
};

CounterplanInfoDlg.processSel = function () {

    var index = layer.open({
        type: 2,
        title: '处置流程',
        area: ['1200px', '550px'], //宽高
        fix: false, //不固定
        maxmin: false,
        content: Feng.ctxPath + '/counterplan/editprocess_sel'
    });
    this.layerIndex = index;
    layer.full(index);
};

CounterplanInfoDlg.lawSel = function () {
    var index = layer.open({
        type: 2,
        title: '法律法规',
        area: ['800px', '450px'], //宽高
        fix: false, //不固定
        maxmin: false,
        content: Feng.ctxPath + '/law/law_sel_admin'
    });
    this.layerIndex = index;
}

CounterplanInfoDlg.resourceSel = function (){
    var index = layer.open({
        type: 2,
        title: '应急资源',
        area: ['1200px', '450px'], //宽高
        fix: false, //不固定
        maxmin: false,
        content: Feng.ctxPath + '/resource/resource_sel'
    });
    this.layerIndex = index;
}


/**
 * 提交修改
 */
CounterplanInfoDlg.editSubmit = function () {
    this.clearData();
    this.collectData();
    //提交信息
    var ajax = new $ax(Feng.ctxPath + "/counterplan/update", function (data) {
        Feng.success("修改成功!");
        window.parent.Counterplan.table.refresh();
        CounterplanInfoDlg.close();
    }, function (data) {
        Feng.error("修改失败!" + data.responseJSON.message + "!");
    });
    ajax.set(this.counterplanInfoData);
    ajax.start();

}

$(function () {
    CounterplanInfoDlg.ue = UE.getEditor('content', {
        autoHeightEnabled: false
        , initialFrameHeight: 300
        ,elementPathEnabled:false//不显示输入框下的路径信息
    });
});
