package com.stylefeng.guns.modular.indication.service.impl;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.mapper.Wrapper;
import com.stylefeng.guns.common.exception.BizExceptionEnum;
import com.stylefeng.guns.common.exception.BussinessException;
import com.stylefeng.guns.common.persistence.dao.IndicationMapper;
import com.stylefeng.guns.common.persistence.model.Indication;
import com.stylefeng.guns.common.plugin.service.impl.BaseTreeableServiceImpl;
import com.stylefeng.guns.modular.indication.service.IIndicationService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * <p>
 * 评价指标体系 服务实现类
 * </p>
 *
 * @author xuziyang
 * @since 2018-01-08
 */
@Service
public class IndicationServiceImpl extends BaseTreeableServiceImpl<IndicationMapper, Indication> implements IIndicationService {

    @Override
    public Integer addNode(Indication indication) {
        setPidsAndLevel(indication);
        return super.baseMapper.insert(indication);
    }

    @Override
    public void updateNode(Indication indication) {
        Indication old = super.selectById(indication.getId());
        //pid不变
        if (indication.getPid().equals(old.getPid())) {
            indication.updateById();
            return;
        }
        //pid是其本身
        if (indication.getPid().equals(indication.getId())) {
            throw new BussinessException(BizExceptionEnum.PARENT_IS_SELF_OR_CHILD);
        }

        //pid为其子节点
        Indication newParent = super.selectById(indication.getPid());
        if (newParent.getPids().startsWith(old.makeSelfAsNewParentIds())) {
            throw new BussinessException(BizExceptionEnum.PARENT_IS_SELF_OR_CHILD);
        }

        //设置父节点们
        setPidsAndLevel(indication);
        indication.updateById();

        //修改其子节点的pids
        Wrapper<Indication> wrapper = new EntityWrapper<>();
        wrapper.like("pids", old.makeSelfAsNewParentIds() + "%");
        List<Indication> children = this.selectList(wrapper);
        for (Indication child : children) {
            setPidsAndLevel(child,indication);
            child.updateById();
        }
    }

    public void setPidsAndLevel(Indication indication,Indication parent) {
        indication.setPids(parent.makeSelfAsNewParentIds());
        indication.setLevel(parent.getLevel() + 1);
    }

    public void setPidsAndLevel(Indication indication) {
        if (0 == indication.getPid() || indication.getPid() == null) {
            indication.setPids("0" + indication.getSeparator());
            indication.setLevel(1);
        }else {
            Indication parent = super.baseMapper.selectById(indication.getPid());
            setPidsAndLevel(indication, parent);
        }
    }

    @Override
    public List<Indication> selectByPid(Integer pid) {
        EntityWrapper<Indication> ew = new EntityWrapper<>();
        ew.where("pid = {0}", pid);
        return selectList(ew);
    }

    @Override
    @Transactional
    public void updateWeight(List<Indication> indications) {
        this.updateBatchById(indications);
    }
}
