package com.stylefeng.guns.modular.prediction.util;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.util.Arrays;
import java.util.List;

/**
 * MultiFactorForecasting Tester.
 *
 * @author <Authors name>
 * @since <pre>八月 28, 2018</pre>
 * @version 1.0
 */
public class MultiFactorForecastingTest {

	@Before
	public void before() throws Exception {
	}

	@After
	public void after() throws Exception {
	}

	/**
	 *
	 * Method: forecast()
	 *
	 */
	@Test
	public void testForecast() {
		List<Integer> factorActualList1 = Arrays.asList(8, 10, 11, 13, 14, 15);
		List<Integer> factorActualList2 = Arrays.asList(2,2,3,3,4,5);
		List<Integer> factorActualList3 = Arrays.asList(9,10,10,11,11,12);
		List<Integer> factorActualList4 = Arrays.asList(62, 62, 63, 64, 65, 65);

		List<Integer> accidentActualList = Arrays.asList(4, 4, 4, 4, 5, 6);

		List<List<Integer>> factorsActualList = Arrays
				.asList(factorActualList1, factorActualList2, factorActualList3, factorActualList4);

		//MultiFactorForecasting.calculateCoefficient(accidentActualList, factorsActualList);

	}


} 
