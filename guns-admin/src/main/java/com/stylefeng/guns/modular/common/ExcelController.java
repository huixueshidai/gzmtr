package com.stylefeng.guns.modular.common;

import org.apache.poi.util.IOUtils;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;

@Controller
@RequestMapping("excel")
public class ExcelController {

	@RequestMapping("failFile")
	public void failFileDownload(@RequestParam("fileName") String fileName, HttpServletResponse response){
		FileInputStream fis = null;
		try {
			String filePath = System.getProperty("java.io.tmpdir") + fileName;
			fis = new FileInputStream(filePath);
			response.setHeader("Content-Disposition", "attachment; filename="+fileName);
			IOUtils.copy(fis,response.getOutputStream());
			response.flushBuffer();

			delFile(filePath);
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}finally {
			if (fis != null) {
				try {
					fis.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
	}

	private void delFile(String path){
		File file = new File(path);
		file.delete();
	}


}
