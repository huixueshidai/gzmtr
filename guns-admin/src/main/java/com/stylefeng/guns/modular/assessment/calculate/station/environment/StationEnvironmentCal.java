package com.stylefeng.guns.modular.assessment.calculate.station.environment;

import com.stylefeng.guns.core.util.SpringContextHolder;
import com.stylefeng.guns.modular.assessment.calculate.SecondIndicationCalculator;
import com.stylefeng.guns.modular.assessment.calculate.ThirdIndicationCalculator;
import com.stylefeng.guns.modular.assessment.calculate.aware.StationSecondIndicationAware;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import java.util.Arrays;
import java.util.List;

@Component
@Scope(ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public class StationEnvironmentCal extends SecondIndicationCalculator implements StationSecondIndicationAware {

    private static final Integer secondIndicationId = 19;

    private List<ThirdIndicationCalculator> thirdIndicationCalculatorList = null;

    public StationEnvironmentCal() {
        thirdIndicationCalculatorList = Arrays.asList(
                SpringContextHolder.getBean(StationTemperatureCal.class),
                SpringContextHolder.getBean(StationHumidityCal.class),
                SpringContextHolder.getBean(StationNoiseCal.class),
                SpringContextHolder.getBean(StationAirCal.class),
                SpringContextHolder.getBean(StationRainstormCal.class),
                SpringContextHolder.getBean(StationThunderstormCal.class),
                SpringContextHolder.getBean(StationTyphoonCal.class),
                SpringContextHolder.getBean(StationGuidCal.class),
                SpringContextHolder.getBean(StationProtectCal.class)
        );
    }


    @Override
    public List<ThirdIndicationCalculator> getThirdIndicationCalculatorList() {
        return thirdIndicationCalculatorList;
    }

    @Override
    protected Integer getIndicationId() {
        return secondIndicationId;
    }
}

