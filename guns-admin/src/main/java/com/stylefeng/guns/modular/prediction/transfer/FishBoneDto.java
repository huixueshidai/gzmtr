package com.stylefeng.guns.modular.prediction.transfer;

import com.alibaba.fastjson.annotation.JSONField;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class FishBoneDto {

    @JSONField(serialize = false)
    private Integer id;

    private String text;

    private List<FishBoneDto> causes;

    private Integer size;

    private String weight;
}
