package com.stylefeng.guns.common.persistence.dao;

import com.stylefeng.guns.common.persistence.model.AccidentEmergencyproperty;
import com.baomidou.mybatisplus.mapper.BaseMapper;

/**
 * <p>
  *  Mapper 接口
 * </p>
 *
 * @author wangshuaikang
 * @since 2018-05-29
 */
public interface AccidentEmergencypropertyMapper extends BaseMapper<AccidentEmergencyproperty> {

}