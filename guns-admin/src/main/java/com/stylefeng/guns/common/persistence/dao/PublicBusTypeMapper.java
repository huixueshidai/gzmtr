package com.stylefeng.guns.common.persistence.dao;

import com.stylefeng.guns.common.persistence.model.PublicBusType;
import com.baomidou.mybatisplus.mapper.BaseMapper;

/**
 * <p>
  * 车辆类型 Mapper 接口
 * </p>
 *
 * @author yinjc
 * @since 2018-03-21
 */
public interface PublicBusTypeMapper extends BaseMapper<PublicBusType> {

}