/**
 * 初始化事故情景路径演练详情对话框
 */
var PathEvolutionInfoDlg = {
    pathEvolutionInfoData : {}
};

/**
 * 清除数据
 */
PathEvolutionInfoDlg.clearData = function() {
    this.pathEvolutionInfoData = {};
}

/**
 * 设置对话框中的数据
 *
 * @param key 数据的名称
 * @param val 数据的具体值
 */
PathEvolutionInfoDlg.set = function(key, val) {
    this.pathEvolutionInfoData[key] = (typeof val == "undefined") ? $("#" + key).val() : val;
    return this;
}

/**
 * 设置对话框中的数据
 *
 * @param key 数据的名称
 * @param val 数据的具体值
 */
PathEvolutionInfoDlg.get = function(key) {
    return $("#" + key).val();
}

/**
 * 关闭此对话框
 */
PathEvolutionInfoDlg.close = function() {
    parent.layer.close(window.parent.PathEvolution.layerIndex);
}

/**
 * 收集数据
 */
PathEvolutionInfoDlg.collectData = function() {
    this
    .set('id')
    .set('field')
    .set('rescuers')
    .set('trapped')
    .set('material')
    .set('FireSourceState')
        .set('dischargeRate')
    ;
}

/**
 * 提交添加
 */
PathEvolutionInfoDlg.addSubmit = function() {

    this.clearData();
    this.collectData();

    //提交信息
    var ajax = new $ax(Feng.ctxPath + "/pathEvolution/add", function(data){
       // Feng.success("添加成功!");
       /* Feng.alert("最有利演化路径 火熄灭并且人员救出的概率 5.11×10-4" +
            "最不利演化路径 经过三次情节演变最不利路径概率为 4.35×10-4")*/
        // Feng.tablerow("最有利演化路径 火熄灭并且人员救出的概率 5.11×10-4" +
        //     "最不利演化路径 经过三次情节演变最不利路径概率为 4.35×10-4");
        window.parent.PathEvolution.table.refresh();
        PathEvolutionInfoDlg.close();
    },function(data){
        //Feng.error("添加失败!" + data.responseJSON.message + "!");
    });
    ajax.set(this.pathEvolutionInfoData);
    ajax.start();
}

/**
 * 提交修改
 */
PathEvolutionInfoDlg.editSubmit = function() {

    this.clearData();
    this.collectData();

    //提交信息
    var ajax = new $ax(Feng.ctxPath + "/pathEvolution/update", function(data){
        Feng.success("修改成功!");
        window.parent.PathEvolution.table.refresh();
        PathEvolutionInfoDlg.close();
    },function(data){
        Feng.error("修改失败!" + data.responseJSON.message + "!");
    });
    ajax.set(this.pathEvolutionInfoData);
    ajax.start();
}

$(function() {

});
