package com.stylefeng.guns.common.persistence.model;

import java.io.Serializable;

import com.baomidou.mybatisplus.enums.IdType;
import java.util.Date;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableName;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.springframework.format.annotation.DateTimeFormat;

/**
 * <p>
 * 事故实例
 * </p>
 *
 * @author xuziyang
 * @since 2018-01-22
 */
@TableName("gzmtr_accident")
@Getter
@Setter
@ToString
public class Accident extends Model<Accident> {

    private static final long serialVersionUID = 1L;

	@TableId(value="id", type= IdType.AUTO)
	private Integer id;

	/**
	 * 事故名称
	 */
	private String name;
    /**
     * 事故类型
     */
	@TableField("accident_type_id")
	private Integer accidentTypeId;
    /**
     * 事故线路
     */
	@TableField("line_id")
	private Integer lineId;
    /**
     * 事故车站
     */
	@TableField("station_id")
	private String stationId;
    /**
     * 车次
     */
	@TableField("train_number")
	private String trainNumber;
    /**
     * 中断时间
     */
	@TableField("interruption_time")
	private Integer interruptionTime;
    /**
     * 影响人数
     */
	@TableField("trap_peo_num")
	private Integer trapPeoNum;
    /**
     * 发生时间
     */
	@TableField("start_time")
	private String startTime;

	/**
	 * 结束时间
	 */
	@TableField("end_time")
	private String endTime;
    /**
     * 事故级别
     */
	@TableField("accident_level")
	private String accidentLevel;
    /**
     * 中断类型
     */
	@TableField("interruption_type")
	private String interruptionType;

	/**
	 * 处置流程
	 */
	private String process;

	/**
	 * 突发事件类型
	 */
	@TableField("emergency_id")
	private Integer emergencyId;

	/**
	 * 突发事件属性
	 */
	@TableField("emergencypro_id")
	private Integer emergencyproId;

	/**
	 * 应急处置完成时处置措施json数据
	 */
	@TableField("process_json")
	private String processJson;

	/**
	 * 添加事故实例时处置措施json数据
	 */
	@TableField("disposal_json")
	private String disposalJson;


	/**
	 * 现场情况
	 */
	@TableField("locale")
	private String locale;

	/**
	 * 增加事故实例方式（1：应急处置增加的；2：手动增加的）
	 */
	@TableField("add_type")
	private Integer addType;


	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Integer getAccidentTypeId() {
		return accidentTypeId;
	}

	public void setAccidentTypeId(Integer accidentTypeId) {
		this.accidentTypeId = accidentTypeId;
	}

	public Integer getLineId() {
		return lineId;
	}

	public void setLineId(Integer lineId) {
		this.lineId = lineId;
	}

	public String getStationId() {
		return stationId;
	}

	public void setStationId(String stationId) {
		this.stationId = stationId;
	}

	public String getTrainNumber() {
		return trainNumber;
	}

	public void setTrainNumber(String trainNumber) {
		this.trainNumber = trainNumber;
	}

	public Integer getInterruptionTime() {
		return interruptionTime;
	}

	public void setInterruptionTime(Integer interruptionTime) {
		this.interruptionTime = interruptionTime;
	}



	public String getAccidentLevel() {
		return accidentLevel;
	}

	public void setAccidentLevel(String accidentLevel) {
		this.accidentLevel = accidentLevel;
	}

	public String getInterruptionType() {
		return interruptionType;
	}

	public void setInterruptionType(String interruptionType) {
		this.interruptionType = interruptionType;
	}


	public String getProcess() {
		return process;
	}

	public void setProcess(String process) {
		this.process = process;
	}

	@Override
	protected Serializable pkVal() {
		return this.id;
	}

}
