package com.stylefeng.guns.modular.prediction.service;

import com.stylefeng.guns.common.persistence.model.RiskFactor;
import com.stylefeng.guns.common.plugin.service.IBaseTreeableService;
import com.stylefeng.guns.modular.prediction.transfer.FishBoneDto;

import java.util.List;

/**
 * <p>
 * 事故致因 服务类
 * </p>
 *
 * @author xuziyang
 * @since 2018-04-11
 */
public interface IRiskFactorService extends IBaseTreeableService<RiskFactor> {

    List<RiskFactor> findByPid(Integer pid);

    List<RiskFactor> analyse(Integer rootId);

    void updateStatusDefault(Integer id);
    
}
