package com.stylefeng.guns.common.persistence.dao;

import com.stylefeng.guns.common.persistence.model.ContactGroup;
import com.baomidou.mybatisplus.mapper.BaseMapper;
import org.apache.ibatis.annotations.Param;

/**
 * <p>
  * 通讯录群组 Mapper 接口
 * </p>
 *
 * @author xuziyang
 * @since 2017-12-26
 */
public interface ContactGroupMapper extends BaseMapper<ContactGroup> {

    Integer selectByName(@Param("name")String  name);
}