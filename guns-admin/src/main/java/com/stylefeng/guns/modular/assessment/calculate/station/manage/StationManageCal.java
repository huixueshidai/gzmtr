package com.stylefeng.guns.modular.assessment.calculate.station.manage;

import com.stylefeng.guns.core.util.SpringContextHolder;
import com.stylefeng.guns.modular.assessment.calculate.SecondIndicationCalculator;
import com.stylefeng.guns.modular.assessment.calculate.ThirdIndicationCalculator;
import com.stylefeng.guns.modular.assessment.calculate.aware.StationSecondIndicationAware;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import java.util.Arrays;
import java.util.List;

/**
 * 车站管理
 */
@Component
@Scope(ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public class StationManageCal extends SecondIndicationCalculator implements StationSecondIndicationAware {

    private List<ThirdIndicationCalculator> thirdIndicationCalculatorList;

    public StationManageCal() {
        thirdIndicationCalculatorList = Arrays.asList(
                //应急疏散能力
                SpringContextHolder.getBean(StationEvacuationCal.class),
                //车站安全管理指数
                SpringContextHolder.getBean(StationSafetyManagementCal.class)
        );
    }

    @Override
    public List<ThirdIndicationCalculator> getThirdIndicationCalculatorList() {
        return thirdIndicationCalculatorList;
    }

    @Override
    protected Integer getIndicationId() {
        return 20;
    }
}
