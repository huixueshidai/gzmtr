/**
 * 初始化源数据详情对话框
 */
var ResourceDataInfoDlg = {
    resourceDataInfoData: {}
};

/**
 * 清除数据
 */
ResourceDataInfoDlg.clearData = function () {
    this.resourceDataInfoData = {};
}

/**
 * 设置对话框中的数据
 *
 * @param key 数据的名称
 * @param val 数据的具体值
 */
ResourceDataInfoDlg.set = function (key, val) {
    this.resourceDataInfoData[key] = (typeof val == "undefined") ? $("#" + key).val() : val;
    return this;
}

/**
 * 设置对话框中的数据
 *
 * @param key 数据的名称
 * @param val 数据的具体值
 */
ResourceDataInfoDlg.get = function (key) {
    return $("#" + key).val();
}

/**
 * 关闭此对话框
 */
ResourceDataInfoDlg.close = function () {
    parent.layer.close(window.parent.ResourceData.layerIndex);
}

/**
 * 收集数据
 */
ResourceDataInfoDlg.collectData = function () {
    this
        .set('id')
        .set('resourceDataTypeId')
        .set('roadnetId')
        .set('value')
        .set('startTime')
        .set('endTime')
        .set('entrance')
    ;
}

/**
 * 提交添加
 */
ResourceDataInfoDlg.addSubmit = function () {

    this.clearData();
    this.collectData();

    //提交信息
    var ajax = new $ax(Feng.ctxPath + "/resourceData/add", function (data) {
        Feng.success("添加成功!");
        window.parent.ResourceData.table.refresh();
        ResourceDataInfoDlg.close();
    }, function (data) {
        Feng.error("添加失败!" + data.responseJSON.message + "!");
    });
    ajax.set(this.resourceDataInfoData);
    ajax.start();
}


/**
 * 提交修改
 */
ResourceDataInfoDlg.editSubmit = function () {

    this.clearData();
    this.collectData();

    //提交信息
    var ajax = new $ax(Feng.ctxPath + "/resourceData/update", function (data) {
        Feng.success("修改成功!");
        window.parent.ResourceData.table.refresh();
        ResourceDataInfoDlg.close();
    }, function (data) {
        Feng.error("修改失败!" + data.responseJSON.message + "!");
    });
    ajax.set(this.resourceDataInfoData);
    ajax.start();
}

ResourceDataInfoDlg.showResourceDataTypeSelectTree = function () {
    Feng.showInputTree("resourceDataTypeName", "resourceDataTypeTreeDiv", 15, 34);
}


ResourceDataInfoDlg.onClickResourceDataType = function (e, treeId, treeNode) {
    if(treeNode.isParent){
        Feng.info("请选择源数据类型");
        return;
    }
    $("#resourceDataTypeName").attr("value", treeNode.name);
    $("#resourceDataTypeId").attr("value", treeNode.id);
    $("#resourceDataTypeTreeDiv").fadeOut("fast");
}

ResourceDataInfoDlg.showRoadnetOrStationSelectTree = function () {
    Feng.showInputTree("roadnetName", "roadnetTreeDiv", 15, 34);
}

ResourceDataInfoDlg.onClickRoadnetOrStation = function (e, treeId, treeNode) {
    $("#roadnetName").attr("value", treeNode.name);
    $("#roadnetId").attr("value", treeNode.id);
    $("#roadnetTreeDiv").fadeOut("fast");
}

$(function () {
    var resourceDataTypeTree = new $ZTree("resourceDataTypeTree", "/resourceDataType/tree");
    resourceDataTypeTree.bindOnClick(ResourceDataInfoDlg.onClickResourceDataType);
    resourceDataTypeTree.init();

    var roadnetTree = new $ZTree("roadnetTree", "/roadnet/tree");
    roadnetTree.bindOnClick(ResourceDataInfoDlg.onClickRoadnetOrStation);
    roadnetTree.init();

    laydate.render({
        elem: '#startTime'
        , type: 'datetime'
    });
    laydate.render({
        elem: '#endTime'
        , type: 'datetime'
    });
});
