package com.stylefeng.guns.common.persistence.model;

import com.baomidou.mybatisplus.annotations.TableName;
import com.stylefeng.guns.common.plugin.entity.BaseModel;
import com.stylefeng.guns.common.plugin.entity.Treeable;
import lombok.ToString;

/**
 * <p>
 * 通讯录群组
 * </p>
 *
 * @author xuziyang
 * @since 2017-12-26
 */
@TableName("gzmtr_contact_group")
@ToString
public class ContactGroup extends BaseModel<ContactGroup> implements Treeable {

    private static final long serialVersionUID = 1L;

    /**
     * 排序
     */
	private Integer num;
    /**
     * 父级id
     */
	private Integer pid;
    /**
     * 父级ids
     */
	private String pids;
    /**
     * 名称
     */
	private String name;



	public Integer getNum() {
		return num;
	}

	public void setNum(Integer num) {
		this.num = num;
	}

	public Integer getPid() {
		return pid;
	}

	public void setPid(Integer pid) {
		this.pid = pid;
	}

	public String getPids() {
		return pids;
	}

	public void setPids(String pids) {
		this.pids = pids;
	}

    @Override
    public String makeSelfAsNewParentIds() {
        return getPids() + getId() + getSeparator();
    }

    @Override
    public String getSeparator() {
        return "/";
    }

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

}
