package com.stylefeng.guns.modular.data.warpper;

import com.stylefeng.guns.common.constant.factory.ConstantFactory;
import com.stylefeng.guns.common.constant.factory.IConstantFactory;
import com.stylefeng.guns.core.base.warpper.BaseControllerWarpper;

import java.util.Map;

public class ResourceDataTypeWrapper extends BaseControllerWarpper {

    public ResourceDataTypeWrapper(Object obj) {
        super(obj);
    }

    @Override
    protected void warpTheMap(Map<String, Object> map) {
        map.put("isFixed", ConstantFactory.me().getDictNameByCode("yes_or_no", (String) map.get("isFixed")));
    }
}
