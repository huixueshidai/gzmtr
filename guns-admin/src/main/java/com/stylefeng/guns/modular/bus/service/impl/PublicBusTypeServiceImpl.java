package com.stylefeng.guns.modular.bus.service.impl;

import com.stylefeng.guns.common.persistence.model.PublicBusType;
import com.stylefeng.guns.common.persistence.dao.PublicBusTypeMapper;
import com.stylefeng.guns.modular.bus.service.IPublicBusTypeService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 车辆类型 服务实现类
 * </p>
 *
 * @author yinjc
 * @since 2018-03-21
 */
@Service
public class PublicBusTypeServiceImpl extends ServiceImpl<PublicBusTypeMapper, PublicBusType> implements IPublicBusTypeService {
	
}
