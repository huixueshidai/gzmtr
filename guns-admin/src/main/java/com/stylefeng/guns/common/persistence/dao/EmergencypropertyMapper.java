package com.stylefeng.guns.common.persistence.dao;

import com.stylefeng.guns.common.persistence.model.Emergencyproperty;
import com.baomidou.mybatisplus.mapper.BaseMapper;
import org.apache.ibatis.annotations.Param;

/**
 * <p>
  *  Mapper 接口
 * </p>
 *
 * @author wangshuaikang
 * @since 2018-05-24
 */
public interface EmergencypropertyMapper extends BaseMapper<Emergencyproperty> {

    Emergencyproperty selectByemergency(@Param("emergency")Integer  emergency);
}