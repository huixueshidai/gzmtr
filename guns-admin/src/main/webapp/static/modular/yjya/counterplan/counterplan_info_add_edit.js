/**
 * 初始化详情对话框
 */
var CounterplanInfoDlgEdit = {
    counterplanInfoData: {}
    , ue: null
    , processIds: null
    , accidentTypeTreeInstance:null
    ,stationTreeInstance:null
    ,zTreepositionStatementInstance:null
    ,layerIndex:-1
    ,contactSel:0
    ,checkCount:0  //选取的车站个数
    ,stationdIds:[]
    , validateFields:{
        name: {
            validators: {
                notEmpty: {
                    message: '预案名称不能为空'
                }
            }
        }
        ,type:{
            validators: {
                notEmpty: {
                    message: '类型不能为空'
                }
            }
        }
        ,accidentTypeName:{
            validators: {
                notEmpty: {
                    message: '事故类型不能为空'
                }
            }
        }
        ,stationSel:{
            validators: {
                notEmpty: {
                    message: '地点不能为空'
                }
            }
        }
        ,level:{
            validators: {
                notEmpty: {
                    message: '响应等级不能为空'
                }
            }
        }
    }
};

/**
 * 清除数据
 */
CounterplanInfoDlgEdit.clearData = function () {
    this.counterplanInfoData = {};
}

/**
 * 验证数据是否为空
 */
CounterplanInfoDlgEdit.validate = function () {
    $('#contente').data("bootstrapValidator").resetForm();
    $('#contente').bootstrapValidator('validate');
    return $("#contente").data('bootstrapValidator').isValid();
}

/**
 * 设置对话框中的数据
 *
 * @param key 数据的名称
 * @param val 数据的具体值
 */
CounterplanInfoDlgEdit.set = function (key, val) {
    this.counterplanInfoData[key] = (typeof val == "undefined") ? $("#" + key).val() : val;
    return this;
}

/**
 * 设置对话框中的数据
 *
 * @param key 数据的名称
 * @param val 数据的具体值
 */
CounterplanInfoDlgEdit.get = function (key) {
    return $("#" + key).val();
}

/**
 * 关闭此对话框
 */
CounterplanInfoDlgEdit.close = function () {
    parent.layer.close(window.parent.CounterplanInfoDlgEdit.layerIndex);
}
/**
 * 关闭添加预案对话框
 */
CounterplanInfoDlgEdit.closeAddLayer = function () {
    parent.layer.close(window.parent.CounterplanTemplate.layerIndex);
}

/**
 * 收集数据
 */
CounterplanInfoDlgEdit.collectData = function () {
    this
        .set('id')
        .set('name')
        .set('type')
        .set('level')
        .set('content',this.ue.getContent())
        .set('accidentTypeId')
        .set('stationId')
        .set('status')
        .set('processIds',CounterplanInfoDlgEdit.processIds)
        .set('accidentMain')
    ;
}


/**
 * 提交添加
 */
CounterplanInfoDlgEdit.addSubmit = function () {

    this.clearData();
    this.collectData();
    if (!this.validate()) {
        return;
    }

    //提交信息
    var ajax = new $ax(Feng.ctxPath + "/counterplan/add", function (data) {
        Feng.success("添加成功!");
        CounterplanInfoDlgEdit.close();
    }, function (data) {
        Feng.error("添加失败!" + data.responseJSON.message + "!");
    });
    ajax.set(this.counterplanInfoData);
    ajax.start();

}

/**
 * 插入通讯录
 */
CounterplanInfoDlgEdit.contactSel = function () {
    var index = layer.open({
        type: 2,
        title: '通讯录',
        area: ['900px', '550px'], //宽高
        fix: false, //不固定
        maxmin: false,
        content: Feng.ctxPath + '/contact/contact_sel'
    });
    this.layerIndex = index;

};

CounterplanInfoDlgEdit.processSel = function () {

    var index = layer.open({
        type: 2,
        title: '处置流程',
        area: ['1200px', '550px'], //宽高
        fix: false, //不固定
        maxmin: false,
        content: Feng.ctxPath + '/counterplan/process_sel'
    });
    this.layerIndex = index;
    layer.full(index);
};

CounterplanInfoDlgEdit.lawSel = function () {
    var index = layer.open({
        type: 2,
        title: '法律法规',
        area: ['800px', '450px'], //宽高
        fix: false, //不固定
        maxmin: false,
        content: Feng.ctxPath + '/law/law_sel'
    });
    this.layerIndex = index;
}

CounterplanInfoDlgEdit.resourceSel = function (){
    var index = layer.open({
        type: 2,
        title: '应急资源',
        area: ['1200px', '450px'], //宽高
        fix: false, //不固定
        maxmin: false,
        content: Feng.ctxPath + '/resource/resource_sel'
    });
    this.layerIndex = index;
}


/**
 * 提交修改
 */
CounterplanInfoDlgEdit.editSubmit = function () {
    this.clearData();
    this.collectData();
    //提交信息
    var ajax = new $ax(Feng.ctxPath + "/counterplan/update", function (data) {
        Feng.success("修改成功!");
        window.parent.Counterplan.table.refresh();
        CounterplanInfoDlgEdit.close();
    }, function (data) {
        Feng.error("修改失败!" + data.responseJSON.message + "!");
    });
    ajax.set(this.counterplanInfoData);
    ajax.start();

}

/**
 * 根据类型填充文本编辑器内容
 */
CounterplanInfoDlgEdit.changeType = function(){
    var type = $("#type").val();
    var ajax = new $ax(Feng.ctxPath + "/counterplanTypeTemplate/getByType", function (data) {
        var content = data.content;
        var ue = CounterplanInfoDlgEdit.ue;
        //如果富文本编辑器中无内容
        // if(!ue.hasContents()) {
        //     ue.execCommand('cleardoc');
        ue.setContent(content);
        // }
    }, function (data) {
    });
    ajax.set("type",type);
    ajax.start();
}


/**
 *显示事故类型的tree
 */
CounterplanInfoDlgEdit.showAccidentTypeSelectTree = function(){
    Feng.showInputTree("accidentTypeName", "accidentTypeTreeDiv", 15, 34);
}

CounterplanInfoDlgEdit.AutoMatchAccidentTypeSelectTree = function(txtObj){
    if (txtObj.value.length > 0) {
        var accidentTypeTree = new $ZTree("accidentTypeTree", "/accidentType/tree");
        accidentTypeTree.bindOnClick(CounterplanInfoDlgEdit.onClickAccidentType);
        accidentTypeTree.init();
        accidentTypeTree.init(txtObj);
        CounterplanInfoDlgEdit.accidentTypeTreeInstance = accidentTypeTree;
        Feng.showInputTree("accidentTypeName", "accidentTypeTreeDiv", 15, 34);
    }
    else{
        var accidentTypeTree = new $ZTree("accidentTypeTree", "/accidentType/tree");
        accidentTypeTree.bindOnClick(CounterplanInfoDlgEdit.onClickAccidentType);
        accidentTypeTree.init();
        CounterplanInfoDlgEdit.accidentTypeTreeInstance = accidentTypeTree;
        Feng.showInputTree("accidentTypeName", "accidentTypeTreeDiv", 15, 34);
    }
}

CounterplanInfoDlgEdit.onClickAccidentType = function (e, treeId,treeNode) {
    $("#accidentTypeName").val(CounterplanInfoDlgEdit.accidentTypeTreeInstance.getSelectedVals());
    $("#accidentTypeId").attr("value", treeNode.id);
    $("#accidentTypeTreeDiv").fadeOut("fast");
}

/**
 * 车站树状图展示
 */
CounterplanInfoDlgEdit.showStationSelectTree = function(){
    Feng.showInputTree("stationSel", "stationTreeDiv", 15, 34);
}

CounterplanInfoDlgEdit.AutoMatchStationSelectTree = function(txtObj){
    if (txtObj.value.length > 0) {
        var stationTree = new $ZTree("stationTree", "/roadnet/tree");
        stationTree.bindOnClick(CounterplanInfoDlgEdit.onClickDept);
        stationTree.init();
        stationTree.init(txtObj);
        CounterplanInfoDlgEdit.stationTreeInstance = stationTree;
        Feng.showInputTree("stationSel", "stationTreeDiv", 15, 34);
    }
    else{
        var stationTree = new $ZTree("stationTree", "/roadnet/tree");
        stationTree.bindOnClick(CounterplanInfoDlgEdit.onClickDept);
        stationTree.init();
        CounterplanInfoDlgEdit.stationTreeInstance = stationTree;
        Feng.showInputTree("stationSel", "stationTreeDiv", 15, 34);
    }
}

CounterplanInfoDlgEdit.onClickDept = function (e, treeId, treeNode) {

    if(treeNode.isParent){
        Feng.info("请选择车站");
        treeNode.checked=false;
        return;
    }
    if(treeNode.checked){
        CounterplanInfoDlgEdit.checkCount++;
        if(CounterplanInfoDlgEdit.checkCount==1){
            $("#stationSel").val(treeNode.getPath()[2].name+":"+treeNode.name);
            CounterplanInfoDlgEdit.stationdIds.push(treeNode.id);
        }
        else{
            $("#stationSel").val( $("#stationSel").val()+">>"+treeNode.name);
            CounterplanInfoDlgEdit.stationdIds.push(treeNode.id);
        }
    }
    else{
        CounterplanInfoDlgEdit.checkCount--;
        if(CounterplanInfoDlgEdit.checkCount==0){
            $("#stationSel").val(null);
            CounterplanInfoDlgEdit.removeByValue(CounterplanInfoDlgEdit.stationdIds,treeNode.id.toString())
        }
        if(CounterplanInfoDlgEdit.checkCount==1){
            $("#stationSel").val(CounterplanInfoDlgEdit.stationTreeInstance.getSelectedValCheck(CounterplanInfoDlgEdit.checkCount));
            CounterplanInfoDlgEdit.removeByValue(CounterplanInfoDlgEdit.stationdIds,treeNode.id.toString())
        }
    }
    if( CounterplanInfoDlgEdit.stationdIds.length>0) {
        var stationId = "";
        for (var i = 0; i < CounterplanInfoDlgEdit.stationdIds.length; i++) {
            stationId += CounterplanInfoDlgEdit.stationdIds[i] + "/";
        }
        $("#stationId").attr("value", stationId);
    }
    if( CounterplanInfoDlgEdit.checkCount==2){
        $("#stationTreeDiv").fadeOut("fast");
    }
}

/**
 * 点击ztree列表的选项时(岗位)
 *
 * @param e
 * @param treeId
 * @param treeNode
 * @returns
 *
 */
CounterplanInfoDlgEdit.onClickPositionStatement = function (e, treeId, treeNode) {
    if(treeNode.checked){
        if($("#accidentMain").val()!=""){
            $("#accidentMain").val( $("#accidentMain").val()+","+treeNode.name);
        }
        else{
            $("#accidentMain").val(treeNode.name);
        }
    }
    else{
        var accidentMain = $("#accidentMain").val().split(",");
        if(treeNode.name==accidentMain[accidentMain.length-1]){
            $("#accidentMain").val($("#accidentMain").val().replace(treeNode.name,""));
        }
        else{
            $("#accidentMain").val($("#accidentMain").val().replace(treeNode.name+",",""));
        }

    }
}

/**
 * 显示tree
 */
CounterplanInfoDlgEdit.showPositionStatementSelectTree = function () {
    Feng.showInputTree("main", "parentTreeDiv", 15, 34);
}


/**
 * 匹配预案模板
 */
CounterplanInfoDlgEdit.counterplanTemplateMate = function(){
    var type = $("#type").val();
    var accidentTypeId = $("#accidentTypeId").val();
    var level = $("#level").val();
    var index = layer.open({
        type: 2,
        title: '预案模板选择',
        area: ['1100px', '700px'], //宽高
        fix: false, //不固定
        maxmin: false,
        content: Feng.ctxPath + '/counterplanTemplate/counterplan_template_mate_index?type='+type+'&level='+level+'&accidentTypeId='+accidentTypeId
    });
    this.layerIndex = index;
    layer.full(index);
}


$(function () {
    CounterplanInfoDlgEdit.ue = UE.getEditor('content', {
        autoHeightEnabled: false
        , initialFrameHeight: 340
        ,elementPathEnabled:false//不显示输入框下的路径信息
    });

    var accidentTypeTree = new $ZTree("accidentTypeTree", "/accidentType/tree");
    accidentTypeTree.bindOnClick(CounterplanInfoDlgEdit.onClickAccidentType);
    accidentTypeTree.init();
    CounterplanInfoDlgEdit.accidentTypeTreeInstance = accidentTypeTree;

    var stationTree = new $ZTree("stationTree", "/roadnet/tree");
    stationTree.setSettings({
        view : {
            dblClickExpand : true,
            selectedMulti : false
        },
        data : {simpleData : {enable : true}},
        callback : {
            onClick : this.onClick,
            onDblClick:this.ondblclick
        },
        check: {
            enable: true,
            chkStyle: "checkbox",
            chkboxType: { "Y": "", "N": "" }
        },
        callback : {
            onCheck:CounterplanInfoDlgEdit.onClickDept
        }
    });
    stationTree.Check(CounterplanInfoDlgEdit.onClickDept);
    stationTree.init();
    CounterplanInfoDlgEdit.stationTreeInstance = stationTree;

//岗位tree
    var positionStatementztree = new $ZTree("parentTree", "/positionStatement/treeMain?accidentMain="+$("#accidentMain").val());
    positionStatementztree.setSettings({
        view : {
            dblClickExpand : true,
            selectedMulti : false
        },
        data : {simpleData : {enable : true}},
        callback : {
            onClick : this.onClick,
            onDblClick:this.ondblclick
        },
        check: {
            enable: true,
            chkStyle: "checkbox",
            chkboxType: { "Y": "", "N": "" }
        },
        callback : {
            onCheck:CounterplanInfoDlgEdit.onClickPositionStatement
        }
    });
    positionStatementztree.Check(CounterplanInfoDlgEdit.onClickPositionStatement);
    positionStatementztree.init();
    CounterplanInfoDlgEdit.zTreepositionStatementInstance = positionStatementztree;
    Feng.initValidator("contente", CounterplanInfoDlgEdit.validateFields);
});
