/**
 * 应急资源管理初始化
 */
var Resource = {
    id: "ResourceTable",	//表格id
    seItem: null,		//选中的条目
    table: null,
    layerIndex: -1,
    uploadInstance:null
};

/**
 * 初始化表格的列
 */
Resource.initColumn = function () {
    return [
        {field: 'selectItem', radio: true},
            {title: 'id', field: 'id', visible: true, align: 'center', valign: 'middle'},
            {title: '名称', field: 'name', visible: true, align: 'center', valign: 'middle'},
            {title: '是否是大型物资', field: 'largeScale', visible: true, align: 'center', valign: 'middle'},
            {title: '型号', field: 'modelNum', visible: true, align: 'center', valign: 'middle'},
            {title: '专业用途', field: 'fist', visible: true, align: 'center', valign: 'middle'},
            {title: '现有数量', field: 'num', visible: true, align: 'center', valign: 'middle'},
            {title: '单位', field: 'unit', visible: true, align: 'center', valign: 'middle'},
            {title: '线别', field: 'line', visible: true, align: 'center', valign: 'middle'},
            {title: '放置地点', field: 'location', visible: true, align: 'center', valign: 'middle'},
            {title: '地点备注', field: 'remark', visible: true, align: 'center', valign: 'middle'},
            {title: '中心', field: 'center', visible: true, align: 'center', valign: 'middle'},
            {title: '使用部门', field: 'usedp', visible: true, align: 'center', valign: 'middle'},
            {title: '维护部门', field: 'maintaindp', visible: true, align: 'center', valign: 'middle'},
            {title: '更新时间', field: 'updateTime', visible: true, align: 'center', valign: 'middle'},
            {title: '状态', field: 'state', visible: true, align: 'center', valign: 'middle'}
    ];
};

/**
 * 检查是否选中
 */
Resource.check = function () {
    var selected = $('#' + this.id).bootstrapTable('getSelections');
    if(selected.length == 0){
        Feng.info("请先选中表格中的某一记录！");
        return false;
    }else{
        Resource.seItem = selected[0];
        return true;
    }
};

/**
 * 点击添加应急资源
 */
Resource.openAddResource = function () {
    var index = layer.open({
        type: 2,
        title: '添加应急资源',
        area: ['800px', '420px'], //宽高
        fix: false, //不固定
        maxmin: true,
        content: Feng.ctxPath + '/resource/resource_add'
    });
    this.layerIndex = index;
    layer.full(index);
};

/**
 * 打开查看应急资源详情
 */
Resource.openResourceDetail = function () {
    if (this.check()) {
        var index = layer.open({
            type: 2,
            title: '应急资源详情',
            area: ['800px', '420px'], //宽高
            fix: false, //不固定
            maxmin: true,
            content: Feng.ctxPath + '/resource/resource_update/' + Resource.seItem.id
        });
        this.layerIndex = index;
        layer.full(index);
    }
};

/**
 * 删除应急资源
 */
Resource.delete = function () {
    if (this.check()) {
        var ajax = new $ax(Feng.ctxPath + "/resource/delete", function (data) {
            Feng.success("删除成功!");
            Resource.table.refresh();
        }, function (data) {
            Feng.error("删除失败!" + data.responseJSON.message + "!");
        });
        ajax.set("resourceId",this.seItem.id);
        ajax.start();
    }
};
Resource.exportXlsByM = function () {
    window.location.href = Feng.ctxPath + "/resource/exportXlsByM";
}

Resource.upload = function () {
    this.uploadInstance
}
/**
 * 重置
 */
Resource.resetSearch = function(){
    $("#condition").val("");
    $("#location").val("");


}

/**
 * 查询应急资源列表
 */
Resource.search = function () {
    var queryData = {};
    queryData['condition'] = $("#condition").val();
    queryData['location'] = $("#location").val();
    Resource.table.refresh({query: queryData});
};

$(function () {
    var defaultColunms = Resource.initColumn();
    var table = new BSTable(Resource.id, "/resource/list", defaultColunms);
    table.setPaginationType("client");
    Resource.table = table.init();

    var upload = new $WebUpload({
        uploadId:"upload",
        mimeTypes:"application/vnd.ms-excel",
        extensions:"xls",
        url:"/resource/importExcel",
        multiple:true
    });
    upload.init();
    ResourceData.uploadInstance = upload;
});
