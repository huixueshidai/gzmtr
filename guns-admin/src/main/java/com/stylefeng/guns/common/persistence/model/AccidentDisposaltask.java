package com.stylefeng.guns.common.persistence.model;

import java.io.Serializable;

import com.baomidou.mybatisplus.enums.IdType;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableName;
import java.io.Serializable;

/**
 * <p>
 * 
 * </p>
 *
 * @author wangshuaikang
 * @since 2018-09-11
 */
@TableName("gzmtr_accident_disposaltask")
public class AccidentDisposaltask extends Model<AccidentDisposaltask> {

    private static final long serialVersionUID = 1L;

	@TableId(value="id", type= IdType.AUTO)
	private Integer id;
    /**
     * 事故实例id
     */
	@TableField("accident_id")
	private Integer accidentId;
    /**
     * 时间
     */
	private String time;
    /**
     * 处置操作
     */
	@TableField("disposal_task")
	private String disposalTask;
    /**
     * 步骤
     */
	private Integer step;


	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getAccidentId() {
		return accidentId;
	}

	public void setAccidentId(Integer accidentId) {
		this.accidentId = accidentId;
	}

	public String getTime() {
		return time;
	}

	public void setTime(String time) {
		this.time = time;
	}

	public String getDisposalTask() {
		return disposalTask;
	}

	public void setDisposalTask(String disposalTask) {
		this.disposalTask = disposalTask;
	}

	public Integer getStep() {
		return step;
	}

	public void setStep(Integer step) {
		this.step = step;
	}

	@Override
	protected Serializable pkVal() {
		return this.id;
	}

	@Override
	public String toString() {
		return "AccidentDisposaltask{" +
			"id=" + id +
			", accidentId=" + accidentId +
			", time=" + time +
			", disposalTask=" + disposalTask +
			", step=" + step +
			"}";
	}
}
