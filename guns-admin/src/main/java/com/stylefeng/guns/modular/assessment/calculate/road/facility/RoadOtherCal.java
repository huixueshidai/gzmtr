package com.stylefeng.guns.modular.assessment.calculate.road.facility;

import com.stylefeng.guns.common.persistence.model.Assessment;
import com.stylefeng.guns.modular.assessment.calculate.ThirdIndicationCalculator;
import com.stylefeng.guns.modular.assessment.calculate.area.facility.AreaQiTaCal;
import com.stylefeng.guns.modular.assessment.calculate.area.facility.util.AreaRoadFacilityCalculatorUtil;
import com.stylefeng.guns.modular.assessment.calculate.aware.RoadnetThirdIndicationAware;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Date;
import java.util.List;

/**
 * 其他系统影响运营风险指数
 *
 * @author yinjc
 * @since 2018/05/11
 */
@Component
public class RoadOtherCal extends ThirdIndicationCalculator implements RoadnetThirdIndicationAware {

    @Autowired
    private AreaQiTaCal areaQiTaCal;

    @Override
    protected Integer getIndicationId() {
        return 105;
    }

    @Override
    public Assessment calculate(Integer roadId, Date beginDate, Date endDate) {
        List<Double> wList = getMulDoubleValue(roadId, 343, beginDate,endDate);
        double resVal = AreaRoadFacilityCalculatorUtil.calculate(roadId, areaQiTaCal, wList, beginDate,endDate);
        return genResult(resVal, roadId, beginDate,endDate);
    }
}
