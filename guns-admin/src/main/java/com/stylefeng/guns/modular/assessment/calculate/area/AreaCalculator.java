package com.stylefeng.guns.modular.assessment.calculate.area;

import com.stylefeng.guns.core.util.SpringContextHolder;
import com.stylefeng.guns.modular.assessment.calculate.FirstIndicationCalculator;
import com.stylefeng.guns.modular.assessment.calculate.SecondIndicationCalculator;
import com.stylefeng.guns.modular.assessment.calculate.area.accident.AreaAccidentCal;
import com.stylefeng.guns.modular.assessment.calculate.area.environment.AreaEnvironmentCal;
import com.stylefeng.guns.modular.assessment.calculate.area.facility.AreaFacilityCal;
import com.stylefeng.guns.modular.assessment.calculate.area.manage.AreaManageCal;
import com.stylefeng.guns.modular.assessment.calculate.area.passenger.AreaPassengerCal;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import java.util.Arrays;
import java.util.List;

@Component
@Scope(ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public class AreaCalculator extends FirstIndicationCalculator {

    private List<SecondIndicationCalculator> secondIndicationCalculates;

    public AreaCalculator() {
        secondIndicationCalculates = Arrays.asList(
                SpringContextHolder.getBean(AreaPassengerCal.class),
                SpringContextHolder.getBean(AreaManageCal.class),
                SpringContextHolder.getBean(AreaFacilityCal.class),
                SpringContextHolder.getBean(AreaEnvironmentCal.class),
                SpringContextHolder.getBean(AreaAccidentCal.class)
        );

    }
    @Override
    public List<SecondIndicationCalculator> getSecondIndicationCalculateList() {
        return secondIndicationCalculates;
    }


    @Override
    protected Integer getIndicationId() {
        return 9;
    }
}
