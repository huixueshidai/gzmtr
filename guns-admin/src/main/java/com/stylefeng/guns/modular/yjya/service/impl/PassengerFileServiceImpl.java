package com.stylefeng.guns.modular.yjya.service.impl;

import com.stylefeng.guns.common.persistence.model.PassengerFile;
import com.stylefeng.guns.common.persistence.dao.PassengerFileMapper;
import com.stylefeng.guns.modular.yjya.service.IPassengerFileService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author wsk
 * @since 2018-09-25
 */
@Service
public class PassengerFileServiceImpl extends ServiceImpl<PassengerFileMapper, PassengerFile> implements IPassengerFileService {
	
}
