package com.stylefeng.guns.modular.prediction.warpper;

import com.stylefeng.guns.common.constant.factory.ConstantFactory;
import com.stylefeng.guns.core.base.warpper.BaseControllerWarpper;
import org.apache.commons.collections.MapUtils;

import java.util.Map;

public class PredictionDataWarpper extends BaseControllerWarpper {

    public PredictionDataWarpper(Object obj) {
        super(obj);
    }

    @Override
    protected void warpTheMap(Map<String, Object> map) {
        map.put("accidentTypeName", ConstantFactory.me().getDictNameByCode("accidentType", MapUtils.getString(map, "accidentType")));
    }
}
