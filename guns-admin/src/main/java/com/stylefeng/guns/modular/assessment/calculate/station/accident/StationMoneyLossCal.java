package com.stylefeng.guns.modular.assessment.calculate.station.accident;


import com.stylefeng.guns.common.persistence.model.Assessment;
import com.stylefeng.guns.core.util.DoubleUtil;
import com.stylefeng.guns.modular.assessment.calculate.ThirdIndicationCalculator;
import com.stylefeng.guns.modular.assessment.calculate.aware.StationThirdIndicationAware;
import org.springframework.stereotype.Component;

import java.util.Date;
import java.util.List;

/**
 * 车站经济损失值
 */
@Component
public class StationMoneyLossCal extends ThirdIndicationCalculator implements StationThirdIndicationAware {

    @Override
    protected Integer getIndicationId() {
        return 48;
    }

    public Assessment calculate(Integer stationId, Date beginDate, Date endDate) {
        List<Double> vals = getMulDoubleValue(stationId, 151, beginDate,endDate);
        Double resVal = vals.stream().reduce(DoubleUtil::add).get();
        return genResult(resVal, stationId, beginDate,endDate);
    }

}
