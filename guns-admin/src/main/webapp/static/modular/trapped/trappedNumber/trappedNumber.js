/**
 * 被困人员录入管理初始化
 */
var TrappedNumber = {
    id: "TrappedNumberTable",	//表格id
    seItem: null,		//选中的条目
    table: null,
    layerIndex: -1
};

/**
 * 初始化表格的列
 */
TrappedNumber.initColumn = function () {
    return [
        {field: 'selectItem', radio: true},
          /*  {title: '', field: 'id', visible: true, align: 'center', valign: 'middle'},*/
            {title: '场地', field: 'field', visible: true, align: 'center', valign: 'middle'},
            {title: '救援人员', field: 'rescuers', visible: true, align: 'center', valign: 'middle'},
            {title: '被困人员', field: 'trapped', visible: true, align: 'center', valign: 'middle'},
            {title: '疏散率A', field: 'discharge1', visible: true, align: 'center', valign: 'middle'},
            {title: '疏散率B', field: 'discharge2', visible: true, align: 'center', valign: 'middle'},
            {title: '疏散率C', field: 'discharge3', visible: true, align: 'center', valign: 'middle'},
            {title: '疏散率E', field: 'discharge4', visible: true, align: 'center', valign: 'middle'},
            {title: '疏散率F', field: 'discharge5', visible: true, align: 'center', valign: 'middle'}
    ];
};

/**
 * 检查是否选中
 */
TrappedNumber.check = function () {
    var selected = $('#' + this.id).bootstrapTable('getSelections');
    if(selected.length == 0){
        Feng.info("请先选中表格中的某一记录！");
        return false;
    }else{
        TrappedNumber.seItem = selected[0];
        return true;
    }
};

/**
 * 点击添加被困人员录入
 */
TrappedNumber.openAddTrappedNumber = function () {
    var index = layer.open({
        type: 2,
        title: '添加被困人员录入',
        area: ['800px', '420px'], //宽高
        fix: false, //不固定
        maxmin: true,
        content: Feng.ctxPath + '/trappedNumber/trappedNumber_add'
    });
    this.layerIndex = index;
};

/**
 * 打开查看被困人员录入详情
 */
TrappedNumber.openTrappedNumberDetail = function () {
    if (this.check()) {
        var index = layer.open({
            type: 2,
            title: '被困人员录入详情',
            area: ['800px', '420px'], //宽高
            fix: false, //不固定
            maxmin: true,
            content: Feng.ctxPath + '/trappedNumber/trappedNumber_update/' + TrappedNumber.seItem.id
        });
        this.layerIndex = index;
    }
};

/**
 * 删除被困人员录入
 */
TrappedNumber.delete = function () {
    if (this.check()) {
        var ajax = new $ax(Feng.ctxPath + "/trappedNumber/delete", function (data) {
            Feng.success("删除成功!");
            TrappedNumber.table.refresh();
        }, function (data) {
            Feng.error("删除失败!" + data.responseJSON.message + "!");
        });
        ajax.set("trappedNumberId",this.seItem.id);
        ajax.start();
    }
};

/**
 * 查询被困人员录入列表
 */
TrappedNumber.search = function () {
    var queryData = {};
    queryData['condition'] = $("#condition").val();
    TrappedNumber.table.refresh({query: queryData});
};

$(function () {
    var defaultColunms = TrappedNumber.initColumn();
    var table = new BSTable(TrappedNumber.id, "/trappedNumber/list", defaultColunms);
    table.setPaginationType("server");
    TrappedNumber.table = table.init();
});
