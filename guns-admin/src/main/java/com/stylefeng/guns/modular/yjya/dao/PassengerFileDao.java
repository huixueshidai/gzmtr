package com.stylefeng.guns.modular.yjya.dao;

import com.baomidou.mybatisplus.plugins.Page;
import com.stylefeng.guns.common.persistence.model.PassengerFile;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

/**
 * Created by 60132 on 2018/9/30.
 */
public interface PassengerFileDao {
    /**
     *根据条件查询预案列表
     * @return
     */
    List<Map<String, Object>> getPassengerfiles(@Param("page") Page<PassengerFile> page);

}
