package com.stylefeng.guns.modular.yjya.dao;

import com.baomidou.mybatisplus.plugins.Page;
import com.stylefeng.guns.common.persistence.model.Accident;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

/**
 * Created by 60132 on 2018/5/29.
 */
public interface AccidentDao {
    /**
     *根据条件查询历史案例列表
     *@param page
     * @param emergencyId
     * @return
     */
    List<Map<String, Object>> getAccident(@Param("page") Page<Accident> page, @Param("emergencyId") Integer emergencyId);
}
