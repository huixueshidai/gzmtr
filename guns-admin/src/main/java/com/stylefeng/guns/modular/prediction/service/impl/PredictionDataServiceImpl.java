package com.stylefeng.guns.modular.prediction.service.impl;

import com.stylefeng.guns.common.persistence.model.PredictionData;
import com.stylefeng.guns.common.persistence.dao.PredictionDataMapper;
import com.stylefeng.guns.modular.prediction.service.IPredictionDataService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 风险预测数据 服务实现类
 * </p>
 *
 * @author xuziyang
 * @since 2018-05-19
 */
@Service
public class PredictionDataServiceImpl extends ServiceImpl<PredictionDataMapper, PredictionData> implements IPredictionDataService {
	
}
