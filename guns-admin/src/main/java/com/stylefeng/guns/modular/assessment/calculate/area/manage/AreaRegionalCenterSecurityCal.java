package com.stylefeng.guns.modular.assessment.calculate.area.manage;

import com.stylefeng.guns.common.persistence.model.Assessment;
import com.stylefeng.guns.modular.assessment.calculate.ThirdIndicationCalculator;
import com.stylefeng.guns.modular.assessment.calculate.aware.AreaThirdIndicationAware;
import org.springframework.stereotype.Component;

import java.util.Date;

/**
 * 区域中心安全管理指数
 *
 * @author yinjc
 * @time 2018/05/11
 */
@Component
public class AreaRegionalCenterSecurityCal extends ThirdIndicationCalculator implements AreaThirdIndicationAware {
    @Override
    protected Integer getIndicationId() {
        return 89;
    }

    @Override
    public Assessment calculate(Integer areaId, Date beginDate, Date endDate) {
        Double anQuan = getDoubleValue(areaId, 309, beginDate,endDate);
        return genResult(anQuan, areaId, beginDate,endDate);
    }
}
