package com.stylefeng.guns.modular.path.dao;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.stylefeng.guns.common.persistence.model.PathEvolution;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

/**
 * <p>
  * 事故情景路径演练 Mapper 接口
 * </p>
 *
 * @author xhq
 * @since 2018-09-04
 */
public interface PathEvolutionMapper extends BaseMapper<PathEvolution> {
    List<Map<String,Object>> selectMap(@Param("page") Page<PathEvolution> page);
}