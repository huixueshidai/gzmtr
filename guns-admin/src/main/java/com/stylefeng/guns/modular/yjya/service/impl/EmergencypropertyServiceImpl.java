package com.stylefeng.guns.modular.yjya.service.impl;

import com.stylefeng.guns.common.persistence.model.Emergencyproperty;
import com.stylefeng.guns.common.persistence.dao.EmergencypropertyMapper;
import com.stylefeng.guns.modular.yjya.service.IEmergencypropertyService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author wangshuaikang
 * @since 2018-05-24
 */
@Service
public class EmergencypropertyServiceImpl extends ServiceImpl<EmergencypropertyMapper, Emergencyproperty> implements IEmergencypropertyService {
	
}
