/**
 * web-upload 工具类
 * 
 * 约定：
 * 上传按钮的id = 图片隐藏域id + 'BtnId'
 * 图片预览框的id = 图片隐藏域id + 'PreId'
 * 
 * @author fengshuonan
 */
(function() {

	var $WebUpload = function(options) {
        if (typeof options == 'string') {

		}
        var settings = $.extend({}, $WebUpload.defaults, options);
        this.pictureId = settings.uploadId;
        this.uploadBtnId = settings.uploadId + "BtnId";
        this.uploadPreId = settings.uploadId + "PreId";
        this.uploadUrl = Feng.ctxPath + settings.url;
        this.fileSizeLimit = settings.fileSizeLimit;
        this.picWidth = 800;
        this.picHeight = 800;
        this.uploadBarId = settings.uploadBarId;
        this.mimeTypes = settings.mimeTypes;
        this.extensions = settings.extensions;
		this.successCallBack= function(file,response){
            if (response.code === 400) { //参数验证失败
                Feng.error(response.message);
            }else{
                Feng.success("上传成功");
                $("#" + this.pictureId).val(response);
				console.log(this.pictureId+"的值"+$("#" + this.pictureId).val());
            }
		}
	};

    $WebUpload.defaults = {
        url: "/mgr/upload",
        fileSizeLimit: 100 * 1024 * 1024,
		multiple:false,

    };

	$WebUpload.prototype = {
		/**
		 * 初始化webUploader
		 */
		init : function() {
			var uploader = this.create();
			this.bindEvent(uploader);
			return uploader;
		},
		
		/**
		 * 创建webuploader对象
		 */
		create : function() {
			var webUploader = WebUploader.create({
				auto : true,
				pick : {
					id : '#' + this.uploadBtnId,
					multiple : this.multiple, //是否支持上传多个
				},
				accept : {
					title : 'Images',
					extensions : this.extensions,
                    mimeTypes : this.mimeTypes
				},
				swf : Feng.ctxPath
						+ '/static/css/plugins/webuploader/Uploader.swf',
				disableGlobalDnd : true,
				duplicate : true,
				server : this.uploadUrl,
				fileSingleSizeLimit : this.fileSizeLimit
			});
			
			return webUploader;
		},

		/**
		 * 绑定事件
		 */
		bindEvent : function(bindedObj) {
			var me =  this;
			bindedObj.on('fileQueued', function(file) {
				var $li = $('<div><img width="100px" height="100px"></div>');
				var $img = $li.find('img');

				$("#" + me.uploadPreId).html($li);

				// 生成缩略图
				bindedObj.makeThumb(file, function(error, src) {
					if (error) {
						$img.replaceWith('<span>不能预览</span>');
						return;
					}
					$img.attr('src', src);
				}, me.picWidth, me.picHeight);
			});

			// 文件上传过程中创建进度条实时显示。
			bindedObj.on('uploadProgress', function(file, percentage) {
                $("#"+me.uploadBarId).css("width",percentage * 100 + "%");
			});

			// 文件上传成功，给item添加成功class, 用样式标记上传成功。
			bindedObj.on('uploadSuccess', function(file,response) {
                me.successCallBack(file, response);
			});

			// 文件上传失败，显示上传出错。
			bindedObj.on('uploadError', function(file) {
                console.log(arguments);
				Feng.error("上传失败");
			});

			// 其他错误
			bindedObj.on('error', function(type) {
				console.table(arguments)
				if ("Q_EXCEED_SIZE_LIMIT" == type) {
					Feng.error("文件大小超出了限制");
				} else if ("Q_TYPE_DENIED" == type) {
					Feng.error("文件类型不满足");
				} else if ("Q_EXCEED_NUM_LIMIT" == type) {
					Feng.error("上传数量超过限制");
				} else if ("F_DUPLICATE" == type) {
					Feng.error("图片选择重复");
				} else {
					Feng.error("上传过程中出错");
				}
			});

			// 完成上传完了，成功或者失败
			bindedObj.on('uploadComplete', function(file) {
			});
		},

        /**
         * 设置图片上传的进度条的id
         */
        setUploadBarId: function (id) {
            this.uploadBarId = id;
        }
	};



	window.$WebUpload = $WebUpload;

}());