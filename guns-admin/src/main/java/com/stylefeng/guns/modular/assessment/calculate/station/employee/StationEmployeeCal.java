package com.stylefeng.guns.modular.assessment.calculate.station.employee;

import com.stylefeng.guns.core.util.SpringContextHolder;
import com.stylefeng.guns.modular.assessment.calculate.SecondIndicationCalculator;
import com.stylefeng.guns.modular.assessment.calculate.ThirdIndicationCalculator;
import com.stylefeng.guns.modular.assessment.calculate.aware.StationSecondIndicationAware;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import java.util.Arrays;
import java.util.List;

/**
 * 车站员工
 */
@Component
@Scope(ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public class StationEmployeeCal extends SecondIndicationCalculator implements StationSecondIndicationAware {

    private List<ThirdIndicationCalculator> thirdIndicationCalculatorList;

    public StationEmployeeCal() {
        thirdIndicationCalculatorList = Arrays.asList(
                SpringContextHolder.getBean(StationQualityEmployeeCalculate.class),
                SpringContextHolder.getBean(StationProblemEmployeeCalculate.class),
                SpringContextHolder.getBean(StationTechnicianCalculate.class)
        );
    }

    @Override
    public List<ThirdIndicationCalculator> getThirdIndicationCalculatorList() {
        return thirdIndicationCalculatorList;
    }


    @Override
    protected Integer getIndicationId() {
        return 17;
    }
}
