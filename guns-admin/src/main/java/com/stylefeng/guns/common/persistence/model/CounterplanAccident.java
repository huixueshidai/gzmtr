package com.stylefeng.guns.common.persistence.model;

import java.io.Serializable;

import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;
import com.baomidou.mybatisplus.enums.IdType;
import lombok.ToString;

import java.io.Serializable;

/**
 * <p>
 * 预案处置流程表
 * </p>
 *
 * @author xuziyang
 * @since 2018-01-22
 */
@TableName("gzmtr_counterplan_accident")
public class CounterplanAccident extends Model<CounterplanAccident> {

    private static final long serialVersionUID = 1L;

	@TableId(value="id", type= IdType.INPUT)
	private Integer id;

	private String main;
    /**
     * 主体属性
     */
	private String mproperty;
    /**
     * 主题动作
     */
	private String mmotion;
    /**
     * 客体
     */
	private String object;
    /**
     * 客体属性
     */
	private String oproperty;
    /**
     * 客体动作
     */
	private String omotion;

	private Integer num;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getMain() {
		return main;
	}

	public void setMain(String main) {
		this.main = main;
	}

	public String getMproperty() {
		return mproperty;
	}

	public void setMproperty(String mproperty) {
		this.mproperty = mproperty;
	}

	public String getMmotion() {
		return mmotion;
	}

	public void setMmotion(String mmotion) {
		this.mmotion = mmotion;
	}

	public String getObject() {
		return object;
	}

	public void setObject(String object) {
		this.object = object;
	}

	public String getOproperty() {
		return oproperty;
	}

	public void setOproperty(String oproperty) {
		this.oproperty = oproperty;
	}

	public String getOmotion() {
		return omotion;
	}

	public void setOmotion(String omotion) {
		this.omotion = omotion;
	}

	public Integer getNum() {
		return num;
	}

	public void setNum(Integer num) {
		this.num = num;
	}

	@Override
	protected Serializable pkVal() {
		return this.id;
	}

	@Override
	public String toString() {
		return main + mproperty + mmotion + object +  oproperty + omotion;
	}
}
