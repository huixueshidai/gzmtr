package com.stylefeng.guns.modular.assessment.calculate.exception;

public class CalculateException extends RuntimeException{

	public CalculateException() {
		super();
	}

	public CalculateException(String message) {
		super(message);
	}
}
