package com.stylefeng.guns.modular.yjya.controller;

import com.stylefeng.guns.common.annotion.Permission;
import com.stylefeng.guns.common.persistence.dao.ContactGroupMapper;
import com.stylefeng.guns.common.persistence.model.ContactGroup;
import com.stylefeng.guns.core.base.controller.BaseController;
import com.stylefeng.guns.core.log.LogObjectHolder;
import com.stylefeng.guns.core.node.ZTreeNode;
import com.stylefeng.guns.modular.yjya.dao.ContactGroupDao;
import com.stylefeng.guns.modular.yjya.service.IContactGroupService;
import com.stylefeng.guns.modular.yjya.warpper.ContactGroupWarpper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.annotation.Resource;
import java.util.List;
import java.util.Map;

/**
 * 通讯录群组控制器
 *
 * @author xuziyang
 * @Date 2017-12-26 10:38:25
 */
@Controller
@RequestMapping("/contactGroup")
public class ContactGroupController extends BaseController {

    private String PREFIX = "/yjya/contactGroup/";

    @Autowired
    private IContactGroupService iContactGroupService;

    @Resource
    private ContactGroupDao contactGroupDao;

    /**
     * 跳转到通讯录群组首页
     */
    @RequestMapping("")
    public String index() {
        return PREFIX + "contactGroup.html";
    }

    /**
     * 跳转到添加通讯录群组
     */
    @RequestMapping("/contactGroup_add")
    public String contactGroupAdd() {
        return PREFIX + "contactGroup_add.html";
    }

    /**
     * 跳转到修改通讯录群组
     */
    @RequestMapping("/contactGroup_update/{contactGroupId}")
    public String contactGroupUpdate(@PathVariable Integer contactGroupId, Model model) {
        ContactGroup contactGroup = iContactGroupService.selectById(contactGroupId);
        model.addAttribute("item", contactGroup);
        String pName;
        if (contactGroup.getPid() == 0) {
            pName = "顶级";
        } else {
            pName = iContactGroupService.selectById(contactGroup.getPid()).getName();
        }
        model.addAttribute("pName", pName);
        LogObjectHolder.me().set(contactGroup);
        return PREFIX + "contactGroup_edit.html";
    }


    /**
     * 获取通讯录群组列表
     */
    @RequestMapping(value = "/list")
    @ResponseBody
    public Object list(String condition) {
        List<Map<String, Object>> list = this.contactGroupDao.list(condition);
        return new ContactGroupWarpper(list).warp();
    }


    /**
     * 通讯录群组详情
     */
    @RequestMapping(value = "/detail/{contactGroupId}")
    @ResponseBody
    public Object detail(@PathVariable("contactGroupId") Integer contactGroupId) {
        return iContactGroupService.selectById(contactGroupId);
    }

    /**
     * 获取tree列表
     */
    @RequestMapping(value = "/tree")
    @ResponseBody
    public List<ZTreeNode> tree() {
        List<ZTreeNode> tree = this.iContactGroupService.tree();
        return tree;
    }


    /**
     * 新增
     */
    @RequestMapping(value = "/add")
    @ResponseBody
    @Permission
    public Object add(ContactGroup contactGroup) {
        return this.iContactGroupService.addNode(contactGroup);
    }

    /**
     * 删除
     */
    @RequestMapping(value = "/delete")
    @ResponseBody
    public Object delete(@RequestParam Integer contactGroupId) {
        iContactGroupService.deleteSelfAndChild(contactGroupId);
        return SUCCESS_TIP;
    }

    /**
     * 修改
     */
    @RequestMapping(value = "/update")
    @ResponseBody
    public Object update(ContactGroup contactGroup) {
        iContactGroupService.updateNode(contactGroup);
        return super.SUCCESS_TIP;
    }

}
