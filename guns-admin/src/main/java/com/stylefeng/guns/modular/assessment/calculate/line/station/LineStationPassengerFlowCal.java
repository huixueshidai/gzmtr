package com.stylefeng.guns.modular.assessment.calculate.line.station;

import com.stylefeng.guns.common.persistence.model.Assessment;
import com.stylefeng.guns.common.persistence.model.Roadnet;
import com.stylefeng.guns.core.util.DoubleUtil;
import com.stylefeng.guns.modular.assessment.calculate.ThirdIndicationCalculator;
import com.stylefeng.guns.modular.assessment.calculate.aware.LineThirdIndicationAware;
import com.stylefeng.guns.modular.assessment.calculate.station.passenger.StationPassengerCal;
import com.stylefeng.guns.modular.system.dao.RoadnetDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Date;
import java.util.List;

@Component
public class LineStationPassengerFlowCal extends ThirdIndicationCalculator implements LineThirdIndicationAware {
    @Autowired
    private StationPassengerCal stationPassengerCal;

    @Autowired
    private RoadnetDao roadnetDao;

    @Override
    protected Integer getIndicationId() {
        return 69;
    }

    public Assessment calculate(Integer lineId, Date beginDate, Date endDate) {
        List<Double> list = getMulDoubleValue(lineId, 253, beginDate,endDate);
        Double keLiuLiang = getDoubleValue(lineId, 254, beginDate,endDate);
        Double resVal = 0D;
        for (int i = 0; i < list.size(); i++) {
            resVal = DoubleUtil.safeDiv(list.get(i), keLiuLiang);
        }

        List<Roadnet> stationList = roadnetDao.getIsUsedChildrenById(lineId);
        Double xH2 = 0D;

        for (int i = 0; i < stationList.size(); i++) {
            Double secondIndicationVal = stationPassengerCal.
                    get(stationList.get(i).getId(), beginDate,endDate)
                    .getAssessmentValue();

            xH2 = DoubleUtil.mul(resVal, secondIndicationVal);
        }

        return genResult(xH2, lineId, beginDate,endDate);
    }
}
