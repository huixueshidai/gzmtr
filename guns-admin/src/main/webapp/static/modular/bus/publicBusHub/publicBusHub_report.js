/**
 * Created by 60132 on 2018/4/13.
 */
var PulicBusHubReport={
    pulicBusHubReport: {},
    count: $("#itemSize").val(),
    itemTemplate: $("#itemTemplate").html(),

};

PulicBusHubReport.close = function () {
    parent.layer.close(window.parent.PublicBusEmulateInfo.layerIndex);
}


/**
 * 生成处置报告
 */
PulicBusHubReport.submit = function () {
    PublicbusInfoDlg.addSubmit();
    $("#reportForm").submit();
}
PulicBusHubReport.addItem = function(){
    $("#itemsArea").append(this.itemTemplate);
    $("#dictItem").attr("id", this.newId());
}

/**
 * item获取新的id
 */
PulicBusHubReport.newId = function () {
    if(this.count == undefined){
        this.count = 0;
    }
    this.count = this.count + 1;
    return "dictItem" + this.count;
};
/**
 * 删除item
 */
PulicBusHubReport.deleteItem = function (event) {
    var obj = Feng.eventParseObject(event);
    obj.parent().parent().remove();
};