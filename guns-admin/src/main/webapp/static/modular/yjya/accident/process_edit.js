var ProcessInfo = {
    processInstance: null
    , counterplanId: null
    , processJson:null
};

 var property = {
    toolBtns: ["start round mix", "end round", "task"],
    haveHead: true,
    headLabel: true,
    headBtns: ["undo", "redo"],//如果haveHead=true，则定义HEAD区的按钮
    haveTool: true,
    haveDashed: false, //虚线
    haveGroup: false,
    useOperStack: true
};

var remark = {
    cursor: "选择指针",
    direct: "结点连线",
    start: "开始结点",
    "end": "结束结点",
    "task": "流程结点",
    node: "自动结点",
    chat: "决策结点",
    state: "状态结点",
    plug: "附加插件",
    fork: "分支结点",
    "join": "联合结点",
    "complex mix": "复合结点",
    group: "组织划分框编辑开关"
};


ProcessInfo.save = function (json) {
    // var json = JSON.stringify(this.processInstance.exportData());
    //获取处置流程阶段的处置措施数据
    $("#processJson").val(JSON.stringify(json));
    if (json) {
        this.processJson = json;
        $("#step_next").click();
    }
}

ProcessInfo.renderLines = function (preNodes, curNode) {
    var preNodesLength = preNodes.length;
    for (var i = 0; i < preNodesLength; i++) {
        var line = {
            "type": "sl",
            "from": preNodes[i],
            "to": curNode,
            "name": ""
        }
        var lineId = preNodes[i] + curNode + "";
        this.processInstance.addLine(lineId, line);
    }
}

ProcessInfo.renderNodes = function (counterplanId) {
    var baseLeft = 120;
    var preNodes = [];
    var start_node = {
        "name": "开始",
        "left": 207,
        "top": 20,
        "type": "start round mix",
        "width": 26,
        "height": 26,
        "alt": true
    }
    var process = this.processInstance;
    process.addNode("start_node", start_node);
    preNodes = ["start_node"];

    var ajax = new $ax(Feng.ctxPath + "/counterplan/process/" + counterplanId, function (data) {
        var top = 100;
        var dataLength = data.length;
        for (var i = 0; i < dataLength; i++) {
            top += 60;
            var theSameNumProcesses = data[i];
            var theSameNumSize = theSameNumProcesses.length;
            //该步骤只有一个处理措施
            if (theSameNumSize == 1) {
                var node = {
                    "name": theSameNumProcesses[0],
                    "left": baseLeft,
                    "top": top,
                    "type": "task",
                    "width": 200,
                    "height": 26,
                    "alt": true
                }
                var curNodeId = 'node_' + i;
                process.addNode(curNodeId, node);
                ProcessInfo.renderLines(preNodes,curNodeId);
                preNodes = [curNodeId];
            } else { //该步骤多个处置措施
                //创建分支节点
                var forkNode = {
                    "name": '',
                    "left": 195,
                    "top": top,
                    "type": "fork",
                    "width": 50,
                    "height": 26,
                    "alt": true,
                    "marked":false
                }
                var curNodeId = 'node_' + i;
                process.addNode(curNodeId, forkNode);
                ProcessInfo.renderLines(preNodes,curNodeId);
                preNodes = [curNodeId];

                //在分支节点下，创建任务节点
                var theSameNumProcessesSize = theSameNumProcesses.length;
                var curNodes = [];
                top += 60;
                for (var j = 0; j < theSameNumProcessesSize; j++) {
                    var node = {
                        "name": theSameNumProcesses[j],
                        "left": baseLeft + j * (200 + 40),
                        "top": top,
                        "type": "task",
                        "width": 200,
                        "height": 26,
                        "alt": true
                    };
                    var curNodeId = 'node_' + i + "_" + j;
                    process.addNode(curNodeId, node);
                    ProcessInfo.renderLines(preNodes,curNodeId);
                    curNodes.push(curNodeId);
                }
                preNodes = curNodes;
            }
        }
    },function (data) {

    });
    ajax.start();
}


ProcessInfo.renderAccidentDisposalNodes = function (disposalJson) {
    var baseLeft = 120;
    var preNodes = [];
    var start_node = {
        "name": "开始",
        "left": 207,
        "top": 20,
        "type": "start round mix",
        "width": 26,
        "height": 26,
        "alt": true
    }
    var process = this.processInstance;
    process.addNode("start_node", start_node);
    preNodes = ["start_node"];

    var jsonObj =  JSON.parse(disposalJson);//转换为json数组对象
    var result = new Array();
    for (var i =0;i<jsonObj.length;i++) {
        var num = jsonObj[i].step;
        var theSameNumStringList;
        if (result.length > (num-1) ) {
            theSameNumStringList = result[num-1];
        }else {
            theSameNumStringList = new Array();
            result.push(theSameNumStringList);
        }
        theSameNumStringList.push(jsonObj[i].disposalTask);
    }
    console.log(result);
        var top = 100;
        var dataLength = result.length;
        for (var i = 0; i < dataLength; i++) {
            top += 60;
            var theSameNumProcesses = result[i];
            var theSameNumSize = theSameNumProcesses.length;
            //该步骤只有一个处理措施
            if (theSameNumSize == 1) {
                var node = {
                    "name": theSameNumProcesses[0],
                    "left": baseLeft,
                    "top": top,
                    "type": "task",
                    "width": 200,
                    "height": 26,
                    "alt": true
                }
                var curNodeId = 'node_' + i;
                process.addNode(curNodeId, node);
                ProcessInfo.renderLines(preNodes,curNodeId);
                preNodes = [curNodeId];
            } else { //该步骤多个处置措施
                //创建分支节点
                var forkNode = {
                    "name": '',
                    "left": 195,
                    "top": top,
                    "type": "fork",
                    "width": 50,
                    "height": 26,
                    "alt": true,
                    "marked":false
                }
                var curNodeId = 'node_' + i;
                process.addNode(curNodeId, forkNode);
                ProcessInfo.renderLines(preNodes,curNodeId);
                preNodes = [curNodeId];

                //在分支节点下，创建任务节点
                var theSameNumProcessesSize = theSameNumProcesses.length;
                var curNodes = [];
                top += 60;
                for (var j = 0; j < theSameNumProcessesSize; j++) {
                    var node = {
                        "name": theSameNumProcesses[j],
                        "left": baseLeft + j * (200 + 40),
                        "top": top,
                        "type": "task",
                        "width": 200,
                        "height": 26,
                        "alt": true
                    };
                    var curNodeId = 'node_' + i + "_" + j;
                    process.addNode(curNodeId, node);
                    ProcessInfo.renderLines(preNodes,curNodeId);
                    curNodes.push(curNodeId);
                }
                preNodes = curNodes;
            }
        }
}

ProcessInfo.getJson = function () {
    return this.processInstance.exportData();
}


ProcessInfo.init = function (counterplanId,type,json) {
    if (this.processInstance) {
        this.processInstance.destrory();
    }
    this.counterplanId = counterplanId;
    var process = $.createGooFlow($("#processEdit"), property);
    process.setNodeRemarks(remark);
    this.processInstance = process;
    process.setTitle("处置流程编辑");
    process.onItemRightClick = function (id, type) {
        return true;//返回false可以阻止浏览器默认的右键菜单事件
    }
    process.onItemDbClick = function (id, type) {
        return true;//返回false可以阻止原组件自带的双击直接编辑事件
    }

    if (!counterplanId) {
        Feng.info("请先选择预案或历史案例！");
    }
    if (type == "历史事故实例") {
        process.loadData(JSON.parse(json));
        ProcessInfo.processInstance = process;
    }
    else if (type == "预案") {
        ProcessInfo.renderNodes(counterplanId);
        process.reconsitut();
    }
    else{
        var disposalJson = AccidentInfoDlg.disposalJson;
        ProcessInfo.renderAccidentDisposalNodes(disposalJson);
        process.reconsitut();
    }
}
    ProcessInfo.initprocessInstance = function (json) {

        var process = $.createGooFlow($("#processExe"), property);
        this.processInstance = process;
        process.setTitle("处置阶段");
        process.loadData(json);
        process.onItemRightClick = function (id, type) {
            return true;//返回false可以阻止浏览器默认的右键菜单事件
        }
        process.onItemDbClick = function (id, type) {
            return true;//返回false可以阻止原组件自带的双击直接编辑事件
        }

    }
