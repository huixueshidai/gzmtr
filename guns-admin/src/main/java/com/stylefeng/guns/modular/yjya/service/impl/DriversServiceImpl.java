package com.stylefeng.guns.modular.yjya.service.impl;

import com.stylefeng.guns.common.persistence.model.Drivers;
import com.stylefeng.guns.common.persistence.dao.DriversMapper;
import com.stylefeng.guns.modular.yjya.service.IDriversService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author wangshuaikang
 * @since 2018-09-04
 */
@Service
public class DriversServiceImpl extends ServiceImpl<DriversMapper, Drivers> implements IDriversService {
	
}
