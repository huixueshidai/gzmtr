package com.stylefeng.guns.modular.yjya.service.impl;

import com.baomidou.mybatisplus.mapper.Condition;
import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.stylefeng.guns.common.persistence.dao.AccidentHistoryMapper;
import com.stylefeng.guns.common.persistence.model.AccidentHistory;
import com.stylefeng.guns.modular.yjya.service.IAccidentHistoryService;
import org.springframework.stereotype.Service;

import java.util.Date;

/**
 * <p>
 * 事故历史信息 服务实现类
 * </p>
 *
 * @author yinjc
 * @since 2018-05-18
 */
@Service
public class AccidentHistoryServiceImpl extends ServiceImpl<AccidentHistoryMapper, AccidentHistory> implements
		IAccidentHistoryService {

	@Override
	public int selectAccidentCountByCategoryAndDate(String category, Date beginDate, Date endDate) {
		return baseMapper.selectCount(Condition.create()
				.setSqlSelect("count(1)")
				.eq("category",category)
				.eq("rank_code","1")
				.between("date",beginDate,endDate)
		);
	}

	@Override
	public int selectFactorCountByCategoryCauseAndDate(String category,String cause,Date beginDate,Date endDate){
		return baseMapper.selectCount(Condition.create()
				.setSqlSelect("count(1)")
				.eq("category",category)
				.eq("cause",cause)
				.eq("rank_code","2")
				.between("date",beginDate,endDate)
		);
	}

}
