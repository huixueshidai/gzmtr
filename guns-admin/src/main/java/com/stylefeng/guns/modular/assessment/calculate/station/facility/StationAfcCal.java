package com.stylefeng.guns.modular.assessment.calculate.station.facility;

import com.stylefeng.guns.common.persistence.model.Assessment;
import com.stylefeng.guns.core.util.DoubleUtil;
import com.stylefeng.guns.modular.assessment.calculate.ThirdIndicationCalculator;
import com.stylefeng.guns.modular.assessment.calculate.aware.StationThirdIndicationAware;
import org.apache.commons.collections.CollectionUtils;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * AFC系统影响运营风险指数
 */
@Component
public class StationAfcCal extends ThirdIndicationCalculator implements StationThirdIndicationAware {

    @Override
    protected Integer getIndicationId() {
        return 32;
    }

    @Override
    public Assessment calculate(Integer stationId, Date beginDate,Date endDate) {
        Double stoppageTime = getDoubleValue(stationId, 56, beginDate,endDate);
        Double totalTime = getDoubleValue(stationId, 57, beginDate,endDate);

        Double d = getDoubleValue(stationId, 58, beginDate,endDate);
        Double D = getDoubleValue(stationId, 59, beginDate,endDate);


        Double n = getDoubleValue(stationId, 60, beginDate,endDate);
        Double N = getDoubleValue(stationId, 61, beginDate,endDate);

        List<Double> wList = getMulDoubleValue(stationId, 62, beginDate,endDate);
        if (CollectionUtils.isEmpty(wList) || wList.size() != 3) {
            wList = new ArrayList<>(3);
            wList.add(0.33d);
            wList.add(0.34d);
            wList.add(0.33d);
        }
        Double result = DoubleUtil.add(
                DoubleUtil.mul(wList.get(0), DoubleUtil.safeDiv(stoppageTime, totalTime)),
                DoubleUtil.mul(wList.get(1), DoubleUtil.safeDiv(d, D)),
                DoubleUtil.mul(wList.get(2), DoubleUtil.safeDiv(n, N))
        );

        return genResult(result, stationId, beginDate,endDate);
    }

}
