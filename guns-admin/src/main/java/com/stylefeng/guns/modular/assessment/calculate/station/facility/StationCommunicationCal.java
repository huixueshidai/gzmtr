package com.stylefeng.guns.modular.assessment.calculate.station.facility;

import com.stylefeng.guns.common.persistence.model.Assessment;
import com.stylefeng.guns.core.util.DoubleUtil;
import com.stylefeng.guns.modular.assessment.calculate.ThirdIndicationCalculator;
import com.stylefeng.guns.modular.assessment.calculate.aware.StationThirdIndicationAware;
import org.springframework.stereotype.Component;

import java.util.Date;

/**
 * 通信系统
 */
@Component
public class StationCommunicationCal extends ThirdIndicationCalculator implements StationThirdIndicationAware {

    @Override
    protected Integer getIndicationId() {
        return 25;
    }

    @Override
    public Assessment calculate(Integer stationId, Date beginDate, Date endDate) {
        Double stop = getDoubleValue(stationId, 42, beginDate,endDate);
        Double total = getDoubleValue(stationId, 43, beginDate,endDate);
        if (total == 0d) {
            return genResult(1d, stationId, beginDate,endDate);
        }
        Double resultValue = DoubleUtil.sub(1, DoubleUtil.div(stop, total));
        return genResult(resultValue, stationId, beginDate,endDate);
    }

}
