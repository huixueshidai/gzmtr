package com.stylefeng.guns.modular.yjya.service.impl;

import com.stylefeng.guns.common.persistence.model.Law;
import com.stylefeng.guns.common.persistence.dao.LawMapper;
import com.stylefeng.guns.modular.yjya.service.ILawService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author 殷佳成
 * @since 2018-01-18
 */
@Service
public class LawServiceImpl extends ServiceImpl<LawMapper, Law> implements ILawService {
	
}
