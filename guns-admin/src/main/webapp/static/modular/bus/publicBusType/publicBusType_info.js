/**
 * 初始化公交车类型详情对话框
 */
var PublicBusTypeInfoDlg = {
    publicBusTypeInfoData : {}
};

/**
 * 清除数据
 */
PublicBusTypeInfoDlg.clearData = function() {
    this.publicBusTypeInfoData = {};
}

/**
 * 设置对话框中的数据
 *
 * @param key 数据的名称
 * @param val 数据的具体值
 */
PublicBusTypeInfoDlg.set = function(key, val) {
    this.publicBusTypeInfoData[key] = (typeof val == "undefined") ? $("#" + key).val() : val;
    return this;
}

/**
 * 设置对话框中的数据
 *
 * @param key 数据的名称
 * @param val 数据的具体值
 */
PublicBusTypeInfoDlg.get = function(key) {
    return $("#" + key).val();
}

/**
 * 关闭此对话框
 */
PublicBusTypeInfoDlg.close = function() {
    parent.layer.close(window.parent.PublicBusType.layerIndex);
}

/**
 * 收集数据
 */
PublicBusTypeInfoDlg.collectData = function() {
    this
    .set('id')
    .set('busTypeName')
    .set('busCapacity')
    ;
}

/**
 * 提交添加
 */
PublicBusTypeInfoDlg.addSubmit = function() {

    this.clearData();
    this.collectData();

    //提交信息
    var ajax = new $ax(Feng.ctxPath + "/publicBusType/add", function(data){
        Feng.success("添加成功!");
        window.parent.PublicBusType.table.refresh();
        PublicBusTypeInfoDlg.close();
    },function(data){
        Feng.error("添加失败!" + data.responseJSON.message + "!");
    });
    ajax.set(this.publicBusTypeInfoData);
    ajax.start();
}

/**
 * 提交修改
 */
PublicBusTypeInfoDlg.editSubmit = function() {

    this.clearData();
    this.collectData();

    //提交信息
    var ajax = new $ax(Feng.ctxPath + "/publicBusType/update", function(data){
        Feng.success("修改成功!");
        window.parent.PublicBusType.table.refresh();
        PublicBusTypeInfoDlg.close();
    },function(data){
        Feng.error("修改失败!" + data.responseJSON.message + "!");
    });
    ajax.set(this.publicBusTypeInfoData);
    ajax.start();
}

$(function() {

});
