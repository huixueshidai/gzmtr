package com.stylefeng.guns.modular.yjya.controller;

import com.stylefeng.guns.core.base.controller.BaseController;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.beans.factory.annotation.Autowired;
import com.stylefeng.guns.core.log.LogObjectHolder;
import org.springframework.web.bind.annotation.RequestParam;
import com.stylefeng.guns.common.persistence.model.Drivers;
import com.stylefeng.guns.modular.yjya.service.IDriversService;

/**
 * 公交车司机控制器
 *
 * @author wangshuaikang
 * @Date 2018-09-04 16:12:10
 */
@Controller
@RequestMapping("/drivers")
public class DriversController extends BaseController {

    private String PREFIX = "/yjya/drivers/";

    @Autowired
    private IDriversService driversService;

    /**
     * 跳转到公交车司机首页
     */
    @RequestMapping("")
    public String index() {
        return PREFIX + "drivers.html";
    }

    /**
     * 跳转到添加公交车司机
     */
    @RequestMapping("/drivers_add")
    public String driversAdd() {
        return PREFIX + "drivers_add.html";
    }

    /**
     * 跳转到修改公交车司机
     */
    @RequestMapping("/drivers_update/{driversId}")
    public String driversUpdate(@PathVariable Integer driversId, Model model) {
        Drivers drivers = driversService.selectById(driversId);
        model.addAttribute("item",drivers);
        LogObjectHolder.me().set(drivers);
        return PREFIX + "drivers_edit.html";
    }

    /**
     * 获取公交车司机列表
     */
    @RequestMapping(value = "/list")
    @ResponseBody
    public Object list(String condition) {
        return driversService.selectList(null);
    }

    /**
     * 新增公交车司机
     */
    @RequestMapping(value = "/add")
    @ResponseBody
    public Object add(Drivers drivers) {
        driversService.insert(drivers);
        return super.SUCCESS_TIP;
    }

    /**
     * 删除公交车司机
     */
    @RequestMapping(value = "/delete")
    @ResponseBody
    public Object delete(@RequestParam Integer driversId) {
        driversService.deleteById(driversId);
        return SUCCESS_TIP;
    }

    /**
     * 修改公交车司机
     */
    @RequestMapping(value = "/update")
    @ResponseBody
    public Object update(Drivers drivers) {
        driversService.updateById(drivers);
        return super.SUCCESS_TIP;
    }

    /**
     * 公交车司机详情
     */
    @RequestMapping(value = "/detail/{driversId}")
    @ResponseBody
    public Object detail(@PathVariable("driversId") Integer driversId) {
        return driversService.selectById(driversId);
    }
}
