package com.stylefeng.guns.modular.prediction.controller;

import com.github.abel533.echarts.Option;
import com.github.abel533.echarts.axis.ValueAxis;
import com.github.abel533.echarts.code.SeriesType;
import com.github.abel533.echarts.code.Trigger;
import com.github.abel533.echarts.json.GsonOption;
import com.github.abel533.echarts.series.Line;
import com.github.abel533.echarts.series.Series;
import com.stylefeng.guns.common.persistence.model.RiskFactor;
import com.stylefeng.guns.core.base.controller.BaseController;
import com.stylefeng.guns.core.log.LogObjectHolder;
import com.stylefeng.guns.core.node.ZTreeNode;
import com.stylefeng.guns.core.util.DoubleUtil;
import com.stylefeng.guns.modular.prediction.dao.RiskFactorDao;
import com.stylefeng.guns.modular.prediction.service.IRiskFactorService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;

import static java.util.stream.Collectors.toList;

/**
 * 事故致因控制器
 *
 * @author xuziyang
 * @Date 2018-04-11 16:57:33
 */
@Controller
@RequestMapping("/riskFactor")
public class RiskFactorController extends BaseController {

    private String PREFIX = "/prediction/riskFactor/";

    @Autowired
    private IRiskFactorService riskFactorService;

    @Autowired
    private RiskFactorDao riskFactorDao;

    /**
     * 跳转到事故致因首页
     */
    @RequestMapping("")
    public String index() {
        return PREFIX + "riskFactor.html";
    }

	/**
	 * 获取事故致因列表
	 */
	@RequestMapping(value = "/list")
	@ResponseBody
	public Object list(String name) {
		return riskFactorDao.selectRiskFactors(name);
	}

    /**
     * 跳转到添加事故致因
     */
    @RequestMapping("/riskFactor_add")
    public String riskFactorAdd() {
        return PREFIX + "riskFactor_add.html";
    }

    /**
     * 跳转到修改事故致因
     */
    @RequestMapping("/riskFactor_update/{riskFactorId}")
    public String riskFactorUpdate(@PathVariable Integer riskFactorId, Model model) {
        RiskFactor riskFactor = riskFactorService.selectById(riskFactorId);
        model.addAttribute("pName", riskFactorDao.selectNameById(riskFactor.getPid()));
        model.addAttribute("item", riskFactor);
        LogObjectHolder.me().set(riskFactor);
        return PREFIX + "riskFactor_edit.html";
    }


    /**
     * 跳转到事故致因分析
     *
     * @return
     */
    @RequestMapping("riskFactor_chart")
    public String riskFactorAnalyse() {
        return PREFIX + "riskFactor_chart.html";
    }


    @RequestMapping("analyse")
    @ResponseBody
    public Object analyse(Integer rootId) {
        List<RiskFactor> list = riskFactorService.analyse(rootId);
        return toEcharsLineDto(list);
    }

    private Object toEcharsLineDto(List<RiskFactor> riskFactors) {
		List<String> legends = riskFactors.stream().map(RiskFactor::getName).collect(toList());
		List<Series> series = riskFactors.stream().map(this::toSeries).collect(toList());

		Option option = new GsonOption();
		option.calculable(true);
		option.tooltip().trigger(Trigger.item).formatter("{a} <br/> {c}");
		option.legend(legends.toArray());
		option.series(series);

		option.yAxis(new ValueAxis().name("置信水平").min(0).max(1));
		option.xAxis(new ValueAxis().name("发生概率"));
		return option.toString();
    }

	private Series toSeries(RiskFactor riskFactor) {
		Line line = new Line();
		Double up = DoubleUtil.add(riskFactor.getMedian(), riskFactor.getUpper());
		Double fl = DoubleUtil.sub(riskFactor.getMedian(), riskFactor.getFlo());
		line
			.smooth(false)
			.type(SeriesType.line)
			.name(riskFactor.getName())
			.data(new double[]{fl, 0}, new double[]{riskFactor.getMedian(), 1}, new double[]{up, 0});
		return line;
	}


	/**
	 * 获取tree列表
	 */
	@RequestMapping(value = "/tree")
	@ResponseBody
	public List<ZTreeNode> tree() {
		List<ZTreeNode> tree = this.riskFactorService.tree();
		return tree;
	}

	/**
	 * 新增事故致因
	 */
	@RequestMapping(value = "/add")
	@ResponseBody
	public Object add(RiskFactor riskFactor) {
		riskFactorService.addNode(riskFactor);
		return super.SUCCESS_TIP;
	}

	/**
	 * 删除事故致因
	 */
	@RequestMapping(value = "/delete")
	@ResponseBody
	public Object delete(@RequestParam Integer riskFactorId) {
		riskFactorService.deleteSelfAndChild(riskFactorId);
		return SUCCESS_TIP;
	}


	/**
     * 修改事故致因
     */
    @RequestMapping(value = "/update")
    @ResponseBody
    public Object update(RiskFactor riskFactor) {
        riskFactorService.updateNode(riskFactor);
        return super.SUCCESS_TIP;
    }

    /**
     * 设为默认
     */
    @RequestMapping(value = "/seDefaultStatus")
    @ResponseBody
    public Object setStatusDefault(Integer id) {
        riskFactorService.updateStatusDefault(id);
        return super.SUCCESS_TIP;
    }

    /**
     * 事故致因详情
     */
    @RequestMapping(value = "/detail/{riskFactorId}")
    @ResponseBody
    public Object detail(@PathVariable("riskFactorId") Integer riskFactorId) {
        return riskFactorService.selectById(riskFactorId);
    }
}
