package com.stylefeng.guns.common.persistence.dao;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.stylefeng.guns.common.persistence.model.AccidentHistory;
import org.apache.ibatis.annotations.Param;

import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * <p>
  * 事故历史信息 Mapper 接口
 * </p>
 *
 * @author yinjc
 * @since 2018-05-18
 */
public interface AccidentHistoryMapper extends BaseMapper<AccidentHistory> {

    List<Map<String,Object>> selectPage2(@Param("page") Page<AccidentHistory> page,
										 @Param("name")String  name,
			                             @Param("category")String category,
			                             @Param("rankCode") String rankCode,
			                             @Param("beginTime") String beginTime,
										 @Param("endTime") String endTime
	);

    /**
     * 查询往年当前月份的事故
     * @return
     */
    List<Map<String, Object>> selectAccidentHistoryInfoOfCurMonthWithoutCurYear();

    /**
     * 查询往年今日的事故
     * @return
     */
    List<Map<String, Object>> selectAccidentHistoryInfoOfCurDayWithoutCurYear();

    /**
     * 查询往年明日的事故
     * @return
     */
    List<Map<String, Object>> selectAccidentHistoryInfoOfTomorrowWithoutCurYear();


}