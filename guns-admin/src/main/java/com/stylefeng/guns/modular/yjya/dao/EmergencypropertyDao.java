package com.stylefeng.guns.modular.yjya.dao;
import com.baomidou.mybatisplus.plugins.Page;
import com.stylefeng.guns.common.persistence.model.Emergencyproperty;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

/**
 * Created by 60132 on 2018/5/29.
 */
public interface EmergencypropertyDao {
    /**
     *根据条件查询历史案例列表
     * @param emergencyId
     * @return
     */
    Emergencyproperty getEmergencyPropertyOne(@Param("emergencyId") Integer emergencyId);

    List<Map<String,Object>> selectEmergencypropertys(@Param("page") Page<Emergencyproperty> page , @Param("emergencyId") Integer emergencyId);
}
