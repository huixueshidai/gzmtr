// 百度地图API功能

var PublicBusFeederRoute ={
    line:$("#line").val(),
    station:$("#station").val(),
    stationList:new Array(),//定义接驳站点
    startStation:null,//接驳起始站
    endStation:null,//接驳结束站点
    selected:false,
    opts:null,
    mySubway:null,
    passengerNums:null

};

$(function(){

    if(!$("#currten").val()||$("#currten").val()<1){
        var div = document.getElementById('busStar');
        div.style.cssText = 'background-color:  #000;opacity: 0.5;width:100%;height:100%;position:fixed;top:0;left:0;z-index:999;';
        $("#busSure").attr("disabled", true);
        $("#busCancle").attr("disabled", true);
        $("#busStarUp").attr("disabled", true);
        Feng.info("请先填写启动公交应急联动信息！")
    }
    else if($("#currten").val()==1){
        //开启easy模式, 直接完成地铁图基本功能, 无需自己写交互
        window.cbk = function() {
            var mySubway = subway("mysubway", {
                adcode: 4401,
                theme: "normal",
                client: 0,
                doubleclick: {
                    switch: false
                }
            });

            mySubway.event.on("subway.complete", function (ev, info) {
                PublicBusFeederRoute.mySubway = mySubway;
                //着重显示故障线路
                mySubway.showLine("1号线");
                var stationNames = PublicBusFeederRoute.station.split(">>");
                console.log(stationNames);
                for(var i = 0;i<stationNames.length;i++){
                    // 显示故障地铁站
                    mySubway.addMarker(stationNames[i],{
                        offset:{x:2,y:10},
                        cnt:'<img src="'+Feng.ctxPath+'/static/img/marker_red_sprite.png" width="20" height="25" title="事故点" style="cursor:pointer"/>',
                    });

                    PublicBusFeederRoute.passengerNum(stationNames[i]);
                    mySubway.addInfoWindow(stationNames[i], {content:'<div class="tip_wrap"> <div class="tip_bady"> <div class="tip_name_detail"> <span class="tip_name" id="tip_name">下行  '+PublicBusFeederRoute.passengerNums[0]+'人/15min</span></div> <div class="tip_name_detail"> <span class="tip_name" id="tip_name">上行  '+PublicBusFeederRoute.passengerNums[1]+'人/15min</span></div> </div> <div class="tip_footer"> </div> </div>'});
                }
                mySubway.addMarker('西朗',{
                    width:22,
                    height:22,
                    offset:{x:1.5,y:9},
                    cnt:'<div id="xilang" onclick="PublicBusFeederRoute.imgClick(this)"><img src="'+Feng.ctxPath+'/static/img/biaoji.png" width="22" height="22" title="折返点" style="cursor:pointer" /><input type="hidden" id="a1" value="西朗"/></div>',
                });
                mySubway.addMarker('东山口',{
                    width:22,
                    height:22,
                    offset:{x:1.5,y:9},
                    cnt:'<div id="dongshankou" onclick="PublicBusFeederRoute.imgClick(this)"><img src="'+Feng.ctxPath+'/static/img/biaoji.png" width="22" height="22" title="折返点" style="cursor:pointer"/><input type="hidden" id="a1" value="东山口"/></div>',
                });
                mySubway.addMarker('芳村',{
                    width:22,
                    height:22,
                    offset:{x:1.5,y:9},
                    cnt:'<div id="fangxun" onclick="PublicBusFeederRoute.imgClick(this)"><img src="'+Feng.ctxPath+'/static/img/biaoji.png" width="22" height="22" title="折返点" style="cursor:pointer" /><input type="hidden" id="a1" value="芳村"/></div>',
                });
                mySubway.addMarker('公园前',{
                    width:22,
                    height:22,
                    offset:{x:1.5,y:9},
                    cnt:'<div id="gongyuanqian" onclick="PublicBusFeederRoute.imgClick(this)"><img src="'+Feng.ctxPath+'/static/img/biaoji.png" width="22" height="22" title="折返点" style="cursor:pointer"/><input type="hidden" id="a1" value="公园前"/></div>',
                });
                mySubway.addMarker('广州东站',{
                    width:22,
                    height:22,
                    offset:{x:1.5,y:9},
                    cnt:'<div id="guangzhoudongzhan" onclick="PublicBusFeederRoute.imgClick(this)"><img src="'+Feng.ctxPath+'/static/img/biaoji.png" width="22" height="22" title="折返点" style="cursor:pointer"/><input type="hidden" id="a1" value="广州东站"/></div>',
                });
            });
            //点击空白区域只显示1号线
            mySubway.event.on("subway.touch", function (){
                mySubway.showLine("1号线");
            });
            mySubway.event.on('station.touch',function(ev,info){
                PublicBusFeederRoute.stationList.push(arguments[1].name);
                mySubway.addMarker(arguments[1].id,{
                    offset:{x:3,y:15},
                    cnt:'<img src="'+Feng.ctxPath+'/static/img/marker_blue_sprite.png" width="18" height="20" title="接驳站点" style="cursor:pointer"/>',
                });
            });

            mySubway.event.on('stationName.touch',function(ev,info){
                var stationName = info.name;
                PublicBusFeederRoute.passengerNum(stationName);
                mySubway.addInfoWindow(stationName, {content:'<div class="tip_wrap"> <div class="tip_bady"> <div class="tip_name_detail"> <span class="tip_name" id="tip_name">下行  '+PublicBusFeederRoute.passengerNums[0]+'人/15min</span></div> <div class="tip_name_detail"> <span class="tip_name" id="tip_name">上行  '+PublicBusFeederRoute.passengerNums[1]+'人/15min</span></div> </div> <div class="tip_footer"> </div> </div>'});
            });

        }
    }
    else{
        $("#busSure").attr("disabled",true);
        $("#busCancle").attr("disabled",true);
        $("#busStarUp").attr("disabled",true);
        //开启easy模式, 直接完成地铁图基本功能, 无需自己写交互
        window.cbk = function() {
            var mySubway = subway("mysubway", {
                adcode: 4401,
                theme: "normal",
                client: 0,
                doubleclick: {
                    switch: false
                }
            });
            PublicBusFeederRoute.mySubway = mySubway;
            mySubway.event.on("subway.complete", function (ev, info) {
                //着重显示故障线路
                mySubway.showLine("1号线");
                mySubway.route($("#startStation").val(),$("#endStation").val(),{
                    closeBtn: false
                });
                var stationList = new Array("西朗","坑口","花地湾","芳村","黄沙","长寿路","陈家祠","西门口","公园前","农讲所","烈士陵园","东山口","杨箕","体育西路","体育中心","广州东站");
                var jieboStation = false;
                PublicBusFeederRoute.setResponselevel(stationList[5]);
                for(var i=0;i<stationList.length;i++){
                    if($("#startStation").val()==stationList[i]||jieboStation==true){
                        PublicBusFeederRoute.setResponselevel(stationList[i]);
                        jieboStation = true;
                    }
                    if($("#endStation").val()==stationList[i]){
                        jieboStation = false;
                    }

                }
            });
            //点击空白区域只显示1号线
            mySubway.event.on("subway.touch", function (){
                mySubway.showLine("1号线");
            });
            mySubway.event.on('stationName.touch',function(ev,info){
                var stationName = info.name;
                PublicBusFeederRoute.passengerNum(stationName);
                mySubway.addInfoWindow(stationName, {content:'<div class="tip_wrap"> <div class="tip_bady"> <div class="tip_name_detail"> <span class="tip_name" id="tip_name">下行  '+PublicBusFeederRoute.passengerNums[0]+'人/15min</span></div> <div class="tip_name_detail"> <span class="tip_name" id="tip_name">上行  '+PublicBusFeederRoute.passengerNums[1]+'人/15min</span></div> </div> <div class="tip_footer"> </div> </div>'});
            });

        }
    }
});



PublicBusFeederRoute.sure = function(){
    PublicBusFeederRoute.mySubway.clearOverlays();
    PublicBusFeederRoute.startSta();
    PublicBusFeederRoute.endSta();
    PublicBusFeederRoute.mySubway.route(PublicBusFeederRoute.startStation,PublicBusFeederRoute.endStation,{
        closeBtn: false
    });
    for(var i=0;i<PublicBusFeederRoute.stationList.length;i++){
        PublicBusFeederRoute.setResponselevel(PublicBusFeederRoute.stationList[i]);
    }

    PublicBusFeederRoute.selected = true;
    $("#busStarUp").attr("disabled", false);

}
/**
 * 启动公交应急联动方案
 */
PublicBusFeederRoute.startUp = function(){
    if(PublicBusFeederRoute.startStation==null){
        Feng.info("请选择接驳站点");
        return false;
    }
        var index = layer.open({
            type: 2,
            title: '公交联动方案',
            area: ['1200px', '700px'], //宽高
            fix: false, //不固定
            maxmin: true,
            content: Feng.ctxPath + "/publicBusHub/Me/"+PublicBusFeederRoute.stationList.length+"/"+PublicBusFeederRoute.startStation+"/"+PublicBusFeederRoute.endStation+"/"+lineId+"/"+startTime+"/"+stationId+"/"+retentionTime+"/"+lossEfficiencyRetention+"/"+lossEfficiencyTransportation
        });
        this.layerIndex = index;

}
PublicBusFeederRoute.startSta = function(){
    var stationList = new Array('西朗','坑口','花地湾','芳村','黄沙','长寿路','陈家祠','西门口','公园前','农讲所','烈士陵园','东山口','杨箕','体育西路','体育中心','广州东站');
    var stationNames = PublicBusFeederRoute.station.split(">>");
    for(var i = 0;i<stationNames.length;i++){
        PublicBusFeederRoute.stationList.push(stationNames[i]);
    }
    for(var i=0;i<stationList.length;i++){
        if(PublicBusFeederRoute.startStation!=null){
            break;
        }
        for(var j= 0;j<PublicBusFeederRoute.stationList.length;j++){
            if(stationList[i]==PublicBusFeederRoute.stationList[j]){
                PublicBusFeederRoute.startStation = PublicBusFeederRoute.stationList[j];
                break;
            }
        }
    }
}
PublicBusFeederRoute.endSta = function(){
    var stationList = new Array('西朗','坑口','花地湾','芳村','黄沙','长寿路','陈家祠','西门口','公园前','农讲所','烈士陵园','东山口','杨箕','体育西路','体育中心','广州东站');
    for(var i=stationList.length-1;i>=0;i--){
        if(PublicBusFeederRoute.endStation!=null){
            break;
        }
        for(var j= 0;j<PublicBusFeederRoute.stationList.length;j++){
            if(stationList[i]==PublicBusFeederRoute.stationList[j]){
                PublicBusFeederRoute.endStation = PublicBusFeederRoute.stationList[j];
                break;
            }
        }
    }
}

PublicBusFeederRoute.cance = function () {
    location.reload();
}

PublicBusFeederRoute.setResponselevel  = function(stationName){
    if(stationName=="烈士陵园"||stationName=="西门口"||stationName=="长寿路"||stationName=="体育中心"){
        PublicBusFeederRoute.mySubway.addMarker(stationName,{
            width:22,
            height:22,
            offset:{x:-1,y:9},
            cnt:'<img src="'+Feng.ctxPath+'/static/img/dongtaired.gif" width="25" height="25" title="接驳站点" style="cursor:pointer"/>',
        });
    }
    if(stationName=="芳村"||stationName=="陈家祠"||stationName=="公园前"||stationName=="广州东站"){
        PublicBusFeederRoute.mySubway.addMarker(stationName,{
            width:25,
            height:25,
            offset:{x:1,y:12},
            cnt:'<img src="'+Feng.ctxPath+'/static/img/dongtaiorange.gif" width="25" height="25" title="接驳站点" style="cursor:pointer"/>',
        });
    }
    if(stationName=="农讲所"||stationName=="体育西路"||stationName=="坑口"||stationName=="东山口"){
        PublicBusFeederRoute.mySubway.addMarker(stationName,{
            width:25,
            height:25,
            offset:{x:0,y:12},
            cnt:'<img src="'+Feng.ctxPath+'/static/img/dongtaiyellow.gif" width="25" height="25" title="接驳站点" style="cursor:pointer"/>',
        });
    }
    if(stationName=="杨箕"||stationName=="黄沙"||stationName=="西朗"||stationName=="花地湾"){
        PublicBusFeederRoute.mySubway.addMarker(stationName,{
            width:25,
            height:25,
            offset:{x:0,y:12},
            cnt:'<img src="'+Feng.ctxPath+'/static/img/dongtaiblue.gif" width="25" height="25" title="接驳站点" style="cursor:pointer"/>',
        });
    }

}

/**
 * 计算上行下行客流
 * @param stationName
 */
PublicBusFeederRoute.passengerNum = function(stationName){
    var startTime = $("#startTime").val();
    var roadnetLine = $("#lineId").val();
    var ajax = new $ax(Feng.ctxPath + "/publicBusHub/passenger_num/"+stationName+"/"+startTime+"/"+roadnetLine, function (data) {
        PublicBusFeederRoute.passengerNums = data.split(",");
    }, function (data) {

    });

    ajax.start();
}


PublicBusFeederRoute.imgClick = function(e){
    var stationName = $($(e).find("input")[0]).attr("value");
    PublicBusFeederRoute.stationList.push(stationName);
    PublicBusFeederRoute.passengerNum(stationName);
    PublicBusFeederRoute.mySubway.addMarker(stationName ,{
            offset:{x:3,y:15},
            cnt:'<img id="a1" src="'+Feng.ctxPath+'/static/img/marker_blue_sprite.png" width="18" height="20" title="接驳站点" style="cursor:pointer" />'
    });

}






