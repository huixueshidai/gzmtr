package com.stylefeng.guns.modular.assessment.calculate.station.passenger;

import com.stylefeng.guns.core.util.SpringContextHolder;
import com.stylefeng.guns.modular.assessment.calculate.SecondIndicationCalculator;
import com.stylefeng.guns.modular.assessment.calculate.ThirdIndicationCalculator;
import com.stylefeng.guns.modular.assessment.calculate.aware.StationSecondIndicationAware;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import java.util.Arrays;
import java.util.List;

@Component
@Scope(ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public class StationPassengerCal extends SecondIndicationCalculator implements StationSecondIndicationAware {

    private static final Integer secondIndicationId = 2;

    private List<ThirdIndicationCalculator> thirdIndicationCalculatorList;

    public StationPassengerCal() {
        thirdIndicationCalculatorList = Arrays.asList(
                SpringContextHolder.getBean(StationChannelCal.class),
                SpringContextHolder.getBean(StationEscalatorCal.class),
                SpringContextHolder.getBean(StationInCal.class),
                SpringContextHolder.getBean(StationOutCal.class),
                SpringContextHolder.getBean(StationStaircaseCal.class),
                SpringContextHolder.getBean(StationZhantaiCal.class)

        );
    }

    @Override
    public List<ThirdIndicationCalculator> getThirdIndicationCalculatorList() {
        return thirdIndicationCalculatorList;
    }

    @Override
    protected Integer getIndicationId() {
        return 2;
    }
}
