package com.stylefeng.guns.modular.assessment.calculate.line.facility;

import com.stylefeng.guns.common.persistence.model.Assessment;
import com.stylefeng.guns.core.util.DoubleUtil;
import com.stylefeng.guns.modular.assessment.calculate.ThirdIndicationCalculator;
import com.stylefeng.guns.modular.assessment.calculate.aware.LineThirdIndicationAware;
import org.springframework.stereotype.Component;

import java.util.Date;

/**
 * 信号系统影响运营风险指数
 */
@Component
public class LineBeaconageCal extends ThirdIndicationCalculator implements LineThirdIndicationAware {

    @Override
    protected Integer getIndicationId() {
        return 60;
    }

    @Override
    public Assessment calculate(Integer lineId, Date beginDate, Date endDate) {
        //信号系统故障率
        Double failureRate = getDoubleValue(lineId, 195, beginDate,endDate);
        Double avgFixTime = getDoubleValue(lineId, 196, beginDate,endDate);
        Double reduceMileageByFailure = getDoubleValue(lineId, 199, beginDate,endDate);
        Double normalMileage = getDoubleValue(lineId, 200, beginDate,endDate);

        Double resultVal = DoubleUtil.add(
                failureRate,
                DoubleUtil.safeDiv(
                        DoubleUtil.sub(normalMileage, reduceMileageByFailure),
                        normalMileage
                ),
                DoubleUtil.safeDiv(1, avgFixTime)
        );

        return genResult(resultVal, lineId, beginDate,endDate);
    }

}
