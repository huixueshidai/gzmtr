package com.stylefeng.guns.modular.yjya.controller;

import com.stylefeng.guns.common.persistence.dao.AlarmMapper;
import com.stylefeng.guns.core.base.controller.BaseController;
import com.stylefeng.guns.core.shiro.ShiroKit;
import com.stylefeng.guns.core.support.BeanKit;
import com.stylefeng.guns.modular.yjya.warpper.AlarmWrapper;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.beans.factory.annotation.Autowired;
import com.stylefeng.guns.core.log.LogObjectHolder;
import org.springframework.web.bind.annotation.RequestParam;
import com.stylefeng.guns.common.persistence.model.Alarm;
import com.stylefeng.guns.modular.yjya.service.IAlarmService;

import java.util.HashMap;
import java.util.Map;

/**
 * 报警管理控制器
 *
 * @author wsk
 * @Date 2018-07-10 17:02:43
 */
@Controller
@RequestMapping("/alarm")
public class AlarmController extends BaseController {

    private String PREFIX = "/yjya/alarm/";

    @Autowired
    private IAlarmService alarmService;

    @Autowired
    private AlarmMapper alarmMapper;

    /**
     *报警页面跳转
     */
    @RequestMapping("/alarm_editer")
    public String alarmEditer(Model model) {
        Alarm alarm = alarmMapper.selectOneAlarm();
        if(alarm!=null){
            Map<String, Object> alarmMap = BeanKit.beanToMap(alarm);
            model.addAttribute("item",new AlarmWrapper(alarmMap).warp());
        }
        else{
            Alarm alarm1 = new Alarm();
            Map<String, Object> alarmMap = BeanKit.beanToMap(alarm1);
            model.addAttribute("item",new AlarmWrapper(alarmMap).warp());
        }

        return PREFIX + "alarm_editer.html";
    }

    /**
     * 接警信息提交数据
     */
    @RequestMapping("/alarm_editerupdate")
    @ResponseBody
    public Object alarmUpdate(Alarm alarm) {
        //获取当前操作用户
        Integer userId = ShiroKit.getUser().getId();
        alarm.setUserId(userId);
        alarm.setStep(1);
        alarmService.insert(alarm);
        return  super.SUCCESS_TIP;
    }

    /**
     * 跳转到报警管理首页
     */
    @RequestMapping("")
    public String index() {
        return PREFIX + "alarm.html";
    }

    /**
     * 跳转到添加报警管理
     */
    @RequestMapping("/alarm_add")
    public String alarmAdd() {
        return PREFIX + "alarm_add.html";
    }

    /**
     * 跳转到修改报警管理
     */
    @RequestMapping("/alarm_update/{alarmId}")
    public String alarmUpdate(@PathVariable Integer alarmId, Model model) {
        Alarm alarm = alarmService.selectById(alarmId);
        model.addAttribute("item",alarm);
        LogObjectHolder.me().set(alarm);
        return PREFIX + "alarm_edit.html";
    }

    /**
     * 获取报警管理列表
     */
    @RequestMapping(value = "/list")
    @ResponseBody
    public Object list(String condition) {
        return alarmService.selectList(null);
    }

    /**
     *
     */
    @RequestMapping("/updateAlarm")
    @ResponseBody
    public Object updateAlarm(Alarm alarm){

        Alarm alarmOld = alarmMapper.selectOneAlarm();
        if(alarmOld!=null){
            alarmOld.setInterruptionType(alarm.getInterruptionType());
            alarmOld.setRetentionTime(alarm.getRetentionTime());
            alarmOld.setInterruptionTime(alarm.getInterruptionTime());
            alarmOld.setExpectDelays(alarm.getExpectDelays());
            alarmOld.setLossEfficiencyRetention(alarm.getLossEfficiencyRetention());
            alarmOld.setLossEfficiencyRransportation(alarm.getLossEfficiencyRransportation());
            alarmOld.setStep(2);
            alarmService.updateById(alarmOld);
        }
        else{
            Map<String,Object> map= new HashMap<>();
        }
        return super.SUCCESS_TIP;
    }

    /**
     * 接警页面的启动按钮提交
     */
    @RequestMapping("/updateAlarmBus")
    @ResponseBody
    public Object updateAlarmBus(Alarm alarm){
        Alarm alarmOld = alarmMapper.selectOneAlarm();
        if(alarmOld!=null){
            alarmOld.setSchedulingMode(alarm.getSchedulingMode());
            alarmOld.setSchedulingModeMin(alarm.schedulingModeMin);
            alarmOld.setBusStep(0);
            alarmService.updateById(alarmOld);
        }
        else{
            Map<String,Object> map= new HashMap<>();
        }
        return super.SUCCESS_TIP;
    }

    /**
     * 处警提交
     */
    @RequestMapping("/updateAlarmTwo")
    @ResponseBody
    public Object updateAlarmTwo(Alarm alarm){
//获取当前系统用户id
        Integer userId = ShiroKit.getUser().getId();
        //查询当前用户是否有正在应急处置的事故
        Alarm alarmOld = alarmMapper.selectByUserId(userId);
        if(alarmOld!=null){
            alarmOld.setCounterplanId(alarm.getCounterplanId());
            alarmOld.setCounterplanType(alarm.getCounterplanType());
            alarmOld.setStep(2);
            alarmService.updateById(alarmOld);
        }
        else{
            Map<String,Object> map= new HashMap<>();
        }
        return super.SUCCESS_TIP;
    }

    /**
     * 编制处置流程提交
     */
    @RequestMapping("/updateAlarmThree")
    @ResponseBody
    public Object updateAlarmThree(Alarm alarm){

        Integer userId = ShiroKit.getUser().getId();
        //查询当前用户是否有正在应急处置的事故
        Alarm alarmOld = alarmMapper.selectByUserId(userId);
        if(alarmOld!=null){
            alarmOld.setProcessJson(alarm.getProcessJson());
            alarmOld.setStep(3);
            alarmService.updateById(alarmOld);
        }
        else{
            Map<String,Object> map= new HashMap<>();
        }
        return super.SUCCESS_TIP;
    }

    /**
     * 编制处置流程提交
     */
    @RequestMapping("/processJson")
    @ResponseBody
    public Object processJson(){

        Integer userId = ShiroKit.getUser().getId();
        //查询当前用户是否有正在应急处置的事故
        Alarm alarm = alarmMapper.selectByUserId(userId);

        return alarm;
    }


    /**
     * 处置阶段操作步骤id提交
     */
    @RequestMapping("/updateAlarmdispositionStep")
    @ResponseBody
    public Object updateAlarmdispositionStep(Alarm alarm){

        Integer userId = ShiroKit.getUser().getId();
        //查询当前用户是否有正在应急处置的事故
        Alarm alarmOld = alarmMapper.selectByUserId(userId);
        if(alarmOld!=null){
            if(alarmOld!=null){
                String dispositionStep =  alarmOld.getDispositionStep()+","+alarm.getDispositionStep();
                alarmOld.setDispositionStep(dispositionStep);
            }
            else{
                alarmOld.setDispositionStep(alarm.getDispositionStep());
            }
            alarmOld.setDispositionStepTxt(alarm.getDispositionStepTxt());
            alarmService.updateById(alarmOld);
        }
        else{
            Map<String,Object> map= new HashMap<>();
        }
        return super.SUCCESS_TIP;
    }

    /**
     * 公交应急联动启动页面额启动按钮点击
     */
    @RequestMapping("/updateAlarmBusStar")
    @ResponseBody
    public Object updateAlarmBusStar(){

        Alarm alarm = alarmMapper.selectOneAlarm();
        if(alarm!=null){
            alarm.setBusStep(1);
            alarmService.updateById(alarm);
        }
        else{
            Map<String,Object> map= new HashMap<>();
        }
        return super.SUCCESS_TIP;
    }

    /**
     * 公交应急联动执行页面启动按钮点击
     */
    @RequestMapping("/updateBusExecute")
    @ResponseBody
    public Object updateBusExecute(Alarm alarm){

        Alarm alarmOld = alarmMapper.selectOneAlarm();
        if(alarm!=null){
            alarmOld.setBusStep(2);
            alarmOld.setStationAmount(alarm.getStationAmount());
            alarmOld.setStartStation(alarm.getStartStation());
            alarmOld.setEndStation(alarm.getEndStation());
            alarmService.updateById(alarmOld);
        }
        else{
            Map<String,Object> map= new HashMap<>();
        }
        return super.SUCCESS_TIP;
    }

    /**
     * 新增报警管理
     */
    @RequestMapping(value = "/add")
    @ResponseBody
    public Object add(Alarm alarm) {
        alarm.setStep(1);
        alarmService.insert(alarm);
        return super.SUCCESS_TIP;
    }

    /**
     * 删除报警管理
     */
    @RequestMapping(value = "/delete")
    @ResponseBody
    public Object delete() {
        Integer userId = ShiroKit.getUser().getId();
        //查询当前用户是否有正在应急处置的事故
        Alarm alarm = alarmMapper.selectByUserId(userId);
        if(alarm!=null){
            alarmService.deleteById(alarm.getId());
            return SUCCESS_TIP;
        }
        else{
            return SUCCESS_TIP;
        }

    }

    /**
     * 修改报警管理
     */
    @RequestMapping(value = "/update")
    @ResponseBody
    public Object update(Alarm alarm) {
        alarmService.updateById(alarm);
        return super.SUCCESS_TIP;
    }


    /**
     * 报警管理详情
     */
    @RequestMapping(value = "/detail/{alarmId}")
    @ResponseBody
    public Object detail(@PathVariable("alarmId") Integer alarmId) {
        return alarmService.selectById(alarmId);
    }
}
