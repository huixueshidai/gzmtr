/**
 * Created by 60132 on 2018/7/25.
 */
/**
 * 系统管理--用户管理的单例对象
 */
var MgrUserR = {
    id: "managerTable",//表格id
    seItem: null,		//选中的条目
    table: null,
    layerIndex: -1,
    deptid:0
};

/**
 * 初始化表格的列
 */
MgrUserR.initColumn = function () {
    var columns = [
        {field: 'selectItem', checkout: true},
        {title: 'id', field: 'id', visible: false, align: 'center', valign: 'middle'},
        {title: '账号', field: 'account', align: 'center', valign: 'middle', sortable: true},
        {title: '姓名', field: 'name', align: 'center', valign: 'middle', sortable: true},
        {title: '性别', field: 'sexName', align: 'center', valign: 'middle', sortable: true},
        {title: '角色', field: 'roleName', align: 'center', valign: 'middle', sortable: true},
        {title: '部门', field: 'deptName', align: 'center', valign: 'middle', sortable: true},
        {title: '邮箱', field: 'email', align: 'center', valign: 'middle', sortable: true},
        {title: '电话', field: 'phone', align: 'center', valign: 'middle', sortable: true},
        {title: '创建时间', field: 'createtime', align: 'center', valign: 'middle', sortable: true},
        {title: '状态', field: 'statusName', align: 'center', valign: 'middle', sortable: true}];
    return columns;
};

/**
 * 检查是否选中
 */
MgrUserR.check = function () {
    var selected = $('#' + this.id).bootstrapTable('getSelections');
    var selects = new Array();
    if (selected.length == 0) {
        Feng.info("请先选择接收通知用户！");
        return false;
    } else {
        for(var i=0;i<selected.length;i++){
            selects[i] = selected[i].id;
        }
        MgrUserR.seItem = selects.join(",");
        return true;
    }
};

/**
 * 点击添加管理员
 */
MgrUserR.openAddMgr = function () {
    var index = layer.open({
        type: 2,
        title: '添加管理员',
        area: ['800px', '560px'], //宽高
        fix: false, //不固定
        maxmin: true,
        content: Feng.ctxPath + '/mgr/user_add'
    });
    this.layerIndex = index;
};

/**
 * 点击修改按钮时
 * @param userId 管理员id
 */
MgrUserR.openChangeUser = function () {
    if (this.check()) {
        var index = layer.open({
            type: 2,
            title: '编辑管理员',
            area: ['800px', '450px'], //宽高
            fix: false, //不固定
            maxmin: true,
            content: Feng.ctxPath + '/mgr/user_edit/' + this.seItem.id
        });
        this.layerIndex = index;
    }
};

/**
 * 点击角色分配
 * @param
 */
MgrUserR.roleAssign = function () {
    if (this.check()) {
        var index = layer.open({
            type: 2,
            title: '角色分配',
            area: ['300px', '400px'], //宽高
            fix: false, //不固定
            maxmin: true,
            content: Feng.ctxPath + '/mgr/role_assign/' + this.seItem.id
        });
        this.layerIndex = index;
    }
};

/**
 * 删除用户
 */
MgrUserR.delMgrUser = function () {
    if (this.check()) {

        var operation = function(){
            var userId = MgrUserR.seItem.id;
            var ajax = new $ax(Feng.ctxPath + "/mgr/delete", function () {
                Feng.success("删除成功!");
                MgrUserR.table.refresh();
            }, function (data) {
                Feng.error("删除失败!" + data.responseJSON.message + "!");
            });
            ajax.set("userId", userId);
            ajax.start();
        };

        Feng.confirm("是否删除用户" + MgrUserR.seItem.account + "?",operation);
    }
};

/**
 * 冻结用户账户
 * @param userId
 */
MgrUserR.freezeAccount = function () {
    if (this.check()) {
        var userId = this.seItem.id;
        var ajax = new $ax(Feng.ctxPath + "/mgr/freeze", function (data) {
            Feng.success("冻结成功!");
            MgrUserR.table.refresh();
        }, function (data) {
            Feng.error("冻结失败!" + data.responseJSON.message + "!");
        });
        ajax.set("userId", userId);
        ajax.start();
    }
};

/**
 * 解除冻结用户账户
 * @param userId
 */
MgrUserR.unfreeze = function () {
    if (this.check()) {
        var userId = this.seItem.id;
        var ajax = new $ax(Feng.ctxPath + "/mgr/unfreeze", function (data) {
            Feng.success("解除冻结成功!");
            MgrUserR.table.refresh();
        }, function (data) {
            Feng.error("解除冻结失败!");
        });
        ajax.set("userId", userId);
        ajax.start();
    }
}

/**
 * 重置密码
 */
MgrUserR.resetPwd = function () {
    if (this.check()) {
        var userId = this.seItem.id;
        parent.layer.confirm('是否重置密码为111111？', {
            btn: ['确定', '取消'],
            shade: false //不显示遮罩
        }, function () {
            var ajax = new $ax(Feng.ctxPath + "/mgr/reset", function (data) {
                Feng.success("重置密码成功!");
            }, function (data) {
                Feng.error("重置密码失败!");
            });
            ajax.set("userId", userId);
            ajax.start();
        });
    }
};

MgrUserR.resetSearch = function () {
    $("#name").val("");
    $("#beginTime").val("");
    $("#endTime").val("");

    MgrUserR.search();
}

MgrUserR.search = function () {
    var queryData = {};

    queryData['deptid'] = MgrUserR.deptid;
    queryData['name'] = $("#name").val();
    queryData['beginTime'] = $("#beginTime").val();
    queryData['endTime'] = $("#endTime").val();

    MgrUserR.table.refresh({query: queryData});
}

MgrUserR.onClickDept = function (e, treeId, treeNode) {
    MgrUserR.deptid = treeNode.id;
    MgrUserR.search();
};

MgrUserR.close = function(){
    parent.layer.close(window.parent.AccidentInfoDlg.layerIndex);
}

/**
 * 接警汇报发送的消息模板
 */
MgrUserR.contentSend = function () {

    var content = window.parent.AccidentInfoDlg.accidentInfo;
    return content;
};


/**
 * 发送消息页面
 */
MgrUserR.send = function(){
    if(this.check()){
        if(typeof(window.parent.AccidentInfoDlg) === "undefined"){
            var index = layer.open({
                type: 2,
                title: '编辑信息',
                area: ['800px', '420px'], //宽高
                fix: false, //不固定
                maxmin: true,
                content: Feng.ctxPath + '/notice/notice_add/'+MgrUserR.seItem
            });
            this.layerIndex = index;
        }
        else if(window.parent.AccidentInfoDlg.currentIndex==0){
            var index = layer.open({
                type: 2,
                title: '添加通知',
                area: ['800px', '420px'], //宽高
                fix: false, //不固定
                maxmin: true,
                content: Feng.ctxPath + '/notice/notice_add_accident/'+MgrUserR.seItem
            });
            this.layerIndex = index;
        }
        else{
            var index = layer.open({
                type: 2,
                title: '添加通知',
                area: ['800px', '420px'], //宽高
                fix: false, //不固定
                maxmin: true,
                content: Feng.ctxPath + '/notice/notice_add/'+MgrUserR.seItem
            });
            this.layerIndex = index;
        }

    }
}

/**
 * 发送消息给上级
 */
MgrUserR.senddTo = function(){

}


$(function () {
    var defaultColunms = MgrUserR.initColumn();
    var table = new BSTable("managerTable", "/mgr/list", defaultColunms);
    table.setPaginationType("client");
    MgrUserR.table = table.init();
    var ztree = new $ZTree("deptTree", "/dept/tree");
    ztree.bindOnClick(MgrUserR.onClickDept);
    ztree.init();
});
