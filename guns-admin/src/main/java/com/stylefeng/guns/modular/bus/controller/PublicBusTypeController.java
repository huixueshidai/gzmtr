package com.stylefeng.guns.modular.bus.controller;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.mapper.Wrapper;
import com.stylefeng.guns.common.persistence.model.Law;
import com.stylefeng.guns.core.base.controller.BaseController;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.beans.factory.annotation.Autowired;
import com.stylefeng.guns.core.log.LogObjectHolder;
import org.springframework.web.bind.annotation.RequestParam;
import com.stylefeng.guns.common.persistence.model.PublicBusType;
import com.stylefeng.guns.modular.bus.service.IPublicBusTypeService;

/**
 * 公交车类型控制器
 *
 * @author yinjc
 * @Date 2018-03-21 11:37:27
 */
@Controller
@RequestMapping("/publicBusType")
public class PublicBusTypeController extends BaseController {

    private String PREFIX = "/bus/publicBusType/";
    @Autowired
    private IPublicBusTypeService publicBusTypeService;

    /**
     * 跳转到公交车类型首页
     */
    @RequestMapping("")
    public String index() {
        return PREFIX + "publicBusType.html";
    }
    /**
     * 跳转到添加公交车类型
     */
    @RequestMapping("/publicBusType_add")
    public String publicBusTypeAdd() {
        return PREFIX + "publicBusType_add.html";
    }
    /**
     * 跳转到修改公交车类型
     */
    @RequestMapping("/publicBusType_update/{publicBusTypeId}")
    public String publicBusTypeUpdate(@PathVariable Integer publicBusTypeId, Model model) {
        PublicBusType publicBusType = publicBusTypeService.selectById(publicBusTypeId);
        model.addAttribute("item",publicBusType);
        LogObjectHolder.me().set(publicBusType);
        return PREFIX + "publicBusType_edit.html";
    }
    /**
     * 获取公交车类型列表
     */
    @RequestMapping(value = "/list")
    @ResponseBody
    public Object list(String condition) {
        com.baomidou.mybatisplus.mapper.Wrapper<PublicBusType> wrapper = new EntityWrapper<>();
        wrapper.like("bus_type_name",condition);
        return publicBusTypeService.selectList(wrapper);
    }

    /**
     * 新增公交车类型
     */
    @RequestMapping(value = "/add")
    @ResponseBody
    public Object add(PublicBusType publicBusType) {
        publicBusTypeService.insert(publicBusType);
        return super.SUCCESS_TIP;
    }

    /**
     * 删除公交车类型
     */
    @RequestMapping(value = "/delete")
    @ResponseBody
    public Object delete(@RequestParam Integer publicBusTypeId) {
        publicBusTypeService.deleteById(publicBusTypeId);
        return SUCCESS_TIP;
    }

    /**
     * 修改公交车类型
     */
    @RequestMapping(value = "/update")
    @ResponseBody
    public Object update(PublicBusType publicBusType) {
        publicBusTypeService.updateById(publicBusType);
        return super.SUCCESS_TIP;
    }

    /**
     * 公交车类型详情
     */
    @RequestMapping(value = "/detail/{publicBusTypeId}")
    @ResponseBody
    public Object detail(@PathVariable("publicBusTypeId") Integer publicBusTypeId) {
        return publicBusTypeService.selectById(publicBusTypeId);
    }
}
