/**
 * 事故处理措施管理初始化
 */
var AccidentProcess = {
    id: "AccidentProcessTable",	//表格id
    seItem: null,		//选中的条目
    selItems:[],      //选中的多条数目
    table: null,
    layerIndex: -1,
    typeId:0,
    uploadInstance:null
};

/**
 * 初始化表格的列
 */
AccidentProcess.initColumn = function () {
    return [
        {field: 'selectItem', checkbox: true},
            {title: 'id', field: 'id', visible: true, align: 'center', valign: 'middle'},
            {title: '主体', field: 'main', visible: true, align: 'center', valign: 'middle'},
            {title: '主体属性', field: 'mproperty', visible: true, align: 'center', valign: 'middle'},
            {title: '主体动作', field: 'mmotion', visible: true, align: 'center', valign: 'middle'},

            {title: '客体', field: 'object', visible: true, align: 'center', valign: 'middle'},
        {title: '客体属性', field: 'oproperty', visible: true, align: 'center', valign: 'middle'},
            {title: '客体动作', field: 'omotion', visible: true, align: 'center', valign: 'middle'},
            {title: '分组', field: 'typeName', visible: true, align: 'center', valign: 'middle'}
    ];
};

/**
 * 检查是否选中
 */
AccidentProcess.check = function () {
    var selected = $('#' + this.id).bootstrapTable('getSelections');
    if(selected.length == 0){
        Feng.info("请先选中表格中的某一记录！");
        return false;
    }else{
        AccidentProcess.seItem = selected[0];
        AccidentProcess.selItems = selected;
        return true;
    }
};

/**
 * 点击添加事故处理措施
 */
AccidentProcess.openAddAccident = function () {
    var index = layer.open({
        type: 2,
        title: '添加事故处理措施',
        area: ['800px', '420px'], //宽高
        fix: false, //不固定
        maxmin: true,
        content: Feng.ctxPath + '/accidentProcess/accidentProcess_add'
    });
    this.layerIndex = index;
    layer.full(index);
};

/**
 * 打开查看事故处理措施详情
 */
AccidentProcess.openAccidentDetail = function () {
    if (this.check()) {
        var index = layer.open({
            type: 2,
            title: '事故处理措施详情',
            area: ['800px', '420px'], //宽高
            fix: false, //不固定
            maxmin: true,
            content: Feng.ctxPath + '/accidentProcess/accidentProcess_update/' + AccidentProcess.seItem.id
        });
        this.layerIndex = index;
        layer.full(index);
    }
};

/**
 * 删除事故处理措施
 */
AccidentProcess.delete = function () {
    if (this.check()) {
        var seItems =  AccidentProcess.selItems;
        var delIds = [];
        seItems.forEach(function(element){
            delIds.push(element.id);
        });
        var operation = function(){
            var ajax = new $ax(Feng.ctxPath + "/accidentProcess/delete", function () {
                Feng.success("删除成功!");
                AccidentProcess.table.refresh();
            }, function (data) {
                Feng.error("删除失败!" + data.responseJSON.message + "!");
            });
            ajax.set("ids", delIds);
            ajax.start();
        };
        Feng.confirm("是否删除选中的事故处置措施?" ,operation);
    }
};

/**
 * 查询事故处理措施列表
 */
AccidentProcess.search = function () {
    var queryData = {};
    queryData['typeId'] = AccidentProcess.typeId;
    queryData['condition'] = $("#condition").val();
    AccidentProcess.table.refresh({query: queryData});
};

AccidentProcess.onClickContactGroup = function (e, treeId, treeNode) {
    AccidentProcess.typeId = treeNode.id;
    AccidentProcess.search();
}

/**
 * 下载模板
 */
AccidentProcess.exportXlsByM = function () {
    window.location.href = Feng.ctxPath + "/accidentProcess/exportXlsByM";
}

AccidentProcess.upload = function () {
    this.uploadInstance;
    AccidentProcess.table.refresh();
}

$(function () {
    var defaultColunms = AccidentProcess.initColumn();
    var table = new BSTable(AccidentProcess.id, "/accidentProcess/list", defaultColunms);
    table.setPaginationType("server");
    AccidentProcess.table = table.init();

    var ztree = new $ZTree("accidentGroupTree", "/accidentType/tree");
    ztree.bindOnClick(AccidentProcess.onClickContactGroup);
    ztree.init();

    var upload = new $WebUpload({
        uploadId:"upload",
        mimeTypes:"application/vnd.ms-excel",
        extensions:"xls",
        url:"/accidentProcess/importExcel",
        multiple:true
    });
    upload.init();
    AccidentProcess.uploadInstance = upload;
});
