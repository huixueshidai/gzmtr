package com.stylefeng.guns.modular.assessment.calculate.station.passenger;

import com.stylefeng.guns.common.persistence.model.Assessment;
import com.stylefeng.guns.core.util.DoubleUtil;
import com.stylefeng.guns.modular.assessment.calculate.ThirdIndicationCalculator;
import com.stylefeng.guns.modular.assessment.calculate.aware.StationThirdIndicationAware;
import org.springframework.stereotype.Component;

import java.util.Date;

/**
 * 站台拥挤度
 */
@Component
public class StationZhantaiCal extends ThirdIndicationCalculator implements StationThirdIndicationAware {

    @Override
    protected Integer getIndicationId() {
        return 11;
    }

    @Override
    public Assessment calculate(Integer stationId, Date beginDate, Date endDate) {
        Double doorWidth = getDoubleValue(stationId, 11, beginDate,endDate);
        Double availabilityZhanTaiWidth = getDoubleValue(stationId, 10, beginDate,endDate);
        Double Sa = DoubleUtil.mul(doorWidth, availabilityZhanTaiWidth);

        //车门数量
        Double doorNum = getDoubleValue(stationId, 273, beginDate,endDate);
        //列车总长度
        Double totolWidth = getDoubleValue(stationId, 12, beginDate,endDate);
        Double Sb = DoubleUtil.mul(availabilityZhanTaiWidth, DoubleUtil.safeDiv(DoubleUtil.sub(totolWidth, DoubleUtil.mul(doorWidth, doorNum)), DoubleUtil.sub(doorNum, 1)));

        Double denseAreaPassengerNum = getDoubleValue(stationId, 8, beginDate,endDate);
        Double thinAreaPassengerNum = getDoubleValue(stationId, 9, beginDate,endDate);

        Double weightDenominator = DoubleUtil.add(
                DoubleUtil.safeDiv(denseAreaPassengerNum, Sa),
                DoubleUtil.safeDiv(thinAreaPassengerNum, Sb)
        );

        Double denseAreaWeight = DoubleUtil.safeDiv(
                DoubleUtil.safeDiv(denseAreaPassengerNum, Sa),
                weightDenominator
        );
        Double thinAreaWeight = DoubleUtil.safeDiv(
                DoubleUtil.safeDiv(thinAreaPassengerNum, Sb)
                , weightDenominator
        );

        double resultValue = DoubleUtil.add(
                DoubleUtil.mul(denseAreaWeight, DoubleUtil.safeDiv(denseAreaPassengerNum, Sa)),
                DoubleUtil.mul(thinAreaWeight, DoubleUtil.safeDiv(thinAreaPassengerNum, Sb))
        );
        return genResult(resultValue, stationId, beginDate,endDate);
    }
}
