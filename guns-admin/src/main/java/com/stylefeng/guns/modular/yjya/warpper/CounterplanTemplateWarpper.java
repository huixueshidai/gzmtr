package com.stylefeng.guns.modular.yjya.warpper;

import com.stylefeng.guns.common.constant.factory.ConstantFactory;
import com.stylefeng.guns.common.persistence.dao.AccidentTypeMapper;
import com.stylefeng.guns.common.persistence.dao.RoadnetMapper;
import com.stylefeng.guns.common.persistence.model.AccidentType;
import com.stylefeng.guns.common.persistence.model.Roadnet;
import com.stylefeng.guns.core.base.warpper.BaseControllerWarpper;
import com.stylefeng.guns.core.util.SpringContextHolder;
import com.vip.vjtools.vjkit.collection.MapUtil;
import org.apache.commons.collections.MapUtils;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

public class CounterplanTemplateWarpper extends BaseControllerWarpper{


    public CounterplanTemplateWarpper(Object obj) {
        super(obj);
    }

    @Override
    protected void warpTheMap(Map<String, Object> map) {
        if(map.containsKey("type")){
            map.put("typeName", ConstantFactory.me().getDictNameByCode("counterplanType", (String) map.get("type")));
        }
        if(map.containsKey("level")){
            map.put("levelName", ConstantFactory.me().getDictNameByCode("responseLevel", (String) map.get("level")));
        }
        if(map.containsKey("stationId")){
            Integer lineId =null;
            String stationIds = MapUtils.getString(map,"stationId");
            List<Integer> stationIdList = new ArrayList(Arrays.asList(stationIds.split(",")));
            List<Roadnet> roadnetList = new ArrayList<>();
            String stationName = "";
            for(int i=0;i<stationIdList.size();i++){
                Roadnet station = SpringContextHolder.getBean(RoadnetMapper.class).selectById(stationIdList.get(i));
                roadnetList.add(station);
            }
            for(int i=0;i<roadnetList.size();i++){
                if(i==roadnetList.size()-1){
                    stationName += roadnetList.get(i).getName();
                    lineId = roadnetList.get(i).getPid();
                }
                else{
                    stationName += roadnetList.get(i).getName()+">>";
                }

            }
            Roadnet line = SpringContextHolder.getBean(RoadnetMapper.class).selectById(lineId);
            map.put("stationName",line.getName()+":"+stationName);
        }
        AccidentType accidentType = SpringContextHolder.getBean(AccidentTypeMapper.class).selectById((Serializable) map.get("accidentTypeId"));
        if (accidentType != null){
            String pids = accidentType.getPids();
            List<String> ids = new ArrayList<>(Arrays.asList(pids.split("/")));
            ids.add(String.valueOf(accidentType.getId()));
            List<AccidentType> accidentTypeList =  SpringContextHolder.getBean(AccidentTypeMapper.class).selectBatchIds(ids);
            String accideentTypeName = "";
            for(int i=1;i<accidentTypeList.size();i++){
                accideentTypeName += accidentTypeList.get(i).getName()+"--";
            }
            map.put("accidentTypeName", accideentTypeName+accidentTypeList.get(0).getName());
        }
        return;
    }
}
