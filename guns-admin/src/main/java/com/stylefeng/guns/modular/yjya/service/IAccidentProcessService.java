package com.stylefeng.guns.modular.yjya.service;

import com.baomidou.mybatisplus.plugins.Page;
import com.stylefeng.guns.common.persistence.model.AccidentProcess;
import com.baomidou.mybatisplus.service.IService;
import com.stylefeng.guns.common.persistence.model.OperationLog;

import java.util.List;
import java.util.Map;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author 殷佳成
 * @since 2018-01-17
 */
public interface IAccidentProcessService extends IService<AccidentProcess> {
//    List<Map<String,Object>> selectTrdIndications(Page<AccidentProcess> page, String condition, Integer typeId);
}
