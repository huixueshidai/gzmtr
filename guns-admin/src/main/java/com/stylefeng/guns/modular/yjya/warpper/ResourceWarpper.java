package com.stylefeng.guns.modular.yjya.warpper;

import com.stylefeng.guns.core.base.warpper.BaseControllerWarpper;
import com.stylefeng.guns.core.support.DateTimeKit;

import java.util.Date;
import java.util.Map;

public class ResourceWarpper extends BaseControllerWarpper {
    public ResourceWarpper(Object o) {
        super(o);
    }

    @Override
    protected void warpTheMap(Map<String, Object> map) {
        Date date = (Date) map.get("updateTime");
        map.put("updateTime", DateTimeKit.formatDate(date));
    }

}
