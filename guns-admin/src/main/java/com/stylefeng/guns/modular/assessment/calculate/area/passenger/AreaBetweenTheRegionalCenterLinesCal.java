package com.stylefeng.guns.modular.assessment.calculate.area.passenger;

import com.stylefeng.guns.common.persistence.model.Assessment;
import com.stylefeng.guns.core.util.DoubleUtil;
import com.stylefeng.guns.modular.assessment.calculate.ThirdIndicationCalculator;
import com.stylefeng.guns.modular.assessment.calculate.aware.AreaThirdIndicationAware;
import org.springframework.stereotype.Component;

import java.util.Date;
import java.util.List;

/**
 * 区域中心线路间能力匹配度
 *
 * @author yinjc
 * @since 2018/05/11
 */
@Component
public class AreaBetweenTheRegionalCenterLinesCal extends ThirdIndicationCalculator implements AreaThirdIndicationAware {

    @Override
    protected Integer getIndicationId() {
        return 78;
    }

    @Override
    public Assessment calculate(Integer areaId, Date beginDate, Date endDate) {
        //区段断面满载率
        List<Double> a = getMulDoubleValue(areaId, 282, beginDate,endDate);
        //区段前一区段断面满载率
        List<Double> b = getMulDoubleValue(areaId, 283, beginDate,endDate);
        //换乘站间各换乘方向换乘量占线路换乘总量的比值
        List<Double> k = getMulDoubleValue(areaId, 284, beginDate,endDate);
        //区段断面满载率权重
        Double w1 = getDoubleValue(areaId, 285, beginDate,endDate);
        //段前一区段断面满载率权重
        Double w2 = getDoubleValue(areaId, 286, beginDate,endDate);

        Integer minSize = Math.min(Math.min(a.size(), b.size()), k.size());
        minSize = Math.min(minSize, k.size());
        Double resVal = 0d;
        for (int i = 0; i < minSize; i++) {
            double m = DoubleUtil.mul(w1, a.get(i));
            double m2 = DoubleUtil.mul(w2, DoubleUtil.sub(a.get(i), b.get(i)));
            double val = DoubleUtil.mul(k.get(i), DoubleUtil.add(m, m2));
            resVal = DoubleUtil.add(val, resVal);
        }
        return genResult(resVal, areaId, beginDate,endDate);
    }
}
