package com.stylefeng.guns.modular.assessment.calculate.road.facility;

import com.stylefeng.guns.core.util.SpringContextHolder;
import com.stylefeng.guns.modular.assessment.calculate.SecondIndicationCalculator;
import com.stylefeng.guns.modular.assessment.calculate.ThirdIndicationCalculator;
import com.stylefeng.guns.modular.assessment.calculate.aware.RoadnetSecondIndicationAware;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import java.util.Arrays;
import java.util.List;

@Component
@Scope(ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public class RoadFacilityCal extends SecondIndicationCalculator implements RoadnetSecondIndicationAware {

    private List<ThirdIndicationCalculator> thirdIndicationCalculatorList;

    public RoadFacilityCal() {
        thirdIndicationCalculatorList = Arrays.asList(
                SpringContextHolder.getBean(RoadLowVoltageDistributionCal.class),
                SpringContextHolder.getBean(RoadLpactOfVehicleSystemCal.class),
                SpringContextHolder.getBean(RoadPingBiMenCal.class),
                SpringContextHolder.getBean(RoadOtherCal.class),
                SpringContextHolder.getBean(RoadSignalCal.class),
                SpringContextHolder.getBean(RoadTongXinCal.class),
                SpringContextHolder.getBean(RoadTractionPowerSupplyCal.class),
                SpringContextHolder.getBean(RoadTuJianCal.class),
                SpringContextHolder.getBean(RoadXianLuCal.class)
        );
    }

    @Override
    public List<ThirdIndicationCalculator> getThirdIndicationCalculatorList() {
        return thirdIndicationCalculatorList;
    }

    @Override
    protected Integer getIndicationId() {
        return 92;
    }
}
