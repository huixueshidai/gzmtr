/**
 * 岗位职责管理初始化
 */
var PositionStatement = {
    id: "PositionStatementTable",	//表格id
    seItem: null,		//选中的条目
    table: null,
    layerIndex: -1
};

/**
 * 初始化表格的列
 */
PositionStatement.initColumn = function () {
    return [
        {field: 'selectItem', radio: true},
            {title: 'id', field: 'id', visible: true, align: 'center', valign: 'middle'},
            {title: '岗位职责名称', field: 'name', visible: true, align: 'center', valign: 'middle'},


    ];
};

/**
 * 检查是否选中
 */
PositionStatement.check = function () {
    var selected = $('#' + this.id).bootstrapTreeTable('getSelections');
    if(selected.length == 0){
        Feng.info("请先选中表格中的某一记录！");
        return false;
    }else{
        PositionStatement.seItem = selected[0];
        return true;
    }
};

/**
 * 点击添加岗位职责
 */
PositionStatement.openAddPositionStatement = function () {
    var index = layer.open({
        type: 2,
        title: '添加岗位职责',
        area: ['800px', '420px'], //宽高
        fix: false, //不固定
        maxmin: true,
        content: Feng.ctxPath + '/positionStatement/positionStatement_add'
    });
    this.layerIndex = index;
};

/**
 * 打开查看岗位职责详情
 */
PositionStatement.openPositionStatementDetail = function () {
    if (this.check()) {
        var index = layer.open({
            type: 2,
            title: '岗位职责详情',
            area: ['800px', '420px'], //宽高
            fix: false, //不固定
            maxmin: true,
            content: Feng.ctxPath + '/positionStatement/positionStatement_update/' + PositionStatement.seItem.id
        });
        this.layerIndex = index;
    }
};

/**
 * 删除岗位职责
 */
PositionStatement.delete = function () {
    if (this.check()) {
        var ajax = new $ax(Feng.ctxPath + "/positionStatement/delete", function (data) {
            Feng.success("删除成功!");
            PositionStatement.table.refresh();
        }, function (data) {
            Feng.error("删除失败!" + data.responseJSON.message + "!");
        });
        ajax.set("positionStatementId",this.seItem.id);
        ajax.start();
    }
};

/**
 * 查询岗位职责列表
 */
PositionStatement.search = function () {
    var queryData = {};
    queryData['condition'] = $("#condition").val();
    PositionStatement.table.refresh({query: queryData});
};

$(function () {
    var defaultColunms = PositionStatement.initColumn();
    var table = new BSTreeTable(PositionStatement.id, "/positionStatement/list", defaultColunms);
    table.setExpandColumn(2);
    table.setIdField("id");
    table.setCodeField("id");
    table.setParentCodeField("pid");
    table.setExpandAll(true);
    table.init();
    PositionStatement.table = table;
});
