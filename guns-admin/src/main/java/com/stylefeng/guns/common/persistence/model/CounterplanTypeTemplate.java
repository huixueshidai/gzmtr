package com.stylefeng.guns.common.persistence.model;

import java.io.Serializable;

import com.baomidou.mybatisplus.enums.IdType;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableName;
import java.io.Serializable;

/**
 * <p>
 * 预案类型模板
 * </p>
 *
 * @author xuziyang
 * @since 2018-01-18
 */
@TableName("gzmtr_counterplan_type_template")
public class CounterplanTypeTemplate extends Model<CounterplanTypeTemplate> {

    private static final long serialVersionUID = 1L;

    /**
     * 编码
     */
	@TableId(value="id", type= IdType.AUTO)
	private Integer id;
    /**
     * 类别
     */
	private String type;
    /**
     * 内容
     */
	private String content;


	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	@Override
	protected Serializable pkVal() {
		return this.id;
	}

	@Override
	public String toString() {
		return "CounterplanTypeTemplate{" +
			"id=" + id +
			", type=" + type +
			", content=" + content +
			"}";
	}
}
