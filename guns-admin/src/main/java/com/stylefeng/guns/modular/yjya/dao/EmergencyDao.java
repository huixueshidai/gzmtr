package com.stylefeng.guns.modular.yjya.dao;

import com.stylefeng.guns.core.node.ZTreeNode;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

/**
 * Created by 60132 on 2018/5/24.
 */
public interface EmergencyDao {
    String getNameById(Integer id);

    List<ZTreeNode> tree();

    List<Map<String, Object>> list(@Param("condition") String condition);
}
