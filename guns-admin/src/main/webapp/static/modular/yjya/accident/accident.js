/**
 * 事故实例管理初始化
 */
var Accident = {
    id: "AccidentTable",	//表格id
    seItem: null,		//选中的条目
    seItem: [],       //选中多条目
    table: null,
    layerIndex: -1,
    validateFields: {
        name: {
            validators: {
                notEmpty: {
                    message: '名称不能为空'
                }
            }
        }
    }
};

/**
 * 初始化表格的列
 */
Accident.initColumn = function () {
    return [
        {field: 'selectItem', checkbox: true},
            {title: '', field: 'id', visible: true, align: 'center', valign: 'middle'},
            {title: '名称', field: 'name', visible: true, align: 'center', valign: 'middle'},
            {title: '事故类型', field: 'accidentTypeName', visible: true, align: 'center', valign: 'middle'},
            {title: '事故线路', field: 'lineName', visible: true, align: 'center', valign: 'middle'},
            {title: '事故地点', field: 'stationName', visible: true, align: 'center', valign: 'middle'},
            {title: '车次', field: 'trainNumber', visible: true, align: 'center', valign: 'middle'},
            {title: '中断时间', field: 'interruptionTime', visible: true, align: 'center', valign: 'middle'},
            {title: '影响人数', field: 'trapPeoNum', visible: true, align: 'center', valign: 'middle'},
            {title: '发生时间', field: 'startTime', visible: true, align: 'center', valign: 'middle'},
            {title: '事故级别', field: 'accidentLevelName', visible: true, align: 'center', valign: 'middle'},
            {title: '中断类型', field: 'interruptionTypeName', visible: true, align: 'center', valign: 'middle'},
            {title: '处置流程', field: 'process', visible: true, align: 'center', valign: 'middle'},
        {title: '新增状态', field: 'addTypeName', visible: true, align: 'center', valign: 'middle'}
    ];
};

/**
 * 检查是否选中
 */
Accident.check = function () {
    var selected = $('#' + this.id).bootstrapTable('getSelections');
    if(selected.length == 0){
        Feng.info("请先选中表格中的某一记录！");
        return false;
    }
    else{
        Accident.seItem = selected[0];
        Accident.seItems = selected;
        return true;
    }
};

/**
 * 检查是否选中手动新增事故实例
 */
Accident.checkAddType = function () {
    var selected = $('#' + this.id).bootstrapTable('getSelections');
    if(selected.length == 0){
        Feng.info("请先选中表格中的某一记录！");
        return false;
    }
    else if(selected[0].addType==1){
        Feng.info("应急处置事故实例不可修改！");
        return false;
    }
    else{
        Accident.seItem = selected[0];
        Accident.seItems = selected;
        return true;
    }
};

/**
 * 点击添加事故实例
 */
Accident.AddAccident = function () {
    var index = layer.open({
        type: 2,
        title: '添加',
        area: ['1200px', '600px'], //宽高
        fix: false, //不固定
        maxmin: true,
        content: Feng.ctxPath + '/accident/accident_addaccident'
    });
    this.layerIndex = index;
    layer.full(index);
};


/**
 * 点击添加事故实例(接警界面)
 */
Accident.openAddAccident = function () {
    var index = layer.open({
        type: 2,
        title: '接警',
        area: ['1450px', '700px'], //宽高
        fix: false, //不固定
        maxmin: true,
        content: Feng.ctxPath + '/accident/accident_add'
    });
    this.layerIndex = index;
};


/**
 * 打开查看事故实例详情
 */
Accident.openAccidentDetail = function () {
    if (this.checkAddType()) {
        var index = layer.open({
            type: 2,
            title: '事故实例详情',
            area: ['800px', '420px'], //宽高
            fix: false, //不固定
            maxmin: true,
            content: Feng.ctxPath + '/accident/accident_update/' + Accident.seItem.id
        });
        this.layerIndex = index;
    }
    layer.full(index);
};

/**
 * 删除事故实例
 */
Accident.delete = function () {
    if (this.check()) {
        var selItems = Accident.seItems;
        var delIds = [];
        selItems.forEach(function(element){
            delIds.push(element.id);
        });
        var operation = function(){
            var ajax = new $ax(Feng.ctxPath + "/accident/delete", function () {
                Feng.success("删除成功!");
                Accident.table.refresh();
            }, function (data) {
                Feng.error("删除失败!" + data.responseJSON.message + "!");
            });
            ajax.set("ids", delIds);
            ajax.start();
        };
        Feng.confirm("是否删除选中事故数据?" ,operation);

    }

};

/**
 * 查询事故实例列表
 */
Accident.search = function () {
    var queryData = {};
    queryData['condition'] = $("#condition").val();
    Accident.table.refresh({query: queryData});
};

/**
 * 应急处置报告
 */
Accident.disposeReport = function(){
    if(this.check()){
        var index = layer.open({
            type: 2,
            title: '应急处置报告',
            area: ['1280px', '600px'], //宽高
            fix: false, //不固定
            maxmin: true,
            content: Feng.ctxPath + '/accident/dispose_report/'+ Accident.seItem.id
        });
        this.layerIndex = index;
        layer.full(index);
    }

};

$(function () {
    var defaultColunms = Accident.initColumn();
    var table = new BSTable(Accident.id, "/accident/list", defaultColunms);
    table.setPaginationType("client");
    Accident.table = table.init();
});
