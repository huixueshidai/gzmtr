package com.stylefeng.guns.common.persistence.model;

import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;
import com.baomidou.mybatisplus.enums.IdType;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.springframework.format.annotation.DateTimeFormat;

import java.io.Serializable;
import java.util.Date;

/**
 * <p>
 * 源数据
 * </p>
 *
 * @author xuziyang
 * @since 2018-01-17
 */
@TableName("gzmtr_resource_data")
@Getter
@Setter
@ToString
public class ResourceData extends Model<ResourceData> {

    private static final long serialVersionUID = 1L;

	@TableId(value="id", type= IdType.AUTO)
	private Integer id;

	@TableField("resource_data_type_id")
	private Integer resourceDataTypeId;

	/**
	 * 数据的来源：0：路网，其他：车站、线路、区域中心
	 */
	@TableField(value = "roadnet_id")
	private Integer roadnetId;

	private String value;

	@TableField("start_time")
	@DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	private Date startTime;

	@TableField("end_time")
	@DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	private Date endTime;

	/**
	 * 地铁站口
	 */
	private String entrance;


	@Override
	protected Serializable pkVal() {
		return this.id;
	}

}
