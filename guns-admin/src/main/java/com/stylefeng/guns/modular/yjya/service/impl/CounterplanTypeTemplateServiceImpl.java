package com.stylefeng.guns.modular.yjya.service.impl;

import com.stylefeng.guns.common.persistence.model.CounterplanTypeTemplate;
import com.stylefeng.guns.common.persistence.dao.CounterplanTypeTemplateMapper;
import com.stylefeng.guns.modular.yjya.service.ICounterplanTypeTemplateService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 预案类型模板 服务实现类
 * </p>
 *
 * @author xuziyang
 * @since 2018-01-18
 */
@Service
public class CounterplanTypeTemplateServiceImpl extends ServiceImpl<CounterplanTypeTemplateMapper, CounterplanTypeTemplate> implements ICounterplanTypeTemplateService {
	
}
