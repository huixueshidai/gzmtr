package com.stylefeng.guns.common.plugin.entity;

/**
 *
 */
public interface Treeable {

    Integer getId();

    void setName(String name);

    String getName();

    /**
     * 父路径
     *
     * @return
     */
    Integer getPid();

    void setPid(Integer parentId);

    /**
     * 所有父路径 如1,2,3,
     *
     * @return
     */
    String getPids();

    void setPids(String parentIds);

    /**
     * 获取 parentIds 之间的分隔符
     *
     * @return
     */
    default String getSeparator(){
        return "/";
    }

    /**
     * 把自己构造出新的父节点路径
     *
     * @return
     */
    default String makeSelfAsNewParentIds(){return getPids() + getId() + getSeparator();};


    /**
     * 权重 用于排序 越小越排在前边
     *
     * @return
     */
    Integer getNum();

    void setNum(Integer num);


}

