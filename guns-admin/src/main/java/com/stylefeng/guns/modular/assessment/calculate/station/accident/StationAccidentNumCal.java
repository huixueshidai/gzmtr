package com.stylefeng.guns.modular.assessment.calculate.station.accident;

import com.stylefeng.guns.common.persistence.model.Assessment;
import com.stylefeng.guns.core.util.DoubleUtil;
import com.stylefeng.guns.modular.assessment.calculate.ThirdIndicationCalculator;
import com.stylefeng.guns.modular.assessment.calculate.aware.StationThirdIndicationAware;
import org.springframework.stereotype.Component;

import java.util.Date;

/**
 * 车站事故次数
 */
@Component
public class StationAccidentNumCal extends ThirdIndicationCalculator implements StationThirdIndicationAware {

    @Override
    protected Integer getIndicationId() {
        return 46;
    }

    @Override
    public Assessment calculate(Integer stationId, Date beginDate, Date endDate) {
        Double resVal = 0d;
        for (int i = 143; i <= 148; i++) {
            resVal = DoubleUtil.add(resVal,getDoubleValue(stationId, i, beginDate,endDate));
        }
        return genResult(resVal, stationId, beginDate,endDate);
    }

}
