/**
 * 初始化公交车司机详情对话框
 */
var DriversInfoDlg = {
    driversInfoData : {}
};

/**
 * 清除数据
 */
DriversInfoDlg.clearData = function() {
    this.driversInfoData = {};
}

/**
 * 设置对话框中的数据
 *
 * @param key 数据的名称
 * @param val 数据的具体值
 */
DriversInfoDlg.set = function(key, val) {
    this.driversInfoData[key] = (typeof val == "undefined") ? $("#" + key).val() : val;
    return this;
}

/**
 * 设置对话框中的数据
 *
 * @param key 数据的名称
 * @param val 数据的具体值
 */
DriversInfoDlg.get = function(key) {
    return $("#" + key).val();
}

/**
 * 关闭此对话框
 */
DriversInfoDlg.close = function() {
    parent.layer.close(window.parent.Drivers.layerIndex);
}

/**
 * 收集数据
 */
DriversInfoDlg.collectData = function() {
    this
    .set('id')
    .set('name')
    .set('plateNum')
    ;
}

/**
 * 提交添加
 */
DriversInfoDlg.addSubmit = function() {

    this.clearData();
    this.collectData();

    //提交信息
    var ajax = new $ax(Feng.ctxPath + "/drivers/add", function(data){
        Feng.success("添加成功!");
        window.parent.Drivers.table.refresh();
        DriversInfoDlg.close();
    },function(data){
        Feng.error("添加失败!" + data.responseJSON.message + "!");
    });
    ajax.set(this.driversInfoData);
    ajax.start();
}

/**
 * 提交修改
 */
DriversInfoDlg.editSubmit = function() {

    this.clearData();
    this.collectData();

    //提交信息
    var ajax = new $ax(Feng.ctxPath + "/drivers/update", function(data){
        Feng.success("修改成功!");
        window.parent.Drivers.table.refresh();
        DriversInfoDlg.close();
    },function(data){
        Feng.error("修改失败!" + data.responseJSON.message + "!");
    });
    ajax.set(this.driversInfoData);
    ajax.start();
}

$(function() {

});
