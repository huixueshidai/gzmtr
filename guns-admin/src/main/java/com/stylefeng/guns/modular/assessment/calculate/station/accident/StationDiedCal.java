package com.stylefeng.guns.modular.assessment.calculate.station.accident;

import com.stylefeng.guns.common.persistence.model.Assessment;
import com.stylefeng.guns.core.util.DoubleUtil;
import com.stylefeng.guns.modular.assessment.calculate.ThirdIndicationCalculator;
import com.stylefeng.guns.modular.assessment.calculate.aware.StationThirdIndicationAware;
import org.springframework.stereotype.Component;

import java.util.Date;

/**
 * 人员死亡指数
 */
@Component
public class StationDiedCal extends ThirdIndicationCalculator implements StationThirdIndicationAware {


    @Override
    protected Integer getIndicationId() {
        return 47;
    }

    @Override
    public Assessment calculate(Integer stationId, Date beginDate, Date endDate) {
        Double num = getDoubleValue(stationId, 149, beginDate,endDate);
        Double total = getDoubleValue(stationId, 150, beginDate,endDate);
        return genResult(DoubleUtil.safeDiv(num, total), stationId, beginDate,endDate);
    }

}
