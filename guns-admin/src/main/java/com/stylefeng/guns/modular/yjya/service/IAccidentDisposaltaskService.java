package com.stylefeng.guns.modular.yjya.service;

import com.stylefeng.guns.common.persistence.model.AccidentDisposaltask;
import com.baomidou.mybatisplus.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author wangshuaikang
 * @since 2018-09-11
 */
public interface IAccidentDisposaltaskService extends IService<AccidentDisposaltask> {
	
}
