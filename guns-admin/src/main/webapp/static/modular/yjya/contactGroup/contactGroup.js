/**
 * 通讯录群组管理初始化
 */
var ContactGroup = {
    id: "ContactGroupTable",	//表格id
    seItem: null,		//选中的条目
    table: null,
    layerIndex: -1
};

/**
 * 初始化表格的列
 */
ContactGroup.initColumn = function () {
    return [
        {field: 'selectItem', radio: true},
        {title: 'id', field: 'id', visible: true, align: 'center', valign: 'middle', width:'50px'},
        {title: '名称', field: 'name', visible: true, align: 'center', valign: 'middle'},
        {title: '排序', field: 'num', visible: true, align: 'center', valign: 'middle'},
    ];
};

/**
 * 检查是否选中
 */
ContactGroup.check = function () {
    var selected = $('#' + this.id).bootstrapTreeTable('getSelections');
    if(selected.length == 0){
        Feng.info("请先选中表格中的某一记录！");
        return false;
    }else{
        ContactGroup.seItem = selected[0];
        return true;
    }
};

/**
 * 点击添加通讯录群组
 */
ContactGroup.openAddContactGroup = function () {
    var index = layer.open({
        type: 2,
        title: '添加通讯录群组',
        area: ['800px', '420px'], //宽高
        fix: false, //不固定
        maxmin: true,
        content: Feng.ctxPath + '/contactGroup/contactGroup_add'
    });
    this.layerIndex = index;
};

/**
 * 打开查看通讯录群组详情
 */
ContactGroup.openContactGroupDetail = function () {
    if (this.check()) {
        var index = layer.open({
            type: 2,
            title: '通讯录群组详情',
            area: ['800px', '420px'], //宽高
            fix: false, //不固定
            maxmin: true,
            content: Feng.ctxPath + '/contactGroup/contactGroup_update/' + ContactGroup.seItem.id
        });
        this.layerIndex = index;
    }
};

/**
 * 删除通讯录群组
 */
ContactGroup.delete = function () {
    if (this.check()) {
        var ajax = new $ax(Feng.ctxPath + "/contactGroup/delete", function (data) {
            Feng.success("删除成功!");
            ContactGroup.table.refresh();
        }, function (data) {
            Feng.error("删除失败!" + data.responseJSON.message + "!");
        });
        ajax.set("contactGroupId", this.seItem.id);
        ajax.start();
    }
};
/**
 * 重置
 */
ContactGroup.resetSearch = function(){
    $("#condition").val("");
}
/**
 * 查询通讯录群组列表
 */
ContactGroup.search = function () {
    var queryData = {};
    queryData['condition'] = $("#condition").val();
    ContactGroup.table.refresh({query: queryData});
};

$(function () {
    var defaultColunms = ContactGroup.initColumn();
    var table = new BSTreeTable(ContactGroup.id, "/contactGroup/list", defaultColunms);
    table.setExpandColumn(2);
    table.setIdField("id");
    table.setCodeField("id");
    table.setParentCodeField("pid");
    table.setExpandAll(true);
    table.init();
    ContactGroup.table = table;
});
