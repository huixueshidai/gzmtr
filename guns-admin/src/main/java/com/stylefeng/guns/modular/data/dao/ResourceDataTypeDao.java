package com.stylefeng.guns.modular.data.dao;

import com.stylefeng.guns.common.persistence.model.ResourceDataType;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

public interface ResourceDataTypeDao {

    List<Map<String, Object>> selectResourceDataTypes(@Param("name") String name, @Param("indicationId") String indicationId);

    ResourceDataType getByAccount(@Param("name") String name);

    List<ResourceDataType> getResourceDataTypesByIndication(@Param("indicationId") Integer indicationId);
}
