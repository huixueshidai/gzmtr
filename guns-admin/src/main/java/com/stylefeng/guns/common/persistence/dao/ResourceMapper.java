package com.stylefeng.guns.common.persistence.dao;

import com.stylefeng.guns.common.persistence.model.Resource;
import com.baomidou.mybatisplus.mapper.BaseMapper;

/**
 * <p>
  * 应急资源 Mapper 接口
 * </p>
 *
 * @author yinjc
 * @since 2018-02-23
 */
public interface ResourceMapper extends BaseMapper<Resource> {

}