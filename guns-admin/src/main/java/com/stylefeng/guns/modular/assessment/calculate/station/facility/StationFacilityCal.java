package com.stylefeng.guns.modular.assessment.calculate.station.facility;

import com.stylefeng.guns.core.util.SpringContextHolder;
import com.stylefeng.guns.modular.assessment.calculate.SecondIndicationCalculator;
import com.stylefeng.guns.modular.assessment.calculate.ThirdIndicationCalculator;
import com.stylefeng.guns.modular.assessment.calculate.aware.StationSecondIndicationAware;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import java.util.Arrays;
import java.util.List;

/**
 * 车站设备
 */
@Component
@Scope(ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public class StationFacilityCal extends SecondIndicationCalculator implements StationSecondIndicationAware {

    private List<ThirdIndicationCalculator> thirdIndicationCalculatorList;

    public StationFacilityCal() {
        thirdIndicationCalculatorList = Arrays.asList(
                //信号系统
                SpringContextHolder.getBean(StationBeaconageCal.class),
                //通讯系统
                SpringContextHolder.getBean(StationCommunicationCal.class),
                //自动扶梯
                SpringContextHolder.getBean(StationAutoFutiCal.class),
                //给排水系统安全指数
                SpringContextHolder.getBean(StationWaterCal.class),
                //消防
                SpringContextHolder.getBean(StationXiaoFangCal.class),
                //屏蔽门
                SpringContextHolder.getBean(StationDoorCal.class),
                //照明系统
                SpringContextHolder.getBean(StationLightCal.class),
                //空调
                SpringContextHolder.getBean(StationKongTiaoCal.class),
                //afc
                SpringContextHolder.getBean(StationAfcCal.class),
                //安检
                SpringContextHolder.getBean(StationSecurityCheckCal.class)
        );

    }


    @Override
    public List<ThirdIndicationCalculator> getThirdIndicationCalculatorList() {
        return thirdIndicationCalculatorList;
    }

    @Override
    protected Integer getIndicationId() {
        return 18;
    }
}
