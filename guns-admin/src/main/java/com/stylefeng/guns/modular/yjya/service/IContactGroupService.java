package com.stylefeng.guns.modular.yjya.service;

import com.stylefeng.guns.common.persistence.model.ContactGroup;
import com.baomidou.mybatisplus.service.IService;
import com.stylefeng.guns.common.plugin.service.IBaseTreeableService;

/**
 * <p>
 * 通讯录群组 服务类
 * </p>
 *
 * @author xuziyang
 * @since 2017-12-26
 */
public interface IContactGroupService extends IBaseTreeableService<ContactGroup> {

}
