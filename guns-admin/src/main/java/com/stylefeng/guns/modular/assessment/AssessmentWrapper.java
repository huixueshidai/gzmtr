package com.stylefeng.guns.modular.assessment;

import com.stylefeng.guns.common.constant.factory.ConstantFactory;
import com.stylefeng.guns.core.base.warpper.BaseControllerWarpper;
import org.apache.commons.collections.MapUtils;

import java.util.Map;

public class AssessmentWrapper extends BaseControllerWarpper {
    public AssessmentWrapper(Object o) {
        super(o);
    }

    @Override
    protected void warpTheMap(Map<String, Object> map) {
        map.put("indicationName", ConstantFactory.me().getIndicationNameById(MapUtils.getInteger(map, "indicationId")));
        map.put("roadnetName", ConstantFactory.me().getRoadnetNameById(MapUtils.getInteger(map, "roadnetId")));
    }
}
