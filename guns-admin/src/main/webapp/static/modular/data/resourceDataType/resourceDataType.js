/**
 * 源数据类型管理初始化
 */
var ResourceDataType = {
    id: "ResourceDataTypeTable",	//表格id
    seItem: null,		//选中的条目
    table: null,
    layerIndex: -1,
    indicationId:0
};

/**
 * 初始化表格的列
 */
ResourceDataType.initColumn = function () {
    return [
        {field: 'selectItem', radio: true},
        {title: 'id', field: 'id', visible: true, align: 'center', valign: 'middle'},
        {title: '源数据名称', field: 'name', visible: true, align: 'center', valign: 'middle'},
        {title: '是否填写进站口', field: 'needEntrance', visible: true, align: 'center', valign: 'middle',
            formatter: function (value, row, index) {
                console.log(value);
                if(value === 1)
                    return "是"

                return "否"
            }
        },
        {title: '说明', field: 'instruction', visible: true, align: 'center', valign: 'middle'},
        // {title: '是否为可修改固值', field: 'isFixed', visible: true, align: 'center', valign: 'middle'},
        // {title: '默认值', field: 'defaultValue', visible: true, align: 'center', valign: 'middle'},
    ];
};

/**
 * 检查是否选中
 */
ResourceDataType.check = function () {
    var selected = $('#' + this.id).bootstrapTable('getSelections');
    if (selected.length == 0) {
        Feng.info("请先选中表格中的某一记录！");
        return false;
    } else {
        ResourceDataType.seItem = selected[0];
        return true;
    }
};

/**
 * 点击添加源数据类型
 */
ResourceDataType.openAddResourceDataType = function () {
    var indicationId = ResourceDataType.indicationId;
    var index = layer.open({
        type: 2,
        title: '添加源数据类型',
        area: ['850px', '420px'], //宽高
        fix: false, //不固定
        maxmin: true,
        content: Feng.ctxPath + '/resourceDataType/resourceDataType_add?indicationId='+indicationId
    });
    this.layerIndex = index;
};

/**
 * 打开查看源数据类型详情
 */
ResourceDataType.openResourceDataTypeDetail = function () {
    if (this.check()) {
        var index = layer.open({
            type: 2,
            title: '源数据类型详情',
            area: ['850px', '420px'], //宽高
            fix: false, //不固定
            maxmin: true,
            content: Feng.ctxPath + '/resourceDataType/resourceDataType_update/' + ResourceDataType.seItem.id
        });
        this.layerIndex = index;
    }
};

/**
 * 删除源数据类型
 */
ResourceDataType.delete = function () {
    if (this.check()) {
        var ajax = new $ax(Feng.ctxPath + "/resourceDataType/delete", function (data) {
            Feng.success("删除成功!");
            ResourceDataType.table.refresh();
        }, function (data) {
            Feng.error("删除失败!" + data.responseJSON.message + "!");
        });
        ajax.set("resourceDataTypeId", this.seItem.id);
        ajax.start();
    }
};

/**
 * 查询源数据类型列表
 */
ResourceDataType.search = function () {
    var queryData = {};
    queryData['name'] = $("#name").val();
    queryData['indicationId'] = ResourceDataType.indicationId;
    ResourceDataType.table.refresh({query: queryData});
};

ResourceDataType.onClickIndication = function (e, treeId, treeNode) {
    ResourceDataType.indicationId = treeNode.id;
    ResourceDataType.search();
}

$(function () {
    $("#needEntrance").val($("#needEntranceValue").val());

    var defaultColunms = ResourceDataType.initColumn();
    var table = new BSTable(ResourceDataType.id, "/resourceDataType/list", defaultColunms);
    table.setPaginationType("client");
    ResourceDataType.table = table.init();

    var ztree = new $ZTree("indicationTree", "/indication/tree");
    ztree.bindOnClick(ResourceDataType.onClickIndication);
    ztree.init();
});
