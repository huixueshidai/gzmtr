/**
 * Created by 60132 on 2018/7/3.
 */
var PublicBusEmulateInfo ={
    publicbusInfoData : {},
    radiusInfo:200,
    endTime:null,
    itemTemplate: $("#itemTemplate").html(),
    t:0,
    layerIndex:-1,
    paaaOut:null,
    emuPassNumZ:[parseInt($("#e0").text())+parseInt($("#e1").text())], //正向滞留客流
    emuPassNumF:[parseInt($("#g0").text())+parseInt($("#g1").text())], //反向滞留客流
    emuPassNumZS:parseInt($("#TranPassA").text())+parseInt($("#TranPassAz").text()), //正向疏运客流
    emuPassNumFS:parseInt($("#TranPassB").text())+parseInt($("#TranPassBz").text()), //反向疏运客流
};

/**
 * 结束按钮
 */
PublicBusEmulateInfo.cancleEnd = function(){
    clearInterval(PublicBusEmulateInfo.paaaOut);
    var time = new Date();
    // 程序计时的月从0开始取值后+1
    var m = time.getMonth() + 1;
    var t = time.getFullYear() + "-" + m + "-"
        + time.getDate() + " " + time.getHours() + ":"
        + time.getMinutes() + ":" + time.getSeconds();
    PublicBusEmulateInfo.endTime = t;
    document.getElementById("endTime").innerHTML = PublicBusEmulateInfo.endTime.toString();

    $("#startUpReport").attr("disabled", false);
}

/**
 * 生成报告
 */
PublicBusEmulateInfo.startUpReport = function(){
    // var stationId = $("#stationId").val();
    var busNum = $("#busNum").val();
    var stationSatrtTime = $("#stationSatrtTime").val();
    var index = layer.open({
        type: 2,
        title: '公交应急联动处置报告',
        area: ['900px', '600px'], //宽高
        fix: false, //不固定
        maxmin: true,
        content: Feng.ctxPath + '/publicBusHub/dispose_report/'+"/"+busNum+"/"+stationSatrtTime+"/"+PublicBusEmulateInfo.endTime
    });
    this.layerIndex = index;
    layer.full(index);
}

/**
 * 仿真客流图展示（正向）
 */
PublicBusEmulateInfo.plan = function(){
    var direction = $("#directionZ").text();
    var index = layer.open({
        type: 2,
        title: '客流仿真图 '+direction,
        area: ['1200px', '600px'], //宽高
        fix: false, //不固定
        maxmin: true,
        content: Feng.ctxPath + '/publicBusHub/plan'
    });
    this.layerIndex = index;
    layer.full(index);
}

/**
 * 仿真客流图展示（反向）
 */
PublicBusEmulateInfo.planF = function(){
    var direction = $("#directionF").text();
    var index = layer.open({
        type: 2,
        title: '客流仿真图 '+direction,
        area: ['1200px', '600px'], //宽高
        fix: false, //不固定
        maxmin: true,
        content: Feng.ctxPath + '/publicBusHub/planF'
    });
    this.layerIndex = index;
    layer.full(index);
}

$(function(){

    var stationNum = $("#stationNum").val();
    var passBeforeList = $("#passBeforeList").val();
    var passAfterList =$("#passAfterList").val();
    var passTranAfterList =$("#passTranAfterList").val();
    var passTranBeforeList =$("#passTranBeforeList").val();
    //设置客流初始状态
    for(var i=0;i<stationNum-1;i++){
        //下行
        $("#a").append('<div style="font-size: 16px;text-align: center;height: 40px"><span>'+passAfterList.replace(/\[|]/g,'').split(',')[i]+'</span>人</div>');
        //上行
        $("#c").append('<div style="font-size: 16px;text-align: center;height: 40px"><span>'+passBeforeList.replace(/\[|]/g,'').split(',')[i]+'</span>人</div>');
    }
    //设置客流当前状态
    for(var i =1;i<stationNum-1;i++){
        var j = i+1;
        $("#e").append('<div style="font-size: 16px;text-align: center;height: 40px"><span id="e'+(i+1)+'">'+passAfterList.replace(/\[|]/g,'').split(',')[i]+'</span>人</div>');
        $("#g").append('<div style="font-size: 16px;text-align: center;height: 40px"><span id="g'+(i+1)+'">'+passBeforeList.replace(/\[|]/g,'').split(',')[i]+'</span>人</div>');
        PublicBusEmulateInfo.emuPassNumZ.push(parseInt($("#e"+j).text().split("：")[1]));
        PublicBusEmulateInfo.emuPassNumF.push(parseInt($("#g"+j).text().split("：")[1]));
    }
    PublicBusEmulateInfo.emuPassNumZ.push(parseInt($("#eEnd").val()));
    PublicBusEmulateInfo.emuPassNumF.push(parseInt($("#eEndF").val()));

    var TranPassAz = parseInt($("#TranPassAz").text());
    var TranPassBz = parseInt($("#TranPassBz").text());

    PublicBusEmulateInfo.paaaOut = setInterval(function() {passengerChange()},60000);

    //边缘站点滞留客流的衰减
    function passengerChange(){
        PublicBusEmulateInfo.t+=1;
        var T = parseInt($("#interruptionTime").val());
        // var T = 20;
        var passengers = parseInt($("#passengers").val());
        var yTranPassAz = parseInt($("#yTranPassAZ").val());
        var yTranPassAzz = parseInt($("#yTranPassAZZ").val());
        var yTranPassBz = parseInt($("#yTranPassBZ").val());
        var yTranPassBzz = parseInt($("#yTranPassBZZ").val());

        $("#TranPassA").text( parseInt($("#TranPassA").text())+yTranPassAz);
        $("#TranPassAz").text( parseInt($("#TranPassAz").text())+yTranPassAzz);
        $("#TranPassB").text( parseInt($("#TranPassB").text())+yTranPassBz);
        $("#TranPassBz").text( parseInt($("#TranPassBz").text())+yTranPassBzz);

        //下行衰减
        if(PublicBusEmulateInfo.t!=T){
            //第一次衰减KCλ
            var passengerOut = parseInt($("#e0").text()/passengers)*passengers;

            if( $("#e0").text()>=0){
                if(parseInt($("#e0").text())>=passengers){
                    $("#e0").text(parseInt($("#e0").text())-passengerOut);
                }
                else{
                    if(parseInt($("#e0").text())+parseInt($("#TranPassA").text())>=passengers){
                        $("#TranPassA").text(parseInt($("#e0").text())+parseInt($("#TranPassA").text())-passengers);
                        $("#e0").text(0);
                    }
                }

            }
        }
        else{
            if(parseInt($("#TranPassA").text())<passengers){
                $("#TranPassA").text(0);
            }
        }

        //上行衰减
        if(PublicBusEmulateInfo.t!=T){
            //第一次衰减KCλ
            var passengerOut = parseInt($("#G0").text()/passengers)*passengers;
            if($("#g0").text()>=0){
                if(parseInt($("#g0").text())>=passengers){
                    $("#g0").text($("#g0").text()-passengerOut);
                }
                else{
                    if( parseInt($("#g0").text())+parseInt($("#TranPassB").text())>=passengers){
                        $("#TranPassB").text(parseInt($("#g0").text())+parseInt($("#TranPassB").text())-passengers);
                        $("#g0").text(0);
                    }
                }
            }
        }
        else{
            if(parseInt($("#TranPassB").text())<passengers){
                $("#TranPassB").text(0);
            }
        }


        //站站停客流疏散
        var xz0 = parseInt($("#xz0").val());
        var xf0 = parseInt($("#xf0").val());
        //F1,F2,F3,F4,F5
        var FlistZ = new Array($("#FlistZ").val().replace(/\[|]/g,'').split(','));
        var FlistF = new Array($("#FlistF").val().replace(/\[|]/g,'').split(','));

        var k = parseInt(xz0/passengers);

        //下行方向
        if(PublicBusEmulateInfo.t==1&&xz0>=passengers){
                for(var i = 1;i<stationNum;i++){
                    $("#e"+i).text(parseInt($("#e"+i).text())-parseInt($.trim(FlistZ[i-1])));
                    if(parseInt($("#e"+i).text())<=0){
                        $("#e"+i).text(0);
                    }
                    if(i != 1){
                        PublicBusEmulateInfo.emuPassNumZ[i]=parseInt($("#e"+i).text())-parseInt($.trim(FlistZ[i-1]));
                    }
                }
        }
        if(PublicBusEmulateInfo.t>1&&PublicBusEmulateInfo.t<T){
            if(xz0+parseInt($("#TranPassAz").text())-(k*passengers)>=passengers*PublicBusEmulateInfo.t){
                $("#e1").text(0);
                $("#TranPassAz").text(parseInt($("#TranPassAz").text())-passengers);
            }
            else if(xz0+parseInt($("#TranPassAz").text())-(k*passengers)>=passengers){
                for(var i = 1;i<stationNum;i++) {
                    $("#E" + i).text(0);
                }
                $("#TranPassAz").text(xz0+parseInt($("#TranPassAz").text())-(k*passengers));
            }
            else{
                return true;
            }
        }
        if(PublicBusEmulateInfo.t==T){
            if(xz0+parseInt($("#TranPassAz").text())-(k*passengers)<passengers*PublicBusEmulateInfo.t){
                $("#TranPassAz").text(0);
            }
            else{
                $("#TranPassAz").text(0);
            }
        }

        //上行方向
        if(PublicBusEmulateInfo.t==1&&xf0>=passengers){
                for(var i = 1;i<stationNum;i++){
                    $("#g"+i).text(parseInt($("#g"+i).text())-parseInt($.trim(FlistF[i-1])));
                    if(parseInt($("#g"+i).text())<=0){
                        $("#g"+i).text(0);
                    }
                    if(i != 1){
                        PublicBusEmulateInfo.emuPassNumF[i]=parseInt($("#g"+i).text())-parseInt($.trim(FlistF[i-1]));
                    }
                }
        }
        if(PublicBusEmulateInfo.t>1&&PublicBusEmulateInfo.t<T){
            if(xf0+parseInt($("#TranPassBz").text())-(k*passengers)>=passengers*PublicBusEmulateInfo.t){
                $("#g1").text(0);
                $("#TranPassBz").text(parseInt($("#TranPassBz").text())-passengers);
            }
            else if(xf0+parseInt($("#TranPassBz").text())-(k*passengers)>=passengers){
                for(var i = 1;i<stationNum;i++) {
                    $("#g" + i).text(0);
                }
                $("#TranPassBz").text(xf0+parseInt($("#TranPassBz").text())-(k*passengers));
            }
        }
        if(PublicBusEmulateInfo.t==T){
            if(xf0+parseInt($("#TranPassBz").text())-(k*passengers)<passengers*PublicBusEmulateInfo.t){
                $("#TranPassBz").text(0);
            }
            else{
                $("#TranPassBz").text(0);
            }
            PublicBusEmulateInfo.cancleEnd();
        }


        PublicBusEmulateInfo.emuPassNumZ[0]=parseInt($("#e0").text())+parseInt($("#e1").text());
        PublicBusEmulateInfo.emuPassNumF[0]=parseInt($("#g0").text())+parseInt($("#g1").text());
        PublicBusEmulateInfo.emuPassNumZS=parseInt($("#TranPassA").text())+parseInt($("#TranPassAz").text());
        PublicBusEmulateInfo.emuPassNumFS=parseInt($("#TranPassB").text())+parseInt($("#TranPassBz").text());
        if(PublicBusEmulateInfo.t == T){
            clearInterval(PublicBusEmulateInfo.paaaOut);
        }
        console.log("疏运Z"+PublicBusEmulateInfo.emuPassNumZS);
        console.log("疏运F"+PublicBusEmulateInfo.emuPassNumFS);

    }

    /**
     * 设置实时时间代码
     */
    function mytime(){
        var time = new Date();
        // 程序计时的月从0开始取值后+1
        var m = time.getMonth() + 1;
        var t = time.getFullYear() + "-" + m + "-"
            + time.getDate() + " " + time.getHours() + ":"
            + time.getMinutes() + ":" + time.getSeconds();
        document.getElementById("Time").innerHTML = t;
    }
    setInterval(function() {mytime()},1000);




});
