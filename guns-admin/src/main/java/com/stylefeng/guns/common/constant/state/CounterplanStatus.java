package com.stylefeng.guns.common.constant.state;

/**
 * Created by 60132 on 2018/1/23.
 */
public enum CounterplanStatus {

    APPROVE(1, "审核通过"), UNAPPROVE(2, "审核未通过"), UNAUDITE(0, "未审核0");
    int code ;
    String message;

    CounterplanStatus(int code, String message) {
        this.code = code;
        this.message = message;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public static String valueOf(Integer value) {
        if (value == null) {
            return "";
        } else {
            for (CounterplanStatus ms : CounterplanStatus.values()) {
                if (ms.getCode() == value) {
                    return ms.getMessage();
                }
            }
            return "";
        }
    }
}
