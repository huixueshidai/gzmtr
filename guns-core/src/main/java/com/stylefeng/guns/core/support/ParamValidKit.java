package com.stylefeng.guns.core.support;

import javax.validation.*;
import java.util.Set;

public class ParamValidKit {

    private static Validator validator;

    static {
        ValidatorFactory vf = Validation.buildDefaultValidatorFactory();
        validator = vf.getValidator();
    }


    /**
     * @throws ValidationException
     * @throws ValidationException
     * @Description: 校验方法
     * @param t 将要校验的对象
     * @throws ValidationException
     * void
     * @throws
     */
    public static <T> void validate(T t) throws ValidationException {
        Set<ConstraintViolation<T>> set =  validator.validate(t);
        if(set.size()>0){
            StringBuilder validateError = new StringBuilder();
            for(ConstraintViolation<T> val : set){
                validateError.append(val.getMessage() + " ;");
            }
            throw new ValidationException(validateError.toString());
        }
    }

}
