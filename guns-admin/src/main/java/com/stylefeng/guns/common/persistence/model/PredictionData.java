package com.stylefeng.guns.common.persistence.model;

import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;
import com.baomidou.mybatisplus.enums.IdType;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;

/**
 * <p>
 * 风险预测数据
 * </p>
 *
 * @author xuziyang
 * @since 2018-05-19
 */
@TableName("gzmtr_prediction_data")
@Data
public class PredictionData extends Model<PredictionData> {

    private static final long serialVersionUID = 1L;

	@TableId(value="id", type= IdType.AUTO)
	private Integer id;
    /**
     * 年份
     */
	private Integer year;
    /**
     * 事故类别
     */
    @TableField("accident_type")
	private String accidentType;
    /**
     * 事故次数
     */
	private Integer num;

	@TableField("create_time")
	private Date createTime;


	@Override
	protected Serializable pkVal() {
		return this.id;
	}

}
