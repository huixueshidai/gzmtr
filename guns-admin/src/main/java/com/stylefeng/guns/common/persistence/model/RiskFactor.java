package com.stylefeng.guns.common.persistence.model;

import com.baomidou.mybatisplus.annotations.TableName;
import com.stylefeng.guns.common.plugin.entity.BaseModel;
import com.stylefeng.guns.common.plugin.entity.Treeable;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

/**
 * <p>
 * 事故致因
 * </p>
 *
 * @author xuziyang
 * @since 2018-04-11
 */
@TableName("gzmtr_risk_factor")
@Getter
@Setter
@ToString
public class RiskFactor extends BaseModel<ContactGroup> implements Treeable {

    private static final long serialVersionUID = 1L;

    /**
     * 名称
     */
	private String name;

	private Integer pid;


	private String pids;
    /**
     * 排序
     */
	private Integer num;
    /**
     * 中值
     */
	private Double median;
    /**
     * 上限
     */
	private Double upper;
    /**
     * 下限
     */
	private Double flo;

	/**
	 * 1：默认
	 */
	private Integer status;

}
