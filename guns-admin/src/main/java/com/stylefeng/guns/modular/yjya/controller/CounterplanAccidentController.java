package com.stylefeng.guns.modular.yjya.controller;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.mapper.Wrapper;
import com.google.common.collect.Lists;
import com.stylefeng.guns.common.exception.BizExceptionEnum;
import com.stylefeng.guns.common.exception.BussinessException;
import com.stylefeng.guns.common.persistence.model.CounterplanAccident;
import com.stylefeng.guns.core.base.controller.BaseController;
import com.stylefeng.guns.modular.yjya.service.ICounterplanAccidentService;
import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;

/**
 * 预案处置流程控制器
 *
 * @author xuziyang
 * @Date 2018-01-22 14:41:55
 */
@Controller
@RequestMapping("/counterplanAccident")
public class CounterplanAccidentController extends BaseController {

    private String PREFIX = "/yjya/accident/";

    @Autowired
    private ICounterplanAccidentService counterplanAccidentService;

    /**
     * 跳转到预案处置便捷也
     */
    @RequestMapping("/process_edit")
    public String update() {
        return PREFIX + "process_edit.html";
    }

    @RequestMapping("/getByCounterplan/{counterplanId}")
    @ResponseBody
    public Object getByCounterplanId(@PathVariable("counterplanId") Integer counterplanId) {
        List<String> result = Lists.newArrayList();
        Wrapper wrapper = new EntityWrapper<CounterplanAccident>();
        wrapper.eq("id", counterplanId);
        wrapper.orderBy("num");
        List<CounterplanAccident> list =  this.counterplanAccidentService.selectList(wrapper);
        if (CollectionUtils.isEmpty(list)) {
            throw new BussinessException(BizExceptionEnum.DB_RESOURCE_NULL);
        }
        for (CounterplanAccident ca : list) {
            result.add(ca.toString());
        }
        return result;
    }

    /**
     * 新增预案处置流程
     */
    @RequestMapping(value = "/add")
    @ResponseBody
    public Object add(@RequestParam("counterplanId") Integer counterplanId
                     ,@RequestParam("accidentIds") Integer[] accidentIds)  {
        counterplanAccidentService.insert(counterplanId,accidentIds);
        return super.SUCCESS_TIP;
    }


    /**
     * 修改预案处置流程
     */
    @RequestMapping(value = "/update")
    @ResponseBody
    public Object update(CounterplanAccident counterplanAccident) {
        counterplanAccidentService.updateById(counterplanAccident);
        return super.SUCCESS_TIP;
    }

    /**
     * 预案处置流程详情
     */
    @RequestMapping(value = "/detail/{counterplanAccidentId}")
    @ResponseBody
    public Object detail(@PathVariable("counterplanAccidentId") Integer counterplanAccidentId) {
        return counterplanAccidentService.selectById(counterplanAccidentId);
    }

    @RequestMapping(value = "/html/{counterplanId}")
    @ResponseBody
    public String html(@PathVariable("counterplanId") Integer counterplanId) {
        return counterplanAccidentService.getHtml(counterplanId);
    }
}
