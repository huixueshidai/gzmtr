/**
 * 管理初始化
 */
var Emergencyproperty = {
    id: "EmergencypropertyTable",	//表格id
    seItem: null,		//选中的条目
    table: null,
    layerIndex: -1,
    emergencyId:null
};

/**
 * 初始化表格的列
 */
Emergencyproperty.initColumn = function () {
    return [
        {field: 'selectItem', radio: true},
            {title: 'id', field: 'id', visible: true, align: 'center', valign: 'middle'},

            {title: '客流', field: 'ridership', visible: true, align: 'center', valign: 'middle'},
            {title: '通行能力', field: 'trafficCapacity', visible: true, align: 'center', valign: 'middle'},
            {title: '故障数量', field: 'fault', visible: true, align: 'center', valign: 'middle'},
            {title: '气体保护', field: 'gasShield', visible: true, align: 'center', valign: 'middle'},
            {title: '隧道长度', field: 'tunnelLength', visible: true, align: 'center', valign: 'middle'},
            {title: '状态', field: 'status', visible: true, align: 'center', valign: 'middle'},
            {title: '距离前一站距离', field: 'distanceOn', visible: true, align: 'center', valign: 'middle'},
            {title: '距离下一站距离', field: 'distanceNext', visible: true, align: 'center', valign: 'middle'},
            {title: '影响人数', field: 'trappedNum', visible: true, align: 'center', valign: 'middle'},
            {title: '被困人员行动能力', field: 'trappedNumPower', visible: true, align: 'center', valign: 'middle'},
            {title: '物理状态', field: 'physicalState', visible: true, align: 'center', valign: 'middle'},
            {title: '毒性', field: 'toxicity', visible: true, align: 'center', valign: 'middle'},
            {title: '燃爆性', field: 'bums', visible: true, align: 'center', valign: 'middle'},
            {title: '腐蚀性', field: 'causticity', visible: true, align: 'center', valign: 'middle'},
            {title: '挥发性', field: 'volatileness', visible: true, align: 'center', valign: 'middle'},
            {title: '温度', field: 'temperature', visible: true, align: 'center', valign: 'middle'},
            {title: '烟气浓度', field: 'smokescope', visible: true, align: 'center', valign: 'middle'},
            {title: '坍塌', field: 'collapse', visible: true, align: 'center', valign: 'middle'},
            {title: '失效', field: 'loseefficacy', visible: true, align: 'center', valign: 'middle'},
            {title: '道路状况', field: 'reoad', visible: true, align: 'center', valign: 'middle'},
            {title: '119距离', field: 'distanceOneOneNine', visible: true, align: 'center', valign: 'middle'},
            {title: '110距离', field: 'distanceOneOneZero', visible: true, align: 'center', valign: 'middle'},
            {title: '120距离', field: 'distanceOneTwoZero', visible: true, align: 'center', valign: 'middle'},
            {title: '消防设施', field: 'fireControl', visible: true, align: 'center', valign: 'middle'}
    ];
};

Emergencyproperty.resetSearch = function () {
    $("#name").val("");
    // Contact.search();

}

/**
 * 检查是否选中
 */
Emergencyproperty.check = function () {
    var selected = $('#' + this.id).bootstrapTable('getSelections');
    if(selected.length == 0){
        Feng.info("请先选中表格中的某一记录！");
        return false;
    }else{
        Emergencyproperty.seItem = selected[0];
        return true;
    }
};

/**
 * 点击添加
 */
Emergencyproperty.openAddEmergencyproperty = function () {
    var index = layer.open({
        type: 2,
        title: '添加',
        area: ['1200px', '600px'], //宽高
        fix: false, //不固定
        maxmin: true,
        content: Feng.ctxPath + '/emergencyproperty/emergencyproperty_add'
    });
    this.layerIndex = index;
    layer.full(index);
};

/**
 * 打开查看详情
 */
Emergencyproperty.openEmergencypropertyDetail = function () {
    if (this.check()) {
        var index = layer.open({
            type: 2,
            title: '详情',
            area: ['1200px', '600px'], //宽高
            fix: false, //不固定
            maxmin: true,
            content: Feng.ctxPath + '/emergencyproperty/emergencyproperty_update/' + Emergencyproperty.seItem.id
        });
        this.layerIndex = index;
    }
};

/**
 * 删除
 */
Emergencyproperty.delete = function () {
    if (this.check()) {
        var ajax = new $ax(Feng.ctxPath + "/emergencyproperty/delete", function (data) {
            Feng.success("删除成功!");
            Emergencyproperty.table.refresh();
        }, function (data) {
            Feng.error("删除失败!" + data.responseJSON.message + "!");
        });
        ajax.set("emergencypropertyId",this.seItem.id);
        ajax.start();
    }
};

/**
 * 查询列表
 */
Emergencyproperty.search = function () {
    var queryData = {};
    queryData['emergencyId'] = Emergencyproperty.emergencyId;
    queryData['condition'] = $("#condition").val();
    Emergencyproperty.table.refresh({query: queryData});
};

Emergencyproperty.onClickContactGroup = function (e, treeId, treeNode) {
    Emergencyproperty.emergencyId = treeNode.id;
    Emergencyproperty.search();
}

$(function () {
    var defaultColunms = Emergencyproperty.initColumn();
    var table = new BSTable(Emergencyproperty.id, "/emergencyproperty/list", defaultColunms);
    table.setPaginationType("server");
    Emergencyproperty.table = table.init();

    var ztree = new $ZTree("emergencyTree", "/emergency/tree");
    ztree.bindOnClick(Emergencyproperty.onClickContactGroup);
    ztree.init();
});
