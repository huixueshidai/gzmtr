package com.stylefeng.guns.modular.yjya.warpper;

import com.stylefeng.guns.common.constant.factory.ConstantFactory;
import com.stylefeng.guns.common.persistence.dao.AccidentProcessMapper;
import com.stylefeng.guns.common.persistence.dao.AccidentTypeMapper;
import com.stylefeng.guns.common.persistence.model.AccidentProcess;
import com.stylefeng.guns.common.persistence.model.AccidentType;
import com.stylefeng.guns.core.base.warpper.BaseControllerWarpper;
import com.stylefeng.guns.core.util.SpringContextHolder;

import java.io.Serializable;
import java.util.Map;

public class AccidentProcessWarpper  extends BaseControllerWarpper{
            public AccidentProcessWarpper(Object o ){
                super(o);
            }
    protected  void warpTheMap(Map<String, Object> map){

        AccidentTypeMapper accidentTypeMapper = SpringContextHolder.getBean(AccidentTypeMapper.class);
        AccidentType accidentType = accidentTypeMapper.selectById((Serializable) map.get("typeId"));
        if (accidentType!=null){
            map.put("typeName",accidentType.getName());

        }

    }
}
