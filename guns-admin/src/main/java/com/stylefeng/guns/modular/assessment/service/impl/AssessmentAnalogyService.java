package com.stylefeng.guns.modular.assessment.service.impl;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.mapper.Wrapper;
import com.stylefeng.guns.common.persistence.model.Assessment;
import com.stylefeng.guns.common.persistence.model.Roadnet;
import com.stylefeng.guns.modular.assessment.entity.Warning;
import com.stylefeng.guns.modular.assessment.service.IAssessmentService;
import com.stylefeng.guns.modular.data.dao.ResourceDataDao;
import com.stylefeng.guns.modular.roadnet.service.IRoadnetService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Service
public class AssessmentAnalogyService {

	@Autowired
	private IAssessmentService assessmentService;

	@Autowired
	private IRoadnetService roadnetService;

	@Autowired
	private ResourceDataDao resourceDataDao;

	public List<Warning> getAllWarningInfo(Date beginDate, Date endDate) {
		List<Roadnet> lines = roadnetService.selectByType(2);
		List<Roadnet> stations = roadnetService.selectByType(3);
		stations.addAll(lines);
		return getWaringInfoList(stations, beginDate, endDate);
	}

	private List<Warning> getWaringInfoList(List<Roadnet> roadnetList,Date beginDate,Date endDate) {
		List<Warning> result = new ArrayList<>();
		for (Roadnet roadnet : roadnetList) {
			Long dataCount = resourceDataDao.selectCount(roadnet.getId(), beginDate, endDate);
			if (dataCount != 0) {
				Warning warningInfo = getWarningInfo(roadnet, beginDate, endDate);
				result.add(warningInfo);
			}
		}
		return result;
	}

	private Warning getWarningInfo(Roadnet roadnet, Date beginDate, Date endDate) {
		Assessment assessment = assessmentService.assessmentOnePeriod(roadnet.getId(), beginDate, endDate);
		Warning warning = assessmentToWarning(assessment, roadnet);
		return warning;
	}


	private Warning assessmentToWarning(Assessment assessment,Roadnet roadnet) {
		Warning warning = new Warning();
		warning.setLevel(assessment.getWarningLevel());
		warning.setType(roadnet.getType());
		warning.setName(roadnet.getName());
		return warning;
	}

	private List<Roadnet> getAnalogyStationByLine(Integer lineId) {
		Wrapper<Roadnet> wrapper = new EntityWrapper<>();
		wrapper.eq("pid", lineId);
		wrapper.eq("is_used", 1);
		return this.roadnetService.selectList(wrapper);
	}

	private List<Roadnet> getAnalogyLines() {
		Wrapper<Roadnet> wrapper = new EntityWrapper<>();
		wrapper.eq("type", 2);
		wrapper.eq("is_used", 1);
		return roadnetService.selectList(wrapper);
	}


}
