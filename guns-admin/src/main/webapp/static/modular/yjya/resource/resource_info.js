/**
 * 初始化应急资源详情对话框
 */
var ResourceInfoDlg = {
    resourceInfoData : {}
};

/**
 * 清除数据
 */
ResourceInfoDlg.clearData = function() {
    this.resourceInfoData = {};
}

/**
 * 设置对话框中的数据
 *
 * @param key 数据的名称
 * @param val 数据的具体值
 */
ResourceInfoDlg.set = function(key, val) {
    this.resourceInfoData[key] = (typeof val == "undefined") ? $("#" + key).val() : val;
    return this;
}

/**
 * 设置对话框中的数据
 *
 * @param key 数据的名称
 * @param val 数据的具体值
 */
ResourceInfoDlg.get = function(key) {
    return $("#" + key).val();
}

/**
 * 关闭此对话框
 */
ResourceInfoDlg.close = function() {
    parent.layer.close(window.parent.Resource.layerIndex);
}

/**
 * 收集数据
 */
ResourceInfoDlg.collectData = function() {
    this
    .set('id')
    .set('name')
    .set('largeScale')
    .set('modelNum')
    .set('fist')
    .set('num')
    .set('unit')
    .set('line')
    .set('location')
    .set('remark')
    .set('center')
    .set('usedp')
    .set('maintaindp')
    .set('updateTime')
        .set('state');
}

/**
 * 提交添加
 */
ResourceInfoDlg.addSubmit = function() {

    this.clearData();
    this.collectData();

    //提交信息
    var ajax = new $ax(Feng.ctxPath + "/resource/add", function(data){
        Feng.success("添加成功!");
        window.parent.Resource.table.refresh();
        ResourceInfoDlg.close();
    },function(data){
        Feng.error("添加失败!" + data.responseJSON.message + "!");
    });
    console.log(this.resourceInfoData);
    ajax.set(this.resourceInfoData);
    ajax.start();
}

/**
 * 提交修改
 */
ResourceInfoDlg.editSubmit = function() {

    this.clearData();
    this.collectData();

    //提交信息
    var ajax = new $ax(Feng.ctxPath + "/resource/update", function(data){
        Feng.success("修改成功!");
        window.parent.Resource.table.refresh();
        ResourceInfoDlg.close();
    },function(data){
        Feng.error("修改失败!" + data.responseJSON.message + "!");
    });
    ajax.set(this.resourceInfoData);
    ajax.start();
}

$(function() {
    laydate.render({
        elem: '#updateTime'
    });
});
