package com.stylefeng.guns.modular.assessment.calculate.line.passenger;

/**
 * 上行区间客流风险指数
 *
 * @author yinjc
 * @since 2018/05/010
 */

import com.stylefeng.guns.common.persistence.model.Assessment;
import com.stylefeng.guns.core.util.DoubleUtil;
import com.stylefeng.guns.modular.assessment.calculate.ThirdIndicationCalculator;
import com.stylefeng.guns.modular.assessment.calculate.aware.LineThirdIndicationAware;
import org.springframework.stereotype.Component;

import java.util.Date;

@Component
public class LineUpperBoundPassengerCal extends ThirdIndicationCalculator implements LineThirdIndicationAware {
    @Override
    protected Integer getIndicationId() {
        return 8;
    }

    @Override
    public Assessment calculate(Integer lineId, Date beginDate, Date endDate) {
        double Pmax = getDoubleValue(lineId, 152, beginDate,endDate);
        double p = getDoubleValue(lineId, 153, beginDate,endDate);
        double Na = getDoubleValue(lineId, 154, beginDate,endDate);
        double N = getDoubleValue(lineId, 155, beginDate,endDate);
        double nT = getDoubleValue(lineId, 156, beginDate,endDate);
        double t = getDoubleValue(lineId, 157, beginDate,endDate);
        double w1 = getDoubleValue(lineId, 158, beginDate,endDate);
        double w2 = getDoubleValue(lineId, 159, beginDate,endDate);
        double w3 = getDoubleValue(lineId, 160, beginDate,endDate);

        Double d = DoubleUtil.safeDiv(Pmax, p);
        Double D = DoubleUtil.mul(w1, d);

        Double m = DoubleUtil.safeDiv(Na, N);
        Double M = DoubleUtil.mul(m, w2);

        Double i = DoubleUtil.safeDiv(nT, t);
        Double I = DoubleUtil.mul(w3, i);

        Double resVal = DoubleUtil.add(D, M, I);

        return genResult(resVal, lineId, beginDate,endDate);
    }
}
