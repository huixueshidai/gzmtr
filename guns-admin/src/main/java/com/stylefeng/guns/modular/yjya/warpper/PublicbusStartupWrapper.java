package com.stylefeng.guns.modular.yjya.warpper;

import com.stylefeng.guns.common.constant.factory.ConstantFactory;
import com.stylefeng.guns.common.persistence.dao.RoadnetMapper;
import com.stylefeng.guns.common.persistence.model.Roadnet;
import com.stylefeng.guns.core.base.warpper.BaseControllerWarpper;
import com.stylefeng.guns.core.util.SpringContextHolder;
import org.apache.commons.collections.MapUtils;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

/**
 * Created by 60132 on 2018/9/9.
 */
public class PublicbusStartupWrapper extends BaseControllerWarpper {

    public PublicbusStartupWrapper(Object obj) {
        super(obj);
    }

    @Override
    protected void warpTheMap(Map<String, Object> map) {

        if(map.containsKey("lineId")){
            map.put("lineName", ConstantFactory.me().getRoadnetNameById((Integer) map.get("lineId")));
        }
        else{
            map.put("lineName", null);
        }
        if(map.containsKey("stationId")){
            String stationIds = MapUtils.getString(map,"stationId");
            List<Integer> stationIdList = new ArrayList(Arrays.asList(stationIds.split("/")));
            List<Roadnet> roadnetList = new ArrayList<>();
            String stationName = "";
            for(int i=0;i<stationIdList.size();i++){
                Roadnet station = SpringContextHolder.getBean(RoadnetMapper.class).selectById(stationIdList.get(i));
                roadnetList.add(station);
            }
            for(int i=0;i<roadnetList.size();i++){
                if(i==roadnetList.size()-1){
                    stationName += roadnetList.get(i).getName();
                }
                else{
                    stationName += roadnetList.get(i).getName()+">>";
                }
            }
            map.put("stationName",stationName);
        }
        else{
            map.put("stationName",null);
        }
        if(map.containsKey("dateStatus")){
            map.put("dateStatusName",map.get("dateStatus").toString());
        }


    }
}
