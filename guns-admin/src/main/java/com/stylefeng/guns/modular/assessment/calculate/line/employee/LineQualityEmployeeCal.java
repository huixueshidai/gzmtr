package com.stylefeng.guns.modular.assessment.calculate.line.employee;

import com.stylefeng.guns.common.persistence.model.Assessment;
import com.stylefeng.guns.core.util.DoubleUtil;
import com.stylefeng.guns.modular.assessment.calculate.ThirdIndicationCalculator;
import com.stylefeng.guns.modular.assessment.calculate.aware.LineThirdIndicationAware;
import org.springframework.stereotype.Component;

import java.util.Date;

/**
 * 线路员工素质指数
 */
@Component
public class LineQualityEmployeeCal extends ThirdIndicationCalculator implements LineThirdIndicationAware {

    @Override
    protected Integer getIndicationId() {
        return 55;
    }

    @Override
    public Assessment calculate(Integer lineId, Date beginDate, Date endDate) {
        Double ageScore = scoreCalculate(lineId, beginDate, endDate, 170, 172, 9, -4);
        Double eduScore = scoreCalculate(lineId, beginDate, endDate, 173, 177, 1, 2);
        Double skillScore = scoreCalculate(lineId, beginDate, endDate, 178, 182, 1, 2);
        double resValue = DoubleUtil.add(ageScore, eduScore, skillScore);
        return genResult(resValue, lineId, beginDate, endDate);
    }


}
