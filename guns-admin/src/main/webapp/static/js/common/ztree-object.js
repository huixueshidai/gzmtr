/**
 * ztree插件的封装
 */
(function() {

	var $ZTree = function(id, url) {
		this.id = id;
		this.url = url;
		this.onClick = null;
		this.settings = null;
		this.ondblclick=null;
		this.onCheck=null
	};

	$ZTree.prototype = {
		/**
		 * 初始化ztree的设置
		 */
		initSetting : function() {
			var settings = {
				view : {
					dblClickExpand : true,
					selectedMulti : false
				},
				data : {simpleData : {enable : true}},
				callback : {
					onClick : this.onClick,
					onDblClick:this.ondblclick,
					onCheck:this.onCheck
				}
			};
			return settings;
		},

		/**
		 * 手动设置ztree的设置
		 */
		setSettings : function(val) {
			this.settings = val;
		},

		/**
		 * 初始化ztree
		 */
		init : function(txtObj) {
			var zNodeSeting = null;
			var zNodes = null;
			if(this.settings != null){
				zNodeSeting = this.settings;
			}else{
				zNodeSeting = this.initSetting();
			}
			if(txtObj==null){
				zNodes = this.loadNodes();
			}
			else{
				var zTree = $.fn.zTree.getZTreeObj(this.id);
				zNodes = zTree.getNodesByParamFuzzy("name", txtObj.value);
				console.log(txtObj.value);
				console.log(zNodes);
				//清除重复数据
				// if(zNodes.length>0){
					zNodes=this.removeNodes(zNodes);
				// }
				// console.log('是的');
				// console.log(zNodes);

			}
			$.fn.zTree.init($("#" + this.id), zNodeSeting, zNodes);
		},

		/**
		 * 绑定onclick事件
		 */
		bindOnClick : function(func) {
			this.onClick = func;
		},
		/**
		 * 绑定双击事件
		 */
		bindOnDblClick : function(func) {
			this.ondblclick=func;
		},

		/**
		 * 绑定点击复选框事件
		 */
		Check : function(func) {
			this.onCheck=func;
		},


		/**
		 * 加载节点
		 */
		loadNodes : function() {
			var zNodes = null;
			var ajax = new $ax(Feng.ctxPath + this.url, function(data) {
				zNodes = data;
			}, function(data) {
				Feng.error("加载ztree信息失败!");
			});
			ajax.start();
			return zNodes;
		},

		/**
		 * 获取复选框选中的值（只有一级树）
		 */
		getSelectedCheck : function(){
			var zTree = $.fn.zTree.getZTreeObj(this.id);
			var nodes = zTree.getChangeCheckedNodes();
			return nodes[0].name;
		},

		/**
		 * 获取选中的值的所有子父节点的name拼接
		 */
		getSelectedVals : function(){
			var zTree = $.fn.zTree.getZTreeObj(this.id);
			var nodes = zTree.getSelectedNodes();
			var nodeAll = nodes[0].getPath();
			var nodeName="";
			if(nodeAll.length>2){
				for(var i =2;i<nodeAll.length;i++){
					nodeName += nodeAll[i].name+"--";
				}
			}
			else{
				return nodeAll[1].name;
			}
			return nodeName+nodeAll[1].name;
		},

		/**
		 * 获取选中的值的所有子父节点的name拼接,依次拼接
		 */
		getSelectedValName : function(){
			var zTree = $.fn.zTree.getZTreeObj(this.id);
			var nodes = zTree.getSelectedNodes();
			var nodeAll = nodes[0].getPath();
			var nodeName="";
			for(var i =0;i<nodeAll.length;i++){
				if(i==0){
					nodeName += nodeAll[i].name
				}
				else{
					nodeName += "-"+nodeAll[i].name;
				}

			}
			return nodeName;
		},

		/**
		 * 获取选中的值
		 */
		getSelectedVal : function(){
			var zTree = $.fn.zTree.getZTreeObj(this.id);
			var nodes = zTree.getSelectedNodes();
			return nodes[0].name;
		},

		/**
		 * 获取选中的复选框
		 * @param checkCount 选中复选框次数
		 */
		getSelectedValCheck : function(checkCount){
			var zTree = $.fn.zTree.getZTreeObj(this.id);
			var nodes = zTree.getChangeCheckedNodes(true);
			if(checkCount == 0){
				return ;
			}
			else if(checkCount == 1){
				return nodes[0].getPath()[2].name+":"+	nodes[0].name;
			}
			else{
				return ">>"+nodes[1].name;
			}


		},

		/**
		 * 清除重复数据
		 */
		removeNodes : function(nodeList){
			if(nodeList.length>0){
				for (var i=0;i<nodeList.length;i++){
					var node=nodeList[i];
					if(!node.isParent&&node.code!=""){
						nodeList.splice(i,1);
						i--;
					}
				}
			}
		return nodeList;
	}
	};

	window.$ZTree = $ZTree;

}());