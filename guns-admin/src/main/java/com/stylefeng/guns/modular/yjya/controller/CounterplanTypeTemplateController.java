package com.stylefeng.guns.modular.yjya.controller;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.mapper.Wrapper;
import com.stylefeng.guns.common.exception.BizExceptionEnum;
import com.stylefeng.guns.common.exception.BussinessException;
import com.stylefeng.guns.core.base.controller.BaseController;
import com.stylefeng.guns.modular.yjya.warpper.CounterplanTypeTemplateWarpper;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.beans.factory.annotation.Autowired;
import com.stylefeng.guns.core.log.LogObjectHolder;
import org.springframework.web.bind.annotation.RequestParam;
import com.stylefeng.guns.common.persistence.model.CounterplanTypeTemplate;
import com.stylefeng.guns.modular.yjya.service.ICounterplanTypeTemplateService;

import java.util.List;
import java.util.Map;

/**
 * 预案类型模板控制器
 *
 * @author xuziyang
 * @Date 2018-01-18 16:11:36
 */
@Controller
@RequestMapping("/counterplanTypeTemplate")
public class CounterplanTypeTemplateController extends BaseController {

    private String PREFIX = "/yjya/counterplanTypeTemplate/";

    @Autowired
    private ICounterplanTypeTemplateService counterplanTypeTemplateService;

    /**
     * 跳转到预案类型模板首页
     */
    @RequestMapping("")
    public String index() {
        return PREFIX + "counterplanTypeTemplate.html";
    }

    /**
     * 跳转到添加预案类型模板
     */
    @RequestMapping("/counterplanTypeTemplate_add")
    public String counterplanTypeTemplateAdd() {
        return PREFIX + "counterplanTypeTemplate_add.html";
    }

    /**
     * 跳转到修改预案类型模板
     */
    @RequestMapping("/counterplanTypeTemplate_update/{counterplanTypeTemplateId}")
    public String counterplanTypeTemplateUpdate(@PathVariable Integer counterplanTypeTemplateId, Model model) {
        CounterplanTypeTemplate counterplanTypeTemplate = counterplanTypeTemplateService.selectById(counterplanTypeTemplateId);
        model.addAttribute("item",counterplanTypeTemplate);
        LogObjectHolder.me().set(counterplanTypeTemplate);
        return PREFIX + "counterplanTypeTemplate_edit.html";
    }

    /**
     * 获取预案类型模板列表
     */
    @RequestMapping(value = "/list")
    @ResponseBody
    public Object list() {
        List<Map<String,Object>> list =  counterplanTypeTemplateService.selectMaps(null);
        return new CounterplanTypeTemplateWarpper(list).warp();
    }

    /**
     * 新增预案类型模板
     */
    @RequestMapping(value = "/add")
    @ResponseBody
    public Object add(CounterplanTypeTemplate counterplanTypeTemplate) {
        counterplanTypeTemplateService.insert(counterplanTypeTemplate);
        return super.SUCCESS_TIP;
    }

    /**
     * 删除预案类型模板
     */
    @RequestMapping(value = "/delete")
    @ResponseBody
    public Object delete(@RequestParam(value = "ids[]") List<Integer> ids) {
        counterplanTypeTemplateService.deleteBatchIds(ids);
        return SUCCESS_TIP;
    }

    /**
     * 修改预案类型模板
     */
    @RequestMapping(value = "/update")
    @ResponseBody
    public Object update(CounterplanTypeTemplate counterplanTypeTemplate) {
        counterplanTypeTemplateService.updateById(counterplanTypeTemplate);
        return super.SUCCESS_TIP;
    }

    @RequestMapping(value = "/getByType")
    @ResponseBody
    public Object getByType(@RequestParam("type") String type) {
        Wrapper wrapper = new EntityWrapper<CounterplanTypeTemplate>();
        wrapper.eq("type", type);
        CounterplanTypeTemplate result = counterplanTypeTemplateService.selectOne(wrapper);
        if (result == null) {
            throw new BussinessException(BizExceptionEnum.DB_RESOURCE_NULL);
        }else {
            return result;
        }
    }

    /**
     * 预案类型模板详情
     */
    @RequestMapping(value = "/detail/{counterplanTypeTemplateId}")
    public String detail(@PathVariable("counterplanTypeTemplateId") Integer counterplanTypeTemplateId,Model model) {
        CounterplanTypeTemplate counterplanTypeTemplate = counterplanTypeTemplateService.selectById(counterplanTypeTemplateId);
        model.addAttribute("item",counterplanTypeTemplate);
        LogObjectHolder.me().set(counterplanTypeTemplate);
        return PREFIX + "counterplanTypeTemplate_detail.html";
    }
}
