package com.stylefeng.guns.modular.assessment.calculate.station.passenger;

import com.stylefeng.guns.common.persistence.model.Assessment;
import com.stylefeng.guns.core.util.DoubleUtil;
import com.stylefeng.guns.modular.assessment.calculate.ThirdIndicationCalculator;
import com.stylefeng.guns.modular.assessment.calculate.aware.StationThirdIndicationAware;
import org.springframework.stereotype.Component;

import java.util.Date;

/**
 * 自动扶梯拥挤度
 */
@Component
public class StationEscalatorCal extends ThirdIndicationCalculator implements StationThirdIndicationAware {
    @Override
    protected Integer getIndicationId() {
        return 14;
    }

    @Override
    public Assessment calculate(Integer stationId, Date beginDate, Date endDate) {
        Double t = getDoubleValue(stationId, 276, beginDate,endDate);
        Double Fmax = getDoubleValue(stationId, 21, beginDate,endDate);
        Double d = getDoubleValue(stationId, 20, beginDate,endDate);
        Double f = getDoubleValue(stationId, 19, beginDate,endDate);

        Double result = DoubleUtil.safeDiv(f, DoubleUtil.mul(Fmax, t, d));
        return genResult(result, stationId, beginDate,endDate);
    }
}
