var AssessmentChar = {
    echarsLineTemplateTemplate: $("#echarsLineTemplate").html(),

}


AssessmentChar.getQueryData = function () {
    var queryData = {};
    queryData['beginDate'] = $("#beginDate").val();
    queryData['endDate'] = $("#endDate").val();
    queryData['roadnetId'] = AssessmentChar.roadnetId;
    return queryData;
};

AssessmentChar.search = function () {

    $.ajax({
        url: Feng.ctxPath+"/assessment/line",
        type: "GET",
        data: AssessmentChar.getQueryData(),
        contentType: "application/json; charset=utf-8",
        dataType: "text",//返回值类型
        success: function (data) {
            $("#echarsLineArea1").empty();
            $("#echarsLineArea2").empty();
            $("#echarsLineArea3").empty();
            var json = eval('(' + data + ')');
            var firstList = json[1];
            var secondList = json[2];
            var thirdList = json[3];
            AssessmentChar.renderLine(firstList,1);
            AssessmentChar.renderLine(secondList,2);
            AssessmentChar.renderLine(thirdList,3);
        },
        error:function(XMLHttpRequest, textStatus, errorThrown){
            var errorData = JSON.parse(XMLHttpRequest.responseText);
            Feng.error("操作失败!" + errorData.message + "!");
        }

    });

}

AssessmentChar.newId = function () {
    if (this.count == undefined) {
        this.count = 0;
    }
    this.count = this.count + 1;
    return "echartsLine" + this.count;
};

AssessmentChar.renderLine = function(optStrAttr,level){
    optStrAttr.forEach(function (optStr) {
        var option = JSON.parse(optStr);
        AssessmentChar.addEcharsLine(option,level);
    });
}


AssessmentChar.addEcharsLine = function (option,level) {
    $("#echarsLineArea"+level).append(this.echarsLineTemplateTemplate);
    var newId = this.newId();
    $("#echartsLine").attr("id", newId);
    var myChart = echarts.init(document.getElementById(newId));
    myChart.setOption(option);
    $(window).resize(myChart.resize);
    return newId;
};

AssessmentChar.onclickRoadnet = function (e, treeId, treeNode) {
    $("#roadnetName").attr("value", treeNode.name);
    $("#roadnetTreeDiv").fadeOut("fast");
    AssessmentChar.roadnetId = treeNode.id;

}

AssessmentChar.showRoadnetSelectTree = function () {
    Feng.showInputTree("roadnetName", "roadnetTreeDiv", 15, 34);
}


$(function () {
    var roadnetTree = new $ZTree("roadnetTree", "/roadnet/assessment/tree");
    roadnetTree.bindOnClick(AssessmentChar.onclickRoadnet);
    roadnetTree.init();

})