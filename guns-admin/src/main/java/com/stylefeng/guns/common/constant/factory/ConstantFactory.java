package com.stylefeng.guns.common.constant.factory;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.mapper.Wrapper;
import com.stylefeng.guns.common.constant.cache.Cache;
import com.stylefeng.guns.common.constant.cache.CacheKey;
import com.stylefeng.guns.common.constant.state.ManagerStatus;
import com.stylefeng.guns.common.constant.state.MenuStatus;
import com.stylefeng.guns.common.persistence.dao.*;
import com.stylefeng.guns.common.persistence.model.*;
import com.stylefeng.guns.core.log.LogObjectHolder;
import com.stylefeng.guns.core.support.StrKit;
import com.stylefeng.guns.core.util.Convert;
import com.stylefeng.guns.core.util.SpringContextHolder;
import com.stylefeng.guns.core.util.ToolUtil;
import com.stylefeng.guns.modular.data.service.IResourceDataTypeService;
import com.stylefeng.guns.modular.system.dao.DictDao;
import com.stylefeng.guns.modular.system.dao.RoadnetDao;
import com.stylefeng.guns.modular.yjya.service.IAccidentTypeService;
import com.stylefeng.guns.modular.yjya.service.IEmergencyService;
import org.apache.commons.lang3.StringUtils;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.context.annotation.DependsOn;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * 常量的生产工厂
 *
 * @author fengshuonan
 * @date 2017年2月13日 下午10:55:21
 */
@Component
@DependsOn("springContextHolder")
public class ConstantFactory implements IConstantFactory {

    private RoleMapper roleMapper = SpringContextHolder.getBean(RoleMapper.class);
    private DeptMapper deptMapper = SpringContextHolder.getBean(DeptMapper.class);
    private DictMapper dictMapper = SpringContextHolder.getBean(DictMapper.class);
    private DictDao dictDao = SpringContextHolder.getBean(DictDao.class);
    private UserMapper userMapper = SpringContextHolder.getBean(UserMapper.class);
    private MenuMapper menuMapper = SpringContextHolder.getBean(MenuMapper.class);
    private NoticeMapper noticeMapper = SpringContextHolder.getBean(NoticeMapper.class);
    private RoadnetMapper roadnetMapper = SpringContextHolder.getBean(RoadnetMapper.class);
    private RoadnetDao roadnetDao = SpringContextHolder.getBean(RoadnetDao.class);
    private IndicationMapper indicationMapper = SpringContextHolder.getBean(IndicationMapper.class);
    private IResourceDataTypeService resourceDataTypeService = SpringContextHolder.getBean(IResourceDataTypeService.class);
    private IAccidentTypeService accidentTypeService = SpringContextHolder.getBean(IAccidentTypeService.class);
    private IEmergencyService emergencyService = SpringContextHolder.getBean(IEmergencyService.class);

    public static IConstantFactory me() {
        return SpringContextHolder.getBean("constantFactory");
    }

    /**
     * 根据用户id获取用户名称
     *
     * @author stylefeng
     * @Date 2017/5/9 23:41
     */
    @Override
    public String getUserNameById(Integer userId) {
        User user = userMapper.selectById(userId);
        if (user != null) {
            return user.getName();
        } else {
            return "--";
        }
    }


    @Override
    @Cacheable(value = Cache.CONSTANT,key = "'" + CacheKey.INDICATION_ID + "'+#indicationId" )
    public String getIndicationNameById(Integer indicationId) {
        Indication indication = indicationMapper.selectById(indicationId);
        if (indication != null) {
            return indication.getName();
        } else {
            return "--";
        }
    }


    /**
     * 根据用户id获取用户账号
     *
     * @author stylefeng
     * @date 2017年5月16日21:55:371
     */
    @Override
    public String getUserAccountById(Integer userId) {
        User user = userMapper.selectById(userId);
        if (user != null) {
            return user.getAccount();
        } else {
            return "--";
        }
    }

    /**
     * 通过角色ids获取角色名称
     */
    @Override
    @Cacheable(value = Cache.CONSTANT, key = "'" + CacheKey.ROLES_NAME + "'+#roleIds")
    public String getRoleName(String roleIds) {
        Integer[] roles = Convert.toIntArray(roleIds);
        StringBuilder sb = new StringBuilder();
        for (int role : roles) {
            Role roleObj = roleMapper.selectById(role);
            if (ToolUtil.isNotEmpty(roleObj) && ToolUtil.isNotEmpty(roleObj.getName())) {
                sb.append(roleObj.getName()).append(",");
            }
        }
        return StrKit.removeSuffix(sb.toString(), ",");
    }

    /**
     * 通过角色id获取角色名称
     */
    @Override
    @Cacheable(value = Cache.CONSTANT, key = "'" + CacheKey.SINGLE_ROLE_NAME + "'+#roleId")
    public String getSingleRoleName(Integer roleId) {
        if (0 == roleId) {
            return "--";
        }
        Role roleObj = roleMapper.selectById(roleId);
        if (ToolUtil.isNotEmpty(roleObj) && ToolUtil.isNotEmpty(roleObj.getName())) {
            return roleObj.getName();
        }
        return "";
    }

    /**
     * 通过角色id获取角色英文名称
     */
    @Override
    @Cacheable(value = Cache.CONSTANT, key = "'" + CacheKey.SINGLE_ROLE_TIP + "'+#roleId")
    public String getSingleRoleTip(Integer roleId) {
        if (0 == roleId) {
            return "--";
        }
        Role roleObj = roleMapper.selectById(roleId);
        if (ToolUtil.isNotEmpty(roleObj) && ToolUtil.isNotEmpty(roleObj.getName())) {
            return roleObj.getTips();
        }
        return "";
    }

    /**
     * 获取部门名称
     */
    @Override
    @Cacheable(value = Cache.CONSTANT, key = "'" + CacheKey.DEPT_NAME + "'+#deptId")
    public String getDeptName(Integer deptId) {
        Dept dept = deptMapper.selectById(deptId);
        if (ToolUtil.isNotEmpty(dept) && ToolUtil.isNotEmpty(dept.getFullname())) {
            return dept.getFullname();
        }
        return "";
    }

    /**
     * 获取菜单的名称们(多个)
     */
    @Override
    public String getMenuNames(String menuIds) {
        Integer[] menus = Convert.toIntArray(menuIds);
        StringBuilder sb = new StringBuilder();
        for (int menu : menus) {
            Menu menuObj = menuMapper.selectById(menu);
            if (ToolUtil.isNotEmpty(menuObj) && ToolUtil.isNotEmpty(menuObj.getName())) {
                sb.append(menuObj.getName()).append(",");
            }
        }
        return StrKit.removeSuffix(sb.toString(), ",");
    }

    /**
     * 获取菜单名称
     */
    @Override
    public String getMenuName(Long menuId) {
        if (ToolUtil.isEmpty(menuId)) {
            return "";
        } else {
            Menu menu = menuMapper.selectById(menuId);
            if (menu == null) {
                return "";
            } else {
                return menu.getName();
            }
        }
    }

    /**
     * 获取菜单名称通过编号
     */
    @Override
    public String getMenuNameByCode(String code) {
        if (ToolUtil.isEmpty(code)) {
            return "";
        } else {
            Menu param = new Menu();
            param.setCode(code);
            Menu menu = menuMapper.selectOne(param);
            if (menu == null) {
                return "";
            } else {
                return menu.getName();
            }
        }
    }

    public List<Dict> getDictByCode(String code) {
        Dict dict = dictDao.selectByCode(code);
        if (dict == null) {
            return new ArrayList();
        }
        return dictDao.selectByPid(dict.getId());
    }

    /**
     * 获取字典名称
     */
    @Override
    public String getDictName(Integer dictId) {
        if (ToolUtil.isEmpty(dictId)) {
            return "";
        } else {
            Dict dict = dictMapper.selectById(dictId);
            if (dict == null) {
                return "";
            } else {
                return dict.getName();
            }
        }
    }

    /**
     * 获取通知标题
     */
    @Override
    public String getNoticeTitle(Integer dictId) {
        if (ToolUtil.isEmpty(dictId)) {
            return "";
        } else {
            Notice notice = noticeMapper.selectById(dictId);
            if (notice == null) {
                return "";
            } else {
                return notice.getTitle();
            }
        }
    }

    public String getDictNameByCode(String pCode, String code) {
        Dict temp = new Dict();
        temp.setCode(pCode);
        Dict dict = dictMapper.selectOne(temp);
        if (dict == null) {
            return "";
        } else {
            Wrapper<Dict> wrapper = new EntityWrapper<>();
            wrapper = wrapper.eq("pid", dict.getId());
            List<Dict> dicts = dictMapper.selectList(wrapper);
            for (Dict item : dicts) {
                if (item.getCode() != null && item.getCode().equals(code)) {
                    return item.getName();
                }
            }
            return "";
        }
    }

    /**
     * 获取性别名称
     */
    @Override
    public String getSexName(String sex) {
        return getDictNameByCode("sex", sex);
    }

    /**
     * 获取用户登录状态
     */
    @Override
    public String getStatusName(Integer status) {
        return ManagerStatus.valueOf(status);
    }

    /**
     * 获取菜单状态
     */
    @Override
    public String getMenuStatusName(Integer status) {
        return MenuStatus.valueOf(status);
    }

    /**
     * 查询字典
     */
    @Override
    public List<Dict> findInDict(Integer id) {
        if (ToolUtil.isEmpty(id)) {
            return null;
        } else {
            EntityWrapper<Dict> wrapper = new EntityWrapper<>();
            List<Dict> dicts = dictMapper.selectList(wrapper.eq("pid", id));
            if (dicts == null || dicts.size() == 0) {
                return null;
            } else {
                return dicts;
            }
        }
    }

    /**
     * 获取被缓存的对象(用户删除业务)
     */
    @Override
    public String getCacheObject(String para) {
        return LogObjectHolder.me().get().toString();
    }

    /**
     * 获取子部门id
     */
    @Override
    public List<Integer> getSubDeptId(Integer deptid) {
        Wrapper<Dept> wrapper = new EntityWrapper<>();
        wrapper = wrapper.like("pids", "%[" + deptid + "]%");
        List<Dept> depts = this.deptMapper.selectList(wrapper);

        ArrayList<Integer> deptids = new ArrayList<>();

        if (depts != null || depts.size() > 0) {
            for (Dept dept : depts) {
                deptids.add(dept.getId());
            }
        }

        return deptids;
    }

    /**
     * 获取所有父部门id
     */
    @Override
    public List<Integer> getParentDeptIds(Integer deptid) {
        Dept dept = deptMapper.selectById(deptid);
        String pids = dept.getPids();
        String[] split = pids.split(",");
        ArrayList<Integer> parentDeptIds = new ArrayList<>();
        for (String s : split) {
            parentDeptIds.add(Integer.valueOf(StrKit.removeSuffix(StrKit.removePrefix(s, "["), "]")));
        }
        return parentDeptIds;
    }


    /**
     * 根据路网id获取路网层级名称
     *
     * @author stylefeng
     * @Date 2018/1/5 14:43
     */
    @Override
    @Cacheable(value = Cache.CONSTANT, key = "'" + CacheKey.ROADNET_ID + "'+#roadnetId")
    public String getRoadnetNameById(Integer roadnetId) {
        if (roadnetId == 0) {
            return "路网";
        }
        Roadnet roadnet = roadnetMapper.selectById(roadnetId);
        if (roadnet != null) {
            return roadnet.getName();
        } else {
            return "--";
        }
    }

    /**
     * 根据事故类型id获取事故类型名称
     *
     * @author stylefeng
     * @Date 2018/07/11 14:43
     */
    @Override
    public String getAccidentTypeNameById(Integer accidentTypeId) {
        if (accidentTypeId == null) {
            return null;
        }
        AccidentType accidentType = accidentTypeService.selectById(accidentTypeId);
        if (accidentType != null) {
            return accidentType.getName();
        } else {
            return "--";
        }
    }


    /**
     * 根据突发事件类型id获取突发事件名称
     *
     * @author stylefeng
     * @Date 2018/07/11 14:43
     */
    @Override
    public String getEmergencyNameById(Integer emergencyId) {
        if (emergencyId == null) {
            return null;
        }
        Emergency emergency = emergencyService.selectById(emergencyId);
        if (emergency != null) {
            return emergency.getName();
        } else {
            return "--";
        }
    }

    /**
     * 根据路网节点名称获取其对应id
     *
     * @param name
     * @return
     */
    @Override
    @Cacheable(value = Cache.CONSTANT, key = "'" + CacheKey.ROADNAME_ID + "'+#name")
    public Integer getRoadnetIdByName(String name){
        if (StringUtils.equals("路网", name)) {
            return 0;
        }
		if (name.contains("-")) {
			String[] names = name.split("-");
			return getStationIdByNameAndLineName(names[1], names[0]);
		}
        Roadnet roadnet = roadnetDao.getRoadnetByName(name);
        if (roadnet == null) {
            return null;
        }
        return roadnet.getId();
    }

    @Override
    public List<Integer> getIsUsedChildrenIdsByPid(Integer pid) {
        List<Roadnet> childrens = roadnetDao.getIsUsedChildrenById(pid);
        List<Integer> childrenIds = childrens.stream().map(Roadnet::getId).collect(Collectors.toList());
        return childrenIds;
    }

    @Override
    public Integer getStationIdByNameAndLineName(String stationName, String lineName) {
        Integer lineId = getRoadnetIdByName(lineName);
        if (lineId != null) return roadnetDao.getStationIdByNameAndLineId(stationName, lineId);
        return null;
    }

    @Override
    @Cacheable(value = Cache.CONSTANT, key = "'" + CacheKey.RESOURCE_DATA_TYPE_ID + "'+#name")
    public ResourceDataType getResourceDataTypeByName(String name) {
        return resourceDataTypeService.selectByName(name);
    }

    @Override
    public String getResourceDataTypeNameById(Integer id) {
        ResourceDataType resourceDataType = resourceDataTypeService.selectById(id);
        if (resourceDataType == null) {
            return "-";
        }
        return resourceDataType.getName();
    }


}
