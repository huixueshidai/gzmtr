package com.stylefeng.guns.common.persistence.model;

import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;
import com.baomidou.mybatisplus.enums.IdType;
import lombok.ToString;

import java.io.Serializable;

/**
 * <p>
 * 预案模板
 * </p>
 *
 * @author xuziyang
 * @since 2018-01-04
 */
@TableName("gzmtr_counterplan_template")
@ToString
public class CounterplanTemplate extends Model<CounterplanTemplate> {

    private static final long serialVersionUID = 1L;

	@TableId(value="id", type= IdType.AUTO)
	private Integer id;
    /**
     * 预案模板名称
     */
	private String name;
    /**
     * 类别
     */
	private String type;
    /**
     * 响应等级
     */
	private String level;

	@TableField("accident_type_id")
	private Integer accidentTypeId;

    /**
     * 内容
     */
	private String content;

	/**
	 * 车站id
	 */
	@TableField("station_id")
	private String stationId;

	/**
	 * 岗位
	 */
	@TableField("accident_main")
	private String accidentMain;


	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getLevel() {
		return level;
	}

	public void setLevel(String level) {
		this.level = level;
	}

	public Integer getAccidentTypeId() {
		return accidentTypeId;
	}

	public void setAccidentTypeId(Integer accidentTypeId) {
		this.accidentTypeId = accidentTypeId;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public String getStationId() {
		return stationId;
	}

	public void setStationId(String stationId) {
		this.stationId = stationId;
	}

	public String getAccidentMain() {
		return accidentMain;
	}

	public void setAccidentMain(String accidentMain) {
		this.accidentMain = accidentMain;
	}

	@Override
	protected Serializable pkVal() {
		return this.id;
	}
}
