package com.stylefeng.guns.common.persistence.model;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

/**
 * Created by 60132 on 2018/4/9.客流量
 */
@Getter
@Setter
@ToString
public class Passenger {

    private Integer passengerOne;

    private Integer passengerTwo;

    private Integer passengerThr;

    private Integer passengerFour;

    private Integer passengerFive;

    private Integer passengerSix;

    private Integer busNum;

}
