package com.stylefeng.guns.modular.yjya.service;

import com.stylefeng.guns.common.persistence.model.CounterplanAccident;
import com.baomidou.mybatisplus.service.IService;

/**
 * <p>
 * 预案处置流程表 服务类
 * </p>
 *
 * @author xuziyang
 * @since 2018-01-22
 */
public interface ICounterplanAccidentService extends IService<CounterplanAccident> {

    void insert(Integer counterplanId, Integer[] accidentIds);

    String getHtml(Integer counterplanId);

    void deleteByCounterplanId(Integer counterplanId);
}
