package com.stylefeng.guns.modular.assessment.calculate.line.facility;

import com.stylefeng.guns.common.persistence.model.Assessment;
import com.stylefeng.guns.core.util.DoubleUtil;
import com.stylefeng.guns.modular.assessment.calculate.ThirdIndicationCalculator;
import com.stylefeng.guns.modular.assessment.calculate.aware.LineThirdIndicationAware;
import org.springframework.stereotype.Component;

import java.util.Date;

/**
 * 低压配电系统影响运营风险指数
 *
 * @author yinjc
 * @since 2018/05/09
 */
@Component
public class LineDistributionSystemInfluenceCal extends ThirdIndicationCalculator implements LineThirdIndicationAware {

    @Override
    protected Integer getIndicationId() {
        return 62;
    }

    @Override
    public Assessment calculate(Integer lineId, Date beginDate, Date endDate) {
        //低压配电系统故障的时间
        Double faultDuration = getDoubleValue(lineId, 208, beginDate,endDate);
        //低压配电总运行时间
        Double totalDuration = getDoubleValue(lineId, 209, beginDate,endDate);

        //受低压配电影响所耽误的列车正线运营里程
        Double delayTheMileage = getDoubleValue(lineId, 210, beginDate,endDate);
        //列车正线计划运营里程数(低压配电)
        Double planTheMileage = getDoubleValue(lineId, 211, beginDate,endDate);

        //线路中低压配电设备发生故障的次数
        Double faultTimes = getDoubleValue(lineId, 212, beginDate,endDate);
        //线路列车运营公里(低压配电)
        Double runMileage = getDoubleValue(lineId, 213, beginDate,endDate);

        double W1 = DoubleUtil.safeDiv(faultDuration, totalDuration);
        double w2 = DoubleUtil.safeDiv(delayTheMileage, planTheMileage);
        double w3 = DoubleUtil.safeDiv(faultTimes, runMileage);
        double he = DoubleUtil.add(W1, w2, w3);
        return genResult(he, lineId, beginDate,endDate);
    }
}
