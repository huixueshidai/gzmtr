package com.stylefeng.guns.modular.trapped.warpper;

import com.stylefeng.guns.common.constant.factory.ConstantFactory;
import com.stylefeng.guns.core.base.warpper.BaseControllerWarpper;
import org.apache.commons.collections.MapUtils;

import java.util.Map;

public class TrappedNumberWarpper extends BaseControllerWarpper{


    public TrappedNumberWarpper(Object obj) {
        super(obj);
    }

    @Override
    protected void warpTheMap(Map<String, Object> map) {
        map.put("field", ConstantFactory.me().getDictNameByCode("fieldType", MapUtils.getString(map,"field")));
        map.put("trapped", ConstantFactory.me().getDictNameByCode("beTrapped", (String) map.get("trapped")));


        map.put("rescuers", ConstantFactory.me().getDictNameByCode("rescuersType", (String) map.get("rescuers")));

        return;
    }
}
