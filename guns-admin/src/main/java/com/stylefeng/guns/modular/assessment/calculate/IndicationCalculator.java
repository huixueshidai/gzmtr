package com.stylefeng.guns.modular.assessment.calculate;

import com.stylefeng.guns.common.persistence.model.Assessment;
import com.stylefeng.guns.core.util.DoubleUtil;
import com.stylefeng.guns.modular.assessment.calculate.exception.CalculateException;
import com.stylefeng.guns.modular.assessment.service.IAssessmentService;
import com.vip.vjtools.vjkit.time.DateFormatUtil;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Date;

import static com.vip.vjtools.vjkit.time.DateFormatUtil.PATTERN_DEFAULT_ON_SECOND;

public abstract class IndicationCalculator {

    @Autowired
    protected IAssessmentService assessmentService;

    protected abstract Integer getIndicationId();

    public Assessment getAssessmentDB(Integer roadnetId, Date beginDate, Date endDate) {
        String beginTime = DateFormatUtil.formatDate(PATTERN_DEFAULT_ON_SECOND, beginDate);
        String endTime = DateFormatUtil.formatDate(PATTERN_DEFAULT_ON_SECOND, endDate);
        return assessmentService.selectOne(getIndicationId(), roadnetId, beginTime, endTime);
    }

    public abstract Assessment calculate(Integer roadnetId, Date beginDate, Date endDate);

    public abstract Assessment get(Integer roadnetId, Date beginDate, Date endDate);


    protected Assessment genResult(Double assessmentValue, Integer roadnetId, Date beginDate, Date endDate) {
		assessmentValue = DoubleUtil.div(assessmentValue, 1.0, 4);
        Assessment assessment = new Assessment(getIndicationId(), roadnetId, assessmentValue, beginDate, endDate);
        return assessment;
    }

}
