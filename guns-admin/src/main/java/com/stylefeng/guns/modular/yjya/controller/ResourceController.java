package com.stylefeng.guns.modular.yjya.controller;

import cn.afterturn.easypoi.entity.vo.NormalExcelConstants;
import cn.afterturn.easypoi.excel.ExcelImportUtil;
import cn.afterturn.easypoi.excel.entity.ExportParams;
import cn.afterturn.easypoi.excel.entity.ImportParams;
import com.stylefeng.guns.common.persistence.model.Resource;
import com.stylefeng.guns.core.base.controller.BaseController;
import com.stylefeng.guns.core.log.LogObjectHolder;
import com.stylefeng.guns.core.shiro.ShiroKit;
import com.stylefeng.guns.modular.yjya.dao.ResourceDao;
import com.stylefeng.guns.modular.yjya.service.IResourceService;
import com.stylefeng.guns.modular.yjya.warpper.ResourceWarpper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * 应急资源控制器
 *
 * @author yinjc
 * @Date 2018-02-23 17:00:33
 */
@Controller
@RequestMapping("/resource")
public class ResourceController extends BaseController {

    private String PREFIX = "/yjya/resource/";

    @Autowired
    private IResourceService resourceService;
    @Autowired
    private ResourceDao resourceDao;

    /**
     * 跳转到应急资源首页
     */
    @RequestMapping("")
    public String index() {
        return PREFIX + "resource.html";
    }

    /**
     * 跳转到添加应急资源
     */
    @RequestMapping("/resource_add")
    public String resourceAdd() {
        return PREFIX + "resource_add.html";
    }

    /**
     * 跳转到修改应急资源
     */
    @RequestMapping("/resource_update/{resourceId}")
    public String resourceUpdate(@PathVariable Integer resourceId, Model model) {
        Resource resource = resourceService.selectById(resourceId);
        model.addAttribute("item",resource);
        LogObjectHolder.me().set(resource);
        return PREFIX + "resource_edit.html";
    }

    /**
     * 获取应急资源列表
     */
    @RequestMapping(value = "/list")
    @ResponseBody
    public Object list(String condition,String location) {
        List<Map<String,Object>> list = this.resourceDao.selectByName(condition,location);
        return new ResourceWarpper(list).warp();
    }

    /**
     * 新增应急资源
     */
    @RequestMapping(value = "/add")
    @ResponseBody
    public Object add(Resource resource) {
        resourceService.insert(resource);
        return super.SUCCESS_TIP;
    }

    /**
     * 删除应急资源
     */
    @RequestMapping(value = "/delete")
    @ResponseBody
    public Object delete(@RequestParam Integer resourceId) {
        resourceService.deleteById(resourceId);
        return SUCCESS_TIP;
    }

    /**
     * 修改应急资源
     */
    @RequestMapping(value = "/update")
    @ResponseBody
    public Object update(Resource resource) {
        resourceService.updateById(resource);
        return super.SUCCESS_TIP;
    }

    /**
     * 应急资源详情
     */
    @RequestMapping(value = "/detail/{resourceId}")
    @ResponseBody
    public Object detail(@PathVariable("resourceId") Integer resourceId) {
        return resourceService.selectById(resourceId);
    }
    /**
     导出Excel模板
     * @param request
     * @param response
    */
    @RequestMapping(value = "/exportXlsByM", headers = {"Accept=application/vnd.ms-excel"})
    public String exportXlsByT(HttpServletRequest request, HttpServletResponse response
            ,ModelMap modelMap) {
        modelMap.put(NormalExcelConstants.FILE_NAME, "应急资源");
        modelMap.put(NormalExcelConstants.CLASS, Resource.class);
        modelMap.put(NormalExcelConstants.PARAMS, new ExportParams("应急资源列表", "导出人:" + ShiroKit.getUser().getName(),
                "导出信息"));
        modelMap.put(NormalExcelConstants.DATA_LIST, new ArrayList());
        return NormalExcelConstants.EASYPOI_EXCEL_VIEW;
    }


    @RequestMapping(method = RequestMethod.POST,path = "/importExcel")
    @ResponseBody
    public String importExcel(MultipartHttpServletRequest multipartHttpServletRequest,HttpServletResponse response)throws Exception{
         Map<String, MultipartFile> fileMap = multipartHttpServletRequest.getFileMap();
         for(Map.Entry<String,MultipartFile> entity : fileMap.entrySet()){
             MultipartFile multipartFile = entity.getValue(); //获取上传文件对象
             ImportParams importParams = new ImportParams();
             importParams.setTitleRows(2);
             importParams.setHeadRows(1);
             importParams.setNeedSave(true);
             List <Resource> list = ExcelImportUtil.importExcel(multipartFile.getInputStream(),Resource.class,importParams);
             resourceService.insertBatch(list);
             multipartFile.getInputStream().close();

         }
        return "文件导入成功";
    }

    @RequestMapping("/resource_sel")
    public String laySel() {
        return PREFIX + "resource_sel.html";
    }

}
