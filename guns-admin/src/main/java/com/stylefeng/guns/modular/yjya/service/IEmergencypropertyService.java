package com.stylefeng.guns.modular.yjya.service;

import com.stylefeng.guns.common.persistence.model.Emergencyproperty;
import com.baomidou.mybatisplus.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author wangshuaikang
 * @since 2018-05-24
 */
public interface IEmergencypropertyService extends IService<Emergencyproperty> {
	
}
