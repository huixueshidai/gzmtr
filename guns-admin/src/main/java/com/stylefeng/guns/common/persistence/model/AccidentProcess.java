package com.stylefeng.guns.common.persistence.model;

import java.io.Serializable;

import cn.afterturn.easypoi.excel.annotation.Excel;
import com.baomidou.mybatisplus.enums.IdType;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableName;
import lombok.Getter;
import lombok.Setter;
import org.springframework.data.annotation.Transient;

/**
 * <p>
 * <p>
 * </p>
 *
 * @author 殷佳成
 * @since 2018-01-17
 */
@TableName("gzmtr_accident_process")
@Getter
@Setter
public class AccidentProcess extends Model<AccidentProcess> {

    private static final long serialVersionUID = 1L;

    /**
     * 主键
     */
    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    /**
     * 分组名称
     */
    @TableField(exist = false)
    @Excel(name = "分组" ,width = 20)
    private String typeName;

    /**
     * 分组
     */
    @TableField("type_id")
    private Integer typeId;
    /**
     * 主体
     */
    @Excel(name = "主体" ,width = 20)
    private String main;
    /**
     * 主体属性
     */
    @Excel(name = "主体属性" ,width = 20)
    private String mproperty;
    /**
     * 主体动作
     */
    @Excel(name = "主体动作" ,width = 20)
    private String mmotion;
    /**
     * 客体
     */
    @Excel(name = "客体" ,width = 20)
    private String object;
    /**
     * 客体属性
     */
    @Excel(name = "客体属性" ,width = 20)
    private String oproperty;
    /**
     * 客体动作
     */
    @Excel(name = "客体动作" ,width = 20)
    private String omotion;


    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getTypeId() {
        return typeId;
    }

    public void setTypeId(Integer typeId) {
        this.typeId = typeId;
    }

    public String getMain() {
        return main;
    }

    public void setMain(String main) {
        this.main = main;
    }

    public String getMproperty() {
        return mproperty;
    }

    public void setMproperty(String mproperty) {
        this.mproperty = mproperty;
    }

    public String getMmotion() {
        return mmotion;
    }

    public void setMmotion(String mmotion) {
        this.mmotion = mmotion;
    }

    public String getObject() {
        return object;
    }

    public void setObject(String object) {
        this.object = object;
    }

    public String getOproperty() {
        return oproperty;
    }

    public void setOproperty(String oproperty) {
        this.oproperty = oproperty;
    }

    public String getOmotion() {
        return omotion;
    }

    public void setOmotion(String omotion) {
        this.omotion = omotion;
    }

    @Override
    protected Serializable pkVal() {
        return this.id;
    }

    @Override
    public String toString() {
        return main + mproperty + mmotion + object + oproperty + omotion;
    }
}
