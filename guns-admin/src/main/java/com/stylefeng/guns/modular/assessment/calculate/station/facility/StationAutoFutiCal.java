package com.stylefeng.guns.modular.assessment.calculate.station.facility;

import com.stylefeng.guns.common.persistence.model.Assessment;
import com.stylefeng.guns.core.util.DoubleUtil;
import com.stylefeng.guns.modular.assessment.calculate.ThirdIndicationCalculator;
import com.stylefeng.guns.modular.assessment.calculate.aware.StationThirdIndicationAware;
import com.stylefeng.guns.modular.assessment.calculate.station.passenger.StationEscalatorCal;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Date;

/**
 * 自动扶梯安全指数
 */
@Component
public class StationAutoFutiCal extends ThirdIndicationCalculator implements StationThirdIndicationAware {

    @Autowired
    private StationEscalatorCal stationEscalatorCal;


    @Override
    protected Integer getIndicationId() {
        return 26;
    }

    @Override
    public Assessment calculate(Integer stationId, Date beginDate, Date endDate) {
        Double stopTime = getDoubleValue(stationId, 44, beginDate,endDate);
        Double totalTime = getDoubleValue(stationId, 45, beginDate,endDate);
        Assessment escalatorAssessment = stationEscalatorCal.calculate(stationId, beginDate,endDate);
        if (totalTime == 0d) {
            return genResult(1d, stationId, beginDate,endDate);
        }
        Double resultValue = DoubleUtil.sub(1, DoubleUtil.mul(
                DoubleUtil.div(stopTime, totalTime),
                escalatorAssessment.getAssessmentValue()
        ));
        return genResult(resultValue, stationId, beginDate,endDate);
    }
}
