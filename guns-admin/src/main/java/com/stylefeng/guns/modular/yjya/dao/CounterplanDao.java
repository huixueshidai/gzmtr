package com.stylefeng.guns.modular.yjya.dao;

import com.baomidou.mybatisplus.plugins.Page;
import com.stylefeng.guns.common.persistence.model.Counterplan;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

/**
 * Created by 60132 on 2018/1/22.
 */
public interface CounterplanDao {
    /**
     *根据条件查询预案列表
     * @param page
     * @param name
     * @param type
     * @param level
     * @return
     */
    List<Map<String, Object>> getCounterplans(@Param("page") Page<Counterplan> page, @Param("name") String name, @Param("type") String type, @Param("level") String level,@Param("accidentTypeId") Integer accidentTypeId);

    /**
     *根据条件查询预案列表
     * @param page
     * @param name
     * @param type
     * @param level
     * @return
     */
    List<Map<String, Object>> getCounterplansHandle(@Param("page") Page<Counterplan> page, @Param("name") String name, @Param("type") String type, @Param("level") String level,@Param("accidentTypeId") Integer accidentTypeId);


    /**
     * 修改用户状态
     *
     * @param counterplanId
     * @date 2017年2月12日 下午8:42:31
     */
    int setStatus(@Param("counterplanId") Integer counterplanId, @Param("status") int status);
}
