/**
 * 初始化预案类型模板详情对话框
 */
var CounterplanTypeTemplateInfoDlg = {
    counterplanTypeTemplateInfoData: {}
    ,ue:null
};

/**
 * 清除数据
 */
CounterplanTypeTemplateInfoDlg.clearData = function () {
    this.counterplanTypeTemplateInfoData = {};
}

/**
 * 设置对话框中的数据
 *
 * @param key 数据的名称
 * @param val 数据的具体值
 */
CounterplanTypeTemplateInfoDlg.set = function (key, val) {
    this.counterplanTypeTemplateInfoData[key] = (typeof val == "undefined") ? $("#" + key).val() : val;
    return this;
}

/**
 * 设置对话框中的数据
 *
 * @param key 数据的名称
 * @param val 数据的具体值
 */
CounterplanTypeTemplateInfoDlg.get = function (key) {
    return $("#" + key).val();
}

/**
 * 关闭此对话框
 */
CounterplanTypeTemplateInfoDlg.close = function () {
    parent.layer.close(window.parent.CounterplanTypeTemplate.layerIndex);
}

/**
 * 收集数据
 */
CounterplanTypeTemplateInfoDlg.collectData = function () {
    this
        .set('id')
        .set('type')
        .set('content', this.ue.getContent())
    ;
}

/**
 * 提交添加
 */
CounterplanTypeTemplateInfoDlg.addSubmit = function () {

    this.clearData();
    this.collectData();

    //提交信息
    var ajax = new $ax(Feng.ctxPath + "/counterplanTypeTemplate/add", function (data) {
        Feng.success("添加成功!");
        window.parent.CounterplanTypeTemplate.table.refresh();
        CounterplanTypeTemplateInfoDlg.close();
    }, function (data) {
        Feng.error("添加失败!" + data.responseJSON.message + "!");
    });
    ajax.set(this.counterplanTypeTemplateInfoData);
    ajax.start();
}

/**
 * 提交修改
 */
CounterplanTypeTemplateInfoDlg.editSubmit = function () {

    this.clearData();
    this.collectData();

    //提交信息
    var ajax = new $ax(Feng.ctxPath + "/counterplanTypeTemplate/update", function (data) {
        Feng.success("修改成功!");
        window.parent.CounterplanTypeTemplate.table.refresh();
        CounterplanTypeTemplateInfoDlg.close();
    }, function (data) {
        Feng.error("修改失败!" + data.responseJSON.message + "!");
    });
    ajax.set(this.counterplanTypeTemplateInfoData);
    ajax.start();
}

$(function () {
    CounterplanTypeTemplateInfoDlg.ue = UE.getEditor('content', {
        autoHeightEnabled: false
        , initialFrameHeight: 500
        , elementPathEnabled:false
    });
});
