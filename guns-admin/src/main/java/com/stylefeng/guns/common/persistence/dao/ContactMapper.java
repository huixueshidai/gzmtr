package com.stylefeng.guns.common.persistence.dao;

import com.stylefeng.guns.common.persistence.model.Contact;
import com.baomidou.mybatisplus.mapper.BaseMapper;

/**
 * <p>
  * 通讯录 Mapper 接口
 * </p>
 *
 * @author xuziyang
 * @since 2017-12-26
 */
public interface ContactMapper extends BaseMapper<Contact> {

}