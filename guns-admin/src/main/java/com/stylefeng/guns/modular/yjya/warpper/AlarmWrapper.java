package com.stylefeng.guns.modular.yjya.warpper;

import com.stylefeng.guns.common.constant.factory.ConstantFactory;
import com.stylefeng.guns.common.persistence.dao.AccidentTypeMapper;
import com.stylefeng.guns.common.persistence.dao.EmergencyMapper;
import com.stylefeng.guns.common.persistence.dao.RoadnetMapper;
import com.stylefeng.guns.common.persistence.model.AccidentType;
import com.stylefeng.guns.common.persistence.model.Emergency;
import com.stylefeng.guns.common.persistence.model.Roadnet;
import com.stylefeng.guns.core.base.warpper.BaseControllerWarpper;
import com.stylefeng.guns.core.util.SpringContextHolder;
import org.apache.commons.collections.MapUtils;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

/**
 * Created by 60132 on 2018/7/11.
 */
public class AlarmWrapper extends BaseControllerWarpper {


    public AlarmWrapper(Object obj) {
        super(obj);
    }

    @Override
    protected void warpTheMap(Map<String, Object> map) {
        if(map.containsKey("accidentTypeId")){
            AccidentType accidentType = SpringContextHolder.getBean(AccidentTypeMapper.class).selectById((Serializable) map.get("accidentTypeId"));
            if (accidentType != null) {
                String pids = accidentType.getPids();
                List<String> ids = new ArrayList<>(Arrays.asList(pids.split("/")));
                ids.add(String.valueOf(accidentType.getId()));
                List<AccidentType> accidentTypeList =  SpringContextHolder.getBean(AccidentTypeMapper.class).selectBatchIds(ids);
                String accideentTypeName = "";
                for(int i=1;i<accidentTypeList.size();i++){
                    accideentTypeName += accidentTypeList.get(i).getName()+"--";
                }
                map.put("accidentTypeName", accideentTypeName+accidentTypeList.get(0).getName());
            }
        }
        else{
            map.put("accidentTypeName", null);
        }
        if(map.containsKey("interruptionType")){
            map.put("interruptionTypeName",ConstantFactory.me().getDictNameByCode("interruption_type",map.get("interruptionType").toString()));
        }
        else{
            map.put("interruptionType",null);
        }
        if(map.containsKey("lineId")){
            map.put("lineName", ConstantFactory.me().getRoadnetNameById((Integer) map.get("lineId")));
        }
        else{
            map.put("lineName", null);
        }
        if(map.containsKey("stationId")){
            String stationIds = MapUtils.getString(map,"stationId");
            List<Integer> stationIdList = new ArrayList(Arrays.asList(stationIds.split("/")));
            List<Roadnet> roadnetList = new ArrayList<>();
            String stationName = "";
            for(int i=0;i<stationIdList.size();i++){
                Roadnet station = SpringContextHolder.getBean(RoadnetMapper.class).selectById(stationIdList.get(i));
                roadnetList.add(station);
            }
            for(int i=0;i<roadnetList.size();i++){
                if(i==roadnetList.size()-1){
                    stationName += roadnetList.get(i).getName();
                }
                else{
                    stationName += roadnetList.get(i).getName()+">>";
                }
            }
            map.put("stationName",stationName);
        }
        else{
            map.put("stationName",null);
        }
        if(map.containsKey("emergencyId")){
            Emergency emergency = SpringContextHolder.getBean(EmergencyMapper.class).selectById(MapUtils.getString(map,"emergencyId"));
            if (emergency != null) {
                String pids = emergency.getPids();
                List<String> ids = new ArrayList<>(Arrays.asList(pids.split("/")));
                ids.add(String.valueOf(emergency.getId()));
                List<Emergency> emergencyList =  SpringContextHolder.getBean(EmergencyMapper.class).selectBatchIds(ids);
                String emergencyName = "";
                for(int i=1;i<emergencyList.size();i++){
                    emergencyName += emergencyList.get(i).getName()+"--";
                }
                map.put("emergencyName", emergencyName+emergencyList.get(0).getName());
            }
        }
        else{
            map.put("emergencyName", null);
        }
        if(map.containsKey("accidentLevel")){
            map.put("accidentLevel", map.get("accidentLevel").toString());
            map.put("accidentLevelName", ConstantFactory.me().getDictNameByCode("responseLevel",  map.get("accidentLevel").toString()));
        }
        else{
            map.put("accidentLevelName", null);
        }
        if(map.containsKey("schedulingMode")){
            map.put("schedulingModeName", ConstantFactory.me().getDictNameByCode("scheduling_mode",  map.get("schedulingMode").toString()));
        }

    }
}