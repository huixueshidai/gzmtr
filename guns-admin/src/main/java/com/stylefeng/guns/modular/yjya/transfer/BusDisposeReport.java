package com.stylefeng.guns.modular.yjya.transfer;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

/**
 * Created by 60132 on 2018/4/13.
 */
@ToString
@Getter
@Setter
public class BusDisposeReport {

    /**
     * 事故单位
     */
    public String accidentName;
    /**
     * 中断地点
     */
    public String accidentPlace;
    /**
     * 发生时间
     */
    public String startTime;
    /**
     * 事故预计结束时间
     */
    public String endTime;
    /**
     * 接驳公交数量
     */
    public String busNum;
    /**
     * 接驳路线
     */
    public String busLine;

}
