package com.stylefeng.guns.modular.roadnet.warpper;

import com.stylefeng.guns.common.constant.factory.ConstantFactory;
import com.stylefeng.guns.core.base.warpper.BaseControllerWarpper;
import org.apache.commons.collections.MapUtils;

import java.util.Map;

/**
 * Created by wangshuaikang on 2018/1/9.
 */
public class RoadnetWarpper extends BaseControllerWarpper {


    public RoadnetWarpper(Object obj) {
        super(obj);
    }

    @Override
    protected void warpTheMap(Map<String, Object> map) {
        map.put("typeName", ConstantFactory.me().getDictNameByCode("roadnetType",MapUtils.getString(map,"type")));
    }
}
