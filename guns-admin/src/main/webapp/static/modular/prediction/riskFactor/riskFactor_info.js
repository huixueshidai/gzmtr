/**
 * 初始化事故致因详情对话框
 */
var RiskFactorInfoDlg = {
    riskFactorInfoData : {}
    , validateFields: {
        name: {
            validators: {
                notEmpty: {
                    message: '名称不能为空'
                }
            }
        },
        pName: {
            validators: {
                notEmpty: {
                    message: '上级不能为空'
                }
            }
        },
        num: {
            validators: {
                notEmpty: {
                    message: '排序不能为空'
                },
                digits: {
                    message: '排序必须为数字'
                }
            }
        }
    }

};

/**
 * 清除数据
 */
RiskFactorInfoDlg.clearData = function() {
    this.riskFactorInfoData = {};
}

/**
 * 设置对话框中的数据
 *
 * @param key 数据的名称
 * @param val 数据的具体值
 */
RiskFactorInfoDlg.set = function(key, val) {
    this.riskFactorInfoData[key] = (typeof val == "undefined") ? $("#" + key).val() : val;
    return this;
}

/**
 * 设置对话框中的数据
 *
 * @param key 数据的名称
 * @param val 数据的具体值
 */
RiskFactorInfoDlg.get = function(key) {
    return $("#" + key).val();
}

/**
 * 关闭此对话框
 */
RiskFactorInfoDlg.close = function() {
    parent.layer.close(window.parent.RiskFactor.layerIndex);
}

/**
 * 收集数据
 */
RiskFactorInfoDlg.collectData = function() {
    this
    .set('id')
    .set('name')
    .set('pid')
    .set('pids')
    .set('num')
    .set('median')
    .set('upper')
        .set('flo')
    ;
}

RiskFactorInfoDlg.validate = function () {
    $('#riskFactorForm').data("bootstrapValidator").resetForm();
    $('#riskFactorForm').bootstrapValidator('validate');
    return $("#riskFactorForm").data('bootstrapValidator').isValid();
}

/**
 * 提交添加
 */
RiskFactorInfoDlg.addSubmit = function() {

    this.clearData();
    this.collectData();

    if (!this.validate()) {
        return;
    }

    //提交信息
    var ajax = new $ax(Feng.ctxPath + "/riskFactor/add", function(data){
        Feng.success("添加成功!");
        window.parent.RiskFactor.table.refresh();
        RiskFactorInfoDlg.close();
    },function(data){
        Feng.error("添加失败!" + data.responseJSON.message + "!");
    });
    ajax.set(this.riskFactorInfoData);
    ajax.start();
}

/**
 * 提交修改
 */
RiskFactorInfoDlg.editSubmit = function() {

    this.clearData();
    this.collectData();

    if (!this.validate()) {
        return;
    }

    //提交信息
    var ajax = new $ax(Feng.ctxPath + "/riskFactor/update", function(data){
        Feng.success("修改成功!");
        window.parent.RiskFactor.table.refresh();
        RiskFactorInfoDlg.close();
    },function(data){
        Feng.error("修改失败!" + data.responseJSON.message + "!");
    });
    ajax.set(this.riskFactorInfoData);
    ajax.start();
}

RiskFactorInfoDlg.showParentSelectTree = function () {
    Feng.showInputTree("pName", "parentTreeDiv", 15, 34);
}

RiskFactorInfoDlg.onClickParent = function (e, treeId, treeNode) {
    $("#pName").attr("value", treeNode.name);
    $("#pid").attr("value", treeNode.id);
    $("#parentTreeDiv").fadeOut("fast");

}

$(function () {
    Feng.initValidator("riskFactorForm", RiskFactorInfoDlg.validateFields);

    var parentTree = new $ZTree("parentTree", "/riskFactor/tree");
    parentTree.bindOnClick(RiskFactorInfoDlg.onClickParent);
    parentTree.init();
});
