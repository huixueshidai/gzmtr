package com.stylefeng.guns.common.persistence.model;

import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;
import com.baomidou.mybatisplus.enums.IdType;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

/**
 * <p>
 * 指标评估
 * </p>
 *
 * @author xuziyang
 * @since 2018-03-27
 */
@TableName("gzmtr_assessment")
@Getter
@Setter
@ToString
@NoArgsConstructor
public class Assessment extends Model<Assessment> {

    private static final long serialVersionUID = 1L;

    public static final int WARNING_LEVEL_LOW = 1;
    public static final int WARNING_LEVEL_LOWER = 2;
    public static final int WARNING_LEVEL_MEDIUM = 3;
    public static final int WARNING_LEVEL_HIGH = 4;
    public static final int WARNING_LEVEL_UNUSUAL = 5;

    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;
    /**
     * 评估指标id
     */
    @TableField("indication_id")
    private Integer indicationId;

    @TableField("roadnet_id")
    private Integer roadnetId;

    /**
     * 评估结果
     */
    @TableField("assessment_value")
    private Double assessmentValue;
    /**
     * 开始时间
     */
    @TableField("begin_time")
    private Date beginTime;
    /**
     * 结束时间
     */
    @TableField("end_time")
    private Date endTime;

    /**
     * 风险等级
     * 0，初始值
     * 1,低风险
     * 2,较低风险
     * 3,中风险
     * 4,高风险
     */
    @TableField(exist = false)
    private int warningLevel = 0;

	@TableField(exist = false)
    private String treeCode;

    @TableField(exist = false)
    private List<Assessment> nexLvlList;


    public Assessment(Integer indicationId, Integer roadnetId, Double assessmentValue, Date beginTime, Date endTime) {
        this.indicationId = indicationId;
        this.roadnetId = roadnetId;
        this.assessmentValue = assessmentValue;
        this.beginTime = beginTime;
        this.endTime = endTime;
    }

    @Override
    protected Serializable pkVal() {
        return this.id;
    }

}
