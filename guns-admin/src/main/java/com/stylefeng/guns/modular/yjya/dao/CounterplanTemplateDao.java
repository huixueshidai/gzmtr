package com.stylefeng.guns.modular.yjya.dao;

import com.baomidou.mybatisplus.plugins.Page;
import com.stylefeng.guns.common.persistence.model.CounterplanTemplate;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

public interface CounterplanTemplateDao {

    List<Map<String, Object>> getCounterplanTemplates(@Param("page") Page<CounterplanTemplate> page, @Param("name") String name, @Param("type") String type, @Param("level") String level,@Param("accidentTypeId") Integer accidentTypeId);
}
