/**
 * 初始化通讯录详情对话框
 */
var ContactInfoDlg = {
    contactInfoData : {},
    validateFields: {
        name: {
            validators: {
                notEmpty: {
                    message: '名称不能为空'
                }
            }
        },
        duty: {
            validators: {
                notEmpty: {
                    message: '职务/专业不能为空'
                }
            }
        },
        mobilePhone: {
            validators: {
                notEmpty: {
                    message: '电话单不能为空'
                },
                digits: {
                    message: '电话必须为数字'
                },
                stringLength: {
                    min: 11,
                    max: 11,
                    message: '请输入11位手机号码'
                },
                regexp: {
                    regexp: /^1[3|5|8]{1}[0-9]{9}$/,
                    message: '请输入正确的手机号码'

                }
            }
        },
        url: {
            validators: {
                notEmpty: {
                    message: '请求地址不能为空'
                }
            }
        },
        email: {
            validators: {
                notEmpty: {
                    message: 'email不能为空'
                },
                emailAddress:{
                    message:'请输入正确的电子邮箱'
                }
            }
        },
        groupSel: {
            validators: {
                notEmpty: {
                    message: '群组不能为空'
                }
            }
        },
        phone: {
            validators: {
                notEmpty: {
                    message: '座机不能为空'
                }
            }
        },
        administrationDuty: {
            validators: {
                notEmpty: {
                    message: '行政职务不能为空'
                }
            }
        }
    }
};

/**
 * 清除数据
 */
ContactInfoDlg.clearData = function() {
    this.contactInfoData = {};
}

/**
 * 设置对话框中的数据
 *
 * @param key 数据的名称
 * @param val 数据的具体值
 */
ContactInfoDlg.set = function(key, val) {
    this.contactInfoData[key] = (typeof val == "undefined") ? $("#" + key).val() : val;
    return this;
}

/**
 * 设置对话框中的数据
 *
 * @param key 数据的名称
 * @param val 数据的具体值
 */
ContactInfoDlg.get = function(key) {
    return $("#" + key).val();
}

/**
 * 关闭此对话框
 */
ContactInfoDlg.close = function() {
    parent.layer.close(window.parent.Contact.layerIndex);
}

/**
 * 收集数据
 */
ContactInfoDlg.collectData = function() {
    this
    .set('id')
    .set('name')
    .set('duty')
    .set('administrationDuty')
    .set('phone')
    .set('groupId')
        .set('email')
        .set('mobilePhone')
    ;
}

/**
 * 验证数据是否为空
 */
ContactInfoDlg.validate = function () {
    $('#contactInfoForm').data("bootstrapValidator").resetForm();
    $('#contactInfoForm').bootstrapValidator('validate');
    return $("#contactInfoForm").data('bootstrapValidator').isValid();
}



ContactInfoDlg.showContactGroupSelectTree = function () {
    var groupObj = $("#groupSel");
    var groupOffset = $("#groupSel").offset();
    $("#menuContent").css({
        left: groupOffset.left + "px",
        top: groupOffset.top + groupObj.outerHeight() + "px"
    }).slideDown("fast");

    $("body").bind("mousedown", onBodyDown);
}

ContactInfoDlg.hideContactGroupSelectTree = function () {
    $("#menuContent").fadeOut("fast");
    $("body").unbind("mousedown", onBodyDown);// mousedown当鼠标按下就可以触发，不用弹起
}

function onBodyDown(event) {
    if (!(event.target.id == "menuBtn" || event.target.id == "menuContent" || $(
            event.target).parents("#menuContent").length > 0)) {
        ContactInfoDlg.hideContactGroupSelectTree();
    }
}


/**
 * 提交添加
 */
ContactInfoDlg.addSubmit = function() {

    this.clearData();
    this.collectData();

    if (!this.validate()) {
        return;
    }

    //提交信息
    var ajax = new $ax(Feng.ctxPath + "/contact/add", function(data){
        Feng.success("添加成功!");
        window.parent.Contact.table.refresh();
        ContactInfoDlg.close();
    },function(data){
        Feng.error("添加失败!" + data.responseJSON.message + "!");
    });
    ajax.set(this.contactInfoData);
    ajax.start();
}

/**
 * 提交修改
 */
ContactInfoDlg.editSubmit = function() {

    this.clearData();
    this.collectData();
    if (!this.validate()) {
        return;
    }

    //提交信息
    var ajax = new $ax(Feng.ctxPath + "/contact/update", function(data){
        Feng.success("修改成功!");
        window.parent.Contact.table.refresh();
        ContactInfoDlg.close();
    },function(data){
        Feng.error("修改失败!" + data.responseJSON.message + "!");
    });
    ajax.set(this.contactInfoData);
    ajax.start();
}

ContactInfoDlg.onClickContactGroup = function (e, treeId, treeNode) {
    $("#groupSel").attr("value", instance.getSelectedVal());
    $("#groupId").attr("value", treeNode.id);
    $("#menuContent").fadeOut("fast");
}


$(function() {

    Feng.initValidator("contactInfoForm", ContactInfoDlg.validateFields);

    var ztree = new $ZTree("treeDemo", "/contactGroup/tree");
    ztree.bindOnClick(ContactInfoDlg.onClickContactGroup);
    ztree.init();
    instance = ztree;
});
