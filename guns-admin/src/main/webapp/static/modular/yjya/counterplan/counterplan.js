/**
 * 管理初始化
 */
var Counterplan = {
    id: "CounterplanTable",	//表格id
    seItem: null,		//选中的条目
    selItems:[],      //选中的多条数目
    table: null,
    layerIndex: -1
};

/**
 * 初始化表格的列
 */
Counterplan.initColumn = function () {
    return [
        {field: 'selectItem', checkbox: true},
        {title: '编码', field: 'id', visible: true, align: 'center', valign: 'middle'},
        {title: '预案名称', field: 'name', visible: true, align: 'center', valign: 'middle'},
        {title: '类别', field: 'type', visible: true, align: 'center', valign: 'middle'},
        {title: '事故类型', field: 'accidentTypeName', visible: true, align: 'center', valign: 'middle'},
        {title: '响应等级', field: 'levelName', visible: true, align: 'center', valign: 'middle'},
        {title: '内容', field: 'content', visible: true, align: 'center', valign: 'middle'},
        {title: '地点', field: 'stationName', visible: true, align: 'center', valign: 'middle'},
        {title: '岗位', field: 'accidentMain', visible: true, align: 'center', valign: 'middle'},
        {title: '状态', field: 'statusName', visible: true, align: 'center', valign: 'middle'}
    ];
};

/**
 * 检查是否选中
 */
Counterplan.check = function () {
    var selected = $('#' + this.id).bootstrapTable('getSelections');
    if (selected.length == 0) {
        Feng.info("请先选中表格中的某一记录！");
        return false;
    } else {
        Counterplan.seItem = selected[0];
        Counterplan.selItems = selected;
        return true;
    }
};

/**
 * 点击添加
 */
Counterplan.openAddCounterplan = function () {
    var index = layer.open({
        type: 2,
        title: '添加',
        area: ['1200px', '600px'], //宽高
        fix: false, //不固定
        maxmin: true,
        content: Feng.ctxPath + '/counterplan/counterplan_add'
    });
    Counterplan.layerIndex = index;
    layer.full(index);
};

/**
 * 修改预案
 */
Counterplan.openCounterplanDetail = function () {
    if (this.check()) {
        if(Counterplan.selItems.length>1){
            Feng.info("仅可以选择一条预案修改！");
            return true;
        }
        if(Counterplan.seItem.status=="1"){
            Feng.info("审核通过的记录不可做修改！");
            return false;
        }
        else{
            var index = layer.open({
                type: 2,
                title: '修改',
                area: ['1200px', '600px'], //宽高
                fix: false, //不固定
                maxmin: true,
                content: Feng.ctxPath + '/counterplan/counterplan_update/' + Counterplan.seItem.id
            });
            Counterplan.layerIndex = index;
            layer.full(index);
        }

    }
};

/**
 * 预览预案
 */
Counterplan.detail = function () {
    if (this.check()) {
        if(Counterplan.selItems.length>1){
            Feng.info("仅可以选择一条预案预览！");
            return true;
        }
        var index = layer.open({
            type: 2,
            title: '预案详情',
            area: ['1200px', '600px'], //宽高
            fix: false, //不固定
            maxmin: true,
            content: Feng.ctxPath + '/counterplan/detail/' + Counterplan.seItem.id
        });
        Counterplan.layerIndex = index;
        layer.full(index);
    }
}

/**
 * 删除
 */
Counterplan.delete = function () {
    if (this.check()) {
        var seItems =  Counterplan.selItems;
        var delIds = [];
        seItems.forEach(function(element){
            delIds.push(element.id);
        });
        var operation = function(){
            var ajax = new $ax(Feng.ctxPath + "/counterplan/delete", function () {
                Feng.success("删除成功!");
                Counterplan.table.refresh();
            }, function (data) {
                Feng.error("删除失败!" + data.responseJSON.message + "!");
            });
            ajax.set("ids", delIds);
            ajax.start();
        };
        Feng.confirm("是否删除选中的预案?" ,operation);
    }
};

/**
 * 审核通过
 * @param counterplanId
 */
Counterplan.approve = function () {
    if (this.check()) {
        var seItems =  Counterplan.selItems;
        var delIds = [];
        seItems.forEach(function(element){
            delIds.push(element.id);
        });
        var operation = function(){
            var ajax = new $ax(Feng.ctxPath + "/counterplan/approve", function () {
                Feng.success("审核通过成功!");
                Counterplan.table.refresh();
            }, function (data) {
                Feng.error("审核通过失败!" + data.responseJSON.message + "!");
            });
            ajax.set("ids", delIds);
            ajax.start();
        };
        Feng.confirm("选中预案审核通过?" ,operation);

    }
};

/**
 * 审核未通过
 * @param counterplanId
 */
Counterplan.unapprove = function () {
    if (this.check()) {
        var seItems =  Counterplan.selItems;
        var delIds = [];
        seItems.forEach(function(element){
            delIds.push(element.id);
        });
        var operation = function(){
            var ajax = new $ax(Feng.ctxPath + "/counterplan/unapprove", function () {
                Feng.success("审核未通过修改成功!");
                Counterplan.table.refresh();
            }, function (data) {
                Feng.error("审核未通过修改失败!" + data.responseJSON.message + "!");
            });
            ajax.set("ids", delIds);
            ajax.start();
        };
        Feng.confirm("选中预案审核未通过?" ,operation);

    }
}

Counterplan.queryParams = function () {
    var queryData = {};
    queryData['name'] = $("#q_name").val();
    queryData['type'] = $("#type").val();
    queryData['level'] = $("#level").val();

    return queryData;
}


/**
 * 查询列表
 */
Counterplan.search = function () {
    var queryData = this.queryParams();
    Counterplan.table.refresh({query: queryData});
};

$(function () {
    var defaultColumns = Counterplan.initColumn();
    var table = new BSTable(Counterplan.id, "/counterplan/list", defaultColumns);
    table.setPaginationType("server");
    Counterplan.table = table.init();
});
