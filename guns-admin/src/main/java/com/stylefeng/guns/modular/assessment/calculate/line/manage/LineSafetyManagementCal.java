package com.stylefeng.guns.modular.assessment.calculate.line.manage;

import com.stylefeng.guns.common.persistence.model.Assessment;
import com.stylefeng.guns.modular.assessment.calculate.ThirdIndicationCalculator;
import com.stylefeng.guns.modular.assessment.calculate.aware.LineThirdIndicationAware;
import org.springframework.stereotype.Component;

import java.util.Date;

/**
 * 线路安全管理综合指数
 */
@Component
public class LineSafetyManagementCal extends ThirdIndicationCalculator implements LineThirdIndicationAware {
    @Override
    protected Integer getIndicationId() {
        return 71;
    }

    @Override
    public Assessment calculate(Integer lineId, Date beginDate, Date endDate) {
        Double fenShu = getDoubleValue(lineId, 260, beginDate,endDate);
        return genResult(fenShu, lineId, beginDate,endDate);
    }
}
