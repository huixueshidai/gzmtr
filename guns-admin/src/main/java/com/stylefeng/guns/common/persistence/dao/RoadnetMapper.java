package com.stylefeng.guns.common.persistence.dao;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.stylefeng.guns.common.persistence.model.Roadnet;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

/**
 * <p>
  *  Mapper 接口
 * </p>
 *
 * @author wangshuaikang
 * @since 2018-01-04
 */
public interface RoadnetMapper extends BaseMapper<Roadnet> {

    List<Roadnet> selectRoadetList(@Param("startStation")String  startStation, @Param("endStation")String  endStation,@Param("pid")Integer  pid);

    List<Roadnet> selectRoadetListBehind(@Param("star")Integer  star,@Param("pid")Integer  pid);

    List<Roadnet> selectRoadetListAfter(@Param("end")Integer  end,@Param("pid")Integer  pid);

    Integer selelctByNmae(@Param("name")String  name,@Param("pid")Integer  pid);


}