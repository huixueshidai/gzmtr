package com.stylefeng.guns.modular.assessment.calculate.line.facility;

import com.stylefeng.guns.core.util.SpringContextHolder;
import com.stylefeng.guns.modular.assessment.calculate.SecondIndicationCalculator;
import com.stylefeng.guns.modular.assessment.calculate.ThirdIndicationCalculator;
import com.stylefeng.guns.modular.assessment.calculate.aware.LineSecondIndicationAware;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import java.util.Arrays;
import java.util.List;

@Component
@Scope(ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public class LineFacilityCal extends SecondIndicationCalculator implements LineSecondIndicationAware {

    private List<ThirdIndicationCalculator> thirdIndicationCalculatorList;

    public LineFacilityCal() {
        thirdIndicationCalculatorList = Arrays.asList(
                SpringContextHolder.getBean(LineBeaconageCal.class),
                SpringContextHolder.getBean(LineCivilEngineeringImpactCal.class),
                SpringContextHolder.getBean(LineCommunicationSystemInfluenceCal.class),
                SpringContextHolder.getBean(LineDistributionSystemInfluenceCal.class),
                SpringContextHolder.getBean(LineDragPowerCal.class),
                SpringContextHolder.getBean(LineInfluenceOfElectromechanicalSystemCal.class),
                SpringContextHolder.getBean(LineOtherDiagnosisCal.class),
                SpringContextHolder.getBean(LineScreenDoorSystemCal.class),
                SpringContextHolder.getBean(LineTheWiringSystemAffectsTransportationCal.class),
                SpringContextHolder.getBean(LineVehicleSystemCal.class)
        );
    }

    @Override
    public List<ThirdIndicationCalculator> getThirdIndicationCalculatorList() {
        return thirdIndicationCalculatorList;
    }


    @Override
    protected Integer getIndicationId() {
        return 51;
    }
}
