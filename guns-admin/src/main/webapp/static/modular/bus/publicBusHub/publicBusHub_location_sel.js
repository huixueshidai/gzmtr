/**
 * 初始化公交站枢纽信息详情对话框
 */
var PublicBusHubLocationSel = {
    marker:null,
    coordinate:$("#location").val()//获取当前公交枢纽的坐标
};

/**
 * 关闭此对话框
 */
PublicBusHubLocationSel.close = function() {
    parent.layer.close(window.parent.PublicBusHub.layerIndex);
}
/**
 * 收集数据
 */
PublicBusHubLocationSel.collectData = function() {
    this
        .set('id')
        .set('location')
    ;
}

PublicBusHubLocationSel.updateLocation = function () {
    var p = this.marker.getPosition();       //获取marker的位置
    console.log("marker的位置是" + p.lng + "," + p.lat);
    var location = p.lng + "," + p.lat;
    var hubId = window.parent.PublicBusHub.seItem.id;

    //提交信息
    var ajax = new $ax(Feng.ctxPath + "/publicBusHub/update", function(data){
        Feng.success("修改成功!");
        window.parent.PublicBusHub.table.refresh();
        PublicBusHubLocationSel.close();
    },function(data){
        Feng.error("修改失败!" + data.responseJSON.message + "!");
    });

    ajax.set('id', hubId);
    ajax.set('location', location);
    ajax.start();
}

$(function() {
    var myStyleJson=[
        {
            "featureType": "subway",
            "elementType": "labels",
            "stylers": {}
        },
        {
            "featureType": "railway",
            "elementType": "labels.icon",
            "stylers": {
                "color": "#76a5afff"
            }
        }
    ];

    var map = new BMap.Map("container");
    map.setMapStyle({styleJson: myStyleJson });

    var x = PublicBusHubLocationSel.coordinate.split(',');
    // 创建地图实例
    // var point = new BMap.Point(parseFloat(x[0])+','+parseFloat(x[1]));
    var point = new BMap.Point(113.25352,23.126657);
    // 创建点坐标
    map.centerAndZoom(point, 15);
    // 初始化地图，设置中心点坐标和地图级别
    map.setCurrentCity("广州");          // 设置地图显示的城市 此项是必须设置的
    map.enableScrollWheelZoom(true);     //开启鼠标滚轮缩放
    var marker = new BMap.Marker(point);  // 创建标注
    map.addOverlay(marker);
    marker.enableDragging();
    PublicBusHubLocationSel.marker = marker;
});
