package com.stylefeng.guns.common.persistence.dao;

import com.stylefeng.guns.common.persistence.model.Law;
import com.baomidou.mybatisplus.mapper.BaseMapper;

/**
 * <p>
  *  Mapper 接口
 * </p>
 *
 * @author 殷佳成
 * @since 2018-01-18
 */
public interface LawMapper extends BaseMapper<Law> {

}