var Warning = {
    data: null,
    warningLevel1: 0.3,
    warningLevel2: 0.9,
    warningLevel3: 1.5,
    subWayMap: null,
};

/**
 * 查询安全状态预警列表
 */
Warning.selectByDate = function () {
    var stations = null;
    var ajax = new $ax(Feng.ctxPath + "/warning/getWarning", function (data) {
        this.data = data;
    }, function (data) {
        Feng.error("Error!" + data.responseJSON.message + "!");
    });

    ajax.setData(Warning.getQueryData());
    ajax.start();

    Warning.twinkle();
};

Warning.resetMap = function () {
    this.subWayMap.scale(0.5);
}

Warning.getQueryData = function () {
    var queryData = {};
    queryData['date'] = $("#date").val();
    return queryData;
};

Warning.twinkle = function () {
    $.each(this.data, function (idx, station) {
        var _id = station.roadnetId;
        var _type = station.type;
        //警告等级
        var _warningLevel = station.warningLevel;
        if (_warningLevel <= this.warningLevel1) {
            _warningLevel = 1;
        } else if (_warningLevel <= this.warningLevel2) {
            _warningLevel = 2;
        } else if (_warningLevel <= this.warningLevel3) {
            _warningLevel = 3;
        } else {
            _warningLevel = 4;
        }
        if (_type == 3) {
            submayMap.addCustomNode(station.name, {customClass: "custom_circle fill_Level" + _warningLevel});
        } else if (_type == 2) {
            //闪烁整条线路

            // subwayMap.showline(station.name) ;
        }
    });
};

Warning.selectByDate = function () {
    //清空点标记
    $(".custom_circle").remove();
    $(".subwayLine").removeClass("subwayLine");
    var date = $("#date").val();
    if (!date) {
        Feng.info("请选择日期!");
        return;
    }

    var ajax = new $ax(Feng.ctxPath + "/assessmentAnalogy/getWarning", function (data) {

        data['3'].forEach(function (value) {
            console.log(value.level);
            Warning.subWayMap.addCustomNode(value.name, {customClass: "custom_circle fill_Level" + value.level});
        });

        data['2'].forEach(function (value) {
            $("#line-" + Warning.subWayMap.getIdByName(value.name)).addClass("subwayLineLevel" + value.level);
        });


    }, function (data) {
        Feng.error("失败,计算数据不符合要求!");
    });
    ajax.set("date", date);
    ajax.start();

}

$(function () {


    function clearTwinkle() {
        //清空点标记
        $(".custom_circle").remove();
        $(".subwayLine").removeClass("subwayLine");

    }

    //开启easy模式, 直接完成地铁图基本功能, 无需自己写交互
    window.cbk = function () {
        var mySubway = subway("mybox", {
            adcode: 4401,
            theme: "colorful",
            client: 0,
            easy: true,
            doubleclick: {
                switch: true
            }
        });
        Warning.subWayMap = mySubway;

        mySubway.event.on("subway.complete", function (ev, info) {
            var id = info.id;
            mySubway.scale(0.5);
            mySubway.addMarker('900000043839002', {
                customClass: 'circle warning-circle',

                height: 30,
                offset: {
                    x: 8,
                    y: 44
                },
                cnt: ""
            });
            // $("#440100023037006").attr('fill','#09f');
            // $("#440100023037006").addClass('warning-circle');
            // $("#440100023308018")
        });

        //点击站点，显示此站点的信息窗体
        mySubway.event.on("station.touch", function (ev, info) {
            var id = info.id;
            mySubway.stopAnimation();
            mySubway.addInfoWindow(id, {});
            var center = mySubway.getStCenter(id);
            mySubway.setCenter(center);
        });

        //点击线路名，高亮此线路
        mySubway.event.on("lineName.touch", function (ev, info) {
            mySubway.showLine(info.id);
            var select_obj = qs('#g-select');
            mySubway.setFitView(select_obj);
            var center = mySubway.getSelectedLineCenter();
            mySubway.setCenter(center);
        });

        //点击空白, 关闭infowindow
        mySubway.event.on("subway.touch", function () {
            mySubway.clearInfoWindow();
        });
    };
})