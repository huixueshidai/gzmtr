package com.stylefeng.guns.modular.fire.service.impl;

import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.stylefeng.guns.common.persistence.model.FireSource;
import com.stylefeng.guns.modular.fire.dao.FireSourceMapper;
import com.stylefeng.guns.modular.fire.service.IFireSourceService;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 火源情景预算 服务实现类
 * </p>
 *
 * @author xhq
 * @since 2018-09-03
 */
@Service
public class FireSourceServiceImpl extends ServiceImpl<FireSourceMapper, FireSource> implements IFireSourceService {
	
}
