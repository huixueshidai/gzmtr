package com.stylefeng.guns.modular.assessment.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class Warning {

    private String name;

    private Integer level;

	private String type;
}
