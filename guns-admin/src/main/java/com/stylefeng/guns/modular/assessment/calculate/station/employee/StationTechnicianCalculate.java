package com.stylefeng.guns.modular.assessment.calculate.station.employee;

import com.stylefeng.guns.common.persistence.model.Assessment;
import com.stylefeng.guns.core.util.DoubleUtil;
import com.stylefeng.guns.modular.assessment.calculate.ThirdIndicationCalculator;
import com.stylefeng.guns.modular.assessment.calculate.aware.StationThirdIndicationAware;
import org.springframework.stereotype.Component;

import java.util.Date;

/**
 * 员工技术水平达标指数
 */
@Component
public class StationTechnicianCalculate extends ThirdIndicationCalculator implements StationThirdIndicationAware {

    @Override
    protected Integer getIndicationId() {
        return 23;
    }

    @Override
    public Assessment calculate(Integer stationId, Date beginDate, Date endDate) {
        double totalNum = getDoubleValue(stationId, 277, beginDate,endDate);
        double num = getDoubleValue(stationId, 39, beginDate,endDate);
        double resValue = DoubleUtil.safeDiv(num, totalNum);
        return genResult(resValue, stationId, beginDate,endDate);
    }

}
