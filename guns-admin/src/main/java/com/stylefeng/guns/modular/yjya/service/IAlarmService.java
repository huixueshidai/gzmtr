package com.stylefeng.guns.modular.yjya.service;

import com.stylefeng.guns.common.persistence.model.Alarm;
import com.baomidou.mybatisplus.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author wsk
 * @since 2018-07-10
 */
public interface IAlarmService extends IService<Alarm> {
	
}
