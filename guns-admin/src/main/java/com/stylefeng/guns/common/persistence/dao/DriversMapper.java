package com.stylefeng.guns.common.persistence.dao;

import com.stylefeng.guns.common.persistence.model.Drivers;
import com.baomidou.mybatisplus.mapper.BaseMapper;

/**
 * <p>
  *  Mapper 接口
 * </p>
 *
 * @author wangshuaikang
 * @since 2018-09-04
 */
public interface DriversMapper extends BaseMapper<Drivers> {

}