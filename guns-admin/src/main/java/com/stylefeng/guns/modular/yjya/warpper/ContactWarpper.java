package com.stylefeng.guns.modular.yjya.warpper;

import com.stylefeng.guns.common.persistence.dao.ContactGroupMapper;
import com.stylefeng.guns.common.persistence.model.ContactGroup;
import com.stylefeng.guns.core.base.warpper.BaseControllerWarpper;
import com.stylefeng.guns.core.util.SpringContextHolder;

import java.io.Serializable;
import java.util.Map;

public class ContactWarpper extends BaseControllerWarpper {

    public ContactWarpper(Object obj) {
        super(obj);
    }

    @Override
    protected void warpTheMap(Map<String, Object> map) {
        ContactGroupMapper contactGroupMapper = SpringContextHolder.getBean(ContactGroupMapper.class);
        ContactGroup contactGroup = contactGroupMapper.selectById((Serializable) map.get("groupId"));
        if (contactGroup != null) {
            map.put("groupName", contactGroup.getName());
        }
    }
}
