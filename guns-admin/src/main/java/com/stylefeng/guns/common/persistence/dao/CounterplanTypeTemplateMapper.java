package com.stylefeng.guns.common.persistence.dao;

import com.stylefeng.guns.common.persistence.model.CounterplanTypeTemplate;
import com.baomidou.mybatisplus.mapper.BaseMapper;

/**
 * <p>
  * 预案类型模板 Mapper 接口
 * </p>
 *
 * @author xuziyang
 * @since 2018-01-18
 */
public interface CounterplanTypeTemplateMapper extends BaseMapper<CounterplanTypeTemplate> {

}