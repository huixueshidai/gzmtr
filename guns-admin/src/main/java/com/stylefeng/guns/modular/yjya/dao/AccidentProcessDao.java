package com.stylefeng.guns.modular.yjya.dao;

import com.baomidou.mybatisplus.plugins.Page;
import com.stylefeng.guns.common.persistence.model.AccidentProcess;
import com.stylefeng.guns.common.persistence.model.OperationLog;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

/**
 * Created by 60132 on 2018/2/6.
 */
public interface AccidentProcessDao {

    List<Map<String,Object>> selectProcesses(@Param("page") Page<AccidentProcess> page, @Param("condition")String  condition, @Param("typeId")Integer typeId);
}
