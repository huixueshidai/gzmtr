package com.stylefeng.guns.modular.assessment.calculate.station.manage;

import com.stylefeng.guns.common.persistence.model.Assessment;
import com.stylefeng.guns.core.util.DoubleUtil;
import com.stylefeng.guns.modular.assessment.calculate.ThirdIndicationCalculator;
import com.stylefeng.guns.modular.assessment.calculate.aware.StationThirdIndicationAware;
import com.stylefeng.guns.modular.assessment.calculate.exception.CalculateException;
import org.springframework.stereotype.Component;

import java.util.Date;

/**
 * 车站应急疏散能力指数
 */
@Component
public class StationEvacuationCal extends ThirdIndicationCalculator implements StationThirdIndicationAware {

    @Override
    protected Integer getIndicationId() {
        return 45;
    }

    @Override
    public Assessment calculate(Integer stationId, Date beginDate, Date endDate)  {

		double q1 = getDoubleValue(stationId, 135, beginDate,endDate);
		double q2 = getDoubleValue(stationId, 136, beginDate,endDate);

        double escalatorCapacity = getDoubleValue(stationId, 138, beginDate,endDate);
        double stairCapacity = getDoubleValue(stationId, 139, beginDate,endDate);
		double escalatorTotal = getDoubleValue(stationId, 140, beginDate,endDate);
		double stairWidth = getDoubleValue(stationId, 141, beginDate,endDate);

		double denominator = DoubleUtil.mul(0.9, DoubleUtil
				.add(DoubleUtil.mul(escalatorCapacity, DoubleUtil.sub(escalatorTotal, 1)),
						DoubleUtil.mul(stairCapacity, stairWidth)));

		if (denominator == 0) {
			throw new CalculateException("denominator zero");
		}

		double resultVal = DoubleUtil.add(1,DoubleUtil.div(
				DoubleUtil.add(q1,q2),
				denominator
		));

		return genResult(resultVal, stationId, beginDate,endDate);
    }

}
