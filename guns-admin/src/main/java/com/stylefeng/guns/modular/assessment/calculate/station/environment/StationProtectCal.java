package com.stylefeng.guns.modular.assessment.calculate.station.environment;

import com.stylefeng.guns.common.persistence.model.Assessment;
import com.stylefeng.guns.core.util.DoubleUtil;
import com.stylefeng.guns.modular.assessment.calculate.ThirdIndicationCalculator;
import com.stylefeng.guns.modular.assessment.calculate.aware.StationThirdIndicationAware;
import org.springframework.stereotype.Component;

import java.util.Date;

/**
 * 地铁保护指数
 */
@Component
public class StationProtectCal extends ThirdIndicationCalculator implements StationThirdIndicationAware {

    @Override
    protected Integer getIndicationId() {
        return 42;
    }

    @Override
    public Assessment calculate(Integer stationId, Date beginDate, Date endDate) {
        Double passengerNum = getDoubleValue(stationId, 132, beginDate, endDate);
        Double extraValue = getEventValue(stationId, beginDate, endDate, 112, 122, passengerNum);
        Double firstValue = getEventValue(stationId, beginDate, endDate, 113, 123, passengerNum);
        Double secondValue = getEventValue(stationId, beginDate, endDate, 114, 124, passengerNum);
        Double thirdValue = getEventValue(stationId, beginDate, endDate, 115, 125, passengerNum);
        Double fourValue = getEventValue(stationId, beginDate, endDate, 116, 126, passengerNum);
        Double aboveFourValue = getEventValue(stationId, beginDate, endDate, 117, 127, passengerNum);

        Double a = getEventValue(stationId, beginDate, endDate, 118, 128, passengerNum);
        Double b = getEventValue(stationId, beginDate, endDate, 119, 129, passengerNum);
        Double c = getEventValue(stationId, beginDate, endDate, 120, 130, passengerNum);
        Double d = getEventValue(stationId, beginDate, endDate, 121, 131, passengerNum);

        Double resultValue = DoubleUtil.sub(1, DoubleUtil.add(extraValue, firstValue, secondValue, thirdValue, fourValue, aboveFourValue, a, b, c, d));

        return genResult(resultValue, stationId, beginDate, endDate);
    }

    /**
     * @param stationId          车站id
     * @param beginDate          日期
     * @param resourceDataTypeId 源数据id
     * @param weightDataTypeId   源数据所对应的权重的源数据id
     * @param passengerNum       乘客数量
     * @return
     */
    private Double getEventValue(Integer stationId, Date beginDate, Date endDate, Integer resourceDataTypeId, Integer weightDataTypeId, Double passengerNum) {
        Double time = getDoubleValue(stationId, resourceDataTypeId, beginDate, endDate);
        Double weight = getDoubleValue(stationId, weightDataTypeId, beginDate, endDate);
        return DoubleUtil.safeDiv(DoubleUtil.mul(time, weight), passengerNum);
    }

}
