package com.stylefeng.guns.common.persistence.dao;

import com.stylefeng.guns.common.persistence.model.ResourceData;
import com.baomidou.mybatisplus.mapper.BaseMapper;

import java.util.List;
import java.util.Map;

/**
 * <p>
  * 源数据 Mapper 接口
 * </p>
 *
 * @author xuziyang
 * @since 2018-01-17
 */
public interface ResourceDataMapper extends BaseMapper<ResourceData> {

}