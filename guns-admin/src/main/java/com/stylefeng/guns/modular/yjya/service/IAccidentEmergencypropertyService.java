package com.stylefeng.guns.modular.yjya.service;

import com.stylefeng.guns.common.persistence.model.AccidentEmergencyproperty;
import com.baomidou.mybatisplus.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author wangshuaikang
 * @since 2018-05-29
 */
public interface IAccidentEmergencypropertyService extends IService<AccidentEmergencyproperty> {
	
}
