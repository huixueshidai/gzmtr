package com.stylefeng.guns.modular.path.service.impl;

import com.stylefeng.guns.common.persistence.model.PathEvolution;
import com.stylefeng.guns.modular.path.dao.PathEvolutionMapper;
import com.stylefeng.guns.modular.path.service.IPathEvolutionService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 事故情景路径演练 服务实现类
 * </p>
 *
 * @author xhq
 * @since 2018-09-04
 */
@Service
public class PathEvolutionServiceImpl extends ServiceImpl<PathEvolutionMapper, PathEvolution> implements IPathEvolutionService {
	
}
