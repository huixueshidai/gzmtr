/**
 * 管理初始化
 */
var Emergency = {
    id: "EmergencyTable",	//表格id
    seItem: null,		//选中的条目
    table: null,
    layerIndex: -1
};

/**
 * 初始化表格的列
 */
Emergency.initColumn = function () {
    return [
        {field: 'selectItem', radio: true},
            {title: 'id', field: 'id', visible: true, align: 'center', valign: 'middle',width:'50px'},
            {title: '突发事件名称', field: 'name', visible: true, align: 'center', valign: 'middle'},
            {title: '排序', field: 'num', visible: true, align: 'center', valign: 'middle'}
    ];
};

/**
 * 检查是否选中
 */
Emergency.check = function () {
    var selected = $('#' + this.id).bootstrapTreeTable('getSelections');
    if(selected.length == 0){
        Feng.info("请先选中表格中的某一记录！");
        return false;
    }else{
        Emergency.seItem = selected[0];
        return true;
    }
};

/**
 * 点击添加
 */
Emergency.openAddEmergency = function () {
    var index = layer.open({
        type: 2,
        title: '添加',
        area: ['800px', '420px'], //宽高
        fix: false, //不固定
        maxmin: true,
        content: Feng.ctxPath + '/emergency/emergency_add'
    });
    this.layerIndex = index;
};

/**
 * 打开查看详情
 */
Emergency.openEmergencyDetail = function () {
    if (this.check()) {
        var index = layer.open({
            type: 2,
            title: '详情',
            area: ['800px', '420px'], //宽高
            fix: false, //不固定
            maxmin: true,
            content: Feng.ctxPath + '/emergency/emergency_update/' + Emergency.seItem.id
        });
        this.layerIndex = index;
    }
};

/**
 * 删除
 */
Emergency.delete = function () {
    if (this.check()) {
        var ajax = new $ax(Feng.ctxPath + "/emergency/delete", function (data) {
            Feng.success("删除成功!");
            Emergency.table.refresh();
        }, function (data) {
            Feng.error("删除失败!" + data.responseJSON.message + "!");
        });
        ajax.set("emergencyId",this.seItem.id);
        ajax.start();
    }
};

/**
 * 查询列表
 */
Emergency.search = function () {
    var queryData = {};
    queryData['condition'] = $("#condition").val();
    Emergency.table.refresh({query: queryData});
};

$(function () {
    var defaultColunms = Emergency.initColumn();
    var table = new BSTreeTable(Emergency.id, "/emergency/list", defaultColunms);
    table.setExpandColumn(2);
    table.setIdField("id");
    table.setCodeField("id");
    table.setParentCodeField("pid");
    table.setExpandAll(true);
    table.init();
    Emergency.table = table;
});
