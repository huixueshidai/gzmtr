/**
 * 初始化法律法规详情对话框
 */
var LawInfoDlg = {
    lawInfoData : {},
    validateFields: {
        name: {
            validators: {
                notEmpty: {
                    message: '名称不能为空'
                }
            }
        }
    }
};

/**
 * 清除数据
 */
LawInfoDlg.clearData = function() {
    this.lawInfoData = {};
}

/**
 * 设置对话框中的数据
 *
 * @param key 数据的名称
 * @param val 数据的具体值
 */
LawInfoDlg.set = function(key, val) {
    this.lawInfoData[key] = (typeof val == "undefined") ? $("#" + key).val() : val;
    return this;
}

/**
 * 设置对话框中的数据
 *
 * @param key 数据的名称
 * @param val 数据的具体值
 */
LawInfoDlg.get = function(key) {
    return $("#" + key).val();
}

/**
 * 关闭此对话框
 */
LawInfoDlg.close = function() {
    parent.layer.close(window.parent.Law.layerIndex);
}

/**
 * 收集数据
 */
LawInfoDlg.collectData = function() {
    this
    .set('id')
        .set('name')
    ;
}
LawInfoDlg.validate = function () {
    $('#lawInfoForm').data("bootstrapValidator").resetForm();
    $('#lawInfoForm').bootstrapValidator('validate');
    return $("#lawInfoForm").data('bootstrapValidator').isValid();
}

/**
 * 提交添加
 */
LawInfoDlg.addSubmit = function() {

    this.clearData();
    this.collectData();
    if (!this.validate()) {
        return;
    }

    //提交信息
    var ajax = new $ax(Feng.ctxPath + "/law/add", function(data){
        Feng.success("添加成功!");
        window.parent.Law.table.refresh();
        LawInfoDlg.close();
    },function(data){
        Feng.error("添加失败!" + data.responseJSON.message + "!");
    });
    ajax.set(this.lawInfoData);
    ajax.start();
}

/**
 * 提交修改
 */
LawInfoDlg.editSubmit = function() {

    this.clearData();
    this.collectData();
    if (!this.validate()) {
        return;
    }

    //提交信息
    var ajax = new $ax(Feng.ctxPath + "/law/update", function(data){
        Feng.success("修改成功!");
        window.parent.Law.table.refresh();
        LawInfoDlg.close();
    },function(data){
        Feng.error("修改失败!" + data.responseJSON.message + "!");
    });
    ajax.set(this.lawInfoData);
    ajax.start();
}

$(function() {
    LawInfoDlg.ue = UE.getEditor('contente', {
        autoHeightEnabled: false
        , initialFrameHeight: 300
        ,elementPathEnabled:false//不显示输入框下的路径信息
    });

    Feng.initValidator("lawInfoForm", LawInfoDlg.validateFields);

});
