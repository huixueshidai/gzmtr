package com.stylefeng.guns.common.persistence.dao;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.stylefeng.guns.common.persistence.model.ResourceDataType;

/**
 * <p>
  * 源数据类型 Mapper 接口
 * </p>
 *
 * @author xuziyang
 * @since 2018-01-15
 */
public interface ResourceDataTypeMapper extends BaseMapper<ResourceDataType> {

}