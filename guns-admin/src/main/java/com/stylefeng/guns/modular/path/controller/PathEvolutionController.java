package com.stylefeng.guns.modular.path.controller;

import com.baomidou.mybatisplus.plugins.Page;
import com.stylefeng.guns.common.constant.factory.PageFactory;
import com.stylefeng.guns.common.persistence.model.PathEvolution;
import com.stylefeng.guns.core.base.controller.BaseController;
import com.stylefeng.guns.core.log.LogObjectHolder;
import com.stylefeng.guns.modular.path.dao.PathEvolutionMapper;
import com.stylefeng.guns.modular.path.service.IPathEvolutionService;
import com.stylefeng.guns.modular.path.warpper.pathEvolutionWarpper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;
import java.util.Map;

/**
 * 事故情景路径演练控制器
 *
 * @author xhq
 * @Date 2018-09-04 11:47:41
 */
@Controller
@RequestMapping("/pathEvolution")
public class PathEvolutionController extends BaseController {

    private String PREFIX = "/path/pathEvolution/";

    @Autowired
    private IPathEvolutionService pathEvolutionService;


    @Autowired
    private PathEvolutionMapper pathEvolutionMapper;


    /**
     * 跳转到事故情景路径演练首页
     */
    @RequestMapping("")
    public String index() {
        return PREFIX + "pathEvolution.html";
    }


    @RequestMapping("volution")
	public String volution() {
    	return PREFIX + "volution.html";
	}


    /**
     * 跳转到添加事故情景路径演练
     */
    @RequestMapping("/pathEvolution_add")
    public String pathEvolutionAdd() {
        return PREFIX + "index.html";
    }

    /**
     * 跳转到修改事故情景路径演练
     */
    @RequestMapping("/pathEvolution_update/{pathEvolutionId}")
    public String pathEvolutionUpdate(@PathVariable Integer pathEvolutionId, Model model) {
        PathEvolution pathEvolution = pathEvolutionService.selectById(pathEvolutionId);
        model.addAttribute("item",pathEvolution);
        LogObjectHolder.me().set(pathEvolution);
        return PREFIX + "pathEvolution_edit.html";
    }

    /**
     * 获取事故情景路径演练列表
     */
    @RequestMapping(value = "/list")
    @ResponseBody
    public Object list(String condition) {
        Page<PathEvolution> page = new PageFactory<PathEvolution>().defaultPage();
        List<Map<String,Object>> retMap =  pathEvolutionMapper.selectMap(page);
        page.setRecords((List<PathEvolution>) new pathEvolutionWarpper(retMap).warp());
        return super.packForBT(page);
//        return pathEvolutionService.selectList(null);
    }

    /**
     * 新增事故情景路径演练
     */
    @RequestMapping(value = "/add")
    @ResponseBody
    public Object add(PathEvolution pathEvolution) {
        System.out.println("*****************:");

       /* EntityWrapper<FireSource> ewf = new EntityWrapper<FireSource>();
        ewf.where("material",pathEvolution.getMaterial()).and("field",pathEvolution.getField())
                .and("rescuers",pathEvolution.getRescuers());
        FireSource FireSource = fireSourceService.selectOne(ewf);
        String[] fsArray = FireSource.getResult().split(",");

            //ConstantFactory.me().getDictNameByCode("","1");
        EntityWrapper<TrappedNumber> ewt = new EntityWrapper<>();
//        ewt.where("trapped",pathEvolution.getTrapped()).and("field",pathEvolution.getField())
//                .and("rescuers",pathEvolution.getRescuers());
        ewt.where("field",pathEvolution.getField())
                .and("rescuers",pathEvolution.getRescuers()).ge("trapped_end",pathEvolution.getTrapped())
        .le("trapped_start",pathEvolution.getTrapped());
        System.out.println("*****************:"+ewt.getSqlSegment());
        TrappedNumber trappedNumber = trappedNumberService.selectOne(ewt);
        String trString = trappedNumber.getDischarge1()+"-"+trappedNumber.getDischarge2()+"-"+trappedNumber.getDischarge3()+"-"+trappedNumber.getDischarge4()+"-"+trappedNumber.getDischarge5();
        String[] trArray = trString.split("-");
        String[] array = new String[10];
        Map<String,Map<String,String>> retMap = new HashMap<String,Map<String,String>>();
        Map mapT= new HashMap();//mapT4是最佳路径概率
        Map mapF= new HashMap();

        for (int i = 0; i < fsArray.length-1; i++) {
            String hz = fsArray[i];
            for (int j = 0; j < trArray.length; j++) {
                System.out.println(hz);
                BigDecimal bd1 = new BigDecimal(hz);
                BigDecimal bd2 = new BigDecimal(trArray[j]);
                BigDecimal cheng = bd1.multiply(bd2);
                System.out.println(hz+"**"+trArray[j]+"="+cheng);
                mapT.put(j,cheng);
            }
        }
        System.out.println("mapT:"+mapT);
        //第二次遍历取未熄灭的概率
        for (int i = 1; i < fsArray.length; i++) {
            String hz = fsArray[i];
            for (int j = 0; j < trArray.length; j++) {
                BigDecimal bd1 = new BigDecimal(hz);
                BigDecimal bd2 = new BigDecimal(trArray[j]);
                BigDecimal cheng = bd1.multiply(bd2);
                System.out.println(hz);
                System.out.println(hz+"**"+trArray[j]+"="+cheng);
                mapF.put(j,cheng);
            }
        }
        System.out.println("mapF:"+mapF);
        pathEvolutionService.insert(pathEvolution);
        return super.SUCCESS_TIP;*/

        return PREFIX + "pathDemonstration.html";
    }

    /**
     * 删除事故情景路径演练
     */
    @RequestMapping(value = "/delete")
    @ResponseBody
    public Object delete(@RequestParam Integer pathEvolutionId) {
        pathEvolutionService.deleteById(pathEvolutionId);
        return SUCCESS_TIP;
    }

    /**
     * 修改事故情景路径演练
     */
    @RequestMapping(value = "/update")
    @ResponseBody
    public Object update(PathEvolution pathEvolution) {
        pathEvolutionService.updateById(pathEvolution);
        return super.SUCCESS_TIP;
    }

    /**
     * 事故情景路径演练详情
     */
    @RequestMapping(value = "/detail/{pathEvolutionId}")
    @ResponseBody
    public Object detail(@PathVariable("pathEvolutionId") Integer pathEvolutionId) {
        return pathEvolutionService.selectById(pathEvolutionId);
    }
}
