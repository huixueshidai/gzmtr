package com.stylefeng.guns.modular.roadnet.service;

import com.stylefeng.guns.common.persistence.model.Roadnet;
import com.stylefeng.guns.common.plugin.service.IBaseTreeableService;

import java.util.List;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author wangshuaikang
 * @since 2018-01-04
 */
public interface IRoadnetService extends IBaseTreeableService<Roadnet> {
	List<Roadnet> selectByType(Integer type);
}
