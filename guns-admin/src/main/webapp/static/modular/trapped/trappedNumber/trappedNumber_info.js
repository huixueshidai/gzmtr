/**
 * 初始化被困人员录入详情对话框
 */
var TrappedNumberInfoDlg = {
    trappedNumberInfoData : {}
};

/**
 * 清除数据
 */
TrappedNumberInfoDlg.clearData = function() {
    this.trappedNumberInfoData = {};
}

/**
 * 设置对话框中的数据
 *
 * @param key 数据的名称
 * @param val 数据的具体值
 */
TrappedNumberInfoDlg.set = function(key, val) {
    this.trappedNumberInfoData[key] = (typeof val == "undefined") ? $("#" + key).val() : val;
    return this;
}

/**
 * 设置对话框中的数据
 *
 * @param key 数据的名称
 * @param val 数据的具体值
 */
TrappedNumberInfoDlg.get = function(key) {
    return $("#" + key).val();
}

/**
 * 关闭此对话框
 */
TrappedNumberInfoDlg.close = function() {
    parent.layer.close(window.parent.TrappedNumber.layerIndex);
}

/**
 * 收集数据
 */
TrappedNumberInfoDlg.collectData = function() {
    this
    .set('id')
    .set('field')
    .set('rescuers')
    .set('trapped')
    .set('discharge1')
    .set('discharge2')
    .set('discharge3')
    .set('discharge4')
        .set('discharge5')
    ;
}

/**
 * 提交添加
 */
TrappedNumberInfoDlg.addSubmit = function() {

    this.clearData();
    this.collectData();

    //提交信息
    var ajax = new $ax(Feng.ctxPath + "/trappedNumber/add", function(data){
        Feng.success("添加成功!");
        window.parent.TrappedNumber.table.refresh();
        TrappedNumberInfoDlg.close();
    },function(data){
        Feng.error("添加失败!" + data.responseJSON.message + "!");
    });
    ajax.set(this.trappedNumberInfoData);
    ajax.start();
}

/**
 * 提交修改
 */
TrappedNumberInfoDlg.editSubmit = function() {

    this.clearData();
    this.collectData();

    //提交信息
    var ajax = new $ax(Feng.ctxPath + "/trappedNumber/update", function(data){
        Feng.success("修改成功!");
        window.parent.TrappedNumber.table.refresh();
        TrappedNumberInfoDlg.close();
    },function(data){
        Feng.error("修改失败!" + data.responseJSON.message + "!");
    });
    ajax.set(this.trappedNumberInfoData);
    ajax.start();
}

$(function() {

});
