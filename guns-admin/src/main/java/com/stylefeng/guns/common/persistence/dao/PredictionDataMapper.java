package com.stylefeng.guns.common.persistence.dao;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.stylefeng.guns.common.persistence.model.PredictionData;

import java.util.List;
import java.util.Map;

/**
 * <p>
  * 风险预测数据 Mapper 接口
 * </p>
 *
 * @author xuziyang
 * @since 2018-05-19
 */
public interface PredictionDataMapper extends BaseMapper<PredictionData> {
    List<Map<String, Object>> selectPredictionData();
}