<div class="form-group">
    <label class="col-sm-3 control-label">${name}</label>
    <div class="col-sm-9">
        <select  class="form-control" id="${id}" name="${id}"
                @if(isNotEmpty(onchangeFun)){
                onchange="${onchangeFun}"
                @}
                 @if(isNotEmpty(style)){
                 style="${style}"
                 @}
                 @if(isNotEmpty(disabled)){
                 disabled="${disabled}"
                 @}
        >
            <option value>请选择</option>
            @for(item in dict.getByCode(code)){
                @if(isNotEmpty(value) && value ==  item.code){
                    <option value="${item.code}" selected="selected">${item.name}</option>
                @}else{
                    <option value="${item.code}" >${item.name}</option>
                @}
            @}
        </select>
        @if(isNotEmpty(hidden)){
        <input class="form-control" type="hidden" id="${hidden}" value="${hiddenValue}">
        @}
    </div>
</div>
@if(isNotEmpty(underline) && underline == 'true'){
<div class="hr-line-dashed"></div>
@}