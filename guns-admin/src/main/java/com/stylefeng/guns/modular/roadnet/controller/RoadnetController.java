package com.stylefeng.guns.modular.roadnet.controller;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.mapper.Wrapper;
import com.stylefeng.guns.common.constant.cache.Cache;
import com.stylefeng.guns.common.constant.factory.ConstantFactory;
import com.stylefeng.guns.common.persistence.dao.RoadnetMapper;
import com.stylefeng.guns.common.persistence.model.Roadnet;
import com.stylefeng.guns.core.base.controller.BaseController;
import com.stylefeng.guns.core.cache.CacheKit;
import com.stylefeng.guns.core.log.LogObjectHolder;
import com.stylefeng.guns.core.node.ZTreeNode;
import com.stylefeng.guns.core.util.ToolUtil;
import com.stylefeng.guns.modular.roadnet.service.IRoadnetService;
import com.stylefeng.guns.modular.roadnet.warpper.RoadnetWarpper;
import com.stylefeng.guns.modular.system.dao.RoadnetDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

import static java.util.stream.Collectors.toList;

/**
 * 控制器
 *
 * @author fengshuonan
 * @Date 2018-01-04 15:57:04
 */
@Controller
@RequestMapping("/roadnet")
public class RoadnetController extends BaseController {

    private String PREFIX = "/roadnet/roadnet/";

    @Resource
    RoadnetDao roadnetDao;

    @Resource
    RoadnetMapper roadnetMapper;

    @Autowired
    private IRoadnetService roadnetService;

    /**
     * 跳转到首页
     */
    @RequestMapping("")
    public String index() {
        return PREFIX + "roadnet.html";
    }

    /**
     * 跳转到添加
     */
    @RequestMapping("/roadnet_add")
    public String roadnetAdd() {
        return PREFIX + "roadnet_add.html";
    }

    /**
     * 跳转到修改
     */
    @RequestMapping("/roadnet_update/{roadnetId}")
    public String roadnetUpdate(@PathVariable Integer roadnetId, Model model) {
        Roadnet roadnet = roadnetService.selectById(roadnetId);
        model.addAttribute("item", roadnet);
        model.addAttribute("typeName", ConstantFactory.me().getRoadnetNameById(roadnet.getPid()));
        LogObjectHolder.me().set(roadnet);
        return PREFIX + "roadnet_edit.html";
    }

    /**
     * 获取列表
     */
    @RequestMapping(value = "/list")
    @ResponseBody
    public Object list(@RequestParam(value = "name", required = false) String name) {
        List<Map<String, Object>> result = roadnetDao.getRoadnets(name);
        return new RoadnetWarpper(result).warp();
    }

    @RequestMapping("/assessment/tree")
    @ResponseBody
    public List<ZTreeNode> assessmentTree() {
        List<ZTreeNode> res = new ArrayList<>();

        Wrapper<Roadnet> ew = new EntityWrapper<>();
        ew.where("is_used = 1");
        List<Roadnet> list = this.roadnetService.selectList(ew);
        List<ZTreeNode> treeNodeList = list.stream()
                .map(roadnet -> new ZTreeNode(roadnet.getId() + "", roadnet.getPid() + "", roadnet.getName(), false, false))
                .collect(toList());

        res.addAll(treeNodeList);
        res.add(ZTreeNode.createParent());
        return res;
    }

    /**
     * @return
     */
    @RequestMapping(value = "/line/tree")
    @ResponseBody
    public List<ZTreeNode> lineTree() {
        Wrapper<Roadnet> wrapper = new EntityWrapper<>();
        wrapper.where("type != {0}", "3").orderBy("num");
        return roadnetService.tree(wrapper);
    }

    @RequestMapping(value = "/station/tree/{lineId}")
    @ResponseBody
    public List<ZTreeNode> stationTree(@PathVariable("lineId") Integer lineId) {
        Wrapper<Roadnet> wrapper = new EntityWrapper<>();
        wrapper.where("type = {0} and pid = {1}", "3", lineId).orderBy("num");
        return roadnetService.tree(wrapper);
    }

    /**
     * 获取路网的tree列表
     */
    @RequestMapping(value = "/tree")
    @ResponseBody
    public List<ZTreeNode> tree() {
        Wrapper<Roadnet> wrapper = new EntityWrapper<>();
        wrapper.orderBy("num");
        return roadnetService.tree(wrapper);
    }


    /**
     * 获取路网的tree列表（且复选框标记）
     */
    @RequestMapping(value = "/tree/{stationIds}")
    @ResponseBody
    public List<ZTreeNode> treeCheck(@PathVariable String stationIds) {

        Wrapper<Roadnet> wrapper = new EntityWrapper<>();
        wrapper.orderBy("num");
        List<Integer> stationIdList = new ArrayList(Arrays.asList(stationIds.split(",")));
        List<ZTreeNode> tree = roadnetService.tree(wrapper);
//        Boolean a = String.valueOf(stationIdList.get(0)).equals(tree.get(j).getId());
        for(int i =0;i<stationIdList.size();i++){
            for(int j = 0;j<tree.size();j++){
                if((String.valueOf(stationIdList.get(i))).equals(tree.get(j).getId())){
                    tree.get(j).setChecked(true);
                }
            }
        }
        return tree;
    }


    /**
     * 新增
     */
    @RequestMapping(value = "/add")
    @ResponseBody
    public Object add(Roadnet roadnet) {
        roadnetService.addNode(roadnet);
        return super.SUCCESS_TIP;
    }

    /**
     * 删除
     */
    @RequestMapping(value = "/delete")
    @ResponseBody
    public Object delete(@RequestParam Integer roadnetId) {
        roadnetService.deleteSelfAndChild(roadnetId);
        CacheKit.removeAll(Cache.CONSTANT);
        return SUCCESS_TIP;
    }

    /**
     * 修改
     */
    @RequestMapping(value = "/update")
    @ResponseBody
    public Object update(Roadnet roadnet) {
        roadnetService.updateNode(roadnet);
        CacheKit.removeAll(Cache.CONSTANT);
        return super.SUCCESS_TIP;
    }

    /**
     * 详情
     */
    @RequestMapping(value = "/detail/{roadnetId}")
    @ResponseBody
    public Object detail(@PathVariable("roadnetId") Integer roadnetId) {
        return roadnetService.selectById(roadnetId);
    }

}
