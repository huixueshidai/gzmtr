package com.stylefeng.guns.modular.yjya.service.impl;

import com.stylefeng.guns.common.persistence.model.ContactGroup;
import com.stylefeng.guns.common.persistence.dao.ContactGroupMapper;
import com.stylefeng.guns.common.plugin.service.impl.BaseTreeableServiceImpl;
import com.stylefeng.guns.modular.yjya.service.IContactGroupService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * <p>
 * 通讯录群组 服务实现类
 * </p>
 *
 * @author xuziyang
 * @since 2017-12-26
 */
@Service
public class ContactGroupServiceImpl extends BaseTreeableServiceImpl<ContactGroupMapper,ContactGroup> implements IContactGroupService {

}
