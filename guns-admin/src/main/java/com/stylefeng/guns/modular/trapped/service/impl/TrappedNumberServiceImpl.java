package com.stylefeng.guns.modular.trapped.service.impl;

import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.stylefeng.guns.common.persistence.model.TrappedNumber;
import com.stylefeng.guns.modular.trapped.dao.TrappedNumberMapper;
import com.stylefeng.guns.modular.trapped.service.ITrappedNumberService;
import org.springframework.stereotype.Service;


/**
 * <p>
 * 被困人员录入 服务实现类
 * </p>
 *
 * @author xhq
 * @since 2018-09-04
 */
@Service
public class TrappedNumberServiceImpl extends ServiceImpl<TrappedNumberMapper, TrappedNumber> implements ITrappedNumberService {
	
}
