package com.stylefeng.guns.modular.yjya.service.impl;

import com.stylefeng.guns.common.persistence.model.AccidentEmergencyproperty;
import com.stylefeng.guns.common.persistence.dao.AccidentEmergencypropertyMapper;
import com.stylefeng.guns.modular.yjya.service.IAccidentEmergencypropertyService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author wangshuaikang
 * @since 2018-05-29
 */
@Service
public class AccidentEmergencypropertyServiceImpl extends ServiceImpl<AccidentEmergencypropertyMapper, AccidentEmergencyproperty> implements IAccidentEmergencypropertyService {
	
}
