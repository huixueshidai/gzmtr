package com.stylefeng.guns.modular.yjya.dao;

import com.baomidou.mybatisplus.plugins.Page;
import com.stylefeng.guns.common.persistence.model.Contact;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

public interface ContactDao {
   List<Map<String,Object>> selectContacts(@Param("page") Page<Contact> page , @Param("name") String name, @Param("groupId") Integer groupId, @Param("administrationDuty") String administrationDuty);

   List<Map<String,Object>> selectContactsNotPage(@Param("name") String name, @Param("groupId") Integer groupId, @Param("administrationDuty") String administrationDuty);
}
