package com.stylefeng.guns.modular.assessment.calculate.line.employee;

import com.stylefeng.guns.common.persistence.model.Assessment;
import com.stylefeng.guns.core.util.DoubleUtil;
import com.stylefeng.guns.modular.assessment.calculate.ThirdIndicationCalculator;
import com.stylefeng.guns.modular.assessment.calculate.aware.LineThirdIndicationAware;
import org.springframework.stereotype.Component;

import java.util.Date;

/**
 * 员工“两纪一化”指数
 *
 * @author yinjc
 * @since now
 */
@Component
public class LineTwoJiACal extends ThirdIndicationCalculator implements LineThirdIndicationAware {
    @Override
    protected Integer getIndicationId() {
        return 57;
    }

    @Override
    public Assessment calculate(Integer lineId, Date beginDate, Date endDate) {
        Double wentiNum = getDoubleValue(lineId, 185, beginDate,endDate);
        Double totalNum = getDoubleValue(lineId, 186, beginDate,endDate);
        double resValue = DoubleUtil.safeDiv(wentiNum, totalNum);
        return genResult(resValue, lineId, beginDate,endDate);
    }
}
