package com.stylefeng.guns.modular.yjya.warpper;

import com.stylefeng.guns.common.constant.factory.ConstantFactory;
import com.stylefeng.guns.common.persistence.dao.AccidentTypeMapper;
import com.stylefeng.guns.common.persistence.dao.RoadnetMapper;
import com.stylefeng.guns.common.persistence.dao.UserMapper;
import com.stylefeng.guns.common.persistence.model.Roadnet;
import com.stylefeng.guns.common.persistence.model.User;
import com.stylefeng.guns.core.base.warpper.BaseControllerWarpper;
import com.stylefeng.guns.core.util.SpringContextHolder;
import org.apache.commons.collections.MapUtils;
import org.springframework.beans.factory.annotation.Autowired;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

/**
 * Created by 60132 on 2018/9/4.
 */
public class PublicbusWarpper extends BaseControllerWarpper {

    public PublicbusWarpper(Object obj) {
        super(obj);
    }

    @Override
    protected void warpTheMap(Map<String, Object> map) {

        if(map.containsKey("level")){
            map.put("level", ConstantFactory.me().getDictNameByCode("responseLevel",  map.get("level").toString()));
        }
        if(map.containsKey("user")){
            map.put("user", ConstantFactory.me().getUserNameById((Integer)map.get("user")));
        }


        if(map.containsKey("stationName")){
            String lineName = "";
            String stationIds = MapUtils.getString(map,"stationName");
            List<Integer> stationIdList = new ArrayList(Arrays.asList(stationIds.split("/")));
            List<Roadnet> roadnetList = new ArrayList<>();
            String stationName = "";
            for(int i=0;i<stationIdList.size();i++){
                Roadnet station = SpringContextHolder.getBean(RoadnetMapper.class).selectById(stationIdList.get(i));
                roadnetList.add(station);
                Integer LineId = station.getPid();
                lineName = SpringContextHolder.getBean(RoadnetMapper.class).selectById(LineId).getName();
            }
            for(int i=0;i<roadnetList.size();i++){
                if(i==roadnetList.size()-1){
                    stationName += roadnetList.get(i).getName();
                }
                else{
                    stationName += roadnetList.get(i).getName()+">>";
                }
            }
            map.put("stationName",lineName+":"+stationName);
        }
    }
}
