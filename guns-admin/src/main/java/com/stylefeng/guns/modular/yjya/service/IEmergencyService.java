package com.stylefeng.guns.modular.yjya.service;

import com.stylefeng.guns.common.persistence.model.Emergency;
import com.baomidou.mybatisplus.service.IService;
import com.stylefeng.guns.common.plugin.service.IBaseTreeableService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author wangshuaikang
 * @since 2018-05-23
 */
public interface IEmergencyService extends IBaseTreeableService<Emergency> {
	
}
