package com.stylefeng.guns.modular.assessment.calculate.station.facility;

import com.stylefeng.guns.common.persistence.model.Assessment;
import com.stylefeng.guns.core.util.DoubleUtil;
import com.stylefeng.guns.modular.assessment.calculate.ThirdIndicationCalculator;
import com.stylefeng.guns.modular.assessment.calculate.aware.StationThirdIndicationAware;
import org.apache.commons.collections.CollectionUtils;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * 安检系统
 */
@Component
public class StationSecurityCheckCal extends ThirdIndicationCalculator implements StationThirdIndicationAware {
    @Override
    protected Integer getIndicationId() {
        return 33;
    }

    @Override
    public Assessment calculate(Integer stationId, Date beginDate, Date endDate) {
        Double t = getDoubleValue(stationId, 63, beginDate,endDate);
        Double T = getDoubleValue(stationId, 64, beginDate,endDate);
        Double d = getDoubleValue(stationId, 65, beginDate,endDate);
        Double D = getDoubleValue(stationId, 360, beginDate,endDate);
        Double n = getDoubleValue(stationId, 66, beginDate,endDate);
        Double N = getDoubleValue(stationId, 278, beginDate,endDate);
        List<Double> wList = getMulDoubleValue(stationId, 279, beginDate,endDate);
        if (CollectionUtils.isEmpty(wList) || wList.size() != 3) {
            wList = new ArrayList<>(3);
            wList.add(0.33d);
            wList.add(0.34d);
            wList.add(0.33d);
        }

        Double result = DoubleUtil.add(
                DoubleUtil.mul(wList.get(0), DoubleUtil.safeDiv(t, T)),
                DoubleUtil.mul(wList.get(1), DoubleUtil.safeDiv(d, D)),
                DoubleUtil.mul(wList.get(2), DoubleUtil.safeDiv(n, N))
        );

        return genResult(result, stationId, beginDate,endDate);
    }
}
