package com.stylefeng.guns.modular.yjya.controller;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.stylefeng.guns.common.persistence.model.AccidentType;
import com.stylefeng.guns.core.base.controller.BaseController;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.beans.factory.annotation.Autowired;
import com.stylefeng.guns.core.log.LogObjectHolder;
import org.springframework.web.bind.annotation.RequestParam;
import com.stylefeng.guns.common.persistence.model.Law;
import com.stylefeng.guns.modular.yjya.service.ILawService;

import java.sql.Wrapper;

/**
 * 法律法规控制器
 *
 * @author 殷佳成
 * @Date 2018-01-18 09:57:15
 */
@Controller
@RequestMapping("/law")
public class LawController extends BaseController {

    private String PREFIX = "/yjya/law/";

    @Autowired
    private ILawService lawService;

    /**
     * 跳转到法律法规首页
     */
    @RequestMapping("")
    public String index() {
        return PREFIX + "law.html";
    }

    /**
     * 跳转到添加法律法规
     */
    @RequestMapping("/law_add")
    public String lawAdd() {
        return PREFIX + "law_add.html";
    }

    /**
     * 跳转到修改法律法规
     */
    @RequestMapping("/law_update/{lawId}")
    public String lawUpdate(@PathVariable Integer lawId, Model model) {
        Law law = lawService.selectById(lawId);
        model.addAttribute("item",law);
        LogObjectHolder.me().set(law);
        return PREFIX + "law_edit.html";
    }

    @RequestMapping("/law_sel")
    public String laySel() {
        return PREFIX + "law_sel.html";
    }

    @RequestMapping("/law_sel_admin")
    public String laySelAdmin() {
        return PREFIX + "law_sel_admin.html";
    }

    /**
     * 获取法律法规列表
     */
    @RequestMapping(value = "/list")
    @ResponseBody
    public Object list(String condition) {
        com.baomidou.mybatisplus.mapper.Wrapper<Law> wrapper = new EntityWrapper<>();
        wrapper.like("name", condition);
        return lawService.selectList(wrapper);
    }

    /**
     * 新增法律法规
     */
    @RequestMapping(value = "/add")
    @ResponseBody
    public Object add(Law law) {
        lawService.insert(law);
        return super.SUCCESS_TIP;
    }

    /**
     * 删除法律法规
     */
    @RequestMapping(value = "/delete")
    @ResponseBody
    public Object delete(@RequestParam Integer lawId) {
        lawService.deleteById(lawId);
        return SUCCESS_TIP;
    }

    /**
     * 修改法律法规
     */
    @RequestMapping(value = "/update")
    @ResponseBody
    public Object update(Law law) {
        lawService.updateById(law);
        return super.SUCCESS_TIP;
    }

    /**
     * 法律法规详情
     */
    @RequestMapping(value = "/detail/{lawId}")
    @ResponseBody
    public Object detail(@PathVariable("lawId") Integer lawId) {
        return lawService.selectById(lawId);
    }
}
