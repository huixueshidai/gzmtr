package com.stylefeng.guns.common.persistence.dao;

import com.stylefeng.guns.common.persistence.model.PublicbusStarup;
import com.baomidou.mybatisplus.mapper.BaseMapper;
import org.apache.ibatis.annotations.Param;

/**
 * <p>
  *  Mapper 接口
 * </p>
 *
 * @author wangshuaikang
 * @since 2018-09-09
 */
public interface PublicbusStarupMapper extends BaseMapper<PublicbusStarup> {

    PublicbusStarup selectByUserId(@Param("userId") Integer userId);
}