package com.stylefeng.guns.modular.yjya.service;

import com.stylefeng.guns.common.persistence.model.CounterplanTemplate;
import com.baomidou.mybatisplus.service.IService;

/**
 * <p>
 * 预案模板 服务类
 * </p>
 *
 * @author xuziyang
 * @since 2018-01-04
 */
public interface ICounterplanTemplateService extends IService<CounterplanTemplate> {

}
