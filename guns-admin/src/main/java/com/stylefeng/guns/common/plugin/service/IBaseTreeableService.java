package com.stylefeng.guns.common.plugin.service;

import com.baomidou.mybatisplus.mapper.Wrapper;
import com.baomidou.mybatisplus.service.IService;
import com.stylefeng.guns.common.plugin.entity.BaseModel;
import com.stylefeng.guns.common.plugin.entity.Treeable;
import com.stylefeng.guns.core.node.ZTreeNode;

import java.util.List;
import java.util.Set;

public interface IBaseTreeableService<T extends BaseModel & Treeable> extends IService<T> {

    void deleteSelfAndChild(Integer id);

    void updateNode(T t);

    List<ZTreeNode> tree();

    List<ZTreeNode> tree(Wrapper<T> wrapper);

    Integer addNode(T t);

    Set<Integer> findChildrenIds(Integer parentId);
}
