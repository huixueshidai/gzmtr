package com.stylefeng.guns.generator.engine;

import com.stylefeng.guns.core.util.ToolUtil;
import com.stylefeng.guns.generator.engine.base.GunsTemplateEngine;

public class TreeTemplateEngine extends GunsTemplateEngine {

    @Override
    protected void generatePageEditHtml() {

    }

    @Override
    protected void generatePageAddHtml() {

    }

    @Override
    protected void generatePageInfoJs() {

    }

    @Override
    protected void generatePageJs() {

    }

    @Override
    protected void generatePageHtml() {

    }

    @Override
    protected void generateController() {
        String controllerPath = ToolUtil.format(super.getContextConfig().getProjectPath() + super.getControllerConfig().getControllerPathTemplate(),
                ToolUtil.firstLetterToUpper(super.getContextConfig().getBizEnName()));
        generateFile(super.getContextConfig().getTemplatePrefixPath() + "/Controller.java.btl", controllerPath);
        System.out.println("生成控制器成功!");
    }

    @Override
    protected void generateSqls() {

    }
}
