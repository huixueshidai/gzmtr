/**
 * 初始化岗位职责详情对话框
 */
var PositionStatementInfoDlg = {
    positionStatementInfoData : {},
    zTreeInstance : null
};

/**
 * 清除数据
 */
PositionStatementInfoDlg.clearData = function() {
    this.positionStatementInfoData = {};
}

/**
 * 设置对话框中的数据
 *
 * @param key 数据的名称
 * @param val 数据的具体值
 */
PositionStatementInfoDlg.set = function(key, val) {
    this.positionStatementInfoData[key] = (typeof val == "undefined") ? $("#" + key).val() : val;
    return this;
}

/**
 * 设置对话框中的数据
 *
 * @param key 数据的名称
 * @param val 数据的具体值
 */
PositionStatementInfoDlg.get = function(key) {
    return $("#" + key).val();
}

/**
 * 关闭此对话框
 */
PositionStatementInfoDlg.close = function() {
    parent.layer.close(window.parent.PositionStatement.layerIndex);
}

/**
 * 收集数据
 */
PositionStatementInfoDlg.collectData = function() {
    this
    .set('id')
    .set('name')
    .set('num')
    .set('pid')
    ;
}

/**
 * 提交添加
 */
PositionStatementInfoDlg.addSubmit = function() {

    this.clearData();
    this.collectData();

    //提交信息
    var ajax = new $ax(Feng.ctxPath + "/positionStatement/add", function(data){
        Feng.success("添加成功!");
        window.parent.PositionStatement.table.refresh();
        PositionStatementInfoDlg.close();
    },function(data){
        Feng.error("添加失败!" + data.responseJSON.message + "!");
    });
    ajax.set(this.positionStatementInfoData);
    ajax.start();
}

/**
 * 提交修改
 */
PositionStatementInfoDlg.editSubmit = function() {

    this.clearData();
    this.collectData();

    //提交信息
    var ajax = new $ax(Feng.ctxPath + "/positionStatement/update", function(data){
        Feng.success("修改成功!");
        window.parent.PositionStatement.table.refresh();
        PositionStatementInfoDlg.close();
    },function(data){
        Feng.error("修改失败!" + data.responseJSON.message + "!");
    });
    ajax.set(this.positionStatementInfoData);
    ajax.start();
}
/**
 * 点击ztree列表的选项时
 *
 * @param e
 * @param treeId
 * @param treeNode
 * @returns
 *
 */
PositionStatementInfoDlg.onClickPositionStatement = function (e, treeId, treeNode) {
    $("#pName").attr("value", PositionStatementInfoDlg.zTreeInstance.getSelectedVal());
    $("#pid").attr("value", treeNode.id);
    $("#parentTreeDiv").fadeOut("fast");

}


/**
 * 显示tree
 */
PositionStatementInfoDlg.showPositionStatementSelectTree = function () {
    Feng.showInputTree("pName", "parentTreeDiv", 15, 34);
}




$(function() {
    Feng.initValidator("positionStatementInfoForm", PositionStatementInfoDlg.validateFields);
    var ztree = new $ZTree("parentTree", "/positionStatement/tree");
    ztree.bindOnClick(PositionStatementInfoDlg.onClickPositionStatement);
    ztree.init();
    PositionStatementInfoDlg.zTreeInstance = ztree;
});
