/**
 * 初始化事故处理措施详情对话框
 */
var AccidentTypeInfoDlg = {
    accidentTypeInfoData : {}
    ,zTreeInstance :null,
    validateFields: {
        name: {
            validators: {
                notEmpty: {
                    message: '名称不能为空'
                }
            }
        },
        code: {
            validators: {
                notEmpty: {
                    message: '编码不能为空'
                }
            }
        },
        num: {
            validators: {
                notEmpty: {
                    message: '排序不能为空'
                },
                digits:{
                    message: '排序必须为数字'
                }
            }
        },
        pName: {
            validators: {
                notEmpty: {
                    message: '父级编号不能为空'
                }
            }
        }
    }
};

/**
 * 清除数据
 */
AccidentTypeInfoDlg.clearData = function () {
    this.accidentTypeInfoData = {};
}

/**
 * 设置对话框中的数据
 *
 * @param key 数据的名称
 * @param val 数据的具体值
 */
AccidentTypeInfoDlg.set = function (key, val) {
    this.accidentTypeInfoData[key] = (typeof val == "undefined") ? $("#" + key).val() : val;
    return this;
}

/**
 * 设置对话框中的数据
 *
 * @param key 数据的名称
 * @param val 数据的具体值
 */
AccidentTypeInfoDlg.get = function (key) {
    return $("#" + key).val();
}

/**
 * 关闭此对话框
 */
AccidentTypeInfoDlg.close = function () {
    parent.layer.close(window.parent.AccidentType.layerIndex);
}

/**
 * 收集数据
 */
AccidentTypeInfoDlg.collectData = function () {
    this
        .set('id')
        .set('num')
        .set('pid')
        .set('pids')
        .set('name')
        .set('code')
        .set('allName',$("#pName").val())
    ;
}

/**
 * 验证数据是否为空
 */
AccidentTypeInfoDlg.validate = function () {
    $('#accidentTypeInfoForm').data("bootstrapValidator").resetForm();
    $('#accidentTypeInfoForm').bootstrapValidator('validate');
    return $("#accidentTypeInfoForm").data('bootstrapValidator').isValid();
}

/**
 * 点击部门ztree列表的选项时
 *
 * @param e
 * @param treeId
 * @param treeNode
 * @returns
 *
 */
AccidentTypeInfoDlg.onClickAccidentType = function (e, treeId, treeNode) {
    $("#pName").attr("value", AccidentTypeInfoDlg.zTreeInstance.getSelectedValName());
    $("#pid").attr("value", treeNode.id);
    $("#parentTreeDiv").fadeOut("fast");

}

/**
 * 显示tree
 */
AccidentTypeInfoDlg.showAccidentTypeSelectTree = function () {
    Feng.showInputTree("pName", "parentTreeDiv", 15, 34);
}


/**
 * 提交添加
 */
AccidentTypeInfoDlg.addSubmit = function () {

    this.clearData();
    this.collectData();
    if (!this.validate()) {
        return;
    }

    //提交信息
    var ajax = new $ax(Feng.ctxPath + "/accidentType/add", function (data) {
        Feng.success("添加成功!");
        window.parent.AccidentType.table.refresh();
        AccidentTypeInfoDlg.close();
    }, function (data) {
        Feng.error("添加失败!" + data.responseJSON.message + "!");
    });
    ajax.set(this.accidentTypeInfoData);
    ajax.start();
}

/**
 * 提交修改
 */
AccidentTypeInfoDlg.editSubmit = function () {

    this.clearData();
    this.collectData();
    if (!this.validate()) {
        return;
    }

    //提交信息
    var ajax = new $ax(Feng.ctxPath + "/accidentType/update", function (data) {
        Feng.success("修改成功!");
        window.parent.AccidentType.table.refresh();
        AccidentTypeInfoDlg.close();
    }, function (data) {
        Feng.error("修改失败!" + data.responseJSON.message + "!");
    });
    ajax.set(this.accidentTypeInfoData);
    ajax.start();
}


$(function() {
    Feng.initValidator("accidentTypeInfoForm", AccidentTypeInfoDlg.validateFields);
    var ztree = new $ZTree("parentTree", "/accidentType/tree");
    ztree.bindOnClick(AccidentTypeInfoDlg.onClickAccidentType);
    ztree.init();
    AccidentTypeInfoDlg.zTreeInstance = ztree;
});
