package com.stylefeng.guns.common.persistence.dao;

import com.stylefeng.guns.common.persistence.model.PublicBusHub;
import com.baomidou.mybatisplus.mapper.BaseMapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

/**
 * <p>
  * 公交枢纽站信息 Mapper 接口
 * </p>
 *
 * @author yinjc
 * @since 2018-03-21
 */
public interface PublicBusHubMapper extends BaseMapper<PublicBusHub> {
       List<Map<String,Object>> selectByName(@Param("condition") String condition);

}