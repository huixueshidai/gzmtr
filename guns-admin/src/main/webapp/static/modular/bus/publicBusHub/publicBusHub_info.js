/**
 * 初始化公交站枢纽信息详情对话框
 */
var PublicBusHubInfoDlg = {
    publicBusHubInfoData: {}
};

/**
 * 清除数据
 */
PublicBusHubInfoDlg.clearData = function () {
    this.publicBusHubInfoData = {};
}

/**
 * 设置对话框中的数据
 *
 * @param key 数据的名称
 * @param val 数据的具体值
 */
PublicBusHubInfoDlg.set = function (key, val) {
    this.publicBusHubInfoData[key] = (typeof val == "undefined") ? $("#" + key).val() : val;
    return this;
}

/**
 * 设置对话框中的数据
 *
 * @param key 数据的名称
 * @param val 数据的具体值
 */
PublicBusHubInfoDlg.get = function (key) {
    return $("#" + key).val();
}

/**
 * 关闭此对话框
 */
PublicBusHubInfoDlg.close = function () {
    parent.layer.close(window.parent.PublicBusHub.layerIndex);
}

/**
 * 收集数据
 */
PublicBusHubInfoDlg.collectData = function () {
    this
        .set('id')
        .set('name')
        .set('driverNum')
        .set('distance')
        .set('busType')
        .set('busCapacity')
        .set('loadFactor')
        .set('maxNum')
        .set('busCapacity')
    ;
}

/**
 * 提交添加
 */
PublicBusHubInfoDlg.addSubmit = function () {

    this.clearData();
    this.collectData();

    //提交信息
    var ajax = new $ax(Feng.ctxPath + "/publicBusHub/add", function (data) {
        Feng.success("添加成功!");
        window.parent.PublicBusHub.table.refresh();
        PublicBusHubInfoDlg.close();
    }, function (data) {
        Feng.error("添加失败!" + data.responseJSON.message + "!");
    });
    ajax.set(this.publicBusHubInfoData);
    ajax.start();
}

/**
 * 提交修改
 */
PublicBusHubInfoDlg.editSubmit = function () {

    this.clearData();
    this.collectData();

    //提交信息
    var ajax = new $ax(Feng.ctxPath + "/publicBusHub/update", function (data) {
        Feng.success("修改成功!");
        window.parent.PublicBusHub.table.refresh();
        PublicBusHubInfoDlg.close();
    }, function (data) {
        Feng.error("修改失败!" + data.responseJSON.message + "!");
    });
    ajax.set(this.publicBusHubInfoData);
    ajax.start();
}

$(function () {

});
