package com.stylefeng.guns.common.plugin.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class Period {

	private Date beginDate;

	private Date endDate;

}
