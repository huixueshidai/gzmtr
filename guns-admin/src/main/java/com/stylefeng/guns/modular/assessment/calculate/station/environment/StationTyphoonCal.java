package com.stylefeng.guns.modular.assessment.calculate.station.environment;

import com.stylefeng.guns.common.persistence.model.Assessment;
import com.stylefeng.guns.core.util.DoubleUtil;
import com.stylefeng.guns.modular.assessment.calculate.ThirdIndicationCalculator;
import com.stylefeng.guns.modular.assessment.calculate.aware.StationThirdIndicationAware;
import org.springframework.stereotype.Component;

import java.util.Date;

/**
 * 台风天气安全指数
 */
@Component
public class StationTyphoonCal extends ThirdIndicationCalculator implements StationThirdIndicationAware {

    @Override
    protected Integer getIndicationId() {
        return 40;
    }

    @Override
    public Assessment calculate(Integer stationId, Date beginDate, Date endDate) {
        Double isBlack = getDoubleValue(stationId, 94, beginDate,endDate);
        Double isThunderstorm = getDoubleValue(stationId, 95, beginDate,endDate);
        if (isBlack == 0d || isThunderstorm == 0d) {
            return genResult(5d, stationId, beginDate,endDate);
        }


        Double red = getDoubleValue(stationId, 96, beginDate,endDate);
        Double orange = getDoubleValue(stationId, 97, beginDate,endDate);
        Double yellow = getDoubleValue(stationId, 98, beginDate,endDate);
        Double blue = getDoubleValue(stationId, 99, beginDate,endDate);
        Double white = getDoubleValue(stationId, 100, beginDate,endDate);

        Double redWeight = getDoubleValue(stationId, 101, beginDate,endDate);
        Double orangeWeight = getDoubleValue(stationId, 102, beginDate,endDate);
        Double yellowWeight = getDoubleValue(stationId, 103, beginDate,endDate);
        Double blueWeight = getDoubleValue(stationId, 104, beginDate,endDate);
        Double whiteWeight = getDoubleValue(stationId, 105, beginDate,endDate);

        Double redResult = DoubleUtil.sub(1, DoubleUtil.div(DoubleUtil.mul(red, redWeight), 24));
        Double orangeResult = DoubleUtil.sub(1, DoubleUtil.div(DoubleUtil.mul(orange, orangeWeight), 24));
        Double yellowResult = DoubleUtil.sub(1, DoubleUtil.div(DoubleUtil.mul(yellow, yellowWeight), 24));
        Double blueResult = DoubleUtil.sub(1, DoubleUtil.div(DoubleUtil.mul(blue, blueWeight), 24));
        Double whiteResult = DoubleUtil.sub(1, DoubleUtil.div(DoubleUtil.mul(white, whiteWeight), 24));

        Double result = DoubleUtil.add(redResult, orangeResult, yellowResult, blueResult, whiteResult);

        return genResult(result, stationId, beginDate,endDate);
    }


}
