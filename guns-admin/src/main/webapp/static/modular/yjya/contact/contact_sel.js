var ContactSel = {
    table: null,
    id: "ContactSelTable",
    seItems: null,
}


ContactSel.initColumn = function () {
    return [
        {field: 'selectItem', checkbox: true},
        {title: '名 称', field: 'name', visible: true, align: 'center', valign: 'middle'},
        {title: '职务/专业', field: 'duty', visible: true, align: 'center', valign: 'middle'},
        {title: '行政职务', field: 'administrationDuty', visible: true, align: 'center', valign: 'middle'},
        {title: '座机', field: 'phone', visible: true, align: 'center', valign: 'middle'},
        {title: '手机号', field: 'mobilePhone', visible: true, align: 'center', valign: 'middle'},
        {title: '邮箱', field: 'email', visible: true, align: 'center', valign: 'middle'},
        {title: '群组', field: 'groupName', visible: true, align: 'center', valign: 'middle'}
    ];
};

/**
 * 检查是否选中
 */
ContactSel.check = function () {
    var selected = $('#' + this.id).bootstrapTable('getSelections');
    if (selected.length == 0) {
        Feng.info("请先选中表格中的某一记录！");
        return false;
    } else {
        ContactSel.seItems = selected;
        return true;
    }
};

ContactSel.close = function () {
    parent.layer.close(window.parent.CounterplanInfoDlgEdit.layerIndex);
}

ContactSel.addContacts = function () {
    if (this.check()) {
        var items = this.seItems;
        console.table(items);
        var table =
            "<table>" +
            "    <tbody>" +
            "        <tr class=\"firstRow\">" +
            "            <td width=\"199\" valign=\"top\">名称</td>" +
            "            <td width=\"199\" valign=\"top\">职务/专业</td>" +
            "            <td width=\"199\" valign=\"top\">行政职务 </td>" +
            "            <td width=\"199\" valign=\"top\">电话</td>" +
            "            <td width=\"199\" valign=\"top\">群组</td>" +
            "        </tr>";
        for (x in items) {
            var ph = "";

            if(items[x].phone){
                ph += items[x].phone;
                if(items[x].mobilePhone) ph += " / ";
            }
            if(items[x].mobilePhone) ph += items[x].mobilePhone

            table +=
                "<tr>" +
                "<td width=\"199\" valign=\"top\">" + items[x].name + "</td>" +
                "<td width=\"199\" valign=\"top\">" + items[x].duty + "</td>" +
                "<td width=\"199\" valign=\"top\">" + items[x].administrationDuty + "</td>" +
                "<td width=\"199\" valign=\"top\">" + ph + "</td>" +
                "<td width=\"199\" valign=\"top\">" + items[x].groupName + "</td>" +
                "</tr>";

        }
        table +=
            "     </tbody>" +
            "</table>";

            window.parent.CounterplanInfoDlgEdit.ue.execCommand('inserthtml', table);
            ContactSel.close();
    }

}

ContactSel.search = function(){
    var queryData = {};

    queryData['name'] = $("#name").val();
    queryData['administrationDuty'] = $("#administrationDuty").val();
    ContactSel.table.refresh({query: queryData});
}

ContactSel.resetSearch = function(){

    $("#name").val("");
    $("#administrationDuty").val("");
}



$(function () {
    var defaultColunms = ContactSel.initColumn();
    var table = new BSTable(ContactSel.id, "/contact/list", defaultColunms);
    table.setPaginationType("server");
    ContactSel.table = table.init();
});