/**
 * 报警管理管理初始化
 */
var Alarm = {
    id: "AlarmTable",	//表格id
    seItem: null,		//选中的条目
    table: null,
    layerIndex: -1
};

/**
 * 初始化表格的列
 */
Alarm.initColumn = function () {
    return [
        {field: 'selectItem', radio: true},
            {title: '', field: 'id', visible: true, align: 'center', valign: 'middle'},
            {title: '事故类型', field: 'accidentTypeName', visible: true, align: 'center', valign: 'middle'},
            {title: '响应等级', field: 'accidentLevel', visible: true, align: 'center', valign: 'middle'},
            {title: '事故发生线路', field: 'lineId', visible: true, align: 'center', valign: 'middle'},
            {title: '事故车站', field: 'stationId', visible: true, align: 'center', valign: 'middle'},
            {title: '事故车次', field: 'trainNumber', visible: true, align: 'center', valign: 'middle'},
            {title: '事故发生时间', field: 'startTime', visible: true, align: 'center', valign: 'middle'},
            {title: '预计行车能力降低', field: 'reduction', visible: true, align: 'center', valign: 'middle'},
            {title: '突发事件类型', field: 'emergencyId', visible: true, align: 'center', valign: 'middle'},
            {title: '影响范围', field: 'trapNum', visible: true, align: 'center', valign: 'middle'}
    ];
};

/**
 * 检查是否选中
 */
Alarm.check = function () {
    var selected = $('#' + this.id).bootstrapTable('getSelections');
    if(selected.length == 0){
        Feng.info("请先选中表格中的某一记录！");
        return false;
    }else{
        Alarm.seItem = selected[0];
        return true;
    }
};

/**
 * 点击添加报警管理
 */
Alarm.openAddAlarm = function () {
    var index = layer.open({
        type: 2,
        title: '添加报警管理',
        area: ['800px', '420px'], //宽高
        fix: false, //不固定
        maxmin: true,
        content: Feng.ctxPath + '/alarm/alarm_add'
    });
    this.layerIndex = index;
};

/**
 * 打开查看报警管理详情
 */
Alarm.openAlarmDetail = function () {
    if (this.check()) {
        var index = layer.open({
            type: 2,
            title: '报警管理详情',
            area: ['800px', '420px'], //宽高
            fix: false, //不固定
            maxmin: true,
            content: Feng.ctxPath + '/alarm/alarm_update/' + Alarm.seItem.id
        });
        this.layerIndex = index;
    }
};

/**
 * 删除报警管理
 */
Alarm.delete = function () {
    if (this.check()) {
        var ajax = new $ax(Feng.ctxPath + "/alarm/delete", function (data) {
            Feng.success("删除成功!");
            Alarm.table.refresh();
        }, function (data) {
            Feng.error("删除失败!" + data.responseJSON.message + "!");
        });
        ajax.set("alarmId",this.seItem.id);
        ajax.start();
    }
};

/**
 * 查询报警管理列表
 */
Alarm.search = function () {
    var queryData = {};
    queryData['condition'] = $("#condition").val();
    Alarm.table.refresh({query: queryData});
};

$(function () {
    var defaultColunms = Alarm.initColumn();
    var table = new BSTable(Alarm.id, "/alarm/list", defaultColunms);
    table.setPaginationType("client");
    Alarm.table = table.init();
});
