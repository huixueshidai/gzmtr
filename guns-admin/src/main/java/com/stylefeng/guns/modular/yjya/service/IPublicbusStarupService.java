package com.stylefeng.guns.modular.yjya.service;

import com.stylefeng.guns.common.persistence.model.PublicbusStarup;
import com.baomidou.mybatisplus.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author wangshuaikang
 * @since 2018-09-09
 */
public interface IPublicbusStarupService extends IService<PublicbusStarup> {
	
}
