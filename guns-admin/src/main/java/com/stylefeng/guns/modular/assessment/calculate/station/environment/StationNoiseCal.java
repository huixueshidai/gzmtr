package com.stylefeng.guns.modular.assessment.calculate.station.environment;

import com.stylefeng.guns.common.persistence.model.Assessment;
import com.stylefeng.guns.core.util.DoubleUtil;
import com.stylefeng.guns.modular.assessment.calculate.ThirdIndicationCalculator;
import com.stylefeng.guns.modular.assessment.calculate.aware.StationThirdIndicationAware;
import org.springframework.stereotype.Component;

import java.util.Date;

/**
 * 车站噪声控制指数
 */
@Component
public class StationNoiseCal extends ThirdIndicationCalculator implements StationThirdIndicationAware {

    @Override
    protected Integer getIndicationId() {
        return 36;
    }

    @Override
    public Assessment calculate(Integer stationId, Date beginDate, Date endDate) {
        Double num = getDoubleValue(stationId, 74, beginDate,endDate);
        Double total = getDoubleValue(stationId, 75, beginDate,endDate);
        return genResult(DoubleUtil.safeDiv(num, total), stationId, beginDate,endDate);
    }

}
