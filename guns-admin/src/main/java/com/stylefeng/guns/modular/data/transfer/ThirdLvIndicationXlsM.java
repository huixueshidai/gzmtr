package com.stylefeng.guns.modular.data.transfer;

import cn.afterturn.easypoi.excel.annotation.Excel;
import cn.afterturn.easypoi.excel.annotation.ExcelCollection;
import cn.afterturn.easypoi.handler.inter.IExcelModel;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.validation.Valid;
import java.util.List;

/**
 * 三级指标Excel导出类
 */
@Getter
@Setter
@NoArgsConstructor
public class ThirdLvIndicationXlsM implements IExcelModel {

    private String errorMsg;

    @Excel(name = "3级指标",needMerge = true,width = 20)
    private String name;

    @ExcelCollection(name = "源数据列表")
    @Valid
    private List<ResourceDataXlsM> resourceDataXlsMList;

    public ThirdLvIndicationXlsM(String name, List<ResourceDataXlsM> resourceDataXlsMList) {
        this.name = name;
        this.resourceDataXlsMList = resourceDataXlsMList;
    }

}
