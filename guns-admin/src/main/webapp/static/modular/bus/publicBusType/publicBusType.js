/**
 * 公交车类型管理初始化
 */
var PublicBusType = {
    id: "PublicBusTypeTable",	//表格id
    seItem: null,		//选中的条目
    table: null,
    layerIndex: -1
};

/**
 * 初始化表格的列
 */
PublicBusType.initColumn = function () {
    return [
        {field: 'selectItem', radio: true},
            {title: 'id', field: 'id', visible: true, align: 'center', valign: 'middle'},
            {title: '车辆类型', field: 'busTypeName', visible: true, align: 'center', valign: 'middle'},
            {title: '车辆定员', field: 'busCapacity', visible: true, align: 'center', valign: 'middle'}
    ];
};

/**
 * 检查是否选中
 */
PublicBusType.check = function () {
    var selected = $('#' + this.id).bootstrapTable('getSelections');
    if(selected.length == 0){
        Feng.info("请先选中表格中的某一记录！");
        return false;
    }else{
        PublicBusType.seItem = selected[0];
        return true;
    }
};

/**
 * 点击添加公交车类型
 */
PublicBusType.openAddPublicBusType = function () {
    var index = layer.open({
        type: 2,
        title: '添加公交车类型',
        area: ['800px', '420px'], //宽高
        fix: false, //不固定
        maxmin: true,
        content: Feng.ctxPath + '/publicBusType/publicBusType_add'
    });
    this.layerIndex = index;
};

/**
 * 打开查看公交车类型详情
 */
PublicBusType.openPublicBusTypeDetail = function () {
    if (this.check()) {
        var index = layer.open({
            type: 2,
            title: '公交车类型详情',
            area: ['800px', '420px'], //宽高
            fix: false, //不固定
            maxmin: true,
            content: Feng.ctxPath + '/publicBusType/publicBusType_update/' + PublicBusType.seItem.id
        });
        this.layerIndex = index;
    }
};

/**
 * 删除公交车类型
 */
PublicBusType.delete = function () {
    if (this.check()) {
        var ajax = new $ax(Feng.ctxPath + "/publicBusType/delete", function (data) {
            Feng.success("删除成功!");
            PublicBusType.table.refresh();
        }, function (data) {
            Feng.error("删除失败!" + data.responseJSON.message + "!");
        });
        ajax.set("publicBusTypeId",this.seItem.id);
        ajax.start();
    }
};

/**
 * 查询公交车类型列表
 */
PublicBusType.search = function () {
    var queryData = {};
    queryData['condition'] = $("#condition").val();
    PublicBusType.table.refresh({query: queryData});
};

$(function () {
    var defaultColunms = PublicBusType.initColumn();
    var table = new BSTable(PublicBusType.id, "/publicBusType/list", defaultColunms);
    table.setPaginationType("client");
    PublicBusType.table = table.init();
});
