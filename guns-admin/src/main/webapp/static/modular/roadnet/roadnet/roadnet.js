/**
 * 管理初始化
 */
var Roadnet = {
    id: "RoadnetTable",	//表格id
    seItem: null,		//选中的条目
    table: null,
    layerIndex: -1
};

/**
 * 初始化表格的列
 */
Roadnet.initColumn = function () {
    return [
        {field: 'selectItem', radio: true},
        {title: 'id', field: 'id', visible: true, align: 'center', valign: 'middle',width:'50px'},
        {title: '名称', field: 'name', visible: true, align: 'center', valign: 'middle'},
        {title: '简称', field: 'shortName', visible: true, align: 'center', valign: 'middle'},
        {title: '类型', field: 'typeName', visible: true, align: 'center', valign: 'middle'},
        {title: '编码', field: 'code', visible: true, align: 'center', valign: 'middle'},
        {title: '排序', field: 'num', visible: true, align: 'center', valign: 'middle'},
    ];
};

/**
 * 检查是否选中
 */
Roadnet.check = function () {
    var selected = $('#' + this.id).bootstrapTreeTable('getSelections');
    if(selected.length == 0){
        Feng.info("请先选中表格中的某一记录！");
        return false;
    }else{
        Roadnet.seItem = selected[0];
        return true;
    }
};

/**
 * 点击添加
 */
Roadnet.openAddRoadnet = function () {
    var index = layer.open({
        type: 2,
        title: '添加',
        area: ['800px', '420px'], //宽高
        fix: false, //不固定
        maxmin: true,
        content: Feng.ctxPath + '/roadnet/roadnet_add'
    });
    this.layerIndex = index;
};

/**
 * 打开查看详情
 */
Roadnet.openRoadnetDetail = function () {
    if (this.check()) {
        var index = layer.open({
            type: 2,
            title: '详情',
            area: ['800px', '420px'], //宽高
            fix: false, //不固定
            maxmin: true,
            content: Feng.ctxPath + '/roadnet/roadnet_update/' + Roadnet.seItem.id
        });
        this.layerIndex = index;
    }
};

/**
 * 删除
 */
Roadnet.delete = function () {
    if (this.check()) {
        var ajax = new $ax(Feng.ctxPath + "/roadnet/delete", function (data) {
            Feng.success("删除成功!");
            Roadnet.table.refresh();
        }, function (data) {
            Feng.error("删除失败!" + data.responseJSON.message + "!");
        });
        ajax.set("roadnetId",this.seItem.id);
        ajax.start();
    }
};

/**
 * 查询列表
 */
Roadnet.search = function () {
    var queryData = {};
    queryData['name'] = $("#q_name").val();
    Roadnet.table.refresh({query: queryData});
};

$(function () {

    var defaultColunms = Roadnet.initColumn();
    var table = new BSTreeTable(Roadnet.id, "/roadnet/list", defaultColunms);
    table.setExpandColumn(2);
    table.setIdField("id");
    table.setCodeField("id");
    table.setParentCodeField("pid");
    table.setExpandAll(false);
    table.init();
    Roadnet.table = table;
});
