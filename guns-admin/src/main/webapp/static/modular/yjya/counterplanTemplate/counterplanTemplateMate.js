/**
 * 预案模板管理初始化
 */
var CounterplanTemplateMate = {
    id: "CounterplanTemplateTable",	//表格id
    seItem: null,		//选中的条目
    table: null,
    layerIndex: -1,
};

/**
 * 初始化表格的列
 */
CounterplanTemplateMate.initColumn = function () {
    return [
        {field: 'selectItem', radio: true},
            {title: 'id', field: 'id', visible: true, align: 'center', valign: 'middle'},
            {title: '预案模板名称', field: 'name', visible: true, align: 'center', valign: 'middle'},
            {title: '类别', field: 'type', visible: true, align: 'center', valign: 'middle',sortable: true},
            {title: '事故类型', field: 'accidentTypeName', visible: true, align: 'center', valign: 'middle',sortable: true},
            {title: '响应等级', field: 'levelName', visible: true, align: 'center', valign: 'middle', sortable: true},
            {title: '车站', field: 'stationName', align: 'center', valign: 'middle', sortable: true},
            {title: '岗位', field: 'accidentMain', align: 'center', valign: 'middle', sortable: true},
    ];
};

/**
 * 检查是否选中
 */
CounterplanTemplateMate.check = function () {
    var selected = $('#' + this.id).bootstrapTable('getSelections');
    if(selected.length == 0){
        Feng.info("请先选中表格中的某一记录！");
        return false;
    }else{
        CounterplanTemplateMate.seItem = selected[0];
        return true;
    }
};


/**
 * 选用一个预案模板
 */
CounterplanTemplateMate.counterplanTemplateContentSel = function(){
    if(this.check()){
        console.log(CounterplanTemplateMate.seItem.id);
        var ajax = new $ax(Feng.ctxPath + "/counterplanTemplate/counterplanTemplateId", function (data) {
            var content = data.content;
            var ue = window.parent.CounterplanInfoDlgEdit.ue;
            ue.setContent(content);
            Feng.success("预案模板应用成功!");
        }, function (data) {
        });
        ajax.set("id",CounterplanTemplateMate.seItem.id);
        ajax.start();
        parent.layer.close(window.parent.CounterplanInfoDlgEdit.layerIndex);
    }
}

CounterplanTemplateMate.detail = function () {
    if (this.check()) {
        var index = layer.open({
            type: 2,
            title: '预案模板详情',
            area: ['1200px', '600px'], //宽高
            fix: false, //不固定
            maxmin: true,
            content: Feng.ctxPath + '/counterplanTemplate/detail/' + CounterplanTemplateMate.seItem.id
        });
        this.layerIndex = index;
        layer.full(index);
    }
}

CounterplanTemplateMate.queryParams = function () {
    var queryData = {};
    queryData['type'] = $("#type").val();
    queryData['level'] = $("#level").val();
    queryData['accidentTypeId'] = $("#accidentTypeId").val();

    return queryData;
}

/**
 * 查询预案模板列表
 */
CounterplanTemplateMate.search = function () {
    var queryData = this.queryParams();
    CounterplanTemplateMate.table.refresh({query: queryData});
};


$(function () {
    var type = $("#type").val();
    var accidentTypeId = $("#accidentTypeId").val();
    var level = $("#level").val();
    console.log(type+"1"+accidentTypeId+"1"+level);
    var defaultColunms = CounterplanTemplateMate.initColumn();
    var table = new BSTable(CounterplanTemplateMate.id, "/counterplanTemplate/counterplan_template_mate" ,defaultColunms);
    table.setPaginationType("server");
    table.init();
    CounterplanTemplateMate.table = table;

    CounterplanTemplateMate.search();

});
