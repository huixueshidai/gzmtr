/**
 * 初始化客流管理详情对话框
 */
var PassengerNumInfoDlg = {
    passengerNumInfoData : {}
};

/**
 * 清除数据
 */
PassengerNumInfoDlg.clearData = function() {
    this.passengerNumInfoData = {};
}

/**
 * 设置对话框中的数据
 *
 * @param key 数据的名称
 * @param val 数据的具体值
 */
PassengerNumInfoDlg.set = function(key, val) {
    this.passengerNumInfoData[key] = (typeof val == "undefined") ? $("#" + key).val() : val;
    return this;
}

/**
 * 设置对话框中的数据
 *
 * @param key 数据的名称
 * @param val 数据的具体值
 */
PassengerNumInfoDlg.get = function(key) {
    return $("#" + key).val();
}

/**
 * 关闭此对话框
 */
PassengerNumInfoDlg.close = function() {
    parent.layer.close(window.parent.PassengerNum.layerIndex);
}

/**
 * 收集数据
 */
PassengerNumInfoDlg.collectData = function() {
    this
    .set('id')
    .set('time')
    .set('startLine')
    .set('startStation')
    .set('endLine')
    .set('endStation')
    ;
}

/**
 * 提交添加
 */
PassengerNumInfoDlg.addSubmit = function() {

    this.clearData();
    this.collectData();

    //提交信息
    var ajax = new $ax(Feng.ctxPath + "/passengerNum/add", function(data){
        Feng.success("添加成功!");
        window.parent.PassengerNum.table.refresh();
        PassengerNumInfoDlg.close();
    },function(data){
        Feng.error("添加失败!" + data.responseJSON.message + "!");
    });
    ajax.set(this.passengerNumInfoData);
    ajax.start();
}

/**
 * 提交修改
 */
PassengerNumInfoDlg.editSubmit = function() {

    this.clearData();
    this.collectData();

    //提交信息
    var ajax = new $ax(Feng.ctxPath + "/passengerNum/update", function(data){
        Feng.success("修改成功!");
        window.parent.PassengerNum.table.refresh();
        PassengerNumInfoDlg.close();
    },function(data){
        Feng.error("修改失败!" + data.responseJSON.message + "!");
    });
    ajax.set(this.passengerNumInfoData);
    ajax.start();
}

$(function() {

});
