@/*
    名称查询条件标签的参数说明:

    name : 查询条件的名称
    id : 查询内容的input框id
    clickFun : 点击事件的方法名
@*/


<div class="input-group">
    <div class="input-group-btn">
        <button data-toggle="dropdown" class="btn btn-white dropdown-toggle"
                type="button">${name}
        </button>
    </div>
    <input class="form-control"  id="${id}" placeholder="${placeholder!}"
           @if(isNotEmpty(type)){
                 type="${type}"
            @}else{
                 type="text"
            @}

            @if(isNotEmpty(readonly)){
                 readonly="${readonly}"
            @}

            @if(isNotEmpty(clickFun)){
                onclick="${clickFun}"
            @}

            @if(isNotEmpty(disabled)){
                disabled="${disabled}"
            @}
    />
    @if(isNotEmpty(selectFlag)){
    <div id="${selectId}" style="display: none; position: absolute; z-index: 1000;">
        <ul id="${selectTreeId}" class="ztree tree-box" style="${selectStyle!}"></ul>
    </div>
    @}
</div>