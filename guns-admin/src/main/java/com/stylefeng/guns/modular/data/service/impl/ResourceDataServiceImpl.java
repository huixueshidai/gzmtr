package com.stylefeng.guns.modular.data.service.impl;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.stylefeng.guns.common.constant.factory.ConstantFactory;
import com.stylefeng.guns.common.persistence.dao.ResourceDataMapper;
import com.stylefeng.guns.common.persistence.model.Indication;
import com.stylefeng.guns.common.persistence.model.ResourceData;
import com.stylefeng.guns.common.persistence.model.ResourceDataType;
import com.stylefeng.guns.modular.data.dao.ResourceDataDao;
import com.stylefeng.guns.modular.data.dao.ResourceDataTypeDao;
import com.stylefeng.guns.modular.data.service.IResourceDataService;
import com.stylefeng.guns.modular.data.transfer.ResourceDataXlsM;
import com.stylefeng.guns.modular.data.transfer.ThirdLvIndicationXlsM;
import com.stylefeng.guns.modular.indication.dao.IndicationDao;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

/**
 * <p>
 * 源数据 服务实现类
 * </p>
 *
 * @author xuziyang
 * @since 2018-01-17
 */
@Service
public class ResourceDataServiceImpl extends ServiceImpl<ResourceDataMapper, ResourceData> implements IResourceDataService {


    @Autowired
    private IndicationDao indicationDao;

    @Autowired
    private ResourceDataTypeDao resourceDataTypeDao;

    @Autowired
    private ResourceDataDao resourceDataDao;

    @Override
    public List<ThirdLvIndicationXlsM> selectThirdLVIndicationXlsMList(Integer pids) {
        List<Indication> trdList = indicationDao.selectTrdIndications(3, pids);
        return trdList.stream().map(thrIcn -> {
            List<ResourceDataType> resourceDataTypeList = resourceDataTypeDao.getResourceDataTypesByIndication(thrIcn.getId());
            List<ResourceDataXlsM> resourceDataXlsMList = resourceDataTypeList.stream().map(resourceDataType -> new ResourceDataXlsM(resourceDataType.getName(), resourceDataType.getInstruction())).collect(Collectors.toList());
            return new ThirdLvIndicationXlsM(thrIcn.getName(), resourceDataXlsMList);
        }).collect(Collectors.toList());
    }


    @Override
    @Transactional
    public boolean insertDtoBatch(List<ResourceDataXlsM> list) {
        list.stream()
                .map(this::convertResourceData)
                .forEach(this::insertOrUpdate);
        return true;
    }

    @Override
    public boolean insertOrUpdate(ResourceData resourceData){
		EntityWrapper<ResourceData> ew = new EntityWrapper<ResourceData>();
		ew.where("resource_data_type_id = {0} ",resourceData.getResourceDataTypeId())
				.and("roadnet_id = {0}",resourceData.getRoadnetId())
				.and("start_time = {0}",resourceData.getStartTime());

		if (StringUtils.isNoneBlank(resourceData.getEntrance())) {
			ew.and("entrance = {0}", resourceData.getEntrance());
		}
		ew.last("limit 1");

		ResourceData old = this.selectOne(ew);
		if(old != null)
			old.deleteById();

		return this.insert(resourceData);
	}

    private ResourceData convertResourceData(ResourceDataXlsM resourceDataXlsM) {
        Integer resourceDataTypeId = ConstantFactory.me().getResourceDataTypeByName(resourceDataXlsM.getResourceDataTypeName()).getId();
        //todo
        Integer roadnetId = ConstantFactory.me().getRoadnetIdByName(resourceDataXlsM.getRoadnet());
        ResourceData resourceData = new ResourceData();
        resourceData.setRoadnetId(roadnetId);
        resourceData.setResourceDataTypeId(resourceDataTypeId);
        resourceData.setValue(resourceDataXlsM.getValue());
        resourceData.setStartTime(resourceDataXlsM.getStartTime());
        resourceData.setEndTime(resourceDataXlsM.getEndTime());
        resourceData.setEntrance(resourceDataXlsM.getEntrance());
        return resourceData;
    }


    @Override
    public ResourceData selectUnique(Integer roadnetId, Integer resourceDataTypeId, Date beginDate,Date endDate) {
        return resourceDataDao.selectUnique(roadnetId, resourceDataTypeId, beginDate,endDate);
    }

	@Override
	public List<ResourceData> selectMore(Integer roadnetId, Integer resourceDataTypeId, Date beginDate, Date endDate) {
		return resourceDataDao.selectMore(roadnetId,resourceDataTypeId,beginDate,endDate);
	}


}
