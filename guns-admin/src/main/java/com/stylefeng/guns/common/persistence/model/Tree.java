package com.stylefeng.guns.common.persistence.model;

import com.baomidou.mybatisplus.activerecord.Model;
import lombok.Data;

import java.io.Serializable;

@Data
public class Tree extends Model<Tree>{

    private static final long serialVersionUID = 1L;

    private String id;
    private Integer pid;
    private String pids;


    @Override
    protected Serializable pkVal() {
        return this.id;
    }
}
