package com.stylefeng.guns.modular.assessment.calculate.line.accident;

import com.stylefeng.guns.core.util.SpringContextHolder;
import com.stylefeng.guns.modular.assessment.calculate.SecondIndicationCalculator;
import com.stylefeng.guns.modular.assessment.calculate.ThirdIndicationCalculator;
import com.stylefeng.guns.modular.assessment.calculate.aware.LineSecondIndicationAware;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import java.util.Arrays;
import java.util.List;

@Component
@Scope(ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public class LineAccidentCal extends SecondIndicationCalculator implements LineSecondIndicationAware {

    private List<ThirdIndicationCalculator> thirdIndicationCalculatorList;

    public void setThirdIndicationCalculateList() {
        thirdIndicationCalculatorList = Arrays.asList(
                SpringContextHolder.getBean(LineDengXiaoAccidentCal.class)
        );
    }

    @Override
    public List<ThirdIndicationCalculator> getThirdIndicationCalculatorList() {
        if (thirdIndicationCalculatorList == null) {
             setThirdIndicationCalculateList();
        }
        return thirdIndicationCalculatorList;
    }


    @Override
    protected Integer getIndicationId() {
        return 54;
    }
}
