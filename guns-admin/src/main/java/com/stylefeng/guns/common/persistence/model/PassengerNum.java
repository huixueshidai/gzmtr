package com.stylefeng.guns.common.persistence.model;

import java.io.Serializable;

import cn.afterturn.easypoi.excel.annotation.Excel;
import cn.afterturn.easypoi.handler.inter.IExcelDataModel;
import com.baomidou.mybatisplus.enums.IdType;
import java.util.Date;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableName;
import com.vip.vjtools.vjkit.base.annotation.NotNull;
import lombok.Data;
import lombok.Getter;
import lombok.Setter;
import org.springframework.format.annotation.DateTimeFormat;

import java.io.Serializable;

/**
 * <p>
 * 
 * </p>
 *
 * @author wsk
 * @since 2018-07-10
 */
@TableName("gzmtr_passenger_num")
@Data
@Getter
@Setter
public class PassengerNum extends Model<PassengerNum>  {

    private static final long serialVersionUID = 1L;

//	private int rowNum;

	@TableId(value="id", type= IdType.AUTO)
	private Integer id;
    /**
     * 时间
     */
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @Excel(name = "时间", width = 50,importFormat = "yyyy-MM-dd HH:mm:ss")
	private Date time;
    /**
     * 起始线路
     */
	@TableField("start_line")
	private Integer startLine;
	/**
	 * 起始线路
	 */
	@TableField(exist=false)
	@Excel(name="起始线路",width = 50)
	@NotNull
	private String startLineName;

    /**
     * 起始点
     */
	@TableField("start_station")
	private Integer startStation;
	/**
	 * 起始点
	 */
	@TableField(exist=false)
	@Excel(name="起始点",width = 50)
	@NotNull
	private String startStationName;

    /**
     * 终到线路
     */
	@TableField("end_line")
	private Integer endLine;
	/**
	 * 终到线路
	 */
	@TableField(exist=false)
	@Excel(name="终到线路",width = 50)
	@NotNull
	private String endLineName;

    /**
     * 终到点
     */
	@TableField("end_station")
	private Integer endStation;
	/**
	 * 终到点
	 */
	@TableField(exist=false)
	@Excel(name="终到点",width = 50)
	@NotNull
	private String endStationName;

    /**
     * 客流/15min
     */
	@TableField("passenger_num")
	@Excel(name="客流/15min",width = 50)
	@NotNull
	private Integer passengerNum;

	/**
	 * 客流分类
	 */
	@TableField("date_status")
	private Integer dateStatus;


	@Override
	protected Serializable pkVal() {
		return id;
	}
}
