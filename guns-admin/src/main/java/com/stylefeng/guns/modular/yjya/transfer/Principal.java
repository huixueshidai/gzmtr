package com.stylefeng.guns.modular.yjya.transfer;

import lombok.*;

/**
 * Created by 60132 on 2018/3/23.
 */
@ToString
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class Principal {
    /**
     * 姓名
     */
    public String name ;
    /**
     * 职务
     */
    public String duty;
    /**
     *联系电话
     */
    public String telphone;

}
