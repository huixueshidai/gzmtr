package com.stylefeng.guns.modular.yjya.controller;

import com.stylefeng.guns.core.base.controller.BaseController;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.beans.factory.annotation.Autowired;
import com.stylefeng.guns.core.log.LogObjectHolder;
import org.springframework.web.bind.annotation.RequestParam;
import com.stylefeng.guns.common.persistence.model.AccidentDisposaltask;
import com.stylefeng.guns.modular.yjya.service.IAccidentDisposaltaskService;

/**
 * 事故实例处置流程控制器
 *
 * @author wangshuaikang
 * @Date 2018-09-11 17:26:35
 */
@Controller
@RequestMapping("/accidentDisposaltask")
public class AccidentDisposaltaskController extends BaseController {

    private String PREFIX = "/yjya/accidentDisposaltask/";

    @Autowired
    private IAccidentDisposaltaskService accidentDisposaltaskService;

    /**
     * 跳转到事故实例处置流程首页
     */
    @RequestMapping("")
    public String index() {
        return PREFIX + "accidentDisposaltask.html";
    }

    /**
     * 跳转到添加事故实例处置流程
     */
    @RequestMapping("/accidentDisposaltask_add")
    public String accidentDisposaltaskAdd() {
        return PREFIX + "accidentDisposaltask_add.html";
    }

    /**
     * 跳转到修改事故实例处置流程
     */
    @RequestMapping("/accidentDisposaltask_update/{accidentDisposaltaskId}")
    public String accidentDisposaltaskUpdate(@PathVariable Integer accidentDisposaltaskId, Model model) {
        AccidentDisposaltask accidentDisposaltask = accidentDisposaltaskService.selectById(accidentDisposaltaskId);
        model.addAttribute("item",accidentDisposaltask);
        LogObjectHolder.me().set(accidentDisposaltask);
        return PREFIX + "accidentDisposaltask_edit.html";
    }

    /**
     * 获取事故实例处置流程列表
     */
    @RequestMapping(value = "/list")
    @ResponseBody
    public Object list(String condition) {
        return accidentDisposaltaskService.selectList(null);
    }

    /**
     * 新增事故实例处置流程
     */
    @RequestMapping(value = "/add")
    @ResponseBody
    public Object add(AccidentDisposaltask accidentDisposaltask) {
        accidentDisposaltaskService.insert(accidentDisposaltask);
        return super.SUCCESS_TIP;
    }

    /**
     * 删除事故实例处置流程
     */
    @RequestMapping(value = "/delete")
    @ResponseBody
    public Object delete(@RequestParam Integer accidentDisposaltaskId) {
        accidentDisposaltaskService.deleteById(accidentDisposaltaskId);
        return SUCCESS_TIP;
    }

    /**
     * 修改事故实例处置流程
     */
    @RequestMapping(value = "/update")
    @ResponseBody
    public Object update(AccidentDisposaltask accidentDisposaltask) {
        accidentDisposaltaskService.updateById(accidentDisposaltask);
        return super.SUCCESS_TIP;
    }

    /**
     * 事故实例处置流程详情
     */
    @RequestMapping(value = "/detail/{accidentDisposaltaskId}")
    @ResponseBody
    public Object detail(@PathVariable("accidentDisposaltaskId") Integer accidentDisposaltaskId) {
        return accidentDisposaltaskService.selectById(accidentDisposaltaskId);
    }
}
