/**
 * Created by 60132 on 2018/4/9.
 */

var PublicBusInfo ={
    opts:null
};

// 百度地图API功能公交枢纽信息
$(function(){

    map = new BMap.Map("allmap");
    map.centerAndZoom(new BMap.Point(113.253528,23.126468), 16);
    var data_info = [[113.253528,23.126468,"枢纽站名称：测试的<br>公交车辆类型：<br>车辆定员：<br>车辆数量：<br>可用驾驶人员：<br>距各轨道车站距离："],
        [113.249809,23.131652,"枢纽站名称：测试的<br>公交车辆类型：<br>车辆定员：<br>车辆数量：<br>可用驾驶人员：<br>距各轨道车站距离："],
        [113.261163,23.136704,"枢纽站名称：测试的<br>公交车辆类型：<br>车辆定员：<br>车辆数量：<br>可用驾驶人员：<br>距各轨道车站距离："]
    ];
     PublicBusInfo.opts = {
        width : 200,     // 信息窗口宽度
        height: 150,     // 信息窗口高度
        title : "公交枢纽属性信息" , // 信息窗口标题
        enableMessage:false//设置允许信息窗发送短息
    };
    for(var i=0;i<data_info.length;i++){
        var marker = new BMap.Marker(new BMap.Point(data_info[i][0],data_info[i][1]));  // 创建标注
        var content = data_info[i][2];
        map.addOverlay(marker);               // 将标注添加到地图中
        PublicBusInfo.addClickHandler(content,marker);
    }
  });
PublicBusInfo.addClickHandler = function(content,marker){
    marker.addEventListener("click",function(e){
        PublicBusInfo.openInfo(content,e)}
     );
   }
PublicBusInfo.openInfo = function(content,e) {
        var p = e.target;
        var point = new BMap.Point(p.getPosition().lng, p.getPosition().lat);
        var infoWindown = new BMap.InfoWindow(content, PublicBusInfo.opts);
        map.openInfoWindow(infoWindown, point);
    }

