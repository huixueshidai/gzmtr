package com.stylefeng.guns.modular.data.dao;


import com.baomidou.mybatisplus.plugins.Page;
import com.stylefeng.guns.common.persistence.model.ResourceData;
import org.apache.ibatis.annotations.Param;

import java.util.Date;
import java.util.List;
import java.util.Map;

public interface ResourceDataDao {
	List<Map<String, Object>> selectPage(Page page, @Param("startTime") String startTime,
			@Param("endTime") String endTime, @Param("resourceDataTypeName") String resourceDataTypeName,
			@Param("roadnetName") String roadnetName);

	List<ResourceData> selectResouceDatas(@Param("startTime") String startTime, @Param("endTime") String endTime,
			@Param("roadnetOrStationId") Integer roadnetOrStationId,
			@Param("resourceDataTypeId") Integer resourceDataTypeId);

	ResourceData selectUnique(@Param("roadnetId") Integer roadnetId, @Param("typeId") Integer typeId,
			@Param("beginDate") Date beginDate, @Param("endDate") Date endDate);

	List<ResourceData> selectMore(@Param("roadnetId") Integer roadnetId, @Param("typeId") Integer typeId,
			@Param("beginDate") Date beginDate, @Param("endDate") Date endDate);

	Long selectCount(@Param("roadnetId") Integer roadnetId,
			@Param("beginDate") Date beginDate, @Param("endDate") Date endDate);
}
