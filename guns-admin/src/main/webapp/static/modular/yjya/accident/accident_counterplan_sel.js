/**
 * 管理初始化
 */
var AccidentCounterplanSel = {
    id: "AccidentCounterplanSelTable",	//表格id
    seItem: null,		//选中的条目
    table: null,
    layerIndex: -1,
    data:null
};

/**
 * 初始化表格的列
 */
AccidentCounterplanSel.initColumn = function () {
    return [
        {field: 'selectItem', radio: true},
        {title: '编码', field: 'id', visible: true, align: 'center', valign: 'middle'},
        {title: '预案名称', field: 'name', visible: true, align: 'center', valign: 'middle'},
        {title: '类别', field: 'type', visible: true, align: 'center', valign: 'middle'},
        {title: '事故类型', field: 'accidentTypeName', visible: true, align: 'center', valign: 'middle'},
        {title: '响应等级', field: 'levelName', visible: true, align: 'center', valign: 'middle'},
        {title: '内容', field: 'content', visible: true, align: 'center', valign: 'middle'},
        {title: '地点', field: 'stationName', visible: true, align: 'center', valign: 'middle'},
        {title: '状态', field: 'statusName', visible: true, align: 'center', valign: 'middle'},
        {title: '匹配度', field: 'similarity', visible: true, align: 'center', valign: 'middle'},
        {title: '类型', field: 'typeStatus', visible: true, align: 'center', valign: 'middle'}
    ];
};

/**
 * 根据事故类型筛选应急预案
 * @returns {{}}
 */
AccidentCounterplanSel.queryParams = function () {
    var queryData = {};
    queryData['accidentTypeId'] = $("#accidentTypeId").val();
    queryData['level'] = $("#accidentLevel").val();
    queryData['emergencyId'] = $("#emergencyId").val();
    queryData['emergencyproId'] = $("#emergencyproId").val();
    return queryData;
}

/**
 * 查询列表
 */
AccidentCounterplanSel.search = function (queryData) {

    AccidentCounterplanSel.table.refresh({query: queryData});
};

/**
 * 检查是否选中
 */
AccidentCounterplanSel.check = function () {
    var selected = $('#' + this.id).bootstrapTable('getSelections');
    if(selected.length == 0){
        Feng.info("请先选中表格中的某一记录！");
        return false;
    }else{
        AccidentCounterplanSel.seItem = selected[0];
        return true;
    }
};

AccidentCounterplanSel.getSelectedId = function () {
    if (this.check()) {
        return this.seItem.id;
    }
}
AccidentCounterplanSel.getSelectedType = function () {
    if (this.check()) {
        console.log("判断"+this.seItem.typeStatus);
        //得到选取的是预案还是历史案例
        return this.seItem.typeStatus;
    }
}
AccidentCounterplanSel.init = function (queryData) {
    var defaultColumns = AccidentCounterplanSel.initColumn();
    var table = new BSTable(AccidentCounterplanSel.id, "/counterplan/listSel", defaultColumns);
    table.setPaginationType("server");
    AccidentCounterplanSel.table = table.init();
    AccidentCounterplanSel.table.refresh({query: queryData});
}

