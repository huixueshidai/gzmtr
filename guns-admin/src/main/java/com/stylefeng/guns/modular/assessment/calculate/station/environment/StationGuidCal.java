package com.stylefeng.guns.modular.assessment.calculate.station.environment;

import com.stylefeng.guns.common.persistence.model.Assessment;
import com.stylefeng.guns.core.util.DoubleUtil;
import com.stylefeng.guns.modular.assessment.calculate.ThirdIndicationCalculator;
import com.stylefeng.guns.modular.assessment.calculate.aware.StationThirdIndicationAware;
import org.springframework.stereotype.Component;

import java.util.Date;

/**
 * 车站引导标识合理指数
 */
@Component
public class StationGuidCal extends ThirdIndicationCalculator implements StationThirdIndicationAware {

    @Override
    protected Integer getIndicationId() {
        return 41;
    }

    @Override
    public Assessment calculate(Integer stationId, Date beginDate, Date endDate) {
        //专家总数
        Double total = this.getDoubleValue(stationId, 106, beginDate,endDate);
        Double totolSocre = 0d;
        Integer baseScore = 1;
        for (int i = 107; i <= 111; i++) {
            Double score = DoubleUtil.mul(baseScore, getDoubleValue(stationId, i, beginDate,endDate));
            totolSocre = DoubleUtil.add(totolSocre, score);
            baseScore++;
        }
        Double result = DoubleUtil.safeDiv(totolSocre, total);
        return genResult(result, stationId, beginDate,endDate);
    }
}
