package com.stylefeng.guns.modular.trapped.controller;

import com.baomidou.mybatisplus.plugins.Page;
import com.stylefeng.guns.common.constant.factory.PageFactory;
import com.stylefeng.guns.common.persistence.model.TrappedNumber;
import com.stylefeng.guns.core.base.controller.BaseController;
import com.stylefeng.guns.core.log.LogObjectHolder;
import com.stylefeng.guns.modular.trapped.dao.TrappedNumberMapper;
import com.stylefeng.guns.modular.trapped.service.ITrappedNumberService;
import com.stylefeng.guns.modular.trapped.warpper.TrappedNumberWarpper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;
import java.util.Map;

/**
 * 被困人员录入控制器
 *
 * @author xhq
 * @Date 2018-09-04 09:50:33
 */
@Controller
@RequestMapping("/trappedNumber")
public class TrappedNumberController extends BaseController {

    private String PREFIX = "/trapped/trappedNumber/";

    @Autowired
    private ITrappedNumberService trappedNumberService;
    @Autowired
    private TrappedNumberMapper trappedNumberMapper;

    /**
     * 跳转到被困人员录入首页
     */
    @RequestMapping("")
    public String index() {
        return PREFIX + "trappedNumber.html";
    }

    /**
     * 跳转到添加被困人员录入
     */
    @RequestMapping("/trappedNumber_add")
    public String trappedNumberAdd() {
        return PREFIX + "trappedNumber_add.html";
    }

    /**
     * 跳转到修改被困人员录入
     */
    @RequestMapping("/trappedNumber_update/{trappedNumberId}")
    public String trappedNumberUpdate(@PathVariable Integer trappedNumberId, Model model) {
        TrappedNumber trappedNumber = trappedNumberService.selectById(trappedNumberId);
        model.addAttribute("item",trappedNumber);
        LogObjectHolder.me().set(trappedNumber);
        return PREFIX + "trappedNumber_edit.html";
    }

    /**
     * 获取被困人员录入列表
     */
    @RequestMapping(value = "/list")
    @ResponseBody
    public Object list(String condition) {
        Page<TrappedNumber> page = new PageFactory<TrappedNumber>().defaultPage();
        List<Map<String,Object>> retMap =  trappedNumberMapper.selectMap(page);
        page.setRecords((List<TrappedNumber>) new TrappedNumberWarpper(retMap).warp());
        return super.packForBT(page);

       // return trappedNumberService.selectList(null);
    }

    /**
     * 新增被困人员录入
     */
    @RequestMapping(value = "/add")
    @ResponseBody
    public Object add(TrappedNumber trappedNumber) {
        String trapped = trappedNumber.getTrapped();
        trappedNumber.setTrappedStart(trapped.substring(0,trapped.indexOf("-")));
        trappedNumber.setTrappedEnd(trapped.substring(trapped.indexOf("-")+1,trapped.length()));
        trappedNumberService.insert(trappedNumber);
        return super.SUCCESS_TIP;
    }

    /**
     * 删除被困人员录入
     */
    @RequestMapping(value = "/delete")
    @ResponseBody
    public Object delete(@RequestParam Integer trappedNumberId) {
        trappedNumberService.deleteById(trappedNumberId);
        return SUCCESS_TIP;
    }

    /**
     * 修改被困人员录入
     */
    @RequestMapping(value = "/update")
    @ResponseBody
    public Object update(TrappedNumber trappedNumber) {
        trappedNumberService.updateById(trappedNumber);
        return super.SUCCESS_TIP;
    }

    /**
     * 被困人员录入详情
     */
    @RequestMapping(value = "/detail/{trappedNumberId}")
    @ResponseBody
    public Object detail(@PathVariable("trappedNumberId") Integer trappedNumberId) {
        return trappedNumberService.selectById(trappedNumberId);
    }
}
