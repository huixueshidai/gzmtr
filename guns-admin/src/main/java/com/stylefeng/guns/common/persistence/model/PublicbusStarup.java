package com.stylefeng.guns.common.persistence.model;

import java.io.Serializable;

import com.baomidou.mybatisplus.enums.IdType;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableName;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

/**
 * <p>
 * 
 * </p>
 *
 * @author wangshuaikang
 * @since 2018-09-09
 */
@TableName("gzmtr_publicbus_starup")
@Setter
@Getter
public class PublicbusStarup extends Model<PublicbusStarup> {

    private static final long serialVersionUID = 1L;

	@TableId(value="id", type= IdType.AUTO)
	private Integer id;
    /**
     * 事故线路
     */
	@TableField("line_id")
	private Integer lineId;
    /**
     * 事故车站
     */
	private String stationId;

	/**
	 * 上下行（1：上行2：下行）
	 * @return
	 */
	@TableField("stream")
	public Integer stream;

    /**
     * 乘客滞留时间
     */
	@TableField("retention_time")
	private Integer retentionTime;
    /**
     * 预计中断时间
     */
	@TableField("interruption_time")
	private Integer interruptionTime;
    /**
     * 预计晚点时间
     */
	@TableField("expect_delays")
	private Integer expectDelays;
    /**
     * 预计行车能力降低
     */
	private String reduction;
    /**
     * 中断类型
     */
	@TableField("interruption_type")
	private String interruptionType;
    /**
     * 调度方式
     */
	@TableField("scheduling_mode")
	private String schedulingMode;
    /**
     * 行车间隔
     */
	@TableField("scheduling_mode_min")
	private Integer schedulingModeMin;
    /**
     * 滞留客流损失系数
     */
	@TableField("loss_efficiency_retention")
	private Integer lossEfficiencyRetention;
    /**
     * 疏运客流损失系数
     */
	@TableField("loss_efficiency_transportation")
	private Integer lossEfficiencyTransportation;
    /**
     * 发生时间
     */
	private String startTime;
    /**
     * 用户id
     */
	@TableField("user_id")
	private Integer userId;

	/**
	 * 当前状态
	 */
	@TableField("currten")
	private Integer currten;

	/**
	 * 接驳起始站点
	 */
	@TableField("start_station")
	private String startStation;

	/**
	 * 接驳末尾站点
	 */
	@TableField("end_station")
	private String endStation;

	/**
	 * 接驳站点数量
	 */
	@TableField("station_amount")
	private Integer stationAmount;

	/**
	 * 客流分类(1:工作日，2：休息日，3：节假日)
	 */
	@TableField("date_status")
	private Integer dateStatus;

	/**
	 * 接驳路径
	 */
	@TableField("connection_path")
	private String connectionPath;


	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getLineId() {
		return lineId;
	}

	public void setLineId(Integer lineId) {
		this.lineId = lineId;
	}

	public String getStationId() {
		return stationId;
	}

	public void setStationId(String stationId) {
		this.stationId = stationId;
	}

	public Integer getRetentionTime() {
		return retentionTime;
	}

	public void setRetentionTime(Integer retentionTime) {
		this.retentionTime = retentionTime;
	}

	public Integer getInterruptionTime() {
		return interruptionTime;
	}

	public void setInterruptionTime(Integer interruptionTime) {
		this.interruptionTime = interruptionTime;
	}

	public Integer getExpectDelays() {
		return expectDelays;
	}

	public void setExpectDelays(Integer expectDelays) {
		this.expectDelays = expectDelays;
	}

	public String getReduction() {
		return reduction;
	}

	public void setReduction(String reduction) {
		this.reduction = reduction;
	}

	public String getInterruptionType() {
		return interruptionType;
	}

	public void setInterruptionType(String interruptionType) {
		this.interruptionType = interruptionType;
	}

	public String getSchedulingMode() {
		return schedulingMode;
	}

	public void setSchedulingMode(String schedulingMode) {
		this.schedulingMode = schedulingMode;
	}

	public Integer getSchedulingModeMin() {
		return schedulingModeMin;
	}

	public void setSchedulingModeMin(Integer schedulingModeMin) {
		this.schedulingModeMin = schedulingModeMin;
	}

	public Integer getLossEfficiencyRetention() {
		return lossEfficiencyRetention;
	}

	public void setLossEfficiencyRetention(Integer lossEfficiencyRetention) {
		this.lossEfficiencyRetention = lossEfficiencyRetention;
	}

	public Integer getLossEfficiencyTransportation() {
		return lossEfficiencyTransportation;
	}

	public void setLossEfficiencyTransportation(Integer lossEfficiencyTransportation) {
		this.lossEfficiencyTransportation = lossEfficiencyTransportation;
	}

	public String getStartTime() {
		return startTime;
	}

	public void setStartTime(String startTime) {
		this.startTime = startTime;
	}

	public Integer getUserId() {
		return userId;
	}

	public void setUserId(Integer userId) {
		this.userId = userId;
	}

	@Override
	protected Serializable pkVal() {
		return this.id;
	}

	@Override
	public String toString() {
		return "PublicbusStarup{" +
			"id=" + id +
			", lineId=" + lineId +
			", stationId=" + stationId +
			", retentionTime=" + retentionTime +
			", interruptionTime=" + interruptionTime +
			", expectDelays=" + expectDelays +
			", reduction=" + reduction +
			", interruptionType=" + interruptionType +
			", schedulingMode=" + schedulingMode +
			", schedulingModeMin=" + schedulingModeMin +
			", lossEfficiencyRetention=" + lossEfficiencyRetention +
			", lossEfficiencyTransportation=" + lossEfficiencyTransportation +
			", startTime=" + startTime +
			", userId=" + userId +
			"}";
	}
}
