/**
 * 事故实例处置流程管理初始化
 */
var AccidentDisposaltask = {
    id: "AccidentDisposaltaskTable",	//表格id
    seItem: null,		//选中的条目
    table: null,
    layerIndex: -1
};

/**
 * 初始化表格的列
 */
AccidentDisposaltask.initColumn = function () {
    return [
        {field: 'selectItem', radio: true},
            {title: '', field: 'id', visible: true, align: 'center', valign: 'middle'},
            {title: '事故实例id', field: 'accidentId', visible: true, align: 'center', valign: 'middle'},
            {title: '时间', field: 'time', visible: true, align: 'center', valign: 'middle'},
            {title: '处置操作', field: 'disposalTask', visible: true, align: 'center', valign: 'middle'},
            {title: '步骤', field: 'step', visible: true, align: 'center', valign: 'middle'}
    ];
};

/**
 * 检查是否选中
 */
AccidentDisposaltask.check = function () {
    var selected = $('#' + this.id).bootstrapTable('getSelections');
    if(selected.length == 0){
        Feng.info("请先选中表格中的某一记录！");
        return false;
    }else{
        AccidentDisposaltask.seItem = selected[0];
        return true;
    }
};

/**
 * 点击添加事故实例处置流程
 */
AccidentDisposaltask.openAddAccidentDisposaltask = function () {
    var index = layer.open({
        type: 2,
        title: '添加事故实例处置流程',
        area: ['800px', '420px'], //宽高
        fix: false, //不固定
        maxmin: true,
        content: Feng.ctxPath + '/accidentDisposaltask/accidentDisposaltask_add'
    });
    this.layerIndex = index;
};

/**
 * 打开查看事故实例处置流程详情
 */
AccidentDisposaltask.openAccidentDisposaltaskDetail = function () {
    if (this.check()) {
        var index = layer.open({
            type: 2,
            title: '事故实例处置流程详情',
            area: ['800px', '420px'], //宽高
            fix: false, //不固定
            maxmin: true,
            content: Feng.ctxPath + '/accidentDisposaltask/accidentDisposaltask_update/' + AccidentDisposaltask.seItem.id
        });
        this.layerIndex = index;
    }
};

/**
 * 删除事故实例处置流程
 */
AccidentDisposaltask.delete = function () {
    if (this.check()) {
        var ajax = new $ax(Feng.ctxPath + "/accidentDisposaltask/delete", function (data) {
            Feng.success("删除成功!");
            AccidentDisposaltask.table.refresh();
        }, function (data) {
            Feng.error("删除失败!" + data.responseJSON.message + "!");
        });
        ajax.set("accidentDisposaltaskId",this.seItem.id);
        ajax.start();
    }
};

/**
 * 查询事故实例处置流程列表
 */
AccidentDisposaltask.search = function () {
    var queryData = {};
    queryData['condition'] = $("#condition").val();
    AccidentDisposaltask.table.refresh({query: queryData});
};

$(function () {
    var defaultColunms = AccidentDisposaltask.initColumn();
    var table = new BSTable(AccidentDisposaltask.id, "/accidentDisposaltask/list", defaultColunms);
    table.setPaginationType("client");
    AccidentDisposaltask.table = table.init();
});
