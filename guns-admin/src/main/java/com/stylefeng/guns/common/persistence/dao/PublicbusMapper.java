package com.stylefeng.guns.common.persistence.dao;

import com.stylefeng.guns.common.persistence.model.Publicbus;
import com.baomidou.mybatisplus.mapper.BaseMapper;

/**
 * <p>
  *  Mapper 接口
 * </p>
 *
 * @author wangshuaikang
 * @since 2018-09-03
 */
public interface PublicbusMapper extends BaseMapper<Publicbus> {

}