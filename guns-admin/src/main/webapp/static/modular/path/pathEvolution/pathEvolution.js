/**
 * 初始化事故情景路径演练详情对话框
 */
var PathEvolution = {
    pathEvolutionInfoData : {},
    validateFields: {
        material: {
            validators: {
                notEmpty: {
                    message: '燃烧材料不能为空'
                }
            }
        },
        field: {
            validators: {
                notEmpty: {
                    message: '处置人员场地不能为空'
                }
            }
        },
        rescuers: {
            validators: {
                notEmpty: {
                    message: '处置人员人数不能为空'
                }
            }
        },
        trapped: {
            validators: {
                notEmpty: {
                    message: '被困人员人数不能为空'
                }
            }
        },
    }
};

/**
 * 清除数据
 */
PathEvolution.clearData = function() {
    this.pathEvolutionInfoData = {};
}

/**
 * 设置对话框中的数据
 *
 * @param key 数据的名称
 * @param val 数据的具体值
 */
PathEvolution.set = function(key, val) {
    this.pathEvolutionInfoData[key] = (typeof val == "undefined") ? $("#" + key).val() : val;
    return this;
}

/**
 * 设置对话框中的数据
 *
 * @param key 数据的名称
 * @param val 数据的具体值
 */
PathEvolution.get = function(key) {
    return $("#" + key).val();
}

/**
 * 验证数据是否为空
 */
PathEvolution.validate = function () {
    $('#pathEvolutionForm').data("bootstrapValidator").resetForm();
    $('#pathEvolutionForm').bootstrapValidator('validate');
    return $("#pathEvolutionForm").data('bootstrapValidator').isValid();
}

PathEvolution.openFireSource = function(){
    Feng.newCrontab(Feng.ctxPath + "/fireSource","火源情景状态表");
}

PathEvolution.openTrappedNumber = function(){
    Feng.newCrontab(Feng.ctxPath + "/trappedNumber","被困人员情景状态表");
}

/**
 * 提交添加
 */
PathEvolution.submit = function() {
    if (!this.validate()) {
        return;
    }
    var index = layer.open({
        type: 2,
        title: '路径演变结果',
        area: ['800px', '500px'], //宽高
        fix: false, //不固定
        maxmin: true,
        content: Feng.ctxPath + '/pathEvolution/volution'
    });
    this.layerIndex = index;
}



$(function() {
    Feng.initValidator("pathEvolutionForm", PathEvolution.validateFields);
});
