package com.stylefeng.guns.modular.assessment.calculate.line.passenger;

import com.stylefeng.guns.common.persistence.model.Assessment;
import com.stylefeng.guns.core.util.DoubleUtil;
import com.stylefeng.guns.modular.assessment.calculate.ThirdIndicationCalculator;
import com.stylefeng.guns.modular.assessment.calculate.aware.LineThirdIndicationAware;
import org.springframework.stereotype.Component;

import java.util.Date;

/**
 * 下行区间客流风险指数
 *
 * @author yinjc
 * @since 2018/05/010
 */
@Component
public class LineDownBoundPassengerCal extends ThirdIndicationCalculator implements LineThirdIndicationAware {

    @Override
    protected Integer getIndicationId() {
        return 49;
    }

    @Override
    public Assessment calculate(Integer lineId, Date beginDate, Date endDate) {
        //高峰小时最大断面满载率值(下行)
        double Pmax = getDoubleValue(lineId, 161, beginDate,endDate);
        //线路断面满载率平均值(下行)
        double p = getDoubleValue(lineId, 162, beginDate,endDate);
        //线路中区间满载率值>70%的区间个数(下行)
        double Na = getDoubleValue(lineId, 163, beginDate,endDate);
        //线路区间总个数(下行)
        double N = getDoubleValue(lineId, 164, beginDate,endDate);
        //线路中存在区间满载率>100%的小时个数(下行)
        double nT = getDoubleValue(lineId, 165, beginDate,endDate);
        //线路运营总小时数(下行)
        double t = getDoubleValue(lineId, 166, beginDate,endDate);
        //高峰小时最大断面满载率风险值权重(下行)
        double w1 = getDoubleValue(lineId, 167, beginDate,endDate);
        //满载区间比例风险值权重(下行)
        double w2 = getDoubleValue(lineId, 168, beginDate,endDate);
        //满载时间比例风险值权重(下行)
        double w3 = getDoubleValue(lineId, 169, beginDate,endDate);

        Double d = DoubleUtil.safeDiv(Pmax, p);
        Double D = DoubleUtil.mul(w1, d);

        Double m = DoubleUtil.safeDiv(Na, N);
        Double M = DoubleUtil.mul(m, w2);

        Double i = DoubleUtil.safeDiv(nT, t);
        Double I = DoubleUtil.mul(w3, i);

        Double resVal = DoubleUtil.add(D, M, I);

        return genResult(resVal, lineId, beginDate,endDate);


    }
}
