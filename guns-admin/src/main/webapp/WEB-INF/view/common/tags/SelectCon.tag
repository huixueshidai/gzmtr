@/*
    选择查询条件标签的参数说明:

    name : 查询条件的名称
    id : 查询内容的input框id
@*/

<div class="input-group">
    <div class="input-group-btn">
        <button data-toggle="dropdown" class="btn btn-white dropdown-toggle" type="button">
            ${name}
        </button>
    </div>
    <select class="form-control" name="${id}" id="${id}"
            @if(isNotEmpty(disabled)){
                disabled="${disabled}"
            @}
            @if(isNotEmpty(onchange)){
               onchange="${onchange}"
            @}

    >
        @if(isNotEmpty(code)){
            <option value="">请选择</option>
            @for(item in dict.getByCode(code)){
                 @if(isNotEmpty(value)&& value == item.code){
                    <option value="${item.code}" selected="selected">${item.name}</option>
                 @}else{
                     <option value="${item.code}">${item.name}</option>
                 @}
             @}
        @}else{
             ${tagBody!}
        @}

    </select>
</div>