package com.stylefeng.guns.modular.assessment.calculate.station.accident;

import com.stylefeng.guns.core.util.SpringContextHolder;
import com.stylefeng.guns.modular.assessment.calculate.SecondIndicationCalculator;
import com.stylefeng.guns.modular.assessment.calculate.ThirdIndicationCalculator;
import com.stylefeng.guns.modular.assessment.calculate.aware.StationSecondIndicationAware;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import java.util.Arrays;
import java.util.List;

/**
 * 事故指标
 */
@Component
@Scope(ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public class StationAccidentCal extends SecondIndicationCalculator implements StationSecondIndicationAware {

    private List<ThirdIndicationCalculator> thirdIndicationCalculatorList;

    public StationAccidentCal() {
        thirdIndicationCalculatorList = Arrays.asList(
                SpringContextHolder.getBean(StationAccidentNumCal.class),
                SpringContextHolder.getBean(StationDiedCal.class),
                SpringContextHolder.getBean(StationMoneyLossCal.class)
        );

    }

    @Override
    public List<ThirdIndicationCalculator> getThirdIndicationCalculatorList() {
        return thirdIndicationCalculatorList;
    }


    @Override
    protected Integer getIndicationId() {
        return 21;
    }
}
