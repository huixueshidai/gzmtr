package com.stylefeng.guns.common.persistence.model;

import java.io.Serializable;

import com.baomidou.mybatisplus.enums.IdType;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableName;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * <p>
 * 
 * </p>
 *
 * @author wangshuaikang
 * @since 2018-05-24
 */
@TableName("gzmtr_emergencyproperty")
public class Emergencyproperty extends Model<Emergencyproperty> {

    private static final long serialVersionUID = 1L;

	@TableId(value="id", type= IdType.AUTO)
	private Integer id;
    /**
     * 突发事件ID
     */
	@TableField("emergency_id")
	private Integer emergencyId;
    /**
     * 客流
     */
	private String ridership;
    /**
     * 同行能力
     */
	@TableField("traffic_capacity")
	private String trafficCapacity;
    /**
     * 故障数量
     */
	private String fault;
    /**
     * 气体保护
     */
	@TableField("gas_shield")
	private String gasShield;
    /**
     * 隧道长度
     */
	@TableField("tunnel_length")
	private String tunnelLength;
    /**
     * 状态
     */
	private String status;
    /**
     * 距离前一站距离
     */
	@TableField("distance_on")
	private String distanceOn;
    /**
     * 距离下一站距离
     */
	@TableField("distance_next")
	private String distanceNext;
    /**
     * 被困人员数量
     */
	@TableField("trapped_num")
	private String trappedNum;
    /**
     * 被困人员行动能力
     */
	@TableField("trapped_num_power")
	private String trappedNumPower;
    /**
     * 物理状态
     */
	@TableField("physical_state")
	private String physicalState;
    /**
     * 毒性
     */
	private String toxicity;
    /**
     * 燃爆性
     */
	private String bums;
    /**
     * 腐蚀性
     */
	private String causticity;
    /**
     * 挥发性
     */
	private String volatileness;
    /**
     * 温度
     */
	private String temperature;
    /**
     * 烟气浓度
     */
	private String smokescope;
    /**
     * 坍塌
     */
	private String collapse;
	/**
	 * 失效
	 */
	@TableField("lose_efficacy")
	private String loseEfficacy;
    /**
     * 道路状况
     */
	private String reoad;
    /**
     * 119距离
     */
	@TableField("distance_one_one_nine")
	private String distanceOneOneNine;
    /**
     * 110距离
     */
	@TableField("distance_one_one_zero")
	private String distanceOneOneZero;
	/**
	 * 120距离
	 */
	@TableField("distance_one_two_zero")
	private String distanceOneTwoZero;
    /**
     * 消防设施
     */
	@TableField("fire_control")
	private String fireControl;


	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getEmergencyId() {
		return emergencyId;
	}

	public void setEmergencyId(Integer emergencyId) {
		this.emergencyId = emergencyId;
	}

	public String getRidership() {
		return ridership;
	}

	public void setRidership(String ridership) {
		this.ridership = ridership;
	}

	public String getTrafficCapacity() {
		return trafficCapacity;
	}

	public void setTrafficCapacity(String trafficCapacity) {
		this.trafficCapacity = trafficCapacity;
	}

	public String getFault() {
		return fault;
	}

	public void setFault(String fault) {
		this.fault = fault;
	}

	public String getGasShield() {
		return gasShield;
	}

	public void setGasShield(String gasShield) {
		this.gasShield = gasShield;
	}

	public String getTunnelLength() {
		return tunnelLength;
	}

	public void setTunnelLength(String tunnelLength) {
		this.tunnelLength = tunnelLength;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getDistanceOn() {
		return distanceOn;
	}

	public void setDistanceOn(String distanceOn) {
		this.distanceOn = distanceOn;
	}

	public String getDistanceNext() {
		return distanceNext;
	}

	public void setDistanceNext(String distanceNext) {
		this.distanceNext = distanceNext;
	}

	public String getTrappedNum() {
		return trappedNum;
	}

	public void setTrappedNum(String trappedNum) {
		this.trappedNum = trappedNum;
	}

	public String getTrappedNumPower() {
		return trappedNumPower;
	}

	public void setTrappedNumPower(String trappedNumPower) {
		this.trappedNumPower = trappedNumPower;
	}

	public String getPhysicalState() {
		return physicalState;
	}

	public void setPhysicalState(String physicalState) {
		this.physicalState = physicalState;
	}

	public String getToxicity() {
		return toxicity;
	}

	public void setToxicity(String toxicity) {
		this.toxicity = toxicity;
	}

	public String getBums() {
		return bums;
	}

	public void setBums(String bums) {
		this.bums = bums;
	}

	public String getCausticity() {
		return causticity;
	}

	public void setCausticity(String causticity) {
		this.causticity = causticity;
	}

	public String getVolatileness() {
		return volatileness;
	}

	public void setVolatileness(String volatileness) {
		this.volatileness = volatileness;
	}

	public String getTemperature() {
		return temperature;
	}

	public void setTemperature(String temperature) {
		this.temperature = temperature;
	}

	public String getSmokescope() {
		return smokescope;
	}

	public void setSmokescope(String smokescope) {
		this.smokescope = smokescope;
	}

	public String getCollapse() {
		return collapse;
	}

	public void setCollapse(String collapse) {
		this.collapse = collapse;
	}

	public String getLoseEfficacy() {
		return loseEfficacy;
	}

	public void setLoseEfficacy(String loseEfficacy) {
		this.loseEfficacy = loseEfficacy;
	}

	public String getReoad() {
		return reoad;
	}

	public void setReoad(String reoad) {
		this.reoad = reoad;
	}

	public String getDistanceOneOneNine() {
		return distanceOneOneNine;
	}

	public void setDistanceOneOneNine(String distanceOneOneNine) {
		this.distanceOneOneNine = distanceOneOneNine;
	}

	public String getDistanceOneOneZero() {
		return distanceOneOneZero;
	}

	public void setDistanceOneOneZero(String distanceOneOneZero) {
		this.distanceOneOneZero = distanceOneOneZero;
	}

	public String getDistanceOneTwoZero() {
		return distanceOneTwoZero;
	}

	public void setDistanceOneTwoZero(String distanceOneTwoZero) {
		this.distanceOneTwoZero = distanceOneTwoZero;
	}

	public String getFireControl() {
		return fireControl;
	}

	public void setFireControl(String fireControl) {
		this.fireControl = fireControl;
	}

	@Override
	protected Serializable pkVal() {
		return this.id;
	}

	@Override
	public String toString() {
		return "Emergencyproperty{" +
			"id=" + id +
			", emergencyId=" + emergencyId +
			", ridership=" + ridership +
			", trafficCapacity=" + trafficCapacity +
			", fault=" + fault +
			", gasShield=" + gasShield +
			", tunnelLength=" + tunnelLength +
			", status=" + status +
			", distanceOn=" + distanceOn +
			", distanceNext=" + distanceNext +
			", trappedNum=" + trappedNum +
			", trappedNumPower=" + trappedNumPower +
			", physicalState=" + physicalState +
			", toxicity=" + toxicity +
			", bums=" + bums +
			", causticity=" + causticity +
			", volatileness=" + volatileness +
			", temperature=" + temperature +
			", smokescope=" + smokescope +
			", collapse=" + collapse +
			", loseEfficacy=" + loseEfficacy +
			", reoad=" + reoad +
			", distanceOneOneNine=" + distanceOneOneNine +
			", distanceOneOneZero=" + distanceOneOneZero +
			", distanceOneTwoZero=" + distanceOneTwoZero +
			", fireControl=" + fireControl +
			"}";
	}
	public List<String> getPropertyList(Emergencyproperty emergencyproperty){
		List<String> list = new ArrayList<>();
		list.add(emergencyproperty.getRidership());
		list.add(emergencyproperty.getTrafficCapacity());
		list.add(emergencyproperty.getFault());
		list.add(emergencyproperty.getGasShield());
		list.add(emergencyproperty.getTunnelLength());
		list.add(emergencyproperty.getStatus());
		list.add(emergencyproperty.getDistanceOn());
		list.add(emergencyproperty.getDistanceNext());
		list.add(emergencyproperty.getTrappedNum());
		list.add(emergencyproperty.getTrappedNumPower());
		list.add(emergencyproperty.getPhysicalState());
		list.add(emergencyproperty.getToxicity());
		list.add(emergencyproperty.getBums());
		list.add(emergencyproperty.getCausticity());
		list.add(emergencyproperty.getVolatileness());
		list.add(emergencyproperty.getTemperature());
		list.add(emergencyproperty.getSmokescope());
		list.add(emergencyproperty.getCollapse());
		list.add(emergencyproperty.getLoseEfficacy());
		list.add(emergencyproperty.getReoad());
		list.add(emergencyproperty.getDistanceOneOneNine());
		list.add(emergencyproperty.getDistanceOneOneZero());
		list.add(emergencyproperty.getDistanceOneTwoZero());
		list.add(emergencyproperty.getFireControl());

		return list;

	}

	public void setEmergency (List<String> list){
		this.setRidership(list.get(0));
		this.setTrafficCapacity(list.get(1));
		this.setFault(list.get(2));
		this.setGasShield(list.get(3));
		this.setTunnelLength(list.get(4));
		this.setStatus(list.get(5));
		this.setDistanceOn(list.get(6));
		this.setDistanceNext(list.get(7));
		this.setTrappedNum(list.get(8));
		this.setTrappedNumPower(list.get(9));
		this.setPhysicalState(list.get(10));
		this.setToxicity(list.get(11));
		this.setBums(list.get(12));
		this.setCausticity(list.get(13));
		this.setVolatileness(list.get(14));
		this.setTemperature(list.get(15));
		this.setSmokescope(list.get(16));
		this.setCollapse(list.get(17));
		this.setLoseEfficacy(list.get(18));
		this.setReoad(list.get(19));
		this.setDistanceOneOneNine(list.get(20));
		this.setDistanceOneOneZero(list.get(21));
		this.setDistanceOneTwoZero(list.get(22));
		this.setFireControl(list.get(23));
	}
}
