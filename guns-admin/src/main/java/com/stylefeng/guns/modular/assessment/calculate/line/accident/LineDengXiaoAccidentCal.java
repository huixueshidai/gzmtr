package com.stylefeng.guns.modular.assessment.calculate.line.accident;

import com.stylefeng.guns.common.persistence.model.Assessment;
import com.stylefeng.guns.core.util.DoubleUtil;
import com.stylefeng.guns.modular.assessment.calculate.ThirdIndicationCalculator;
import com.stylefeng.guns.modular.assessment.calculate.aware.LineThirdIndicationAware;
import org.springframework.stereotype.Component;

import java.util.Date;

/**
 * 线路等效事故事件率
 */
@Component
public class LineDengXiaoAccidentCal extends ThirdIndicationCalculator implements LineThirdIndicationAware {
    @Override
    protected Integer getIndicationId() {
        return 72;
    }

    @Override
    public Assessment calculate(Integer lineId, Date beginDate, Date endDate) {
        Double veryMax = getDoubleValue(lineId, 261, beginDate,endDate);
        Double zhong = getDoubleValue(lineId, 262, beginDate,endDate);
        Double da = getDoubleValue(lineId, 263, beginDate,endDate);
        Double dangerous = getDoubleValue(lineId, 264, beginDate,endDate);
        Double general = getDoubleValue(lineId, 265, beginDate,endDate);
        Double particularly = getDoubleValue(lineId, 266, beginDate,endDate);
        Double zhongDa = getDoubleValue(lineId, 267, beginDate,endDate);
        Double daYinzi = getDoubleValue(lineId, 268, beginDate,endDate);
        Double dangerousYinZi = getDoubleValue(lineId, 269, beginDate,endDate);
        Double yiBanYinZi = getDoubleValue(lineId, 270, beginDate,endDate);
        Double car = getDoubleValue(lineId, 271, beginDate,endDate);
        double resValueO = DoubleUtil.mul(veryMax, particularly);
        double resValueT = DoubleUtil.mul(zhong, zhongDa);
        double resValueH = DoubleUtil.mul(da, daYinzi);
        double resValueF = DoubleUtil.mul(dangerous, dangerousYinZi);
        double resValueV = DoubleUtil.mul(general, yiBanYinZi);
        double he = DoubleUtil.add(resValueO, resValueT, resValueH, resValueF, resValueV);
        //等效事故率 quivalentAccidentRate
        double quivalentAccidentRate = DoubleUtil.safeDiv(he, car);
        return genResult(quivalentAccidentRate, lineId, beginDate,endDate);
    }
}
