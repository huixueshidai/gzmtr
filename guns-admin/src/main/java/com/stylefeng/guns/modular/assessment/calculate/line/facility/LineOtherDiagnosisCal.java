package com.stylefeng.guns.modular.assessment.calculate.line.facility;

import com.stylefeng.guns.common.persistence.model.Assessment;
import com.stylefeng.guns.core.util.DoubleUtil;
import com.stylefeng.guns.modular.assessment.calculate.ThirdIndicationCalculator;
import com.stylefeng.guns.modular.assessment.calculate.aware.LineThirdIndicationAware;
import org.springframework.stereotype.Component;

import java.util.Date;

/**
 * 其他因素影响运营风险指数
 *
 * @author yinjc
 * @since 2018/05/10
 */
@Component
public class LineOtherDiagnosisCal extends ThirdIndicationCalculator implements LineThirdIndicationAware {
    @Override
    protected Integer getIndicationId() {
        return 68;
    }

    @Override
    public Assessment calculate(Integer lineId, Date beginDate, Date endDate) {
        //线路其他因素故障的时间
        Double faultDuration = getDoubleValue(lineId, 244, beginDate,endDate);
        //其他因素总运行时间
        Double totalDuration = getDoubleValue(lineId, 245, beginDate,endDate);

        //受其他因素影响所耽误的列车正线运营里程
        Double faultMileage = getDoubleValue(lineId, 246, beginDate,endDate);
        //列车正线计划运营里程数(其他因素)
        Double planMileage = getDoubleValue(lineId, 247, beginDate,endDate);

        //线路中其他设备发生故障的次数
        Double faultTime = getDoubleValue(lineId, 248, beginDate,endDate);
        //线路列车运营公里(其他因素)
        Double runMileage = getDoubleValue(lineId, 249, beginDate,endDate);

        //其他因素故障率权重
        Double rateWeight = getDoubleValue(lineId, 250, beginDate,endDate);

        //其他因素故障时间权重
        Double durationWeight = getDoubleValue(lineId, 251, beginDate,endDate);
        //其他因素故障影响范围权重
        Double rangeWeight = getDoubleValue(lineId, 252, beginDate,endDate);


        double resVal = DoubleUtil.add(
                DoubleUtil.mul(durationWeight, DoubleUtil.safeDiv(faultDuration, totalDuration)),
                DoubleUtil.mul(rangeWeight, DoubleUtil.safeDiv(faultMileage, planMileage)),
                DoubleUtil.mul(rateWeight, DoubleUtil.safeDiv(faultTime, runMileage)));

        return genResult(resVal, lineId, beginDate,endDate);
    }
}
