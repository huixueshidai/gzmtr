package com.stylefeng.guns.modular.bus.service;

import com.stylefeng.guns.common.persistence.model.PublicBusType;
import com.baomidou.mybatisplus.service.IService;

/**
 * <p>
 * 车辆类型 服务类
 * </p>
 *
 * @author yinjc
 * @since 2018-03-21
 */
public interface IPublicBusTypeService extends IService<PublicBusType> {
	
}
