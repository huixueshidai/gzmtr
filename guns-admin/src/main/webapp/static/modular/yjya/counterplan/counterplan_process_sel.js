var CounterplanProcessSel = {
    id: "AccidentProcessSelTable",	//表格id
    seItems: [],		//选中的条目
    table: null,
    layerIndex: -1,
    typeId: 0,
    processNum: 0,
    processIds: [],
    queryData:{},
    map:{},
    zTreepositionStatementInstance:null

};


CounterplanProcessSel.initColumn = function () {
    return [
        {field: 'selectItem', chechbox: true},
        {title: 'id', field: 'id', visible: true, align: 'center', valign: 'middle'},
        {title: '主体', field: 'main', visible: true, align: 'center', valign: 'middle'},
        {title: '主体属性', field: 'mproperty', visible: true, align: 'center', valign: 'middle'},
        {title: '主体动作', field: 'mmotion', visible: true, align: 'center', valign: 'middle'},
        {title: '客体', field: 'object', visible: true, align: 'center', valign: 'middle'},
        {title: '客体属性', field: 'oproperty', visible: true, align: 'center', valign: 'middle'},
        {title: '客体动作', field: 'omotion', visible: true, align: 'center', valign: 'middle'},
        {title: '分组', field: 'typeName', visible: true, align: 'center', valign: 'middle'}
    ];
};

/**
 * 关闭此对话框
 */
CounterplanProcessSel.close = function () {
    parent.layer.close(window.parent.CounterplanInfoDlgEdit.layerIndex);
}

CounterplanProcessSel.getItemString = function (id) {
    var res = "";
    var ajax = new $ax(Feng.ctxPath + "/accidentProcess/string/" + id,
        function (data) {
            res = data;
        }, function (data) {

        }
    );
    ajax.start();
    return res;
}

CounterplanProcessSel.getmainAction = function (id) {
    var res = "";
    var ajax = new $ax(Feng.ctxPath + "/accidentProcess/mainAction/" + id,
        function (data) {
            res = data;
        }, function (data) {

        }
    );
    ajax.start();
    return res;
}


CounterplanProcessSel.resetSel = function () {
    this.processIds = [];
    this.processNum = 0;
    $("#processCon").val("");
}

/**
 * 添加
 */
CounterplanProcessSel.addAccidents = function () {
    if (this.check()) {
        this.processNum++;
        var setItemIds = this.seItems;
        this.processIds.push(setItemIds.join(",") + "");

        var step = "第" + this.processNum + "步\n";
        for (var x in setItemIds) {
            var itemString = this.getItemString(setItemIds[x]);
            var mainAccident = this.getmainAction(setItemIds[x]);
            if(this.map[mainAccident.main]){
                this.map[mainAccident.main]+=(mainAccident.mproperty+mainAccident.mmotion+mainAccident.object+mainAccident.oproperty+mainAccident.omotion+"；<br/>");
            }else{
                this.map[mainAccident.main]= (mainAccident.mproperty+mainAccident.mmotion+mainAccident.object+mainAccident.oproperty+mainAccident.omotion+"；<br/>");
            }
            step += (itemString + "\n");
        }
        $("#processCon").val($("#processCon").val() + step + "\n");
        //取消选中
        $('#' + this.id).bootstrapTable('uncheckAll');

    }
}

/**
 * 重置
 */
CounterplanProcessSel.resetSearch = function(){
    this.queryData['main'] = null;
    CounterplanProcessSel.table.refresh(this.queryData);
}

CounterplanProcessSel.submit = function () {
    window.parent.CounterplanInfoDlgEdit.processIds = this.processIds;
    var processCon = $("#processCon").val();
    processCon = processCon.replace(/\n/g,"<br>");
    var main="";
    var mainMmotion="";
    for(var x in this.map){
        var mainOne = "<td width=\"199\" valign=\"top\">"+x+"</td>";
        var mainMmotionOne = "<td width=\"199\" valign=\"top\">"+this.map[x]+"</td>";
        main+=mainOne;
        mainMmotion+=mainMmotionOne;
    }
    var table =
        "<table>" +
        "    <tbody>" +
        "        <tr class=\"firstRow\">" +
        main +
        "        </tr>";
    table+=
        "<tr>" +
        mainMmotion +
        "</tr>";
    window.parent.CounterplanInfoDlgEdit.ue.execCommand('inserthtml', processCon);
    window.parent.CounterplanInfoDlgEdit.ue.execCommand('inserthtml', table);
    this.close()
}

CounterplanProcessSel.getSelections = function () {
    var _ipt = $('#' + this.id).find("tbody").find("tr").find("input[name='btSelectItem']:checked");
    var chk_value = [];
    _ipt.each(function (_i, _item) {
        chk_value.push($(_item).val());
    });
    return chk_value;
}

CounterplanProcessSel.check = function () {
    var selected = CounterplanProcessSel.getSelections();

    if (selected.length == 0) {
        Feng.info("请先选中表格中的某一记录！");
        return false;
    } else {
        CounterplanProcessSel.seItems = selected;
        return true;
    }
};

/**
 * 查询事故处理措施列表
 */
CounterplanProcessSel.search = function () {
    this.queryData['main'] = $("#main").val();
    CounterplanProcessSel.table.refresh({query: this.queryData});
};


CounterplanProcessSel.onClickContactGroup = function (e, treeId, treeNode) {
    CounterplanProcessSel.typeId = treeNode.id;
    CounterplanProcessSel.search();
}

/**
 * 点击ztree列表的选项时
 *
 * @param e
 * @param treeId
 * @param treeNode
 * @returns
 *
 */
CounterplanProcessSel.onClickPositionStatement = function (e, treeId, treeNode) {
    $("#main").attr("value", CounterplanProcessSel.zTreepositionStatementInstance.getSelectedVal());
    $("#parentTreeDiv").fadeOut("fast");
}

/**
 * 显示tree
 */
CounterplanProcessSel.showPositionStatementSelectTree = function () {
    Feng.showInputTree("main", "parentTreeDiv", 15, 34);
}


$(function () {
    var processTypeId = window.parent.CounterplanInfoDlgEdit.get('accidentTypeId');
    CounterplanProcessSel.queryData['typeId'] = processTypeId;
    var processMain = window.parent.CounterplanInfoDlgEdit.get('accidentMain');
    if(processMain!=""){
        CounterplanProcessSel.queryData['main'] = processMain;
    }

    var defaultColunms = CounterplanProcessSel.initColumn();
    var table = new BSTable(CounterplanProcessSel.id, "/accidentProcess/list", defaultColunms);
    table.setQueryParams(CounterplanProcessSel.queryData);
    table.setPaginationType("server");
    table.setIdField("id");
    table.setPageSize(500);
    CounterplanProcessSel.table = table.init();


    var ztree = new $ZTree("accidentGroupTree", "/accidentType/tree");
    ztree.bindOnClick(CounterplanProcessSel.onClickContactGroup);
    ztree.init();

    //主体tree
    var positionStatementztree = new $ZTree("parentTree", "/positionStatement/tree");
    positionStatementztree.bindOnClick(CounterplanProcessSel.onClickPositionStatement);
    positionStatementztree.init();
    CounterplanProcessSel.zTreepositionStatementInstance = positionStatementztree;

});
