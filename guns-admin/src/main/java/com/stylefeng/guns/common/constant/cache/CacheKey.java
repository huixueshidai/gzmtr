package com.stylefeng.guns.common.constant.cache;

/**
 * 缓存的key集合
 *
 * @author fengshuonan
 * @date 2017-04-25 9:37
 */
public interface CacheKey {

    /**
     * ConstantFactory中的缓存
     */
    String ROLES_NAME = "roles_name_";

    String SINGLE_ROLE_NAME = "single_role_name_";

    String SINGLE_ROLE_TIP = "single_role_tip_";

    String DEPT_NAME = "dept_name_";

    String ROADNET_ID = "roadnet_id_";

    String ROADNAME_ID = "roadnet_name_";

    String STATION_NAME = "station_name_";

    String INDICATION_ID = "indication_id_";

    String RESOURCE_DATA_TYPE_ID = "resource_data_type_id_";
}
