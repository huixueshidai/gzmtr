/**
 * Created by 60132 on 2018/9/21.
 */
var CounterplanEditProcessSel = {
    id: "AccidentProcessSelTable",	//表格id
    seItems: [],		//选中的条目
    table: null,
    layerIndex: -1,
    typeId: 0,
    processNum: 0,
    processIds: [],
    queryData:{},
    map:{}

};


CounterplanEditProcessSel.initColumn = function () {
    return [
        {field: 'selectItem', chechbox: true},
        {title: 'id', field: 'id', visible: true, align: 'center', valign: 'middle'},
        {title: '主体', field: 'main', visible: true, align: 'center', valign: 'middle'},
        {title: '主体属性', field: 'mproperty', visible: true, align: 'center', valign: 'middle'},
        {title: '主体动作', field: 'mmotion', visible: true, align: 'center', valign: 'middle'},
        {title: '客体', field: 'object', visible: true, align: 'center', valign: 'middle'},
        {title: '客体属性', field: 'oproperty', visible: true, align: 'center', valign: 'middle'},
        {title: '客体动作', field: 'omotion', visible: true, align: 'center', valign: 'middle'},
        {title: '分组', field: 'typeName', visible: true, align: 'center', valign: 'middle'}
    ];
};

/**
 * 关闭此对话框
 */
CounterplanEditProcessSel.close = function () {
    parent.layer.close(window.parent.CounterplanInfoDlg.layerIndex);
}

CounterplanEditProcessSel.getItemString = function (id) {
    var res = "";
    var ajax = new $ax(Feng.ctxPath + "/accidentProcess/string/" + id,
        function (data) {
            res = data;
        }, function (data) {

        }
    );
    ajax.start();
    return res;
}

CounterplanEditProcessSel.getmainAction = function (id) {
    var res = "";
    var ajax = new $ax(Feng.ctxPath + "/accidentProcess/mainAction/" + id,
        function (data) {
            res = data;
        }, function (data) {

        }
    );
    ajax.start();
    return res;
}


CounterplanEditProcessSel.resetSel = function () {
    this.processIds = [];
    this.processNum = 0;
    $("#processCon").val("");
}

/**
 * 添加
 */
CounterplanEditProcessSel.addAccidents = function () {
    if (this.check()) {
        this.processNum++;
        var setItemIds = this.seItems;
        this.processIds.push(setItemIds.join(",") + "");

        var step = "第" + this.processNum + "步\n";
        for (var x in setItemIds) {
            var itemString = this.getItemString(setItemIds[x]);
            var mainAccident = this.getmainAction(setItemIds[x]);
            if(this.map[mainAccident.main]){
                this.map[mainAccident.main]+=(mainAccident.mproperty+mainAccident.mmotion+mainAccident.object+mainAccident.oproperty+mainAccident.omotion+"；<br/>");
            }else{
                this.map[mainAccident.main]= (mainAccident.mproperty+mainAccident.mmotion+mainAccident.object+mainAccident.oproperty+mainAccident.omotion+"；<br/>");
            }
            step += (itemString + "\n");
        }
        $("#processCon").val($("#processCon").val() + step + "\n");
        //取消选中
        $('#' + this.id).bootstrapTable('uncheckAll');

    }
}

/**
 * 重置
 */
CounterplanEditProcessSel.resetSearch = function(){
    this.queryData['main'] = null;
    CounterplanEditProcessSel.table.refresh(this.queryData);
}

CounterplanEditProcessSel.submit = function () {
    window.parent.CounterplanInfoDlg.processIds = this.processIds;
    var processCon = $("#processCon").val();
    processCon = processCon.replace(/\n/g,"<br>");
    var main="";
    var mainMmotion="";
    for(var x in this.map){
        var mainOne = "<td width=\"199\" valign=\"top\">"+x+"</td>";
        var mainMmotionOne = "<td width=\"199\" valign=\"top\">"+this.map[x]+"</td>";
        main+=mainOne;
        mainMmotion+=mainMmotionOne;
    }
    var table =
        "<table>" +
        "    <tbody>" +
        "        <tr class=\"firstRow\">" +
        main +
        "        </tr>";
    table+=
        "<tr>" +
        mainMmotion +
        "</tr>";
    window.parent.CounterplanInfoDlg.ue.execCommand('inserthtml', processCon);
    window.parent.CounterplanInfoDlg.ue.execCommand('inserthtml', table);
    console.log(this.processIds);
    this.close()
}

CounterplanEditProcessSel.getSelections = function () {
    var _ipt = $('#' + this.id).find("tbody").find("tr").find("input[name='btSelectItem']:checked");
    var chk_value = [];
    _ipt.each(function (_i, _item) {
        chk_value.push($(_item).val());
    });
    return chk_value;
}

CounterplanEditProcessSel.check = function () {
    var selected = CounterplanEditProcessSel.getSelections();

    if (selected.length == 0) {
        Feng.info("请先选中表格中的某一记录！");
        return false;
    } else {
        CounterplanEditProcessSel.seItems = selected;
        return true;
    }
};

/**
 * 查询事故处理措施列表
 */
CounterplanEditProcessSel.search = function () {
    this.queryData['main'] = $("#main").val();
    CounterplanEditProcessSel.table.refresh({query: this.queryData});
};


CounterplanEditProcessSel.onClickContactGroup = function (e, treeId, treeNode) {
    CounterplanEditProcessSel.typeId = treeNode.id;
    CounterplanEditProcessSel.search();
}


$(function () {
    var processTypeId = window.parent.CounterplanInfoDlg.get('accidentTypeId');
    CounterplanEditProcessSel.queryData['typeId'] = processTypeId;
    var processMain = window.parent.CounterplanInfoDlg.get('accidentMain');
    console.log(!processMain);
    if(processMain!=""){
        CounterplanEditProcessSel.queryData['main'] = processMain;
    }

    var defaultColunms = CounterplanEditProcessSel.initColumn();
    var table = new BSTable(CounterplanEditProcessSel.id, "/accidentProcess/list", defaultColunms);
    console.log(CounterplanEditProcessSel.queryData)
    table.setQueryParams(CounterplanEditProcessSel.queryData);
    table.setPaginationType("server");
    table.setIdField("id");
    table.setPageSize(500);
    CounterplanEditProcessSel.table = table.init();


    var ztree = new $ZTree("accidentGroupTree", "/accidentType/tree");
    ztree.bindOnClick(CounterplanEditProcessSel.onClickContactGroup);
    ztree.init();

});
