package com.stylefeng.guns.modular.assessment.calculate.station.passenger;

import com.stylefeng.guns.common.persistence.model.Assessment;
import com.stylefeng.guns.core.util.DoubleUtil;
import com.stylefeng.guns.modular.assessment.calculate.ThirdIndicationCalculator;
import com.stylefeng.guns.modular.assessment.calculate.aware.StationThirdIndicationAware;
import org.springframework.stereotype.Component;

import java.util.Date;

/**
 * 通道拥挤度
 */
@Component
public class StationChannelCal extends ThirdIndicationCalculator implements StationThirdIndicationAware {

    @Override
    protected Integer getIndicationId() {
        return 13;
    }

    @Override
    public Assessment calculate(Integer stationId, Date beginDate, Date endDate) {
        Double c = getDoubleValue(stationId, 17, beginDate,endDate);
        Double Cmax = getDoubleValue(stationId, 274, beginDate,endDate);
        Double T = getDoubleValue(stationId, 275, beginDate,endDate);
        Double d = getDoubleValue(stationId, 18, beginDate,endDate);
        Double result = DoubleUtil.safeDiv(c, DoubleUtil.mul(Cmax, T, d));
        return genResult(result, stationId, beginDate,endDate);
    }
}
