package com.stylefeng.guns.common.persistence.dao;

import com.stylefeng.guns.common.persistence.model.Assessment;
import com.baomidou.mybatisplus.mapper.BaseMapper;
import org.apache.ibatis.annotations.Param;

import java.util.Date;

/**
 * <p>
  * 指标评估 Mapper 接口
 * </p>
 *
 * @author xuziyang
 * @since 2018-03-27
 */
public interface AssessmentMapper extends BaseMapper<Assessment> {
    Assessment selectAssessment(@Param("indicationId") Integer indicationId,
                                @Param("roadnetId") Integer roadnetId,
                                @Param("beginTime")  String beginTime,
                                @Param("endTime") String endTime);
}