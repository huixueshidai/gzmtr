/**
 * 初始化详情对话框
 */
var RoadnetInfoDlg = {
    roadnetInfoData: {},
    zTreeInstance: null,
    validateFields: {
        name: {
            validators: {
                notEmpty: {
                    message: '名称不能为空'
                }
            }
        },
        code: {
            validators: {
                notEmpty: {
                    message: '编号不能为空'
                }
            }
        },
        num: {
            validators: {
                notEmpty: {
                    message: '排序不能为空'
                },
                digits: {
                    message: '排序必须为数字'
                }
            }
        },
        pName: {
            validators: {
                notEmpty: {
                    message: '层级不能为空'
                }
            }
        }
    }
};

/**
 * 清除数据
 */
RoadnetInfoDlg.clearData = function () {
    this.roadnetInfoData = {};
}

/**
 * 设置对话框中的数据
 *
 * @param key 数据的名称
 * @param val 数据的具体值
 */
RoadnetInfoDlg.set = function (key, val) {
    this.roadnetInfoData[key] = (typeof val == "undefined") ? $("#" + key).val() : val;
    return this;
}

/**
 * 设置对话框中的数据
 *
 * @param key 数据的名称
 * @param val 数据的具体值
 */
RoadnetInfoDlg.get = function (key) {
    return $("#" + key).val();
}

/**
 * 关闭此对话框
 */
RoadnetInfoDlg.close = function () {
    parent.layer.close(window.parent.Roadnet.layerIndex);
}

/**
 * 收集数据
 */
RoadnetInfoDlg.collectData = function () {
    this
        .set('id')
        .set('code')
        .set('pid')
        .set('pids')
        .set('name')
        .set('shortName')
        .set('type')
        .set('num')
        .set('isUsed')
    ;
}
/**
 * 验证数据是否为空
 */
RoadnetInfoDlg.validate = function () {
    $('#roadnetInfoForm').data("bootstrapValidator").resetForm();
    $('#roadnetInfoForm').bootstrapValidator('validate');
    return $("#roadnetInfoForm").data('bootstrapValidator').isValid();
}

/**
 * 提交添加
 */
RoadnetInfoDlg.addSubmit = function () {

    this.clearData();
    this.collectData();
    if (!this.validate()) {
        return;
    }

    //提交信息
    var ajax = new $ax(Feng.ctxPath + "/roadnet/add", function (data) {
        Feng.success("添加成功!");
        window.parent.Roadnet.table.refresh();
        RoadnetInfoDlg.close();
    }, function (data) {
        Feng.error("添加失败!" + data.responseJSON.message + "!");
    });
    ajax.set(this.roadnetInfoData);
    ajax.start();
}

/**
 * 提交修改
 */
RoadnetInfoDlg.editSubmit = function () {

    this.clearData();
    this.collectData();
    if (!this.validate()) {
        return;
    }

    //提交信息
    var ajax = new $ax(Feng.ctxPath + "/roadnet/update", function (data) {
        Feng.success("修改成功!");
        window.parent.Roadnet.table.refresh();
        RoadnetInfoDlg.close();
    }, function (data) {
        Feng.error("修改失败!" + data.responseJSON.message + "!");
    });
    ajax.set(this.roadnetInfoData);
    ajax.start();
}

/**
 * 点击路网ztree列表的选项时
 *
 * @param e
 * @param treeId
 * @param treeNode
 * @returns
 */
RoadnetInfoDlg.onClickDept = function (e, treeId, treeNode) {
    $("#pName").attr("value", RoadnetInfoDlg.zTreeInstance.getSelectedVal());
    $("#pid").attr("value", treeNode.id);
    $("#parentRoadnetMenu").fadeOut("fast");
}

/**
 * 显示路网选择的树
 *
 * @returns
 */
RoadnetInfoDlg.showRoadnetSelectTree = function () {
    var pName = $("#pName");
    var pNameOffset = $("#pName").offset();//获得pName的偏移坐标（left top）
    $("#parentRoadnetMenu").css({
        left: pNameOffset.left + "px",
        top: pNameOffset.top + pName.outerHeight() + "px"
    }).slideDown("fast");

    $("body").bind("mousedown", onBodyDown);//两个参数（鼠标按下,回调函数）

}

/**
 * 路网选择框鼠标按下回调函数
 */
function onBodyDown(event) {
    if (!(event.target.id == "menuBtn" || event.target.id == "parentRoadnetMenu" || $(
            event.target).parents("#parentRoadnetMenu").length > 0)) {
        RoadnetInfoDlg.hideRoadnetSelectTree();
    }
}

/**
 * 隐藏路网选择的树
 */
RoadnetInfoDlg.hideRoadnetSelectTree = function () {
    $("#parentRoadnetMenu").fadeOut("fast");
    $("body").unbind("mousedown", onBodyDown);// mousedown当鼠标按下就可以触发，不用弹起
}


$(function () {
    Feng.initValidator("roadnetInfoForm", RoadnetInfoDlg.validateFields);

    //初始化性别选项
    $("#isUsed").val($("#isUsedValue").val());

    var ztree = new $ZTree("parentRoadnetMenuTree", "/roadnet/tree");
    ztree.bindOnClick(RoadnetInfoDlg.onClickDept);
    ztree.init();
    RoadnetInfoDlg.zTreeInstance = ztree;
});
