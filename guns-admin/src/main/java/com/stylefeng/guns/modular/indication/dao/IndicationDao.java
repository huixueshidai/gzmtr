package com.stylefeng.guns.modular.indication.dao;

import com.stylefeng.guns.common.persistence.model.Indication;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface IndicationDao {

    /**
     * 查询三级指标
     * @param level
     * @return
     */
    List<Indication> selectTrdIndications(@Param("level") Integer level,@Param("pids")Integer pids);

    List<Indication> selectChildren(@Param("pid") Integer pid);

    String selectNameById(@Param("id") Integer id);

    Indication selectById(@Param("id") Integer id);
}
