package com.stylefeng.guns.modular.assessment.calculate.road.manage;

import com.stylefeng.guns.core.util.SpringContextHolder;
import com.stylefeng.guns.modular.assessment.calculate.SecondIndicationCalculator;
import com.stylefeng.guns.modular.assessment.calculate.ThirdIndicationCalculator;
import com.stylefeng.guns.modular.assessment.calculate.aware.RoadnetSecondIndicationAware;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import java.util.Arrays;
import java.util.List;

@Component
@Scope(ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public class RoadManageCal extends SecondIndicationCalculator implements RoadnetSecondIndicationAware {

    private List<ThirdIndicationCalculator> thirdIndicationCalculatorList;

    public RoadManageCal() {
        thirdIndicationCalculatorList = Arrays.asList(
                SpringContextHolder.getBean(RoadRegionalCenterSecurityCal.class)
        );
    }

    @Override
    public List<ThirdIndicationCalculator> getThirdIndicationCalculatorList() {
        return thirdIndicationCalculatorList;
    }


    @Override
    protected Integer getIndicationId() {
        return 94;
    }
}
