package com.stylefeng.guns.modular.prediction.service;

import com.stylefeng.guns.common.persistence.model.PredictionData;
import com.baomidou.mybatisplus.service.IService;

/**
 * <p>
 * 风险预测数据 服务类
 * </p>
 *
 * @author xuziyang
 * @since 2018-05-19
 */
public interface IPredictionDataService extends IService<PredictionData> {
	
}
