package com.stylefeng.guns.modular.assessment.calculate.line.facility;

import com.stylefeng.guns.common.persistence.model.Assessment;
import com.stylefeng.guns.core.util.DoubleUtil;
import com.stylefeng.guns.modular.assessment.calculate.ThirdIndicationCalculator;
import com.stylefeng.guns.modular.assessment.calculate.aware.LineThirdIndicationAware;
import org.springframework.stereotype.Component;

import java.util.Date;

/**
 * 机电系统影响运营风险指数
 *
 * @author yinjc
 * @since 2018/05/09
 */
@Component
public class LineInfluenceOfElectromechanicalSystemCal extends ThirdIndicationCalculator implements LineThirdIndicationAware {
    @Override
    protected Integer getIndicationId() {
        return 64;
    }

    @Override
    public Assessment calculate(Integer lineId, Date beginDate, Date endDate) {
        //
        Double lv = getDoubleValue(lineId, 223, beginDate,endDate);
        Double guZhang = getDoubleValue(lineId, 225, beginDate,endDate);
        Double cuShu = getDoubleValue(lineId, 226, beginDate,endDate);
        Double liCheng = getDoubleValue(lineId, 227, beginDate,endDate);
        Double lieChe = getDoubleValue(lineId, 228, beginDate,endDate);
        double tM = DoubleUtil.safeDiv(guZhang, cuShu);
        double m = DoubleUtil.sub(lieChe, liCheng);
        double M = DoubleUtil.safeDiv(m, lieChe);
        double T = DoubleUtil.safeDiv(1.0, tM);
        double zhiShu = DoubleUtil.add(M, T, lv);
        return genResult(zhiShu, lineId, beginDate,endDate);
    }
}
