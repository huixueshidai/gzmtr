@var spaceCss = "";
@var btnType = "";
@if(isEmpty(space)){
@   spaceCss = "";
@}else{
@   spaceCss = "button-margin";
@}
@if(isEmpty(btnCss)){
@   btnType = "primary";
@}else{
@   btnType = btnCss;
@}
<space type="button" class="btn btn-${btnType} ${spaceCss} upload-btn" onclick="${clickFun!}" id="${id!}">
    <i class="fa ${icon}"></i>&nbsp;${name}
</space>
