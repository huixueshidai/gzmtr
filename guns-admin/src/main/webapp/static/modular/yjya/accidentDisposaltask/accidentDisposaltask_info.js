/**
 * 初始化事故实例处置流程详情对话框
 */
var AccidentDisposaltaskInfoDlg = {
    accidentDisposaltaskInfoData : {}
};

/**
 * 清除数据
 */
AccidentDisposaltaskInfoDlg.clearData = function() {
    this.accidentDisposaltaskInfoData = {};
}

/**
 * 设置对话框中的数据
 *
 * @param key 数据的名称
 * @param val 数据的具体值
 */
AccidentDisposaltaskInfoDlg.set = function(key, val) {
    this.accidentDisposaltaskInfoData[key] = (typeof val == "undefined") ? $("#" + key).val() : val;
    return this;
}

/**
 * 设置对话框中的数据
 *
 * @param key 数据的名称
 * @param val 数据的具体值
 */
AccidentDisposaltaskInfoDlg.get = function(key) {
    return $("#" + key).val();
}

/**
 * 关闭此对话框
 */
AccidentDisposaltaskInfoDlg.close = function() {
    parent.layer.close(window.parent.AccidentDisposaltask.layerIndex);
}

/**
 * 收集数据
 */
AccidentDisposaltaskInfoDlg.collectData = function() {
    this
    .set('id')
    .set('accidentId')
    .set('time')
    .set('disposalTask')
    ;
}

/**
 * 提交添加
 */
AccidentDisposaltaskInfoDlg.addSubmit = function() {

    this.clearData();
    this.collectData();

    //提交信息
    var ajax = new $ax(Feng.ctxPath + "/accidentDisposaltask/add", function(data){
        Feng.success("添加成功!");
        window.parent.AccidentDisposaltask.table.refresh();
        AccidentDisposaltaskInfoDlg.close();
    },function(data){
        Feng.error("添加失败!" + data.responseJSON.message + "!");
    });
    ajax.set(this.accidentDisposaltaskInfoData);
    ajax.start();
}

/**
 * 提交修改
 */
AccidentDisposaltaskInfoDlg.editSubmit = function() {

    this.clearData();
    this.collectData();

    //提交信息
    var ajax = new $ax(Feng.ctxPath + "/accidentDisposaltask/update", function(data){
        Feng.success("修改成功!");
        window.parent.AccidentDisposaltask.table.refresh();
        AccidentDisposaltaskInfoDlg.close();
    },function(data){
        Feng.error("修改失败!" + data.responseJSON.message + "!");
    });
    ajax.set(this.accidentDisposaltaskInfoData);
    ajax.start();
}

$(function() {

});
