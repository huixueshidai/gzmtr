package com.stylefeng.guns.core.beetl;

import com.stylefeng.guns.common.constant.factory.ConstantFactory;
import com.stylefeng.guns.common.persistence.model.Dict;

import java.util.List;

public class DictExt {
    public List<Dict> getByCode(String code) {
        return ConstantFactory.me().getDictByCode(code);
    }
}
