package com.stylefeng.guns.common.persistence.model;

import java.io.Serializable;

import com.baomidou.mybatisplus.enums.IdType;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableName;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

/**
 * <p>
 * 公交枢纽站信息
 * </p>
 *
 * @author yinjc
 * @since 2018-03-21
 */
@TableName("gzmtr_public_bus_hub")
@Setter
@Getter
@ToString
public class PublicBusHub extends Model<PublicBusHub> {

    private static final long serialVersionUID = 1L;

	@TableId(value="id", type= IdType.AUTO)
	private Integer id;
    /**
     * 枢纽站名称
     */
	private String name;
    /**
     * 可用司机数量
     */
	@TableField("driver_num")
	private String driverNum;
    /**
     * 距轨道车站距离
     */
	private String distance;

	/**
	 * 最大载客量
	 * @return
	 */
	@TableField("max_num")
    private Integer maxNum;

	/**
	 * 满载率
	 * @return
	 */
	@TableField("load_factor")
	private Double  loadFactor;
	@TableField("bus_type")
	private String  busType;
	@TableField("bus_capacity")
	private Integer  busCapacity;


	@Override
	protected Serializable pkVal() {
		return this.id;
	}

}
