package com.stylefeng.guns.common.persistence.dao;

import com.stylefeng.guns.common.persistence.model.Indication;
import com.baomidou.mybatisplus.mapper.BaseMapper;

/**
 * <p>
  * 评价指标体系 Mapper 接口
 * </p>
 *
 * @author xuziyang
 * @since 2018-01-08
 */
public interface IndicationMapper extends BaseMapper<Indication> {

}