package com.stylefeng.guns.modular.yjya.service;

import com.baomidou.mybatisplus.service.IService;
import com.stylefeng.guns.common.persistence.model.AccidentHistory;
import net.sf.ehcache.search.aggregator.Count;

import java.util.Date;

/**
 * <p>
 * 事故历史信息 服务类
 * </p>
 *
 * @author yinjc
 * @since 2018-05-18
 */
public interface IAccidentHistoryService extends IService<AccidentHistory> {
	int selectAccidentCountByCategoryAndDate(String category, Date beginDate, Date endDate);

	int selectFactorCountByCategoryCauseAndDate(String category, String cause, Date beginDate, Date endDate);
}
