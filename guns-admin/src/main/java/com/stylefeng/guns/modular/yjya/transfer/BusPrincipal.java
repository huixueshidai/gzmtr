package com.stylefeng.guns.modular.yjya.transfer;

import lombok.*;

/**
 * Created by 60132 on 2018/4/13.
 */
@ToString
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class BusPrincipal {

    /**
     * 姓名
     */
    public String name ;
    /**
     * 车牌号
     */
    public String plateNum;
    /**
     *联系电话
     */
    public String telphone;
}
