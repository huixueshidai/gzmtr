package com.stylefeng.guns.modular.yjya.controller;

import com.baomidou.mybatisplus.plugins.Page;
import com.stylefeng.guns.common.constant.factory.PageFactory;
import com.stylefeng.guns.common.persistence.dao.AlarmMapper;
import com.stylefeng.guns.common.persistence.model.Alarm;
import com.stylefeng.guns.common.persistence.model.Counterplan;
import com.stylefeng.guns.common.persistence.model.Drivers;
import com.stylefeng.guns.core.base.controller.BaseController;
import com.stylefeng.guns.core.shiro.ShiroKit;
import com.stylefeng.guns.core.support.BeanKit;
import com.stylefeng.guns.modular.yjya.dao.PublicbusDao;
import com.stylefeng.guns.modular.yjya.service.IDriversService;
import com.stylefeng.guns.modular.yjya.warpper.CounterplanWarpper;
import com.stylefeng.guns.modular.yjya.warpper.PublicbusWarpper;
import org.apache.velocity.util.ArrayListWrapper;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.beans.factory.annotation.Autowired;
import com.stylefeng.guns.core.log.LogObjectHolder;
import org.springframework.web.bind.annotation.RequestParam;
import com.stylefeng.guns.common.persistence.model.Publicbus;
import com.stylefeng.guns.modular.yjya.service.IPublicbusService;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * 控制器
 *
 * @author wangshuaikang
 * @Date 2018-09-03 17:28:30
 */
@Controller
@RequestMapping("/publicbus")
public class PublicbusController extends BaseController {

    private String PREFIX = "/yjya/publicbus/";

    @Autowired
    private IPublicbusService publicbusService;

    @Autowired
    private AlarmMapper alarmMapper;

    @Autowired
    private PublicbusDao publicbusDao;

    @Autowired
    private IDriversService driversService;

    /**
     * 跳转到首页
     */
    @RequestMapping("")
    public String index() {
        return PREFIX + "publicbus.html";
    }

    /**
     * 跳转到添加
     */
    @RequestMapping("/publicbus_add")
    public String publicbusAdd() {
        return PREFIX + "publicbus_add.html";
    }

    /**
     * 跳转到修改
     */
    @RequestMapping("/publicbus_update/{publicbusId}")
    public String publicbusUpdate(@PathVariable Integer publicbusId, Model model) {
        Publicbus publicbus = publicbusService.selectById(publicbusId);
        model.addAttribute("item",publicbus);
        LogObjectHolder.me().set(publicbus);
        return PREFIX + "publicbus_edit.html";
    }

    /**
     * 获取列表
     */
    @RequestMapping(value = "/list")
    @ResponseBody
    public Object list() {

        Page<Publicbus> page = new PageFactory<Publicbus>().defaultPage();
        List<Map<String, Object>> result = publicbusDao.getPublicBus(page);
        page.setRecords((List<Publicbus>) new PublicbusWarpper(result).warp());
        return super.packForBT(page);

    }


    @RequestMapping("/publicreport/{id}")
    public Object add(@PathVariable Integer id,Model model) {
        Publicbus publicbus = publicbusService.selectById(id);
        Map<String, Object> templateMap = BeanKit.beanToMap(publicbus);
        model.addAttribute("item", new PublicbusWarpper(templateMap).warp());
        List<Drivers> driversList = new ArrayList<>();
        String [] driversIds = publicbus.getDriverId().split(";");
        for(int i = 0;i<driversIds.length;i++){
            Drivers drivers = driversService.selectById(driversIds[i]);
            driversList.add(drivers);
        }
        model.addAttribute("driversList",driversList);
        return PREFIX + "publicbus_report.html";
    }

    /**
     * 新增
     */
    @RequestMapping(value = "/add")
    @ResponseBody
    public Object add(Publicbus publicbus) {

        return super.SUCCESS_TIP;
    }

    /**
     * 删除
     */
    @RequestMapping(value = "/delete")
    @ResponseBody
    public Object delete(@RequestParam(value = "ids[]") List<Integer> ids) {
        publicbusService.deleteBatchIds(ids);
        return SUCCESS_TIP;
    }

    /**
     * 修改
     */
    @RequestMapping(value = "/update")
    @ResponseBody
    public Object update(Publicbus publicbus) {
        publicbusService.updateById(publicbus);
        return super.SUCCESS_TIP;
    }

    /**
     * 详情
     */
    @RequestMapping(value = "/detail/{publicbusId}")
    @ResponseBody
    public Object detail(@PathVariable("publicbusId") Integer publicbusId) {
        return publicbusService.selectById(publicbusId);
    }
}
