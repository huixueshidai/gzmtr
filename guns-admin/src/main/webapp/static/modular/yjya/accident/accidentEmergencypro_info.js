/**
 * 初始化详情对话框
 */
var AccidentEmergencyproInfoDlg = {
    accidentEmergencypropertyInfoData : {}
};

/**
 * 清除数据
 */
AccidentEmergencyproInfoDlg.clearData = function() {
    this.accidentEmergencypropertyInfoData = {};
}

/**
 * 设置对话框中的数据
 *
 * @param key 数据的名称
 * @param val 数据的具体值
 */
AccidentEmergencyproInfoDlg.set = function(key, val) {
    this.accidentEmergencypropertyInfoData[key] = (typeof val == "undefined") ? $("#" + key).val() : val;
    return this;
}

/**
 * 设置对话框中的数据
 *
 * @param key 数据的名称
 * @param val 数据的具体值
 */
AccidentEmergencyproInfoDlg.get = function(key) {
    return $("#" + key).val();
}

/**
 * 关闭此对话框
 */
AccidentEmergencyproInfoDlg.close = function() {
    parent.layer.close(window.parent.AccidentInfoDlg1.layerIndex);
}

/**
 * 收集数据
 */
AccidentEmergencyproInfoDlg.collectData = function() {
    this
    .set('id')
    .set('ridership')
    .set('trafficCapacity')
    .set('fault')
    .set('gasShield')
    .set('tunnelLength')
    .set('status')
    .set('distanceOn')
    .set('distanceNext')
    .set('trappedNum')
    .set('trappedNumPower')
    .set('physicalState')
    .set('toxicity')
    .set('bums')
    .set('causticity')
    .set('volatileness')
    .set('temperature')
    .set('smokescope')
    .set('collapse')
    .set('loseEfficacy')
    .set('reoad')
    .set('distanceOneOneNine')
    .set('distanceOneOneZero')
    .set('distanceOneTwoZero')
        .set('fireControl')
    ;
}

/**
 * 提交添加
 */
AccidentEmergencyproInfoDlg.addSubmit = function() {

    this.clearData();
    this.collectData();

    //提交信息
    var ajax = new $ax(Feng.ctxPath + "/accidentEmergencyproperty/add", function(data){
        Feng.success("突发事件属性添加成功!");
        window.parent.AccidentInfoDlg1.emergencyPropertyAssign(data.id);
        AccidentEmergencyproInfoDlg.close();
    },function(data){
        Feng.error("添加失败!" + data.responseJSON.message + "!");
    });
    ajax.set(this.accidentEmergencypropertyInfoData);
    ajax.start();
}

/**
 * 提交修改
 */
AccidentEmergencyproInfoDlg.editSubmit = function() {

    this.clearData();
    this.collectData();

    //提交信息
    var ajax = new $ax(Feng.ctxPath + "/accidentEmergencyproperty/update", function(data){
        Feng.success("突发时间属性修改成功!");
        AccidentEmergencyproInfoDlg.close();
    },function(data){
        Feng.error("修改失败!" + data.responseJSON.message + "!");
    });
    ajax.set(this.accidentEmergencypropertyInfoData);
    ajax.start();
}

/**
 * 点击文本框提示消失
 */
AccidentEmergencyproInfoDlg.onfocus = function(){
    if(this.value=='请输入内容'){
        this.value=";";
    }

}

$(function() {

});
