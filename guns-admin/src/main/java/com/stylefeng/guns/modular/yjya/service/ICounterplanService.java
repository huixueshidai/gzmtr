package com.stylefeng.guns.modular.yjya.service;

import com.stylefeng.guns.common.persistence.model.Counterplan;
import com.baomidou.mybatisplus.service.IService;

/**
 * <p>
 * 预案 服务类
 * </p>
 *
 * @author xuziyang
 * @since 2018-01-18
 */
public interface ICounterplanService extends IService<Counterplan> {

    void insert(Counterplan counterplan, String[] accidentIds);

    void updateById(Counterplan counterplan, String[] accidentIds);
}