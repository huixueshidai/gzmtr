package com.stylefeng.guns.modular.assessment.calculate.station.environment;

import com.stylefeng.guns.common.persistence.model.Assessment;
import com.stylefeng.guns.core.util.DoubleUtil;
import com.stylefeng.guns.modular.assessment.calculate.ThirdIndicationCalculator;
import com.stylefeng.guns.modular.assessment.calculate.aware.StationThirdIndicationAware;
import org.apache.commons.collections.CollectionUtils;
import org.springframework.stereotype.Component;

import java.util.Date;
import java.util.List;

/**
 * 车站湿度
 */
@Component
public class StationHumidityCal extends ThirdIndicationCalculator implements StationThirdIndicationAware {

    @Override
    protected Integer getIndicationId() {
        return 35;
    }

    @Override
    public Assessment calculate(Integer stationId, Date beginDate, Date endDate) {
        List<Double> facts = getMulDoubleValue(stationId, 70, beginDate,endDate);
        Double standard = getDoubleValue(stationId, 71, beginDate,endDate);
        Double allowableDiff = getDoubleValue(stationId, 72, beginDate,endDate);

        if (CollectionUtils.isEmpty(facts)) {
            return genResult(1d, stationId, beginDate,endDate);
        }

        Double fact = facts.get(0);
        Double result = DoubleUtil.sub(1, DoubleUtil.safeDiv(DoubleUtil.absSub(fact, standard), allowableDiff));

        return genResult(result, stationId, beginDate,endDate);
    }
}
