package com.stylefeng.guns.modular.assessment.calculate.station;

import com.stylefeng.guns.core.util.SpringContextHolder;
import com.stylefeng.guns.modular.assessment.calculate.FirstIndicationCalculator;
import com.stylefeng.guns.modular.assessment.calculate.SecondIndicationCalculator;
import com.stylefeng.guns.modular.assessment.calculate.station.accident.StationAccidentCal;
import com.stylefeng.guns.modular.assessment.calculate.station.employee.StationEmployeeCal;
import com.stylefeng.guns.modular.assessment.calculate.station.environment.StationEnvironmentCal;
import com.stylefeng.guns.modular.assessment.calculate.station.facility.StationFacilityCal;
import com.stylefeng.guns.modular.assessment.calculate.station.manage.StationManageCal;
import com.stylefeng.guns.modular.assessment.calculate.station.passenger.StationPassengerCal;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import java.util.Arrays;
import java.util.List;

@Component
@Scope(ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public class StationCalculate extends FirstIndicationCalculator {

    private List<SecondIndicationCalculator> secondIndicationCalculates;

    public StationCalculate(){
        secondIndicationCalculates = Arrays.asList(
                SpringContextHolder.getBean(StationAccidentCal.class),
                SpringContextHolder.getBean(StationEmployeeCal.class),
                SpringContextHolder.getBean(StationEnvironmentCal.class),
                SpringContextHolder.getBean(StationFacilityCal.class),
                SpringContextHolder.getBean(StationManageCal.class),
                SpringContextHolder.getBean(StationPassengerCal.class)
        );
    }
    @Override
    public List<SecondIndicationCalculator> getSecondIndicationCalculateList() {
        return secondIndicationCalculates;
    }

    @Override
    protected Integer getIndicationId() {
        return 1;
    }
}
