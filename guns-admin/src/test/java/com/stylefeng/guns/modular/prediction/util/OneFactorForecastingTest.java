package com.stylefeng.guns.modular.prediction.util;

import com.stylefeng.guns.core.util.DoubleUtil;
import org.junit.Assert;
import org.junit.Test;
import org.junit.Before;
import org.junit.After;

import java.util.Arrays;
import java.util.List;

/**
 * OneFactorForecasting Tester.
 *
 * @author <Authors name>
 * @since <pre>八月 28, 2018</pre>
 * @version 1.0
 */
public class OneFactorForecastingTest {

	/**
	 *
	 * Method: forecast(List<Integer> actualList)
	 *
	 */
	@Test
	public void testForecast() throws Exception {
		List<Integer> actualList = Arrays.asList(1, 2, 3, 4);
		List<Double> forecastingList = OneFactorForecasting.forecast(actualList);
		Assert.assertTrue(forecastingList.get(0) == 1.0);
		Assert.assertTrue(DoubleUtil.sub(5.533958505032871, forecastingList.get(4)) < 0.0001);
	}


	@Test
	public void testForecast2() throws Exception {
		List<Integer> actualList = Arrays.asList(76, 65, 46);
		List<Double> forecastList = OneFactorForecasting.forecast(actualList);
		System.out.println(forecastList);
	}



} 
