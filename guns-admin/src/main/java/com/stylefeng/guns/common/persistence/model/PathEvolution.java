package com.stylefeng.guns.common.persistence.model;

import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;
import com.baomidou.mybatisplus.enums.IdType;
import lombok.Data;

import java.io.Serializable;

/**
 * <p>
 * 事故情景路径演练
 * </p>
 *
 * @author xhq
 * @since 2018-09-04
 */
@TableName("gzmtr_path_evolution")
@Data
public class PathEvolution extends Model<PathEvolution> {

    private static final long serialVersionUID = 1L;

	@TableId(value="id", type= IdType.AUTO)
	private Integer id;
    /**
     * 场地
     */
	private String field;
    /**
     * 救援人员
     */
	private String rescuers;
    /**
     * 被困人员
     */
	private String trapped;
    /**
     * 材料
     */
	private String material;
    /**
     * 火源情景状�??
     */
	private String FireSourceState;
    /**
     * 被困人员情景状�??
     */
	private String dischargeRate;


	@Override
	protected Serializable pkVal() {
		return this.id;
	}


}
