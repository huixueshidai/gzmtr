package com.stylefeng.guns.common.persistence.model;

import java.io.Serializable;

import com.baomidou.mybatisplus.enums.IdType;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableName;
import java.io.Serializable;

/**
 * <p>
 * 
 * </p>
 *
 * @author wsk
 * @since 2018-09-25
 */
@TableName("gzmtr_passenger_file")
public class PassengerFile extends Model<PassengerFile> {

    private static final long serialVersionUID = 1L;

	@TableId(value="id", type= IdType.AUTO)
	private Integer id;
    /**
     * 客流所属（平时、周六日、节假日）
     */
	@TableField("date_status")
	private Integer dateStatus;
    /**
     * 更新客流的文件
     */
	@TableField("passenger_file")
	private String passengerFile;


	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getDateStatus() {
		return dateStatus;
	}

	public void setDateStatus(Integer dateStatus) {
		this.dateStatus = dateStatus;
	}

	public String getPassengerFile() {
		return passengerFile;
	}

	public void setPassengerFile(String passengerFile) {
		this.passengerFile = passengerFile;
	}

	@Override
	protected Serializable pkVal() {
		return this.id;
	}

	@Override
	public String toString() {
		return "PassengerFile{" +
			"id=" + id +
			", dateStatus=" + dateStatus +
			", passengerFile=" + passengerFile +
			"}";
	}
}
