/**
 * Created by 60132 on 2018/7/3.
 */
var PublicBusEmulate ={
    opts:null,
    mySubway:null,
    radiusInfo:260,
    info1:[],
    markers:[],
    map:null,
    endTime:null

};

PublicBusEmulate.passeneger = function(map){

    var markers=new Array();

    map.remove(PublicBusEmulate.info1);
    if(PublicBusEmulate.radiusInfo>0){
        PublicBusEmulate.radiusInfo--;
    }
    else{
        PublicBusEmulate.radiusInfo = 0;
    }
    var stationNameList = $("#stationNameList").val().replace(/\[|]/g,'').split(',');
    var lnglats1 = [
        ["113.231855","23.065446","#7B68EE","西朗"],//西朗
        ["113.232456","23.07887","#FFFF00","坑口"], //坑口
        ["113.233915","23.087003","#7B68EE","花地湾"],//花地湾
        ["113.235546","23.09853","#FF7F00","芳村"],//芳村
        ["113.239795","23.11057","#7B68EE","黄沙"],//黄沙
        ["113.242241","23.118345","#FF0000","长寿路"],//长寿路
        ["113.246361","23.125844","#FF7F00","陈家祠"],//陈家祠
        ["113.255793","23.125173","#FF0000","西门口"],//西门口
        ["113.264213","23.125528","#FF7F00","公园前"],//公园前
        ["113.275586","23.126634","#FFFF00","农讲所"],//农讲所
        ["113.285628","23.126712","#FF0000","烈士陵园"],//烈士陵园
        ["113.295348","23.124009","#FFFF00","东山口"],//东山口
        ["113.308867","23.128133","#7B68EE","杨箕"],//杨箕
        ["113.321484","23.131172","#FFFF00","体育西路"],//体育西路
        ["113.32835","23.13499","#FF0000","体育中心"],//体育中心
        ["113.325395","23.150304","#FF7F00","广州东站"],//广州东站
    ];
    PublicBusEmulate.info1=[];
    var radius = PublicBusEmulate.radiusInfo;
    var stationSort = 0;
    for(var i=0;i<lnglats1.length;i++){
        if(PublicBusEmulate.in_array(lnglats1[i][3],stationNameList)){
            // 构造衰减的矢量圆形
            var circle = new AMap.Circle({
                center: new AMap.LngLat(lnglats1[i][0],lnglats1[i][1]), // 圆心位置
                radius: radius,  //半径
                strokeColor: lnglats1[i][2],  //线颜色
                strokeOpacity: 1,  //线透明度
                strokeWeight: 1,  //线粗细度
                fillColor: lnglats1[i][2],  //填充颜色  橙色#FF7F00  黄色#FFFF00  蓝色#7B68EE  红色#FF0000
                fillOpacity: 0.6 //填充透明度
            });
            PublicBusEmulate.info1.push(circle);
            //设置客流点
            var passengerlistZ = window.parent.PublicBusEmulateInfo.emuPassNumZ;
            var passengerlistS = window.parent.PublicBusEmulateInfo.emuPassNumZS;
            if(stationSort==0){
                PublicBusEmulate.setCirclePass(circle,passengerlistZ[stationSort],passengerlistS);
            }
            else{
                PublicBusEmulate.setCirclePass(circle,passengerlistZ[stationSort],0);
            }

            stationSort++;

        }
    }

    map.add(PublicBusEmulate.info1);
    map.add(PublicBusEmulate.markers);

}

PublicBusEmulate.in_array = function(element,arr){
    for(var i=0;i<arr.length;i++){
        if(element==arr[i].trim()){
            return true;
        }
    }
}

PublicBusEmulate.cancleEnd = function(){
        var time = new Date();
       // 程序计时的月从0开始取值后+1
       var m = time.getMonth() + 1;
       var t = time.getFullYear() + "-" + m + "-"
        + time.getDate() + " " + time.getHours() + ":"
        + time.getMinutes() + ":" + time.getSeconds();
        PublicBusEmulate.endTime = t;
        document.getElementById("endTime").innerHTML = PublicBusEmulate.endTime.toString();
        PublicBusEmulate.map.remove(PublicBusEmulate.info1);
        PublicBusEmulate.radiusInfo = 0;

}


PublicBusEmulate.startUp = function(){
    var stationId = $("#stationId").val();
    var busNum = $("#busNum").val();
    var stationSatrtTime = $("#stationSatrtTime").val();
    var index = layer.open({
        type: 2,
        title: '公交应急联动处置报告',
        area: ['900px', '600px'], //宽高
        fix: false, //不固定
        maxmin: true,
        content: Feng.ctxPath + '/publicBusHub/dispose_report/'+stationId+"/"+busNum+"/"+stationSatrtTime+"/"+PublicBusEmulate.endTime
    });
    this.layerIndex = index;
}

/**
 * 设置客流人数点
 * @param passengeNumZ滞留客流
 * @param passengerNumS疏运客流
 */
PublicBusEmulate.setCirclePass = function(circle,passengeNumZ,passengerNumS){
    var mapBounds = circle.getBounds();
    var sw = mapBounds.getSouthWest();
    var ne = mapBounds.getNorthEast();
    var lngSpan = Math.abs(sw.lng - ne.lng);
    var latSpan = Math.abs(ne.lat - sw.lat);
    //滞留客流
    var numz = 0;
    while(numz<passengeNumZ) {
        var markerPosition = new AMap.LngLat(sw.lng + lngSpan * (Math.random() * 1), ne.lat - latSpan * (Math.random() * 1));
        var circle1 = new AMap.CircleMarker({
            center: markerPosition, // 圆心位置
            radius: 0.3,  //半径
            strokeColor: "#2E2E2E",  //线颜色
            fillColor: "#2E2E2E",  //填充颜色  橙色#FF7F00  黄色#FFFF00  蓝色#7B68EE  红色#FF0000
            fillOpacity: 1 //填充透明度
        });
        if (circle.contains(markerPosition)) {
            numz++;
            PublicBusEmulate.markers.push(circle1);
        }
    }
    //疏运客流
    var nums = 0;
    while (nums < passengerNumS) {
        var markerPosition = new AMap.LngLat(sw.lng + lngSpan * (Math.random() * 1), ne.lat - latSpan * (Math.random() * 1));
        var circle1 = new AMap.CircleMarker({
            center: markerPosition, // 圆心位置
            radius: 0.3,  //半径
            strokeColor: "#76EE00",  //线颜色
            fillColor: "#76EE00",  //填充颜色  橙色#FF7F00  黄色#FFFF00  蓝色#7B68EE  红色#FF0000
            fillOpacity: 1 //填充透明度
        });
        if (circle.contains(markerPosition)) {
            nums++;
            PublicBusEmulate.markers.push(circle1);
        }
    }
}



$(function(){

    PublicBusEmulate.map = new AMap.Map('container', {
        resizeEnable: true,
        zoom:15,
        center: [113.249923,23.122884]
    });
    var lnglats = [
        [113.233093,23.064922],
        [113.252525,23.107517],
        [113.266403,23.127357],
        [113.295212,23.124874],
        [113.32383,23.147726]
    ];
    var info = [];
    info.push("<div style=\"padding:0px 0px 0px 4px;\"><b>公交枢纽站信息1</b><br><br>枢纽站信息："+$("#publicBusHub1_name").val()+"<br><br>公交车类型："+$("#publicBusHub1_busType").val()+"<br><br>车辆定员："+$("#publicBusHub1_busCapacity").val()+"<br><br>车辆数量："+$("#publicBusHub1_driverNum").val()+"辆<br><br>可驾驶人员："+$("#publicBusHub1_driverNum").val()+"人<br><br>距轨道车站距离："+$("#publicBusHub1_distance").val()+"</div>");
    info.push("<div style=\"padding:0px 0px 0px 4px;\"><b>公交枢纽站信息2</b><br><br>枢纽站信息："+$("#publicBusHub2_name").val()+"<br><br>公交车类型："+$("#publicBusHub2_busType").val()+"<br><br>车辆定员："+$("#publicBusHub2_busCapacity").val()+"<br><br>车辆数量："+$("#publicBusHub2_driverNum").val()+"辆<br><br>可驾驶人员："+$("#publicBusHub2_ddriverNum").val()+"人<br><br>距轨道车站距离："+$("#publicBusHub1_distance").val()+"</div>");
    info.push("<div style=\"padding:0px 0px 0px 4px;\"><b>公交枢纽站信息3</b><br><br>枢纽站信息："+$("#publicBusHub3_name").val()+"<br><br>公交车类型："+$("#publicBusHub3_busType").val()+"<br><br>车辆定员："+$("#publicBusHub3_busCapacity").val()+"<br><br>车辆数量："+$("#publicBusHub3_driverNum").val()+"辆<br><br>可驾驶人员："+$("#publicBusHub3_driverNum").val()+"人<br><br>距轨道车站距离："+$("#publicBusHub1_distance").val()+"</div>");
    info.push("<div style=\"padding:0px 0px 0px 4px;\"><b>公交枢纽站信息4</b><br><br>枢纽站信息："+$("#publicBusHub4_name").val()+"<br><br>公交车类型："+$("#publicBusHub4_busType").val()+"<br><br>车辆定员："+$("#publicBusHub4_busCapacity").val()+"<br><br>车辆数量："+$("#publicBusHub4_driverNum").val()+"辆<br><br>可驾驶人员："+$("#publicBusHub4_driverNum").val()+"人<br><br>距轨道车站距离："+$("#publicBusHub1_distance").val()+"</div>");
    info.push("<div style=\"padding:0px 0px 0px 4px;\"><b>公交枢纽站信息5</b><br><br>枢纽站信息："+$("#publicBusHub5_name").val()+"<br><br>公交车类型："+$("#publicBusHub5_busType").val()+"<br><br>车辆定员："+$("#publicBusHub5_busCapacity").val()+"<br><br>车辆数量："+$("#publicBusHub4_driverNum").val()+"辆<br><br>可驾驶人员："+$("#publicBusHub4_driverNum").val()+"人<br><br>距轨道车站距离："+$("#publicBusHub1_distance").val()+"</div>");
    var infoWindow = new AMap.InfoWindow({offset: new AMap.Pixel(0, -30)});
    for (var i = 0; i < lnglats.length; i++) {
        var marker = new AMap.Marker({
            position: lnglats[i],
            map: PublicBusEmulate.map
    });
        marker.content = info[i];
        marker.on('click', markerClick);
        // marker.emit('click', {target: marker});
    }
    function markerClick(e) {
        infoWindow.setContent(e.target.content);
        infoWindow.open(PublicBusEmulate.map, e.target.getPosition());
    }

    PublicBusEmulate.passeneger(PublicBusEmulate.map);
    // setInterval(PublicBusEmulate.passeneger,5000,PublicBusEmulate.map);
    //根据标注点自动缩放地图
    // map.setFitView();


    /**
     * 设置实时时间代码
     */
    function mytime(){
        var time = new Date();
        // 程序计时的月从0开始取值后+1
        var m = time.getMonth() + 1;
        var t = time.getFullYear() + "-" + m + "-"
            + time.getDate() + " " + time.getHours() + ":"
            + time.getMinutes() + ":" + time.getSeconds();
        document.getElementById("Time").innerHTML = t;
    }
    setInterval(function() {mytime()},1000);


});
