package com.stylefeng.guns.modular.yjya.controller;

import com.baomidou.mybatisplus.plugins.Page;
import com.stylefeng.guns.common.constant.factory.PageFactory;
import com.stylefeng.guns.common.persistence.dao.AccidentHistoryMapper;
import com.stylefeng.guns.common.persistence.model.AccidentHistory;
import com.stylefeng.guns.core.base.controller.BaseController;
import com.stylefeng.guns.core.support.BeanKit;
import com.stylefeng.guns.core.support.DateTimeKit;
import com.stylefeng.guns.modular.system.transfer.ReqEditManager;
import com.stylefeng.guns.modular.yjya.service.IAccidentHistoryService;
import com.stylefeng.guns.modular.yjya.warpper.AccidentHistoryInformationWrapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;
import java.util.Map;

/**
 * 事故（事件）历史信息控制器
 *
 * @author yinjc
 * @Date 2018-05-18 15:53:29
 */
@Controller
@RequestMapping("/accidentHistory")
public class AccidentHistoryController extends BaseController {

	private String PREFIX = "/yjya/accidentHistory/";

	@Autowired
	private IAccidentHistoryService accidentHistoryService;

	@Autowired
	private AccidentHistoryMapper accidentHistoryMapper;

	/**
	 * 跳转到事故（事件）历史信息首页
	 */
	@RequestMapping("")
	public String index() {
		return PREFIX + "accidentHistory.html";
	}

	/**
	 * 跳转到添加事故（事件）历史信息
	 */
	@RequestMapping("/accidentHistory_add")
	public String accidentHistoryAdd() {
		return PREFIX + "accidentHistory_add.html";
	}

	/**
	 * 跳转到修改事故（事件）历史信息
	 */
	@RequestMapping("/accidentHistory_update/{accidentHistoryId}")
	public String accidentHistoryUpdate(@PathVariable Integer accidentHistoryId, Model model) {
		AccidentHistory accidentHistory = accidentHistoryService.selectById(accidentHistoryId);
		Map<String, Object> mapData = BeanKit.beanToMap(accidentHistory);
		mapData.put("date", DateTimeKit.formatDateTime(accidentHistory.getDate()));
		new AccidentHistoryInformationWrapper(mapData).warp();
		model.addAttribute("item", mapData);
		return PREFIX + "accidentHistory_edit.html";
	}

	/**
	 * 事故同期提醒
	 * @param model
	 * @return
	 */
	@RequestMapping("/accident_forecast")
	public String accidentForecast(Model model) {
		List<Map<String, Object>> toDayList = accidentHistoryMapper.selectAccidentHistoryInfoOfCurDayWithoutCurYear();
		List<Map<String, Object>> tomorrowList = accidentHistoryMapper
				.selectAccidentHistoryInfoOfTomorrowWithoutCurYear();
		List<Map<String, Object>> theMonthList = accidentHistoryMapper
				.selectAccidentHistoryInfoOfCurMonthWithoutCurYear();
		new AccidentHistoryInformationWrapper(toDayList).warp();
		new AccidentHistoryInformationWrapper(tomorrowList).warp();
		new AccidentHistoryInformationWrapper(theMonthList).warp();
		model.addAttribute("toDayList", toDayList);
		model.addAttribute("tomorrowList", tomorrowList);
		model.addAttribute("theMonthList", theMonthList);
		return PREFIX + "accident_forecast.html";
	}

	/**
	 * 获取事故（事件）历史信息列表
	 */
	@RequestMapping(value = "/list")
	@ResponseBody
	public Object list(@RequestParam(value = "name", required = false) String name,
			@RequestParam(value = "category",required = false)String category,
			@RequestParam(value = "rankCode",required = false)String rankCode,
			@RequestParam(value = "beginTime",required = false) String beginTime,
			@RequestParam(value = "endTime",required = false) String endTime) {
		Page<AccidentHistory> page = new PageFactory<AccidentHistory>().defaultPage();
		List<Map<String, Object>> mapList = accidentHistoryMapper.selectPage2(page, name,category,rankCode,beginTime,endTime);
		page.setRecords((List<AccidentHistory>) new AccidentHistoryInformationWrapper(mapList).warp());
		return super.packForBT(page);
	}

	/**
	 * 新增事故（事件）历史信息
	 */
	@RequestMapping(value = "/add")
	@ResponseBody
	public Object add(AccidentHistory accidentHistory) {
		accidentHistoryService.insert(accidentHistory);
		return super.SUCCESS_TIP;
	}

	/**
	 * 删除事故（事件）历史信息
	 */
	@RequestMapping(value = "/delete")
	@ResponseBody
	public Object delete(@RequestParam Integer accidentHistoryId) {
		accidentHistoryService.deleteById(accidentHistoryId);
		return SUCCESS_TIP;
	}

	/**
	 * 修改事故（事件）历史信息
	 */
	@RequestMapping(value = "/update")
	@ResponseBody
	public Object update(AccidentHistory accidentHistory) {
		accidentHistoryService.updateById(accidentHistory);
		return super.SUCCESS_TIP;
	}

	/**
	 * 事故（事件）历史信息详情
	 */
	@RequestMapping(value = "/detail/{accidentHistoryId}")
	@ResponseBody
	public Object detail(@PathVariable("accidentHistoryId") Integer accidentHistoryId) {
		return accidentHistoryService.selectById(accidentHistoryId);
	}
}
