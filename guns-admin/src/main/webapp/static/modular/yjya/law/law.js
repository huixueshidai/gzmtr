/**
 * 法律法规管理初始化
 */
var Law = {
    id: "LawTable",	//表格id
    seItem: null,		//选中的条目
    table: null,
    layerIndex: -1
};

/**
 * 初始化表格的列
 */
Law.initColumn = function () {
    return [
        {field: 'selectItem', radio: true},
            {title: '编号', field: 'id', visible: true, align: 'center', valign: 'middle'},
            {title: '名称', field: 'name', visible: true, align: 'center', valign: 'middle'}
    ];
};

/**
 * 检查是否选中
 */
Law.check = function () {
    var selected = $('#' + this.id).bootstrapTable('getSelections');
    if(selected.length == 0){
        Feng.info("请先选中表格中的某一记录！");
        return false;
    }else{
        Law.seItem = selected[0];
        return true;
    }
};

/**
 * 点击添加法律法规
 */
Law.openAddLaw = function () {
    var index = layer.open({
        type: 2,
        title: '添加法律法规',
        area: ['800px', '420px'], //宽高
        fix: false, //不固定
        maxmin: true,
        content: Feng.ctxPath + '/law/law_add'
    });
    this.layerIndex = index;
};

/**
 * 打开查看法律法规详情
 */
Law.openLawDetail = function () {
    if (this.check()) {
        var index = layer.open({
            type: 2,
            title: '法律法规详情',
            area: ['800px', '420px'], //宽高
            fix: false, //不固定
            maxmin: true,
            content: Feng.ctxPath + '/law/law_update/' + Law.seItem.id
        });
        this.layerIndex = index;
    }
};

/**
 * 删除法律法规
 */
Law.delete = function () {
    if (this.check()) {
        var ajax = new $ax(Feng.ctxPath + "/law/delete", function (data) {
            Feng.success("删除成功!");
            Law.table.refresh();
        }, function (data) {
            Feng.error("删除失败!" + data.responseJSON.message + "!");
        });
        ajax.set("lawId",this.seItem.id);
        ajax.start();
    }
};
/**
 * 重置
 */
Law.resetSearch = function(){
    $("#condition").val("");
}
/**
 * 查询法律法规列表
 */
Law.search = function () {
    var queryData = {};
    queryData['condition'] = $("#condition").val();
    Law.table.refresh({query: queryData});
};

$(function () {
    var defaultColunms = Law.initColumn();
    var table = new BSTable(Law.id, "/law/list", defaultColunms);
    table.setPaginationType("client");
    Law.table = table.init();
});
