package com.stylefeng.guns.modular.prediction.controller;

import com.stylefeng.guns.common.persistence.dao.PredictionDataMapper;
import com.stylefeng.guns.common.persistence.model.PredictionData;
import com.stylefeng.guns.core.base.controller.BaseController;
import com.stylefeng.guns.core.log.LogObjectHolder;
import com.stylefeng.guns.modular.data.warpper.ResourceDataWrapper;
import com.stylefeng.guns.modular.prediction.service.IPredictionDataService;
import com.stylefeng.guns.modular.prediction.warpper.PredictionDataWarpper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * 风险预测数据管理控制器
 *
 * @author xuziyang
 * @Date 2018-05-19 18:58:44
 */
@Controller
@RequestMapping("/predictionData")
public class PredictionDataController extends BaseController {

    private String PREFIX = "/prediction/predictionData/";

    @Autowired
    private IPredictionDataService predictionDataService;

    @Autowired
    private PredictionDataMapper predictionDataMapper;

    /**
     * 跳转到风险预测数据管理首页
     */
    @RequestMapping("")
    public String index() {
        return PREFIX + "predictionData.html";
    }

    /**
     * 跳转到添加风险预测数据管理
     */
    @RequestMapping("/predictionData_add")
    public String predictionDataAdd() {
        return PREFIX + "predictionData_add.html";
    }

    /**
     * 跳转到修改风险预测数据管理
     */
    @RequestMapping("/predictionData_update/{predictionDataId}")
    public String predictionDataUpdate(@PathVariable Integer predictionDataId, Model model) {
        PredictionData predictionData = predictionDataService.selectById(predictionDataId);
        model.addAttribute("item",predictionData);
        LogObjectHolder.me().set(predictionData);
        return PREFIX + "predictionData_edit.html";
    }

    /**
     * 获取风险预测数据管理列表
     */
    @RequestMapping(value = "/list")
    @ResponseBody
    public Object list() {
        List<Map<String,Object>> mapData = predictionDataMapper.selectPredictionData();
        return new PredictionDataWarpper(mapData).warp();
    }

    /**
     * 新增风险预测数据管理
     */
    @RequestMapping(value = "/add")
    @ResponseBody
    public Object add(PredictionData predictionData) {
        predictionData.setCreateTime(new Date());
        predictionDataService.insert(predictionData);
        return super.SUCCESS_TIP;
    }

    /**
     * 删除风险预测数据管理
     */
    @RequestMapping(value = "/delete")
    @ResponseBody
    public Object delete(@RequestParam Integer predictionDataId) {
        predictionDataService.deleteById(predictionDataId);
        return SUCCESS_TIP;
    }

    /**
     * 修改风险预测数据管理
     */
    @RequestMapping(value = "/update")
    @ResponseBody
    public Object update(PredictionData predictionData) {
        predictionDataService.updateById(predictionData);
        return super.SUCCESS_TIP;
    }

    /**
     * 风险预测数据管理详情
     */
    @RequestMapping(value = "/detail/{predictionDataId}")
    @ResponseBody
    public Object detail(@PathVariable("predictionDataId") Integer predictionDataId) {
        return predictionDataService.selectById(predictionDataId);
    }
}
