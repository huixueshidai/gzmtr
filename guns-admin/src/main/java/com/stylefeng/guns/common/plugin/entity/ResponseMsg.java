package com.stylefeng.guns.common.plugin.entity;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class ResponseMsg {
	private final int code;
	private final String msg;
}
