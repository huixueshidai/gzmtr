package com.stylefeng.guns.common.constant.factory;

import com.stylefeng.guns.common.persistence.model.Dict;
import com.stylefeng.guns.common.persistence.model.ResourceDataType;

import java.util.List;

/**
 * 常量生产工厂的接口
 *
 * @author fengshuonan
 * @date 2017-06-14 21:12
 */
public interface IConstantFactory {

    /**
     * 根据用户id获取用户名称
     *
     * @author stylefeng
     * @Date 2017/5/9 23:41
     */
    String getUserNameById(Integer userId);



    /**
     * 获取评估指标的名称
     * @param indicationId
     * @return
     */
    String getIndicationNameById(Integer indicationId);

    /**
     * 根据用户id获取用户账号
     *
     * @author stylefeng
     * @date 2017年5月16日21:55:371
     */
    String getUserAccountById(Integer userId);

    /**
     * 通过角色ids获取角色名称
     */
    String getRoleName(String roleIds);

    /**
     * 通过角色id获取角色名称
     */
    String getSingleRoleName(Integer roleId);

    /**
     * 通过角色id获取角色英文名称
     */
    String getSingleRoleTip(Integer roleId);

    /**
     * 获取部门名称
     */
    String getDeptName(Integer deptId);

    /**
     * 获取菜单的名称们(多个)
     */
    String getMenuNames(String menuIds);

    /**
     * 获取菜单名称
     */
    String getMenuName(Long menuId);

    /**
     * 获取菜单名称通过编号
     */
    String getMenuNameByCode(String code);

    /**
     * 获取字典名称
     */
    String getDictName(Integer dictId);

    /**
     * 获取通知标题
     */
    String getNoticeTitle(Integer dictId);

    /**
     * 获取性别名称
     */
    String getSexName(String sex);

    /**
     * 获取用户登录状态
     */
    String getStatusName(Integer status);

    /**
     * 获取菜单状态
     */
    String getMenuStatusName(Integer status);

    List<Dict> getDictByCode(String code);

    String getDictNameByCode(String pCode, String code);

    /**
     * 查询字典
     */
    List<Dict> findInDict(Integer id);

    /**
     * 获取被缓存的对象(用户删除业务)
     */
    String getCacheObject(String para);

    /**
     * 获取子部门id
     */
    List<Integer> getSubDeptId(Integer deptid);

    /**
     * 获取所有父部门id
     */
    List<Integer> getParentDeptIds(Integer deptid);

    /**
     * 根据路网id获取路网层级名称
     *
     * @author wangshuaikang
     * @Date 2018/1/5 14:41
     */
    String getRoadnetNameById(Integer roadnetId);


    /**
     * 根据事故类型id获取事故类型名称
     *
     * @author wangshuaikang
     * @Date 2018/07/11 14:41
     */
    String getAccidentTypeNameById(Integer accidentTypeId);

    /**
     * 根据事故类型id获取事故类型名称
     *
     * @author wangshuaikang
     * @Date 2018/07/11 14:41
     */
    String getEmergencyNameById(Integer emergencyId);

    Integer getRoadnetIdByName(String name);

    List<Integer> getIsUsedChildrenIdsByPid(Integer pid);

    Integer getStationIdByNameAndLineName(String stationName, String lineName);

    /**
     * 通过源数据类型name 获取其id
     * @param name
     * @return
     */
    ResourceDataType getResourceDataTypeByName(String name);

    String getResourceDataTypeNameById(Integer id);
}
