package com.stylefeng.guns.common.persistence.model;

import java.io.Serializable;

import com.baomidou.mybatisplus.enums.IdType;
import java.util.Date;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableName;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

/**
 * <p>
 * 
 * </p>
 *
 * @author wangshuaikang
 * @since 2018-09-03
 */
@TableName("gzmtr_publicbus")
@Setter
@Getter
public class Publicbus extends Model<Publicbus> {

    private static final long serialVersionUID = 1L;

    /**
     * id
     */
	@TableId(value="id", type= IdType.AUTO)
	private Integer id;
    /**
     * 时间
     */
	private String time;
    /**
     * 操作人
     */
	private Integer user;
    /**
     * 站点名称
     */
	@TableField("station_name")
	private String stationName;
	private Integer level;
    /**
     * 处置报告
     */
	@TableField("dispose_doc")
	private String disposeDoc;

	/**
	 * 事故单位
	 */
	@TableField("accident_name")
	private String accidentName	;

	/**
	 * 结束时间
	 */
	@TableField("end_time")
	private String endTime;

	/**
	 * 接驳公交车数量
	 */
	@TableField("bus_num")
	private String busNum;

	/**
	 * 接驳路线
	 */
	@TableField("bus_line")
	private String busLine;

	/**
	 * 接驳路线
	 */
	@TableField("driver_id")
	private String driverId;



	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getTime() {
		return time;
	}

	public void setTime(String time) {
		this.time = time;
	}

	public Integer getUser() {
		return user;
	}

	public void setUser(Integer user) {
		this.user = user;
	}

	public String getStationName() {
		return stationName;
	}

	public void setStationName(String stationName) {
		this.stationName = stationName;
	}

	public Integer getLevel() {
		return level;
	}

	public void setLevel(Integer level) {
		this.level = level;
	}

	public String getDisposeDoc() {
		return disposeDoc;
	}

	public void setDisposeDoc(String disposeDoc) {
		this.disposeDoc = disposeDoc;
	}

	@Override
	protected Serializable pkVal() {
		return this.id;
	}

	@Override
	public String toString() {
		return "Publicbus{" +
			"id=" + id +
			", time=" + time +
			", user=" + user +
			", stationName=" + stationName +
			", level=" + level +
			", disposeDoc=" + disposeDoc +
			"}";
	}
}
