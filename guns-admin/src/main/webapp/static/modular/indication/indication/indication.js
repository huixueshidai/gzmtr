/**
 * 评价指标管理初始化
 */
var Indication = {
    id: "IndicationTable",	//表格id
    seItem: null,		//选中的条目
    table: null,
    layerIndex: -1
};

/**
 * 初始化表格的列
 */
Indication.initColumn = function () {
    return [
        {field: 'selectItem', radio: true},
        {title: 'id', field: 'id', visible: true, align: 'center', valign: 'middle', width: '50px'},
        {title: '指标名称', field: 'name', align: 'center', valign: 'middle',width:'500px'},
        {title: '权重', field: 'weight', align: 'center', valign: 'middle', width: '100px'},
        {title: '阈值', field: 'threshold', visible: true, align: 'center', valign: 'middle'},
        {title: '历史最大值', field: 'maxVal', visible: true, align: 'center', valign: 'middle'},
        {title: '历史最小值', field: 'minVal', visible: true, align: 'center', valign: 'middle'},
        {
            title: '值类型',
            field: 'bigIsBetter',
            visible: true,
            align: 'center',
            valign: 'middle',
            formatter: function (value, row, index) {
                value = row.bigIsBetter;
                if (value === 1) {
                    return "越大越优型";
                }else if(value === 0){
                    return "越小越优型";
                }

            }
        },
        {title: '层级', field: 'level', visible: true, align: 'center', valign: 'middle', width: '50px'},
        {title: '排序', field: 'num', visible: true, align: 'center', valign: 'middle', width: '50px'},
    ];
};

/**
 * 检查是否选中
 */
Indication.check = function () {
    var selected = $('#' + this.id).bootstrapTreeTable('getSelections');
    if (selected.length == 0) {
        Feng.info("请先选中表格中的某一记录！");
        return false;
    } else {
        Indication.seItem = selected[0];
        return true;
    }
};

/**
 * 点击添加评价指标
 */
Indication.openAddIndication = function () {
    var index = layer.open({
        type: 2,
        title: '添加评价指标',
        area: ['850px', '500px'], //宽高
        fix: false, //不固定
        maxmin: true,
        content: Feng.ctxPath + '/indication/indication_add'
    });
    this.layerIndex = index;
};

/**
 * 打开查看评价指标详情
 */
Indication.openIndicationDetail = function () {
    if (this.check()) {
        var index = layer.open({
            type: 2,
            title: '评价指标详情',
            area: ['850px', '500px'], //宽高
            fix: false, //不固定
            maxmin: true,
            content: Feng.ctxPath + '/indication/indication_update/' + Indication.seItem.id
        });
        this.layerIndex = index;
    }
};

/**
 * 打开权重
 */
Indication.openWeight = function () {
    if (this.check()) {
        var index = layer.open({
            type: 2,
            title: '权重',
            area: ['800px', '500px'], //宽高
            fix: false, //不固定
            maxmin: true,
            content: Feng.ctxPath + '/indication/indication_weight/' + Indication.seItem.id
        });
        this.layerIndex = index;
        layer.full(index);
    }
};

/**
 * 删除评价指标
 */
Indication.delete = function () {
    if (this.check()) {
        var ajax = new $ax(Feng.ctxPath + "/indication/delete", function (data) {
            Feng.success("删除成功!");
            Indication.table.refresh();
        }, function (data) {
            Feng.error("删除失败!" + data.responseJSON.message + "!");
        });
        ajax.set("indicationId", this.seItem.id);
        ajax.start();
    }
};

/**
 * 查询评价指标列表
 */
Indication.search = function () {
    var queryData = {};
    queryData['qName'] = $("#qName").val();
    Indication.table.refresh({query: queryData});
};

$(function () {
    var defaultColumns = Indication.initColumn();
    var table = new BSTreeTable(Indication.id, "/indication/list", defaultColumns);
    table.setExpandColumn(2);
    table.setIdField("id");
    table.setCodeField("id");
    table.setParentCodeField("pid");
    table.setExpandAll(false);
    Indication.table = table.init();
});
