var AccidentReport={
    accidentReport: {},
    count: $("#itemSize").val(),
    itemTemplate: $("#itemTemplate").html(),
    layerIndex:-1

};

AccidentReport.close = function () {
    parent.layer.close(window.parent.Accident.layerIndex);
}

/**
 * 清除数据
 */
AccidentReport.clearData = function () {
    this.accidentInfoData = {};
}

AccidentReport.set = function (key, val) {
    this.accidentReport[key] = (typeof val == "undefined") ? $("#" + key).val() : val;
    return this;
}
/**
 * 收集数据
 */
AccidentReport.collectData = function () {
    this
        .set('accidentName')
    ;
}

/**
 * 生成处置报告
 */
AccidentReport.submit = function () {
    $("#reportForm").submit();
}
/**
 * 添加空白可手动输入的联系人
 */
AccidentReport.addItem = function(){
    $("#itemsArea").append(this.itemTemplate);
    $("#dictItem").attr("id", this.newId());
    $("#name").attr("id", "name"+this.count);
    $("#duty").attr("id", "duty"+this.count);
    $("#telphone").attr("id", "telphone"+this.count);
}

/**
 * 通讯录添加
 */
AccidentReport.addItemContactSelData = function(name,duty,tel){
    $("#itemsArea").append(this.itemTemplate);
    $("#dictItem").attr("id", this.newId());
    $("#name").attr("id", "name"+this.count);
    $("#duty").attr("id", "duty"+this.count);
    $("#telphone").attr("id", "telphone"+this.count);
    $("#name"+this.count).attr("value", name);
    $("#duty"+this.count).attr("value", duty);
    $("#telphone"+this.count).attr("value", tel);

}


/**
 * 打开增加通讯录人员页面
 */
AccidentReport.addContact = function(){
    var index = layer.open({
        type: 2,
        title: '选择通讯录人员',
        area: ['1100px', '500px'], //宽高
        fix: false, //不固定
        maxmin: false,
        content: Feng.ctxPath + '/contact/contactSelIndex'
    });
    this.layerIndex = index;
}

/**
 * item获取新的id
 */
AccidentReport.newId = function () {
    if(this.count == undefined){
        this.count = 0;
    }
    this.count = this.count + 1;
    return "dictItem" + this.count;
};
/**
 * 删除item
 */
AccidentReport.deleteItem = function (event) {
    var obj = Feng.eventParseObject(event);
    obj.parent().parent().remove();
};

