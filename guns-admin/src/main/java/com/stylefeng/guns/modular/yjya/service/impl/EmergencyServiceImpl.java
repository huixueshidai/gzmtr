package com.stylefeng.guns.modular.yjya.service.impl;

import com.stylefeng.guns.common.persistence.model.Emergency;
import com.stylefeng.guns.common.persistence.dao.EmergencyMapper;
import com.stylefeng.guns.common.plugin.service.impl.BaseTreeableServiceImpl;
import com.stylefeng.guns.modular.yjya.service.IEmergencyService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author wangshuaikang
 * @since 2018-05-23
 */
@Service
public class EmergencyServiceImpl extends BaseTreeableServiceImpl<EmergencyMapper, Emergency> implements IEmergencyService {
	
}
