package com.stylefeng.guns.common.persistence.model;

import java.io.Serializable;

import com.baomidou.mybatisplus.enums.IdType;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableName;
import com.stylefeng.guns.common.plugin.entity.BaseModel;
import com.stylefeng.guns.common.plugin.entity.Treeable;

import java.io.Serializable;

/**
 * <p>
 * 
 * </p>
 *
 * @author 殷佳成
 * @since 2018-09-07
 */
@TableName("gzmtr_position_statement")
public class PositionStatement extends BaseModel<PositionStatement> implements Treeable {

    private static final long serialVersionUID = 1L;

    /**
     * id
     */
	@TableId(value="id", type= IdType.AUTO)
	private Integer id;
    /**
     * 岗位职责名称
     */
	private String name;
    /**
     * 排序
     */
	private Integer num;
	private Integer pid;
	private String pids;


	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Integer getNum() {
		return num;
	}

	public void setNum(Integer num) {
		this.num = num;
	}

	public Integer getPid() {
		return pid;
	}

	public void setPid(Integer pid) {
		this.pid = pid;
	}

	public String getPids() {
		return pids;
	}

	public void setPids(String pids) {
		this.pids = pids;
	}

	@Override
	protected Serializable pkVal() {
		return this.id;
	}

	@Override
	public String toString() {
		return "PositionStatement{" +
			"id=" + id +
			", name=" + name +
			", num=" + num +
			", pid=" + pid +
			", pids=" + pids +
			"}";
	}
}
