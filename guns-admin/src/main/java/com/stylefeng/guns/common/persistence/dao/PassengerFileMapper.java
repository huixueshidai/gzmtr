package com.stylefeng.guns.common.persistence.dao;

import com.stylefeng.guns.common.persistence.model.PassengerFile;
import com.baomidou.mybatisplus.mapper.BaseMapper;
import org.apache.ibatis.annotations.Param;

/**
 * <p>
  *  Mapper 接口
 * </p>
 *
 * @author wsk
 * @since 2018-09-25
 */
public interface PassengerFileMapper extends BaseMapper<PassengerFile> {


}