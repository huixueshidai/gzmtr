package com.stylefeng.guns.common.persistence.dao;

import com.stylefeng.guns.common.persistence.model.HubBus;
import com.baomidou.mybatisplus.mapper.BaseMapper;

/**
 * <p>
  * 公交站和车辆类型 Mapper 接口
 * </p>
 *
 * @author yinjc
 * @since 2018-03-21
 */
public interface HubBusMapper extends BaseMapper<HubBus> {

}