package com.stylefeng.guns.common.persistence.dao;

import com.stylefeng.guns.common.persistence.model.PassengerNum;
import com.baomidou.mybatisplus.mapper.BaseMapper;
import org.apache.ibatis.annotations.Param;

import java.util.Date;
import java.util.List;

/**
 * <p>
  *  Mapper 接口
 * </p>
 *
 * @author wsk
 * @since 2018-07-10
 */
public interface PassengerNumMapper extends BaseMapper<PassengerNum> {

    PassengerNum selectNum(@Param("starId")Integer starId,@Param("endId")Integer endId,@Param("starTime")String starTime );

    List<PassengerNum> selectNumStation(@Param("stationId")Integer stationId,@Param("starTime")String starTime );

    List<String> selectTime();

    List<String> selectByDatestatus(@Param("dateStatus")Integer dateStatus);

    void deleteByDateStatus ( @Param("dateStatus")Integer  dateStatus);
}