package com.stylefeng.guns.modular.yjya.service.impl;

import com.baomidou.mybatisplus.plugins.Page;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.stylefeng.guns.common.persistence.dao.AccidentProcessMapper;
import com.stylefeng.guns.common.persistence.model.AccidentProcess;
import com.stylefeng.guns.common.persistence.model.OperationLog;
import com.stylefeng.guns.modular.yjya.service.IAccidentProcessService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author 殷佳成
 * @since 2018-01-17
 */
@Service
public class AccidentProcessServiceImpl extends ServiceImpl<AccidentProcessMapper, AccidentProcess> implements IAccidentProcessService {
//	@Autowired
//    private IAccidentTypeService iAccidentTypeService;
//
//    @Override
//    public List<Map<String,Object>> selectPage2(String condition, Integer typeId) {
//        com.baomidou.mybatisplus.mapper.Wrapper wrapper = new EntityWrapper<AccidentProcess>();
//        wrapper.like(StringUtils.isNotBlank(condition), "condition", condition);
//        if (typeId != null && typeId != 0) {
//            Set<Integer> typeIdSet = iAccidentTypeService.findChildrenIds(typeId);
//            typeIdSet.add(typeId);
//            wrapper.in("Type_id", typeIdSet);
//        }
//        return selectPage2(wrapper);
//    }
//      @Autowired
//      private  AccidentProcessMapper accidentProcessMapper;
//
//      @Override
//      public  List<Map<String ,Object>> selectTrdIndications(Page<AccidentProcess> page, String condition, Integer typeId){
//           return accidentProcessMapper.selectPage2(page,condition,typeId ,page.getOrderByField(), page.isAsc());
//      }



}
