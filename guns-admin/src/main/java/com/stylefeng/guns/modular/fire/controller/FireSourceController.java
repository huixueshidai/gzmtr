package com.stylefeng.guns.modular.fire.controller;

import com.baomidou.mybatisplus.plugins.Page;
import com.stylefeng.guns.common.constant.factory.PageFactory;
import com.stylefeng.guns.common.persistence.model.FireSource;
import com.stylefeng.guns.core.base.controller.BaseController;
import com.stylefeng.guns.core.log.LogObjectHolder;
import com.stylefeng.guns.modular.fire.dao.FireSourceMapper;
import com.stylefeng.guns.modular.fire.service.IFireSourceService;
import com.stylefeng.guns.modular.fire.warpper.FireSourceWarpper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.List;
import java.util.Map;

/**
 * 火灾情景路径选择控制器
 *
 * @author xhq
 * @Date 2018-09-03 11:31:57
 */
@Controller
@RequestMapping("/fireSource")
public class FireSourceController extends BaseController {

    private String PREFIX = "/fire/fireSource/";

    @Autowired
    private IFireSourceService fireSourceService;

    @Autowired
    private FireSourceMapper fireSourceMapper;

    /**
     * 跳转到火灾情景路径选择首页
     */
    @RequestMapping("")
    public String index() {
        return PREFIX + "fireSource.html";
    }

    /**
     * 跳转到火灾情景路径选择首页
     */
    @RequestMapping("pathIndex")
    public String path_index() {
        return PREFIX + "path_volution.html";
    }


    /**
     * 跳转到添加火灾情景路径选择
     */
    @RequestMapping("/fireSource_add")
    public String fireSourceAdd() {
        return PREFIX + "fireSource_add.html";
    }

    /**
     * 跳转到修改火灾情景路径选择
     */
    @RequestMapping("/fireSource_update/{fireSourceId}")
    public String fireSourceUpdate(@PathVariable Integer fireSourceId, Model model) {
        FireSource fireSource = fireSourceService.selectById(fireSourceId);
        model.addAttribute("item",fireSource);

        LogObjectHolder.me().set(fireSource);
        return PREFIX + "fireSource_edit.html";
    }

    /**
     * 获取火灾情景路径选择列表
     */
    @RequestMapping(value = "/list")
    @ResponseBody
    public Object list(String condition) {
        Page<FireSource> page = new PageFactory<FireSource>().defaultPage();
        List<Map<String,Object>> retMap =  fireSourceMapper.selectMap(page);
        page.setRecords((List<FireSource>) new FireSourceWarpper(retMap).warp());
        return super.packForBT(page);
    }

    /**
     * 新增火灾情景路径选择
     */
    @RequestMapping(value = "/add")
    @ResponseBody
    public Object add(FireSource fireSource) {
        String rescuers = fireSource.getRescuers();
        String rescuersStart = rescuers.substring(0,rescuers.indexOf("-"));
        String rescuersEnd = rescuers.substring(rescuers.indexOf("-")+1,rescuers.length());
        fireSource.setRescuersStart(rescuersStart);
        fireSource.setRescuersEnd(rescuersEnd);

//        String e1 = fireSource.getExpert1();
//        String e1T = e1.substring(0,e1.indexOf(","));
        String e1T = fireSource.getExpert1T();
        String e1F = fireSource.getExpert1F();
//        String e1F = e1.substring(e1.indexOf(",")+1,e1.length());
        BigDecimal b1T = new BigDecimal(e1T);
        BigDecimal b1F = new BigDecimal(e1F);

//        String e2 = fireSource.getExpert2();
//        String e2T = e2.substring(0,e2.indexOf(","));
//        String e2F = e2.substring(e2.indexOf(",")+1,e2.length());

        String e2T = fireSource.getExpert2T();
        String e2F = fireSource.getExpert2F();
        BigDecimal b2T = new BigDecimal(e2T);
        BigDecimal b2F = new BigDecimal(e2F);

//        String e3 = fireSource.getExpert3();
//        String e3T = e3.substring(0,e3.indexOf(","));
//        String e3F = e3.substring(e3.indexOf(",")+1,e3.length());
        String e3T = fireSource.getExpert3T();
        String e3F = fireSource.getExpert3F();
        BigDecimal b3T = new BigDecimal(e3T);
        BigDecimal b3F = new BigDecimal(e3F);

        BigDecimal TK =b1T.multiply(b2T).multiply(b3T);
        BigDecimal FK = b1F.multiply(b2F).multiply(b3F);
        BigDecimal k1 = new BigDecimal(1);
        BigDecimal k = k1.subtract(TK).subtract(FK);

        BigDecimal e4T = TK.divide(k1.subtract(k),3, RoundingMode.HALF_UP);
        BigDecimal e4F= FK.divide(k1.subtract(k),3, RoundingMode.HALF_UP);
        fireSource.setResult(e4T+","+e4F);
        fireSourceService.insert(fireSource);
        return super.SUCCESS_TIP;
    }

    /**
     * 删除火灾情景路径选择
     */
    @RequestMapping(value = "/delete")
    @ResponseBody
    public Object delete(@RequestParam Integer fireSourceId) {
        fireSourceService.deleteById(fireSourceId);
        return SUCCESS_TIP;
    }

    /**
     * 修改火灾情景路径选择
     */
    @RequestMapping(value = "/update")
    @ResponseBody
    public Object update(FireSource fireSource) {
        String rescuers = fireSource.getRescuers();
        String rescuersStart = rescuers.substring(0,rescuers.indexOf("-"));
        String rescuersEnd = rescuers.substring(rescuers.indexOf("-")+1,rescuers.length());
        fireSource.setRescuersStart(rescuersStart);
        fireSource.setRescuersEnd(rescuersEnd);

//        String e1 = fireSource.getExpert1();
//        String e1T = e1.substring(0,e1.indexOf(","));
        String e1T = fireSource.getExpert1T();
        String e1F = fireSource.getExpert1F();
//        String e1F = e1.substring(e1.indexOf(",")+1,e1.length());
        BigDecimal b1T = new BigDecimal(e1T);
        BigDecimal b1F = new BigDecimal(e1F);

//        String e2 = fireSource.getExpert2();
//        String e2T = e2.substring(0,e2.indexOf(","));
//        String e2F = e2.substring(e2.indexOf(",")+1,e2.length());

        String e2T = fireSource.getExpert2T();
        String e2F = fireSource.getExpert2F();
        BigDecimal b2T = new BigDecimal(e2T);
        BigDecimal b2F = new BigDecimal(e2F);

//        String e3 = fireSource.getExpert3();
//        String e3T = e3.substring(0,e3.indexOf(","));
//        String e3F = e3.substring(e3.indexOf(",")+1,e3.length());
        String e3T = fireSource.getExpert3T();
        String e3F = fireSource.getExpert3F();
        BigDecimal b3T = new BigDecimal(e3T);
        BigDecimal b3F = new BigDecimal(e3F);

        BigDecimal TK =b1T.multiply(b2T).multiply(b3T);
        BigDecimal FK = b1F.multiply(b2F).multiply(b3F);
        BigDecimal k1 = new BigDecimal(1);
        BigDecimal k = k1.subtract(TK).subtract(FK);

        BigDecimal e4T = TK.divide(k1.subtract(k),3, RoundingMode.HALF_UP);
        BigDecimal e4F= FK.divide(k1.subtract(k),3, RoundingMode.HALF_UP);
        fireSource.setResult(e4T+","+e4F);
        fireSourceService.updateById(fireSource);
        return super.SUCCESS_TIP;
    }

    /**
     * 火灾情景路径选择详情
     */
    @RequestMapping(value = "/detail/{fireSourceId}")
    @ResponseBody
    public Object detail(@PathVariable("fireSourceId") Integer fireSourceId) {
        return fireSourceService.selectById(fireSourceId);
    }
}
