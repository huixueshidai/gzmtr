package com.stylefeng.guns.modular.fire.service;

import com.stylefeng.guns.common.persistence.model.FireSource;
import com.baomidou.mybatisplus.service.IService;

/**
 * <p>
 * 火源情景预算 服务类
 * </p>
 *
 * @author xhq
 * @since 2018-09-03
 */
public interface IFireSourceService extends IService<FireSource> {
	
}
