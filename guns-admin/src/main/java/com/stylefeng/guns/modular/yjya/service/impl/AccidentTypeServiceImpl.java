package com.stylefeng.guns.modular.yjya.service.impl;

import com.stylefeng.guns.common.persistence.model.AccidentType;
import com.stylefeng.guns.common.persistence.dao.AccidentTypeMapper;
import com.stylefeng.guns.common.plugin.entity.BaseModel;
import com.stylefeng.guns.common.plugin.service.impl.BaseTreeableServiceImpl;
import com.stylefeng.guns.modular.yjya.service.IAccidentTypeService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 事故处理措施 服务实现类
 * </p>
 *
 * @author 殷佳成
 * @since 2018-01-11
 */
@Service
public class AccidentTypeServiceImpl extends BaseTreeableServiceImpl<AccidentTypeMapper,AccidentType> implements IAccidentTypeService {
	
}
