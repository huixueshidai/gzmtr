package com.stylefeng.guns.modular.yjya.service;

import com.stylefeng.guns.common.persistence.model.Contact;
import com.baomidou.mybatisplus.service.IService;

import java.util.List;
import java.util.Map;

/**
 * <p>
 * 通讯录 服务类
 * </p>
 *
 * @author xuziyang
 * @since 2017-12-26
 */
public interface IContactService extends IService<Contact> {
//    List<Map<String, Object>> selectContacts(String name, Integer groupId);
}
