package com.stylefeng.guns.modular.assessment.calculate.station.passenger;

import com.stylefeng.guns.common.persistence.model.Assessment;
import com.stylefeng.guns.common.persistence.model.ResourceData;
import com.stylefeng.guns.core.util.DoubleUtil;
import com.stylefeng.guns.modular.assessment.calculate.ThirdIndicationCalculator;
import com.stylefeng.guns.modular.assessment.calculate.aware.StationThirdIndicationAware;
import org.springframework.stereotype.Component;

import java.util.Date;
import java.util.List;
import java.util.Map;

import static java.util.stream.Collectors.groupingBy;

/**
 * 出站闸机复杂度
 */
@Component
public class StationOutCal extends ThirdIndicationCalculator implements StationThirdIndicationAware {

    @Override
    protected Integer getIndicationId() {
        return 4;
    }

    @Override
    public Assessment calculate(Integer stationId, Date beginDate, Date endDate) {

        //查询这个站的闸机进站人数
        List<ResourceData> inNumList = resourceDataService.selectMore(stationId, 1, beginDate,endDate);

        //查询出站人数list
        List<ResourceData> outNumList = resourceDataService.selectMore(stationId, 5, beginDate,endDate);

        //单向闸机开放个数
        List<ResourceData> oneWayGateList = resourceDataService.selectMore(stationId, 6, beginDate,endDate);
        //双向闸机开放个数
        List<ResourceData> twoWayGateList = resourceDataService.selectMore(stationId, 272, beginDate,endDate);

        //每台进站闸机实际最大通过能力
        double ability = getDoubleValue(stationId,7,beginDate,endDate);

        //计算总的出站人数
        Double outNumTotal = outNumList.stream()
                .filter(resourceData -> resourceData.getValue() != null)
                .map(data -> Double.valueOf(data.getValue()))
                .reduce(DoubleUtil::add).get();



        //按进站口分组
        Map<String, List<ResourceData>> inNumEntryMap =
                inNumList.stream().collect(groupingBy(ResourceData::getEntrance));

        Map<String, List<ResourceData>> outNumEntryMap =
                outNumList.stream().collect(groupingBy(ResourceData::getEntrance));

        Map<String, List<ResourceData>> oneWayGateEntryMap =
                oneWayGateList.stream().collect(groupingBy(ResourceData::getEntrance));

        Map<String, List<ResourceData>> twoWayGateEntryMap =
                twoWayGateList.stream().collect(groupingBy(ResourceData::getEntrance));

        double resValue = 0D;


        for (String key : inNumEntryMap.keySet()) {
            double inNum = getDoubleValue(inNumEntryMap.get(key));
            double outNum = getDoubleValue(outNumEntryMap.get(key));
            double oneWayNum = getDoubleValue(oneWayGateEntryMap.get(key));
            double twoWayNum = getDoubleValue(twoWayGateEntryMap.get(key));

            //比重
            double ratio = DoubleUtil.div(outNum, outNumTotal);
            double wayNum = DoubleUtil.add(oneWayNum,
                    DoubleUtil.mul(twoWayNum,
                            DoubleUtil.div(inNum,
                                    DoubleUtil.add(inNum, outNum))));

            double durationT = getDurationT();

            double entryResValue = DoubleUtil.mul(ratio, DoubleUtil.safeDiv(outNum, DoubleUtil.mul(wayNum, durationT, ability)));

            resValue = DoubleUtil.add(resValue, entryResValue);
        }


        return genResult(resValue, stationId, beginDate,endDate);
    }
}
