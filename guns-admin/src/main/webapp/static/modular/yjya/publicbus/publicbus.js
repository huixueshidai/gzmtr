/**
 * 管理初始化
 */
var Publicbus = {
    id: "PublicbusTable",	//表格id
    seItem: null,		//选中的条目
    seItems:[],           //选中多条
    table: null,
    layerIndex: -1
};

/**
 * 初始化表格的列
 */
Publicbus.initColumn = function () {
    return [
        {field: 'selectItem', radio: true},
            {title: '时间', field: 'time', visible: true, align: 'center', valign: 'middle'},
            {title: '操作人', field: 'user', visible: true, align: 'center', valign: 'middle'},
            {title: '站点名称', field: 'stationName', visible: true, align: 'center', valign: 'middle'},
            {title: '响应等级', field: 'level', visible: true, align: 'center', valign: 'middle'},
            {title: '处置报告', field: 'disposeDoc', visible: true, align: 'center', valign: 'middle',formatter:function (value, row, index, field) {
                return "<a href='#' onclick='Publicbus.openPublicbusReport("+row.id+")'>公交应急联动处置报告.doc</a>";
            }}
    ];
};

/**
 * 检查是否选中
 */
Publicbus.check = function () {
    var selected = $('#' + this.id).bootstrapTable('getSelections');
    if(selected.length == 0){
        Feng.info("请先选中表格中的某一记录！");
        return false;
    }else{
        Publicbus.seItem = selected[0];
        Publicbus.seItems = selected;
        return true;
    }
};

/**
 * 点击添加
 */
Publicbus.openAddPublicbus = function () {
    var index = layer.open({
        type: 2,
        title: '添加',
        area: ['800px', '420px'], //宽高
        fix: false, //不固定
        maxmin: true,
        content: Feng.ctxPath + '/publicbus/publicbus_add'
    });
    this.layerIndex = index;
};

/**
 * 打开查看详情
 */
Publicbus.openPublicbusDetail = function () {
    if (this.check()) {
        var index = layer.open({
            type: 2,
            title: '详情',
            area: ['800px', '420px'], //宽高
            fix: false, //不固定
            maxmin: true,
            content: Feng.ctxPath + '/publicbus/publicbus_update/' + Publicbus.seItem.id
        });
        this.layerIndex = index;
    }
};

/**
 * 删除
 */
Publicbus.delete = function () {
    if (this.check()) {
        var selItems = Publicbus.seItems;
        var delIds = [];
        selItems.forEach(function(element){
            delIds.push(element.id);
        });
        var operation = function(){
            var ajax = new $ax(Feng.ctxPath + "/publicbus/delete", function () {
                Feng.success("删除成功!");
                Publicbus.table.refresh();
            }, function (data) {
                Feng.error("删除失败!" + data.responseJSON.message + "!");
            });
            ajax.set("ids", delIds);
            ajax.start();
        };
        Feng.confirm("是否删除选中处置留痕?" ,operation);
    }
};

/**
 * 点击查看报告
 */
Publicbus.openPublicbusReport= function (id) {
    var index = layer.open({
        type: 2,
        title: '公交联动处置报告',
        area: ['800px', '420px'], //宽高
        fix: false, //不固定
        maxmin: true,
        content: Feng.ctxPath + '/publicbus/publicreport/'+id
    });
    this.layerIndex = index;
    layer.full(index);
};


/**
 * 查询列表
 */
Publicbus.search = function () {
    var queryData = {};
    queryData['condition'] = $("#condition").val();
    Publicbus.table.refresh({query: queryData});
};

$(function () {
    var defaultColunms = Publicbus.initColumn();
    var table = new BSTable(Publicbus.id, "/publicbus/list", defaultColunms);
    table.setPaginationType("server");
    Publicbus.table = table.init();
});
