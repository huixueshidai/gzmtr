/**
 * 初始化详情对话框
 */
var EmergencypropertyInfoDlg = {
    emergencypropertyInfoData : {},
    validateFields: {
        emergencySel: {
            validators: {
                notEmpty: {
                    message: '突发事件不能为空'
                }
            }
        },
        ridership: {
            validators: {
                notEmpty: {
                    message: '客流不能为空'
                }
            }
        },
        trafficCapacity: {
            validators: {
                notEmpty: {
                    message: '通行能力不能为空'
                }
            }
        },
        fault: {
            validators: {
                notEmpty: {
                    message: '故障数量不能为空'
                }
            }
        },
        gasShield: {
            validators: {
                notEmpty: {
                    message: '气体保护不能为空'
                }
            }
        },
        tunnelLength: {
            validators: {
                notEmpty: {
                    message: '隧道长度不能为空'
                }
            }
        },
        status: {
            validators: {
                notEmpty: {
                    message: '状态不能为空'
                }
            }
        },
        distanceOn: {
            validators: {
                notEmpty: {
                    message: '距离前一站距离不能为空'
                }
            }
        },
        distanceNext: {
            validators: {
                notEmpty: {
                    message: '距离下一站距离不能为空'
                }
            }
        },
        trappedNum: {
            validators: {
                notEmpty: {
                    message: '被困人员数量不能为空'
                }
            }
        },
        trappedNumPower: {
            validators: {
                notEmpty: {
                    message: '被困人员行动能力不能为空'
                }
            }
        },
        physicalState: {
            validators: {
                notEmpty: {
                    message: '物理状态不能为空'
                }
            }
        },
        toxicity: {
            validators: {
                notEmpty: {
                    message: '毒性不能为空'
                }
            }
        },
        bums: {
            validators: {
                notEmpty: {
                    message: '燃爆性不能为空'
                }
            }
        },
        causticity: {
            validators: {
                notEmpty: {
                    message: '腐蚀性不能为空'
                }
            }
        },
        volatileness: {
            validators: {
                notEmpty: {
                    message: '挥发性不能为空'
                }
            }
        },
        temperature: {
            validators: {
                notEmpty: {
                    message: '温度不能为空'
                }
            }
        },
        smokescope: {
            validators: {
                notEmpty: {
                    message: '烟气浓度不能为空'
                }
            }
        },
        collapse: {
            validators: {
                notEmpty: {
                    message: '坍塌不能为空'
                }
            }
        },
        loseEfficacy: {
            validators: {
                notEmpty: {
                    message: '失效不能为空'
                }
            }
        },
        reoad: {
            validators: {
                notEmpty: {
                    message: '道路状况不能为空'
                }
            }
        },
        distanceOneOneNine: {
            validators: {
                notEmpty: {
                    message: '119距离不能为空'
                }
            }
        },
        distanceOneOneZero: {
            validators: {
                notEmpty: {
                    message: '110距离不能为空'
                }
            }
        },
        distanceOneTwoZero: {
            validators: {
                notEmpty: {
                    message: '120距离不能为空'
                }
            }
        },
        fireControl: {
            validators: {
                notEmpty: {
                    message: '消防设施不能为空'
                }
            }
        }
    }

};

/**
 * 验证数据是否为空
 */
EmergencypropertyInfoDlg.validate = function () {
    $('#emergencyInfoForm').data("bootstrapValidator").resetForm();
    $('#emergencyInfoForm').bootstrapValidator('validate');
    return $("#emergencyInfoForm").data('bootstrapValidator').isValid();
}

/**
 * 清除数据
 */
EmergencypropertyInfoDlg.clearData = function() {
    this.emergencypropertyInfoData = {};
}

/**
 * 设置对话框中的数据
 *
 * @param key 数据的名称
 * @param val 数据的具体值
 */
EmergencypropertyInfoDlg.set = function(key, val) {
    this.emergencypropertyInfoData[key] = (typeof val == "undefined") ? $("#" + key).val() : val;
    return this;
}

/**
 * 设置对话框中的数据
 *
 * @param key 数据的名称
 * @param val 数据的具体值
 */
EmergencypropertyInfoDlg.get = function(key) {
    return $("#" + key).val();
}

/**
 * 关闭此对话框
 */
EmergencypropertyInfoDlg.close = function() {
    parent.layer.close(window.parent.Emergencyproperty.layerIndex);
}

/**
 * 收集数据
 */
EmergencypropertyInfoDlg.collectData = function() {
    this
    .set('id')
    .set('emergencyId')
    .set('ridership')
    .set('trafficCapacity')
    .set('fault')
    .set('gasShield')
    .set('tunnelLength')
    .set('status')
    .set('distanceOn')
    .set('distanceNext')
    .set('trappedNum')
    .set('trappedNumPower')
    .set('physicalState')
    .set('toxicity')
    .set('bums')
    .set('causticity')
    .set('volatileness')
    .set('temperature')
    .set('smokescope')
    .set('collapse')
    .set('loseEfficacy')
    .set('reoad')
    .set('distanceOneOneNine')
    .set('distanceOneOneZero')
    .set('distanceOneTwoZero')
        .set('fireControl')
    ;
}

EmergencypropertyInfoDlg.showEmergencySelectTree = function () {
    var groupObj = $("#emergencySel");
    var groupOffset = $("#emergencySel").offset();
    $("#menuContent").css({
        left: groupOffset.left + "px",
        top: groupOffset.top + groupObj.outerHeight() + "px"
    }).slideDown("fast");

    $("body").bind("mousedown", onBodyDown);
}

EmergencypropertyInfoDlg.hideEmergencySelectTree = function () {
    $("#menuContent").fadeOut("fast");
    $("body").unbind("mousedown", onBodyDown);// mousedown当鼠标按下就可以触发，不用弹起
}

function onBodyDown(event) {
    if (!(event.target.id == "menuBtn" || event.target.id == "menuContent" || $(
            event.target).parents("#menuContent").length > 0)) {
        EmergencypropertyInfoDlg.hideEmergencySelectTree();
    }
}

EmergencypropertyInfoDlg.onClickEmergency = function (e, treeId, treeNode) {
    $("#emergencySel").attr("value", instance.getSelectedVal());
    $("#emergencyId").attr("value", treeNode.id);
}


/**
 * 提交添加
 */
EmergencypropertyInfoDlg.addSubmit = function() {

    this.clearData();
    this.collectData();
    if (!this.validate()) {
        return;
    }

    //提交信息
    var ajax = new $ax(Feng.ctxPath + "/emergencyproperty/add", function(data){
        Feng.success("添加成功!");
        window.parent.Emergencyproperty.table.refresh();
        EmergencypropertyInfoDlg.close();
    },function(data){
        Feng.error("添加失败!" + data.responseJSON.message + "!");
    });
    ajax.set(this.emergencypropertyInfoData);
    ajax.start();
}

/**
 * 提交修改
 */
EmergencypropertyInfoDlg.editSubmit = function() {

    this.clearData();
    this.collectData();

    //提交信息
    var ajax = new $ax(Feng.ctxPath + "/emergencyproperty/update", function(data){
        Feng.success("修改成功!");
        window.parent.Emergencyproperty.table.refresh();
        EmergencypropertyInfoDlg.close();
    },function(data){
        Feng.error("修改失败!" + data.responseJSON.message + "!");
    });
    ajax.set(this.emergencypropertyInfoData);
    ajax.start();
}

$(function() {
    Feng.initValidator("emergencyInfoForm", EmergencypropertyInfoDlg.validateFields);

    var ztree = new $ZTree("treeDemo", "/emergency/tree");
    ztree.bindOnClick(EmergencypropertyInfoDlg.onClickEmergency);
    ztree.init();
    instance = ztree;
});
