var FishBone = {
    myDiagram: null
}

// use FishboneLayout and FishboneLink
FishBone.layoutFishbone = function () {
    this.myDiagram.startTransaction("fishbone layout");
    this.myDiagram.linkTemplate =  this.myDiagram.linkTemplateMap.getValue("fishbone");
    this.myDiagram.layout = go.GraphObject.make(FishboneLayout, {  // defined above
        angle: 180,
        layerSpacing: 10,
        nodeSpacing: 20,
        rowSpacing: 10
    });
    this.myDiagram.commitTransaction("fishbone layout");
}

// make the layout a branching tree layout and use a normal link template
FishBone.layoutBranching = function () {
    this.myDiagram.startTransaction("branching layout");
    this.myDiagram.linkTemplate = this.myDiagram.linkTemplateMap.getValue("normal");
    this.myDiagram.layout = go.GraphObject.make(go.TreeLayout, {
        angle: 180,
        layerSpacing: 20,
        alignment: go.TreeLayout.AlignmentBusBranching
    });
    this.myDiagram.commitTransaction("branching layout");
}

// make the layout a basic tree layout and use a normal link template
FishBone.layoutNormal = function () {
    this.myDiagram.startTransaction("normal layout");
    this.myDiagram.linkTemplate = this.myDiagram.linkTemplateMap.getValue("normal");
    this.myDiagram.layout = go.GraphObject.make(go.TreeLayout, {
        angle: 180,
        breadthLimit: 1000,
        alignment: go.TreeLayout.AlignmentStart
    });
    this.myDiagram.commitTransaction("normal layout");
}

FishBone.init = function (json) {
    if (window.goSamples) goSamples();  // init for these samples -- you don't need to call this
    var $ = go.GraphObject.make;  // for conciseness in defining templates

    var myDiagram =
        $(go.Diagram, "myDiagramDiv",  // refers to its DIV HTML element by id
            {isReadOnly: true});  // do not allow the user to modify the diagram

    FishBone.myDiagram = myDiagram;

    // define the normal node template, just some text
    myDiagram.nodeTemplate =
        $(go.Node, "Auto",
            $(go.TextBlock,
                new go.Binding("text"),
                new go.Binding("font", "", convertFont))
        );

    function convertFont(data) {
        var size = data.size;
        if (!size) size = 13;
        var weight = data.weight;
        // if (weight === undefined) weight = "";
        return weight + " " + size + "px sans-serif";
    }

    myDiagram.linkTemplateMap.add("normal",
        $(go.Link,
            {routing: go.Link.Orthogonal, corner: 4},
            $(go.Shape)
        ));

    // use this link template for fishbone layouts
    myDiagram.linkTemplateMap.add("fishbone",
        $(FishboneLink,  // defined above
            $(go.Shape)
        ));


    function walkJson(obj, arr) {

        console.log(obj);
        var key = arr.length;
        obj.key = key;
        arr.push(obj);

        var children = obj.causes;
        if (children) {
            for (var i = 0; i < children.length; i++) {
                var o = children[i];
                o.parent = key;  // reference to parent node data
                walkJson(o, arr);
            }
        }
    }

    // build the tree model
    var nodeDataArray = [];
    walkJson(json, nodeDataArray);
    myDiagram.model = new go.TreeModel(nodeDataArray);

    FishBone.layoutFishbone();
}

$(function () {
    $.ajax({
        url:Feng.ctxPath + "/fishbone/data"
        ,type:"GET"
        ,dataType:"json"
        ,success:function (data) {
            FishBone.init(data);
        }
    })
})