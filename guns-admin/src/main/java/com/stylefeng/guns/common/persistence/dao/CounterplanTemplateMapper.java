package com.stylefeng.guns.common.persistence.dao;

import com.stylefeng.guns.common.persistence.model.CounterplanTemplate;
import com.baomidou.mybatisplus.mapper.BaseMapper;

/**
 * <p>
  * 预案模板 Mapper 接口
 * </p>
 *
 * @author xuziyang
 * @since 2018-01-04
 */
public interface CounterplanTemplateMapper extends BaseMapper<CounterplanTemplate> {

}