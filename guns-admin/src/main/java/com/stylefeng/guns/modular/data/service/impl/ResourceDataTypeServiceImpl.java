package com.stylefeng.guns.modular.data.service.impl;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.mapper.Wrapper;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.stylefeng.guns.common.persistence.dao.ResourceDataTypeMapper;
import com.stylefeng.guns.common.persistence.model.ResourceDataType;
import com.stylefeng.guns.core.node.ZTreeNode;
import com.stylefeng.guns.modular.data.service.IResourceDataTypeService;
import com.stylefeng.guns.modular.indication.service.IIndicationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

/**
 * <p>
 * 源数据类型 服务实现类
 * </p>
 *
 * @author xuziyang
 * @since 2018-01-15
 */
@Service
public class ResourceDataTypeServiceImpl extends ServiceImpl<ResourceDataTypeMapper, ResourceDataType> implements IResourceDataTypeService {

    @Autowired
    private IIndicationService indicationService;

    @Override
    public List<ZTreeNode> tree() {
        List<ZTreeNode> treeList = new ArrayList<>();

        List<ZTreeNode> indicationTree = indicationService.tree();
        for (ZTreeNode node : indicationTree) {
            String id = "i_" + node.getId();
            String pid = "i_" + node.getpId();
            node.setId(id);
            node.setpId(pid);
        }

        treeList.addAll(indicationTree);

        List<ResourceDataType> resourceDataTypeList = selectList(null);
        for (ResourceDataType type : resourceDataTypeList) {
            ZTreeNode node = new ZTreeNode();
            treeList.add(node);
            node.setId(type.getId() + "");
            node.setpId("i_" + type.getIndicationId());
            node.setName(type.getName());
            node.setIsOpen(false);
            node.setChecked(false);
        }
        return treeList;

    }

    @Override
    public ResourceDataType selectByName(String name) {
        Wrapper<ResourceDataType> wrapper = new EntityWrapper<>();
        wrapper.eq("name", name);
        return this.selectOne(wrapper);
    }
}
