package com.stylefeng.guns.modular.assessment.calculate.road;

import com.stylefeng.guns.core.util.SpringContextHolder;
import com.stylefeng.guns.modular.assessment.calculate.FirstIndicationCalculator;
import com.stylefeng.guns.modular.assessment.calculate.SecondIndicationCalculator;
import com.stylefeng.guns.modular.assessment.calculate.road.accident.RoadAccidentCal;
import com.stylefeng.guns.modular.assessment.calculate.road.environment.RoadEnvironmentCal;
import com.stylefeng.guns.modular.assessment.calculate.road.facility.RoadFacilityCal;
import com.stylefeng.guns.modular.assessment.calculate.road.manage.RoadManageCal;
import com.stylefeng.guns.modular.assessment.calculate.road.passenger.RoadPassengerCal;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import java.util.Arrays;
import java.util.List;

@Component
@Scope(ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public class RoadCal extends FirstIndicationCalculator {

    private List<SecondIndicationCalculator> secondIndicationCalculates;

    public RoadCal() {
        secondIndicationCalculates = Arrays.asList(
                SpringContextHolder.getBean(RoadAccidentCal.class),
                SpringContextHolder.getBean(RoadEnvironmentCal.class),
                SpringContextHolder.getBean(RoadFacilityCal.class),
                SpringContextHolder.getBean(RoadManageCal.class),
                SpringContextHolder.getBean(RoadPassengerCal.class)
        );

    }

    @Override
    public List<SecondIndicationCalculator> getSecondIndicationCalculateList() {
        return secondIndicationCalculates;
    }



    @Override
    protected Integer getIndicationId() {
        return 43;
    }
}
