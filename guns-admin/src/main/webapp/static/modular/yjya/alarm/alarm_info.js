/**
 * 初始化报警管理详情对话框
 */
var AlarmInfoDlg = {
    alarmInfoData : {},
    accidentTypeTreeInstance:null,
    lineTreeInstance:null,
    stationTreeInstance:null,
    emergencyTreeInstatce:null,
    checkCount:parseInt($("#checkCount").val()),  //选择的车站数量
    stationdIds:[],
    locale:""

};

/**
 * 清除数据
 */
AlarmInfoDlg.clearData = function() {
    AlarmInfoDlg.alarmInfoData = {};
}

/**
 * 设置对话框中的数据
 *
 * @param key 数据的名称
 * @param val 数据的具体值
 */
AlarmInfoDlg.set = function(key, val) {
    AlarmInfoDlg.alarmInfoData[key] = (typeof val == "undefined") ? $("#" + key).val() : val;
    return this;
}

/**
 * 设置对话框中的数据
 *
 * @param key 数据的名称
 * @param val 数据的具体值
 */
AlarmInfoDlg.get = function(key) {
    return $("#" + key).val();
}

/**
 * 关闭此对话框
 */
AlarmInfoDlg.close = function() {
    parent.layer.close(window.parent.Alarm.layerIndex);
}

/**
 * 收集数据
 */
AlarmInfoDlg.collectData = function() {
    AlarmInfoDlg
    .set('id')
    .set('accidentTypeId')
    .set('accidentLevel')
    .set('lineId')
    .set('stationId')
    .set('trainNumber')
    .set('startTime')
    .set('reduction')
    .set('emergencyId')
        .set('trapPeoNum')
        .set('lossPeoNum')
        .set('closedStation')
        .set('intervalEvacuation')
        .set('personalCasualty')
        .set('trainClearingMan')
        .set('largePassenger')
        .set('otherCheck')
        .set('operationInterruption')
        .set('other')
        .set('measuresToken')
        .set('trapNum')
        .set('emergencyproId')
        .set('interruptionType')
        .set('retentionTime')
        .set('interruptionTime')
        .set('expectDelays')
        .set('lossEfficiencyRetention')
        .set('lossEfficiencyRransportation')
        .set('schedulingMode')
        .set('schedulingModeMin')
        .set('stream')
        .set('locale',AlarmInfoDlg.locale)
    ;
}

/**
 * 提交添加
 */
AlarmInfoDlg.addSubmit = function() {

    this.clearData();
    this.collectData();

    //提交信息
    var ajax = new $ax(Feng.ctxPath + "/alarm/add", function(data){
        Feng.success("添加成功!");
        window.parent.Alarm.table.refresh();
        AlarmInfoDlg.close();
    },function(data){
        Feng.error("添加失败!" + data.responseJSON.message + "!");
    });
    ajax.set(this.alarmInfoData);
    ajax.start();
}

/**
 * 提交修改
 */
AlarmInfoDlg.editSubmit = function() {

    this.clearData();
    this.collectData();

    //提交信息
    var ajax = new $ax(Feng.ctxPath + "/alarm/update", function(data){
        Feng.success("修改成功!");
        window.parent.Alarm.table.refresh();
        AlarmInfoDlg.close();
    },function(data){
        Feng.error("修改失败!" + data.responseJSON.message + "!");
    });
    ajax.set(this.alarmInfoData);
    ajax.start();
}

/**
 * 重置
 */
AlarmInfoDlg.delete = function () {

    var operation = function(){
        var ajax = new $ax(Feng.ctxPath + "/alarm/delete", function (data) {
            Feng.success("重置成功!");
            $("#jiejing").attr("disabled",false);
            location.reload();
            Alarm.table.refresh();
        }, function (data) {
            Feng.error("重置失败!" + data.responseJSON.message + "!");
        });
        ajax.start();
    }
    Feng.confirm("确定重置接警信息?" ,operation);
};

/**
 * 接警信息提交
 */
AlarmInfoDlg.alarmSubmit = function() {
    this.clearData();
    //现场情况
    if($("#operationInterruption").prop('checked')){
        if($("#interruptionType").val()!=""||$("#interruptionType ").val()!=null){
            AlarmInfoDlg.locale += "运营中断："+ $("#interruptionType option:selected").text()+" ";
            if($("#interruptionTime").val()!=""||$("#interruptionTime").val()!=null){
                AlarmInfoDlg.locale += $("#interruptionTime").val()+"min;"+"\n";
            }
            else{
                AlarmInfoDlg.locale += ";"+"\n";
            }
        }
        else{
            AlarmInfoDlg.locale += "运营中断;"+"\n";
        }
    }
    if($("#closedStation").prop('checked')){
        AlarmInfoDlg.locale += "车站封闭;"+"\n";
    }
    if($("#trainClearingMan").prop('checked')){
        AlarmInfoDlg.locale += "列车清人;"+"\n";
    }
    if($("#personalCasualty").prop('checked')){
        if($("#lossPeoNum").val()!=""||$("#lossPeoNum").val()!=null){
            AlarmInfoDlg.locale += "人员伤亡 "+ $("#lossPeoNum").val()+" 人;"+"\n";
        }
        else{
            AlarmInfoDlg.locale += "人员伤亡;"+"\n";
        }
    }
    if($("#intervalEvacuation").prop('checked')){
        AlarmInfoDlg.locale += "区间疏散;"+"\n";
    }
    if($("#largePassenger").prop('checked')){
        AlarmInfoDlg.locale += "大客流聚集;"+"\n";
    }
    if($("#otherCheck").prop('checked')){
        if($("#other").val()!=""||$("#other").val()!=null){
            AlarmInfoDlg.locale += "其它："+ $("#other").val()+";"+"\n";
        }
        else{
            AlarmInfoDlg.locale += "其它;"+"\n";
        }

    }
    this.collectData();

    var operation = function(){
        //提交信息
        var ajax = new $ax(Feng.ctxPath + "/alarm/alarm_editerupdate", function(data){
            Feng.success("接警信息提交成功!");
            $("#jiejing").attr("disabled",true);
            $("#ensure").attr("disabled", false);
            window.parent.document.getElementById("menu_313").click();
        },function(data){
            Feng.error("接警信息提交失败!" + data.responseJSON.message + "!");
        });
        ajax.set(AlarmInfoDlg.alarmInfoData);
        ajax.start();
    }
    Feng.confirm("确定提交接警信息?" ,operation);
}

/**
 * 事故类型选择
 */
AlarmInfoDlg.showAccidentTypeSelectTree = function () {
    Feng.showInputTree("accidentTypeName", "accidentTypeTreeDiv", 15, 34);
}
AlarmInfoDlg.onClickAccidentType = function (e, treeId, treeNode) {
    $("#accidentTypeName").attr("value", AlarmInfoDlg.accidentTypeTreeInstance.getSelectedVals());
    $("#accidentTypeId").attr("value", treeNode.id);
    $("#accidentTypeTreeDiv").fadeOut("fast");
}

/**
 * 事故线路
 */
AlarmInfoDlg.showLineTree = function () {
    Feng.showInputTree("lineName", "lineTreeDiv", 15, 34);
}
AlarmInfoDlg.onClickLine = function (e, treeId, treeNode) {
    if(treeNode.isParent){
        Feng.info("请选择线路");
        return;
    }
    $("#lineName").attr("value", AlarmInfoDlg.lineTreeInstance.getSelectedVal());
    $("#lineId").attr("value", treeNode.id);

    $("#stationName").attr("value", "");
    $("#stationId").attr("value", "");
    AlarmInfoDlg.initStationTree(treeNode.id);
    $("#lineTreeDiv").fadeOut("fast");

}
AlarmInfoDlg.initStationTree = function (lineId) {
    var stationTree = new $ZTree("stationTree","/roadnet/station/tree/"+lineId);
    stationTree.setSettings({
        view : {
            dblClickExpand : true,
            selectedMulti : false
        },
        data : {simpleData : {enable : true}},
        check: {
            enable: true,
            chkStyle: "checkbox",
            chkboxType: { "Y": "", "N": "" }
        },
        callback : {
            onCheck:AlarmInfoDlg.onClickStation
        }
    });
    stationTree.Check(AlarmInfoDlg.onClickStation);
    stationTree.init();
    AlarmInfoDlg.stationTreeInstance = stationTree;
}

AlarmInfoDlg.check = function(){
    var interruptionTime = $("#interruptionTime").val();

    var retentionTime = $("#retentionTime").val();

    if(retentionTime == ""){
        Feng.info("请填写滞留时间");
        return false;
    }
    else if(interruptionTime == ""){
        Feng.info("请填写预计中断时间");
        return false;
    }
    else{
        return true;
    }

}

/**
 * 事故车站选择
 */
AlarmInfoDlg.showStationTree = function () {
    if(!AlarmInfoDlg.stationTreeInstance){
        Feng.info("请选择线路");
    }
    Feng.showInputTree("stationName", "stationTreeDiv", 15, 34);
}

/**
 * 根据数组的值删除
 * @param arr
 * @param val
 */
AlarmInfoDlg.removeByValue = function(arr,val){
    for(var i=0;i<arr.length;i++){
        if(arr[i] == val){
            arr.splice(i, 1);
            break;
        }
    }
}

AlarmInfoDlg.onClickStation = function (e, treeId, treeNode) {
    if(treeNode.checked){
        AlarmInfoDlg.checkCount++;
        if(AlarmInfoDlg.checkCount==1){
            $("#stationName").val(treeNode.name);
            AlarmInfoDlg.stationdIds.push(treeNode.id);
        }
        if(AlarmInfoDlg.checkCount==2){
            $("#stationName").val($("#stationName").val()+">>"+treeNode.name);
            AlarmInfoDlg.stationdIds.push(treeNode.id);
        }
    }
    else{
        AlarmInfoDlg.checkCount--;
        if(AlarmInfoDlg.checkCount==0){
            $("#stationName").val(null);
            AlarmInfoDlg.removeByValue(AlarmInfoDlg.stationdIds,treeNode.id);
        }
        if(AlarmInfoDlg.checkCount==1){
            $("#stationName").val(AlarmInfoDlg.stationTreeInstance.getSelectedCheck());
            AlarmInfoDlg.removeByValue(AlarmInfoDlg.stationdIds,treeNode.id);
        }
    }
    if( AlarmInfoDlg.stationdIds.length>0) {
        var stationId = "";
        for (var i = 0; i < AlarmInfoDlg.stationdIds.length; i++) {
            stationId += AlarmInfoDlg.stationdIds[i] + "/";
        }
        $("#stationId").attr("value", stationId);
    }

    if( AlarmInfoDlg.checkCount==2){
        $("#stationTreeDiv").fadeOut("fast");
    }
}

/**
 * 突发事件类型
 */
AlarmInfoDlg.showEmergencySelectTree = function () {
    Feng.showInputTree("emergencyName", "emergencyTreeDiv", 15, 34);
}
AlarmInfoDlg.onClickEmergency = function (e, treeId, treeNode) {
    $("#emergencyName").attr("value", AlarmInfoDlg.emergencyTreeInstatce.getSelectedVals());
    $("#emergencyId").attr("value", treeNode.id);
    $("#emergencyTreeDiv").fadeOut("fast");
}

/**
 * 突发事件属性选择弹出页面
 */
AlarmInfoDlg.emergencyPropertySel = function () {
    var emergencyproId = $("#emergencyproId").val();
    if(emergencyproId){
        var index = layer.open({
            type: 2,
            title: '请填写突发事件属性值',
            area: ['1200px', '600px'], //宽高
            fix: false, //不固定
            maxmin: true,
            content: Feng.ctxPath + '/accidentEmergencyproperty/accidentEmergencyproperty_update/'+emergencyproId
        });
    }
    else{
        var index = layer.open({
            type: 2,
            title: '请填写突发事件属性值',
            area: ['1200px', '600px'], //宽高
            fix: false, //不固定
            maxmin: true,
            content: Feng.ctxPath + '/accidentEmergencyproperty/accidentEmergencyproperty_add'
        });
    }

    this.layerIndex = index;
    layer.full(index);
};

/**
 * 突发事件属性选择赋值
 */
AlarmInfoDlg.emergencyPropertyAssign = function (emergencyproId) {
    $("#emergencyproId").val(emergencyproId);
};


/**
 * 接警信息提交
 */
AlarmInfoDlg.submitAccident = function(){
    AlarmInfoDlg.collectData();
    var ajax = new $ax(Feng.ctxPath + "/alarm/updateAlarm", function (data) {
        Feng.success("接警信息提交成功!");
        window.parent.document.getElementById("menu_313").click();
    }, function (data) {
        Feng.error("接警信息提交失败!" + data.responseJSON.message + "!");
    });
    ajax.set(AlarmInfoDlg.alarmInfoData);
    ajax.start();
}

/**
 * 接警页面启动公交应急按钮提交
 */
AlarmInfoDlg.submitAccidentBus = function(){


    window.parent.document.getElementById("menu_316").click();
        // AlarmInfoDlg.collectData();
        // var ajax = new $ax(Feng.ctxPath + "/alarm/updateAlarmBus", function (data) {
        //     Feng.success("公交应急开启!");
        //     window.parent.document.getElementById("menu_316").click();
        // }, function (data) {
        //     Feng.error("启动失败!" + data.responseJSON.message + "!");
        // });
        // ajax.set(AlarmInfoDlg.alarmInfoData);
        // ajax.start();

}


/**
 * 处警信息提交
 */
AlarmInfoDlg.submitAccidentTwo = function(){
    var counterplanId = AccidentCounterplanSel.getSelectedId();
    var type = AccidentCounterplanSel.getSelectedType();
    AlarmInfoDlg.set("counterplanId",counterplanId);
    AlarmInfoDlg.set("counterplanType",type);
    var ajax = new $ax(Feng.ctxPath + "/alarm/updateAlarmTwo", function (data) {
        Feng.success("处警信息选择成功!");
        $("#chujing").attr("disabled",true);
        window.parent.document.getElementById("menu_314").click();
    }, function (data) {
        Feng.error("处警信息选择失败!" + data.responseJSON.message + "!");
    });
    console.log(AlarmInfoDlg.alarmInfoData);
    ajax.set(AlarmInfoDlg.alarmInfoData);
    ajax.start();
}

/**
 * 公交应急联动执行“启动”按钮
 */
AlarmInfoDlg.submitBusExecute = function(){
    var stationAmount = PublicBusFeederRoute.stationList.length;
    var startStation = PublicBusFeederRoute.startStation;
    var endStation = PublicBusFeederRoute.endStation;
    AlarmInfoDlg.set("stationAmount",stationAmount);
    AlarmInfoDlg.set("startStation",startStation);
    AlarmInfoDlg.set("endStation",endStation);
    var ajax = new $ax(Feng.ctxPath + "/alarm/updateBusExecute", function (data) {
        Feng.success("公交应急联动启动执行成功!");
        window.parent.document.getElementById("menu_318").click();
    }, function (data) {
        Feng.error("公交应急联动启动执行失败!" + data.responseJSON.message + "!");
    });
    ajax.set(AlarmInfoDlg.alarmInfoData);
    ajax.start();
}

/**
 * 编制处置流程信息提交
 */
AlarmInfoDlg.submitAccidentThree = function(){
    var json = JSON.stringify(ProcessInfo.processInstance.exportData());
    AlarmInfoDlg.set("processJson",json);
    var ajax = new $ax(Feng.ctxPath + "/alarm/updateAlarmThree", function (data) {
        Feng.success("处置流程提交成功!");
        $("#editorFlow").attr("disabled",true);
        window.parent.document.getElementById("menu_315").click();
    }, function (data) {
        Feng.error("处置流程提交失败!" + data.responseJSON.message + "!");
    });
    ajax.set(AlarmInfoDlg.alarmInfoData);
    ajax.start();
}

/**
 * 处置阶段步骤id信息文本保存提交
 */
AlarmInfoDlg.submitdispositionStep = function(){
    AlarmInfoDlg.set("dispositionStep",AccidentInfoDlg.dispositionStep.toString());
    AlarmInfoDlg.set("dispositionStepTxt",$("#report").val());
    var ajax = new $ax(Feng.ctxPath + "/alarm/updateAlarmdispositionStep", function (data) {

    }, function (data) {
        Feng.error("失败!" + data.responseJSON.message + "!");
    });
    ajax.set(AlarmInfoDlg.alarmInfoData);
    ajax.start();
}




$(function() {
    var accidentTypeTree = new $ZTree("accidentTypeTree", "/accidentType/tree");
    accidentTypeTree.bindOnClick(AlarmInfoDlg.onClickAccidentType);
    accidentTypeTree.init();

    var lineTree = new $ZTree("lineTree", "/roadnet/line/tree");
    lineTree.bindOnClick(AlarmInfoDlg.onClickLine);
    lineTree.init();
    AlarmInfoDlg.lineTreeInstance = lineTree;

    var emergencyTree = new $ZTree("emergencyTree", "/emergency/tree");
    emergencyTree.bindOnClick(AlarmInfoDlg.onClickEmergency);
    emergencyTree.init();
    AlarmInfoDlg.emergencyTreeInstatce = emergencyTree;
    AlarmInfoDlg.accidentTypeTreeInstance = accidentTypeTree;
});
