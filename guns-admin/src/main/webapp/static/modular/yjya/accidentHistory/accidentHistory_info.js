/**
 * 初始化事故（事件）历史信息详情对话框
 */
var AccidentHistoryInfoDlg = {
    accidentHistoryInfoData : {}
};

/**
 * 清除数据
 */
AccidentHistoryInfoDlg.clearData = function() {
    this.accidentHistoryInfoData = {};
}

/**
 * 设置对话框中的数据
 *
 * @param key 数据的名称
 * @param val 数据的具体值
 */
AccidentHistoryInfoDlg.set = function(key, val) {
    this.accidentHistoryInfoData[key] = (typeof val == "undefined") ? $("#" + key).val() : val;
    return this;
}

/**
 * 设置对话框中的数据
 *
 * @param key 数据的名称
 * @param val 数据的具体值
 */
AccidentHistoryInfoDlg.get = function(key) {
    return $("#" + key).val();
}

/**
 * 关闭此对话框
 */
AccidentHistoryInfoDlg.close = function() {
    parent.layer.close(window.parent.AccidentHistory.layerIndex);
}

/**
 * 收集数据
 */
AccidentHistoryInfoDlg.collectData = function() {
    this
        .set('id')
        .set('name')
        .set('date')
        .set("roadnetId")
        .set("areaCode")
        .set('rankCode')
        .set('category')
        .set('cause')
        .set('detail')

}

/**
 * 提交添加
 */
AccidentHistoryInfoDlg.addSubmit = function() {

    this.clearData();
    this.collectData();

    //提交信息
    var ajax = new $ax(Feng.ctxPath + "/accidentHistory/add", function(data){
        Feng.success("添加成功!");
        window.parent.AccidentHistory.table.refresh();
        AccidentHistoryInfoDlg.close();
    },function(data){
        Feng.error("添加失败!" + data.responseJSON.message + "!");
    });
    ajax.set(this.accidentHistoryInfoData);
    ajax.start();
}



/**
 * 提交修改
 */
AccidentHistoryInfoDlg.editSubmit = function() {

    this.clearData();
    this.collectData();

    //提交信息
    var ajax = new $ax(Feng.ctxPath + "/accidentHistory/update", function(data){
        Feng.success("修改成功!");
        window.parent.AccidentHistory.table.refresh();
        AccidentHistoryInfoDlg.close();
    },function(data){
        Feng.error("修改失败!" + data.responseJSON.message + "!");
    });
    ajax.set(this.accidentHistoryInfoData);
    ajax.start();
}

AccidentHistoryInfoDlg.showStationTree = function () {
    Feng.showInputTree("stationSel", "stationTreeDiv", 15, 34);
};


AccidentHistoryInfoDlg.onClickStation = function (e, treeId, treeNode) {
    // if(treeNode.isParent){
    //     Feng.info("请选择车站");
    //     return;
    // }
    $("#roadnetName").attr("value",treeNode.name);
    $("#roadnetId").attr("value", treeNode.id);
    $("#stationTreeDiv").fadeOut("fast");

};

$(function() {
    var stationTree = new $ZTree("stationTree", "/roadnet/tree");
    stationTree.bindOnClick(AccidentHistoryInfoDlg.onClickStation);
    stationTree.init();

    laydate.render({
        elem: '#date'
        ,type:'datetime'
    });
});
