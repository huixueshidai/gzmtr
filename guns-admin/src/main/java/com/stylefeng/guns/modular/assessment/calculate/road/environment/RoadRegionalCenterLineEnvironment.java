package com.stylefeng.guns.modular.assessment.calculate.road.environment;

import com.stylefeng.guns.common.persistence.model.Assessment;
import com.stylefeng.guns.modular.assessment.calculate.ThirdIndicationCalculator;
import com.stylefeng.guns.modular.assessment.calculate.area.environment.AreaRegionalCenterLineEnvironment;
import com.stylefeng.guns.modular.assessment.calculate.area.facility.util.AreaRoadFacilityCalculatorUtil;
import com.stylefeng.guns.modular.assessment.calculate.aware.RoadnetThirdIndicationAware;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Date;
import java.util.List;

/**
 * 路网区域中心线路环境综合指数
 *
 * @author yinjc
 * @time 2018/05/11
 */
@Component
public class RoadRegionalCenterLineEnvironment extends ThirdIndicationCalculator implements RoadnetThirdIndicationAware {

    @Autowired
    private AreaRegionalCenterLineEnvironment areaRegionalCenterLineEnvironment;

    @Override
    protected Integer getIndicationId() {
        return 106;
    }

    @Override
    public Assessment calculate(Integer roadId, Date beginDate, Date endDate) {
        //路网中各区域中心强度
        List<Double> wList = getMulDoubleValue(roadId, 345, beginDate,endDate);
        double resVal = AreaRoadFacilityCalculatorUtil.calculate(roadId, areaRegionalCenterLineEnvironment, wList, beginDate,endDate);
        return genResult(resVal, roadId, beginDate,endDate);
    }
}
