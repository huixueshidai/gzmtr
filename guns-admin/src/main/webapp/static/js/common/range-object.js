(function() {
    var $Range = function (opts) {
        var settings = $.extend({}, $Range.defaults, opts);
        this.id = opts.id;
        this.v = settings.v;
        this.min = settings.min;
        this.max = settings.max;
        this.step = settings.step;
        this.grid = settings.grid;
        this.disable = settings.disable,
            this.onChange = settings.onChange;
        this.onUpdate = settings.onUpdate;
    }

    //默认配置
    $Range.defaults = {
        v:0,
        min:0,
        max:1,
        step:0.01,
        grid: true,
        disable:false,
        onChange:function () {

        },
        onUpdate:function () {

        }
    }

    $Range.prototype.init = function () {
        var $range = $("#" + this.id);
        var me = this;
        $range.ionRangeSlider({
            type: "single",
            min: this.min,
            max: this.max,
            from: this.v,
            step: this.step,
            grid:this.grid,
            disable:this.disable,
            onChange:function(data){
                me.onChange.call(null, data);
            },
            onUpdate:function (data) {
                me.onUpdate.call(null, data);
            },
        });
        return $range.data("ionRangeSlider");

    }

    window.$Range = $Range;
}());