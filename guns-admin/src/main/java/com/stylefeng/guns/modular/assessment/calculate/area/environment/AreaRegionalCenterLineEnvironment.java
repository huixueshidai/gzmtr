package com.stylefeng.guns.modular.assessment.calculate.area.environment;

import com.stylefeng.guns.common.persistence.model.Assessment;
import com.stylefeng.guns.modular.assessment.calculate.ThirdIndicationCalculator;
import com.stylefeng.guns.modular.assessment.calculate.area.facility.util.AreaRoadFacilityCalculatorUtil;
import com.stylefeng.guns.modular.assessment.calculate.aware.AreaThirdIndicationAware;
import com.stylefeng.guns.modular.assessment.calculate.line.station.LineEnvironmentCal;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Date;
import java.util.List;

/**
 * 区域中心线路环境综合指数
 *
 * @author yinjc
 * @time 2018/05/11
 */
@Component
public class AreaRegionalCenterLineEnvironment extends ThirdIndicationCalculator implements AreaThirdIndicationAware {

    @Autowired
    private LineEnvironmentCal lineEnvironmentCal;

    @Override
    protected Integer getIndicationId() {
        return 88;
    }

    @Override
    public Assessment calculate(Integer areaId, Date beginDate, Date endDate) {
        List<Double> w = getMulDoubleValue(areaId, 306, beginDate,endDate);
        Double resVal = AreaRoadFacilityCalculatorUtil.calculate(areaId, lineEnvironmentCal, w, beginDate,endDate);
        return genResult(resVal, areaId, beginDate,endDate);
    }
}
