package com.stylefeng.guns.modular.bus.service.impl;

import com.stylefeng.guns.common.persistence.model.HubBus;
import com.stylefeng.guns.common.persistence.dao.HubBusMapper;
import com.stylefeng.guns.modular.bus.service.IHubBusService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 公交站和车辆类型 服务实现类
 * </p>
 *
 * @author yinjc
 * @since 2018-03-21
 */
@Service
public class HubBusServiceImpl extends ServiceImpl<HubBusMapper, HubBus> implements IHubBusService {
	
}
