package com.stylefeng.guns.modular.assessment.calculate.area.facility;

import com.stylefeng.guns.common.persistence.model.Assessment;
import com.stylefeng.guns.modular.assessment.calculate.ThirdIndicationCalculator;
import com.stylefeng.guns.modular.assessment.calculate.area.facility.util.AreaRoadFacilityCalculatorUtil;
import com.stylefeng.guns.modular.assessment.calculate.aware.AreaThirdIndicationAware;
import com.stylefeng.guns.modular.assessment.calculate.line.facility.LineCivilEngineeringImpactCal;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Date;
import java.util.List;

/**
 * 土建系统影响运营风险指数
 *
 * @author yinjc
 * @since 2018/05/11
 */
@Component
public class AreaTuJianCal extends ThirdIndicationCalculator implements AreaThirdIndicationAware {

    @Autowired
    private LineCivilEngineeringImpactCal lineCivilEngineeringImpactCal;

    @Override
    protected Integer getIndicationId() {
        return 84;
    }

    @Override
    public Assessment calculate(Integer areaId, Date beginDate, Date endDate) {
        List<Double> w = getMulDoubleValue(areaId, 298, beginDate,endDate);
        Double resVal = AreaRoadFacilityCalculatorUtil.calculate(areaId, lineCivilEngineeringImpactCal, w, beginDate,endDate);
        return genResult(resVal, areaId, beginDate,endDate);
    }
}
