// 百度地图API功能

var PublicBusFeederRoute ={
    opts:null,
};
$(function(){
    var map = new BMap.Map("allmap");

    map.centerAndZoom(new BMap.Point(113.25863,23.128379), 16);

    var p1 = new BMap.Point(113.270793,23.131785);

    var lushu;
    var driving = new BMap.DrivingRoute(map,
        {
            renderOptions: {map: map, autoViewport: true},
            onSearchComplete: function (res) {

                if (driving.getStatus() == BMAP_STATUS_SUCCESS) {
                    var plan = res.getPlan(0);

                    //获取线路的坐标点
                    var arrPois = [];
                    for (var j = 0; j < plan.getNumRoutes(); j++) {
                        var route = plan.getRoute(j);
                        arrPois = arrPois.concat(route.getPath());    //行走路线
                    }

                    //获取途径站点的坐标
                    var tujingPois = plan.getDragPois();

                    var landmarkPois = tujingPois.map(function(poi){
                        return {
                            lng:poi.point.lng,lat:poi.point.lat,html:'等待乘客上车',pauseTime:2
                        }

                    });

                    map.addOverlay(new BMap.Polyline(arrPois, {strokeColor: '#337AB7'}));
                    map.setViewport(arrPois);

                    lushu = new BMapLib.LuShu(map, arrPois, {
                        defaultContent: "接驳公交车",
                        autoView: false,//是否开启自动视野调整，如果开启那么路书在运动过程中会根据视野自动调整
                        icon: new BMap.Icon('../static/img/car.png', new BMap.Size(52, 26), {anchor: new BMap.Size(27, 13)}),
                        speed: 500,
                        enableRotation: true,//是否设置marker随着道路的走向进行旋转
                        landmarkPois:landmarkPois
                    });

                    lushu.start();
                }
            },
            onMarkersSet:function(routes) {

                map.removeOverlay(routes[0].marker); //删除起点
                map.removeOverlay(routes[2].marker);
            }
        }
    );
    var data_info = [[113.25352,23.126657,"枢纽站名称："+$("#publicBusHub1_name").val()+"<br>可用驾驶人员："+$("#publicBusHub1_driverUserNum").val()+"<br>距各轨道车站距离："+$("#publicBusHub1_distance").val()],
        [113.25025,23.131476,"枢纽站名称："+$("#publicBusHub2_name").val()+"<br>可用驾驶人员："+$("#publicBusHub2_driverUserNum").val()+"<br>距各轨道车站距离："+$("#publicBusHub2_distance").val()],
        [113.261129,23.136901,"枢纽站名称："+$("#publicBusHub3_name").val()+"<br>可用驾驶人员："+$("#publicBusHub3_driverUserNum").val()+"<br>距各轨道车站距离："+$("#publicBusHub3_distance").val()],
            [113.26359,23.129349,"枢纽站名称："+$("#publicBusHub4_name").val()+"<br>可用驾驶人员："+$("#publicBusHub4_driverUserNum").val()+"<br>距各轨道车站距离："+$("#publicBusHub4_distance").val()]
    ];
    for(var i=0;i<data_info.length;i++){
        var marker = new BMap.Marker(new BMap.Point(data_info[i][0],data_info[i][1]));  // 创建标注
        var content = data_info[i][2];
        map.addOverlay(marker);               // 将标注添加到地图中
        PublicBusFeederRoute.addClickHandler(content,marker,map);
    }
    PublicBusFeederRoute.opts = {
        width : 200,     // 信息窗口宽度
        height: 150,     // 信息窗口高度
        title : "公交枢纽属性信息" , // 信息窗口标题
        enableMessage:false//设置允许信息窗发送短息
    };

    if($("#shigu").val()==1){
        //长寿路，陈家祠，西门口
        var point_info = [[113.248767,23.124108,$("#stationOne").val()],[113.252935,23.131619,$("#stationTwo").val()],[113.2626,23.13132,$("#stationThr").val()]];
        //冒泡排序
        for(var i = 0;i<point_info.length-1;i++){
            for(var j = 0;j<point_info.length-1;j++){
                if(point_info[j][2]<point_info[j+1][2]){
                    var temp = point_info[j];
                    point_info[j] = point_info[j+1];
                    point_info[j+1] = temp;
                }
            }
        }
        for(var i = 0;i<point_info.length;i++){
            if(i==0){
                var myIcon = new BMap.Icon("../static/img/red.png", new BMap.Size(39,25));
            }
            if(i==1){
                var myIcon = new BMap.Icon("../static/img/marker_yellow_sprite.png", new BMap.Size(39,25));
            }
            if(i==2){
                var myIcon = new BMap.Icon("../static/img/marker_blue_sprite.png", new BMap.Size(39,25));
            }
            var marker = new BMap.Marker(new BMap.Point(point_info[i][0],point_info[i][1]),{icon:myIcon});  // 创建标注
            map.addOverlay(marker);               // 将标注添加到地图中
            // marker.setAnimation(BMAP_ANIMATION_BOUNCE); //跳动的动画
        }
        if(start == "true"){
            driving.search('长寿路地铁站-B口', '西门口地铁站A口',{waypoints:["陈家祠地铁站A口"]});
            driving.search('西门口地铁站', '长寿路地铁站',{waypoints:["陈家祠地铁站C口"]});
        }

    }

    if($("#shigu").val()==2){
        //陈家祠，西门口，公园前
        var point_info = [[113.252935,23.131619,$("#stationOne").val()],[113.2626,23.13132,$("#stationTwo").val()],[113.270865,23.131652,$("#stationThr").val()]];
        //冒泡排序
        for(var i = 0;i<point_info.length-1;i++){
            for(var j = 0;j<point_info.length-1-i;j++){
                if(point_info[j][2]<point_info[j+1][2]){
                    var temp = point_info[j];
                    point_info[j] = point_info[j+1];
                    point_info[j+1] = temp;
                }
            }
        }
        for(var i = 0;i<point_info.length;i++){
            if(i==0){
                var myIcon = new BMap.Icon("../static/img/marker_red_sprite.png", new BMap.Size(39,25));
            }
            if(i==1){
                var myIcon = new BMap.Icon("../static/img/marker_yellow_sprite.png", new BMap.Size(39,25));
            }
            if(i==2){
                var myIcon = new BMap.Icon("../static/img/marker_blue_sprite.png", new BMap.Size(39,25));
            }
            var marker = new BMap.Marker(new BMap.Point(point_info[i][0],point_info[i][1]),{icon:myIcon});  // 创建标注
            map.addOverlay(marker);               // 将标注添加到地图中
            marker.setAnimation(BMAP_ANIMATION_BOUNCE); //跳动的动画
        }
        if(start == "true"){
            driving.search('陈家祠地铁站A口', '公园前地铁站-C口',{waypoints:["西门口地铁站A口"]});
            driving.search(p1, '陈家祠地铁站D口',{waypoints:["西门口地铁站D"]});
        }

    }


});
PublicBusFeederRoute.addClickHandler = function(content,marker,map){
    marker.addEventListener("click",function(e){
        PublicBusFeederRoute.openInfo(content,e,map)}
    );
}

PublicBusFeederRoute.openInfo = function(content,e,map) {
    var p = e.target;
    var point = new BMap.Point(p.getPosition().lng, p.getPosition().lat);
    var infoWindown = new BMap.InfoWindow(content, PublicBusFeederRoute.opts);
    map.openInfoWindow(infoWindown, point);
}

PublicBusFeederRoute.stop = function(){
    var ajax = new $ax(Feng.ctxPath + "/publicBusHub/stop", function (data) {

    }, function (data) {

    });
    ajax.start();
    location.reload([true])
}


/**
 * 打开启动公交应急联动
 *
 */
PublicBusFeederRoute.openPublicBus = function () {

    var accidentStationId = $("#accidentStationId").val();
            var index = layer.open({
                type: 2,
                title: '公交应急联动',
                area: ['1200px', '600px'], //宽高
                fix: false, //不固定
                maxmin: false,
                content: Feng.ctxPath + '/publicBusHub/Me/'+accidentStationId
            });
            this.layerIndex = index;


}