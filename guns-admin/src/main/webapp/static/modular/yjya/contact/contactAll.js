/**
 * 通讯录管理初始化
 */
var Contact = {
    id: "ContactTable",	//表格id
    seItem: null,		//选中的条目
    table: null,
    layerIndex: -1,
    groupId:0
};

/**
 * 初始化表格的列
 */
Contact.initColumn = function () {
    return [
        {field: 'selectItem', checkbox: true},
        {title: '名称', field: 'name', visible: true, align: 'center', valign: 'middle'},
        {title: '职务/专业', field: 'duty', visible: true, align: 'center', valign: 'middle'},
        {title: '行政职务', field: 'administrationDuty', visible: true, align: 'center', valign: 'middle'},
        {title: '座机', field: 'phone', visible: true, align: 'center', valign: 'middle'},
        {title: '手机号', field: 'mobilePhone', visible: true, align: 'center', valign: 'middle'},
        {title: '邮箱', field: 'email', visible: true, align: 'center', valign: 'middle'},
        {title: '群组', field: 'groupName', visible: true, align: 'center', valign: 'middle'}
    ];
};

/**
 * 检查是否选中
 */
Contact.check = function () {
    var selected = $('#' + this.id).bootstrapTable('getSelections');
    if(selected.length == 0){
        Feng.info("请至少选择一条通讯信息！");
        return false;
    }else{
        Contact.seItem = selected[0];
        return true;
    }
};

/**
 * 发送信息
 */
Contact.send = function(){
    if(this.check()){
        var index = layer.open({
            type: 2,
            title: '汇报上级',
            area: ['600px', '300px'], //宽高
            fix: false, //不固定
            maxmin: false,
            content: Feng.ctxPath + '/contact/editor_info'
        });
        this.layerIndex = index;
    }
}

Contact.submit = function(){
    Feng.success("汇报成功!");
    parent.layer.close(window.parent.Contact.layerIndex);
}

Contact.close = function(){
    parent.layer.close(window.parent.Contact.layerIndex);
}

/**
 * 查询通讯录列表
 */
Contact.search = function () {
    var queryData = {};
    queryData['groupId'] = Contact.groupId;
    queryData['name'] = $("#name").val();
    Contact.table.refresh({query: queryData});
};

Contact.onClickContactGroup = function (e, treeId, treeNode) {
    Contact.groupId = treeNode.id;
    Contact.search();
}

$(function () {
    var defaultColunms = Contact.initColumn();
    var table = new BSTable(Contact.id, "/contact/list", defaultColunms);
    table.setPaginationType("server");
    Contact.table = table.init();

    var ztree = new $ZTree("contactGroupTree", "/contactGroup/tree");
    ztree.bindOnClick(Contact.onClickContactGroup);
    ztree.init();
});
