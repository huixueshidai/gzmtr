package com.stylefeng.guns.modular.yjya.service.impl;

import com.stylefeng.guns.common.persistence.model.AccidentDisposaltask;
import com.stylefeng.guns.common.persistence.dao.AccidentDisposaltaskMapper;
import com.stylefeng.guns.modular.yjya.service.IAccidentDisposaltaskService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author wangshuaikang
 * @since 2018-09-11
 */
@Service
public class AccidentDisposaltaskServiceImpl extends ServiceImpl<AccidentDisposaltaskMapper, AccidentDisposaltask> implements IAccidentDisposaltaskService {
	
}
