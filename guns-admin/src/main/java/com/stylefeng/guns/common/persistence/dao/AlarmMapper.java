package com.stylefeng.guns.common.persistence.dao;

import com.stylefeng.guns.common.persistence.model.Alarm;
import com.baomidou.mybatisplus.mapper.BaseMapper;
import org.apache.ibatis.annotations.Param;

/**
 * <p>
  *  Mapper 接口
 * </p>
 *
 * @author wsk
 * @since 2018-07-10
 */
public interface AlarmMapper extends BaseMapper<Alarm> {

    Alarm selectOneAlarm();

    Alarm selectByUserId(@Param("userId") Integer userId);
}