/**
 * Created by 60132 on 2018/5/14.
 */
var ResourceSel = {
    table: null,
    id: "ResourceSelTable",
    seItems: null,
}


ResourceSel.initColumn = function () {
    return [
        {field: 'selectItem', checkbox: true},
        {title: 'id', field: 'id', visible: true, align: 'center', valign: 'middle'},
        {title: '名称', field: 'name', visible: true, align: 'center', valign: 'middle'},
        {title: '是否是大型物资', field: 'largeScale', visible: true, align: 'center', valign: 'middle'},
        {title: '型号', field: 'modelNum', visible: true, align: 'center', valign: 'middle'},
        {title: '专业用途', field: 'fist', visible: true, align: 'center', valign: 'middle'},
        {title: '现有数量', field: 'num', visible: true, align: 'center', valign: 'middle'},
        {title: '单位', field: 'unit', visible: true, align: 'center', valign: 'middle'},
        {title: '线别', field: 'line', visible: true, align: 'center', valign: 'middle'},
        {title: '放置地点', field: 'location', visible: true, align: 'center', valign: 'middle'},
        {title: '地点备注', field: 'remark', visible: true, align: 'center', valign: 'middle'},
        {title: '中心', field: 'center', visible: true, align: 'center', valign: 'middle'},
        {title: '使用部门', field: 'usedp', visible: true, align: 'center', valign: 'middle'},
        {title: '维护部门', field: 'maintaindp', visible: true, align: 'center', valign: 'middle'},
        {title: '更新时间', field: 'updateTime', visible: true, align: 'center', valign: 'middle'},
        {title: '状态', field: 'state', visible: true, align: 'center', valign: 'middle'}
    ];
};

/**
 * 检查是否选中
 */
ResourceSel.check = function () {
    var selected = $('#' + this.id).bootstrapTable('getSelections');
    if (selected.length == 0) {
        Feng.info("请先选中表格中的某一记录！");
        return false;
    } else {
        ResourceSel.seItems = selected;
        return true;
    }
};

ResourceSel.close = function () {
    parent.layer.close(window.parent.CounterplanInfoDlgEdit.layerIndex);
}

ResourceSel.addResources = function () {
    // if (this.check()) {
    //     var items = this.seItems;
    //     var resources = "";
    //     for(i in items) {
    //         resources += "《"+items[i].name+"》<br/>";
    //     }
    //     window.parent.CounterplanInfoDlgEdit.ue.execCommand('inserthtml', resources);
    //     ResourceSel.close();
    // }
    if (this.check()) {
        var items = this.seItems;
        console.table(items);
        var table =
            "<table>" +
            "    <tbody>" +
            "        <tr class=\"firstRow\">" +
            "            <td width=\"199\" valign=\"top\">名称</td>" +
            "            <td width=\"199\" valign=\"top\">是否是大型物资</td>" +
            "            <td width=\"199\" valign=\"top\">型号 </td>" +
            "            <td width=\"199\" valign=\"top\">专业用途</td>" +
            "            <td width=\"199\" valign=\"top\">现有数量</td>" +
            "            <td width=\"199\" valign=\"top\">单位</td>" +
            "            <td width=\"199\" valign=\"top\">线别</td>" +
            "            <td width=\"199\" valign=\"top\">放置地点</td>" +
            "            <td width=\"199\" valign=\"top\">地点备注</td>" +
            "            <td width=\"199\" valign=\"top\">中心</td>" +
            "            <td width=\"199\" valign=\"top\">使用部门</td>" +
            "            <td width=\"199\" valign=\"top\">维护部门</td>" +
            "            <td width=\"199\" valign=\"top\">更新时间</td>" +
            "            <td width=\"199\" valign=\"top\">状态</td>" +
            "        </tr>";
        for (x in items) {

            table +=
                "<tr>" +
                "<td width=\"199\" valign=\"top\">" + items[x].name + "</td>" +
                "<td width=\"199\" valign=\"top\">" + items[x].largeScale + "</td>" +
                "<td width=\"199\" valign=\"top\">" + items[x].modelNum + "</td>" +
                "<td width=\"199\" valign=\"top\">" + items[x].fist + "</td>" +
                "<td width=\"199\" valign=\"top\">" + items[x].num + "</td>" +
                "<td width=\"199\" valign=\"top\">" + items[x].unit + "</td>" +
                "<td width=\"199\" valign=\"top\">" + items[x].line + "</td>" +
                "<td width=\"199\" valign=\"top\">" + items[x].location + "</td>" +
                "<td width=\"199\" valign=\"top\">" + items[x].remark + "</td>" +
                "<td width=\"199\" valign=\"top\">" + items[x].center + "</td>" +
                "<td width=\"199\" valign=\"top\">" + items[x].usedp + "</td>" +
                "<td width=\"199\" valign=\"top\">" + items[x].maintaindp + "</td>" +
                "<td width=\"199\" valign=\"top\">" + items[x].updateTime + "</td>" +
                "<td width=\"199\" valign=\"top\">" + items[x].state + "</td>" +
                "</tr>";

        }
        table +=
            "     </tbody>" +
            "</table>";

        window.parent.CounterplanInfoDlgEdit.ue.execCommand('inserthtml', table);
        ResourceSel.close();
    }

}

ResourceSel.search = function(){

    var queryData = {};
    queryData['condition'] = $("#condition").val();
    queryData['location'] = $("#location").val();
    ResourceSel.table.refresh({query: queryData});
}

ResourceSel.resetSearch = function(){
    $("#condition").val("");
    $("#location").val("");
}

$(function () {
    var defaultColunms = ResourceSel.initColumn();
    var table = new BSTable(ResourceSel.id, "/resource/list", defaultColunms);
    table.setPaginationType("client");
    ResourceSel.table = table.init();
});