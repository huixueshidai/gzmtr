package com.stylefeng.guns.modular.prediction.dao;

import com.stylefeng.guns.common.persistence.model.RiskFactor;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

public interface RiskFactorDao {
    List<Map<String, Object>> selectRiskFactors(@Param("name") String name);

    String selectNameById(@Param("id") Integer id);

    List<RiskFactor> selectChildren(@Param("pid")Integer  pid);
}
