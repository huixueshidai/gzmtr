package com.stylefeng.guns.modular.assessment.calculate.line;

import com.stylefeng.guns.core.util.SpringContextHolder;
import com.stylefeng.guns.modular.assessment.calculate.FirstIndicationCalculator;
import com.stylefeng.guns.modular.assessment.calculate.SecondIndicationCalculator;
import com.stylefeng.guns.modular.assessment.calculate.line.accident.LineAccidentCal;
import com.stylefeng.guns.modular.assessment.calculate.line.employee.LineEmployeeCal;
import com.stylefeng.guns.modular.assessment.calculate.line.facility.LineFacilityCal;
import com.stylefeng.guns.modular.assessment.calculate.line.manage.LineManageCal;
import com.stylefeng.guns.modular.assessment.calculate.line.passenger.LinePassengerCal;
import com.stylefeng.guns.modular.assessment.calculate.line.station.LineStationCal;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import java.util.Arrays;
import java.util.List;

@Component
@Scope(ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public class LineCalculator extends FirstIndicationCalculator {

    private List<SecondIndicationCalculator> secondIndicationCalculates;

    public LineCalculator() {
        secondIndicationCalculates = Arrays.asList(
                SpringContextHolder.getBean(LineAccidentCal.class),
                SpringContextHolder.getBean(LineEmployeeCal.class),
                SpringContextHolder.getBean(LineFacilityCal.class),
                SpringContextHolder.getBean(LineManageCal.class),
                SpringContextHolder.getBean(LinePassengerCal.class),
                SpringContextHolder.getBean(LineStationCal.class)
        );
    }

    @Override
    public List<SecondIndicationCalculator> getSecondIndicationCalculateList() {
        return secondIndicationCalculates;
    }


    @Override
    protected Integer getIndicationId() {
        return 5;
    }
}
