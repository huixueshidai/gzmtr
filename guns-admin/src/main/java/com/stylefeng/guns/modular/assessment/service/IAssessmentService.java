package com.stylefeng.guns.modular.assessment.service;

import com.baomidou.mybatisplus.service.IService;
import com.stylefeng.guns.common.persistence.model.Assessment;
import com.stylefeng.guns.common.plugin.entity.Period;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.List;

/**
 * <p>
 * 指标评估 服务类
 * </p>
 *
 * @author xuziyang
 * @since 2018-03-27
 */
public interface IAssessmentService extends IService<Assessment> {

	List<Assessment> assessmentMulPeriod(Integer roadnetId, List<Period> periodList);

	Assessment assessmentOnePeriod(Integer roadnetId,Date beginDate,Date endDate);

	Assessment selectOne(Integer indicationId, Integer roadnetId, String beginTime, String endTime);

}
