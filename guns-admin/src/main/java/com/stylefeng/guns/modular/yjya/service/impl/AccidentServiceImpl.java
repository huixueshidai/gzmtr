package com.stylefeng.guns.modular.yjya.service.impl;

import com.stylefeng.guns.common.persistence.model.Accident;
import com.stylefeng.guns.common.persistence.dao.AccidentMapper;
import com.stylefeng.guns.modular.yjya.service.IAccidentService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 事故实例 服务实现类
 * </p>
 *
 * @author xuziyang
 * @since 2018-01-22
 */
@Service
public class AccidentServiceImpl extends ServiceImpl<AccidentMapper, Accident> implements IAccidentService {

}

