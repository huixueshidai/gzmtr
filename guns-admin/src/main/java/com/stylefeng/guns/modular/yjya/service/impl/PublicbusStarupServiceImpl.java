package com.stylefeng.guns.modular.yjya.service.impl;

import com.stylefeng.guns.common.persistence.model.PublicbusStarup;
import com.stylefeng.guns.common.persistence.dao.PublicbusStarupMapper;
import com.stylefeng.guns.modular.yjya.service.IPublicbusStarupService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author wangshuaikang
 * @since 2018-09-09
 */
@Service
public class PublicbusStarupServiceImpl extends ServiceImpl<PublicbusStarupMapper, PublicbusStarup> implements IPublicbusStarupService {
	
}
