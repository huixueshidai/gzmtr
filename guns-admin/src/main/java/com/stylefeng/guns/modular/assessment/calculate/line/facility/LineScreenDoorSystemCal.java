package com.stylefeng.guns.modular.assessment.calculate.line.facility;

import com.stylefeng.guns.common.persistence.model.Assessment;
import com.stylefeng.guns.core.util.DoubleUtil;
import com.stylefeng.guns.modular.assessment.calculate.ThirdIndicationCalculator;
import com.stylefeng.guns.modular.assessment.calculate.aware.LineThirdIndicationAware;
import org.springframework.stereotype.Component;

import java.util.Date;
import java.util.List;

/**
 * @author YinJiaCheng
 * @Description: 屏蔽门系统影响运营风险指数
 */
@Component
public class LineScreenDoorSystemCal extends ThirdIndicationCalculator implements LineThirdIndicationAware {
    @Override
    protected Integer getIndicationId() {
        return 66;
    }

    @Override
    public Assessment calculate(Integer lineId, Date beginDate, Date endDate) {
        //安全门故障次数
        List<Double> numOfFailure = getMulDoubleValue(lineId, 235, beginDate,endDate);
        //安全门计划正常开启次数
        List<Double> planNum = getMulDoubleValue(lineId, 236, beginDate,endDate);
        //安全门总数
        List<Double> k = getMulDoubleValue(lineId, 237, beginDate,endDate);
        //安全门的基于故障率的权重系数
        List<Double> a = getMulDoubleValue(lineId, 238, beginDate,endDate);
        //线路中车站总数
        Double m = getDoubleValue(lineId, 239, beginDate,endDate);

        Integer size = Math.min(numOfFailure.size(), planNum.size());
        size = Math.min(size, k.size());
        Integer minsize = Math.min(size, a.size());
        Double pU = 0D;
        Double pS = 0D;

        for (int i = 0; i < minsize; i++) {

            Double pi = DoubleUtil.safeDiv(numOfFailure.get(i), planNum.get(i));
            pU += DoubleUtil.mul(a.get(i), pi);
            pS += DoubleUtil.safeDiv(pU, m);
        }
        return genResult(pS, lineId, beginDate,endDate);
    }
}
