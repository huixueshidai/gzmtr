package com.stylefeng.guns.modular.yjya.warpper;

import com.stylefeng.guns.common.constant.factory.ConstantFactory;
import com.stylefeng.guns.core.base.warpper.BaseControllerWarpper;
import org.apache.commons.collections.MapUtils;

import java.util.Map;

/**
 * Created by 60132 on 2018/9/30.
 */
public class PassengerFileWarpper extends BaseControllerWarpper {
    public PassengerFileWarpper(Object obj) {
        super(obj);
    }

    @Override
    protected void warpTheMap(Map<String, Object> map) {
        if(map.containsKey("dateStatus")){
            map.put("dateStatusName", ConstantFactory.me().getDictNameByCode("passenger_type", MapUtils.getString(map, "dateStatus")));
        }

    }
}
