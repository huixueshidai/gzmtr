package com.stylefeng.guns.modular.path.warpper;

import com.stylefeng.guns.common.constant.factory.ConstantFactory;
import com.stylefeng.guns.core.base.warpper.BaseControllerWarpper;
import org.apache.commons.collections.MapUtils;

import java.util.Map;

public class pathEvolutionWarpper extends BaseControllerWarpper{


    public pathEvolutionWarpper(Object obj) {
        super(obj);
    }

    @Override
    protected void warpTheMap(Map<String, Object> map) {
        map.put("field", ConstantFactory.me().getDictNameByCode("fieldType", MapUtils.getString(map,"field")));
        map.put("trapped", ConstantFactory.me().getDictNameByCode("beTrapped", (String) map.get("trapped")));

        map.put("material", ConstantFactory.me().getDictNameByCode("materialType", (String) map.get("material")));
        map.put("rescuers", ConstantFactory.me().getDictNameByCode("rescuersType", (String) map.get("rescuers")));
        map.put("FireSourceState", ConstantFactory.me().getDictNameByCode("fireSourceState", (String) map.get("FireSourceState")));
        map.put("dischargeRate", ConstantFactory.me().getDictNameByCode("dischargeRate", (String) map.get("dischargeRate")));

        return;
    }
}
