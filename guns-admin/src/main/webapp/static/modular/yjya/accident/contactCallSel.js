/**
 * 通讯录管理初始化
 */
var ContactCall = {
    id: "ContactTable",	//表格id
    seItem: null,		//选中的条目
    table: null,
    layerIndex: -1,
    groupId:0,
    uploadInstance:null
};

/**
 * 初始化表格的列
 */
ContactCall.initColumn = function () {
    return [
        {field: 'selectItem', radio: true},
        {title: 'id', field: 'id', visible: true, align: 'center', valign: 'middle'},
        {title: '姓名', field: 'name', visible: true, align: 'center', valign: 'middle'},
        {title: '职务/专业', field: 'duty', visible: true, align: 'center', valign: 'middle'},
        {title: '行政职务', field: 'administrationDuty', visible: true, align: 'center', valign: 'middle'},
        {title: '座机', field: 'phone', visible: true, align: 'center', valign: 'middle'},
        {title: '手机号', field: 'mobilePhone', visible: true, align: 'center', valign: 'middle'},
        {title: '邮箱', field: 'email', visible: true, align: 'center', valign: 'middle'},
        {title: '群组', field: 'groupName', visible: true, align: 'center', valign: 'middle'}
    ];
};

/**
 * 检查是否选中
 */
ContactCall.check = function () {
    var selected = $('#' + this.id).bootstrapTable('getSelections');
    var selects = new Array();
    if(selected.length == 0){
        return false;
    }else{
        ContactCall.seItem = selected[0];
        return true;
    }
};


ContactCall.resetSearch = function () {
    $("#name").val("");
    $("#administrationDuty").val("");
    // Contact.search();

}

/**
 * 查询通讯录列表
 */
ContactCall.search = function () {
    var queryData = {};

    queryData['groupId'] = ContactCall.groupId;
    queryData['name'] = $("#name").val();
    queryData['administrationDuty'] = $("#administrationDuty").val();
    ContactCall.table.refresh({query: queryData});
};

ContactCall.onClickContactGroup = function (e, treeId, treeNode) {
    ContactCall.groupId = treeNode.id;
    ContactCall.search();
}

ContactCall.exportXlsByM = function () {
    window.location.href = Feng.ctxPath + "/contact/exportXlsByM";
}

ContactCall.upload = function () {
    this.uploadInstance
}

/**
 * 添加选择的通讯录人员
 */
ContactCall.addToReport = function(){
    if (this.check()) {
        var index = layer.open({
            type: 2,
            title: '电话',
            area: ['320px', '400px'], //宽高
            fix: false, //不固定
            maxmin: false,
            content: Feng.ctxPath + '/contact/contactTelphone?tel='+ContactCall.seItem.mobilePhone
        });
        this.layerIndex = index;
    }
    else{
        var index = layer.open({
            type: 2,
            title: '电话',
            area: ['320px', '400px'], //宽高
            fix: false, //不固定
            maxmin: false,
            content: Feng.ctxPath + '/contact/contactTelphone?tel='+""
        });
        this.layerIndex = index;
    }
}

$(function () {
    var defaultColunms = ContactCall.initColumn();
    var table = new BSTable(ContactCall.id, "/contact/reportContactSel", defaultColunms);
    table.setPaginationType("server");
    ContactCall.table = table.init();

    var ztree = new $ZTree("contactGroupTree", "/contactGroup/tree");
    ztree.bindOnClick(ContactCall.onClickContactGroup);
    ztree.init();

    var upload = new $WebUpload({
        uploadId:"upload",
        mimeTypes:"application/vnd.ms-excel",
        extensions:"xls",
        url:"/contact/importExcel",
        multiple:true
    });
    upload.init();
    ContactCall.uploadInstance = upload;
});

