package com.stylefeng.guns.modular.data.service;

import com.baomidou.mybatisplus.service.IService;
import com.stylefeng.guns.common.persistence.model.ResourceDataType;
import com.stylefeng.guns.core.node.ZTreeNode;

import java.util.List;

/**
 * <p>
 * 源数据类型 服务类
 * </p>
 *
 * @author xuziyang
 * @since 2018-01-15
 */
public interface IResourceDataTypeService extends IService<ResourceDataType> {

    List<ZTreeNode> tree();

    ResourceDataType selectByName(String name);
}
