package com.stylefeng.guns.test;

import org.apache.poi.util.SystemOutLogger;
import org.springframework.core.io.ClassPathResource;
import org.w3c.dom.Document;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import java.io.IOException;

/**
 * Created by 60132 on 2018/11/1.
 */
public class uplloadVal {

    public static void main(String [] ags){
        DocumentBuilderFactory a = DocumentBuilderFactory.newInstance();
        try {
            //创建DocumentBuilder对象
            DocumentBuilder b = a.newDocumentBuilder();
            //通过DocumentBuilder对象的parse方法返回一个Document对象
            ClassPathResource classPathResource = new ClassPathResource("upload.xml");
            String path = classPathResource.getFile().getPath();
            System.out.println("===="+path);
            System.out.print("----------"+classPathResource.getInputStream());
            Document document = b.parse(classPathResource.getInputStream());
            //通过Document对象的getElementsByTagName()返根节点的一个list集合
            NodeList bookList = document.getElementsByTagName("book");
            for(int i =0; i<bookList.getLength(); i++){
                System.out.println("========开始遍历第"+(i+1)+"本书========");

                //获取id的属性值
                System.out.println("id:"+document.getElementsByTagName("id").item(i).getFirstChild().getNodeValue());
                //获取name的属性值
                System.out.println("name:"+document.getElementsByTagName("name").item(i).getFirstChild().getNodeValue());
                //获取author的属性值
                System.out.println("author:"+document.getElementsByTagName("author").item(i).getFirstChild().getNodeValue());
                //获取year的属性值
                System.out.println("year:"+document.getElementsByTagName("year").item(i).getFirstChild().getNodeValue());
                //获取price的属性值
                System.out.println("price:"+document.getElementsByTagName("price").item(i).getFirstChild().getNodeValue());

                System.out.println("========结束遍历第"+(i+1)+"本书========");
            }
        } catch (ParserConfigurationException e) {
            e.printStackTrace();
        } catch (SAXException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }
}
