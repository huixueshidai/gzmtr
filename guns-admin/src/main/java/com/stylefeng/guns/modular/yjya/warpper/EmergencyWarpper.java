package com.stylefeng.guns.modular.yjya.warpper;

import com.stylefeng.guns.core.base.warpper.BaseControllerWarpper;
import com.stylefeng.guns.core.util.SpringContextHolder;
import com.stylefeng.guns.core.util.ToolUtil;
import com.stylefeng.guns.modular.yjya.dao.ContactGroupDao;
import com.stylefeng.guns.modular.yjya.dao.EmergencyDao;

import java.util.Map;

/**
 * Created by 60132 on 2018/5/24.
 */
public class EmergencyWarpper extends BaseControllerWarpper {
    public EmergencyWarpper(Object obj) {
        super(obj);
    }

    @Override
    protected void warpTheMap(Map<String, Object> map) {
        Integer pid = (Integer) map.get("pid");
        if (ToolUtil.isEmpty(pid) || pid.equals(0)) {
            map.put("pName", "--");
        } else {
            EmergencyDao emergenyDao  = SpringContextHolder.getBean(EmergencyDao.class);
            map.put("pName", emergenyDao.getNameById(pid));
        }
        return;
    }
}
