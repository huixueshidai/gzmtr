package com.stylefeng.guns.common.persistence.model;

import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableName;
import com.stylefeng.guns.common.constant.factory.ConstantFactory;

import java.io.Serializable;

/**
 * <p>
 * 预案处置流程表
 * </p>
 *
 * @author xuziyang
 * @since 2018-03-01
 */
@TableName("gzmtr_counterplan_process")
public class CounterplanProcess extends Model<CounterplanProcess> {

    private static final long serialVersionUID = 1L;


    private Integer id;
    /**
     * 预案id
     */
    @TableField("counterplan_id")
    private Integer counterplanId;
    private String main;
    /**
     * 主体属性
     */
    private String mproperty;
    /**
     * 主题动作
     */
    private String mmotion;
    /**
     * 客体
     */
    private String object;
    /**
     * 客体属性
     */
    private String oproperty;
    /**
     * 客体动作
     */
    private String omotion;
    /**
     * 处置步骤
     */
    private Integer num;


    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getCounterplanId() {
        return counterplanId;
    }

    public void setCounterplanId(Integer counterplanId) {
        this.counterplanId = counterplanId;
    }

    public String getMain() {
        return main;
    }

    public void setMain(String main) {
        this.main = main;
    }

    public String getMproperty() {
        return mproperty;
    }

    public void setMproperty(String mproperty) {
        this.mproperty = mproperty;
    }

    public String getMmotion() {
        return mmotion;
    }

    public void setMmotion(String mmotion) {
        this.mmotion = mmotion;
    }

    public String getObject() {
        return object;
    }

    public void setObject(String object) {
        this.object = object;
    }

    public String getOproperty() {
        return oproperty;
    }

    public void setOproperty(String oproperty) {
        this.oproperty = oproperty;
    }

    public String getOmotion() {
        return omotion;
    }

    public void setOmotion(String omotion) {
        this.omotion = omotion;
    }


    public Integer getNum() {
        return num;
    }

    public void setNum(Integer num) {
        this.num = num;
    }

    @Override
    protected Serializable pkVal() {
        return this.id;
    }

    @Override
    public String toString() {

        return main + mproperty + mmotion + object + oproperty + omotion;
    }
}
