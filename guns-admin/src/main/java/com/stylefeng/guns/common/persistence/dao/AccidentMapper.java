package com.stylefeng.guns.common.persistence.dao;

import com.stylefeng.guns.common.persistence.model.Accident;
import com.baomidou.mybatisplus.mapper.BaseMapper;

/**
 * <p>
  * 事故实例 Mapper 接口
 * </p>
 *
 * @author xuziyang
 * @since 2018-01-22
 */
public interface AccidentMapper extends BaseMapper<Accident> {

}

