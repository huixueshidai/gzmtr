package com.stylefeng.guns.modular.yjya.service;

import com.stylefeng.guns.common.persistence.model.AccidentType;
import com.baomidou.mybatisplus.service.IService;
import com.stylefeng.guns.common.plugin.service.IBaseTreeableService;

/**
 * <p>
 * 事故处理措施 服务类
 * </p>
 *
 * @author 殷佳成
 * @since 2018-01-11
 */
public interface IAccidentTypeService extends IBaseTreeableService<AccidentType> {


	
}
