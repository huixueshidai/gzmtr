package com.stylefeng.guns.modular.yjya.service;

import com.baomidou.mybatisplus.service.IService;
import com.stylefeng.guns.common.persistence.model.CounterplanProcess;

import java.util.List;

/**
 * <p>
 * 预案处置流程表 服务类
 * </p>
 *
 * @author xuziyang
 * @since 2018-03-01
 */
public interface ICounterplanProcessService extends IService<CounterplanProcess> {
    void insert(Integer counterplanId, String[] accidentIds);

    void update(Integer counterplanId, String[] accidentIds);

    void deleteByCounterplanId(Integer counterplanId);

    List<List<String>> selectProcesses(Integer counterplanId);
}
