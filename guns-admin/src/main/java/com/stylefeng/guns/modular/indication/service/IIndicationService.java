package com.stylefeng.guns.modular.indication.service;

import com.stylefeng.guns.common.persistence.model.Indication;
import com.stylefeng.guns.common.plugin.service.IBaseTreeableService;

import java.util.List;

/**
 * <p>
 * 评价指标体系 服务类
 * </p>
 *
 * @author xuziyang
 * @since 2018-01-08
 */
public interface IIndicationService extends IBaseTreeableService<Indication> {
    List<Indication> selectByPid(Integer pid);

    void updateWeight(List<Indication> indications);
}
