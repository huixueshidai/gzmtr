package com.stylefeng.guns.modular.assessment.calculate.area.facility;

import com.stylefeng.guns.common.persistence.model.Assessment;
import com.stylefeng.guns.modular.assessment.calculate.ThirdIndicationCalculator;
import com.stylefeng.guns.modular.assessment.calculate.area.facility.util.AreaRoadFacilityCalculatorUtil;
import com.stylefeng.guns.modular.assessment.calculate.aware.AreaThirdIndicationAware;
import com.stylefeng.guns.modular.assessment.calculate.line.facility.LineVehicleSystemCal;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Date;
import java.util.List;

/**
 * 车辆系统影响运营风险指数
 *
 * @author yinjc
 * @since 2018/05/11
 */
@Component
public class AreaLpactOfVehicleSystemCal extends ThirdIndicationCalculator implements AreaThirdIndicationAware {

    @Autowired
    private LineVehicleSystemCal lineVehicleSystemCal;

    @Override
    protected Integer getIndicationId() {
        return 79;
    }

    @Override
    public Assessment calculate(Integer areaId, Date beginDate, Date endDate) {
        //区域中心中各线的线路强度(车辆)
        List<Double> w = getMulDoubleValue(areaId, 288, beginDate,endDate);
        Double resVal = AreaRoadFacilityCalculatorUtil.calculate(areaId, lineVehicleSystemCal, w, beginDate,endDate);
        return genResult(resVal, areaId, beginDate,endDate);
    }
}
