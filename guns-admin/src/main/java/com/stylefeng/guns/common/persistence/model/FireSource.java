package com.stylefeng.guns.common.persistence.model;

import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;
import com.baomidou.mybatisplus.enums.IdType;

import java.io.Serializable;

/**
 * <p>
 * 火源情景预算
 * </p>
 *
 * @author xhq
 * @since 2018-09-03
 */
@TableName("gzmtr_fire_source")
public class FireSource extends Model<FireSource> {

    private static final long serialVersionUID = 1L;

	@TableId(value="id", type= IdType.AUTO)
	private Integer id;
    /**
     * 材料
     */
	private String material;
    /**
     * 场地
     */
	private String field;
    /**
     * 救援人员
     */
	private String rescuers;
    /**
     * 专家�?
     */
	private String expert1;
    /**
     * 专家�?
     */
	private String expert2;
    /**
     * 专家�?
     */
	private String expert3;
    /**
     * 结果
     */
	private String result;

	@TableField("rescuers_start")
	private String rescuersStart;

	@TableField("rescuers_end")
	private String rescuersEnd;

	@TableField("expert1T")
	private String expert1T;

	@TableField("expert1F")
	private String expert1F;

	@TableField("expert2T")
	private String expert2T;

	@TableField("expert2F")
	private String expert2F;

	@TableField("expert3T")
	private String expert3T;

	@TableField("expert3F")
	private String expert3F;



	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getMaterial() {
		return material;
	}

	public void setMaterial(String material) {
		this.material = material;
	}

	public String getField() {
		return field;
	}

	public void setField(String field) {
		this.field = field;
	}

	public String getRescuers() {
		return rescuers;
	}

	public void setRescuers(String rescuers) {
		this.rescuers = rescuers;
	}

	public String getExpert1() {
		return expert1;
	}

	public void setExpert1(String expert1) {
		this.expert1 = expert1;
	}

	public String getExpert2() {
		return expert2;
	}

	public void setExpert2(String expert2) {
		this.expert2 = expert2;
	}

	public String getExpert3() {
		return expert3;
	}

	public void setExpert3(String expert3) {
		this.expert3 = expert3;
	}

	public String getResult() {
		return result;
	}

	public void setResult(String result) {
		this.result = result;
	}

	public String getRescuersEnd() {
		return rescuersEnd;
	}

	public void setRescuersEnd(String rescuersEnd) {
		this.rescuersEnd = rescuersEnd;
	}

	public String getExpert1T() {
		return expert1T;
	}

	public void setExpert1T(String expert1T) {
		this.expert1T = expert1T;
	}

	public String getExpert1F() {
		return expert1F;
	}

	public void setExpert1F(String expert1F) {
		this.expert1F = expert1F;
	}

	public String getExpert2T() {
		return expert2T;
	}

	public void setExpert2T(String expert2T) {
		this.expert2T = expert2T;
	}

	public String getExpert2F() {
		return expert2F;
	}

	public void setExpert2F(String expert2F) {
		this.expert2F = expert2F;
	}

	public String getExpert3T() {
		return expert3T;
	}

	public void setExpert3T(String expert3T) {
		this.expert3T = expert3T;
	}

	public String getExpert3F() {
		return expert3F;
	}

	public void setExpert3F(String expert3F) {
		this.expert3F = expert3F;
	}

	public String getRescuersStart() {
		return rescuersStart;
	}

	public void setRescuersStart(String rescuersStart) {
		this.rescuersStart = rescuersStart;
	}

	@Override
	protected Serializable pkVal() {
		return this.id;
	}

	@Override
	public String toString() {
		return "FireSource{" +
			"id=" + id +
			", material=" + material +
			", field=" + field +
			", rescuers=" + rescuers +
			", expert1=" + expert1 +
			", expert2=" + expert2 +
			", expert3=" + expert3 +
			", result=" + result +
			"}";
	}
}
