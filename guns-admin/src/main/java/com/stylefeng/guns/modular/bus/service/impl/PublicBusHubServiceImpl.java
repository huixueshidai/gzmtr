package com.stylefeng.guns.modular.bus.service.impl;

import com.stylefeng.guns.common.persistence.model.PublicBusHub;
import com.stylefeng.guns.common.persistence.dao.PublicBusHubMapper;
import com.stylefeng.guns.modular.bus.service.IPublicBusHubService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 公交枢纽站信息 服务实现类
 * </p>
 *
 * @author yinjc
 * @since 2018-03-21
 */
@Service
public class PublicBusHubServiceImpl extends ServiceImpl<PublicBusHubMapper, PublicBusHub> implements IPublicBusHubService {
	
}
