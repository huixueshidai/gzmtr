package com.stylefeng.guns.common.persistence.model;

import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableName;
import com.stylefeng.guns.common.plugin.entity.BaseModel;
import com.stylefeng.guns.common.plugin.entity.Treeable;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

/**
 * <p>
 * 评价指标体系
 * </p>
 *
 * @author xuziyang
 * @since 2018-01-08
 */
@ToString
@TableName("gzmtr_indication")
@Getter
@Setter
public class Indication extends BaseModel<Indication> implements Treeable{

    private static final long serialVersionUID = 1L;

    /**
     * 指标名称
     */
	private String name;
    /**
     * 权重
     */
	private Double weight;
    /**
     * 阈值
     */
	private Double threshold;
    /**
     * 历史最大值
     */
	@TableField("max_val")
	private Double maxVal;
    /**
     * 历史最小值
     */
	@TableField("min_val")
	private Double minVal;
    /**
     * 排序
     */
	private Integer num;

	/**
	 * 层级
	 */
	private Integer level;

	private Integer pid;

	private String pids;

	/**
	 * 1 :越大越优
	 * 0: 越小越优
	 */
	@TableField("big_is_better")
	private Integer bigIsBetter;



	@Override
	public String getSeparator() {
		return "/";
	}

	@Override
	public String makeSelfAsNewParentIds() {
		return getPids() + getId() + getSeparator();
	}


}
