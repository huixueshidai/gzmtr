package com.stylefeng.guns.modular.yjya.controller;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.mapper.Wrapper;
import com.stylefeng.guns.common.persistence.dao.AccidentTypeMapper;
import com.stylefeng.guns.core.base.controller.BaseController;
import com.stylefeng.guns.core.node.ZTreeNode;
import org.omg.CORBA.PRIVATE_MEMBER;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.beans.factory.annotation.Autowired;
import com.stylefeng.guns.core.log.LogObjectHolder;
import org.springframework.web.bind.annotation.RequestParam;
import com.stylefeng.guns.common.persistence.model.AccidentType;
import com.stylefeng.guns.modular.yjya.service.IAccidentTypeService;

import java.util.List;
import java.util.Map;

/**
 * 事故处理措施控制器
 *
 * @author 殷佳成
 * @Date 2018-01-11 16:55:25
 */
@Controller
@RequestMapping("/accidentType")
public class AccidentTypeController extends BaseController {

    private String PREFIX = "/yjya/accidentType/";

    @Autowired
    private IAccidentTypeService accidentTypeService;
    @Autowired
    private AccidentTypeMapper accidentTypeMapper;

    /**
     * 跳转到事故处理措施首页
     */
    @RequestMapping("")
    public String index() {
        return PREFIX + "accidentType.html";
    }

    /**
     * 跳转到添加事故处理措施
     */
    @RequestMapping("/accidentType_add")
    public String accidentTypeAdd() {
        return PREFIX + "accidentType_add.html";
    }

    /**
     * 跳转到修改事故处理措施
     */
    @RequestMapping("/accidentType_update/{accidentTypeId}")
    public String accidentTypeUpdate(@PathVariable Integer accidentTypeId, Model model) {
        AccidentType accidentType = accidentTypeService.selectById(accidentTypeId);
        model.addAttribute("item", accidentType);
        String pName;
        if (accidentType.getPid() == 0) {
            pName = "顶级";
        } else {
            pName = accidentTypeService.selectById(accidentType.getPid()).getName();
        }
        model.addAttribute("pName", pName);
        LogObjectHolder.me().set(accidentType);
        return PREFIX + "accidentType_edit.html";
    }

    /**
     * 获取事故处理措施列表
     */
    @RequestMapping(value = "/list")
    @ResponseBody
    public Object list(String name) {
        Wrapper<AccidentType> wrapper = new EntityWrapper<>();
        wrapper.like("name", name);
        wrapper.orderBy("num");
        List list = this.accidentTypeMapper.selectList(wrapper);
        return list;
    }

    /**
     * 新增事故处理措施
     */
    @RequestMapping(value = "/add")
    @ResponseBody
    public Object add(AccidentType accidentType) {
        if(accidentType.getPid()==0){
            String allName = accidentType.getName();
            accidentType.setAllName(allName);
        }
        else{
            String allName = accidentType.getAllName().replaceAll("顶级-","")+"-"+accidentType.getName();
            accidentType.setAllName(allName);
        }

        accidentTypeService.addNode(accidentType);
        return super.SUCCESS_TIP;
    }

    /**
     * 删除事故处理措施
     */
    @RequestMapping(value = "/delete")
    @ResponseBody
    public Object delete(@RequestParam Integer accidentTypeId) {
        accidentTypeService.deleteSelfAndChild(accidentTypeId);
        return SUCCESS_TIP;
    }

    /**
     * 获取tree列表
     */
    @RequestMapping(value = "/tree")
    @ResponseBody
    public List<ZTreeNode> tree() {
        List<ZTreeNode> tree = this.accidentTypeService.tree();
        return tree;
    }

    /**
     * 修改事故处理措施
     */
    @RequestMapping(value = "/update")
    @ResponseBody
    public Object update(AccidentType accidentType) {
        if(accidentType.getPid()==0){
            String allName = accidentType.getName();
            accidentType.setAllName(allName);
        }
        else{
            String allName = accidentType.getAllName().replaceAll("顶级-","")+"-"+accidentType.getName();
            accidentType.setAllName(allName);
        }
        accidentTypeService.updateNode(accidentType);
        return super.SUCCESS_TIP;
    }

    /**
     * 事故处理措施详情
     */
    @RequestMapping(value = "/detail/{accidentTypeId}")
    @ResponseBody
    public Object detail(@PathVariable("accidentTypeId") Integer accidentTypeId) {
        return accidentTypeService.selectById(accidentTypeId);
    }
}
