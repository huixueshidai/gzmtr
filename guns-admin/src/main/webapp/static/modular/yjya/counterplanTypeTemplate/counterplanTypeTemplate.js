/**
 * 预案类型模板管理初始化
 */
var CounterplanTypeTemplate = {
    id: "CounterplanTypeTemplateTable",	//表格id
    seItem: null,		//选中的条目
    table: null,
    layerIndex: -1,
    seItems:[]
};

/**
 * 初始化表格的列
 */
CounterplanTypeTemplate.initColumn = function () {
    return [
        {field: 'selectItem', checkbox: true},
            {title: '编码', field: 'id', visible: true, align: 'center', valign: 'middle'},
            {title: '类别', field: 'type', visible: true, align: 'center', valign: 'middle'},
    ];
};

/**
 * 检查是否选中
 */
CounterplanTypeTemplate.check = function () {
    var selected = $('#' + this.id).bootstrapTable('getSelections');
    if(selected.length == 0){
        Feng.info("请先选中表格中的某一记录！");
        return false;
    }else{
        CounterplanTypeTemplate.seItem = selected[0];
        CounterplanTypeTemplate.seItems = selected;
        return true;
    }
};

/**
 * 关闭此对话框
 */
CounterplanTypeTemplate.close = function () {
    parent.layer.close(window.parent.CounterplanTypeTemplate.layerIndex);
}

/**
 * 点击添加预案类型模板
 */
CounterplanTypeTemplate.openAddCounterplanTypeTemplate = function () {
    var index = layer.open({
        type: 2,
        title: '添加预案类型模板',
        area: ['1200px', '600px'], //宽高
        fix: false, //不固定
        maxmin: true,
        content: Feng.ctxPath + '/counterplanTypeTemplate/counterplanTypeTemplate_add'
    });
    this.layerIndex = index;
    layer.full(index);
};

/**
 * 打开查看修改预案类型模板详情
 */
CounterplanTypeTemplate.openCounterplanTypeTemplateDetail = function () {
    if (this.check()) {
        if(CounterplanTypeTemplate.seItems.length>1){
            Feng.info("仅可以选择一条预案类型模板修改！");
            return true;
        }
        var index = layer.open({
            type: 2,
            title: '修改预案类型模板',
            area: ['1200px', '600px'], //宽高
            fix: false, //不固定
            maxmin: true,
            content: Feng.ctxPath + '/counterplanTypeTemplate/counterplanTypeTemplate_update/' + CounterplanTypeTemplate.seItem.id
        });
        this.layerIndex = index;
        layer.full(index);
    }
};

/**
 * 预览内容
 */
CounterplanTypeTemplate.openCounterplanTypeTemplateInfo = function () {
    if (this.check()) {
        if(CounterplanTypeTemplate.seItems.length>1){
            Feng.info("仅可以选择一条预案类型模板预览！");
            return true;
        }
        var index = layer.open({
            type: 2,
            title: '预案类型模板详情',
            area: ['1200px', '600px'], //宽高
            fix: false, //不固定
            maxmin: true,
            content: Feng.ctxPath + '/counterplanTypeTemplate/detail/' + CounterplanTypeTemplate.seItem.id
        });
        this.layerIndex = index;
        layer.full(index);
    }
};

/**
 * 预览页面跳转修改页面
 */
CounterplanTypeTemplate.openCounterplanTypeTemplateDetailUpdate = function () {
    CounterplanTypeTemplate.close();
        var counterplanTypeTemplateId = $("#id").val();

        var index = window.parent.layer.open({
            type: 2,
            title: '修改预案类型模板',
            area: ['1200px', '600px'], //宽高
            fix: false, //不固定
            maxmin: true,
            content: Feng.ctxPath + '/counterplanTypeTemplate/counterplanTypeTemplate_update/' + counterplanTypeTemplateId
        });
    window.parent.CounterplanTypeTemplate.layerIndex = index;
    window.parent.layer.full(index);


};

/**
 * 删除预案类型模板
 */
CounterplanTypeTemplate.delete = function () {

    if (this.check()) {
        var seItems = CounterplanTypeTemplate.seItems;
        var delIds = [];
        seItems.forEach(function(element){
            delIds.push(element.id);
        });
        var operation = function(){
            var ajax = new $ax(Feng.ctxPath + "/counterplanTypeTemplate/delete", function () {
                Feng.success("删除成功!");
                CounterplanTypeTemplate.table.refresh();
            }, function (data) {
                Feng.error("删除失败!" + data.responseJSON.message + "!");
            });
            ajax.set("ids",delIds);
            ajax.start();
        };
        Feng.confirm("是否删除选中预案类型模板?" ,operation);
    }
};

/**
 * 查询预案类型模板列表
 */
CounterplanTypeTemplate.search = function () {
    var queryData = {};
    queryData['condition'] = $("#condition").val();
    CounterplanTypeTemplate.table.refresh({query: queryData});
};

$(function () {
    var defaultColunms = CounterplanTypeTemplate.initColumn();
    var table = new BSTable(CounterplanTypeTemplate.id, "/counterplanTypeTemplate/list", defaultColunms);
    table.setPaginationType("client");
    CounterplanTypeTemplate.table = table.init();
});
