package com.stylefeng.guns.common.persistence.model;

import java.io.Serializable;

import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableName;
import java.io.Serializable;

/**
 * <p>
 * 
 * </p>
 *
 * @author wangshuaikang
 * @since 2018-05-29
 */
@TableName("gzmtr_accident_emergencyproperty")
public class AccidentEmergencyproperty extends Model<AccidentEmergencyproperty> {

    private static final long serialVersionUID = 1L;

	private Integer id;
    /**
     * 客流
     */
	private String ridership;
    /**
     * 同行能力
     */
	@TableField("traffic_capacity")
	private String trafficCapacity;
    /**
     * 故障数量
     */
	private String fault;
    /**
     * 气体保护
     */
	@TableField("gas_shield")
	private String gasShield;
    /**
     * 隧道长度
     */
	@TableField("tunnel_length")
	private String tunnelLength;
    /**
     * 状态
     */
	private String status;
    /**
     * 距离前一站距离
     */
	@TableField("distance_on")
	private String distanceOn;
    /**
     * 距离下一站距离
     */
	@TableField("distance_next")
	private String distanceNext;
    /**
     * 被困人员数量
     */
	@TableField("trapped_num")
	private String trappedNum;
    /**
     * 被困人员行动能力
     */
	@TableField("trapped_num_power")
	private String trappedNumPower;
    /**
     * 物理状态
     */
	@TableField("physical_state")
	private String physicalState;
    /**
     * 毒性
     */
	private String toxicity;
    /**
     * 燃爆性
     */
	private String bums;
    /**
     * 腐蚀性
     */
	private String causticity;
    /**
     * 挥发性
     */
	private String volatileness;
    /**
     * 温度
     */
	private String temperature;
    /**
     * 烟气浓度
     */
	private String smokescope;
    /**
     * 坍塌
     */
	private String collapse;
	@TableField("lose_efficacy")
	private String loseEfficacy;
    /**
     * 道路状况
     */
	private String reoad;
    /**
     * 119距离
     */
	@TableField("distance_one_one_nine")
	private String distanceOneOneNine;
    /**
     * 110距离
     */
	@TableField("distance_one_one_zero")
	private String distanceOneOneZero;
    /**
     * 120距离
     */
	@TableField("distance_one_two_zero")
	private String distanceOneTwoZero;
    /**
     * 消防设施
     */
	@TableField("fire_control")
	private String fireControl;


	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getRidership() {
		return ridership;
	}

	public void setRidership(String ridership) {
		this.ridership = ridership;
	}

	public String getTrafficCapacity() {
		return trafficCapacity;
	}

	public void setTrafficCapacity(String trafficCapacity) {
		this.trafficCapacity = trafficCapacity;
	}

	public String getFault() {
		return fault;
	}

	public void setFault(String fault) {
		this.fault = fault;
	}

	public String getGasShield() {
		return gasShield;
	}

	public void setGasShield(String gasShield) {
		this.gasShield = gasShield;
	}

	public String getTunnelLength() {
		return tunnelLength;
	}

	public void setTunnelLength(String tunnelLength) {
		this.tunnelLength = tunnelLength;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getDistanceOn() {
		return distanceOn;
	}

	public void setDistanceOn(String distanceOn) {
		this.distanceOn = distanceOn;
	}

	public String getDistanceNext() {
		return distanceNext;
	}

	public void setDistanceNext(String distanceNext) {
		this.distanceNext = distanceNext;
	}

	public String getTrappedNum() {
		return trappedNum;
	}

	public void setTrappedNum(String trappedNum) {
		this.trappedNum = trappedNum;
	}

	public String getTrappedNumPower() {
		return trappedNumPower;
	}

	public void setTrappedNumPower(String trappedNumPower) {
		this.trappedNumPower = trappedNumPower;
	}

	public String getPhysicalState() {
		return physicalState;
	}

	public void setPhysicalState(String physicalState) {
		this.physicalState = physicalState;
	}

	public String getToxicity() {
		return toxicity;
	}

	public void setToxicity(String toxicity) {
		this.toxicity = toxicity;
	}

	public String getBums() {
		return bums;
	}

	public void setBums(String bums) {
		this.bums = bums;
	}

	public String getCausticity() {
		return causticity;
	}

	public void setCausticity(String causticity) {
		this.causticity = causticity;
	}

	public String getVolatileness() {
		return volatileness;
	}

	public void setVolatileness(String volatileness) {
		this.volatileness = volatileness;
	}

	public String getTemperature() {
		return temperature;
	}

	public void setTemperature(String temperature) {
		this.temperature = temperature;
	}

	public String getSmokescope() {
		return smokescope;
	}

	public void setSmokescope(String smokescope) {
		this.smokescope = smokescope;
	}

	public String getCollapse() {
		return collapse;
	}

	public void setCollapse(String collapse) {
		this.collapse = collapse;
	}

	public String getLoseEfficacy() {
		return loseEfficacy;
	}

	public void setLoseEfficacy(String loseEfficacy) {
		this.loseEfficacy = loseEfficacy;
	}

	public String getReoad() {
		return reoad;
	}

	public void setReoad(String reoad) {
		this.reoad = reoad;
	}

	public String getDistanceOneOneNine() {
		return distanceOneOneNine;
	}

	public void setDistanceOneOneNine(String distanceOneOneNine) {
		this.distanceOneOneNine = distanceOneOneNine;
	}

	public String getDistanceOneOneZero() {
		return distanceOneOneZero;
	}

	public void setDistanceOneOneZero(String distanceOneOneZero) {
		this.distanceOneOneZero = distanceOneOneZero;
	}

	public String getDistanceOneTwoZero() {
		return distanceOneTwoZero;
	}

	public void setDistanceOneTwoZero(String distanceOneTwoZero) {
		this.distanceOneTwoZero = distanceOneTwoZero;
	}

	public String getFireControl() {
		return fireControl;
	}

	public void setFireControl(String fireControl) {
		this.fireControl = fireControl;
	}

	@Override
	protected Serializable pkVal() {
		return this.id;
	}

	@Override
	public String toString() {
		return "AccidentEmergencyproperty{" +
			"id=" + id +
			", ridership=" + ridership +
			", trafficCapacity=" + trafficCapacity +
			", fault=" + fault +
			", gasShield=" + gasShield +
			", tunnelLength=" + tunnelLength +
			", status=" + status +
			", distanceOn=" + distanceOn +
			", distanceNext=" + distanceNext +
			", trappedNum=" + trappedNum +
			", trappedNumPower=" + trappedNumPower +
			", physicalState=" + physicalState +
			", toxicity=" + toxicity +
			", bums=" + bums +
			", causticity=" + causticity +
			", volatileness=" + volatileness +
			", temperature=" + temperature +
			", smokescope=" + smokescope +
			", collapse=" + collapse +
			", loseEfficacy=" + loseEfficacy +
			", reoad=" + reoad +
			", distanceOneOneNine=" + distanceOneOneNine +
			", distanceOneOneZero=" + distanceOneOneZero +
			", distanceOneTwoZero=" + distanceOneTwoZero +
			", fireControl=" + fireControl +
			"}";
	}
}
