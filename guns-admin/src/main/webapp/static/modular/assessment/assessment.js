/**
 * 指标评估管理初始化
 */
var Assessment = {
    id: "AssessmentTable",	//表格id
    seItem: null,		//选中的条目
    count: 0,
    echarsLineTemplateTemplate: $("#echarsLineTemplate").html(),
    table: null,
    layerIndex: -1,
    roadnetId: null
};

/**
 * 初始化表格的列
 */
Assessment.initColumn = function () {
    return [
        {field: 'selectItem', radio: true},
        // {title: '编码', field: 'treeCode', visible: false, align: 'center', valign: 'middle'},
        {title: '路网', field: 'roadnetName', visible: true, align: 'center', valign: 'middle'},
        {title: '评估指标', field: 'indicationName', visible: true, align: 'center', valign: 'middle'},
        {title: '评估结果', field: 'assessmentValue', visible: true, align: 'center', valign: 'middle'},
        {title: '运营状态', field: 'warningLevelName', visible: true, align: 'center', valign: 'middle'},
        {title: '开始时间', field: 'beginTime', visible: true, align: 'center', valign: 'middle'},
        {title: '结束时间', field: 'endTime', visible: true, align: 'center', valign: 'middle'}
    ];
};


/**
 * echarsLine获取新的id
 */
Assessment.newId = function () {
    if (this.count == undefined) {
        this.count = 0;
    }
    this.count = this.count + 1;
    return "echartsLine" + this.count;
};

/**
 * 添加折线图
 */
Assessment.addEcharsLine = function () {
    $("#echarsLineArea").append(this.echarsLineTemplateTemplate);
    var newId = this.newId();
    $("#echartsLine").attr("id", newId);
    return newId;
};


/**
 * 检查是否选中
 */
Assessment.check = function () {
    var selected = $('#' + this.id).bootstrapTreeTable('getSelections');
    if (selected.length == 0) {
        Feng.info("请先选中表格中的某一记录！");
        return false;
    } else {
        Assessment.seItem = selected[0];
        return true;
    }
};

/**
 * 点击添加指标评估
 */
Assessment.openAddAssessment = function () {
    var index = layer.open({
        type: 2,
        title: '添加指标评估',
        area: ['800px', '420px'], //宽高
        fix: false, //不固定
        maxmin: true,
        content: Feng.ctxPath + '/assessment/assessment_add'
    });
    this.layerIndex = index;
};

/**
 * 打开查看指标评估详情
 */
Assessment.openAssessmentDetail = function () {
    if (this.check()) {
        var index = layer.open({
            type: 2,
            title: '指标评估详情',
            area: ['800px', '420px'], //宽高
            fix: false, //不固定
            maxmin: true,
            content: Feng.ctxPath + '/assessment/assessment_update/' + Assessment.seItem.id
        });
        this.layerIndex = index;
    }
};

/**
 * 删除指标评估
 */
Assessment.delete = function () {
    if (this.check()) {
        var ajax = new $ax(Feng.ctxPath + "/assessment/delete", function (data) {
            Feng.success("删除成功!");
            Assessment.table.refresh();
        }, function (data) {
            Feng.error("删除失败!" + data.responseJSON.message + "!");
        });
        ajax.set("assessmentId", this.seItem.id);
        ajax.start();
    }
};

Assessment.getQueryData = function () {
    var queryData = {};
    queryData['beginDate'] = $("#beginDate").val();
    queryData['endDate'] = $("#endDate").val();
    queryData['roadnetId'] = Assessment.roadnetId;
    return queryData;
};

/**
 * 查询指标评估列表
 */
Assessment.search = function () {
    var qa = this.getQueryData();
    Assessment.table.refresh({query: qa});
};

Assessment.onclickRoadnet = function (e, treeId, treeNode) {
    $("#roadnetName").attr("value", treeNode.name);
    $("#roadnetTreeDiv").fadeOut("fast");
    Assessment.roadnetId = treeNode.id;

}

Assessment.showRoadnetSelectTree = function () {
    Feng.showInputTree("roadnetName", "roadnetTreeDiv", 15, 34);
}




$(function () {
    var defaultColunms = Assessment.initColumn();
    var table = new BSTreeTable(Assessment.id, "/assessment/list", defaultColunms);
    table.setExpandColumn(2);
    table.setIdField("treeCode");
    table.setCodeField("treeCode");
    table.setParentCodeField("pid");
    table.setExpandAll(true);
    table.init();
    Assessment.table = table;


    var roadnetTree = new $ZTree("roadnetTree", "/roadnet/tree");
    roadnetTree.bindOnClick(Assessment.onclickRoadnet);
    roadnetTree.init();
});


