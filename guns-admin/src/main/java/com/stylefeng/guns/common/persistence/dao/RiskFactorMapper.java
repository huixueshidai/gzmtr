package com.stylefeng.guns.common.persistence.dao;

import com.stylefeng.guns.common.persistence.model.RiskFactor;
import com.baomidou.mybatisplus.mapper.BaseMapper;

/**
 * <p>
  * 事故致因 Mapper 接口
 * </p>
 *
 * @author xuziyang
 * @since 2018-04-11
 */
public interface RiskFactorMapper extends BaseMapper<RiskFactor> {

}