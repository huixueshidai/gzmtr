package com.stylefeng.guns.modular.yjya.warpper;

import com.stylefeng.guns.core.base.warpper.BaseControllerWarpper;
import com.stylefeng.guns.core.util.SpringContextHolder;
import com.stylefeng.guns.core.util.ToolUtil;
import com.stylefeng.guns.modular.yjya.dao.ContactGroupDao;

import java.util.Map;

public class ContactGroupWarpper extends BaseControllerWarpper {


    public ContactGroupWarpper(Object obj) {
        super(obj);
    }

    @Override
    protected void warpTheMap(Map<String, Object> map) {
        Integer pid = (Integer) map.get("pid");
        if (ToolUtil.isEmpty(pid) || pid.equals(0)) {
            map.put("pName", "--");
        } else {
            ContactGroupDao contactGroupDao  = SpringContextHolder.getBean(ContactGroupDao.class);
            map.put("pName", contactGroupDao.getNameById(pid));
        }
        return;
    }
}
