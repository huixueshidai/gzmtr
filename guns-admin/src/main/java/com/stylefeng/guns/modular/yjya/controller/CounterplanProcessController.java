package com.stylefeng.guns.modular.yjya.controller;

import com.stylefeng.guns.core.base.controller.BaseController;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.beans.factory.annotation.Autowired;
import com.stylefeng.guns.core.log.LogObjectHolder;
import org.springframework.web.bind.annotation.RequestParam;
import com.stylefeng.guns.common.persistence.model.CounterplanProcess;
import com.stylefeng.guns.modular.yjya.service.ICounterplanProcessService;

/**
 * 预案处置流程控制器
 *
 * @author xuziyang
 * @Date 2018-03-01 14:12:51
 */
@Controller
@RequestMapping("/counterplanProcess")
public class CounterplanProcessController extends BaseController {

    private String PREFIX = "/yjya/counterplanProcess/";

    @Autowired
    private ICounterplanProcessService counterplanProcessService;

    /**
     * 跳转到预案处置流程首页
     */
    @RequestMapping("")
    public String index() {
        return PREFIX + "counterplanProcess.html";
    }

    /**
     * 跳转到添加预案处置流程
     */
    @RequestMapping("/counterplanProcess_add")
    public String counterplanProcessAdd() {
        return PREFIX + "counterplanProcess_add.html";
    }

    /**
     * 跳转到修改预案处置流程
     */
    @RequestMapping("/counterplanProcess_update/{counterplanProcessId}")
    public String counterplanProcessUpdate(@PathVariable Integer counterplanProcessId, Model model) {
        CounterplanProcess counterplanProcess = counterplanProcessService.selectById(counterplanProcessId);
        model.addAttribute("item",counterplanProcess);
        LogObjectHolder.me().set(counterplanProcess);
        return PREFIX + "counterplanProcess_edit.html";
    }

    /**
     * 获取预案处置流程列表
     */
    @RequestMapping(value = "/list")
    @ResponseBody
    public Object list(String condition) {
        return counterplanProcessService.selectList(null);
    }

    /**
     * 新增预案处置流程
     */
    @RequestMapping(value = "/add")
    @ResponseBody
    public Object add(CounterplanProcess counterplanProcess) {
        counterplanProcessService.insert(counterplanProcess);
        return super.SUCCESS_TIP;
    }

    /**
     * 删除预案处置流程
     */
    @RequestMapping(value = "/delete")
    @ResponseBody
    public Object delete(@RequestParam Integer counterplanProcessId) {
        counterplanProcessService.deleteById(counterplanProcessId);
        return SUCCESS_TIP;
    }

    /**
     * 修改预案处置流程
     */
    @RequestMapping(value = "/update")
    @ResponseBody
    public Object update(CounterplanProcess counterplanProcess) {
        counterplanProcessService.updateById(counterplanProcess);
        return super.SUCCESS_TIP;
    }

    /**
     * 预案处置流程详情
     */
    @RequestMapping(value = "/detail/{counterplanProcessId}")
    @ResponseBody
    public Object detail(@PathVariable("counterplanProcessId") Integer counterplanProcessId) {
        return counterplanProcessService.selectById(counterplanProcessId);
    }
}
