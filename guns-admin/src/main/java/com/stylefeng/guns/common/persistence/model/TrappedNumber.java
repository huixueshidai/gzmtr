package com.stylefeng.guns.common.persistence.model;

import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;
import com.baomidou.mybatisplus.enums.IdType;

import java.io.Serializable;

/**
 * <p>
 * 被困人员录入
 * </p>
 *
 * @author xhq
 * @since 2018-09-04
 */
@TableName("gzmtr_trapped_number")
public class TrappedNumber extends Model<TrappedNumber> {

    private static final long serialVersionUID = 1L;

	@TableId(value="id", type= IdType.AUTO)
	private Integer id;
    /**
     * 场地
     */
	private String field;
    /**
     * 救援人员
     */
	private String rescuers;
    /**
     * 被困人员
     */
	private String trapped;
    /**
     * 疏散率A
     */
	private String discharge1;
    /**
     * 疏散率B
     */
	private String discharge2;
    /**
     * 疏散率C
     */
	private String discharge3;
    /**
     * 疏散率E
     */
	private String discharge4;
    /**
     * 疏散率F
     */
	private String discharge5;
	@TableField("trapped_start")
	private String trappedStart;

	@TableField("trapped_end")
	private String trappedEnd;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getField() {
		return field;
	}

	public void setField(String field) {
		this.field = field;
	}

	public String getRescuers() {
		return rescuers;
	}

	public void setRescuers(String rescuers) {
		this.rescuers = rescuers;
	}

	public String getTrapped() {
		return trapped;
	}

	public void setTrapped(String trapped) {
		this.trapped = trapped;
	}

	public String getDischarge1() {
		return discharge1;
	}

	public void setDischarge1(String discharge1) {
		this.discharge1 = discharge1;
	}

	public String getDischarge2() {
		return discharge2;
	}

	public void setDischarge2(String discharge2) {
		this.discharge2 = discharge2;
	}

	public String getDischarge3() {
		return discharge3;
	}

	public void setDischarge3(String discharge3) {
		this.discharge3 = discharge3;
	}

	public String getDischarge4() {
		return discharge4;
	}

	public void setDischarge4(String discharge4) {
		this.discharge4 = discharge4;
	}

	public String getDischarge5() {
		return discharge5;
	}

	public void setDischarge5(String discharge5) {
		this.discharge5 = discharge5;
	}

	public String getTrappedStart() {
		return trappedStart;
	}

	public void setTrappedStart(String trappedStart) {
		this.trappedStart = trappedStart;
	}

	public String getTrappedEnd() {
		return trappedEnd;
	}

	public void setTrappedEnd(String trappedEnd) {
		this.trappedEnd = trappedEnd;
	}

	@Override
	protected Serializable pkVal() {
		return this.id;
	}

	@Override
	public String toString() {
		return "TrappedNumber{" +
			"id=" + id +
			", field=" + field +
			", rescuers=" + rescuers +
			", trapped=" + trapped +
			", discharge1=" + discharge1 +
			", discharge2=" + discharge2 +
			", discharge3=" + discharge3 +
			", discharge4=" + discharge4 +
			", discharge5=" + discharge5 +
			"}";
	}
}
