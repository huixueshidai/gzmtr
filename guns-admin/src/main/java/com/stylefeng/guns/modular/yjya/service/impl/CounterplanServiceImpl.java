package com.stylefeng.guns.modular.yjya.service.impl;

import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.stylefeng.guns.common.persistence.dao.CounterplanMapper;
import com.stylefeng.guns.common.persistence.model.Counterplan;
import com.stylefeng.guns.modular.yjya.service.ICounterplanProcessService;
import com.stylefeng.guns.modular.yjya.service.ICounterplanService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.io.Serializable;

/**
 * <p>
 * 预案 服务实现类
 * </p>
 *
 * @author xuziyang
 * @since 2018-01-18
 */
@Service
public class CounterplanServiceImpl extends ServiceImpl<CounterplanMapper, Counterplan> implements ICounterplanService {

    @Autowired
    private ICounterplanProcessService counterplanProcessService;

    @Override
    @Transactional
    public void insert(Counterplan counterplan, String[] accidentIds) {
        insert(counterplan);
        if (accidentIds != null) {
            counterplanProcessService.insert(counterplan.getId(),accidentIds);
        }
    }

    @Override
    @Transactional
    public void updateById(Counterplan counterplan, String[] accidentIds) {
        if (accidentIds != null && accidentIds.length > 0) {
            counterplanProcessService.update(counterplan.getId(), accidentIds);
        }
        super.updateById(counterplan);
    }

    @Override
    @Transactional
    public boolean deleteById(Serializable id) {
        counterplanProcessService.deleteByCounterplanId((Integer) id);
        return super.deleteById(id);
    }
}
