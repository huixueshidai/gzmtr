/**
 * 通讯录管理初始化
 */
var ContactMessage = {
    id: "ContactTable",	//表格id
    seItem: null,		//选中的条目
    table: null,
    layerIndex: -1,
    groupId:0,
    uploadInstance:null,
    contactSelects:null
};

/**
 * 初始化表格的列
 */
ContactMessage.initColumn = function () {
    return [
        {field: 'selectItem', checkbox: true,maintainSelected:true},
        {title: 'id', field: 'id', visible: true, align: 'center', valign: 'middle'},
        {title: '姓名', field: 'name', visible: true, align: 'center', valign: 'middle'},
        {title: '职务/专业', field: 'duty', visible: true, align: 'center', valign: 'middle'},
        {title: '行政职务', field: 'administrationDuty', visible: true, align: 'center', valign: 'middle'},
        {title: '座机', field: 'phone', visible: true, align: 'center', valign: 'middle'},
        {title: '手机号', field: 'mobilePhone', visible: true, align: 'center', valign: 'middle'},
        {title: '邮箱', field: 'email', visible: true, align: 'center', valign: 'middle'},
        {title: '群组', field: 'groupName', visible: true, align: 'center', valign: 'middle'}
    ];
};

/**
 * 检查是否选中
 */
ContactMessage.check = function () {
    var selected = $('#' + this.id).bootstrapTable('getSelections');
    var selects = new Array();
    if(selected.length == 0){
        Feng.info("请先选择通讯录人员！");
        return false;
    }else{
        for(var i=0;i<selected.length;i++){
            selects[i] = selected[i].id;
        }
        ContactMessage.seItem = selects.join(",");
        return true;
    }
};


ContactMessage.resetSearch = function () {
    $("#name").val("");
    $("#administrationDuty").val("");
    // Contact.search();

}

/**
 * 查询通讯录列表
 */
ContactMessage.search = function () {
    var queryData = {};

    queryData['groupId'] = ContactMessage.groupId;
    queryData['name'] = $("#name").val();
    queryData['administrationDuty'] = $("#administrationDuty").val();
    ContactMessage.table.refresh({query: queryData});
};

ContactMessage.onClickContactGroup = function (e, treeId, treeNode) {
    ContactMessage.groupId = treeNode.id;
    ContactMessage.search();
}

ContactMessage.exportXlsByM = function () {
    window.location.href = Feng.ctxPath + "/contact/exportXlsByM";
}

ContactMessage.upload = function () {
    this.uploadInstance
}

/**
 * 编辑短信
 */
ContactMessage.addToReport = function(){
    if (this.check()) {
        var ajax = new $ax(Feng.ctxPath + "/contact/reportContactSelAdd", function (data) {
            ContactMessage.contactSelects = data;
        }, function (data) {
            Feng.error("失败!" + data.responseJSON.message + "!");
        });
        ajax.set("contactIds",this.seItem);
        ajax.start();
        var index = layer.open({
            type: 2,
            title: '编辑短信',
            area: ['1200px', '500px'], //宽高
            fix: false, //不固定
            maxmin: false,
            content: Feng.ctxPath + '/contact/contactShortMessage'
        });
        this.layerIndex = index;
        layer.full(index);

        // parent.layer.close(window.parent.AccidentReport.layerIndex);
    }
}

$(function () {
    var defaultColunms = ContactMessage.initColumn();
    var table = new BSTable(ContactMessage.id, "/contact/MessageContactSel", defaultColunms);
    table.setPaginationType("client");
    table.setmaintainSelected(true);
    ContactMessage.table = table.init();

    var ztree = new $ZTree("contactGroupTree", "/contactGroup/tree");
    ztree.bindOnClick(ContactMessage.onClickContactGroup);
    ztree.init();

    var upload = new $WebUpload({
        uploadId:"upload",
        mimeTypes:"application/vnd.ms-excel",
        extensions:"xls",
        url:"/contact/importExcel",
        multiple:true
    });
    upload.init();
    ContactMessage.uploadInstance = upload;
});


