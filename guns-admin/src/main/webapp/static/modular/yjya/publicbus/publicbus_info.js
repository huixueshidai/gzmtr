/**
 * 初始化详情对话框
 */
var PublicbusInfoDlg = {
    publicbusInfoData : {}
};

/**
 * 清除数据
 */
PublicbusInfoDlg.clearData = function() {
    this.publicbusInfoData = {};
}

/**
 * 设置对话框中的数据
 *
 * @param key 数据的名称
 * @param val 数据的具体值
 */
PublicbusInfoDlg.set = function(key, val) {
    this.publicbusInfoData[key] = (typeof val == "undefined") ? $("#" + key).val() : val;
    return this;
}

/**
 * 设置对话框中的数据
 *
 * @param key 数据的名称
 * @param val 数据的具体值
 */
PublicbusInfoDlg.get = function(key) {
    return $("#" + key).val();
}

/**
 * 关闭此对话框
 */
PublicbusInfoDlg.close = function() {
    parent.layer.close(window.parent.Publicbus.layerIndex);
}

/**
 * 收集数据
 */
PublicbusInfoDlg.collectData = function() {
    this
    .set('id')
    .set('time')
    .set('user')
    .set('stationName')
    .set('level')
    ;
}

/**
 * 提交添加
 */
PublicbusInfoDlg.addSubmit = function() {

    this.clearData();
    this.collectData();

    //提交信息
    var ajax = new $ax(Feng.ctxPath + "/publicbus/add", function(data){
        Feng.success("已生成处置留痕!");
        window.parent.Publicbus.table.refresh();
        PublicbusInfoDlg.close();
    },function(data){
        Feng.error("添加失败!" + data.responseJSON.message + "!");
    });
    ajax.set(this.publicbusInfoData);
    ajax.start();
}

/**
 * 提交修改
 */
PublicbusInfoDlg.editSubmit = function() {

    this.clearData();
    this.collectData();

    //提交信息
    var ajax = new $ax(Feng.ctxPath + "/publicbus/update", function(data){
        Feng.success("修改成功!");
        window.parent.Publicbus.table.refresh();
        PublicbusInfoDlg.close();
    },function(data){
        Feng.error("修改失败!" + data.responseJSON.message + "!");
    });
    ajax.set(this.publicbusInfoData);
    ajax.start();
}

$(function() {

});
