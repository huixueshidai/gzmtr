package com.stylefeng.guns.modular.assessment.calculate.line.facility;

import com.stylefeng.guns.common.persistence.model.Assessment;
import com.stylefeng.guns.core.util.DoubleUtil;
import com.stylefeng.guns.modular.assessment.calculate.ThirdIndicationCalculator;
import com.stylefeng.guns.modular.assessment.calculate.aware.LineThirdIndicationAware;
import org.springframework.stereotype.Component;

import java.util.Date;
import java.util.List;
import java.util.stream.IntStream;

/**
 * 线路系统影响运营风险指数
 *
 * @author yinjc
 * @since 2018/05/09
 */
@Component
public class
LineTheWiringSystemAffectsTransportationCal extends ThirdIndicationCalculator implements LineThirdIndicationAware {

    @Override
    protected Integer getIndicationId() {
        return 67;
    }

    @Override
    public Assessment calculate(Integer lineId, Date beginDate, Date endDate) {
        //区段中伤损钢轨的数量
        List<Double> badNumOfSectionList = getMulDoubleValue(lineId, 240, beginDate,endDate);
        //区段中钢轨的总数量
        List<Double> totalOfSectionList = getMulDoubleValue(lineId, 241, beginDate,endDate);
        //区段轨道指数
        List<Double> indexList = getMulDoubleValue(lineId, 242, beginDate,endDate);
        //出现轨道伤损的区段个数与该线路区段总个数的比值
        Double specificValue = getDoubleValue(lineId, 243, beginDate,endDate);

        //区段数量
        Integer sectionNum = Math.min(Math.min(badNumOfSectionList.size(), totalOfSectionList.size()), indexList.size());

        Double sumOfIndex = indexList.stream()
                .limit(sectionNum)
                .reduce(0d, DoubleUtil::add);
        Double avgOfIndex = DoubleUtil.safeDiv(sumOfIndex, sectionNum);

        Double ratSumOfBad = IntStream.range(0, sectionNum)
                .boxed()
                .map(i -> DoubleUtil.safeDiv(badNumOfSectionList.get(i), totalOfSectionList.get(i))).mapToDouble(d -> d).sum();
        Double ratSumOfBanWithRate = DoubleUtil.mul(specificValue, ratSumOfBad);

        Double resultVal = DoubleUtil.add(avgOfIndex, ratSumOfBanWithRate);


        return genResult(resultVal,lineId,beginDate,endDate);
    }
}
