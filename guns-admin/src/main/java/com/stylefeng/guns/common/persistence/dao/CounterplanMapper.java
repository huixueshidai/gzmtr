package com.stylefeng.guns.common.persistence.dao;

import com.stylefeng.guns.common.persistence.model.Counterplan;
import com.baomidou.mybatisplus.mapper.BaseMapper;

/**
 * <p>
  * 预案 Mapper 接口
 * </p>
 *
 * @author xuziyang
 * @since 2018-01-18
 */
public interface CounterplanMapper extends BaseMapper<Counterplan> {

}