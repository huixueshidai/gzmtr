/**
 * 事故（事件）历史信息管理初始化
 */
var AccidentHistory = {
    id: "AccidentHistoryTable",	//表格id
    seItem: null,		//选中的条目
    table: null,
    layerIndex: -1
};

/**
 * 初始化表格的列
 */
AccidentHistory.initColumn = function () {
    return [
        {field: 'selectItem', radio: true},
        {title: '名称', field: 'name', visible: true, align: 'center', valign: 'middle'},
        {title: '日期', field: 'date', visible: true, align: 'center', valign: 'middle'},
        {title: '车站', field: 'roadnetName', visible: true, align: 'center', valign: 'middle'},
        {title: '区域', field: 'areaName', visible: true, align: 'center', valign: 'middle'},
        {title: '级别', field: 'rankName', visible: true, align: 'center', valign: 'middle'},
        {title: '种类', field: 'categoryName', visible: true, align: 'center', valign: 'middle'},
        {title: '致因', field: 'causeName', visible: true, align: 'center', valign: 'middle'},
        {title: '描述', field: 'detail', visible: true, align: 'center', valign: 'middle'},
    ];
};

/**
 * 检查是否选中
 */
AccidentHistory.check = function () {
    var selected = $('#' + this.id).bootstrapTable('getSelections');
    if (selected.length == 0) {
        Feng.info("请先选中表格中的某一记录！");
        return false;
    } else {
        AccidentHistory.seItem = selected[0];
        return true;
    }
};

/**
 * 点击添加事故（事件）历史信息
 */
AccidentHistory.openAddAccidentHistory = function () {
    var index = layer.open({
        type: 2,
        title: '添加事故（事件）历史信息',
        area: ['800px', '420px'], //宽高
        fix: false, //不固定
        maxmin: true,
        content: Feng.ctxPath + '/accidentHistory/accidentHistory_add'
    });
    this.layerIndex = index;
};

/**
 * 打开查看事故（事件）历史信息详情
 */
AccidentHistory.openAccidentHistoryDetail = function () {
    if (this.check()) {
        var index = layer.open({
            type: 2,
            title: '事故（事件）历史信息详情',
            area: ['800px', '420px'], //宽高
            fix: false, //不固定
            maxmin: true,
            content: Feng.ctxPath + '/accidentHistory/accidentHistory_update/' + AccidentHistory.seItem.id
        });
        this.layerIndex = index;
    }
};

/**
 * 同期事件提醒
 */
AccidentHistory.openAccidentForecast = function () {
    var index = layer.open({
        type: 2,
        title: '',
        area: ['800px', '420px'], //宽高
        fix: false, //不固定
        maxmin: true,
        content: Feng.ctxPath + '/accidentHistory/accident_forecast'
    });
    this.layerIndex = index;
};

/**
 * 删除事故（事件）历史信息
 */
AccidentHistory.delete = function () {
    if (this.check()) {
        var ajax = new $ax(Feng.ctxPath + "/accidentHistory/delete", function (data) {
            Feng.success("删除成功!");
            AccidentHistory.table.refresh();
        }, function (data) {
            Feng.error("删除失败!" + data.responseJSON.message + "!");
        });
        ajax.set("accidentHistoryId", this.seItem.id);
        ajax.start();
    }
};

/**
 * 查询事故（事件）历史信息列表
 */
AccidentHistory.search = function () {
    var queryData = {};
    queryData['name'] = $("#name").val();
    queryData['category'] = $("#category").val();
    queryData['rankCode'] = $("#rankCode").val();
    queryData['beginTime'] = $("#beginTime").val();
    queryData['endTime'] = $("#endTime").val();
    AccidentHistory.table.refresh({query: queryData});
};

$(function () {
    var defaultColunms = AccidentHistory.initColumn();
    var table = new BSTable(AccidentHistory.id, "/accidentHistory/list", defaultColunms);
    table.setPaginationType("server");
    AccidentHistory.table = table.init();
});
