package com.stylefeng.guns.modular.assessment.calculate.area.accident;

import com.stylefeng.guns.common.persistence.model.Assessment;
import com.stylefeng.guns.core.util.DoubleUtil;
import com.stylefeng.guns.modular.assessment.calculate.ThirdIndicationCalculator;
import com.stylefeng.guns.modular.assessment.calculate.aware.AreaThirdIndicationAware;
import org.springframework.stereotype.Component;

import java.util.Date;

/**
 * 区域中心等效事故率
 *
 * @author yinjc
 * @time 2018/05/11
 */
@Component
public class AreaRegionalCenterEquivalentCal extends ThirdIndicationCalculator implements AreaThirdIndicationAware {

    @Override
    protected Integer getIndicationId() {
        return 90;
    }

    @Override
    public Assessment calculate(Integer areaId, Date beginDate, Date endDate) {
        double y1 = getDoubleValue(areaId, 310, beginDate,endDate);
        double y2 = getDoubleValue(areaId, 311, beginDate,endDate);
        double y3 = getDoubleValue(areaId, 312, beginDate,endDate);
        double y4 = getDoubleValue(areaId, 313, beginDate,endDate);
        double y5 = getDoubleValue(areaId, 314, beginDate,endDate);
        double a1 = getDoubleValue(areaId, 315, beginDate,endDate);
        double a2 = getDoubleValue(areaId, 316, beginDate,endDate);
        double a3 = getDoubleValue(areaId, 317, beginDate,endDate);
        double a4 = getDoubleValue(areaId, 318, beginDate,endDate);
        double a5 = getDoubleValue(areaId, 319, beginDate,endDate);
        double l = getDoubleValue(areaId, 320, beginDate,endDate);

        double m1 = DoubleUtil.mul(y1, a1);
        double m2 = DoubleUtil.mul(y2, a2);
        double m3 = DoubleUtil.mul(y3, a3);
        double m4 = DoubleUtil.mul(y4, a4);
        double m5 = DoubleUtil.mul(y5, a5);

        double he = DoubleUtil.add(m1, m2, m3, m4, m5);
        double resVal = DoubleUtil.safeDiv(he, l);

        return genResult(resVal, areaId, beginDate,endDate);
    }
}
