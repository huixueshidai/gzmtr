/**
 * 初始化事故实例详情对话框
 */
var AccidentInfoDlg1 = {
    accidentInfoData: {}
    ,lineId:null
    , accidentTypeTreeInstance: null
     , accidentId: null
    , lineTreeInstance: null
    , stationTreeInstance: null
    , emergencyTreeInstatce: null
    , counterplanId:null
    ,uploadInstance:null
    ,processJson:null //历史案例处置流程jso数据
    ,stationdIds:[]
    ,checkCount:0
    ,itemTemplate: $("#itemTemplate").html()
    ,dispoasalList:[]     //处置流程步骤实体
};
/**
 * item获取新的ID
 * @returns {string}
 */
AccidentInfoDlg1.newId = function () {
    if(this.count == undefined){
        this.count = 0;
    }
    this.count = this.count + 1;
    return "dictItem" + this.count;
};
/**
 * 清除数据
 */
AccidentInfoDlg1.clearData = function () {
    this.accidentInfoData = {};
}

/**
 * 设置对话框中的数据
 *
 * @param key 数据的名称
 * @param val 数据的具体值
 */
AccidentInfoDlg1.set = function (key, val) {
    this.accidentInfoData[key] = (typeof val == "undefined") ? $("#" + key).val() : val;
    return this;
}

/**
 * 设置对话框中的数据
 *
 * @param key 数据的名称
 * @param val 数据的具体值
 */
AccidentInfoDlg1.get = function (key) {
    return $("#" + key).val();
};

/**
 * 关闭此对话框
 */
AccidentInfoDlg1.close = function () {
    parent.layer.close(window.parent.Accident.layerIndex);
};

/**
 * 添加条目
 */
AccidentInfoDlg1.addItem = function () {
    $("#itemsArea").append(this.itemTemplate);
    $("#dictItem").attr("id", this.newId());
};

/**
 * 删除item
 */
AccidentInfoDlg1.deleteItem = function (event) {
    var obj = Feng.eventParseObject(event);
    this.count = this.count - 1;
    obj.parent().parent().remove();
};
/**
 * 收集数据
 */
AccidentInfoDlg1.collectData = function () {
    this
        .set('id')
        .set('name')
        .set('accidentTypeId')
        .set('lineId')
        .set('stationId')
        .set('trainNumber')
        .set('interruptionTime')
        .set('trapNum')
        .set('startTime')
        .set('accidentLevel')
        .set('interruptionType')
        .set('reduction')
        .set('emergencyId')
        .set('emergencyproId')
        .set('processJson')
        .set('addType')
    ;
}

AccidentInfoDlg1.emergencyPropertyAssign = function(emergencyproId){
    $("#emergencyproId").val(emergencyproId);
}

AccidentInfoDlg1.accidentType = function () {
    return this.get('accidentTypeId');
}

/**
 * 提交添加
 */
AccidentInfoDlg1.addSubmit = function () {
    this.clearData();
    this.collectData();
    for(var i=1;i<this.count+1;i++){
        var accidentDisposal = new Object();
        accidentDisposal.step = $("#step"+i).val();
        accidentDisposal.time = $("#time"+i).val();
        accidentDisposal.disposalTask = $("#disposalTask"+i).val();
        AccidentInfoDlg1.dispoasalList.push(accidentDisposal);
    }
    var disposalJson = JSON.stringify(AccidentInfoDlg1.dispoasalList);
    AccidentInfoDlg1.set("disposalJson",disposalJson);
    var processs = "";
    var index = [-1];
    for(var i=0;i<AccidentInfoDlg1.dispoasalList.length;i++){
        var processTime = AccidentInfoDlg1.dispoasalList[i].time + AccidentInfoDlg1.dispoasalList[i].disposalTask;
        if(AccidentInfoDlg1.judgeVal(index,i)){
            for(var j=0;j<AccidentInfoDlg1.dispoasalList.length;j++){
                if(i!=j&&AccidentInfoDlg1.judgeVal(index,j)){
                    if(AccidentInfoDlg1.dispoasalList[i].time==AccidentInfoDlg1.dispoasalList[j].time){
                        processTime += AccidentInfoDlg1.dispoasalList[j].disposalTask;
                        index.push(j);
                    }
                }
            }
            processs += processTime;
        }
        else{
            continue;
        }
    }
    AccidentInfoDlg1.set("process",processs);
    //提交信息
    var ajax = new $ax(Feng.ctxPath + "/accident/addAccident", function (data) {
        Feng.success("添加完成!");
        window.parent.Accident.table.refresh();
        AccidentInfoDlg1.close();
    }, function (data) {
        Feng.error("添加失败!" + data.responseJSON.message + "!");
    });
    ajax.set(this.accidentInfoData);
    ajax.start();
}


AccidentInfoDlg1.judgeVal = function(array,value){
    for(var i =0;i<array.length;i++){
        if(array[i]==value) {
            return false;
        }
    }
    return true;
}

/**
 *提交修改
 */
AccidentInfoDlg1.editSubmit = function () {

    this.clearData();
    this.collectData();
    $("[name='dictItem']").each(function(){
        var accidentDisposal = new Object();
        accidentDisposal.step = $(this).find("[name='step']").val();
        accidentDisposal.time = $(this).find("[name='time']").val();
        accidentDisposal.disposalTask = $(this).find("[name='disposalTask']").val();
        AccidentInfoDlg1.dispoasalList.push(accidentDisposal);
    });
    var disposalJson = JSON.stringify(AccidentInfoDlg1.dispoasalList);
    AccidentInfoDlg1.set("disposalJson",disposalJson);
    var processs = "";
    var index = [-1];
    for(var i=0;i<AccidentInfoDlg1.dispoasalList.length;i++){
        var processTime = AccidentInfoDlg1.dispoasalList[i].time + AccidentInfoDlg1.dispoasalList[i].disposalTask;
        if(AccidentInfoDlg1.judgeVal(index,i)){
            for(var j=0;j<AccidentInfoDlg1.dispoasalList.length;j++){
                if(i!=j&&AccidentInfoDlg1.judgeVal(index,j)){
                    if(AccidentInfoDlg1.dispoasalList[i].time==AccidentInfoDlg1.dispoasalList[j].time){
                        processTime += AccidentInfoDlg1.dispoasalList[j].disposalTask;
                        index.push(j);
                    }
                }
            }
            processs += processTime;
        }
        else{
            continue;
        }
    }
    AccidentInfoDlg1.set("process",processs);

    //提交信息
    var ajax = new $ax(Feng.ctxPath + "/accident/update", function (data) {
        Feng.success("修改成功!");
        window.parent.Accident.table.refresh();
        AccidentInfoDlg1.close();
    }, function (data) {
        Feng.error("修改失败!" + data.responseJSON.message + "!");
    });
    ajax.set(this.accidentInfoData);
    ajax.start();
}


/**
 * 接警时选择历史案例，返回处置流程json数据（rocessjson）
 */
AccidentInfoDlg1.selectAccident = function (accidentId) {

    //提交信息
    var ajax = new $ax(Feng.ctxPath + "/accident/select", function (data) {
        AccidentInfoDlg1.processJson = data.processJson;
    }, function (data) {

    });
    ajax.set("accidentId",accidentId);
    ajax.start();
}


/**
 * 显示tree
 */
AccidentInfoDlg1.showAccidentTypeSelectTree = function () {
    Feng.showInputTree("accidentTypeName", "accidentTypeTreeDiv", 15, 34);
}
AccidentInfoDlg1.onClickAccidentType = function (e, treeId, treeNode) {
    $("#accidentTypeName").attr("value", AccidentInfoDlg1.accidentTypeTreeInstance.getSelectedVals());
    $("#accidentTypeId").attr("value", treeNode.id);
    $("#accidentTypeTreeDiv").fadeOut("fast");
}
AccidentInfoDlg1.showLineTree = function () {
    Feng.showInputTree("lineName", "lineTreeDiv", 15, 34);
}
AccidentInfoDlg1.onClickLine = function (e, treeId, treeNode) {
    if(treeNode.isParent){
        Feng.info("请选择线路");
        return;
    }

    $("#lineName").attr("value", AccidentInfoDlg1.lineTreeInstance.getSelectedVal());
    $("#lineId").attr("value", treeNode.id);

    $("#stationName").attr("value", "");
    $("#stationId").attr("value", "");
    AccidentInfoDlg1.initStationTree(treeNode.id);
    $("#lineTreeDiv").fadeOut("fast");
}

AccidentInfoDlg1.initStationTree = function (lineId) {
    var stationTree = new $ZTree("stationTree","/roadnet/station/tree/"+lineId);
    stationTree.setSettings({
        view : {
            dblClickExpand : true,
            selectedMulti : false
        },
        data : {simpleData : {enable : true}},
        check: {
            enable: true,
            chkStyle: "checkbox",
            chkboxType: { "Y": "", "N": "" }
        },
        callback : {
            onCheck:AccidentInfoDlg1.onClickStation
        }
    });
    stationTree.Check(AccidentInfoDlg1.onClickStation);
    stationTree.init();
    AccidentInfoDlg1.stationTreeInstance = stationTree;
}

AccidentInfoDlg1.showStationTree = function () {
    if(!AccidentInfoDlg1.stationTreeInstance){
        Feng.info("请选择线路");
    }
    Feng.showInputTree("stationName", "stationTreeDiv", 15, 34);
}
AccidentInfoDlg1.onClickStation = function (e, treeId, treeNode) {
    if(treeNode.checked){
        AccidentInfoDlg1.checkCount++;
        if(AccidentInfoDlg1.checkCount==1){
            $("#stationName").val(treeNode.name);
            AccidentInfoDlg1.stationdIds.push(treeNode.id);
        }
        if(AccidentInfoDlg1.checkCount==2){
            $("#stationName").val($("#stationName").val()+">>"+treeNode.name);
            AccidentInfoDlg1.stationdIds.push(treeNode.id);
        }
    }
    else{
        AccidentInfoDlg1.checkCount--;
        if(AccidentInfoDlg1.checkCount==0){
            $("#stationName").val(null);
            AccidentInfoDlg1.removeByValue(AccidentInfoDlg1.stationdIds,treeNode.id);
        }
        if(AccidentInfoDlg1.checkCount==1){
            $("#stationName").val(AccidentInfoDlg1.stationTreeInstance.getSelectedCheck());
            AccidentInfoDlg1.removeByValue(AccidentInfoDlg1.stationdIds,treeNode.id);
        }
    }
    if( AccidentInfoDlg1.stationdIds.length>0) {
        var stationId = "";
        for (var i = 0; i < AccidentInfoDlg1.stationdIds.length; i++) {
            stationId += AccidentInfoDlg1.stationdIds[i] + "/";
        }
        $("#stationId").attr("value", stationId);
    }

    if( AccidentInfoDlg1.checkCount==2){
        $("#stationTreeDiv").fadeOut("fast");
    }
}

/**
 * 突发事件类型
 */
AccidentInfoDlg1.showEmergencySelectTree = function () {
    Feng.showInputTree("emergencyName", "emergencyTreeDiv", 15, 34);
}

AccidentInfoDlg1.onClickEmergency = function (e, treeId, treeNode) {
    $("#emergencyName").attr("value", AccidentInfoDlg1.emergencyTreeInstatce.getSelectedVals());
    $("#emergencyId").attr("value", treeNode.id);
    $("#emergencyTreeDiv").fadeOut("fast");
}

/**
 * 突发事件属性选择弹出页面
 */
AccidentInfoDlg1.emergencyPropertySel = function () {
    var emergencyproId = $("#emergencyproId").val();
    if(emergencyproId){
        var index = layer.open({
            type: 2,
            title: '请填写突发事件属性值',
            area: ['1200px', '600px'], //宽高
            fix: false, //不固定
            maxmin: true,
            content: Feng.ctxPath + '/accidentEmergencyproperty/edit_accidentEmergencyproperty_update/'+emergencyproId
        });
    }
    else{
        var index = layer.open({
            type: 2,
            title: '请填写突发事件属性值',
            area: ['1200px', '600px'], //宽高
            fix: false, //不固定
            maxmin: true,
            content: Feng.ctxPath + '/accidentEmergencyproperty/add_accidentEmergencyproperty'
        });
    }

    this.layerIndex = index;
    layer.full(index);
};

/**
 * 删除数组指定数据
 * @param arr
 * @param val
 */
AccidentInfoDlg1.removeByValue = function(arr,val){
    for(var i=0;i<arr.length;i++){
        if(arr[i] == val){
            arr.splice(i, 1);
            break;
        }
    }
}

/**
 * 添加每步骤的处置流程
 */
AccidentInfoDlg1.addDispoalprocess = function(){
    $("#itemsArea").append(this.itemTemplate);
    $("#dictItem").attr("id", this.newId());
    $("#step").attr("id", "step"+this.count);
    $("#time").attr("id", "time"+this.count);
    $("#disposalTask").attr("id", "disposalTask"+this.count);
}

/**
 * 删除item
 */
AccidentInfoDlg1.deleteItem = function (event) {
    var obj = Feng.eventParseObject(event);
    obj.parent().parent().remove();
};

$(function () {
    Feng.initValidator("accidentInfoForm", AccidentInfoDlg1.validateFields);

    var accidentTypeTree = new $ZTree("accidentTypeTree", "/accidentType/tree");
    accidentTypeTree.bindOnClick(AccidentInfoDlg1.onClickAccidentType);
    accidentTypeTree.init();
    AccidentInfoDlg1.accidentTypeTreeInstance = accidentTypeTree;

    var lineTree = new $ZTree("lineTree", "/roadnet/line/tree");
    lineTree.bindOnClick(AccidentInfoDlg1.onClickLine);
    lineTree.init();
    AccidentInfoDlg1.lineTreeInstance = lineTree;

    var emergencyTree = new $ZTree("emergencyTree", "/emergency/tree");
    emergencyTree.bindOnClick(AccidentInfoDlg1.onClickEmergency);
    emergencyTree.init();
    AccidentInfoDlg1.emergencyTreeInstatce = emergencyTree;

    if(disposalJsonE){
        var jsonObj =  JSON.parse(disposalJsonE);//转换为json数组对象
        for(var i=0;i<jsonObj.length;i++){
            AccidentInfoDlg1.addDispoalprocess();
            $("#step"+(i+1)).val(jsonObj[i].step);
            $("#time"+(i+1)).val(jsonObj[i].time);
            $("#disposalTask"+(i+1)).val(jsonObj[i].disposalTask);
        }
    }


});



