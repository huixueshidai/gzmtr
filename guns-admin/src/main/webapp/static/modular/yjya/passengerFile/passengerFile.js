/**
 * 客流更新管理初始化
 */
var PassengerFile = {
    id: "PassengerFileTable",	//表格id
    seItem: null,		//选中的条目
    table: null,
    layerIndex: -1,
    uploadInstance:null
};

/**
 * 初始化表格的列
 */
PassengerFile.initColumn = function () {
    return [
        {field: 'selectItem', radio: true},
            {title: 'id', field: 'id', visible: true, align: 'center', valign: 'middle'},
            {title: '客流分类 ', field: 'dateStatusName', visible: true, align: 'center', valign: 'middle'},
            {title: '客流文件', field: 'passengerFile', visible: true, align: 'center', valign: 'middle',formatter:function (value, row, index, field) {
                return "<a href='#' onclick='PassengerFile.export("+row.id+")'>客流文件</a>";
            }}
    ];
};

/**
 * 检查是否选中
 */
PassengerFile.check = function () {
    var selected = $('#' + this.id).bootstrapTable('getSelections');
    if(selected.length == 0){
        Feng.info("请先选中表格中的某一记录！");
        return false;
    }else{
        PassengerFile.seItem = selected[0];
        return true;
    }
};

/**
 * 点击添加客流更新
 */
PassengerFile.openAddPassengerFile = function () {
    var index = layer.open({
        type: 2,
        title: '添加客流更新',
        area: ['800px', '420px'], //宽高
        fix: false, //不固定
        maxmin: true,
        content: Feng.ctxPath + '/passengerFile/passengerFile_add'
    });
    this.layerIndex = index;
};

/**
 * 打开查看客流更新详情
 */
PassengerFile.openPassengerFileDetail = function () {
    if (this.check()) {
        var index = layer.open({
            type: 2,
            title: '客流更新详情',
            area: ['800px', '420px'], //宽高
            fix: false, //不固定
            maxmin: true,
            content: Feng.ctxPath + '/passengerFile/passengerFile_update/' + PassengerFile.seItem.id
        });
        this.layerIndex = index;
    }
};

/**
 * 删除客流更新
 */
PassengerFile.delete = function () {
    if (this.check()) {
        var ajax = new $ax(Feng.ctxPath + "/passengerFile/delete", function (data) {
            Feng.success("删除成功!");
            PassengerFile.table.refresh();
        }, function (data) {
            Feng.error("删除失败!" + data.responseJSON.message + "!");
        });
        ajax.set("passengerFileId",this.seItem.id);
        ajax.start();
    }
};

/**
 * 查询客流更新列表
 */
PassengerFile.search = function () {
    var queryData = {};
    queryData['condition'] = $("#condition").val();
    PassengerFile.table.refresh({query: queryData});
};

PassengerFile.export = function(id){
    window.location.href = Feng.ctxPath + "/passengerFile/exportExcel/"+id;
}

PassengerFile.exportXlsByM = function () {
    window.location.href = Feng.ctxPath + "/passengerFile/exportXlsByM";
}


$(function () {
    var defaultColunms = PassengerFile.initColumn();
    var table = new BSTable(PassengerFile.id, "/passengerFile/list", defaultColunms);
    table.setPaginationType("server");
    PassengerFile.table = table.init();
    var upload = new $WebUpload({
        uploadId:"upload",
        mimeTypes: "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet,application/vnd.ms-excel",
        extensions: "xlsx,xls",
        url:"/passengerFile/importExcel",
        multiple:true
    });
    upload.init();
    PassengerFile.uploadInstance = upload;
});
