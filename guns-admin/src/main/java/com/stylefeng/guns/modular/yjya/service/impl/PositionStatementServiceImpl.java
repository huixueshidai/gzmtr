package com.stylefeng.guns.modular.yjya.service.impl;

import com.stylefeng.guns.common.persistence.model.PositionStatement;
import com.stylefeng.guns.common.persistence.dao.PositionStatementMapper;
import com.stylefeng.guns.common.plugin.service.impl.BaseTreeableServiceImpl;
import com.stylefeng.guns.modular.yjya.service.IPositionStatementService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author 殷佳成
 * @since 2018-09-07
 */
@Service
public class PositionStatementServiceImpl extends BaseTreeableServiceImpl<PositionStatementMapper, PositionStatement> implements IPositionStatementService {

}
