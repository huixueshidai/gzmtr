package com.stylefeng.guns.modular.assessment.calculate.road.passenger;

import com.stylefeng.guns.common.persistence.model.Assessment;
import com.stylefeng.guns.core.util.DoubleUtil;
import com.stylefeng.guns.modular.assessment.calculate.ThirdIndicationCalculator;
import com.stylefeng.guns.modular.assessment.calculate.aware.RoadnetThirdIndicationAware;
import org.springframework.stereotype.Component;

import java.util.Date;
import java.util.List;

/**
 * 路网线路间能力匹配度
 *
 * @author yinjc
 * @since 2018/05/11
 */
@Component
public class RoadBetweenTheRegionalCenterLinesCal extends ThirdIndicationCalculator implements RoadnetThirdIndicationAware {
    @Override
    protected Integer getIndicationId() {
        return 96;
    }

    @Override
    public Assessment calculate(Integer roadId, Date beginDate, Date endDate) {
        //区段断面满载率
        List<Double> fullLoadList = getMulDoubleValue(roadId, 321, beginDate,endDate);
        //区段前一区段断面满载率(路网)
        List<Double> preFullLoadList = getMulDoubleValue(roadId, 322, beginDate,endDate);
        //换乘站间各换乘方向换乘量占线路换乘总量的(路网)
        List<Double> k = getMulDoubleValue(roadId, 323, beginDate,endDate);
        //区段断面满载率权重(路网)
        Double w1 = getDoubleValue(roadId, 324, beginDate,endDate);
        //区段前一区段断面满载率权重(路网)
        Double w2 = getDoubleValue(roadId, 325, beginDate,endDate);

        Integer minSize = Math.min(
                Math.min(fullLoadList.size(), preFullLoadList.size()), k.size());
        Double resVal = 0d;
        for (int i = 0; i < minSize; i++) {
            double m = DoubleUtil.mul(w1, fullLoadList.get(i));
            double m2 = DoubleUtil.sub(fullLoadList.get(i), preFullLoadList.get(i));
            double m3 = DoubleUtil.mul(w2, m2);
            double resV = DoubleUtil.add(m, m3);
            double val = DoubleUtil.mul(resV, k.get(i));
            resVal = DoubleUtil.add(val, resVal);
        }
        return genResult(resVal, roadId, beginDate,endDate);
    }
}
