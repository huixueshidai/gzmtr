/**
 * 公交车司机管理初始化
 */
var Drivers = {
    id: "DriversTable",	//表格id
    seItem: null,		//选中的条目
    table: null,
    layerIndex: -1
};

/**
 * 初始化表格的列
 */
Drivers.initColumn = function () {
    return [
        {field: 'selectItem', radio: true},
            {title: '', field: 'id', visible: true, align: 'center', valign: 'middle'},
            {title: '姓名', field: 'name', visible: true, align: 'center', valign: 'middle'},
            {title: '车牌号', field: 'plateNum', visible: true, align: 'center', valign: 'middle'},
            {title: '联系电话', field: 'telphone', visible: true, align: 'center', valign: 'middle'}
    ];
};

/**
 * 检查是否选中
 */
Drivers.check = function () {
    var selected = $('#' + this.id).bootstrapTable('getSelections');
    if(selected.length == 0){
        Feng.info("请先选中表格中的某一记录！");
        return false;
    }else{
        Drivers.seItem = selected[0];
        return true;
    }
};

/**
 * 点击添加公交车司机
 */
Drivers.openAddDrivers = function () {
    var index = layer.open({
        type: 2,
        title: '添加公交车司机',
        area: ['800px', '420px'], //宽高
        fix: false, //不固定
        maxmin: true,
        content: Feng.ctxPath + '/drivers/drivers_add'
    });
    this.layerIndex = index;
};

/**
 * 打开查看公交车司机详情
 */
Drivers.openDriversDetail = function () {
    if (this.check()) {
        var index = layer.open({
            type: 2,
            title: '公交车司机详情',
            area: ['800px', '420px'], //宽高
            fix: false, //不固定
            maxmin: true,
            content: Feng.ctxPath + '/drivers/drivers_update/' + Drivers.seItem.id
        });
        this.layerIndex = index;
    }
};

/**
 * 删除公交车司机
 */
Drivers.delete = function () {
    if (this.check()) {
        var ajax = new $ax(Feng.ctxPath + "/drivers/delete", function (data) {
            Feng.success("删除成功!");
            Drivers.table.refresh();
        }, function (data) {
            Feng.error("删除失败!" + data.responseJSON.message + "!");
        });
        ajax.set("driversId",this.seItem.id);
        ajax.start();
    }
};

/**
 * 查询公交车司机列表
 */
Drivers.search = function () {
    var queryData = {};
    queryData['condition'] = $("#condition").val();
    Drivers.table.refresh({query: queryData});
};

$(function () {
    var defaultColunms = Drivers.initColumn();
    var table = new BSTable(Drivers.id, "/drivers/list", defaultColunms);
    table.setPaginationType("client");
    Drivers.table = table.init();
});
