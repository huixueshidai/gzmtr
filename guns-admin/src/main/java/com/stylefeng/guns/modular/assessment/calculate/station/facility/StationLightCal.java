package com.stylefeng.guns.modular.assessment.calculate.station.facility;

import com.stylefeng.guns.common.persistence.model.Assessment;
import com.stylefeng.guns.core.util.DoubleUtil;
import com.stylefeng.guns.modular.assessment.calculate.ThirdIndicationCalculator;
import com.stylefeng.guns.modular.assessment.calculate.aware.StationThirdIndicationAware;
import org.springframework.stereotype.Component;

import java.util.Date;

/**
 * 照明系统安全指数
 */
@Component
public class StationLightCal extends ThirdIndicationCalculator implements StationThirdIndicationAware {

    @Override
    protected Integer getIndicationId() {
        return 30;
    }

    @Override
    public Assessment calculate(Integer stationId, Date beginDate, Date endDate) {
        Double num = getDoubleValue(stationId, 52, beginDate,endDate);
        Double total = getDoubleValue(stationId, 53, beginDate,endDate);
        if (num == 0d && total == 0d) {
            return genResult(1d, stationId, beginDate,endDate);
        }
        Double resultValue = DoubleUtil.sub(1, DoubleUtil.safeDiv(num, total));
        return genResult(resultValue, stationId, beginDate,endDate);
    }
}
