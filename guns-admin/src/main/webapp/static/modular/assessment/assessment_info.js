/**
 * 初始化指标评估详情对话框
 */
var AssessmentInfoDlg = {
    assessmentInfoData : {}
};

/**
 * 清除数据
 */
AssessmentInfoDlg.clearData = function() {
    this.assessmentInfoData = {};
}

/**
 * 设置对话框中的数据
 *
 * @param key 数据的名称
 * @param val 数据的具体值
 */
AssessmentInfoDlg.set = function(key, val) {
    this.assessmentInfoData[key] = (typeof val == "undefined") ? $("#" + key).val() : val;
    return this;
}

/**
 * 设置对话框中的数据
 *
 * @param key 数据的名称
 * @param val 数据的具体值
 */
AssessmentInfoDlg.get = function(key) {
    return $("#" + key).val();
}

/**
 * 关闭此对话框
 */
AssessmentInfoDlg.close = function() {
    parent.layer.close(window.parent.Assessment.layerIndex);
}

/**
 * 收集数据
 */
AssessmentInfoDlg.collectData = function() {
    this
    .set('id')
    .set('indicationId')
    .set('assessmentValue')
    .set('beginTime')
    ;
}

/**
 * 提交添加
 */
AssessmentInfoDlg.addSubmit = function() {

    this.clearData();
    this.collectData();

    //提交信息
    var ajax = new $ax(Feng.ctxPath + "/assessment/add", function(data){
        Feng.success("添加成功!");
        window.parent.Assessment.table.refresh();
        AssessmentInfoDlg.close();
    },function(data){
        Feng.error("添加失败!" + data.responseJSON.message + "!");
    });
    ajax.set(this.assessmentInfoData);
    ajax.start();
}

/**
 * 提交修改
 */
AssessmentInfoDlg.editSubmit = function() {

    this.clearData();
    this.collectData();

    //提交信息
    var ajax = new $ax(Feng.ctxPath + "/assessment/update", function(data){
        Feng.success("修改成功!");
        window.parent.Assessment.table.refresh();
        AssessmentInfoDlg.close();
    },function(data){
        Feng.error("修改失败!" + data.responseJSON.message + "!");
    });
    ajax.set(this.assessmentInfoData);
    ajax.start();
}

$(function() {

});
