package com.stylefeng.guns.modular.yjya.dao;

import com.stylefeng.guns.common.persistence.model.CounterplanProcess;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface CounterplanProcessDao {
    void deleteBycounterplanId(@Param("counterplanId") Integer counterplanId);

    List<CounterplanProcess> selectListByCounterplanId(Integer counterplanId);
}
