package com.stylefeng.guns.modular.yjya.service;

import com.stylefeng.guns.common.persistence.model.Accident;
import com.baomidou.mybatisplus.service.IService;

/**
 * <p>
 * 事故实例 服务类
 * </p>
 *
 * @author xuziyang
 * @since 2018-01-22
 */
public interface IAccidentService extends IService<Accident> {

}
