package com.stylefeng.guns.modular.assessment.calculate.area.facility;

import com.stylefeng.guns.common.persistence.model.Assessment;
import com.stylefeng.guns.modular.assessment.calculate.ThirdIndicationCalculator;
import com.stylefeng.guns.modular.assessment.calculate.area.facility.util.AreaRoadFacilityCalculatorUtil;
import com.stylefeng.guns.modular.assessment.calculate.aware.AreaThirdIndicationAware;
import com.stylefeng.guns.modular.assessment.calculate.line.facility.LineBeaconageCal;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Date;
import java.util.List;

/**
 * 信号系统影响运营风险指数
 *
 * @author yinjc
 * @since 2018/05/11
 */
@Component
public class AreaSignalCal extends ThirdIndicationCalculator implements AreaThirdIndicationAware {

    @Autowired
    private LineBeaconageCal lineBeaconageCal;

    @Override
    protected Integer getIndicationId() {
        return 80;
    }

    @Override
    public Assessment calculate(Integer areaId, Date beginDate, Date endDate) {
        List<Double> wList = getMulDoubleValue(areaId, 290, beginDate,endDate);
        Double resVal = AreaRoadFacilityCalculatorUtil.calculate(areaId, lineBeaconageCal, wList, beginDate,endDate);
        return genResult(resVal, areaId, beginDate,endDate);
    }
}
