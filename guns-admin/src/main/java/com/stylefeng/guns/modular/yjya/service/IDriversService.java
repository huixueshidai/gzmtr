package com.stylefeng.guns.modular.yjya.service;

import com.stylefeng.guns.common.persistence.model.Drivers;
import com.baomidou.mybatisplus.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author wangshuaikang
 * @since 2018-09-04
 */
public interface IDriversService extends IService<Drivers> {
	
}
