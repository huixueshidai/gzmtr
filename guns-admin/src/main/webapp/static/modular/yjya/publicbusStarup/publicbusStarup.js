/**
 * 公交联动启动管理初始化
 */
var PublicbusStarup = {
    id: "PublicbusStarupTable",	//表格id
    seItem: null,		//选中的条目
    table: null,
    layerIndex: -1
};

/**
 * 初始化表格的列
 */
PublicbusStarup.initColumn = function () {
    return [
        {field: 'selectItem', radio: true},
            {title: '', field: 'id', visible: true, align: 'center', valign: 'middle'},
            {title: '事故线路', field: 'lineId', visible: true, align: 'center', valign: 'middle'},
            {title: '事故车站', field: 'stationId', visible: true, align: 'center', valign: 'middle'},
            {title: '乘客滞留时间', field: 'retentionTime', visible: true, align: 'center', valign: 'middle'},
            {title: '预计中断时间', field: 'interruptionTime', visible: true, align: 'center', valign: 'middle'},
            {title: '预计晚点时间', field: 'expectDelays', visible: true, align: 'center', valign: 'middle'},
            {title: '预计行车能力降低', field: 'reduction', visible: true, align: 'center', valign: 'middle'},
            {title: '中断类型', field: 'interruptionType', visible: true, align: 'center', valign: 'middle'},
            {title: '调度方式', field: 'schedulingMode', visible: true, align: 'center', valign: 'middle'},
            {title: '行车间隔', field: 'schedulingModeMin', visible: true, align: 'center', valign: 'middle'},
            {title: '滞留客流损失系数', field: 'lossEfficiencyRetention', visible: true, align: 'center', valign: 'middle'},
            {title: '疏运客流损失系数', field: 'lossEfficiencyTransportation', visible: true, align: 'center', valign: 'middle'},
            {title: '发生时间', field: 'startTime', visible: true, align: 'center', valign: 'middle'},
            {title: '用户id', field: 'userId', visible: true, align: 'center', valign: 'middle'}
    ];
};

/**
 * 检查是否选中
 */
PublicbusStarup.check = function () {
    var selected = $('#' + this.id).bootstrapTable('getSelections');
    if(selected.length == 0){
        Feng.info("请先选中表格中的某一记录！");
        return false;
    }else{
        PublicbusStarup.seItem = selected[0];
        return true;
    }
};

/**
 * 点击添加公交联动启动
 */
PublicbusStarup.openAddPublicbusStarup = function () {
    var index = layer.open({
        type: 2,
        title: '添加公交联动启动',
        area: ['800px', '420px'], //宽高
        fix: false, //不固定
        maxmin: true,
        content: Feng.ctxPath + '/publicbusStarup/publicbusStarup_add'
    });
    this.layerIndex = index;
};

/**
 * 打开查看公交联动启动详情
 */
PublicbusStarup.openPublicbusStarupDetail = function () {
    if (this.check()) {
        var index = layer.open({
            type: 2,
            title: '公交联动启动详情',
            area: ['800px', '420px'], //宽高
            fix: false, //不固定
            maxmin: true,
            content: Feng.ctxPath + '/publicbusStarup/publicbusStarup_update/' + PublicbusStarup.seItem.id
        });
        this.layerIndex = index;
    }
};

/**
 * 删除公交联动启动
 */
PublicbusStarup.delete = function () {
    if (this.check()) {
        var ajax = new $ax(Feng.ctxPath + "/publicbusStarup/delete", function (data) {
            Feng.success("删除成功!");
            PublicbusStarup.table.refresh();
        }, function (data) {
            Feng.error("删除失败!" + data.responseJSON.message + "!");
        });
        ajax.set("publicbusStarupId",this.seItem.id);
        ajax.start();
    }
};

/**
 * 查询公交联动启动列表
 */
PublicbusStarup.search = function () {
    var queryData = {};
    queryData['condition'] = $("#condition").val();
    PublicbusStarup.table.refresh({query: queryData});
};

$(function () {
    var defaultColunms = PublicbusStarup.initColumn();
    var table = new BSTable(PublicbusStarup.id, "/publicbusStarup/list", defaultColunms);
    table.setPaginationType("client");
    PublicbusStarup.table = table.init();
});
