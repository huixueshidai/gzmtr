package com.stylefeng.guns.common.persistence.model;

import java.io.Serializable;

import com.baomidou.mybatisplus.enums.IdType;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableName;
import java.io.Serializable;

/**
 * <p>
 * 公交站和车辆类型
 * </p>
 *
 * @author yinjc
 * @since 2018-03-21
 */
@TableName("gzmtr_hub_bus")
public class HubBus extends Model<HubBus> {

    private static final long serialVersionUID = 1L;

	@TableId(value="id", type= IdType.AUTO)
	private Integer id;
    /**
     * 车辆类型id
     */
	@TableField("bus_type_id")
	private Integer busTypeId;
    /**
     * 公交枢纽id
     */
	@TableField("hub_id")
	private Integer hubId;
    /**
     * 车辆数量
     */
	private Integer num;


	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getBusTypeId() {
		return busTypeId;
	}

	public void setBusTypeId(Integer busTypeId) {
		this.busTypeId = busTypeId;
	}

	public Integer getHubId() {
		return hubId;
	}

	public void setHubId(Integer hubId) {
		this.hubId = hubId;
	}

	public Integer getNum() {
		return num;
	}

	public void setNum(Integer num) {
		this.num = num;
	}

	@Override
	protected Serializable pkVal() {
		return this.id;
	}

	@Override
	public String toString() {
		return "HubBus{" +
			"id=" + id +
			", busTypeId=" + busTypeId +
			", hubId=" + hubId +
			", num=" + num +
			"}";
	}
}
