package com.stylefeng.guns.modular.assessment.calculate.line.employee;

import com.stylefeng.guns.core.util.SpringContextHolder;
import com.stylefeng.guns.modular.assessment.calculate.SecondIndicationCalculator;
import com.stylefeng.guns.modular.assessment.calculate.ThirdIndicationCalculator;
import com.stylefeng.guns.modular.assessment.calculate.aware.LineSecondIndicationAware;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import java.util.Arrays;
import java.util.List;

@Component
@Scope(ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public class LineEmployeeCal extends SecondIndicationCalculator implements LineSecondIndicationAware {

    private List<ThirdIndicationCalculator> thirdIndicationCalculatorList;

    public LineEmployeeCal() {
        thirdIndicationCalculatorList = Arrays.asList(
                SpringContextHolder.getBean(LineQualityEmployeeCal.class),
                SpringContextHolder.getBean(LineTechnicalStandardCal.class),
                SpringContextHolder.getBean(LineTwoJiACal.class)
        );

    }
    @Override
    public List<ThirdIndicationCalculator> getThirdIndicationCalculatorList() {
        return thirdIndicationCalculatorList;
    }


    @Override
    protected Integer getIndicationId() {
        return 50;
    }
}
