package com.stylefeng.guns.common.persistence.dao;

import com.stylefeng.guns.common.persistence.model.CounterplanAccident;
import com.baomidou.mybatisplus.mapper.BaseMapper;

/**
 * <p>
  * 预案处置流程表 Mapper 接口
 * </p>
 *
 * @author xuziyang
 * @since 2018-01-22
 */
public interface CounterplanAccidentMapper extends BaseMapper<CounterplanAccident> {

}