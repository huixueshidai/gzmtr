/**
 * 客流管理管理初始化
 */
var PassengerNum = {
    id: "PassengerNumTable",	//表格id
    seItem: null,		//选中的条目
    table: null,
    layerIndex: -1
};

/**
 * 初始化表格的列
 */
PassengerNum.initColumn = function () {
    return [
        {field: 'selectItem', radio: true},
            {title: '', field: 'id', visible: true, align: 'center', valign: 'middle'},
            {title: '时间', field: 'time', visible: true, align: 'center', valign: 'middle'},
            {title: '起始线路', field: 'startLine', visible: true, align: 'center', valign: 'middle'},
            {title: '起始点', field: 'startStation', visible: true, align: 'center', valign: 'middle'},
            {title: '终到线路', field: 'endLine', visible: true, align: 'center', valign: 'middle'},
            {title: '终到点', field: 'endStation', visible: true, align: 'center', valign: 'middle'},
            {title: '客流/15min', field: 'passengerNum', visible: true, align: 'center', valign: 'middle'}
    ];
};

/**
 * 检查是否选中
 */
PassengerNum.check = function () {
    var selected = $('#' + this.id).bootstrapTable('getSelections');
    if(selected.length == 0){
        Feng.info("请先选中表格中的某一记录！");
        return false;
    }else{
        PassengerNum.seItem = selected[0];
        return true;
    }
};

/**
 * 点击添加客流管理
 */
PassengerNum.openAddPassengerNum = function () {
    var index = layer.open({
        type: 2,
        title: '添加客流管理',
        area: ['800px', '420px'], //宽高
        fix: false, //不固定
        maxmin: true,
        content: Feng.ctxPath + '/passengerNum/passengerNum_add'
    });
    this.layerIndex = index;
};

/**
 * 打开查看客流管理详情
 */
PassengerNum.openPassengerNumDetail = function () {
    if (this.check()) {
        var index = layer.open({
            type: 2,
            title: '客流管理详情',
            area: ['800px', '420px'], //宽高
            fix: false, //不固定
            maxmin: true,
            content: Feng.ctxPath + '/passengerNum/passengerNum_update/' + PassengerNum.seItem.id
        });
        this.layerIndex = index;
    }
};

/**
 * 删除客流管理
 */
PassengerNum.delete = function () {
    if (this.check()) {
        var ajax = new $ax(Feng.ctxPath + "/passengerNum/delete", function (data) {
            Feng.success("删除成功!");
            PassengerNum.table.refresh();
        }, function (data) {
            Feng.error("删除失败!" + data.responseJSON.message + "!");
        });
        ajax.set("passengerNumId",this.seItem.id);
        ajax.start();
    }
};

/**
 * 查询客流管理列表
 */
PassengerNum.search = function () {
    var queryData = {};
    queryData['condition'] = $("#condition").val();
    PassengerNum.table.refresh({query: queryData});
};

$(function () {
    var defaultColunms = PassengerNum.initColumn();
    var table = new BSTable(PassengerNum.id, "/passengerNum/list", defaultColunms);
    table.setPaginationType("client");
    PassengerNum.table = table.init();
});
