package com.stylefeng.guns.modular.yjya.service;

import com.stylefeng.guns.common.persistence.model.Resource;
import com.baomidou.mybatisplus.service.IService;

import java.util.List;

/**
 * <p>
 * 应急资源 服务类
 * </p>
 *
 * @author yinjc
 * @since 2018-02-23
 */
public interface IResourceService extends IService<Resource> {
}
