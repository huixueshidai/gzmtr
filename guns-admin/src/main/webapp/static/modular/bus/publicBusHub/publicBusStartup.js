/**
 * 公交应急联动启动
 */
var PublicBusStartup = {
    lineTreeInstance:null,
    stationTreeInstance:null,
    checkCount:0,
    stationdIds:[]
}

/**
 * 判断是否符合公交应急联动条件
 * @returns {boolean}
 */
PublicBusStartup.startUp = function(){

    if($("#reduction").val()>=60){ //预计行车能力降低>=60
        PublicBusStartup.toPublicBusExecute();
    }
    else if($("#expectDelays").val()>=40){ //预计晚点》=40
        PublicBusStartup.toPublicBusExecute();
    }
    else if(($("#interruptionType").val()==2)&&($("#interruptionTime").val()>=20)){ //双向中断，中断时间大于20
        PublicBusStartup.toPublicBusExecute();
    }
    else if(($("#interruptionType").val()==1)&&($("#schedulingMode").val()==1)&&($("#schedulingModeMin").val()>=20)&&($("#interruptionTime").val()>=20)){
        PublicBusStartup.toPublicBusExecute();
    }
    else{
        Feng.info("未满足公交应急联动启动条件");
        return false;
    }
}

/**
 * 启动公交应急跳转公交应急联动执行
 */
PublicBusStartup.toPublicBusExecute = function(){

    var ajax = new $ax(Feng.ctxPath + "/alarm/updateAlarmBusStar", function (data) {
        window.parent.document.getElementById("menu_317").click();
    }, function (data) {
        Feng.error("提交失败!" + data.responseJSON.message + "!");
    });
    ajax.start();

}

/**
 * 返回接警页面
 */
PublicBusStartup.backToAcicident = function(){
    window.parent.document.getElementById("menu_312").click();
}

/**
 * 根据数组的值删除
 * @param arr
 * @param val
 */
PublicBusStartup.removeByValue = function(arr,val){
    for(var i=0;i<arr.length;i++){
        if(arr[i] == val){
            arr.splice(i, 1);
            break;
        }
    }
}


/**
 * 事故线路
 */
PublicBusStartup.showLineTree = function () {
    Feng.showInputTree("lineName", "lineTreeDiv", 15, 34);
}
PublicBusStartup.onClickLine = function (e, treeId, treeNode) {
    if(treeNode.isParent){
        Feng.info("请选择线路");
        return;
    }
    $("#lineName").attr("value", PublicBusStartup.lineTreeInstance.getSelectedVal());
    $("#lineId").attr("value", treeNode.id);

    $("#stationName").attr("value", "");
    $("#stationId").attr("value", "");
    PublicBusStartup.initStationTree(treeNode.id);
    $("#lineTreeDiv").fadeOut("fast");

}

/**
 * 初始化车站数据
 * @param lineId
 */
PublicBusStartup.initStationTree = function (lineId) {
    var stationTree = new $ZTree("stationTree","/roadnet/station/tree/"+lineId);
    stationTree.setSettings({
        view : {
            dblClickExpand : true,
            selectedMulti : false
        },
        data : {simpleData : {enable : true}},
        check: {
            enable: true,
            chkStyle: "checkbox",
            chkboxType: { "Y": "", "N": "" }
        },
        callback : {
            onCheck:PublicBusStartup.onClickStation
        }
    });
    stationTree.Check(PublicBusStartup.onClickStation);
    stationTree.init();
    PublicBusStartup.stationTreeInstance = stationTree;
}

PublicBusStartup.onClickStation = function (e, treeId, treeNode) {
    if(treeNode.checked){
        PublicBusStartup.checkCount++;
        if(PublicBusStartup.checkCount==1){
            $("#stationName").val(treeNode.name);
            PublicBusStartup.stationdIds.push(treeNode.id);
        }
        if(PublicBusStartup.checkCount==2){
            $("#stationName").val($("#stationName").val()+">>"+treeNode.name);
            PublicBusStartup.stationdIds.push(treeNode.id);
        }
    }
    else{
        PublicBusStartup.checkCount--;
        if(PublicBusStartup.checkCount==0){
            $("#stationName").val(null);
            PublicBusStartup.removeByValue(PublicBusStartup.stationdIds,treeNode.id);
        }
        if(PublicBusStartup.checkCount==1){
            $("#stationName").val(PublicBusStartup.stationTreeInstance.getSelectedCheck());
            PublicBusStartup.removeByValue(PublicBusStartup.stationdIds,treeNode.id);
        }
    }
    if( PublicBusStartup.stationdIds.length>0) {
        var stationId = "";
        for (var i = 0; i < PublicBusStartup.stationdIds.length; i++) {
            stationId += PublicBusStartup.stationdIds[i] + "/";
        }
        $("#stationId").attr("value", stationId);
    }

    if( PublicBusStartup.checkCount==2){
        $("#stationTreeDiv").fadeOut("fast");
    }
}

/**
 * 事故车站选择
 */
PublicBusStartup.showStationTree = function () {
    if(!PublicBusStartup.stationTreeInstance){
        Feng.info("请选择线路");
    }
    Feng.showInputTree("stationName", "stationTreeDiv", 15, 34);
}

$(function() {

    var lineTree = new $ZTree("lineTree", "/roadnet/line/tree");
    lineTree.bindOnClick(PublicBusStartup.onClickLine);
    lineTree.init();
    PublicBusStartup.lineTreeInstance = lineTree;

    //判断上下行
    var stream = $("#stream").val();
    if(stream==1){
        $("#upstream").attr("checked","checked");
    }
    else if(stream==2){
        $("#downstreeam").attr("checked","checked");
    }
    else{
        $("#upstream").attr("checked",false);
        $("#downstreeam").attr("checked",false);
    }

    $("#upstream").change(function(){
        if($("#upstream").prop('checked')){
            $("#stream").val(1);
        }
        if($("#downstreeam").prop('checked')){
            $("#downstreeam").attr("checked",false);
        }
    });
    $("#downstreeam").change(function(){
        if($("#upsdownstreeamtream").prop('checked')){
            $("#stream").val(2);
        }
        if($("#upstream").prop('checked')){
            $("#stream").val(2);
            $("#upstream").attr("checked",false);
        }
    });

    var currten = $("#currten").val();
    if(currten>=1){
        $("#PublicbusStar").attr("disabled",true);
    }

    var schedulingMode = $("#schedulingMode").val();
    if(schedulingMode==1||schedulingMode==2){
        $("#schedulingModeMin").attr("disabled",false);
    }

    $("#schedulingMode").change(function(){
        if($("#schedulingMode").val()==1||$("#schedulingMode").val()==2){
            $("#schedulingModeMin").attr("disabled",false);
        }
        else{
            $("#schedulingModeMin").attr("disabled",true);
        }
    });

});
