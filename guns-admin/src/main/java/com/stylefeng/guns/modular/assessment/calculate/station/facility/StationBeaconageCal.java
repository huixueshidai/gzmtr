package com.stylefeng.guns.modular.assessment.calculate.station.facility;

import com.stylefeng.guns.common.persistence.model.Assessment;
import com.stylefeng.guns.core.util.DoubleUtil;
import com.stylefeng.guns.modular.assessment.calculate.ThirdIndicationCalculator;
import com.stylefeng.guns.modular.assessment.calculate.aware.StationThirdIndicationAware;
import org.springframework.stereotype.Component;

import java.util.Date;

/**
 * 信号系统安全指数
 */
@Component
public class StationBeaconageCal extends ThirdIndicationCalculator implements StationThirdIndicationAware {
    @Override
    protected Integer getIndicationId() {
        return 24;
    }

    @Override
    public Assessment calculate(Integer stationId, Date beginDate, Date endDate) {
        Double stoppage = getDoubleValue(stationId, 40, beginDate,endDate);
        Double total = getDoubleValue(stationId, 41, beginDate,endDate);
        if (stoppage == 0d || total == 0d) {
            return genResult(1d, stationId, beginDate,endDate);
        }
        Double result = DoubleUtil.sub(1, DoubleUtil.div(stoppage, total));
        return genResult(result, stationId, beginDate,endDate);
    }
}
