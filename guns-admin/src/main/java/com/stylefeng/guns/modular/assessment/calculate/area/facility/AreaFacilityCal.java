package com.stylefeng.guns.modular.assessment.calculate.area.facility;

import com.stylefeng.guns.core.util.SpringContextHolder;
import com.stylefeng.guns.modular.assessment.calculate.SecondIndicationCalculator;
import com.stylefeng.guns.modular.assessment.calculate.ThirdIndicationCalculator;
import com.stylefeng.guns.modular.assessment.calculate.aware.RoadnetSecondIndicationAware;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import java.util.Arrays;
import java.util.List;

@Component
@Scope(ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public class AreaFacilityCal extends SecondIndicationCalculator implements RoadnetSecondIndicationAware {
    private List<ThirdIndicationCalculator> thirdIndicationCalculatorList;

    public  AreaFacilityCal() {
        thirdIndicationCalculatorList = Arrays.asList(
                SpringContextHolder.getBean(AreaLowVoltageDistributionCal.class),
                SpringContextHolder.getBean(AreaLpactOfVehicleSystemCal.class),
                SpringContextHolder.getBean(AreaPingBiMenCal.class),
                SpringContextHolder.getBean(AreaQiTaCal.class),
                SpringContextHolder.getBean(AreaSignalCal.class),
                SpringContextHolder.getBean(AreaTongXinCal.class),
                SpringContextHolder.getBean(AreaTractionPowerSupplyCal.class),
                SpringContextHolder.getBean(AreaTuJianCal.class),
                SpringContextHolder.getBean(AreaXianLuCal.class)
        );
    }

    @Override
    public List<ThirdIndicationCalculator> getThirdIndicationCalculatorList() {
        return thirdIndicationCalculatorList;
    }


    @Override
    protected Integer getIndicationId() {
        return 74;
    }
}
