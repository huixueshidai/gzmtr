package com.stylefeng.guns.common.persistence.model;

import java.io.Serializable;

import cn.afterturn.easypoi.excel.annotation.Excel;
import com.baomidou.mybatisplus.enums.IdType;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableName;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

/**
 * <p>
 * 通讯录
 * </p>
 *
 * @author xuziyang
 * @since 2017-12-26
 */
@TableName("gzmtr_contact")
@Setter
@Getter
public class Contact extends Model<Contact> {

    private static final long serialVersionUID = 1L;

	@TableId(value="id", type= IdType.AUTO)
	private Integer id;
	/**
	 * 邮箱
	 */
	@Excel(name = "邮箱" ,width = 50)
	private String email;

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getMobilePhone() {
		return mobilePhone;
	}

	public void setMobilePhone(String mobilePhone) {
		this.mobilePhone = mobilePhone;
	}

	/**

	 * 手机号
	 */
	@TableField("mobile_phone")
	@Excel(name = "手机号" ,width = 50)
	private String mobilePhone;
    /**
     * 姓名
     */
	@Excel(name = "姓名" ,width = 50)
	private String name;
    /**
     * 职务/专业
     */
	@Excel(name = "职务/专业" ,width = 50)
	private String duty;
    /**
     * 行政职务
     */
	@TableField("administration_duty")
	@Excel(name = "行政职务" ,width = 50)
	private String administrationDuty;
    /**
     * 座机
     */
	@Excel(name = "座机" ,width = 50)
	private String phone;

	/**
	 * 分组名称
	 */
	@TableField(exist=false)
	@Excel(name = "分组" ,width = 50)
	private String groupName ;

    /**
     * 分组
     */
	@TableField("group_id")
	private Integer groupId;


	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDuty() {
		return duty;
	}

	public void setDuty(String duty) {
		this.duty = duty;
	}

	public String getAdministrationDuty() {
		return administrationDuty;
	}

	public void setAdministrationDuty(String administrationDuty) {
		this.administrationDuty = administrationDuty;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public Integer getGroupId() {
		return groupId;
	}

	public void setGroupId(Integer groupId) {
		this.groupId = groupId;
	}

	@Override
	protected Serializable pkVal() {
		return this.id;
	}

	@Override
	public String toString() {
		return "Contact{" +
			"id=" + id +
			", name=" + name +
			", duty=" + duty +
			", administrationDuty=" + administrationDuty +
			", phone=" + phone +
			", groupId=" + groupId +
			", email="	+ email +
			", mobile_phone=" + mobilePhone +
			"}";
	}
}
