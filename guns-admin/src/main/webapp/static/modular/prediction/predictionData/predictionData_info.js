/**
 * 初始化风险预测数据管理详情对话框
 */
var PredictionDataInfoDlg = {
    predictionDataInfoData: {}
};

/**
 * 清除数据
 */
PredictionDataInfoDlg.clearData = function () {
    this.predictionDataInfoData = {};
}

/**
 * 设置对话框中的数据
 *
 * @param key 数据的名称
 * @param val 数据的具体值
 */
PredictionDataInfoDlg.set = function (key, val) {
    this.predictionDataInfoData[key] = (typeof val == "undefined") ? $("#" + key).val() : val;
    return this;
}

/**
 * 设置对话框中的数据
 *
 * @param key 数据的名称
 * @param val 数据的具体值
 */
PredictionDataInfoDlg.get = function (key) {
    return $("#" + key).val();
}

/**
 * 关闭此对话框
 */
PredictionDataInfoDlg.close = function () {
    parent.layer.close(window.parent.PredictionData.layerIndex);
}

/**
 * 收集数据
 */
PredictionDataInfoDlg.collectData = function () {
    this
        .set('id')
        .set('year')
        .set('accidentType')
        .set('num')
    ;
}

/**
 * 提交添加
 */
PredictionDataInfoDlg.addSubmit = function () {

    this.clearData();
    this.collectData();

    //提交信息
    var ajax = new $ax(Feng.ctxPath + "/predictionData/add", function (data) {
        Feng.success("添加成功!");
        window.parent.PredictionData.table.refresh();
        PredictionDataInfoDlg.close();
    }, function (data) {
        Feng.error("添加失败!" + data.responseJSON.message + "!");
    });
    ajax.set(this.predictionDataInfoData);
    ajax.start();
}

/**
 * 提交修改
 */
PredictionDataInfoDlg.editSubmit = function () {

    this.clearData();
    this.collectData();

    //提交信息
    var ajax = new $ax(Feng.ctxPath + "/predictionData/update", function (data) {
        Feng.success("修改成功!");
        window.parent.PredictionData.table.refresh();
        PredictionDataInfoDlg.close();
    }, function (data) {
        Feng.error("修改失败!" + data.responseJSON.message + "!");
    });
    ajax.set(this.predictionDataInfoData);
    ajax.start();
}

$(function () {

});
