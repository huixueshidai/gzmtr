package com.stylefeng.guns.modular.yjya.controller;

import com.stylefeng.guns.common.persistence.dao.PublicbusStarupMapper;
import com.stylefeng.guns.core.base.controller.BaseController;
import com.stylefeng.guns.core.shiro.ShiroKit;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.beans.factory.annotation.Autowired;
import com.stylefeng.guns.core.log.LogObjectHolder;
import org.springframework.web.bind.annotation.RequestParam;
import com.stylefeng.guns.common.persistence.model.PublicbusStarup;
import com.stylefeng.guns.modular.yjya.service.IPublicbusStarupService;

/**
 * 公交联动启动控制器
 *
 * @author wangshuaikang
 * @Date 2018-09-09 14:56:52
 */
@Controller
@RequestMapping("/publicbusStarup")
public class PublicbusStarupController extends BaseController {

    private String PREFIX = "/yjya/publicbusStarup/";

    @Autowired
    private IPublicbusStarupService publicbusStarupService;

    @Autowired
    private PublicbusStarupMapper publicbusStarupMapper;

    /**
     * 跳转到公交联动启动首页
     */
    @RequestMapping("")
    public String index() {
        return PREFIX + "publicbusStarup.html";
    }

    /**
     * 跳转到添加公交联动启动
     */
    @RequestMapping("/publicbusStarup_add")
    public String publicbusStarupAdd() {
        return PREFIX + "publicbusStarup_add.html";
    }

    /**
     * 跳转到修改公交联动启动
     */
    @RequestMapping("/publicbusStarup_update/{publicbusStarupId}")
    public String publicbusStarupUpdate(@PathVariable Integer publicbusStarupId, Model model) {
        PublicbusStarup publicbusStarup = publicbusStarupService.selectById(publicbusStarupId);
        model.addAttribute("item",publicbusStarup);
        LogObjectHolder.me().set(publicbusStarup);
        return PREFIX + "publicbusStarup_edit.html";
    }

    /**
     * 获取公交联动启动列表
     */
    @RequestMapping(value = "/list")
    @ResponseBody
    public Object list(String condition) {
        return publicbusStarupService.selectList(null);
    }

    /**
     * 新增公交联动启动
     */
    @RequestMapping(value = "/add")
    @ResponseBody
    public Object add(PublicbusStarup publicbusStarup) {
        //获取当前系统用户id
        Integer userId = ShiroKit.getUser().getId();
        publicbusStarup.setUserId(userId);
        if(publicbusStarup.getLossEfficiencyRetention()==null){
            publicbusStarup.setLossEfficiencyRetention(75);
        }
        if(publicbusStarup.getLossEfficiencyTransportation()==null){
            publicbusStarup.setLossEfficiencyTransportation(85);
        }
        publicbusStarup.setCurrten(1);
        publicbusStarupService.insert(publicbusStarup);
        return super.SUCCESS_TIP;
    }


    /**
     * 删除公交联动启动
     */
    @RequestMapping(value = "/delete")
    @ResponseBody
    public Object delete() {
        //获取当前系统用户id
        Integer userId = ShiroKit.getUser().getId();
        //查询是否有公交联动启动数据
        PublicbusStarup publicbusStarup = publicbusStarupMapper.selectByUserId(userId);
        if(publicbusStarup!=null){
            publicbusStarupService.deleteById(publicbusStarup.getId());
            return SUCCESS_TIP;
        }
        else{
            return SUCCESS_TIP;
        }

    }

    /**
     * 修改公交联动启动
     */
    @RequestMapping(value = "/update")
    @ResponseBody
    public Object update(PublicbusStarup publicbusStarup) {
        //获取当前系统用户id
        Integer userId = ShiroKit.getUser().getId();
        PublicbusStarup publicbusStarup1 = publicbusStarupMapper.selectByUserId(userId);
        publicbusStarup1.setStartStation(publicbusStarup.getStartStation());
        publicbusStarup1.setEndStation(publicbusStarup.getEndStation());
        publicbusStarup1.setStationAmount(publicbusStarup.getStationAmount());
        publicbusStarup1.setCurrten(2);
        publicbusStarupService.updateById(publicbusStarup1);
        return super.SUCCESS_TIP;
    }

    /**
     * 公交联动启动详情
     */
    @RequestMapping(value = "/detail/{publicbusStarupId}")
    @ResponseBody
    public Object detail(@PathVariable("publicbusStarupId") Integer publicbusStarupId) {
        return publicbusStarupService.selectById(publicbusStarupId);
    }
}
