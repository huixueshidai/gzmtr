package com.stylefeng.guns.modular.assessment.calculate.area.facility.util;

import com.stylefeng.guns.common.constant.factory.ConstantFactory;
import com.stylefeng.guns.core.util.DoubleUtil;
import com.stylefeng.guns.modular.assessment.calculate.ThirdIndicationCalculator;

import java.util.Date;
import java.util.List;

public class AreaRoadFacilityCalculatorUtil {

    public static double calculate(Integer areaId, ThirdIndicationCalculator calculator, List<Double> weightList, Date beginDate, Date endDate) {
        Double totalVal = 0d;
        List<Integer> lineIdList = ConstantFactory.me().getIsUsedChildrenIdsByPid(areaId);

        for (int i = 0; i < lineIdList.size(); i++) {
            Double val = calculator.calculate(lineIdList.get(i), beginDate,endDate).getAssessmentValue();
            double weight = 0d;
            if (i < weightList.size()) {
                weight = weightList.get(i);
            }
            totalVal = DoubleUtil.add(totalVal, DoubleUtil.mul(weight, val));
        }
        return totalVal;
    }

}
