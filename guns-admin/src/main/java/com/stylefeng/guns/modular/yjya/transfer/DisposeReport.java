package com.stylefeng.guns.modular.yjya.transfer;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

/**
 * Created by 60132 on 2018/3/23.
 */
@ToString
@Getter
@Setter
public class DisposeReport {
    /**
     * 事故单位
     */
    public String accidentName = " ";
    /**
     * 事故地点
     */
    public String accidentPlace = " ";
    /**
     * 事故开始时间
     */
    public String startTime = " ";
    /**
     * 事故预计结束时间
     */
    public String endTime = " ";
    /**
     * 事故车次
     */
    public String trainNumber = " ";
    /**
     * 事故类型
     */
    public String accidentTypeName = " ";
    /**
     * 响应等级
     */
    public String accidentLevel = " ";
    /**
     * 影响人数
     */
    public String trapPeoNum = " ";

    /**
     * 现场情况
     */
    public String locale = " ";

    /**
     * 事故概况
     */
    public String accidentSurvey = " ";
    /**
     * 事故损失
     */
    public String accidentDamage = " ";
    /**
     * 事故初步原因
     */
    public String accidentCause = " ";
    /**
     * 采取措施
     */
    public String accidentProcess = " ";

}
