/**
 * 事故处理措施管理初始化
 */
var AccidentType = {
    id: "AccidentTypeTable",	//表格id
    seItem: null,		//选中的条目
    table: null,
    layerIndex: -1
};

/**
 * 初始化表格的列
 */
AccidentType.initColumn = function () {
    return [
        {field: 'selectItem', radio: true},
            {title: 'id', field: 'id', visible: true, align: 'center', valign: 'middle' , width:'50px'},
            {title: '名称', field: 'name', visible: true, align: 'center', valign: 'middle'},
            {title: '排序', field: 'num', visible: true, align: 'center', valign: 'middle'},
            {title: '编码', field: 'code', visible: true, align: 'center', valign: 'middle'}
    ];
};

/**
 * 检查是否选中
 */
AccidentType.check = function () {
    var selected = $('#' + this.id).bootstrapTreeTable('getSelections');
    if(selected.length == 0){
        Feng.info("请先选中表格中的某一记录！");
        return false;
    }else{
        AccidentType.seItem = selected[0];
        return true;
    }
};

/**
 * 点击添加事故处理措施
 */
AccidentType.openAddAccidentType = function () {
    var index = layer.open({
        type: 2,
        title: '添加事故处理措施',
        area: ['800px', '420px'], //宽高
        fix: false, //不固定
        maxmin: true,
        content: Feng.ctxPath + '/accidentType/accidentType_add'
    });
    this.layerIndex = index;
};

/**
 * 打开查看事故处理措施详情
 */
AccidentType.openAccidentTypeDetail = function () {
    if (this.check()) {
        var index = layer.open({
            type: 2,
            title: '事故处理措施详情',
            area: ['800px', '420px'], //宽高
            fix: false, //不固定
            maxmin: true,
            content: Feng.ctxPath + '/accidentType/accidentType_update/' + AccidentType.seItem.id
        });
        this.layerIndex = index;
    }
};

/**
 * 删除事故处理措施
 */
AccidentType.delete = function () {
    if (this.check()) {
        var ajax = new $ax(Feng.ctxPath + "/accidentType/delete", function (data) {
            Feng.success("删除成功!");
            AccidentType.table.refresh();
        }, function (data) {
            Feng.error("删除失败!" + data.responseJSON.message + "!");
        });
        console.log(this.seItem);
        ajax.set("accidentTypeId",this.seItem.id);
        ajax.start();
    }
};
/**
 * 重置
 */
AccidentType.resetSearch = function(){
    $("#name").val("");
}
/**
 * 查询事故处理措施列表
 */
AccidentType.search = function () {
    var queryData = {};
    queryData['name'] = $("#name").val();
    AccidentType.table.refresh({query: queryData});
};

$(function () {
    var defaultColunms = AccidentType.initColumn();
    var table = new BSTreeTable(AccidentType.id, "/accidentType/list", defaultColunms);
    table.setExpandColumn(2);
    table.setIdField("id");
    table.setCodeField("id");
    table.setParentCodeField("pid");
    table.setExpandAll(true);
    table.init();
    AccidentType.table = table
});
