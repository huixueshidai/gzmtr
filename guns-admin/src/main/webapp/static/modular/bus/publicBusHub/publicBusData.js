
/**
 * 初始化事故实例详情对话框
 */
var PublicBusHubInfoDlg = {
    PublicBusHubInfoData: {}
    , itemTemplate: $("#itemTemplate").html()
    , itemTemplateLine:$("#itemTemplateLine").html()
    , count: $("#itemSize").val()
    , stationNameListOD:$("#stationNameListOD").val()

};
/**
 * item获取新的ID
 * @returns {string}
 */
PublicBusHubInfoDlg.newId = function () {
    if(this.count == undefined){
        this.count = 0;
    }
    this.count = this.count + 1;
    return "zItem" + this.count;
};
/**
 * 清除数据
 */
PublicBusHubInfoDlg.clearData = function () {
    this.PublicBusHubInfoData = {};
};

/**
 * 设置对话框中的数据
 *
 * @param key 数据的名称
 * @param val 数据的具体值
 */
PublicBusHubInfoDlg.set = function (key, val) {
    this.PublicBusHubInfoData[key] = (typeof val == "undefined") ? $("#" + key).val() : val;
    return this;
};

/**
 * 收集数据
 */
PublicBusHubInfoDlg.collectData = function() {
    this
        .set('passengerOne')
        .set('passengerTwo')
        .set('passengerThr')
        .set('passengerFour')
        .set('passengerFive')
        .set('passengerSix')
        .set('busNum')
    ;
}

/**
 * 设置对话框中的数据
 *
 * @param key 数据的名称
 * @param val 数据的具体值
 */
PublicBusHubInfoDlg.get = function (key) {
    return $("#" + key).val();
};

/**
 * 关闭此对话框
 */
PublicBusHubInfoDlg.close = function () {
    parent.layer.close(window.parent.AccidentInfoDlg.layerIndex);
};
/**
 * 提交添加
 */
PublicBusHubInfoDlg.addSubmit = function () {
    // var stationId = $("#stationId").val();
    var busNum = $("#busNum").val();
    var xz0 = $("#xz0").val();
    var xf0 = $("#xf0").val();

    console.log(window.parent.parent.document)
    window.parent.parent.document.getElementById("menu_305").setAttribute("href", Feng.ctxPath+"/publicBusHub/emulate/"+busNum+"/"+xz0+"/"+xf0);
    window.parent.parent.document.getElementById("menu_305").click();

}
/**
 * 添加条目
 */
PublicBusHubInfoDlg.addItem = function () {
    for(var i = 0;i<2;i++){
        $("#itemsArea").append(this.itemTemplate);
        $("#dictItem").attr("id", this.newId());
    }

};

/**
 * 生成公交应急联动报告
 */
PublicBusHubInfoDlg.disposeReport = function(){

        var stationId = $("#stationId").val();
    var busNum = $("#busNum").val();
        var index = layer.open({
            type: 2,
            title: '公交应急联动处置报告',
            area: ['900px', '600px'], //宽高
            fix: false, //不固定
            maxmin: true,
            content: Feng.ctxPath + '/publicBusHub/dispose_report/'+stationId+"/"+busNum
        });
        this.layerIndex = index;


}

/**
 * 修改客流后重新计算接驳公交数量
 */
PublicBusHubInfoDlg.count = function(){
    this.collectData();
    //提交信息
    var ajax = new $ax(Feng.ctxPath + "/publicBusHub/count", function(data){
        $("#busNum").val(data.busNum);
    },function(data){

    });
    ajax.set(this.PublicBusHubInfoData);
    ajax.start();

}

/**
 * 删除item
 */
PublicBusHubInfoDlg.deleteItem = function (event) {
    var obj = Feng.eventParseObject(event);
    obj.parent().parent().remove();
};

/**
 * 通知上下级
 */
PublicBusHubInfoDlg.render = function(){
    var index = layer.open({
        type: 2,
        title: '汇报上级',
        area: ['1100px', '500px'], //宽高
        fix: false, //不固定
        maxmin: false,
        content: Feng.ctxPath + '/mgr/all'
    });
    this.layerIndex = index;
}

$(function(){

    var stationList = $("#stationList").val();
    var line = (stationList-1)*stationList;
console.log(line);
    for(var i=0;i<line;i++){
        $("#aa").append(PublicBusHubInfoDlg.itemTemplate);
    }
    var inps = document.getElementsByTagName('input');
    var j=0;

    for(var i = 0,inp; inp = inps[i++];){
        if(inp.id == 'star'||inp.id == 'end'||inp.id == 'passengerTwo'){
            inp.value = PublicBusHubInfoDlg.stationNameListOD.replace(/\[|]/g,'').split(',')[j];
            j++;
        }
    }

})


