package com.stylefeng.guns.modular.indication.controller;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.mapper.Wrapper;
import com.stylefeng.guns.common.constant.cache.Cache;
import com.stylefeng.guns.core.base.controller.BaseController;
import com.stylefeng.guns.core.cache.CacheKit;
import com.stylefeng.guns.core.node.ZTreeNode;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.ui.Model;
import org.springframework.beans.factory.annotation.Autowired;
import com.stylefeng.guns.core.log.LogObjectHolder;
import com.stylefeng.guns.common.persistence.model.Indication;
import com.stylefeng.guns.modular.indication.service.IIndicationService;

import java.util.List;

/**
 * 评价指标控制器
 *
 * @author xuziyang
 * @Date 2018-01-08 16:57:03
 */
@Controller
@RequestMapping("/indication")
public class IndicationController extends BaseController {

    private String PREFIX = "/indication/indication/";

    @Autowired
    private IIndicationService indicationService;

    /**
     * 跳转到评价指标首页
     */
    @RequestMapping("")
    public String index() {
        return PREFIX + "indication.html";
    }

    /**
     * 跳转到添加评价指标
     */
    @RequestMapping("/indication_add")
    public String indicationAdd() {
        return PREFIX + "indication_add.html";
    }


    @RequestMapping("/indication_weight/{id}")
    public String weight(@PathVariable("id")Integer id,Model model) {
        Indication indication = indicationService.selectById(id);
        List<Indication> indications = this.indicationService.selectByPid(indication.getPid());
        model.addAttribute("items", indications);
        return PREFIX + "indication_weight.html";
    }

    /**
     * 跳转到修改评价指标
     */
    @RequestMapping("/indication_update/{indicationId}")
    public String indicationUpdate(@PathVariable Integer indicationId, Model model) {
        Indication indication = indicationService.selectById(indicationId);
        model.addAttribute("item",indication);
        String pName;
        if (indication.getPid() == 0) {
            pName = "顶级";
        } else {
            pName = indicationService.selectById(indication.getPid()).getName();
        }
        model.addAttribute("pName", pName);
        LogObjectHolder.me().set(indication);
        return PREFIX + "indication_edit.html";
    }



    /**
     * 获取评价指标列表
     */
    @RequestMapping(value = "/list")
    @ResponseBody
    public Object list(@RequestParam(value = "qName",required = false) String qName) {
        Wrapper<Indication> wrapper = new EntityWrapper();
        wrapper.like("name", qName);
        return indicationService.selectList(wrapper);
    }

    @RequestMapping(value = "/tree")
    @ResponseBody
    public List<ZTreeNode> tree() {
        List<ZTreeNode> tree = indicationService.tree();
        return tree;
    }

    /**
     * 新增评价指标
     */
    @RequestMapping(value = "/add")
    @ResponseBody
    public Object add(Indication indication) {
        indicationService.addNode(indication);
        return super.SUCCESS_TIP;
    }

    /**
     * 删除评价指标
     */
    @RequestMapping(value = "/delete")
    @ResponseBody
    public Object delete(@RequestParam Integer indicationId) {
        indicationService.deleteSelfAndChild(indicationId);
        CacheKit.removeAll(Cache.CONSTANT);
        return SUCCESS_TIP;
    }


    /**
     * 修改评价指标
     */
    @RequestMapping(value = "/update")
    @ResponseBody
    public Object update(Indication indication) {
        indicationService.updateNode(indication);
        CacheKit.removeAll(Cache.CONSTANT);
        return super.SUCCESS_TIP;
    }

    @RequestMapping(value = "/weight/update")
    @ResponseBody
    public Object updateWeight(@RequestBody List<Indication> indications) {
        this.indicationService.updateWeight(indications);
        CacheKit.removeAll(Cache.CONSTANT);
        return super.SUCCESS_TIP;
    }

    /**
     * 评价指标详情
     */
    @RequestMapping(value = "/detail/{indicationId}")
    @ResponseBody
    public Object detail(@PathVariable("indicationId") Integer indicationId) {
        return indicationService.selectById(indicationId);
    }
}
