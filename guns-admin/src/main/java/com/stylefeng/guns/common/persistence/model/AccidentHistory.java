package com.stylefeng.guns.common.persistence.model;

import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;
import com.baomidou.mybatisplus.enums.IdType;
import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

import java.io.Serializable;
import java.util.Date;

/**
 * <p>
 * 事故历史信息
 * </p>
 *
 * @author yinjc
 * @since 2018-05-18
 */
@TableName("gzmtr_accident_history")
@Data
public class AccidentHistory extends Model<AccidentHistory> {

    private static final long serialVersionUID = 1L;

    /**
     * 主键
     */
	@TableId(value="id", type= IdType.AUTO)
	private Integer id;
    /**
     * 名称
     */
	private String name;
    /**
     * 日期
     */
	@DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	private Date date;


	@TableField(value = "roadnet_id")
	private Integer roadnetId;

	/**
	 * 区域字典 code
	 */
	@TableField(value = "area_code")
	private String areaCode;

    /**
     * 级别
     */
    @TableField(value = "rank_code")
	private String rankCode;
    /**
     * 种类
     */
	private String category;

	/**
	 * 原因
	 */
	private String cause;

    /**
     * 描述
     */
	private String detail;



	@Override
	protected Serializable pkVal() {
		return this.id;
	}

}
