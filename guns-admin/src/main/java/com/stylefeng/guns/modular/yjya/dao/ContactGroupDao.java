package com.stylefeng.guns.modular.yjya.dao;

import com.stylefeng.guns.core.node.ZTreeNode;
import io.swagger.models.auth.In;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

public interface ContactGroupDao {

    String getNameById(Integer id);

    List<ZTreeNode> tree();

    List<Map<String, Object>> list(@Param("condition") String condition);
}
