package com.stylefeng.guns.modular.yjya.service;

import com.stylefeng.guns.common.persistence.model.PassengerFile;
import com.baomidou.mybatisplus.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author wsk
 * @since 2018-09-25
 */
public interface IPassengerFileService extends IService<PassengerFile> {
	
}
