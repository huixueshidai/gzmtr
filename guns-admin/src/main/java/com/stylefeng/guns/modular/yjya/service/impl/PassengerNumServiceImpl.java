package com.stylefeng.guns.modular.yjya.service.impl;

import com.stylefeng.guns.common.persistence.model.PassengerNum;
import com.stylefeng.guns.common.persistence.dao.PassengerNumMapper;
import com.stylefeng.guns.modular.yjya.service.IPassengerNumService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author wsk
 * @since 2018-07-10
 */
@Service
public class PassengerNumServiceImpl extends ServiceImpl<PassengerNumMapper, PassengerNum> implements IPassengerNumService {
	
}
