package com.stylefeng.guns.modular.bus.controller;

import cn.afterturn.easypoi.entity.vo.TemplateWordConstants;
import cn.afterturn.easypoi.view.EasypoiTemplateWordView;
import com.stylefeng.guns.common.persistence.dao.*;
import com.stylefeng.guns.common.persistence.model.*;
import com.stylefeng.guns.core.base.controller.BaseController;
import com.stylefeng.guns.core.log.LogObjectHolder;
import com.stylefeng.guns.core.shiro.ShiroKit;
import com.stylefeng.guns.core.support.BeanKit;
import com.stylefeng.guns.core.util.SpringContextHolder;
import com.stylefeng.guns.modular.bus.service.IPublicBusHubService;
import com.stylefeng.guns.modular.roadnet.service.IRoadnetService;
import com.stylefeng.guns.modular.yjya.service.IDriversService;
import com.stylefeng.guns.modular.yjya.service.IPublicbusService;
import com.stylefeng.guns.modular.yjya.transfer.BusDisposeReport;
import com.stylefeng.guns.modular.yjya.transfer.BusPrincipal;
import com.stylefeng.guns.modular.yjya.warpper.AlarmWrapper;
import com.stylefeng.guns.modular.yjya.warpper.BusWarpper;
import com.stylefeng.guns.modular.yjya.warpper.PublicbusStartupWrapper;
import org.apache.commons.collections.MapUtils;
import org.apache.commons.collections.map.HashedMap;
import org.apache.poi.sl.draw.geom.Path;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;


/**
 * 公交站枢纽信息控制器
 *
 * @author yinjc
 * @Date 2018-03-21 11:36:10
 */
@Controller
@RequestMapping("/publicBusHub")
public class PublicBusHubController extends BaseController {

    private String PREFIX = "/bus/publicBusHub/";

    @Autowired
    private IPublicBusHubService publicBusHubService;
    @Autowired
    private PublicBusHubMapper publicBusHubMapper;
    @Autowired
    private IRoadnetService roadnetService;

    @Autowired
    private RoadnetMapper roadnetMapper;

    @Autowired
    private PassengerNumMapper passengerNumMapper;

    @Autowired
    private AlarmMapper alarmMapper;

    @Autowired
    private PublicbusStarupMapper publicbusStarupMapper;

    @Autowired
    private IPublicbusService publicbusService;

    @Autowired
    private IDriversService driversService;

    private Integer shigu ;

    private List<String> stationNameList;

    private String stationSatrtTime = "";

    //与事故事件对应的数据库时间
    private String time = "";

    private Integer maxNum = 80;

    private Double loadFactor = 0.6;

    Integer lossEfficiencyRetention = 25;
    Integer lossEfficiencyTransportation = 35;

    List<String> passengerTime = new ArrayList();

    List<Integer> FlistZ = new ArrayList();
    List<Integer> FlistF = new ArrayList();
    //站站停站点流出客流(单向)1--n-1 正向
    List<Integer> listExportPeoZ = new ArrayList<>();
    //站站停站点汇入客流（单向）2--n 正向
    List<Integer> listEnterPeoZ = new ArrayList<>();
    //站站停站点流出客流(单向)1--n-1 正向
    List<Integer> listExportPeoF = new ArrayList<>();
    //站站停站点汇入客流（单向）2--n 正向
    List<Integer> listEnterPeoF = new ArrayList<>();

    /**
     * 公交应急联动启动页面
     */
    @RequestMapping("/startup")
    public String startUp(Model model){
        //获取当前系统用户id
        Integer userId = ShiroKit.getUser().getId();
        //查询是否有公交联动启动数据
        PublicbusStarup publicbusStarup = publicbusStarupMapper.selectByUserId(userId);
        if(publicbusStarup!=null){
            Map<String, Object> templateMap = BeanKit.beanToMap(publicbusStarup);
            model.addAttribute("item", new PublicbusStartupWrapper(templateMap).warp());
        }
        else{
            //查询当前用户是否有正在应急处置的事故
            Alarm alarm = alarmMapper.selectByUserId(userId);
            if(alarm!=null){
                Map<String, Object> templateMap = BeanKit.beanToMap(alarm);
                model.addAttribute("item", new AlarmWrapper(templateMap).warp());
            }
            else{
                Map<String,Object> map= new HashMap<>();
                model.addAttribute("item", new AlarmWrapper(map).warp());
            }
        }
        return PREFIX + "publicBusStartup.html";
    }


    /**
     * 跳转到公交应急联动执行
     *
     */
    @RequestMapping("/line")
    public String indexLine(Model model) {
        passengerTime = passengerNumMapper.selectTime();
        //获取当前系统用户id
        Integer userId = ShiroKit.getUser().getId();
        //查询是否有公交联动启动数据
        PublicbusStarup publicbusStarup = publicbusStarupMapper.selectByUserId(userId);
        if(publicbusStarup!=null){
            Roadnet roadnetLine = roadnetService.selectById(publicbusStarup.getLineId());
            model.addAttribute("roadnetLine",roadnetLine);
            model.addAttribute("interruptionTime",publicbusStarup.getInterruptionTime());
            model.addAttribute("startTime",publicbusStarup.getStartTime());
            model.addAttribute("retentionTime",publicbusStarup.getRetentionTime());
            model.addAttribute("lossEfficiencyRetention",publicbusStarup.getLossEfficiencyRetention());
            model.addAttribute("lossEfficiencyTransportation",publicbusStarup.getLossEfficiencyTransportation());
            model.addAttribute("item",publicbusStarup);
            //设置事故站点名称
            String stationIds = publicbusStarup.getStationId();
            List<Integer> stationIdList = new ArrayList(Arrays.asList(stationIds.split("/")));
            List<Roadnet> roadnetList = new ArrayList<>();
            String stationName = "";
            for(int i=0;i<stationIdList.size();i++){
                Roadnet station = SpringContextHolder.getBean(RoadnetMapper.class).selectById(stationIdList.get(i));
                roadnetList.add(station);
            }
            for(int i=0;i<roadnetList.size();i++){
                if(i==roadnetList.size()-1){
                    stationName += roadnetList.get(i).getName();
                }
                else{
                    stationName += roadnetList.get(i).getName()+">>";
                }
                model.addAttribute("name",stationName);
            }
//            model.addAttribute("currten",publicbusStarup.getBusStep());
        }
        return PREFIX + "publicBusFeederRoute.html";
    }

    /**
     * 计算站点上行下行客流
     */
    @RequestMapping("/passenger_num/{stationName}/{startTime}/{roadnetLine}")
    @ResponseBody
    public Object passengerNum(@PathVariable("stationName") String stationName,@PathVariable("startTime") String startTime,@PathVariable("roadnetLine") Integer roadnetLine){
        //获取当前系统用户id
        Integer userId = ShiroKit.getUser().getId();
        PublicbusStarup publicbusStarup = publicbusStarupMapper.selectByUserId(userId);
        //上行客流
        Integer upPassenger = 0;
        //下行客流
        Integer downPassenger = 0;

        //获取车站id
        Integer stationId = roadnetMapper.selelctByNmae(stationName,roadnetLine);
        //获取车站上行车站
        List<Roadnet> stationBeforeList = roadnetMapper.selectRoadetListBehind(stationId,roadnetLine);
        //获取车站下行车站
        List<Roadnet> stationAfterList = roadnetMapper.selectRoadetListAfter(stationId,roadnetLine);
        //查询车站点事故事发时间的所有客流
        List<PassengerNum> passengerNumList = passengerNumMapper.selectNumStation(stationId,this.setTime(startTime,publicbusStarup.getDateStatus()));
        String time = this.setTime(startTime,publicbusStarup.getDateStatus());
        for(int i=0;i<passengerNumList.size();i++){
            //上行客流计算
            for(int j=0;j<stationBeforeList.size();j++){
                if(passengerNumList.get(i).getEndStation()==stationBeforeList.get(j).getId()){
                    upPassenger += passengerNumList.get(i).getPassengerNum();
                }
            }
            //下行客流计算
            for(int h = 0;h<stationAfterList.size();h++){
                if(passengerNumList.get(i).getEndStation()==stationAfterList.get(h).getId()){
                    downPassenger += passengerNumList.get(i).getPassengerNum();
                }
            }
        }

        return upPassenger.toString()+","+downPassenger.toString();
    }


    /**
     * 联动方案呈现
     * @param model
     * @return
     */
    @RequestMapping("/linkagscheme")
    public String indexMe (Model model) {
        //获取当前系统用户id
        Integer userId = ShiroKit.getUser().getId();
        PublicbusStarup publicbusStarup = publicbusStarupMapper.selectByUserId(userId);
        Integer lineId = publicbusStarup.getLineId();
        Integer retentionTime = publicbusStarup.getRetentionTime();
        if(publicbusStarup.getLossEfficiencyRetention()!=null){
            lossEfficiencyRetention = publicbusStarup.getLossEfficiencyRetention();
        }
        if(publicbusStarup.getLossEfficiencyTransportation()!=null){
            lossEfficiencyTransportation = publicbusStarup.getLossEfficiencyTransportation();
        }
        Integer stationList = publicbusStarup.getStationAmount();
        String startStation = publicbusStarup.getStartStation();
        String endStation = publicbusStarup.getEndStation();
        stationSatrtTime = publicbusStarup.getStartTime();
        List<PublicBusHub> publicBusHubList = publicBusHubService.selectList(null);
        if(publicBusHubList.size()>0){
            maxNum = publicBusHubList.get(0).getMaxNum();
            loadFactor = publicBusHubList.get(0).getLoadFactor();
        }
        model.addAttribute("stationId",publicbusStarup.getStationId());
        model.addAttribute("stationList",stationList);
        //中断OD
        List<String> stationNameListOD = new ArrayList<>();
        //车站排序的客流（中断时间T为中断时间）
        List<Integer> stationSort = new ArrayList<>();
        //车站排序的客流（中断时间T为0）
        List<Integer> stationSortT = new ArrayList<>();
        //生成路径
        String stationNameRote1="";
        String stationNameRote2="";
        List<Roadnet> roadnetList = roadnetMapper.selectRoadetList(startStation,endStation,lineId);
        //事故车站排序
        List<Integer> stationNumList = new ArrayList<>();
        //根据日期查询数据库数据
        time = this.setTime(publicbusStarup.getStartTime(),publicbusStarup.getDateStatus());
        //事故车站名称
        stationNameList = new ArrayList<>();
        for(int i=0;i<roadnetList.size();i++){
            stationNameList.add(roadnetList.get(i).getName());
            stationNumList.add(roadnetList.get(i).getNum());
        }
        //当t=0时，计算X的值；
        for(int i=0;i<roadnetList.size();i++){
            for(int j=0;j<roadnetList.size();j++){
                if(!roadnetList.get(i).getName().equals(roadnetList.get(j).getName())){
                    stationSort.add(roadnetList.get(i).getNum());
                    stationSort.add(roadnetList.get(j).getNum());
                    stationSortT.add(roadnetList.get(i).getNum());
                    stationSortT.add(roadnetList.get(j).getNum());
                    stationNameListOD.add(roadnetList.get(i).getName());
                    stationNameListOD.add(roadnetList.get(j).getName());
                    //边缘1
                    if(i==0){
                        if(j==0){
                            continue;
                        }
                        //边缘1到边缘2
                        else if(j==roadnetList.size()-1){
                            //疏运客流
                            int x = (this.setPaaaengerBianS(roadnetList.get(i).getId(),roadnetList.get(j).getId(),lineId,publicbusStarup.getInterruptionTime()))*lossEfficiencyTransportation/100;
                            //滞留客流
                            int y = (((this.setPassengerRetenBian(roadnetList.get(i).getId(),roadnetList.get(j).getId(),lineId))*((int)(Math.ceil((float)retentionTime/15))))*lossEfficiencyRetention/100);
                            stationNameListOD.add(String.valueOf(x+y));
                            stationSort.add(x+y);
                            stationSortT.add(y);
                        }
                        //边缘1到故障
                        else{
                           //疏运客流
                            int x = (this.setPaaaengerS(roadnetList.get(i).getId(),roadnetList.get(j).getId(),lineId,publicbusStarup.getInterruptionTime()))*lossEfficiencyTransportation/100;
                            //滞留客流
                            int y= 0;
                            PassengerNum passengerNum = passengerNumMapper.selectNum(roadnetList.get(i).getId(),roadnetList.get(j).getId(),time);
                            if(passengerNum!=null){
                                 y =(int)Math.ceil(Math.ceil((float)passengerNum.getPassengerNum()*retentionTime/15)*lossEfficiencyRetention/100);
                            }
                            stationNameListOD.add(String.valueOf(x+y));
                            stationSort.add(x+y);
                            stationSortT.add(y);
                        }
                    }
                    //边缘2
                    else if(i==roadnetList.size()-1){
                        if(j==roadnetList.size()-1){
                            continue;
                        }
                        //边缘2到边缘1
                        else if(j==0){
                            //疏运客流
                            int x = (this.setPaaaengerBianA(roadnetList.get(j).getId(),roadnetList.get(i).getId(),lineId,publicbusStarup.getInterruptionTime()))*lossEfficiencyTransportation/100;
                            //滞留客流
                            int y = (int)Math.ceil(Math.ceil((float)(this.setPassengerRetenBian(roadnetList.get(i).getId(),roadnetList.get(j).getId(),lineId))*retentionTime/15)*lossEfficiencyRetention/100);
                            stationNameListOD.add(String.valueOf(x+y));
                            stationSort.add(x+y);
                            stationSortT.add(y);
                        }
                        //边缘2到故障
                        else{
                            //疏运客流
                            int x = (this.setPaaaengerA(roadnetList.get(i).getId(),roadnetList.get(j).getId(),lineId,publicbusStarup.getInterruptionTime()))*lossEfficiencyTransportation/100;
                            //滞留客流
                            int y = 0;
                            PassengerNum passengerNum = passengerNumMapper.selectNum(roadnetList.get(i).getId(),roadnetList.get(j).getId(),time);
                            if(passengerNum!=null){

                                y = (int)Math.ceil(Math.ceil((float)passengerNum.getPassengerNum()*retentionTime/15)*lossEfficiencyRetention/100);
                            }
                            stationNameListOD.add(String.valueOf(x+y));
                            stationSort.add(x+y);
                            stationSortT.add(y);
                        }
                    }
                    //故障
                    else{
                        //故障到边缘
                        if(j==0||j==(roadnetList.size()-1)){
                            //疏运客流 0
                            int x = 0;
                            int y = 0;
                            //滞留客流
                            y = (int)Math.ceil(Math.ceil((float)(this.setPassengerRetenFaultBian(roadnetList.get(j).getId(),roadnetList.get(i).getId(),lineId))*retentionTime/15)*lossEfficiencyRetention/100);
                            stationNameListOD.add(String.valueOf(x+y));
                            stationSort.add(x+y);
                            stationSortT.add(y);
                        }
                        //故障到故障
                        else {
                            if(i!=j){
                                //疏运客流 0
                                int x = 0;
                                //滞留客流
                                int y = 0;
                                PassengerNum passengerNum = passengerNumMapper.selectNum(roadnetList.get(i).getId(),roadnetList.get(j).getId(),time);
                                if(passengerNum!=null){
                                    y = (int)Math.ceil(Math.ceil((float)passengerNum.getPassengerNum()*retentionTime/15)*lossEfficiencyRetention/100);
                                }
                                stationNameListOD.add(String.valueOf(x+y));
                                stationSort.add(x+y);
                                stationSortT.add(y);
                            }
                        }
                    }
                }
            }
        }

        List<Integer> X = this.setPassengerX(stationSortT,roadnetList.size(),stationNumList);

        List<Integer> busNumList = this.setBusNumber(stationSort,roadnetList.size(),stationNumList,retentionTime,roadnetList,lineId,publicbusStarup.getInterruptionTime());
        for(int i=0;i<roadnetList.size();i++){
            if(i==roadnetList.size()-1){
                stationNameRote1  += roadnetList.get(i).getName()+"       需要接驳车"+busNumList.get(2) + "辆";
                stationNameRote2  += roadnetList.get(roadnetList.size()-1-i).getName()+"       需要接驳车"+busNumList.get(3) +"辆";
            }
            else{
                stationNameRote1  += roadnetList.get(i).getName()+"    >>   ";
                stationNameRote2  += roadnetList.get(roadnetList.size()-1-i).getName()+"    >>   ";
            }

        }
        String stationNameRote3=startStation+"    >>   "+endStation+"       需要接驳车"+ busNumList.get(0)+ "辆";
        String stationNameRote4=endStation+"    >>   "+startStation+"       需要接驳车"+ busNumList.get(1)+ "辆";

        //所需接驳车辆总量
        Integer busNumAll = 0;
        for(int i =0;i<busNumList.size();i++){
            busNumAll += busNumList.get(i);
        }

        model.addAttribute("xz0",X.get(0));
        model.addAttribute("xf0",X.get(1));
        model.addAttribute("stationNameListOD",stationNameListOD);
        model.addAttribute("stationNameRote1",stationNameRote1);
        model.addAttribute("stationNameRote2",stationNameRote2);
        model.addAttribute("stationNameRote3",stationNameRote3);
        model.addAttribute("stationNameRote4",stationNameRote4);
        model.addAttribute("busNumAll",busNumAll);
        //接驳路径
        String connectionPath = stationNameRote1+"\n"+stationNameRote2+"\n"+stationNameRote3+"\n"+stationNameRote4;
        publicbusStarup.setConnectionPath(connectionPath);
        publicbusStarupMapper.updateById(publicbusStarup);
        return PREFIX + "publicBusData.html";
    }

    /**
     * 跳转到公交应急联动客流仿真页面
     *
     */
    @RequestMapping("/emulate/{busNum}/{xz0}/{xf0}")
    public String emulate(Model model,@PathVariable("busNum") Integer busNum,@PathVariable("xz0") Integer xz0,@PathVariable("xf0") Integer xf0) {
        passengerTime = passengerNumMapper.selectTime();
        //获取当前系统用户id
        Integer userId = ShiroKit.getUser().getId();
        PublicbusStarup publicbusStarup = publicbusStarupMapper.selectByUserId(userId);
//        Alarm alarm = alarmMapper.selectOneAlarm();
        //滞留时间
        Integer retentionTime = publicbusStarup.getRetentionTime();
            if(publicbusStarup.getLossEfficiencyRetention()!=null){
            //滞留客流损失系数
            lossEfficiencyRetention = publicbusStarup.getLossEfficiencyRetention();
            time = this.setTime(publicbusStarup.getStartTime(),publicbusStarup.getDateStatus());
        }
        Integer lineId = publicbusStarup.getLineId();
        String startStation = publicbusStarup.getStartStation();
        String endStation = publicbusStarup.getEndStation();
        //获取接驳站点list
        List<Roadnet> roadnetList = roadnetMapper.selectRoadetList(startStation,endStation,lineId);
        //车站初始状态的滞留客流(下行)西朗至广州东方向
        List<String> passAfterList = new ArrayList();
        //车站初始状态的滞留客流（上行）广州东至西朗方向
        List<String> passBeforeList = new ArrayList();
        //车站初始状态的疏运客流(下行)西朗至广州东方向边缘站(增长率)
        Integer yTranPassA = 0;
        Integer yTranPassAZ = 0;//直达
        Integer yTranPassAZZ = 0;//站站停
        Integer EaA = 0;
        Integer Eza = 0;
        //车站初始状态的疏运客流（上行）广州东至西朗方向边缘站（增长率）
        Integer yTranPassB = 0;
        Integer yTranPassBZ = 0;//直达
        Integer yTranPassBZZ = 0;//站站停
        Integer EaB = 0;
        Integer EzB = 0;

        //各车站下行滞留客流计算
        for(int i =0;i<roadnetList.size();i++){
            if(roadnetList.get(i).getName().equals(startStation)){
                //下行疏运客流（初始状态增长率）
                 List<Integer> yTranPassAList = this.setTransportationPass(roadnetList.get(i).getId(),roadnetList.get(roadnetList.size()-1).getId(),lineId,publicbusStarup.getStartTime(),true);
                //总疏运客流增长率
                yTranPassA = yTranPassAList.get(1);
                yTranPassAZZ = yTranPassAList.get(0);
                yTranPassAZ = yTranPassA - yTranPassAZZ;
            }
            //滞留客流(下行)
            int x = 0;
            //获取车站下行车站
            List<Roadnet> stationAfterList = roadnetMapper.selectRoadetListAfter(roadnetList.get(i).getId(),lineId);
            List<PassengerNum> passengerNumList = passengerNumMapper.selectNumStation(roadnetList.get(i).getId(),this.setTime(publicbusStarup.getStartTime(),publicbusStarup.getDateStatus()));
            for(int j =0;j<passengerNumList.size();j++){
                for(int g =0;g<stationAfterList.size();g++){
                    if(passengerNumList.get(j).getEndStation()==stationAfterList.get(g).getId()){
                        x += passengerNumList.get(j).getPassengerNum();
                    }
                }
            }
            x =(int)Math.ceil(Math.ceil((float)x*retentionTime/15)*lossEfficiencyRetention/100);
            passAfterList.add(roadnetList.get(i).getName()+"：" +String.valueOf(x)+"");
            if(roadnetList.get(i).getName().equals(startStation)){
                //边缘站点的直达滞留客流
                EaA = (int)Math.ceil(Math.ceil((float)this.setPassengerRetenBian(roadnetList.get(i).getId(),roadnetList.get(roadnetList.size()-1).getId(),lineId)*retentionTime/15)*lossEfficiencyRetention/100);
                //边缘站点的站站停客流
                Eza = x - EaA;
            }

        }
        //各车站上行滞留客流计算
        for(int i =roadnetList.size()-1;i>=0;i--){
            if(roadnetList.get(i).getName().equals(endStation)){
                //上行疏运客流（初始状态增长率）
               List<Integer> yTranPassBList = this.setTransportationPass(roadnetList.get(i).getId(),roadnetList.get(0).getId(),lineId,publicbusStarup.getStartTime(),false);
                yTranPassB = yTranPassBList.get(1);
                yTranPassBZZ = yTranPassBList.get(0);
                yTranPassBZ = yTranPassB - yTranPassBZZ;
            }
            //滞留客流(上行)
            int y = 0;
            //获取车站上行车站
            List<Roadnet> stationBeforeList = roadnetMapper.selectRoadetListBehind(roadnetList.get(i).getId(),lineId);
            List<PassengerNum> passengerNumList = passengerNumMapper.selectNumStation(roadnetList.get(i).getId(),this.setTime(publicbusStarup.getStartTime(),publicbusStarup.getDateStatus()));
            for(int j =0;j<passengerNumList.size();j++){
                for(int g =0;g<stationBeforeList.size();g++){
                    if(passengerNumList.get(j).getEndStation()==stationBeforeList.get(g).getId()){
                        y += passengerNumList.get(j).getPassengerNum();
                    }
                }
            }

            y = (int)Math.ceil(Math.ceil((float)y*retentionTime/15)*lossEfficiencyRetention/100);
            passBeforeList.add(roadnetList.get(i).getName()+"："+String.valueOf(y)+"");
            if(roadnetList.get(i).getName().equals(endStation)){
                //边缘站点的直达客流
                EaB = (int)Math.ceil(Math.ceil((float)this.setPassengerRetenBian(roadnetList.get(i).getId(),roadnetList.get(0).getId(),lineId)*retentionTime/15)*lossEfficiencyRetention/100);
                //边缘站点的站站停客流
                EzB = y - EaB;
            }
        }
        FlistZ = this.dissipatePass(listExportPeoZ,listEnterPeoZ,roadnetList.size(),xz0);
        FlistF = this.dissipatePass(listExportPeoF,listEnterPeoF,roadnetList.size(),xf0);
        model.addAttribute("FlistZ",FlistZ);
        model.addAttribute("FlistF",FlistF);
        model.addAttribute("passAfterList",passAfterList);
        model.addAttribute("passBeforeList",passBeforeList);
        model.addAttribute("yTranPassA",yTranPassA);
        model.addAttribute("yTranPassAZ",yTranPassAZ);
        model.addAttribute("yTranPassAZZ",yTranPassAZZ);
        model.addAttribute("yTranPassB",yTranPassB);
        model.addAttribute("yTranPassBZ",yTranPassBZ);
        model.addAttribute("yTranPassBZZ",yTranPassBZZ);
        model.addAttribute("EaA",EaA);
        model.addAttribute("Eza",Eza);
        model.addAttribute("EaB",EaB);
        model.addAttribute("EzB",EzB);
        model.addAttribute("xz0",xz0);
        model.addAttribute("xf0",xf0);

        List<PublicBusHub> publicBusHubList = publicBusHubService.selectList(null);
        //设置公交车满载率人数
        if(publicBusHubList.size()>0){
            maxNum = publicBusHubList.get(0).getMaxNum();
            loadFactor = publicBusHubList.get(0).getLoadFactor();
        }
        model.addAttribute("passengers",maxNum*loadFactor);
        model.addAttribute("startStation",startStation);
        model.addAttribute("endStation",endStation);
        model.addAttribute("stationNum",stationNameList.size());
        model.addAttribute("stationSatrtTime",stationSatrtTime);
//        model.addAttribute("stationId",stationId);
        model.addAttribute("busNum",busNum);
        model.addAttribute("item",publicbusStarup);

        return PREFIX + "publicBusEmulate_info.html";
    }


    /**
     * 客流仿真图页面（正向）
     *
     */
    @RequestMapping("plan")
    public String plan(Model model){
        //获取当前系统用户id
        Integer userId = ShiroKit.getUser().getId();
        PublicbusStarup publicbusStarup = publicbusStarupMapper.selectByUserId(userId);
        List<PublicBusHub> publicBusHubList = publicBusHubService.selectList(null);
        PublicBusHub publicBusHub1 = publicBusHubList.get(0);
        PublicBusHub publicBusHub2 = publicBusHubList.get(1);
        PublicBusHub publicBusHub3 = publicBusHubList.get(2);
        PublicBusHub publicBusHub4 = publicBusHubList.get(3);
        PublicBusHub publicBusHub5 = publicBusHubList.get(4);
        model.addAttribute("publicBusHub1",publicBusHub1);
        model.addAttribute("publicBusHub2",publicBusHub2);
        model.addAttribute("publicBusHub3",publicBusHub3);
        model.addAttribute("publicBusHub4",publicBusHub4);
        model.addAttribute("publicBusHub5",publicBusHub5);
        model.addAttribute("stationNameList",stationNameList);
        model.addAttribute("stationSatrtTime",publicbusStarup.getStartTime());

        return  PREFIX + "publicBusEmulate.html";
    }


    /**
     * 客流仿真图页面（反向）
     *
     */
    @RequestMapping("planF")
    public String planF(Model model){
        //获取当前系统用户id
        Integer userId = ShiroKit.getUser().getId();
        PublicbusStarup publicbusStarup = publicbusStarupMapper.selectByUserId(userId);
        List<PublicBusHub> publicBusHubList = publicBusHubService.selectList(null);
        PublicBusHub publicBusHub1 = publicBusHubList.get(0);
        PublicBusHub publicBusHub2 = publicBusHubList.get(1);
        PublicBusHub publicBusHub3 = publicBusHubList.get(2);
        PublicBusHub publicBusHub4 = publicBusHubList.get(3);
        PublicBusHub publicBusHub5 = publicBusHubList.get(4);
        model.addAttribute("publicBusHub1",publicBusHub1);
        model.addAttribute("publicBusHub2",publicBusHub2);
        model.addAttribute("publicBusHub3",publicBusHub3);
        model.addAttribute("publicBusHub4",publicBusHub4);
        model.addAttribute("publicBusHub5",publicBusHub5);
        model.addAttribute("stationNameList",stationNameList);
        model.addAttribute("stationSatrtTime",publicbusStarup.getStartTime());

        return  PREFIX + "publicBusEmulateF.html";
    }



    /**
     * 跳转到公交站枢纽信息首页
     */
    @RequestMapping("")
    public String index() {
        return PREFIX + "publicBusHub.html";
    }

    /**
     * 跳转到添加公交站枢纽信息
     */
    @RequestMapping("/publicBusHub_add")
    public String publicBusHubAdd() {
        return PREFIX + "publicBusHub_add.html";
    }

    /**
     * 跳转到修改公交站枢纽信息
     */
    @RequestMapping("/publicBusHub_update/{publicBusHubId}")
    public String publicBusHubUpdate(@PathVariable Integer publicBusHubId, Model model) {
        PublicBusHub publicBusHub = publicBusHubService.selectById(publicBusHubId);
        model.addAttribute("item",publicBusHub);
        LogObjectHolder.me().set(publicBusHub);
        return PREFIX + "publicBusHub_edit.html";
    }
    /**
     * 跳转到获取公交枢纽站地理位置页面
     */
    @RequestMapping( "/publicBusHub_location/{publicBusHubId}")
    public String publicBusHubLocation(@PathVariable Integer publicBusHubId,Model model) {
        PublicBusHub publicBusHub = publicBusHubService.selectById(publicBusHubId);
        model.addAttribute("publicBusHub",publicBusHub);
        return PREFIX + "publicBusHub_location_sel.html";
    }
    /**
     * 获取公交站枢纽信息列表
     */
    @RequestMapping(value = "/list")
    @ResponseBody
    public Object list(String condition) {
        List<Map<String,Object>> list =this.publicBusHubMapper.selectByName(condition);
        return new BusWarpper(list).warp();
    }

    /**
     * 新增公交站枢纽信息
     */
    @RequestMapping(value = "/add")
    @ResponseBody
    public Object add(PublicBusHub publicBusHub) {
        publicBusHubService.insert(publicBusHub);
        return super.SUCCESS_TIP;
    }

    /**
     * 删除公交站枢纽信息
     */
    @RequestMapping(value = "/delete")
    @ResponseBody
    public Object delete(@RequestParam Integer publicBusHubId) {
        publicBusHubService.deleteById(publicBusHubId);
        return SUCCESS_TIP;
    }

    /**
     * 修改公交站枢纽信息
     */
    @RequestMapping(value = "/update")
    @ResponseBody
    public Object update(PublicBusHub publicBusHub) {
        publicBusHubService.updateById(publicBusHub);
        return super.SUCCESS_TIP;
    }

    /**
     * 公交站枢纽信息详情
     */
    @RequestMapping(value = "/detail/{publicBusHubId}")
    @ResponseBody
    public Object detail(@PathVariable("publicBusHubId") Integer publicBusHubId) {
        return publicBusHubService.selectById(publicBusHubId);
    }

    /**
     * 跳转到公交联动处置报告
     */
    @RequestMapping("/dispose_report/{busNum}/{stationSatrtTime}/{endTime}")
    public String publicDisposeReport(@PathVariable String busNum,@PathVariable String stationSatrtTime, @PathVariable String endTime,Model model) {

        Integer userId = ShiroKit.getUser().getId();
        PublicbusStarup publicbusStarup = publicbusStarupMapper.selectByUserId(userId);
        String stationIds = publicbusStarup.getStationId();
        Roadnet line = roadnetMapper.selectById(publicbusStarup.getLineId());
        List<Integer> stationIdList = new ArrayList(Arrays.asList(stationIds.split("/")));
        List<Roadnet> roadnetList = new ArrayList<>();
        String stationName = "";
        for(int i=0;i<stationIdList.size();i++){
            Roadnet station = SpringContextHolder.getBean(RoadnetMapper.class).selectById(stationIdList.get(i));
            roadnetList.add(station);
        }
        for(int i=0;i<roadnetList.size();i++){
            if(i==roadnetList.size()-1){
                stationName += roadnetList.get(i).getName();
            }
            else{
                stationName += roadnetList.get(i).getName()+">>";
            }
        }
        List<Roadnet> roadnetList1 = roadnetMapper.selectRoadetList(publicbusStarup.getStartStation(),publicbusStarup.getEndStation(),publicbusStarup.getLineId());

        model.addAttribute("stationName",line.getName()+":"+stationName);
        model.addAttribute("connectionPath",publicbusStarup.getConnectionPath());
        model.addAttribute("busNum",busNum);
        model.addAttribute("stationSatrtTime",stationSatrtTime);
        model.addAttribute("endTime",endTime);
        return PREFIX + "publicDispose_report.html";
    }


    /**
     * 停止公交站联动演示
     */
    @RequestMapping(value = "/stop")
    public Object stop() {
        this.shigu = 0;
        return SUCCESS_TIP;
    }

    /**
     * 生成下载公交联动应急报告
     * @param modelMap
     * @param busDisposeReport
     * @return
     */
    @RequestMapping(value = "/exportDocByT",headers = {"Accept=application/msword"})
    public String exportDocByT(ModelMap modelMap, BusDisposeReport busDisposeReport,Publicbus publicbus) {
        busDisposeReport.setBusLine(busDisposeReport.getBusLine().replace("& gt;",">"));
        Map map = BeanKit.beanToMap(busDisposeReport);
        List<BusPrincipal> busPrincipalList = this.getPricipals();
        map.put("principals", busPrincipalList);
        Integer userId = ShiroKit.getUser().getId();
        PublicbusStarup publicbusStarup = publicbusStarupMapper.selectByUserId(userId);
//        Alarm alarm = alarmMapper.selectOneAlarm();
        publicbus.setLevel(2);
        publicbus.setStationName(publicbusStarup.getStationId());
        publicbus.setTime(publicbusStarup.getStartTime());
        publicbus.setUser(userId);
        publicbus.setAccidentName(map.get("accidentName").toString());
        publicbus.setEndTime(map.get("endTime").toString());
        publicbus.setBusNum(map.get("busNum").toString());
        publicbus.setBusLine(map.get("busLine").toString());

        List<Drivers> driversList = new ArrayList<>();
        String driverIds = "";
        for(int i = 0;i<busPrincipalList.size();i++){
            Drivers driver = new Drivers();
            driver.setName(busPrincipalList.get(i).getName());
            driver.setPlateNum(busPrincipalList.get(i).getPlateNum());
            driver.setTelphone(busPrincipalList.get(i).getTelphone());
            driversList.add(driver);
            driversService.insert(driver);
            driverIds+=(driver.getId()+";");
        }
        publicbus.setDriverId(driverIds);
        publicbusService.insert(publicbus);
        modelMap.put(TemplateWordConstants.FILE_NAME,"公交联动处置报告");
        modelMap.put(TemplateWordConstants.MAP_DATA,map);
        modelMap.put(TemplateWordConstants.URL,"export/template/busReport.docx");
        return TemplateWordConstants.EASYPOI_TEMPLATE_WORD_VIEW;
    }

    private List<BusPrincipal> getPricipals() {
        List<BusPrincipal> list = new ArrayList<>();
        String[] names = getParas("name");
        String[] plateNums = getParas("plateNum");
        String[] telphones = getParas("telphone");

        if (names == null || plateNums == null || telphones == null) {
            BusPrincipal busPrincipal = new BusPrincipal("", "", "");
            list.add(busPrincipal);
            return list;
        }

        int length = Math.min(Math.min(names.length, plateNums.length), telphones.length);

        for(int i = 0 ; i<length;i++) {
            BusPrincipal busPrincipal = new BusPrincipal(names[i], plateNums[i], telphones[i]);
            list.add(busPrincipal);
        }
        return list;

    }


    /**
     * 设置上行(正向)疏运客流 边缘1到--故障区各站点的疏运客流
     * @param star 接驳车站起始点
     * @param faultStationId 故障区站点
     * @param pid 接驳车站线路id
     */
    public Integer setPaaaengerS(Integer  star,Integer faultStationId,Integer pid,Integer interruptionTime)  {

        //疏运客流(边缘站点)
        int people=0;
        //获取边缘1站点前的几个站点
        List<Roadnet> roadnetListBehind = roadnetMapper.selectRoadetListBehind(star,pid);
        //通过时间段、起始站、到站点获取客流数量
        for(int i=0;i<roadnetListBehind.size();i++){
            int a = 0;
            PassengerNum passengerNum = passengerNumMapper.selectNum(roadnetListBehind.get(i).getId(),faultStationId,time);
            if(passengerNum!=null){
                a = passengerNum.getPassengerNum();
            }
            people+= a;
        }
        return (int)(Math.ceil(people*((double)interruptionTime/15)));

    }
    /**
     * 设置下行(反向)疏运客流 边缘2---故障
     * @param end 边缘站点2
     * @param faultStationId 故障区站点id
     * param pid 故障车站线路id
     */
    public Integer setPaaaengerA(Integer end,Integer faultStationId,Integer pid,Integer interruptionTime)  {

        //疏运客流(边缘站点)
        int people=0;
        //获取边缘2站点后的几个站点
        List<Roadnet> roadnetListAfter = roadnetMapper.selectRoadetListAfter(end,pid);
        //通过时间段、起始站、到站点获取客流数量
        for(int i=0;i<roadnetListAfter.size();i++){
            int a = 0;
            PassengerNum passengerNum = passengerNumMapper.selectNum(roadnetListAfter.get(i).getId(),faultStationId,time);
            if(passengerNum!=null){
                a = passengerNum.getPassengerNum();
            }
            people+= a;
        }
        return (int)(Math.ceil(people*((double)interruptionTime/15)));

    }

    /**
     * @param star 边缘站点1
     * @param end 边缘站点2
     * @param pid 车站的pid
     * @param interruptionTime 中断时间
     * 设置边缘点1到边缘点2疏运客流正向（上行）
     */
    public Integer setPaaaengerBianS(Integer  star,Integer end,Integer pid,Integer interruptionTime)  {

        //疏运客流(边缘站点)
        int people=0;
        //获取边缘1站点前的几个站点
        List<Roadnet> roadnetListBehind = roadnetMapper.selectRoadetListBehind(star,pid);
        //获取边缘2站点后几个站点
        List<Roadnet> roadnetListAfter = roadnetMapper.selectRoadetListAfter(end,pid);
        //通过时间段、起始站、到站点获取客流数量
        for(int i=0;i<roadnetListBehind.size();i++){
            for(int j=0;j<roadnetListAfter.size();j++){
                int a = 0;
                PassengerNum passengerNum = passengerNumMapper.selectNum(roadnetListBehind.get(i).getId(),roadnetListAfter.get(j).getId(),time);
                if(passengerNum!=null){
                    a = passengerNum.getPassengerNum();
                }
                people+= a;
            }
        }
        for(int i=0;i<roadnetListBehind.size();i++){
            int a1 = 0;
            PassengerNum passengerNum = passengerNumMapper.selectNum(roadnetListBehind.get(i).getId(),end,time);
            if(passengerNum!=null){
                a1 = passengerNum.getPassengerNum();
            }
            people+= a1;
        }
        return (int)(Math.ceil(people*((double)interruptionTime/15)));


    }

    /**
     * @param star 边缘站点1
     * @param end 边缘站点2
     * @param pid 车站的pid
     * 设置边缘点2到边缘点1疏运客流反向（下行）
     */
    public Integer setPaaaengerBianA(Integer star,Integer end,Integer pid,Integer interruptionTime)  {

        //疏运客流(边缘站点)
        int people=0;
        //获取边缘1站点前的几个站点
        List<Roadnet> roadnetListBehind = roadnetMapper.selectRoadetListBehind(star,pid);
        //获取边缘2站点后几个站点
        List<Roadnet> roadnetListAfter = roadnetMapper.selectRoadetListAfter(end,pid);
        //通过时间段、起始站、到站点获取客流数量
        for(int i=0;i<roadnetListAfter.size();i++){
            for(int j=0;j<roadnetListBehind.size();j++){
                int a = 0;
                PassengerNum passengerNum = passengerNumMapper.selectNum(roadnetListAfter.get(i).getId(),roadnetListBehind.get(j).getId(),time);
                if(passengerNum!=null){
                    a = passengerNum.getPassengerNum();
                }
                people+= a;
            }
        }
        for(int i=0;i<roadnetListAfter.size();i++){
            int a1 = 0;
            PassengerNum passengerNum = passengerNumMapper.selectNum(roadnetListAfter.get(i).getId(),star,time);
            if(passengerNum!=null){
                a1 = passengerNum.getPassengerNum();
            }
            people+= a1;
        }
        return (int)(Math.ceil(people*((double)interruptionTime/15)));

    }

    /**
     * star 边缘站点1
     * end 边缘站点2
     * startTime 发生时间
     * pid 车站的pid
     * 设置边缘点到边缘点滞留客流
     */
    public Integer setPassengerRetenBian(Integer star,Integer end,Integer pid){

        List<Roadnet> roadnetList = null;
        int people = 0;
        //获取边缘2站点后几个站点（上行正向）
        if(star<end){
            roadnetList = roadnetMapper.selectRoadetListAfter(end,pid);
            //滞留客流(边缘1到边缘2)
            PassengerNum passengerNum1 = passengerNumMapper.selectNum(star,end,time);
            if(passengerNum1!=null){
                people = passengerNum1.getPassengerNum();
            }
            List<PassengerNum> passengerNumList = passengerNumMapper.selectNumStation(star,time);
            for(int i=0;i<roadnetList.size();i++){

                for(int j =0;j<passengerNumList.size();j++){
                    if(roadnetList.get(i).getId()==passengerNumList.get(j).getEndStation()){
                        people += passengerNumList.get(j).getPassengerNum();
                    }
                }
            }
        }
        //下行滞留客流（反向）
        if(star>end){
            roadnetList = roadnetMapper.selectRoadetListBehind(end,pid);
            PassengerNum passengerNum1 = passengerNumMapper.selectNum(star,end,time);
            if(passengerNum1!=null){
                people = passengerNum1.getPassengerNum();
            }
            List<PassengerNum> passengerNumList = passengerNumMapper.selectNumStation(star,time);
            for(int i=0;i<roadnetList.size();i++){
                for(int j = 0;j<passengerNumList.size();j++){
                    if(roadnetList.get(i).getId()==passengerNumList.get(j).getEndStation()){
                        people += passengerNumList.get(j).getPassengerNum();
                    }
                }

            }
        }
        return people;
    }


    /**
     * end 边缘站点
     * startTime 发生时间
     * pid 车站的pid
     * faultStationId 故障点
     * 设置故障到边缘点滞留客流
     */
    public Integer setPassengerRetenFaultBian(Integer end,Integer faultStationId,Integer pid){

        int people = 0;
        //正向下行
        if(end>faultStationId){
            List<Roadnet> roadnetListAfter = roadnetMapper.selectRoadetListAfter(end,pid);
            PassengerNum passengerNum1 = passengerNumMapper.selectNum(faultStationId,end,time);
            if(passengerNum1!=null){
                people = passengerNum1.getPassengerNum();
            }
            List<PassengerNum> passengerNumList = passengerNumMapper.selectNumStation(faultStationId,time);
            for(int i =0;i<roadnetListAfter.size();i++){
                for (int j =0;j<passengerNumList.size();j++){
                    if(roadnetListAfter.get(i).getId()==passengerNumList.get(j).getEndStation()){
                        people += passengerNumList.get(j).getPassengerNum();
                    }
                }
            }
        }
        //反向上行
        if(end<faultStationId){
            List<Roadnet> roadnetListBehind = roadnetMapper.selectRoadetListBehind(end,pid);
            PassengerNum passengerNum1 = passengerNumMapper.selectNum(faultStationId,end,time);
            if(passengerNum1!=null){
                people = passengerNum1.getPassengerNum();
            }
            List<PassengerNum> passengerNumList = passengerNumMapper.selectNumStation(faultStationId,time);
            for(int i =0;i<roadnetListBehind.size();i++){
                for(int j =0;j<passengerNumList.size();j++){
                    if(roadnetListBehind.get(i).getId()==passengerNumList.get(j).getEndStation()){
                        people += passengerNumList.get(j).getPassengerNum();
                    }
                }
            }
        }
        return people;
    }


    /**
     * 时间范围的设置
     * @param time 时间
     * @param dateStatus 客流类型（工作日，休息日，节假日）
     * @return
     * @throws ParseException
     */
    public String setTime(String time,Integer dateStatus)  {

        Date date = null;
        try {
            date = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse(time);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        Calendar now = Calendar.getInstance();
        now.setTime(date);
        List<String> times = passengerNumMapper.selectByDatestatus(dateStatus);
        for  ( int  i  =   0 ; i  <  times.size()  -   1 ; i ++ )  {
            for  ( int  j  =  times.size()  -   1 ; j  >  i; j -- )  {
                if  (times.get(j).equals(times.get(i)))  {
                    times.remove(j);
                }
            }
        }
        for(int i =0;i<times.size();i++){
            if(times.size()==1){
                try {
                    Date date1 = new SimpleDateFormat("yyyy-MM-dd").parse(times.get(i));
                    Calendar now1 = Calendar.getInstance();
                    now1.setTime(date1);
                    now.set(now1.get(Calendar.YEAR),now1.get(Calendar.MONTH),now1.get(Calendar.DATE),now.get(Calendar.HOUR_OF_DAY),now.get(Calendar.MINUTE),now.get(Calendar.SECOND));
                } catch (ParseException e) {
                e.printStackTrace();
            }
            }
            else{
                try {
                    Date date1 = new SimpleDateFormat("yyyy-MM-dd").parse(times.get(i));
                    Calendar now1 = Calendar.getInstance();
                    now1.setTime(date1);
                    now.set(now1.get(Calendar.YEAR),now1.get(Calendar.MONTH),now1.get(Calendar.DATE),now.get(Calendar.HOUR_OF_DAY),now.get(Calendar.MINUTE),now.get(Calendar.SECOND));
                    break;
                } catch (Exception exception) {
                    exception.printStackTrace();
                }
            }
        }
        int minute = now.get(Calendar.MINUTE);
        if(minute>=0&&minute<15){
            now.set(now.get(Calendar.YEAR),now.get(Calendar.MONTH),now.get(Calendar.DATE),now.get(Calendar.HOUR_OF_DAY),00,00);
        }
        if(minute>=15&&minute<30){
            now.set(now.get(Calendar.YEAR),now.get(Calendar.MONTH),now.get(Calendar.DATE),now.get(Calendar.HOUR_OF_DAY),15,00);
        }
        if(minute>=30&&minute<45){
            now.set(now.get(Calendar.YEAR),now.get(Calendar.MONTH),now.get(Calendar.DATE),now.get(Calendar.HOUR_OF_DAY),30,00);
        }
        if(minute>=45&&minute<=59){
            now.set(now.get(Calendar.YEAR),now.get(Calendar.MONTH),now.get(Calendar.DATE),now.get(Calendar.HOUR_OF_DAY),45,00);
        }
        return new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(now.getTime());
    }

    /**
     *
     * @param roadnetSortList 车站对应的是排序
     * @param stationNum
     * @param  stationList 事故车站的排序list
     * @param retentionTime 滞留时间
     * @param roadnetList 事故车站实体list
     */
    public List<Integer> setBusNumber(List<Integer> roadnetSortList,Integer stationNum,List<Integer> stationList,Integer retentionTime,List<Roadnet> roadnetList,Integer pid,Integer interruptionTime){

        List<Integer> bunsNumList = new ArrayList<>();

        //直达车 如果边缘站点到边缘站点的滞留客流+疏运客流/15*t的和大于公交载客量  正向
        int zhiliu = this.setPassengerRetenBian(roadnetList.get(0).getId(),roadnetList.get(roadnetList.size()-1).getId(),pid);
        int shuyun = this.setPaaaengerBianS(roadnetList.get(0).getId(),roadnetList.get(roadnetList.size()-1).getId(),pid,interruptionTime);
        if(zhiliu +((shuyun/15)*retentionTime)>(maxNum*loadFactor)||zhiliu +((shuyun/15)*retentionTime)==(maxNum*loadFactor)){
            int busNumzhida = (zhiliu +(shuyun/15*retentionTime))/(int)(maxNum*loadFactor);
            bunsNumList.add(busNumzhida);
        }
        else{
            bunsNumList.add(0);
        }

        //直达车 如果边缘站点到边缘站点的滞留客流+疏运客流/15*t的和大于公交载客量  反向
        int zhiliu1 = this.setPassengerRetenBian(roadnetList.get(roadnetList.size()-1).getId(),roadnetList.get(0).getId(),pid);
        int shuyun1 = this.setPaaaengerBianA(roadnetList.get(0).getId(),roadnetList.get(roadnetList.size()-1).getId(),pid,interruptionTime);
        if(zhiliu1 +((shuyun1/15)*retentionTime)>(maxNum*loadFactor)||zhiliu1 +((shuyun1/15)*retentionTime)==(maxNum*loadFactor)){
            int busNumzhida = (zhiliu1 +(shuyun1/15*retentionTime))/(int)(maxNum*loadFactor);
            bunsNumList.add(busNumzhida);
        }
        else{
            bunsNumList.add(0);
        }

        //站站停站点流出客流(单向)1--n-1 正向
        List<Integer> listExportPeo = new ArrayList<>();
        //站站停站点汇入客流（单向）2--n 正向
        List<Integer> listEnterPeo = new ArrayList<>();
        int x = 0;
        for(int i=0;i<roadnetSortList.size()-stationNum+2;i+=3){

            if(i>3||i==3){
                if(roadnetSortList.get(i)==roadnetSortList.get(i-3)){
                    if(roadnetSortList.get(i+1)>roadnetSortList.get(i)){
                        x += roadnetSortList.get(i+2);
                    }
                }
                else{
                    listExportPeo.add(x);
                    x = 0;
                    if(roadnetSortList.get(i+1)>roadnetSortList.get(i)){
                        x += roadnetSortList.get(i+2);
                    }
                }
            }
        }
        listExportPeo.set(0,listExportPeo.get(0)+roadnetSortList.get(2)-roadnetSortList.get((stationNum-2)*3+2));
        int y =0;
        for(int i =1;i<stationList.size();i++){
            if(i>1){
                listEnterPeo.add(y);
                y=0;
            }
            for(int j=1;j<roadnetSortList.size()-stationNum+3;j+=3){
                if(stationList.get(i)==roadnetSortList.get(j)&&roadnetSortList.get(j)>roadnetSortList.get(j-1)){
                    y += roadnetSortList.get(j+1);
                }
            }
        }
        listEnterPeo.set(listEnterPeo.size()-1,listEnterPeo.get(listEnterPeo.size()-1)-roadnetSortList.get(roadnetSortList.size()-(((stationList.size()-1)*3)-1)));

        //正向站站停  正向
        int xx = this.passengerZZ(listExportPeo,listEnterPeo,stationNum);
        int busNumS = (int) Math.ceil((float)xx/(int)(maxNum*loadFactor));
        bunsNumList.add(busNumS);

        //站站停站点流出客流(单向)1--n-1 反向
        List<Integer> listExportPeo1 = new ArrayList<>();
        //站站停站点汇入客流（单向）2--n 反向
        List<Integer> listEnterPeo1 = new ArrayList<>();
        int x1 = 0;
        for(int i=roadnetSortList.size()-3;i>=2;i-=3){

            if(i<roadnetSortList.size()-3){
                if(roadnetSortList.get(i)==roadnetSortList.get(i+3)){
                    if(roadnetSortList.get(i+1)<roadnetSortList.get(i)){
                        x1 += roadnetSortList.get(i+2);
                    }
                }
                else{
                    listExportPeo1.add(x1);
                    x1 = 0;
                    if(roadnetSortList.get(i+1)<roadnetSortList.get(i)){
                        x1 += roadnetSortList.get(i+2);
                    }
                }
            }
            else{
                    if(roadnetSortList.get(i+1)<roadnetSortList.get(i)){
                        x1 += roadnetSortList.get(i+2);
                    }
            }
        }
        listExportPeo1.set(0,listExportPeo1.get(0)-roadnetSortList.get((roadnetSortList.size()-1)-((stationNum-2)*3)));
        int y1 =0;
        for(int i =stationList.size()-2;i>=0;i--){
            y1=0;

            for(int j=1;j<roadnetSortList.size()-stationNum+3;j+=3){
                if(stationList.get(i)==roadnetSortList.get(j)&&roadnetSortList.get(j)<roadnetSortList.get(j-1)){
                    y1 += roadnetSortList.get(j+1);
                }
            }
            listEnterPeo1.add(y1);
        }
        listEnterPeo1.set(listEnterPeo1.size()-1,listEnterPeo1.get(listEnterPeo1.size()-1)-roadnetSortList.get((roadnetSortList.size()-1)-((stationList.size()-2)*3)));

        //站站停  反向
        int xx1 = this.passengerZZ(listExportPeo1,listEnterPeo1,stationNum);
        int busNumS1 = (int) Math.ceil((float)xx1/(int)(maxNum*loadFactor));
        bunsNumList.add(busNumS1);
        return bunsNumList;

    }

    /**
     *计算站站停所需要的车辆
     * @param listExportPeo
     * @param listEnterPeo
     * @param stationNum  故障车站数量
     * @return
     */
    public Integer passengerZZ(List<Integer> listExportPeo,List<Integer> listEnterPeo,Integer stationNum){
        int xx = 0;
        //站站停所需要的车
        if(stationNum>2){
            if(listExportPeo.get(1)>listEnterPeo.get(0)&&listExportPeo.get(1)==listEnterPeo.get(0)){ //(p2-)>=(p2+)
                xx = listExportPeo.get(0)+listExportPeo.get(1)-listEnterPeo.get(0);
                if(stationNum>3){
                    if(listExportPeo.get(2)>listEnterPeo.get(1)&&listExportPeo.get(2)==listEnterPeo.get(1)){ //(p3-)>=(p3+)
                        xx = listExportPeo.get(0)+listExportPeo.get(1)-listEnterPeo.get(0)+listExportPeo.get(2)-listEnterPeo.get(1);
                        if(stationNum>4){
                            if(listExportPeo.get(3)>listEnterPeo.get(2)&&listExportPeo.get(3)==listEnterPeo.get(2)){  //(p4-)>=(p4+)
                                xx = listExportPeo.get(0)+listExportPeo.get(1)-listEnterPeo.get(0)+listExportPeo.get(2)-listEnterPeo.get(1)+listExportPeo.get(3)-listEnterPeo.get(2);
                                if(stationNum>5){
                                    if(listExportPeo.get(4)>listEnterPeo.get(3)&&listExportPeo.get(4)==listEnterPeo.get(3)){ //(p5-)>=(p5+)
                                        xx = listExportPeo.get(0)+listExportPeo.get(1)-listEnterPeo.get(0)+listExportPeo.get(2)-listEnterPeo.get(1)+listExportPeo.get(3)-listEnterPeo.get(2)+listExportPeo.get(4)-listEnterPeo.get(3);
                                    }
                                    else{  //(p5-)<(p5+)
                                        xx = listExportPeo.get(0)+listExportPeo.get(1)-listEnterPeo.get(0)+listExportPeo.get(2)-listEnterPeo.get(1)+listExportPeo.get(3)-listEnterPeo.get(2);
                                    }
                                }
                            }
                            else{ //(p4-)<(p4+)
                                xx = listExportPeo.get(0)+listExportPeo.get(1)-listEnterPeo.get(0)+listExportPeo.get(2)-listEnterPeo.get(1)+listExportPeo.get(3);
                                if(stationNum>5){
                                    if(listExportPeo.get(4)>listEnterPeo.get(3)&&listExportPeo.get(4)==listEnterPeo.get(3)){
                                        if((listEnterPeo.get(2)-listExportPeo.get(3))>(listExportPeo.get(4)-listEnterPeo.get(3))&&(listEnterPeo.get(2)-listExportPeo.get(3))==(listExportPeo.get(4)-listEnterPeo.get(3))){  //If p_5^-≥p_5^+ 且 p_4^+-p_4^-≥p_5^--p_5^+
                                            xx = listExportPeo.get(0)+listExportPeo.get(1)-listEnterPeo.get(0)+listExportPeo.get(2)-listEnterPeo.get(1);
                                        }
                                        else{  //If p_5^-≥p_5^+  且 p_4^+-p_4^-＜p_5^--p_5^+
                                            xx = listExportPeo.get(0)+listExportPeo.get(1)-listEnterPeo.get(0)+listExportPeo.get(2)-listEnterPeo.get(1)+listExportPeo.get(3)-listEnterPeo.get(2)+listExportPeo.get(4)-listEnterPeo.get(3);
                                        }
                                    }
                                    else{   //p_5^-＜p_5^+
                                        xx = listExportPeo.get(0)+listExportPeo.get(1)-listEnterPeo.get(0)+listExportPeo.get(2)-listEnterPeo.get(1);
                                    }
                                }
                            }
                        }
                    }
                    else{ //p_3^-＜p_3^+
                        xx = listExportPeo.get(0)+listExportPeo.get(1)-listEnterPeo.get(0);
                        if(stationNum>4){
                            if(listExportPeo.get(3)>listEnterPeo.get(2)&&listExportPeo.get(3)==listEnterPeo.get(2)){  //p_4^-≥p_4^
                                if((listEnterPeo.get(1)-listExportPeo.get(2))>(listExportPeo.get(3)-listEnterPeo.get(2))&&(listEnterPeo.get(1)-listExportPeo.get(2))==(listExportPeo.get(3)-listEnterPeo.get(2))){ //且 p_3^+-p_3^-≥p_4^--p_4^+
                                    xx = listExportPeo.get(0)+listExportPeo.get(1)-listEnterPeo.get(0);
                                    if(stationNum>5){
                                        if(listExportPeo.get(4)>listEnterPeo.get(3)&&listExportPeo.get(4)==listEnterPeo.get(3)){  //p_5^-≥p_5^+
                                            xx = listExportPeo.get(0)+listExportPeo.get(1)-listEnterPeo.get(0)+listExportPeo.get(2)-listEnterPeo.get(1)+listExportPeo.get(3)-listEnterPeo.get(2)+listExportPeo.get(4)-listEnterPeo.get(3);
                                        }
                                        else{   //p_5^-＜p_5^+
                                            xx = listExportPeo.get(0)+listExportPeo.get(1)-listEnterPeo.get(0)+listExportPeo.get(2)-listEnterPeo.get(1)+listExportPeo.get(3)-listEnterPeo.get(2);
                                        }
                                    }
                                }
                            }
                            else{  //Else p_4^-＜p_4^+
                                xx = listExportPeo.get(0)+listExportPeo.get(1)-listEnterPeo.get(0)+listExportPeo.get(2)-listEnterPeo.get(1)+listExportPeo.get(3)-listEnterPeo.get(2);
                                if(stationNum>5){
                                    if(listExportPeo.get(4)>listEnterPeo.get(3)&&listExportPeo.get(4)==listEnterPeo.get(3)){ // p_5^-≥p_5^+
                                        if((listEnterPeo.get(1)-listExportPeo.get(2)+listEnterPeo.get(2)-listExportPeo.get(3))>(listExportPeo.get(4)-listEnterPeo.get(3))&&(listEnterPeo.get(1)-listExportPeo.get(2)+listEnterPeo.get(2)-listExportPeo.get(3))==(listExportPeo.get(4)-listEnterPeo.get(3))){  //且（p_3^+-p_3^-）+（p_4^+-p_4^-）≥p_5^--p_5^+
                                            xx = listExportPeo.get(0)+listExportPeo.get(1)-listEnterPeo.get(0);
                                        }
                                        else{  //且（p_3^+-p_3^-）+（p_4^+-p_4^-）＜p_5^--p_5^+
                                            xx = listExportPeo.get(0)+listExportPeo.get(1)-listEnterPeo.get(0)+listExportPeo.get(2)-listEnterPeo.get(1)+listExportPeo.get(3)-listEnterPeo.get(2)+listExportPeo.get(4)-listEnterPeo.get(3);
                                        }
                                    }
                                    else{// p_5^-＜p_5^+
                                        xx = listExportPeo.get(0)+listExportPeo.get(1)-listEnterPeo.get(0);
                                    }
                                }
                            }
                        }
                    }
                }
            }
            else{ // p_2^-＜p_2^+
                xx = listExportPeo.get(0);
                if(stationNum>3){
                    if(listExportPeo.get(2)>listEnterPeo.get(1)&&listExportPeo.get(2)==listEnterPeo.get(1)){ //p_3^-≥p_3^+
                        if((listEnterPeo.get(0)-listExportPeo.get(1)>listExportPeo.get(2)-listEnterPeo.get(1))&&(listEnterPeo.get(0)-listExportPeo.get(1)==listExportPeo.get(2)-listEnterPeo.get(1))){ //且 p_2^+-p_2^-≥p_3^--p_3^+
                            xx = listExportPeo.get(0);
                            if(stationNum>4){
                                if(listExportPeo.get(3)>listEnterPeo.get(2)&&listExportPeo.get(3)==listEnterPeo.get(2)){ // p_4^-≥p_4^+
                                    if((listEnterPeo.get(0)-listExportPeo.get(1)>listExportPeo.get(2)-listEnterPeo.get(1)+listExportPeo.get(3)-listEnterPeo.get(3))&&(listEnterPeo.get(0)-listExportPeo.get(1)==listExportPeo.get(2)-listEnterPeo.get(1)+listExportPeo.get(3)-listEnterPeo.get(3))){ //且（p_2^+-p_2^-）≥（p_3^--p_3^+）+（p_4^--p_4^+）
                                        xx = listExportPeo.get(0);
                                        if(stationNum>5){
                                            if(listEnterPeo.get(4)>listExportPeo.get(3)&&listEnterPeo.get(4)==listExportPeo.get(3)){ //p_5^-≥p_5^+
                                                if((listExportPeo.get(0)-listEnterPeo.get(1)>listExportPeo.get(2)-listEnterPeo.get(1)+listExportPeo.get(3)-listEnterPeo.get(2)+listExportPeo.get(4)-listEnterPeo.get(3))&&(listExportPeo.get(0)-listEnterPeo.get(1)==listExportPeo.get(2)-listEnterPeo.get(1)+listExportPeo.get(3)-listEnterPeo.get(2)+listExportPeo.get(4)-listEnterPeo.get(3))){ //且（p_2^+-p_2^-）≥（p_3^--p_3^+ ）+（p_4^--p_4^+）+（p_5^--p_5^+）
                                                    xx = listExportPeo.get(0);
                                                }
                                                else{ //且（p_2^+-p_2^-）＜（p_3^--p_3^+ ）+（p_4^--p_4^+）+（p_5^--p_5^+）
                                                    xx = listExportPeo.get(0)+listExportPeo.get(1)-listEnterPeo.get(0)+listExportPeo.get(2)-listEnterPeo.get(1)+listExportPeo.get(3)-listEnterPeo.get(2)+listExportPeo.get(4)-listEnterPeo.get(3);
                                                }
                                            }
                                            else{ //p_5^-＜p_5^+
                                                xx = listExportPeo.get(0);
                                            }
                                        }
                                    }
                                    else{ //且（p_2^+-p_2^-）＜（p_3^--p_3^+）+（p_4^--p_4^+）
                                        xx = listExportPeo.get(0)+listExportPeo.get(1)-listEnterPeo.get(0)+listExportPeo.get(2)-listEnterPeo.get(1)+listExportPeo.get(3)-listEnterPeo.get(2);
                                        if(stationNum>5){
                                            if(listExportPeo.get(4)>listExportPeo.get(3)&&listExportPeo.get(4)==listExportPeo.get(3)){  //p_5^-≥p_5^+
                                                xx = listExportPeo.get(0)+listExportPeo.get(1)-listEnterPeo.get(0)+listExportPeo.get(2)-listEnterPeo.get(1)+listExportPeo.get(3)-listEnterPeo.get(2)+listExportPeo.get(4)-listEnterPeo.get(3);
                                            }
                                            else{  // p_5^-＜p_5^+
                                                xx = listExportPeo.get(0)+listExportPeo.get(1)-listEnterPeo.get(0)+listExportPeo.get(2)-listEnterPeo.get(1)+listExportPeo.get(3)-listEnterPeo.get(2);
                                            }
                                        }
                                    }
                                }
                                else{  //p_4^-＜p_4^+
                                    xx = listExportPeo.get(0);
                                    if(stationNum>5){
                                        if(listExportPeo.get(4)>listExportPeo.get(3)&&listExportPeo.get(4)==listExportPeo.get(3)){ // p_5^-≥p_5^+
                                            if(listEnterPeo.get(0)-listExportPeo.get(1)+listEnterPeo.get(2)-listExportPeo.get(3)>listExportPeo.get(2)-listEnterPeo.get(1)+listExportPeo.get(4)-listEnterPeo.get(3)){  //且 （p_2^+-p_2^- ）+（p_4^+-p_4^-）≥（p_3^--p_3^+ ）+（p_5^--p_5^+）
                                                xx = listExportPeo.get(0);
                                            }
                                            else{ //且  （p_2^+-p_2^- ）+（p_4^+-p_4^- ）＜（p_3^--p_3^+ ）+（p_5^--p_5^+）
                                                xx = listExportPeo.get(0)+listExportPeo.get(1)-listEnterPeo.get(0)+listExportPeo.get(2)-listEnterPeo.get(1)+listExportPeo.get(3)-listEnterPeo.get(2)+listExportPeo.get(4)-listEnterPeo.get(3);
                                            }
                                        }
                                        else{
                                            xx = listExportPeo.get(0);
                                        }
                                    }
                                }
                            }
                        }
                        else{ //且 p_2^+-p_2^-＜p_3^--p_3^+
                            xx = listExportPeo.get(0)+listExportPeo.get(1)-listEnterPeo.get(0)+listExportPeo.get(2)-listEnterPeo.get(1);
                            if(stationNum>4){
                                if(listExportPeo.get(3)>listEnterPeo.get(2)&&listExportPeo.get(3)==listEnterPeo.get(2)){ //p_4^-≥p_4^+
                                    xx = listExportPeo.get(0)+listExportPeo.get(1)-listEnterPeo.get(0)+listExportPeo.get(2)-listEnterPeo.get(1)+listExportPeo.get(3)-listEnterPeo.get(2);
                                    if(stationNum>5){
                                        if(listExportPeo.get(4)>listEnterPeo.get(3)&&listExportPeo.get(4)==listEnterPeo.get(3)){ //p_5^-≥p_5^+
                                            xx = listExportPeo.get(0)+listExportPeo.get(1)-listEnterPeo.get(0)+listExportPeo.get(2)-listEnterPeo.get(1)+listExportPeo.get(3)-listEnterPeo.get(2)+listExportPeo.get(4)-listEnterPeo.get(3);
                                        }
                                        else{ // p_5^-＜p_5^+
                                            xx = listExportPeo.get(0)+listExportPeo.get(1)-listEnterPeo.get(0)+listExportPeo.get(2)-listEnterPeo.get(1)+listExportPeo.get(3)-listEnterPeo.get(2);
                                        }
                                    }
                                }
                                else{ // p_4^-＜p_4^+
                                    xx = listExportPeo.get(0)+listExportPeo.get(1)-listEnterPeo.get(0)+listExportPeo.get(2)-listEnterPeo.get(1);
                                    if(stationNum>5){
                                        if(listExportPeo.get(4)>listEnterPeo.get(3)&&listExportPeo.get(4)==listEnterPeo.get(3)){ //p_5^-≥p_5^+
                                            if((listEnterPeo.get(2)-listExportPeo.get(3)>listExportPeo.get(4)-listEnterPeo.get(3))&&(listEnterPeo.get(2)-listExportPeo.get(3)==listExportPeo.get(4)-listEnterPeo.get(3))){ //且 （p_4^+-p_4^- ）≥（p_5^--p_5^+ ）
                                                xx = listExportPeo.get(0)+listExportPeo.get(1)-listEnterPeo.get(0)+listExportPeo.get(2)-listEnterPeo.get(1);
                                            }
                                            else{ //且 （p_4^+-p_4^- ）＜（p_5^--p_5^+ ）
                                                xx = listExportPeo.get(0)+listExportPeo.get(1)-listEnterPeo.get(0)+listExportPeo.get(2)-listEnterPeo.get(1)+listExportPeo.get(3)-listEnterPeo.get(2)+listExportPeo.get(4)-listEnterPeo.get(3);
                                            }
                                        }
                                        else{ //p_5^-＜p_5^+
                                            xx = listExportPeo.get(0)+listExportPeo.get(1)-listEnterPeo.get(0)+listExportPeo.get(2)-listEnterPeo.get(1);
                                        }
                                    }
                                }
                            }
                        }
                    }
                    else{  //p_3^-＜p_3^+
                        xx = listExportPeo.get(0);
                        if(stationNum>4){
                            if(listExportPeo.get(3)>listEnterPeo.get(2)&&listExportPeo.get(3)==listEnterPeo.get(2)){ //p_4^-≥p_4^+
                                if((listEnterPeo.get(0)-listExportPeo.get(1)+listEnterPeo.get(1)-listEnterPeo.get(2)>listExportPeo.get(3)-listEnterPeo.get(2))&&(listEnterPeo.get(0)-listExportPeo.get(1)+listEnterPeo.get(1)-listEnterPeo.get(2)==listExportPeo.get(3)-listEnterPeo.get(2))){ //且（p_2^+-p_2^- ）+（p_3^+-p_3^-）≥p_4^--p_4^+
                                    xx = listExportPeo.get(0);
                                    if(stationNum>5){
                                        if(listExportPeo.get(4)>listEnterPeo.get(3)&&listExportPeo.get(4)==listEnterPeo.get(3)){ //p_5^-≥p_5^+
                                            if((listEnterPeo.get(0)-listExportPeo.get(1)+listEnterPeo.get(1)-listExportPeo.get(2)>listExportPeo.get(3)-listEnterPeo.get(2))&&(listEnterPeo.get(0)-listExportPeo.get(1)+listEnterPeo.get(1)-listExportPeo.get(2)==listExportPeo.get(3)-listEnterPeo.get(2))){ //且（p_2^+-p_2^-）+（p_3^+-p_3^-）≥（p_4^--p_4^+ ）+（p_5^--p_5^+）
                                                xx = listExportPeo.get(0);
                                            }
                                            else{  //且（p_2^+-p_2^-）+（p_3^+-p_3^-）＜ （p_4^--p_4^+ ）+（p_5^--p_5^+）
                                                xx = listExportPeo.get(0)+listExportPeo.get(1)-listEnterPeo.get(0)+listExportPeo.get(2)-listEnterPeo.get(1)+listExportPeo.get(3)-listEnterPeo.get(2)+listExportPeo.get(4)-listEnterPeo.get(3);
                                            }
                                        }
                                        else{  //p_5^-＜p_5^+
                                            xx = listExportPeo.get(0);
                                        }
                                    }
                                }
                                else{ //且（p_2^+-p_2^- ）+（p_3^+-p_3^- ）＜p_4^--p_4^+
                                    xx = listExportPeo.get(0)+listExportPeo.get(1)-listEnterPeo.get(0)+listExportPeo.get(2)-listEnterPeo.get(1)+listExportPeo.get(3)-listEnterPeo.get(2);
                                    if(stationNum>5){
                                        if(listExportPeo.get(4)>listEnterPeo.get(3)&&listExportPeo.get(4)==listEnterPeo.get(3)){  //p_5^-≥p_5^+
                                            xx = listExportPeo.get(0)+listExportPeo.get(1)-listEnterPeo.get(0)+listExportPeo.get(2)-listEnterPeo.get(1)+listExportPeo.get(3)-listEnterPeo.get(2)+listExportPeo.get(4)-listEnterPeo.get(3);
                                        }
                                        else{ // p_5^-＜p_5^+
                                            xx = listExportPeo.get(0)+listExportPeo.get(1)-listEnterPeo.get(0)+listExportPeo.get(2)-listEnterPeo.get(1)+listExportPeo.get(3)-listEnterPeo.get(2);
                                        }
                                    }
                                }
                            }
                            else{ //p_4^-＜p_4^+
                                xx = listExportPeo.get(0);
                                if(stationNum>5){
                                    if(listExportPeo.get(4)>listEnterPeo.get(3)&&listExportPeo.get(4)==listEnterPeo.get(3)){   //p_5^-≥p_5^+
                                        if((listEnterPeo.get(0)-listExportPeo.get(1)+listEnterPeo.get(1)-listExportPeo.get(2)+listEnterPeo.get(2)-listExportPeo.get(3)>listExportPeo.get(4)-listEnterPeo.get(3))&&(listEnterPeo.get(0)-listExportPeo.get(1)+listEnterPeo.get(1)-listExportPeo.get(2)+listEnterPeo.get(2)-listExportPeo.get(3)==listExportPeo.get(4)-listEnterPeo.get(3))){ //且（p_2^+-p_2^-）+（p_3^+-p_3^-）+（p_4^+-p_4^-）≥（p_5^--p_5^+）
                                            xx = listExportPeo.get(0);
                                        }
                                        else{ //且（𝑝2+−𝑝2−）+（𝑝3+−𝑝3−）+（𝑝4+−𝑝4−）＜（𝑝5−−𝑝5+）
                                            xx = listExportPeo.get(0)+listExportPeo.get(1)-listEnterPeo.get(0)+listExportPeo.get(2)-listEnterPeo.get(1)+listExportPeo.get(3)-listEnterPeo.get(2)+listExportPeo.get(4)-listEnterPeo.get(3);
                                        }
                                    }
                                    else{  //p_5^-＜p_5^+
                                        xx = listExportPeo.get(0);
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
        return xx;
    }

    /**
     * 计算当中断时间为0时，返回的X的值
     * @param roadnetSortList
     * @param stationNum
     * @param stationList
     * @return
     */
    public List<Integer> setPassengerX(List<Integer> roadnetSortList,Integer stationNum,List<Integer> stationList){

        List<Integer> X = new ArrayList<>();
        //站站停站点流出客流(单向)1--n-1 正向（P-）
        List<Integer> listExportPeo = new ArrayList<>();
        //站站停站点汇入客流（单向）2--n 正向(P+)
        List<Integer> listEnterPeo = new ArrayList<>();
        int x = 0;
        for(int i=0;i<roadnetSortList.size()-stationNum+2;i+=3){
            if(i>3||i==3){
                if(roadnetSortList.get(i)==roadnetSortList.get(i-3)){
                    if(roadnetSortList.get(i+1)>roadnetSortList.get(i)){
                        x += roadnetSortList.get(i+2);
                    }
                }
                else{
                    listExportPeo.add(x);
                    x = 0;
                    if(roadnetSortList.get(i+1)>roadnetSortList.get(i)){
                        x += roadnetSortList.get(i+2);
                    }
                }
            }
        }
        listExportPeo.set(0,listExportPeo.get(0)+roadnetSortList.get(2)-roadnetSortList.get((stationNum-2)*3+2));
        int y =0;
        for(int i =1;i<stationList.size();i++){
            if(i>1){
                listEnterPeo.add(y);
                y=0;
            }
            for(int j=1;j<roadnetSortList.size()-stationNum+3;j+=3){
                if(stationList.get(i)==roadnetSortList.get(j)&&roadnetSortList.get(j)>roadnetSortList.get(j-1)){
                    y += roadnetSortList.get(j+1);
                }
            }
        }
        listEnterPeo.set(listEnterPeo.size()-1,listEnterPeo.get(listEnterPeo.size()-1)-roadnetSortList.get(roadnetSortList.size()-(((stationList.size()-1)*3)-1)));
        this.listExportPeoZ = listExportPeo;
        this.listEnterPeoZ = listEnterPeo;
        //当中断时间为0时，X的值为
        Integer xz = this.passengerZZ(listExportPeo,listEnterPeo,stationNum);
        X.add(xz);

        //站站停站点流出客流(单向)1--n-1 反向
        List<Integer> listExportPeo1 = new ArrayList<>();
        //站站停站点汇入客流（单向）2--n 反向
        List<Integer> listEnterPeo1 = new ArrayList<>();
        int x1 = 0;
        for(int i=roadnetSortList.size()-3;i>=2;i-=3){
            if(i<roadnetSortList.size()-3){
                if(roadnetSortList.get(i)==roadnetSortList.get(i+3)){
                    if(roadnetSortList.get(i+1)<roadnetSortList.get(i)){
                        x1 += roadnetSortList.get(i+2);
                    }
                }
                else{
                    listExportPeo1.add(x1);
                    x1 = 0;
                    if(roadnetSortList.get(i+1)<roadnetSortList.get(i)){
                        x1 += roadnetSortList.get(i+2);
                    }
                }
            }
            else{
                if(roadnetSortList.get(i+1)<roadnetSortList.get(i)){
                    x1 += roadnetSortList.get(i+2);
                }
            }
        }
        listExportPeo1.set(0,listExportPeo1.get(0)-roadnetSortList.get((roadnetSortList.size()-1)-((stationNum-2)*3)));
        int y1 =0;
        for(int i =stationList.size()-2;i>=0;i--){
            y1=0;

            for(int j=1;j<roadnetSortList.size()-stationNum+3;j+=3){
                if(stationList.get(i)==roadnetSortList.get(j)&&roadnetSortList.get(j)<roadnetSortList.get(j-1)){
                    y1 += roadnetSortList.get(j+1);
                }
            }
            listEnterPeo1.add(y1);
        }
        listEnterPeo1.set(listEnterPeo1.size()-1,listEnterPeo1.get(listEnterPeo1.size()-1)-roadnetSortList.get((roadnetSortList.size()-1)-((stationList.size()-2)*3)));
        this.listExportPeoF = listExportPeo1;
        this.listEnterPeoF = listEnterPeo1;
        Integer xf = this.passengerZZ(listExportPeo1,listEnterPeo1,stationNum);
        X.add(xf);
        return X;
    }

    /**
     * 计算初始状态的疏运客流增长率
     * decide (true为下行false为上行)
     * @return
     */
    public List<Integer> setTransportationPass(Integer stationId,Integer endStationId,Integer lineId,String time,Boolean decide){

        //获取当前系统用户id
        Integer userId = ShiroKit.getUser().getId();
        PublicbusStarup publicbusStarup = publicbusStarupMapper.selectByUserId(userId);
        int y =0;//直达加站站停
        int x = 0;//站站停
        List<Roadnet> stationBeforeList = roadnetMapper.selectRoadetListBehind(stationId,lineId);
        List<Roadnet> stationAfterList = roadnetMapper.selectRoadetListAfter(stationId,lineId);
        List<Integer> stationBeforeListId = new ArrayList<>();
        List<Integer> stationAfterListId = new ArrayList<>();
        for(int i = 0;i<stationBeforeList.size();i++){
            stationBeforeListId.add(stationBeforeList.get(i).getId());
        }
        for(int i = 0;i<stationAfterList.size();i++){
            stationAfterListId.add(stationAfterList.get(i).getId());
        }
        if(decide==true){
            for(int i =0;i<stationBeforeList.size();i++){
                List<PassengerNum> passengerNumList = passengerNumMapper.selectNumStation(stationBeforeList.get(i).getId(),this.setTime(time,publicbusStarup.getDateStatus()));
                for(int j = 0;j<passengerNumList.size();j++){
                    if(stationAfterListId.contains(passengerNumList.get(j).getEndStation())){
                        y += passengerNumList.get(j).getPassengerNum();
                        if(passengerNumList.get(j).getEndStation()<endStationId){
                            x += passengerNumList.get(j).getPassengerNum();
                        }
                    }
                }
            }
        }
        else{
            for(int i =0;i<stationAfterList.size();i++){
                List<PassengerNum> passengerNumList = passengerNumMapper.selectNumStation(stationAfterList.get(i).getId(),this.setTime(time,publicbusStarup.getDateStatus()));
                for(int j = 0;j<passengerNumList.size();j++){
                    if(stationBeforeListId.contains(passengerNumList.get(j).getEndStation())){
                        y += passengerNumList.get(j).getPassengerNum();
                        if(passengerNumList.get(j).getEndStation()>endStationId){
                            x += passengerNumList.get(j).getPassengerNum();
                        }
                    }
                }
            }
        }
        List<Integer> TranList = new ArrayList<>();
        TranList.add(new Long(Math.round((double) (x/15)*((double)lossEfficiencyTransportation/100))).intValue());
        TranList.add(new Long(Math.round((double) (y/15)*((double)lossEfficiencyTransportation/100))).intValue());
        return TranList;
    }

    /**
     * @param x 为k(C入)
     * 计算F1,F2,F3,F4,F5的值
     */
    public List<Integer> dissipatePass(List<Integer> listExportPeo,List<Integer> listEnterPeo,Integer stationNum,Integer x){
        Integer F1=0;
        Integer F2=0;
        Integer F3=0;
        Integer F4=0;
        Integer F5=0;
        if(stationNum>2){
            if(listExportPeo.get(0)<=x){ // p1-<=k(C入)
                F1 = listExportPeo.get(0);
                if(listExportPeo.get(1)<=x-listExportPeo.get(0)+listEnterPeo.get(0)){ // p2-<=k(C入)-p1-+p2+
                    F2 = listExportPeo.get(1);
                    if(stationNum>3){
                        if(listExportPeo.get(2)<=x-listExportPeo.get(0)-(listExportPeo.get(1)-listEnterPeo.get(0))+listEnterPeo.get(1)){ //p3- <= k(C入)-p1--(p2- - p2+)+p3+
                            F3 = listExportPeo.get(2);
                            if(stationNum>4){
                                if(listExportPeo.get(3)<=x-listExportPeo.get(0)-(listExportPeo.get(1)-listEnterPeo.get(0))-(listExportPeo.get(2)-listEnterPeo.get(1))+listEnterPeo.get(2)){ //p4- <= k(C入)-p1--(p2- - p2+)-(p3- - p3+)+p4+
                                    F4 = listExportPeo.get(3);
                                    if(stationNum>5){
                                        if(listExportPeo.get(3)<=x-listExportPeo.get(0)-(listExportPeo.get(1)-listEnterPeo.get(0))-(listExportPeo.get(2)-listEnterPeo.get(1))-(listExportPeo.get(3)-listEnterPeo.get(2))+listEnterPeo.get(3)){  //p5- <= k(C入)-p1--(p2- - p2+)-(p3- - p3+)-(p4- - p4+)+p5+
                                            F5 = listExportPeo.get(3);
                                        }
                                        else{
                                            F5 = x-listExportPeo.get(0)-(listExportPeo.get(1)-listEnterPeo.get(0))-(listExportPeo.get(2)-listEnterPeo.get(1))-(listExportPeo.get(3)-listEnterPeo.get(2))+listEnterPeo.get(3);
                                        }
                                    }
                                }
                                else{
                                    F4 = x-listExportPeo.get(0)-(listExportPeo.get(1)-listEnterPeo.get(0))-(listExportPeo.get(2)-listEnterPeo.get(1))+listEnterPeo.get(2);
                                    if(stationNum>5){
                                        if(listExportPeo.get(4)<=listEnterPeo.get(3)){
                                            F5 = listExportPeo.get(4);
                                        }
                                        else{
                                            F5 = listEnterPeo.get(3);
                                        }
                                    }
                                }
                            }
                        }
                        else{
                            F3 = x-listExportPeo.get(0)-(listExportPeo.get(1)-listEnterPeo.get(0))+listEnterPeo.get(1);
                            if(stationNum>4){
                                if(listExportPeo.get(3)<=listEnterPeo.get(2)){ //P4- < P4+
                                    F4 = listExportPeo.get(3);
                                    if(stationNum>5){
                                        if(listExportPeo.get(4)<= listEnterPeo.get(2)-listExportPeo.get(3)+listEnterPeo.get(3)){ //P5- <= (P4+ - P4-)+P5+
                                            F5 = listExportPeo.get(4);
                                        }
                                        else{
                                            F5 = listEnterPeo.get(2)-listExportPeo.get(3)+listEnterPeo.get(3);
                                        }
                                    }
                                }
                                else{
                                    F4 = listEnterPeo.get(2);
                                    if(stationNum>5){
                                        if(listExportPeo.get(4)<=listEnterPeo.get(3)){  //P5- <= P5+
                                            F5 = listExportPeo.get(4);
                                        }
                                        else{
                                            F5 = listEnterPeo.get(3);
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
                else{
                    F2 = x-listExportPeo.get(0)+listEnterPeo.get(0);
                    if(stationNum>3){
                        if(listExportPeo.get(2)<=listEnterPeo.get(1)){ //P3- <= P3+
                            F3 = listExportPeo.get(2);
                            if(stationNum>4){
                                if(listExportPeo.get(3)<=listEnterPeo.get(1)-listExportPeo.get(2)+listEnterPeo.get(2)){ //P4- <= P3+  - P3- + P4+
                                    F4 = listExportPeo.get(3);
                                    if(stationNum>5){
                                        if(listExportPeo.get(4)<= x-listExportPeo.get(0)-(listExportPeo.get(1)-listEnterPeo.get(0))-(listExportPeo.get(2)-listEnterPeo.get(1))-(listExportPeo.get(3)-listEnterPeo.get(2))+listEnterPeo.get(3)){ //P5- <= X-P1- - (P2- - P2+)-(P3- - P3+)-(P4- - P4+)+P5+
                                            F5 = listExportPeo.get(4);
                                        }
                                        else{
                                            F5 = x-listExportPeo.get(0)-(listExportPeo.get(1)-listEnterPeo.get(0))-(listExportPeo.get(2)-listEnterPeo.get(1))-(listExportPeo.get(3)-listEnterPeo.get(2))+listEnterPeo.get(3);
                                        }
                                    }
                                }
                                else{
                                    F4 = listEnterPeo.get(1)-listExportPeo.get(2)+listEnterPeo.get(2);
                                    if(stationNum>5){
                                        if(listExportPeo.get(4)<=listEnterPeo.get(3)){  //P5- <= P5+
                                            F5 = listExportPeo.get(4);
                                        }
                                        else{
                                            F5 = listEnterPeo.get(3);
                                        }
                                    }
                                }
                            }
                        }
                        else{
                            F3 = listEnterPeo.get(1);
                            if(stationNum>4){
                                if(listExportPeo.get(3)<=listEnterPeo.get(2)){  //P4- <= P4+
                                    F4 = listExportPeo.get(3);
                                    if(stationNum>5){
                                        if(listExportPeo.get(4)<=listEnterPeo.get(2)-listExportPeo.get(3)+listEnterPeo.get(3)){ //P5- <= P4+ - P4- + P5+
                                            F5 = listExportPeo.get(4);
                                        }
                                        else{
                                            F5 = listEnterPeo.get(2)-listExportPeo.get(3)+listEnterPeo.get(3);
                                        }
                                    }
                                }
                                else{
                                    F4 = listEnterPeo.get(2);
                                    if(stationNum>5){
                                        if(listExportPeo.get(4)<=listEnterPeo.get(3)){ //P5- <= P5+
                                            F5 = listExportPeo.get(4);
                                        }
                                        else{
                                            F5 = listEnterPeo.get(3);
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
            else{ //p1->k(C入)
                F1 = x;
                if(listExportPeo.get(1)<=listEnterPeo.get(0)){  //p2- <= p2+
                    F2 = listExportPeo.get(1);
                    if(stationNum>3){
                        if(listExportPeo.get(2)<=listEnterPeo.get(0)-listExportPeo.get(1)+listEnterPeo.get(1)){ //p2+ - p2- + p3+
                            F3 = listExportPeo.get(2);
                            if(stationNum>4){
                                if(listExportPeo.get(3)<=listEnterPeo.get(0)-listExportPeo.get(1)+listEnterPeo.get(1)-listExportPeo.get(2)+listEnterPeo.get(2)){
                                    F4 = listExportPeo.get(2);
                                    if(stationNum>5){
                                        if(listExportPeo.get(4)<=listEnterPeo.get(0)-listExportPeo.get(1)+listEnterPeo.get(1)-listExportPeo.get(2)+listEnterPeo.get(2)-listExportPeo.get(3)+listEnterPeo.get(3)){
                                            F5 = listExportPeo.get(4);
                                        }
                                        else{
                                            F5 = listEnterPeo.get(0)-listExportPeo.get(1)+listEnterPeo.get(1)-listExportPeo.get(2)+listEnterPeo.get(2)-listExportPeo.get(3)+listEnterPeo.get(3);
                                        }
                                    }
                                }
                                else{
                                    F4 = listEnterPeo.get(0)-listExportPeo.get(1)+listEnterPeo.get(1)-listExportPeo.get(2)+listEnterPeo.get(2);
                                    if(stationNum>5){
                                        if(listExportPeo.get(4)<=listEnterPeo.get(3)){
                                            F5 = listExportPeo.get(4);
                                        }
                                        else{
                                            F5 = listEnterPeo.get(3);
                                        }
                                    }
                                }
                            }
                        }
                        else{
                            F3 = listEnterPeo.get(0)-listExportPeo.get(1)+listEnterPeo.get(1);
                            if(stationNum>4){
                                if(listExportPeo.get(3)<=listEnterPeo.get(2)){
                                    F4 = listExportPeo.get(3);
                                    if(stationNum>5){
                                        if(listExportPeo.get(4)<=listEnterPeo.get(2)-listExportPeo.get(3)+listEnterPeo.get(3)){
                                            F5 = listExportPeo.get(4);
                                        }
                                        else{
                                            F5 = listEnterPeo.get(2)-listExportPeo.get(3)+listEnterPeo.get(3);
                                        }
                                    }
                                }
                                else{
                                    F4 = listEnterPeo.get(2);
                                    if(stationNum>5){
                                        if(listExportPeo.get(4)<=listEnterPeo.get(3)){
                                            F5 = listExportPeo.get(4);
                                        }
                                        else{
                                            F5 = listEnterPeo.get(3);
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
                else{
                    F2 = listEnterPeo.get(0);
                    if(stationNum>3){
                        if(listExportPeo.get(2)<=listEnterPeo.get(1)){
                            F3 = listExportPeo.get(2);
                            if(stationNum>4){
                                if(listExportPeo.get(3)<=listEnterPeo.get(1)-listExportPeo.get(2)+listEnterPeo.get(2)){
                                    F4 = listExportPeo.get(3);
                                    if(stationNum>5){
                                        if(listExportPeo.get(4)<=listEnterPeo.get(1)-listExportPeo.get(2)+listEnterPeo.get(2)-listExportPeo.get(3)+listEnterPeo.get(3)){
                                            F5 = listExportPeo.get(4);
                                        }
                                        else{
                                            F5 = listEnterPeo.get(1)-listExportPeo.get(2)+listEnterPeo.get(2)-listExportPeo.get(3)+listEnterPeo.get(3);
                                        }
                                    }
                                }
                                else{
                                    F4 = listEnterPeo.get(1)-listExportPeo.get(2)+listEnterPeo.get(2);
                                    if(stationNum>5){
                                        if(listExportPeo.get(4)<=listEnterPeo.get(3)){
                                            F5 = listExportPeo.get(4);
                                        }
                                        else{
                                            F5 = listEnterPeo.get(3);
                                        }
                                    }
                                }
                            }
                        }
                        else{
                            F3 = listEnterPeo.get(1);
                            if(stationNum>4){
                                if(listExportPeo.get(3)<=listEnterPeo.get(2)){
                                    F4 = listExportPeo.get(3);
                                    if(stationNum>5){
                                        if(listExportPeo.get(4)<=listEnterPeo.get(2)-listExportPeo.get(3)+listEnterPeo.get(3)){
                                            F5 = listExportPeo.get(4);
                                        }
                                        else{
                                            F5 = listEnterPeo.get(2)-listExportPeo.get(3)+listEnterPeo.get(3);
                                        }
                                    }
                                }
                                else{
                                    F4 = listEnterPeo.get(2);
                                    if(stationNum>5){
                                        if(listExportPeo.get(4)<=listEnterPeo.get(3)){
                                            F5 = listExportPeo.get(4);
                                        }
                                        else{
                                            listEnterPeo.get(3);
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
        List<Integer> Flist = new ArrayList<>();
        Flist.add(F1);
        Flist.add(F2);
        Flist.add(F3);
        Flist.add(F4);
        Flist.add(F5);

        return Flist;
    }

}
