/**
 * 初始化字典详情对话框
 */
var DictInfoDlg = {
    count: $("#itemSize").val(),
    dictName: '',			//字典的名称
    mutiString: '',		//拼接字符串内容(拼接字典条目)
    itemTemplate: $("#itemTemplate").html(),
    validateFields: {
        dictName: {
            validators: {
                notEmpty: {
                    message: '字典名称不能为空'
                }
            }
        },
        dictCode: {
            validators: {
                notEmpty: {
                    message: '编码不能为空'
                }
            }
        }
    }
};

/**
 * item获取新的id
 */
DictInfoDlg.newId = function () {
    if(this.count == undefined){
        this.count = 0;
    }
    this.count = this.count + 1;
    return "dictItem" + this.count;
};

/**
 * 关闭此对话框
 */
DictInfoDlg.close = function () {
    parent.layer.close(window.parent.Dict.layerIndex);
};
/**
 * 添加条目
 */
DictInfoDlg.addItem = function () {
    $("#itemsArea").append(this.itemTemplate);
    $("#dictItem").attr("id", this.newId());
};

/**
 * 删除item
 */
DictInfoDlg.deleteItem = function (event) {
    var obj = Feng.eventParseObject(event);
    obj.parent().parent().remove();
};

/**
 * 清除为空的item Dom
 */
DictInfoDlg.clearNullDom = function(){
    $("[name='dictItem']").each(function(){
        var code = $(this).find("[name='itemCode']").val();
        var name = $(this).find("[name='itemName']").val();
        if(code == '' || name == ''){
            $(this).remove();
        }
    });
};

/**
 * 收集添加字典的数据
 */
DictInfoDlg.collectData = function () {
    this.clearNullDom();
    var mutiString = "";
    $("[name='dictItem']").each(function(){
        var code = $(this).find("[name='itemCode']").val();
        var name = $(this).find("[name='itemName']").val();
        mutiString = mutiString + (code + ":" + name + ";");
    });
    this.dictName = $("#dictName").val();
    this.dictCode = $("#dictCode").val();
    this.mutiString = mutiString;
};
/**
 * 验证数据是否为空
 */
DictInfoDlg.validate = function () {
    $('#dictInfoForm').data("bootstrapValidator").resetForm();
    $('#dictInfoForm').bootstrapValidator('validate');
    return $("#dictInfoForm").data('bootstrapValidator').isValid();
}



/**
 * 提交添加字典
 */
DictInfoDlg.addSubmit = function () {
    this.collectData();
    if (!this.validate()) {
        return;
    }
    //提交信息
    var ajax = new $ax(Feng.ctxPath + "/dict/add", function (data) {
        Feng.success("添加成功!");
        window.parent.Dict.table.refresh();
        DictInfoDlg.close();
    }, function (data) {
        Feng.error("添加失败!" + data.responseJSON.message + "!");
    });
    ajax.set('dictName',this.dictName);
    ajax.set('dictCode',this.dictCode);
    ajax.set('dictValues',this.mutiString);
    ajax.start();
};

/**
 * 提交修改
 */
DictInfoDlg.editSubmit = function () {
    this.collectData();
    if (!this.validate()) {
        return;
    }
    var ajax = new $ax(Feng.ctxPath + "/dict/update", function (data) {
        Feng.success("修改成功!");
        window.parent.Dict.table.refresh();
        DictInfoDlg.close();
    }, function (data) {
        Feng.error("修改失败!" + data.responseJSON.message + "!");
    });
    ajax.set('dictId',$("#dictId").val());
    ajax.set('dictName',this.dictName);
    ajax.set('dictCode',this.dictCode);
    ajax.set('dictValues',this.mutiString);
    ajax.start();
};
$(function() {

    Feng.initValidator("dictInfoForm", DictInfoDlg.validateFields);

    // var ztree = new $ZTree("treeDemo", "/contactGroup/tree");
    // ztree.bindOnClick(ContactInfoDlg.onClickContactGroup);
    // ztree.init();
    // instance = ztree;
});