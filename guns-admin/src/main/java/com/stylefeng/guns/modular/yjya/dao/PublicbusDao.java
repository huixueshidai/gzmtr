package com.stylefeng.guns.modular.yjya.dao;

import com.baomidou.mybatisplus.plugins.Page;
import com.stylefeng.guns.common.persistence.model.Publicbus;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

/**
 * Created by 60132 on 2018/9/4.
 */
public interface PublicbusDao {
    /**
     *根据条件查询预案列表
     * @param page
     * @return
     */
    List<Map<String, Object>> getPublicBus(@Param("page") Page<Publicbus> page);
}
