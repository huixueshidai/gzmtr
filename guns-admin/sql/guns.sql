/*
Navicat MySQL Data Transfer

Source Server         : 本地
Source Server Version : 50621
Source Host           : localhost:3306
Source Database       : guns

Target Server Type    : MYSQL
Target Server Version : 50621
File Encoding         : 65001

Date: 2017-12-10 11:34:35
*/

DROP DATABASE IF EXISTS guns_flowable;
CREATE DATABASE guns_flowable;

DROP DATABASE IF EXISTS guns;
CREATE DATABASE guns;

USE guns;

SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for sys_dept
-- ----------------------------
DROP TABLE IF EXISTS `sys_dept`;
CREATE TABLE `sys_dept` (
  `id`         INT(11) NOT NULL AUTO_INCREMENT COMMENT '主键id',
  `num`        INT(11)          DEFAULT NULL COMMENT '排序',
  `pid`        INT(11)          DEFAULT NULL COMMENT '父部门id',
  `pids`       VARCHAR(255)     DEFAULT NULL COMMENT '父级ids',
  `simplename` VARCHAR(45)      DEFAULT NULL COMMENT '简称',
  `fullname`   VARCHAR(255)     DEFAULT NULL COMMENT '全称',
  `tips`       VARCHAR(255)     DEFAULT NULL COMMENT '提示',
  `version`    INT(11)          DEFAULT NULL COMMENT '版本（乐观锁保留字段)',
  PRIMARY KEY (`id`)
)
  ENGINE = InnoDB AUTO_INCREMENT = 28 DEFAULT CHARSET = utf8 COMMENT ='部门表';

-- ----------------------------
-- Records of sys_dept
-- ----------------------------
INSERT INTO `sys_dept` VALUES ('24', '1', '0', '[0],', '总公司', '总公司', '', NULL);
INSERT INTO `sys_dept` VALUES ('25', '2', '24', '[0],[24],', '开发部', '开发部', '', NULL);
INSERT INTO `sys_dept` VALUES ('26', '3', '24', '[0],[24],', '运营部', '运营部', '', NULL);
INSERT INTO `sys_dept` VALUES ('27', '4', '24', '[0],[24],', '战略部', '战略部', '', NULL);

-- ----------------------------
-- Table structure for sys_dict
-- ----------------------------
DROP TABLE IF EXISTS `sys_dict`;
CREATE TABLE `sys_dict` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键id',
  `code` char(30) DEFAULT NULL COMMENT '编码',
  `pid` int(11) DEFAULT NULL COMMENT '父级字典',
  `name` varchar(255) DEFAULT NULL COMMENT '名称',
  `tips` varchar(255) DEFAULT NULL COMMENT '提示',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=144 DEFAULT CHARSET=utf8 COMMENT='字典表';

-- ----------------------------
-- Records of sys_dict
-- ----------------------------
INSERT INTO `sys_dict` VALUES ('29', 'sex', '0', '性别', null),
  ('30', '1', '29', '男', null),
  ('31', '2', '29', '女', null),
  ('35', 'account_status', '0', '账号状态', null),
  ('36', '1', '35', '启用', null),
  ('37', '2', '35', '冻结', null),
  ('38', '3', '35', '已删除', null),
  ('44', 'status', '0', '状态', null),
  ('45', '1', '44', '启用', null),
  ('46', '2', '44', '禁用', null),
  ('59', 'responseLevel', '0', '响应等级', null),
  ('60', '1', '59', '一级', null),
  ('61', '2', '59', '二级', null),
  ('62', '3', '59', '三级', null),
  ('136', 'counterplanType', '0', '预案类别', null),
  ('137', '1', '136', '综合预案', null),
  ('138', '2', '136', '专项预案', null),
  ('139', '3', '136', '现场处置方案', null),
  ('71', 'interruption_type', '0', '中断类型', null),
  ('72', '1', '71', '单向行车中断', null),
  ('73', '2', '71', '双向行车中断', null),
  ('82', 'counterplanStatus', '0', '预案状态', null),
  ('83', '0', '82', '未审核', null),
  ('84', '1', '82', '审核通过', null),
  ('85', '2', '82', '审核未通过', null),
  ('86', 'yes_or_no', '0', '是或否', null),
  ('87', 'Y', '86', '是', null),
  ('88', 'N', '86', '否', null),
  ('97', 'accident_main', '0', '事故处理主体', null),
  ('98', '0', '97', 'COCC', ''),
  ('99', '1', '97', '电调', ''),
  ('100', '2', '97', '行调', ''),
  ('101', '3', '97', '环调', ''),
  ('102', '4', '97', '综合调度', ''),
  ('103', '5', '97', '值班主任助理', ''),
  ('104', '6', '97', '值班主任', ''),
  ('105', '7', '97', '站台岗/巡视岗', ''),
  ('106', '8', '97', '客运值班员', ''),
  ('107', '9', '97', '行车值班员', ''),
  ('108', '10', '97', '值班站长', ''),
  ('109', '11', '97', '站务员', ''),
  ('110', '12', '97', '保洁人员', ''),
  ('111', '13', '97', '售票员（1）', ''),
  ('112', '14', '97', '售票员（2）', ''),
  ('113', '15', '97', '其他驻站人员', ''),
  ('114', '16', '97', '保洁、商铺等驻站人员', ''),
  ('115', '17', '97', '司机', ''),
  ('116', '18', '97', '厅巡岗', ''),
  ('117', '19', '97', '票亭岗', ''),
  ('118', '20', '97', '屏蔽门操作员', ''),
  ('119', '21', '97', '消防队', ''),
  ('120', '22', '97', '消防负责人', ''),
  ('121', '23', '97', '公安', ''),
  ('122', '24', '97', '警务人员', ''),
  ('123', '25', '97', 'OCC', null),
  ('124', '26', '97', '所有岗位', null),
  ('125', '27', '97', '安全质量部', null),
  ('126', '28', '97', '集团公司安全检监察部', null),
  ('127', '29', '97', '总部值班领导', null),
  ('128', '30', '97', '分管副总经理', null),
  ('132', 'roadnetType', '0', '路网类型', null),
  ('133', '1', '132', '区域中心', null),
  ('134', '2', '132', '线路', null),
  ('135', '3', '132', '车站', null),
  ('142', 'accidentType', '0', '事故类型', null),
  ('143', '1', '142', '火源', null),
  ('144', '2', '142', '可燃物', null),
  ('145', '3', '142', '报警设备不健全', null),
  ('146', '4', '142', '管理不完善', null),
  ('147', '5', '142', '灭火设施不起作用', null),
  ('148', 'categoryCode', '0', '类别', null),
  ('149', '1', '148', '人员', null),
  ('150', '2', '148', '设备', null),
  ('151', '3', '148', '环境', null),
  ('152', '4', '148', '管理', null),
  ('156', 'accidentArea', '0', '事故区域', null),
  ('157', '1', '156', '公共区', null),
  ('158', '2', '156', '非公共区', null),
  ('159', '3', '156', '区间', null),
  ('160', 'rank', '0', '级别', null),
  ('161', '1', '160', '安全事故', null),
  ('162', '2', '160', '安全事件', null),
  ('163', '3', '160', '风险事件', null);



-- ----------------------------
-- Table structure for sys_expense
-- ----------------------------
DROP TABLE IF EXISTS `sys_expense`;
CREATE TABLE `sys_expense` (
  `id`         INT(11) NOT NULL AUTO_INCREMENT,
  `money`      DECIMAL(20, 2)   DEFAULT NULL COMMENT '报销金额',
  `detail`       VARCHAR(255)     DEFAULT '' COMMENT '描述',
  `createtime` DATETIME         ,
  `state`      INT(11)          DEFAULT NULL COMMENT '状态: 1.待提交  2:待审核   3.审核通过 4:驳回',
  `userid`     INT(11)          DEFAULT NULL COMMENT '用户id',
  `processId`  VARCHAR(255)     DEFAULT NULL COMMENT '流程定义id',
  PRIMARY KEY (`id`)
)
  ENGINE = InnoDB AUTO_INCREMENT = 23 DEFAULT CHARSET = utf8 COMMENT ='报销表';

-- ----------------------------
-- Records of sys_expense
-- ----------------------------

-- ----------------------------
-- Table structure for sys_login_log
-- ----------------------------
DROP TABLE IF EXISTS `sys_login_log`;
CREATE TABLE `sys_login_log` (
  `id`         INT(65) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `logname`    VARCHAR(255)     DEFAULT NULL COMMENT '日志名称',
  `userid`     INT(65)          DEFAULT NULL COMMENT '管理员id',
  `createtime` DATETIME         DEFAULT NULL COMMENT '创建时间',
  `succeed`    VARCHAR(255)     DEFAULT NULL COMMENT '是否执行成功', `message`    TEXT COMMENT '具体消息',
  `ip`         VARCHAR(255)     DEFAULT NULL COMMENT '登录ip',
  PRIMARY KEY (`id`)
)
  ENGINE = InnoDB AUTO_INCREMENT = 216 DEFAULT CHARSET = utf8 COMMENT ='登录记录';

/*
Navicat MySQL Data Transfer

Source Server         : aaa
Source Server Version : 50634
Source Host           : 127.0.0.1:3306
Source Database       : guns

Target Server Type    : MYSQL
Target Server Version : 50634
File Encoding         : 65001

Date: 2018-05-30 18:39:09
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for `sys_menu`
-- ----------------------------
DROP TABLE IF EXISTS `sys_menu`;
CREATE TABLE `sys_menu` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '主键id',
  `code` varchar(255) DEFAULT NULL COMMENT '菜单编号',
  `pcode` varchar(255) DEFAULT NULL COMMENT '菜单父编号',
  `pcodes` varchar(255) DEFAULT NULL COMMENT '当前菜单的所有父菜单编号',
  `name` varchar(255) DEFAULT NULL COMMENT '菜单名称',
  `icon` varchar(255) DEFAULT NULL COMMENT '菜单图标',
  `url` varchar(255) DEFAULT NULL COMMENT 'url地址',
  `num` int(65) DEFAULT NULL COMMENT '菜单排序号',
  `levels` int(65) DEFAULT NULL COMMENT '菜单层级',
  `ismenu` int(11) DEFAULT NULL COMMENT '是否是菜单（1：是  0：不是）',
  `tips` varchar(255) DEFAULT NULL COMMENT '备注',
  `status` int(65) DEFAULT NULL COMMENT '菜单状态 :  1:启用   0:不启用',
  `isopen` int(11) DEFAULT NULL COMMENT '是否打开:    1:打开   0:不打开',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=309 DEFAULT CHARSET=utf8 COMMENT='菜单表';

-- ----------------------------
-- Records of sys_menu
-- ----------------------------
INSERT INTO `sys_menu` VALUES ('105', 'system', '0', '[0],', '系统管理', 'fa-user', '#', '100', '1', '1', null, '1', '1');
INSERT INTO `sys_menu` VALUES ('106', 'mgr', 'system', '[0],[system],', '用户管理', '', '/mgr', '1', '2', '1', null, '1', '0');
INSERT INTO `sys_menu` VALUES ('107', 'mgr_add', 'mgr', '[0],[system],[mgr],', '添加用户', null, '/mgr/add', '1', '3', '0', null, '1', '0');
INSERT INTO `sys_menu` VALUES ('108', 'mgr_edit', 'mgr', '[0],[system],[mgr],', '修改用户', null, '/mgr/edit', '2', '3', '0', null, '1', '0');
INSERT INTO `sys_menu` VALUES ('109', 'mgr_delete', 'mgr', '[0],[system],[mgr],', '删除用户', null, '/mgr/delete', '3', '3', '0', null, '1', '0');
INSERT INTO `sys_menu` VALUES ('110', 'mgr_reset', 'mgr', '[0],[system],[mgr],', '重置密码', null, '/mgr/reset', '4', '3', '0', null, '1', '0');
INSERT INTO `sys_menu` VALUES ('111', 'mgr_freeze', 'mgr', '[0],[system],[mgr],', '冻结用户', null, '/mgr/freeze', '5', '3', '0', null, '1', '0');
INSERT INTO `sys_menu` VALUES ('112', 'mgr_unfreeze', 'mgr', '[0],[system],[mgr],', '解除冻结用户', null, '/mgr/unfreeze', '6', '3', '0', null, '1', '0');
INSERT INTO `sys_menu` VALUES ('113', 'mgr_setRole', 'mgr', '[0],[system],[mgr],', '分配角色', null, '/mgr/setRole', '7', '3', '0', null, '1', '0');
INSERT INTO `sys_menu` VALUES ('114', 'role', 'system', '[0],[system],', '角色管理', null, '/role', '2', '2', '1', null, '1', '0');
INSERT INTO `sys_menu` VALUES ('115', 'role_add', 'role', '[0],[system],[role],', '添加角色', null, '/role/add', '1', '3', '0', null, '1', '0');
INSERT INTO `sys_menu` VALUES ('116', 'role_edit', 'role', '[0],[system],[role],', '修改角色', null, '/role/edit', '2', '3', '0', null, '1', '0');
INSERT INTO `sys_menu` VALUES ('117', 'role_remove', 'role', '[0],[system],[role],', '删除角色', null, '/role/remove', '3', '3', '0', null, '1', '0');
INSERT INTO `sys_menu` VALUES ('118', 'role_setAuthority', 'role', '[0],[system],[role],', '配置权限', null, '/role/setAuthority', '4', '3', '0', null, '1', '0');
INSERT INTO `sys_menu` VALUES ('119', 'menu', 'system', '[0],[system],', '菜单管理', null, '/menu', '4', '2', '1', null, '1', '0');
INSERT INTO `sys_menu` VALUES ('120', 'menu_add', 'menu', '[0],[system],[menu],', '添加菜单', null, '/menu/add', '1', '3', '0', null, '1', '0');
INSERT INTO `sys_menu` VALUES ('121', 'menu_edit', 'menu', '[0],[system],[menu],', '修改菜单', null, '/menu/edit', '2', '3', '0', null, '1', '0');
INSERT INTO `sys_menu` VALUES ('122', 'menu_remove', 'menu', '[0],[system],[menu],', '删除菜单', null, '/menu/remove', '3', '3', '0', null, '1', '0');
INSERT INTO `sys_menu` VALUES ('128', 'log', 'system', '[0],[system],', '业务日志', null, '/log', '6', '2', '1', null, '1', '0');
INSERT INTO `sys_menu` VALUES ('130', 'druid', 'system', '[0],[system],', '监控管理', null, '/druid', '7', '2', '1', null, '1', null);
INSERT INTO `sys_menu` VALUES ('131', 'dept', 'system', '[0],[system],', '部门管理', null, '/dept', '3', '2', '1', null, '1', null);
INSERT INTO `sys_menu` VALUES ('132', 'dict', 'system', '[0],[system],', '字典管理', null, '/dict', '4', '2', '1', null, '1', null);
INSERT INTO `sys_menu` VALUES ('133', 'loginLog', 'system', '[0],[system],', '登录日志', null, '/loginLog', '6', '2', '1', null, '1', null);
INSERT INTO `sys_menu` VALUES ('134', 'log_clean', 'log', '[0],[system],[log],', '清空日志', null, '/log/delLog', '3', '3', '0', null, '1', null);
INSERT INTO `sys_menu` VALUES ('135', 'dept_add', 'dept', '[0],[system],[dept],', '添加部门', null, '/dept/add', '1', '3', '0', null, '1', null);
INSERT INTO `sys_menu` VALUES ('136', 'dept_update', 'dept', '[0],[system],[dept],', '修改部门', null, '/dept/update', '1', '3', '0', null, '1', null);
INSERT INTO `sys_menu` VALUES ('137', 'dept_delete', 'dept', '[0],[system],[dept],', '删除部门', null, '/dept/delete', '1', '3', '0', null, '1', null);
INSERT INTO `sys_menu` VALUES ('138', 'dict_add', 'dict', '[0],[system],[dict],', '添加字典', null, '/dict/add', '1', '3', '0', null, '1', null);
INSERT INTO `sys_menu` VALUES ('139', 'dict_update', 'dict', '[0],[system],[dict],', '修改字典', null, '/dict/update', '1', '3', '0', null, '1', null);
INSERT INTO `sys_menu` VALUES ('140', 'dict_delete', 'dict', '[0],[system],[dict],', '删除字典', null, '/dict/delete', '1', '3', '0', null, '1', null);
INSERT INTO `sys_menu` VALUES ('141', 'notice', 'system', '[0],[system],', '通知管理', null, '/notice', '9', '2', '1', null, '1', null);
INSERT INTO `sys_menu` VALUES ('142', 'notice_add', 'notice', '[0],[system],[notice],', '添加通知', null, '/notice/add', '1', '3', '0', null, '1', null);
INSERT INTO `sys_menu` VALUES ('143', 'notice_update', 'notice', '[0],[system],[notice],', '修改通知', null, '/notice/update', '2', '3', '0', null, '1', null);
INSERT INTO `sys_menu` VALUES ('144', 'notice_delete', 'notice', '[0],[system],[notice],', '删除通知', null, '/notice/delete', '3', '3', '0', null, '1', null);
INSERT INTO `sys_menu` VALUES ('145', 'hello', '0', '[0],', '通知', 'fa-rocket', '/notice/hello', '0', '1', '1', null, '1', null);
INSERT INTO `sys_menu` VALUES ('148', 'code', '0', '[0],', '代码生成', 'fa-code', '/code', '60', '1', '1', null, '1', null);
INSERT INTO `sys_menu` VALUES ('150', 'to_menu_edit', 'menu', '[0],[system],[menu],', '菜单编辑跳转', '', '/menu/menu_edit', '4', '3', '0', null, '1', null);
INSERT INTO `sys_menu` VALUES ('151', 'menu_list', 'menu', '[0],[system],[menu],', '菜单列表', '', '/menu/list', '5', '3', '0', null, '1', null);
INSERT INTO `sys_menu` VALUES ('152', 'to_dept_update', 'dept', '[0],[system],[dept],', '修改部门跳转', '', '/dept/dept_update', '4', '3', '0', null, '1', null);
INSERT INTO `sys_menu` VALUES ('153', 'dept_list', 'dept', '[0],[system],[dept],', '部门列表', '', '/dept/list', '5', '3', '0', null, '1', null);
INSERT INTO `sys_menu` VALUES ('154', 'dept_detail', 'dept', '[0],[system],[dept],', '部门详情', '', '/dept/detail', '6', '3', '0', null, '1', null);
INSERT INTO `sys_menu` VALUES ('155', 'to_dict_edit', 'dict', '[0],[system],[dict],', '修改菜单跳转', '', '/dict/dict_edit', '4', '3', '0', null, '1', null);
INSERT INTO `sys_menu` VALUES ('156', 'dict_list', 'dict', '[0],[system],[dict],', '字典列表', '', '/dict/list', '5', '3', '0', null, '1', null);
INSERT INTO `sys_menu` VALUES ('157', 'dict_detail', 'dict', '[0],[system],[dict],', '字典详情', '', '/dict/detail', '6', '3', '0', null, '1', null);
INSERT INTO `sys_menu` VALUES ('158', 'log_list', 'log', '[0],[system],[log],', '日志列表', '', '/log/list', '2', '3', '0', null, '1', null);
INSERT INTO `sys_menu` VALUES ('159', 'log_detail', 'log', '[0],[system],[log],', '日志详情', '', '/log/detail', '3', '3', '0', null, '1', null);
INSERT INTO `sys_menu` VALUES ('160', 'del_login_log', 'loginLog', '[0],[system],[loginLog],', '清空登录日志', '', '/loginLog/delLoginLog', '1', '3', '0', null, '1', null);
INSERT INTO `sys_menu` VALUES ('161', 'login_log_list', 'loginLog', '[0],[system],[loginLog],', '登录日志列表', '', '/loginLog/list', '2', '3', '0', null, '1', null);
INSERT INTO `sys_menu` VALUES ('162', 'to_role_edit', 'role', '[0],[system],[role],', '修改角色跳转', '', '/role/role_edit', '5', '3', '0', null, '1', null);
INSERT INTO `sys_menu` VALUES ('163', 'to_role_assign', 'role', '[0],[system],[role],', '角色分配跳转', '', '/role/role_assign', '6', '3', '0', null, '1', null);
INSERT INTO `sys_menu` VALUES ('164', 'role_list', 'role', '[0],[system],[role],', '角色列表', '', '/role/list', '7', '3', '0', null, '1', null);
INSERT INTO `sys_menu` VALUES ('165', 'to_assign_role', 'mgr', '[0],[system],[mgr],', '分配角色跳转', '', '/mgr/role_assign', '8', '3', '0', null, '1', null);
INSERT INTO `sys_menu` VALUES ('166', 'to_user_edit', 'mgr', '[0],[system],[mgr],', '编辑用户跳转', '', '/mgr/user_edit', '9', '3', '0', null, '1', null);
INSERT INTO `sys_menu` VALUES ('167', 'mgr_list', 'mgr', '[0],[system],[mgr],', '用户列表', '', '/mgr/list', '10', '3', '0', null, '1', null);
INSERT INTO `sys_menu` VALUES ('171', 'stationmanager', '0', '[0],', '车站信息管理', 'fa-rocket', '#', '55', '1', '1', null, '1', null);
INSERT INTO `sys_menu` VALUES ('172', 'basedata', '0', '[0],', '基本数据', 'fa-rocket', '#', '50', '1', '1', null, '1', null);
INSERT INTO `sys_menu` VALUES ('173', 'resource', 'basedata', '[0],[basedata],', '应急资源管理', '', '/resource', '1', '2', '1', null, '1', null);
INSERT INTO `sys_menu` VALUES ('174', 'resource_add', 'resource', '[0],[basedata],[resource],', '添加资源', '', '/resource/add', '1', '3', '0', null, '1', null);
INSERT INTO `sys_menu` VALUES ('175', 'resource_delete', 'resource', '[0],[basedata],[resource],', '删除资源', '', '/resource/delete', '2', '3', '0', null, '1', null);
INSERT INTO `sys_menu` VALUES ('176', 'contactGroup', 'basedata', '[0],[basedata],', '通讯录群组', '', '/contactGroup', '2', '2', '1', null, '1', null);
INSERT INTO `sys_menu` VALUES ('177', 'menu_addAddDeleteUpdate', 'menu', '[0],[system],[menu],', '增加增删改按钮', '', '/menu/addAddDeleteUpdate', '6', '3', '0', null, '1', null);
INSERT INTO `sys_menu` VALUES ('178', 'contactGroup_add', 'contactGroup', '[0],[basedata],[contactGroup],', '增加', null, '/contactGroup/add', '1', '3', '0', null, '1', null);
INSERT INTO `sys_menu` VALUES ('181', 'roadnet', 'stationmanager', '[0],[stationmanager],', '路网管理', '', '/roadnet', '3', '2', '1', null, '1', null);
INSERT INTO `sys_menu` VALUES ('182', 'road_add', 'roadnet', '[0],[stationmanager],[roadnet],', '添加路网', '', '/roadnet/add', '1', '3', '0', null, '1', null);
INSERT INTO `sys_menu` VALUES ('183', 'roadnet_update', 'roadnet', '[0],[stationmanager],[roadnet],', '修改路网', '', '/roadnet/update', '2', '3', '0', null, '1', null);
INSERT INTO `sys_menu` VALUES ('184', 'roadnet_delete', 'roadnet', '[0],[stationmanager],[roadnet],', '路网删除', '', '/roadnet/delete', '3', '3', '0', null, '1', null);
INSERT INTO `sys_menu` VALUES ('190', 'contactGroup_delete', 'contactGroup', '[0],[basedata],[contactGroup],', '删除', null, '/contactGroup/delete', '2', '3', '0', null, '1', null);
INSERT INTO `sys_menu` VALUES ('191', 'contactGroup_update', 'contactGroup', '[0],[basedata],[contactGroup],', '更新', null, '/contactGroup/update', '3', '3', '0', null, '1', null);
INSERT INTO `sys_menu` VALUES ('192', 'resource_update', 'resource', '[0],[basedata],[resource],', '更新资源', '', '/resource/update', '3', '3', '0', null, '1', null);
INSERT INTO `sys_menu` VALUES ('193', 'contact', 'basedata', '[0],[basedata],', '通讯录', '', '/contact', '3', '2', '1', null, '1', null);
INSERT INTO `sys_menu` VALUES ('194', 'contact_add', 'contact', '[0],[basedata],[contact],', '增加', null, '/contact/add', '1', '3', '0', null, '1', null);
INSERT INTO `sys_menu` VALUES ('195', 'contact_delete', 'contact', '[0],[basedata],[contact],', '删除', null, '/contact/delete', '2', '3', '0', null, '1', null);
INSERT INTO `sys_menu` VALUES ('196', 'contact_update', 'contact', '[0],[basedata],[contact],', '更新', null, '/contact/update', '3', '3', '0', null, '1', null);
INSERT INTO `sys_menu` VALUES ('202', '1', '0', '[0],', '应急预案', 'fa-rocket', '#', '20', '1', '1', null, '1', null);
INSERT INTO `sys_menu` VALUES ('203', 'counterplan_template', '1', '[0],[1],', ' 预案模板', '', '/counterplanTemplate', '1', '2', '1', null, '1', null);
INSERT INTO `sys_menu` VALUES ('204', 'counterplan_template_add', 'counterplan_template', '[0],[1],[counterplan_template],', '增加', null, '/counterplanTemplate/add', '1', '3', '0', null, '1', null);
INSERT INTO `sys_menu` VALUES ('205', 'counterplan_template_delete', 'counterplan_template', '[0],[1],[counterplan_template],', '删除', null, '/counterplanTemplate/delete', '2', '3', '0', null, '1', null);
INSERT INTO `sys_menu` VALUES ('206', 'counterplan_template_update', 'counterplan_template', '[0],[1],[counterplan_template],', '更新', null, '/counterplanTemplate/update', '3', '3', '0', null, '1', null);
INSERT INTO `sys_menu` VALUES ('207', 'accidentType', 'basedata', '[0],[basedata],', '事故类型', '', '/accidentType', '20', '2', '1', null, '1', null);
INSERT INTO `sys_menu` VALUES ('208', 'accidentType_add', 'accidentType', '[0],[basedata],[accidentType],', '增加', null, '/accidentType/add', '1', '3', '0', null, '1', null);
INSERT INTO `sys_menu` VALUES ('209', 'accidentType_delete', 'accidentType', '[0],[basedata],[accidentType],', '删除', null, '/accidentType/delete', '2', '3', '0', null, '1', null);
INSERT INTO `sys_menu` VALUES ('210', 'accidentType_update', 'accidentType', '[0],[basedata],[accidentType],', '更新', null, '/accidentType/update', '3', '3', '0', null, '1', null);
INSERT INTO `sys_menu` VALUES ('212', 'accidentProcess', 'basedata', '[0],[basedata],', '事故处理措施', '', '/accidentProcess', '30', '2', '1', null, '1', null);
INSERT INTO `sys_menu` VALUES ('213', 'accidentProcess_add', 'accidentProcess', '[0],[basedata],[accidentProcess],', '增加', null, '/accidentProcess/add', '1', '3', '0', null, '1', null);
INSERT INTO `sys_menu` VALUES ('214', 'accidentProcess_delete', 'accidentProcess', '[0],[basedata],[accidentProcess],', '删除', null, '/accidentProcess/delete', '2', '3', '0', null, '1', null);
INSERT INTO `sys_menu` VALUES ('215', 'accidentProcess_update', 'accidentProcess', '[0],[basedata],[accidentProcess],', '更新', null, '/accidentProcess/update', '3', '3', '0', null, '1', null);
INSERT INTO `sys_menu` VALUES ('216', 'law', 'basedata', '[0],[basedata],', '法律法规', '', '/law', '10', '2', '1', null, '1', null);
INSERT INTO `sys_menu` VALUES ('217', 'law_add', 'law', '[0],[basedata],[law],', '增加', null, '/law/add', '1', '3', '0', null, '1', null);
INSERT INTO `sys_menu` VALUES ('218', 'law_delete', 'law', '[0],[basedata],[law],', '删除', null, '/law/delete', '2', '3', '0', null, '1', null);
INSERT INTO `sys_menu` VALUES ('219', 'law_update', 'law', '[0],[basedata],[law],', '更新', null, '/law/update', '3', '3', '0', null, '1', null);
INSERT INTO `sys_menu` VALUES ('227', 'counterplan', '1', '[0],[1],', '预案编制', '', '/counterplan', '2', '2', '1', null, '1', null);
INSERT INTO `sys_menu` VALUES ('228', 'counterplan_add', 'counterplan', '[0],[1],[counterplan],', '增加', null, '/counterplan/add', '1', '3', '0', null, '1', null);
INSERT INTO `sys_menu` VALUES ('229', 'counterplan_delete', 'counterplan', '[0],[1],[counterplan],', '删除', null, '/counterplan/delete', '2', '3', '0', null, '1', null);
INSERT INTO `sys_menu` VALUES ('230', 'counterplan_update', 'counterplan', '[0],[1],[counterplan],', '更新', null, '/counterplan/update', '3', '3', '0', null, '1', null);
INSERT INTO `sys_menu` VALUES ('231', 'counterplanTypeTemplate', '1', '[0],[1],', '预案类型模板', '', '/counterplanTypeTemplate', '0', '2', '1', null, '1', null);
INSERT INTO `sys_menu` VALUES ('232', 'counterplanTypeTemplate_add', 'counterplanTypeTemplate', '[0],[1],[counterplanTypeTemplate],', '增加', null, '/counterplanTypeTemplate/add', '1', '3', '0', null, '1', null);
INSERT INTO `sys_menu` VALUES ('233', 'counterplanTypeTemplate_delete', 'counterplanTypeTemplate', '[0],[1],[counterplanTypeTemplate],', '删除', null, '/counterplanTypeTemplate/delete', '2', '3', '0', null, '1', null);
INSERT INTO `sys_menu` VALUES ('234', 'counterplanTypeTemplate_update', 'counterplanTypeTemplate', '[0],[1],[counterplanTypeTemplate],', '更新', null, '/counterplanTypeTemplate/update', '3', '3', '0', null, '1', null);
INSERT INTO `sys_menu` VALUES ('235', 'accident', '1', '[0],[1],', '事故实例', '', '/accident', '5', '2', '1', null, '1', null);
INSERT INTO `sys_menu` VALUES ('236', 'accident_add', 'accident', '[0],[1],[accident],', '增加', null, '/accident/add', '1', '3', '0', null, '1', null);
INSERT INTO `sys_menu` VALUES ('237', 'accident_delete', 'accident', '[0],[1],[accident],', '删除', null, '/accident/delete', '2', '3', '0', null, '1', null);
INSERT INTO `sys_menu` VALUES ('238', 'accident_update', 'accident', '[0],[1],[accident],', '更新', null, '/accident/update', '3', '3', '0', null, '1', null);
INSERT INTO `sys_menu` VALUES ('239', 'estimate', '0', '[0],', '综合安全评估子系统', 'fa-rocket', '#', '6', '1', '1', null, '1', null);
INSERT INTO `sys_menu` VALUES ('240', 'indication', 'estimate', '[0],[estimate],', '评价指标体系维护', '', '/indication', '1', '2', '1', null, '1', null);
INSERT INTO `sys_menu` VALUES ('241', 'indication_add', 'indication', '[0],[estimate],[indication],', '增加', null, '/indication/add', '1', '3', '0', null, '1', null);
INSERT INTO `sys_menu` VALUES ('242', 'indication_delete', 'indication', '[0],[estimate],[indication],', '删除', null, '/indication/delete', '2', '3', '0', null, '1', null);
INSERT INTO `sys_menu` VALUES ('243', 'indication_update', 'indication', '[0],[estimate],[indication],', '更新', null, '/indication/update', '3', '3', '0', null, '1', null);
INSERT INTO `sys_menu` VALUES ('244', 'resourceDataType', 'estimate', '[0],[estimate],', '指标源数据类型', '', '/resourceDataType', '3', '2', '1', null, '1', null);
INSERT INTO `sys_menu` VALUES ('245', 'resourceDataType_add', 'resourceDataType', '[0],[estimate],[resourceDataType],', '增加', null, '/resourceDataType/add', '1', '3', '0', null, '1', null);
INSERT INTO `sys_menu` VALUES ('246', 'resourceDataType_delete', 'resourceDataType', '[0],[estimate],[resourceDataType],', '删除', null, '/resourceDataType/delete', '2', '3', '0', null, '1', null);
INSERT INTO `sys_menu` VALUES ('247', 'resourceDataType_update', 'resourceDataType', '[0],[estimate],[resourceDataType],', '更新', null, '/resourceDataType/update', '3', '3', '0', null, '1', null);
INSERT INTO `sys_menu` VALUES ('248', 'resourceData', 'estimate', '[0],[estimate],', '指标源数据', '', '/resourceData', '4', '2', '1', null, '1', null);
INSERT INTO `sys_menu` VALUES ('249', 'resourceData_add', 'resourceData', '[0],[estimate],[resourceData],', '增加', null, '/resourceData/add', '1', '3', '0', null, '1', null);
INSERT INTO `sys_menu` VALUES ('250', 'resourceData_delete', 'resourceData', '[0],[estimate],[resourceData],', '删除', null, '/resourceData/delete', '2', '3', '0', null, '1', null);
INSERT INTO `sys_menu` VALUES ('251', 'resourceData_update', 'resourceData', '[0],[estimate],[resourceData],', '更新', null, '/resourceData/update', '3', '3', '0', null, '1', null);
INSERT INTO `sys_menu` VALUES ('252', 'counterplan_approve', 'counterplan', '[0],[1],[counterplan],', '审核通过', '', '/counterplan/approve', '4', '3', '0', null, '1', null);
INSERT INTO `sys_menu` VALUES ('253', 'counterplan_unapprove', 'counterplan', '[0],[1],[counterplan],', '预案审核未通过', '', '/counterplan/unapprove', '5', '3', '0', null, '1', null);
INSERT INTO `sys_menu` VALUES ('254', 'accident_accidentAdd', '0', '[0],', '应急处置', 'fa-rocket', '/accident/accident_add', '30', '1', '1', null, '1', null);
INSERT INTO `sys_menu` VALUES ('255', 'publicBusHub', 'stationmanager', '[0],[stationmanager],', '公交枢纽站信息', '', '/publicBusHub', '4', '2', '1', null, '1', null);
INSERT INTO `sys_menu` VALUES ('257', 'publicBusHub_add', 'publicBusHub', '[0],[stationmanager],[publicBusHub],', '增加', null, '/publicBusHub/add', '1', '3', '0', null, '1', null);
INSERT INTO `sys_menu` VALUES ('258', 'publicBusHub_delete', 'publicBusHub', '[0],[stationmanager],[publicBusHub],', '删除', null, '/publicBusHub/delete', '2', '3', '0', null, '1', null);
INSERT INTO `sys_menu` VALUES ('259', 'publicBusHub_update', 'publicBusHub', '[0],[stationmanager],[publicBusHub],', '更新', null, '/publicBusHub/update', '3', '3', '0', null, '1', null);
INSERT INTO `sys_menu` VALUES ('263', 'publicBusHub/line', '0', '[0],', '公交应急联动接驳路线', 'fa-rocket', '/publicBusHub/line', '40', '1', '1', null, '1', null);
INSERT INTO `sys_menu` VALUES ('264', 'assessment', 'estimate', '[0],[estimate],', '运营安全状态评估', '', '/assessment', '5', '2', '1', null, '1', null);
INSERT INTO `sys_menu` VALUES ('266', 'persistence', '0', '[0],', '事故致因与风险预测系统', 'fa-rocket', '#', '8', '1', '1', null, '1', null);
INSERT INTO `sys_menu` VALUES ('267', 'riskFactor', 'persistence', '[0],[persistence],', '事故信息录入', '', '/riskFactor', '1', '2', '1', null, '1', null);
INSERT INTO `sys_menu` VALUES ('268', 'riskFactor_add', 'riskFactor', '[0],[persistence],[riskFactor],', '增加', null, '/riskFactor/add', '1', '3', '0', null, '1', null);
INSERT INTO `sys_menu` VALUES ('269', 'riskFactor_delete', 'riskFactor', '[0],[persistence],[riskFactor],', '删除', null, '/riskFactor/delete', '2', '3', '0', null, '1', null);
INSERT INTO `sys_menu` VALUES ('270', 'riskFactor_update', 'riskFactor', '[0],[persistence],[riskFactor],', '更新', null, '/riskFactor/update', '3', '3', '0', null, '1', null);
INSERT INTO `sys_menu` VALUES ('271', 'riskFactor_fishBone', 'persistence', '[0],[persistence],', '事故因果网络图', '', '/riskFactor/riskFactor_fishBone', '0', '2', '1', null, '1', null);
INSERT INTO `sys_menu` VALUES ('272', '/resource', 'resource', '[0],[basedata],[resource],', '下载模板', '', '/resource/exportXlsByM', '6', '3', '0', null, '1', null);
INSERT INTO `sys_menu` VALUES ('273', '/mportExcel', 'resource', '[0],[basedata],[resource],', '上传', '', '/resource/importExcel', '9', '3', '0', null, '1', null);
INSERT INTO `sys_menu` VALUES ('274', '/detail', 'counterplan_template', '[0],[1],[counterplan_template],', '查看详情', '', '/counterplanTemplate/detail', '9', '3', '0', null, '1', null);
INSERT INTO `sys_menu` VALUES ('275', '/compile', 'counterplan_template', '[0],[1],[counterplan_template],', '编制预案', '', '/counterplanTemplate/compile', '7', '3', '0', null, '1', null);
INSERT INTO `sys_menu` VALUES ('276', '/counterplan/detail', 'counterplan', '[0],[1],[counterplan],', '预览内容', '', '/counterplan/detail', '7', '3', '0', null, '1', null);
INSERT INTO `sys_menu` VALUES ('277', '/accident/dispose_report/', 'accident', '[0],[1],[accident],', '应急处置报告', '', '/accident/dispose_report', '5', '3', '0', null, '1', null);
INSERT INTO `sys_menu` VALUES ('278', '/resourceData/delete/all', 'resourceData', '[0],[estimate],[resourceData],', '清空源数据', '', '/resourceData/delete/all', '4', '3', '0', null, '1', null);
INSERT INTO `sys_menu` VALUES ('279', '/resourceData/exportXlsByT', 'resourceData', '[0],[estimate],[resourceData],', '下载车站模板', '', '/resourceData/exportXlsByT', '5', '3', '0', null, '1', null);
INSERT INTO `sys_menu` VALUES ('280', '/exportXlsByT', 'resourceData', '[0],[estimate],[resourceData],', '下载线路模板', '', '/resourceData/exportXlsByT', '6', '3', '0', null, '1', null);
INSERT INTO `sys_menu` VALUES ('281', '/resourceData', 'resourceData', '[0],[estimate],[resourceData],', '下载区域中心模板', '', '/resourceData/exportXlsByT', '7', '3', '0', null, '1', null);
INSERT INTO `sys_menu` VALUES ('282', '/resource/T', 'resourceData', '[0],[estimate],[resourceData],', '下载路网模板', '', '/resourceData/exportXlsByT', '8', '3', '0', null, '1', null);
INSERT INTO `sys_menu` VALUES ('283', '/resourceData/importExcel', 'resourceData', '[0],[estimate],[resourceData],', '上传', '', '/resourceData/importExcel', '9', '3', '0', null, '1', null);
INSERT INTO `sys_menu` VALUES ('284', 'predictionData', 'persistence', '[0],[persistence],', '风险预测数据管理', '', '/predictionData', '5', '2', '1', null, '1', null);
INSERT INTO `sys_menu` VALUES ('285', 'predictionData_add', 'predictionData', '[0],[persistence],[predictionData],', '增加', null, '/predictionData/add', '1', '3', '0', null, '1', null);
INSERT INTO `sys_menu` VALUES ('286', 'predictionData_delete', 'predictionData', '[0],[persistence],[predictionData],', '删除', null, '/predictionData/delete', '2', '3', '0', null, '1', null);
INSERT INTO `sys_menu` VALUES ('287', 'predictionData_update', 'predictionData', '[0],[persistence],[predictionData],', '更新', null, '/predictionData/update', '3', '3', '0', null, '1', null);
INSERT INTO `sys_menu` VALUES ('288', 'riskFactor_chart', 'persistence', '[0],[persistence],', '事故致因概率预测', '', '/riskFactor/riskFactor_chart', '3', '2', '1', null, '1', null);
INSERT INTO `sys_menu` VALUES ('290', 'prediction_chart', 'persistence', '[0],[persistence],', '风险预测报表', '', '/prediction/prediction_chart', '7', '2', '1', null, '1', null);
INSERT INTO `sys_menu` VALUES ('291', 'assessment_warning', 'estimate', '[0],[estimate],', '运营安全状态预警', '', '/assessment/warning', '8', '2', '1', null, '1', null);
INSERT INTO `sys_menu` VALUES ('292', 'accidentHistory', 'persistence', '[0],[persistence],', '事故历史信息', '', '/accidentHistory', '4', '2', '1', null, '1', null);
INSERT INTO `sys_menu` VALUES ('293', 'accidentHistoryInformation_add', 'accidentHistory', '[0],[persistence],[accidentHistory],', '增加', null, '/accidentHistory/add', '1', '3', '0', null, '1', null);
INSERT INTO `sys_menu` VALUES ('294', 'accidentHistoryInformation_delete', 'accidentHistory', '[0],[persistence],[accidentHistory],', '删除', null, '/accidentHistory/delete', '2', '3', '0', null, '1', null);
INSERT INTO `sys_menu` VALUES ('295', 'accidentHistoryInformation_update', 'accidentHistory', '[0],[persistence],[accidentHistory],', '更新', null, '/accidentHistory/update', '3', '3', '0', null, '1', null);
INSERT INTO `sys_menu` VALUES ('296', 'assessment_char', 'estimate', '[0],[estimate],', '运营安全状态报表', '', '/assessment/assessment_char', '6', '2', '1', null, '1', null);
INSERT INTO `sys_menu` VALUES ('297', 'emergency', 'basedata', '[0],[basedata],', '突发事件类型', '', '/emergency', '31', '2', '1', null, '1', null);
INSERT INTO `sys_menu` VALUES ('298', 'emergency_add', 'emergency', '[0],[basedata],[emergency],', '增加', null, '/emergency/add', '1', '3', '0', null, '1', null);
INSERT INTO `sys_menu` VALUES ('299', 'emergency_delete', 'emergency', '[0],[basedata],[emergency],', '删除', null, '/emergency/delete', '2', '3', '0', null, '1', null);
INSERT INTO `sys_menu` VALUES ('300', 'emergency_update', 'emergency', '[0],[basedata],[emergency],', '更新', null, '/emergency/update', '3', '3', '0', null, '1', null);
INSERT INTO `sys_menu` VALUES ('301', 'emergencyproperty', 'basedata', '[0],[basedata],', '突发事件属性', '', '/emergencyproperty', '32', '2', '1', null, '1', null);
INSERT INTO `sys_menu` VALUES ('302', 'emergencyproperty_add', 'emergencyproperty', '[0],[basedata],[emergencyproperty],', '增加', null, '/emergencyproperty/add', '1', '3', '0', null, '1', null);
INSERT INTO `sys_menu` VALUES ('303', 'emergencyproperty_delete', 'emergencyproperty', '[0],[basedata],[emergencyproperty],', '删除', null, '/emergencyproperty/delete', '2', '3', '0', null, '1', null);
INSERT INTO `sys_menu` VALUES ('304', 'emergencyproperty_update', 'emergencyproperty', '[0],[basedata],[emergencyproperty],', '更新', null, '/emergencyproperty/update', '3', '3', '0', null, '1', null);
INSERT INTO `sys_menu` VALUES ('305', 'emulate', '0', '[0],', '公交联动仿真', '', '/publicBusHub/emulate', '110', '1', '1', null, '1', null);
INSERT INTO `sys_menu` VALUES ('306', 'importExcel', 'contact', '[0],[basedata],[contact],', '上传', '', '/contact/importExcel', '4', '3', '0', null, '1', null);
INSERT INTO `sys_menu` VALUES ('307', 'exportXlsByM', 'contact', '[0],[basedata],[contact],', '下载模板', '', '/contact/exportXlsByM', '5', '3', '0', null, '1', null);
INSERT INTO `sys_menu` VALUES ('308', 'alarm_editer', '0', '[0],', '报警', 'fa-rocket', '/alarm/alarm_editer', '25', '1', '1', null, '1', null);


-- ----------------------------
-- Table structure for `sys_notice`
-- ----------------------------
DROP TABLE IF EXISTS `sys_notice`;
CREATE TABLE `sys_notice` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `title` varchar(255) DEFAULT NULL COMMENT '标题',
  `type` int(11) DEFAULT NULL COMMENT '类型',
  `content` text COMMENT '内容',
  `createtime` datetime DEFAULT NULL COMMENT '创建时间',
  `creater` int(11) DEFAULT NULL COMMENT '创建人',
  `userIds` varchar(255) DEFAULT NULL COMMENT '用户的ids',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=utf8 COMMENT='通知表';

-- ----------------------------
-- Records of sys_notice
-- ----------------------------
INSERT INTO `sys_notice` VALUES ('9', '超管', null, '<p>超管消息</p>', '2018-07-25 16:34:27', '1', ',1,');
INSERT INTO `sys_notice` VALUES ('10', '管理员', null, '<p>管理员消息</p>', '2018-07-25 16:34:47', '1', ',48,');
INSERT INTO `sys_notice` VALUES ('11', '222', null, '<p>不卡了</p>', '2018-07-25 16:35:01', '1', ',48,49,');
INSERT INTO `sys_notice` VALUES ('14', '所有', null, '<p>通用通知！</p>', '2018-07-25 17:14:59', '1', ',null,');
INSERT INTO `sys_notice` VALUES ('15', 'ceshi ', null, '<p>测试通知</p>', '2018-07-25 17:20:09', '1', ',48,49,');


-- ----------------------------
-- Table structure for sys_operation_log
-- ----------------------------
DROP TABLE IF EXISTS `sys_operation_log`;
CREATE TABLE `sys_operation_log` (
  `id`         INT(65) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `logtype`    VARCHAR(255)     DEFAULT NULL COMMENT '日志类型',
  `logname`    VARCHAR(255)     DEFAULT NULL COMMENT '日志名称',
  `userid`     INT(65)          DEFAULT NULL COMMENT '用户id',
  `classname`  VARCHAR(255)     DEFAULT NULL COMMENT '类名称',
  `method`     TEXT COMMENT '方法名称',
  `createtime` DATETIME         DEFAULT NULL COMMENT '创建时间',
  `succeed`    VARCHAR(255)     DEFAULT NULL COMMENT '是否成功',
  `message`    TEXT COMMENT '备注',
  PRIMARY KEY (`id`)
)
  ENGINE = InnoDB AUTO_INCREMENT = 550 DEFAULT CHARSET = utf8 COMMENT ='操作日志';

-- ----------------------------
-- Records of sys_operation_log
-- ----------------------------
-- ----------------------------
-- Table structure for sys_relation
-- ----------------------------
DROP TABLE IF EXISTS `sys_relation`;
DROP TABLE IF EXISTS `sys_relation`;
CREATE TABLE `sys_relation` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `menuid` bigint(11) DEFAULT NULL COMMENT '菜单id',
  `roleid` int(11) DEFAULT NULL COMMENT '角色id',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5458 DEFAULT CHARSET=utf8 COMMENT='角色和菜单关联表';

-- ----------------------------
-- Records of sys_relation
-- ----------------------------
INSERT INTO `sys_relation` VALUES
  ('3377', '105', '5'),
  ('3378', '106', '5'),
  ('3379', '107', '5'),
  ('3380', '108', '5'),
  ('3381', '109', '5'),
  ('3382', '110', '5'),
  ('3383', '111', '5'),
  ('3384', '112', '5'),
  ('3385', '113', '5'),
  ('3386', '114', '5'),
  ('3387', '115', '5'),
  ('3388', '116', '5'),
  ('3389', '117', '5'),
  ('3390', '118', '5'),
  ('3391', '119', '5'),
  ('3392', '120', '5'),
  ('3393', '121', '5'),
  ('3394', '122', '5'),
  ('3395', '150', '5'),
  ('3396', '151', '5'),
  ('3961', '105', '5'),
  ('3962', '106', '5'),
  ('3963', '107', '5'),
  ('3964', '108', '5'),
  ('3965', '109', '5'),
  ('3966', '110', '5'),
  ('3967', '111', '5'),
  ('3968', '112', '5'),
  ('3969', '113', '5'),
  ('3970', '114', '5'),
  ('3971', '115', '5'),
  ('3972', '116', '5'),
  ('3973', '117', '5'),
  ('3974', '118', '5'),
  ('3975', '131', '5'),
  ('3976', '135', '5'),
  ('3977', '136', '5'),
  ('3978', '137', '5'),
  ('3979', '152', '5'),
  ('3980', '153', '5'),
  ('3981', '154', '5'),
  ('3982', '132', '5'),
  ('3983', '138', '5'),
  ('3984', '139', '5'),
  ('3985', '140', '5'),
  ('3986', '155', '5'),
  ('3987', '156', '5'),
  ('3988', '157', '5'),
  ('3989', '133', '5'),
  ('3990', '160', '5'),
  ('3991', '161', '5'),
  ('3992', '141', '5'),
  ('3993', '142', '5'),
  ('3994', '143', '5'),
  ('3995', '144', '5'),
  ('3996', '145', '5'),
  ('3997', '171', '5'),
  ('3998', '181', '5'),
  ('3999', '182', '5'),
  ('4000', '183', '5'),
  ('4001', '184', '5'),
  ('4006', '172', '5'),
  ('4007', '173', '5'),
  ('4008', '174', '5'),
  ('4009', '175', '5'),
  ('4010', '192', '5'),
  ('4011', '176', '5'),
  ('4012', '178', '5'),
  ('4013', '190', '5'),
  ('4014', '191', '5'),
  ('4015', '193', '5'),
  ('4016', '194', '5'),
  ('4017', '195', '5'),
  ('4018', '196', '5'),
  ('4019', '202', '5'),
  ('4020', '203', '5'),
  ('4021', '204', '5'),
  ('4022', '205', '5'),
  ('4023', '206', '5'),
  ('6084', '105', '1'),
  ('6085', '106', '1'),
  ('6086', '107', '1'),
  ('6087', '108', '1'),
  ('6088', '109', '1'),
  ('6089', '110', '1'),
  ('6090', '111', '1'),
  ('6091', '112', '1'),
  ('6092', '113', '1'),
  ('6093', '165', '1'),
  ('6094', '166', '1'),
  ('6095', '167', '1'),
  ('6096', '114', '1'),
  ('6097', '115', '1'),
  ('6098', '116', '1'),
  ('6099', '117', '1'),
  ('6100', '118', '1'),
  ('6101', '162', '1'),
  ('6102', '163', '1'),
  ('6103', '164', '1'),
  ('6104', '119', '1'),
  ('6105', '120', '1'),
  ('6106', '121', '1'),
  ('6107', '122', '1'),
  ('6108', '150', '1'),
  ('6109', '151', '1'),
  ('6110', '177', '1'),
  ('6111', '128', '1'),
  ('6112', '134', '1'),
  ('6113', '158', '1'),
  ('6114', '159', '1'),
  ('6115', '130', '1'),
  ('6116', '131', '1'),
  ('6117', '135', '1'),
  ('6118', '136', '1'),
  ('6119', '137', '1'),
  ('6120', '152', '1'),
  ('6121', '153', '1'),
  ('6122', '154', '1'),
  ('6123', '132', '1'),
  ('6124', '138', '1'),
  ('6125', '139', '1'),
  ('6126', '140', '1'),
  ('6127', '155', '1'),
  ('6128', '156', '1'),
  ('6129', '157', '1'),
  ('6130', '133', '1'),
  ('6131', '160', '1'),
  ('6132', '161', '1'),
  ('6133', '141', '1'),
  ('6134', '142', '1'),
  ('6135', '143', '1'),
  ('6136', '144', '1'),
  ('6137', '145', '1'),
  ('6138', '148', '1'),
  ('6139', '171', '1'),
  ('6140', '181', '1'),
  ('6141', '182', '1'),
  ('6142', '183', '1'),
  ('6143', '184', '1'),
  ('6144', '255', '1'),
  ('6145', '257', '1'),
  ('6146', '258', '1'),
  ('6147', '259', '1'),
  ('6148', '256', '1'),
  ('6149', '260', '1'),
  ('6150', '261', '1'),
  ('6151', '262', '1'),
  ('6152', '172', '1'),
  ('6153', '173', '1'),
  ('6154', '174', '1'),
  ('6155', '175', '1'),
  ('6156', '192', '1'),
  ('6157', '272', '1'),
  ('6158', '273', '1'),
  ('6159', '176', '1'),
  ('6160', '178', '1'),
  ('6161', '190', '1'),
  ('6162', '191', '1'),
  ('6163', '193', '1'),
  ('6164', '194', '1'),
  ('6165', '195', '1'),
  ('6166', '196', '1'),
  ('6167', '207', '1'),
  ('6168', '208', '1'),
  ('6169', '209', '1'),
  ('6170', '210', '1'),
  ('6171', '212', '1'),
  ('6172', '213', '1'),
  ('6173', '214', '1'),
  ('6174', '215', '1'),
  ('6175', '216', '1'),
  ('6176', '217', '1'),
  ('6177', '218', '1'),
  ('6178', '219', '1'),
  ('6179', '202', '1'),
  ('6180', '203', '1'),
  ('6181', '204', '1'),
  ('6182', '205', '1'),
  ('6183', '206', '1'),
  ('6184', '274', '1'),
  ('6185', '275', '1'),
  ('6186', '227', '1'),
  ('6187', '228', '1'),
  ('6188', '229', '1'),
  ('6189', '230', '1'),
  ('6190', '252', '1'),
  ('6191', '253', '1'),
  ('6192', '276', '1'),
  ('6193', '231', '1'),
  ('6194', '232', '1'),
  ('6195', '233', '1'),
  ('6196', '234', '1'),
  ('6197', '235', '1'),
  ('6198', '236', '1'),
  ('6199', '237', '1'),
  ('6200', '238', '1'),
  ('6201', '277', '1'),
  ('6202', '239', '1'),
  ('6203', '240', '1'),
  ('6204', '241', '1'),
  ('6205', '242', '1'),
  ('6206', '243', '1'),
  ('6207', '244', '1'),
  ('6208', '245', '1'),
  ('6209', '246', '1'),
  ('6210', '247', '1'),
  ('6211', '248', '1'),
  ('6212', '249', '1'),
  ('6213', '250', '1'),
  ('6214', '251', '1'),
  ('6215', '278', '1'),
  ('6216', '279', '1'),
  ('6217', '280', '1'),
  ('6218', '281', '1'),
  ('6219', '282', '1'),
  ('6220', '283', '1'),
  ('6221', '264', '1'),
  ('6222', '291', '1'),
  ('6223', '296', '1'),
  ('6224', '254', '1'),
  ('6225', '263', '1'),
  ('6226', '266', '1'),
  ('6227', '267', '1'),
  ('6228', '268', '1'),
  ('6229', '269', '1'),
  ('6230', '270', '1'),
  ('6231', '271', '1'),
  ('6232', '284', '1'),
  ('6233', '285', '1'),
  ('6234', '286', '1'),
  ('6235', '287', '1'),
  ('6236', '288', '1'),
  ('6237', '290', '1'),
  ('6238', '292', '1'),
  ('6239', '293', '1'),
  ('6240', '294', '1'),
  ('6241', '295', '1');




-- ----------------------------
-- Table structure for sys_role
-- ----------------------------
DROP TABLE IF EXISTS `sys_role`;
CREATE TABLE `sys_role` (
  `id`      INT(11) NOT NULL AUTO_INCREMENT COMMENT '主键id',
  `num`     INT(11)          DEFAULT NULL COMMENT '序号',
  `pid`     INT(11)          DEFAULT NULL COMMENT '父角色id',
  `name`    VARCHAR(255)     DEFAULT NULL COMMENT '角色名称',
  `deptid`  INT(11)          DEFAULT NULL COMMENT '部门名称',
  `tips`    VARCHAR(255)     DEFAULT NULL COMMENT '提示',
  `version` INT(11)          DEFAULT NULL COMMENT '保留字段(暂时没用）',
  PRIMARY KEY (`id`)
)
  ENGINE = InnoDB
  AUTO_INCREMENT = 6
  DEFAULT CHARSET = utf8
  COMMENT ='角色表';

-- ----------------------------
-- Records of sys_role
-- ----------------------------
INSERT INTO `sys_role` VALUES ('1', '1', '0', '超级管理员', '24', 'administrator', '1');
INSERT INTO `sys_role` VALUES ('5', '2', '1', '普通管理员', '24', 'manager', NULL);

-- ----------------------------
-- Table structure for sys_user
-- ----------------------------
DROP TABLE IF EXISTS `sys_user`;
CREATE TABLE `sys_user` (
  `id`         INT(11) NOT NULL AUTO_INCREMENT COMMENT '主键id',
  `avatar`     VARCHAR(255)     DEFAULT NULL COMMENT '头像',
  `account`    VARCHAR(45)      DEFAULT NULL COMMENT '账号',
  `password`   VARCHAR(45)      DEFAULT NULL COMMENT '密码',
  `salt`       VARCHAR(45)      DEFAULT NULL COMMENT 'md5密码盐',
  `name`       VARCHAR(45)      DEFAULT NULL COMMENT '名字',
  `birthday`   DATETIME         DEFAULT NULL COMMENT '生日',
  `sex`        INT(11)          DEFAULT NULL COMMENT '性别（1：男 2：女）',
  `email`      VARCHAR(45)      DEFAULT NULL COMMENT '电子邮件',
  `phone`      VARCHAR(45)      DEFAULT NULL COMMENT '电话',
  `roleid`     VARCHAR(255)     DEFAULT NULL COMMENT '角色id',
  `deptid`     INT(11)          DEFAULT NULL COMMENT '部门id',
  `status`     INT(11)          DEFAULT NULL COMMENT '状态(1：启用  2：冻结  3：删除）',
  `createtime` DATETIME         DEFAULT NULL COMMENT '创建时间',
  `version`    INT(11)          DEFAULT NULL COMMENT '保留字段',
  PRIMARY KEY (`id`)
)
  ENGINE = InnoDB
  AUTO_INCREMENT = 47
  DEFAULT CHARSET = utf8
  COMMENT ='管理员表';

-- ----------------------------
-- Records of sys_user
-- ----------------------------
INSERT INTO `sys_user` VALUES
  ('1', 'girl.gif', 'admin', 'ecfadcde9305f8891bcfe5a1e28c253e', '8pgby', '张三', '2017-05-05 00:00:00', '2',
        'sn93@qq.com', '18200000000', '1', '27', '1', '2016-01-29 08:49:53', '25');
INSERT INTO `sys_user` VALUES
  ('44', NULL, 'test', '45abb7879f6a8268f1ef600e6038ac73', 'ssts3', 'test', '2017-05-01 00:00:00', '1', 'abc@123.com',
         '', '5', '26', '3', '2017-05-16 20:33:37', NULL);
INSERT INTO `sys_user` VALUES
  ('45', NULL, 'boss', '71887a5ad666a18f709e1d4e693d5a35', '1f7bf', '老板', '2017-12-04 00:00:00', '1', '', '', '1', '24',
   '3', '2017-12-04 22:24:02', NULL);
INSERT INTO `sys_user` VALUES
  ('46', NULL, 'manager', 'b53cac62e7175637d4beb3b16b2f7915', 'j3cs9', '经理', '2017-12-04 00:00:00', '1', '', '', '1',
   '24', '3', '2017-12-04 22:24:24', NULL);
INSERT INTO `sys_user` VALUES
  ('48', NULL, 'gzmtr', 'cc75fee85e4f706c13a933f65178ca8f', '2z3w6', '管理员', '2017-01-09 00:00:00', '1', '', '', '5',
   '24', '1', '2018-01-09 15:47:16', NULL);



-- ----------------------------
--  Table structure for `gzmtr_roadnet`
-- ----------------------------
DROP TABLE IF EXISTS `gzmtr_roadnet`;
CREATE TABLE `gzmtr_roadnet` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键ID',
  `code` bigint(14) DEFAULT NULL COMMENT '编码，唯一',
  `pid` int(11) DEFAULT NULL COMMENT '父级',
  `pids` varchar(255) DEFAULT NULL COMMENT '父级IDS',
  `name` char(50) NOT NULL DEFAULT '' COMMENT '名称',
  `short_name` char(50) NOT NULL DEFAULT '' COMMENT '简称',
  `type` int(11) NOT NULL COMMENT '类型（区域中心、线路)',
  `num` int(11) NOT NULL COMMENT '排序',
  `is_transfer` char(2) NOT NULL DEFAULT 'N' COMMENT '换乘站，Y：是，N：否',
  `is_used` int(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=268 DEFAULT CHARSET=utf8;

-- ----------------------------
--  Records of `gzmtr_roadnet`
-- ----------------------------
INSERT INTO `gzmtr_roadnet` VALUES ('19', '440100023049', '33', '0,33,', '1号线', '1', '2', '0', '', '1');
INSERT INTO `gzmtr_roadnet` VALUES ('20', '440100023308', '33', '0,33,', '2号线', '地铁2号线', '2', '1', '', '0');
INSERT INTO `gzmtr_roadnet` VALUES ('21', '440100023033', '35', '0,35,', '3号线', '3', '2', '2', '', '1');
INSERT INTO `gzmtr_roadnet` VALUES ('22', '440100023031', '35', '0,35,', '3号线(北延段)', '3(北延)', '2', '3', '', '0');
INSERT INTO `gzmtr_roadnet` VALUES ('23', '440100023035', '34', '0,34,', '4号线', '地铁4号线', '2', '4', '', '0');
INSERT INTO `gzmtr_roadnet` VALUES ('24', '440100023037', '34', '0,34,', '5号线', '5', '2', '5', '', '0');
INSERT INTO `gzmtr_roadnet` VALUES ('25', '440100023053', '34', '0,34,', '6号线', '地铁6号线', '2', '6', '', '1');
INSERT INTO `gzmtr_roadnet` VALUES ('26', '900000043742', '0', '0,', '7号线', '地铁7号线', '2', '7', '', '0');
INSERT INTO `gzmtr_roadnet` VALUES ('27', '440100023047', '33', '0,33,', '8号线', '8', '2', '8', '', '0');
INSERT INTO `gzmtr_roadnet` VALUES ('28', '900000043839', '0', '0,', '9号线', '地铁9号线', '2', '9', '', '0');
INSERT INTO `gzmtr_roadnet` VALUES ('29', '900000071578', '0', '0,', '13号线', '地铁13号线', '2', '10', '', '0');
INSERT INTO `gzmtr_roadnet` VALUES ('30', '900000074703', '0', '0,', '14号线支线(知识城线)', '地铁14号线支线(知识城线)', '2', '11', '', '0');
INSERT INTO `gzmtr_roadnet` VALUES ('31', '440100023025', '35', '0,35,', 'APM线', 'APM', '2', '12', '', '0');
INSERT INTO `gzmtr_roadnet` VALUES ('32', '440100023043', '0', '0,', '广佛线', '广佛', '2', '13', '', '0');
INSERT INTO `gzmtr_roadnet` VALUES ('33', '1', '0', '0,', '1中心', '1中心', '1', '0', '', '0');
INSERT INTO `gzmtr_roadnet` VALUES ('34', '2', '0', '0,', '2中心', '2中心', '1', '1', '', '0');
INSERT INTO `gzmtr_roadnet` VALUES ('35', '3', '0', '0,', '3中心', '3中心', '1', '2', '', '0');
INSERT INTO `gzmtr_roadnet` VALUES ('36', '440100023043015', '19', '0,33,19,', '西朗', '', '3', '15', 'N', '0');
INSERT INTO `gzmtr_roadnet` VALUES ('37', '440100023049016', '19', '0,33,19,', '坑口', '', '3', '16', 'N', '0');
INSERT INTO `gzmtr_roadnet` VALUES ('38', '440100023049015', '19', '0,33,19,', '花地湾', '', '3', '15', 'N', '0');
INSERT INTO `gzmtr_roadnet` VALUES ('39', '440100023049014', '19', '0,33,19,', '芳村', '', '3', '14', 'N', '1');
INSERT INTO `gzmtr_roadnet` VALUES ('40', '440100023049013', '19', '0,33,19,', '黄沙', '', '3', '13', 'N', '1');
INSERT INTO `gzmtr_roadnet` VALUES ('41', '440100023049012', '19', '0,33,19,', '长寿路', '', '3', '12', 'N', '0');
INSERT INTO `gzmtr_roadnet` VALUES ('42', '440100023049011', '19', '0,33,19,', '陈家祠', '', '3', '11', 'N', '0');
INSERT INTO `gzmtr_roadnet` VALUES ('43', '440100023049010', '19', '0,33,19,', '西门口', '', '3', '10', 'N', '0');
INSERT INTO `gzmtr_roadnet` VALUES ('44', '440100023049009', '19', '0,33,19,', '公园前', '', '3', '9', 'N', '0');
INSERT INTO `gzmtr_roadnet` VALUES ('45', '440100023049008', '19', '0,33,19,', '农讲所', '', '3', '8', 'N', '0');
INSERT INTO `gzmtr_roadnet` VALUES ('46', '440100023049007', '19', '0,33,19,', '烈士陵园', '', '3', '7', 'N', '0');
INSERT INTO `gzmtr_roadnet` VALUES ('47', '440100023049006', '19', '0,33,19,', '东山口', '', '3', '6', 'N', '0');
INSERT INTO `gzmtr_roadnet` VALUES ('48', '440100023037012', '19', '0,33,19,', '杨箕', '', '3', '12', 'N', '0');
INSERT INTO `gzmtr_roadnet` VALUES ('49', '440100023031015', '19', '0,33,19,', '体育西站', '', '3', '15', 'N', '0');
INSERT INTO `gzmtr_roadnet` VALUES ('50', '440100023049003', '19', '0,33,19,', '体育中心', '', '3', '3', 'N', '0');
INSERT INTO `gzmtr_roadnet` VALUES ('51', '440100023031013', '19', '0,33,19,', '广州东站', '', '3', '13', 'N', '1');
INSERT INTO `gzmtr_roadnet` VALUES ('52', '440100023308002', '20', '0,33,20,', '广州南站', '', '3', '2', 'N', '0');
INSERT INTO `gzmtr_roadnet` VALUES ('53', '440100023308003', '20', '0,33,20,', '石壁', '', '3', '3', 'N', '0');
INSERT INTO `gzmtr_roadnet` VALUES ('54', '440100023308004', '20', '0,33,20,', '会江', '', '3', '4', 'N', '0');
INSERT INTO `gzmtr_roadnet` VALUES ('55', '440100023308005', '20', '0,33,20,', '南浦', '', '3', '5', 'N', '0');
INSERT INTO `gzmtr_roadnet` VALUES ('56', '440100023308006', '20', '0,33,20,', '洛溪', '', '3', '6', 'N', '0');
INSERT INTO `gzmtr_roadnet` VALUES ('57', '440100023308007', '20', '0,33,20,', '南洲', '', '3', '7', 'N', '0');
INSERT INTO `gzmtr_roadnet` VALUES ('58', '440100023308008', '20', '0,33,20,', '东晓南', '', '3', '8', 'N', '0');
INSERT INTO `gzmtr_roadnet` VALUES ('59', '440100023308009', '20', '0,33,20,', '江泰路', '', '3', '9', 'N', '0');
INSERT INTO `gzmtr_roadnet` VALUES ('60', '440100023047005', '20', '0,33,20,', '昌岗', '', '3', '5', 'N', '0');
INSERT INTO `gzmtr_roadnet` VALUES ('61', '440100023308011', '20', '0,33,20,', '江南西', '', '3', '11', 'N', '0');
INSERT INTO `gzmtr_roadnet` VALUES ('62', '440100023308012', '20', '0,33,20,', '市二宫', '', '3', '12', 'N', '0');
INSERT INTO `gzmtr_roadnet` VALUES ('63', '440100023053014', '20', '0,33,20,', '海珠广场', '', '3', '14', 'N', '0');
INSERT INTO `gzmtr_roadnet` VALUES ('64', '440100023308015', '20', '0,33,20,', '纪念堂', '', '3', '15', 'N', '0');
INSERT INTO `gzmtr_roadnet` VALUES ('65', '440100023308016', '20', '0,33,20,', '越秀公园', '', '3', '16', 'N', '0');
INSERT INTO `gzmtr_roadnet` VALUES ('66', '440100023037007', '20', '0,33,20,', '广州火车站', '', '3', '7', 'N', '0');
INSERT INTO `gzmtr_roadnet` VALUES ('67', '440100023308018', '20', '0,33,20,', '三元里', '', '3', '18', 'N', '0');
INSERT INTO `gzmtr_roadnet` VALUES ('68', '440100023308019', '20', '0,33,20,', '飞翔公园', '', '3', '19', 'N', '0');
INSERT INTO `gzmtr_roadnet` VALUES ('69', '440100023308020', '20', '0,33,20,', '白云公园', '', '3', '20', 'N', '0');
INSERT INTO `gzmtr_roadnet` VALUES ('70', '440100023308021', '20', '0,33,20,', '白云文化广场', '', '3', '21', 'N', '0');
INSERT INTO `gzmtr_roadnet` VALUES ('71', '440100023308022', '20', '0,33,20,', '萧岗', '', '3', '22', 'N', '0');
INSERT INTO `gzmtr_roadnet` VALUES ('72', '440100023308023', '20', '0,33,20,', '江夏', '', '3', '23', 'N', '0');
INSERT INTO `gzmtr_roadnet` VALUES ('73', '440100023308024', '20', '0,33,20,', '黄边', '', '3', '24', 'N', '0');
INSERT INTO `gzmtr_roadnet` VALUES ('74', '440100023031006', '20', '0,33,20,', '嘉禾望岗', '', '3', '6', 'N', '0');
INSERT INTO `gzmtr_roadnet` VALUES ('75', '440100023033002', '21', '0,35,21,', '番禺广场', '', '3', '2', 'N', '1');
INSERT INTO `gzmtr_roadnet` VALUES ('76', '440100023033003', '21', '0,35,21,', '市桥', '', '3', '3', 'N', '0');
INSERT INTO `gzmtr_roadnet` VALUES ('77', '440100023033004', '21', '0,35,21,', '汉溪长隆', '', '3', '4', 'N', '0');
INSERT INTO `gzmtr_roadnet` VALUES ('78', '440100023033005', '21', '0,35,21,', '大石', '', '3', '5', 'N', '0');
INSERT INTO `gzmtr_roadnet` VALUES ('79', '440100023033006', '21', '0,35,21,', '厦滘', '', '3', '6', 'N', '0');
INSERT INTO `gzmtr_roadnet` VALUES ('80', '440100023033007', '21', '0,35,21,', '沥滘', '', '3', '7', 'N', '1');
INSERT INTO `gzmtr_roadnet` VALUES ('81', '440100023033008', '21', '0,35,21,', '大塘', '', '3', '8', 'N', '0');
INSERT INTO `gzmtr_roadnet` VALUES ('82', '440100023033009', '21', '0,35,21,', '客村', '', '3', '9', 'N', '0');
INSERT INTO `gzmtr_roadnet` VALUES ('83', '440100023025002', '21', '0,35,21,', '广州塔', '', '3', '2', 'N', '0');
INSERT INTO `gzmtr_roadnet` VALUES ('84', '440100023033011', '21', '0,35,21,', '珠江新城', '', '3', '11', 'N', '0');
INSERT INTO `gzmtr_roadnet` VALUES ('85', '440100023033013', '21', '0,35,21,', '石牌桥', '', '3', '13', 'N', '0');
INSERT INTO `gzmtr_roadnet` VALUES ('86', '440100023033014', '21', '0,35,21,', '岗顶', '', '3', '14', 'N', '0');
INSERT INTO `gzmtr_roadnet` VALUES ('87', '440100023033015', '21', '0,35,21,', '华师', '', '3', '15', 'N', '0');
INSERT INTO `gzmtr_roadnet` VALUES ('88', '440100023033016', '21', '0,35,21,', '五山', '', '3', '16', 'N', '0');
INSERT INTO `gzmtr_roadnet` VALUES ('89', '440100023033017', '21', '0,35,21,', '天河客运站', '', '3', '17', 'N', '0');
INSERT INTO `gzmtr_roadnet` VALUES ('90', '440100023031002', '22', '0,35,22,', '机场南', '', '3', '2', 'N', '0');
INSERT INTO `gzmtr_roadnet` VALUES ('91', '440100023031003', '22', '0,35,22,', '高增', '', '3', '3', 'N', '0');
INSERT INTO `gzmtr_roadnet` VALUES ('92', '440100023031004', '22', '0,35,22,', '人和', '', '3', '4', 'N', '0');
INSERT INTO `gzmtr_roadnet` VALUES ('93', '440100023031005', '22', '0,35,22,', '龙归', '', '3', '5', 'N', '0');
INSERT INTO `gzmtr_roadnet` VALUES ('94', '440100023031007', '22', '0,35,22,', '白云大道北', '', '3', '7', 'N', '0');
INSERT INTO `gzmtr_roadnet` VALUES ('95', '440100023031008', '22', '0,35,22,', '永泰', '', '3', '8', 'N', '0');
INSERT INTO `gzmtr_roadnet` VALUES ('96', '440100023031009', '22', '0,35,22,', '同和', '', '3', '9', 'N', '0');
INSERT INTO `gzmtr_roadnet` VALUES ('97', '440100023031010', '22', '0,35,22,', '京溪南方医院', '', '3', '10', 'N', '0');
INSERT INTO `gzmtr_roadnet` VALUES ('98', '440100023031011', '22', '0,35,22,', '梅花园', '', '3', '11', 'N', '0');
INSERT INTO `gzmtr_roadnet` VALUES ('99', '440100023031012', '22', '0,35,22,', '燕塘站', '', '3', '12', 'N', '0');
INSERT INTO `gzmtr_roadnet` VALUES ('100', '440100023025010', '22', '0,35,22,', '林和西', '', '3', '10', 'N', '0');
INSERT INTO `gzmtr_roadnet` VALUES ('101', '440100023035002', '23', '0,34,23,', '黄村', '', '3', '2', 'N', '0');
INSERT INTO `gzmtr_roadnet` VALUES ('102', '440100023035003', '23', '0,34,23,', '车陂', '', '3', '3', 'N', '0');
INSERT INTO `gzmtr_roadnet` VALUES ('103', '440100023035004', '23', '0,34,23,', '车陂南', '', '3', '4', 'N', '0');
INSERT INTO `gzmtr_roadnet` VALUES ('104', '440100023035005', '23', '0,34,23,', '万胜围', '', '3', '5', 'N', '0');
INSERT INTO `gzmtr_roadnet` VALUES ('105', '440100023035006', '23', '0,34,23,', '官洲', '', '3', '6', 'N', '0');
INSERT INTO `gzmtr_roadnet` VALUES ('106', '440100023035007', '23', '0,34,23,', '大学城北', '', '3', '7', 'N', '0');
INSERT INTO `gzmtr_roadnet` VALUES ('107', '440100023035008', '23', '0,34,23,', '大学城南', '', '3', '8', 'N', '0');
INSERT INTO `gzmtr_roadnet` VALUES ('108', '440100023035009', '23', '0,34,23,', '新造', '', '3', '9', 'N', '0');
INSERT INTO `gzmtr_roadnet` VALUES ('109', '440100023035011', '23', '0,34,23,', '石碁', '', '3', '11', 'N', '0');
INSERT INTO `gzmtr_roadnet` VALUES ('110', '440100023035012', '23', '0,34,23,', '海傍', '', '3', '12', 'N', '0');
INSERT INTO `gzmtr_roadnet` VALUES ('111', '440100023035013', '23', '0,34,23,', '低涌', '', '3', '13', 'N', '0');
INSERT INTO `gzmtr_roadnet` VALUES ('112', '440100023035014', '23', '0,34,23,', '东涌', '', '3', '14', 'N', '0');
INSERT INTO `gzmtr_roadnet` VALUES ('113', '440100023035015', '23', '0,34,23,', '庆盛', '', '3', '15', 'N', '0');
INSERT INTO `gzmtr_roadnet` VALUES ('114', '440100023035016', '23', '0,34,23,', '黄阁汽车城', '', '3', '16', 'N', '0');
INSERT INTO `gzmtr_roadnet` VALUES ('115', '440100023035017', '23', '0,34,23,', '黄阁', '', '3', '17', 'N', '0');
INSERT INTO `gzmtr_roadnet` VALUES ('116', '440100023035018', '23', '0,34,23,', '蕉门', '', '3', '18', 'N', '0');
INSERT INTO `gzmtr_roadnet` VALUES ('117', '440100023035019', '23', '0,34,23,', '金洲', '', '3', '19', 'N', '0');
INSERT INTO `gzmtr_roadnet` VALUES ('118', '440100023035020', '23', '0,34,23,', '飞沙角', '', '3', '20', 'N', '0');
INSERT INTO `gzmtr_roadnet` VALUES ('119', '440100023035021', '23', '0,34,23,', '广隆', '', '3', '21', 'N', '0');
INSERT INTO `gzmtr_roadnet` VALUES ('120', '440100023035022', '23', '0,34,23,', '大涌', '', '3', '22', 'N', '0');
INSERT INTO `gzmtr_roadnet` VALUES ('121', '440100023035023', '23', '0,34,23,', '塘坑', '', '3', '23', 'N', '0');
INSERT INTO `gzmtr_roadnet` VALUES ('122', '440100023035024', '23', '0,34,23,', '南横', '', '3', '24', 'N', '0');
INSERT INTO `gzmtr_roadnet` VALUES ('123', '440100023035025', '23', '0,34,23,', '南沙客运港', '', '3', '25', 'N', '0');
INSERT INTO `gzmtr_roadnet` VALUES ('124', '440100023037002', '24', '0,34,24,', '滘口', '', '3', '2', 'N', '0');
INSERT INTO `gzmtr_roadnet` VALUES ('125', '440100023037003', '24', '0,34,24,', '坦尾', '', '3', '3', 'N', '0');
INSERT INTO `gzmtr_roadnet` VALUES ('126', '440100023037004', '24', '0,34,24,', '中山八', '', '3', '4', 'N', '0');
INSERT INTO `gzmtr_roadnet` VALUES ('127', '440100023037005', '24', '0,34,24,', '西场', '', '3', '5', 'N', '0');
INSERT INTO `gzmtr_roadnet` VALUES ('128', '440100023037006', '24', '0,34,24,', '西村', '', '3', '6', 'N', '0');
INSERT INTO `gzmtr_roadnet` VALUES ('129', '440100023037008', '24', '0,34,24,', '小北', '', '3', '8', 'N', '0');
INSERT INTO `gzmtr_roadnet` VALUES ('130', '440100023037009', '24', '0,34,24,', '淘金', '', '3', '9', 'N', '0');
INSERT INTO `gzmtr_roadnet` VALUES ('131', '440100023037010', '24', '0,34,24,', '区庄', '', '3', '10', 'N', '0');
INSERT INTO `gzmtr_roadnet` VALUES ('132', '440100023037011', '24', '0,34,24,', '动物园', '', '3', '11', 'N', '0');
INSERT INTO `gzmtr_roadnet` VALUES ('133', '440100023037013', '24', '0,34,24,', '五羊邨', '', '3', '13', 'N', '0');
INSERT INTO `gzmtr_roadnet` VALUES ('134', '440100023037015', '24', '0,34,24,', '猎德', '', '3', '15', 'N', '0');
INSERT INTO `gzmtr_roadnet` VALUES ('135', '440100023037016', '24', '0,34,24,', '潭村', '', '3', '16', 'N', '0');
INSERT INTO `gzmtr_roadnet` VALUES ('136', '440100023037017', '24', '0,34,24,', '员村', '', '3', '17', 'N', '0');
INSERT INTO `gzmtr_roadnet` VALUES ('137', '440100023037018', '24', '0,34,24,', '科韵路', '', '3', '18', 'N', '0');
INSERT INTO `gzmtr_roadnet` VALUES ('138', '440100023037020', '24', '0,34,24,', '东圃', '', '3', '20', 'N', '0');
INSERT INTO `gzmtr_roadnet` VALUES ('139', '440100023037021', '24', '0,34,24,', '三溪', '', '3', '21', 'N', '0');
INSERT INTO `gzmtr_roadnet` VALUES ('140', '440100023037022', '24', '0,34,24,', '鱼珠', '', '3', '22', 'N', '0');
INSERT INTO `gzmtr_roadnet` VALUES ('141', '440100023037023', '24', '0,34,24,', '大沙地', '', '3', '23', 'N', '0');
INSERT INTO `gzmtr_roadnet` VALUES ('142', '440100023037024', '24', '0,34,24,', '大沙东', '', '3', '24', 'N', '0');
INSERT INTO `gzmtr_roadnet` VALUES ('143', '440100023037025', '24', '0,34,24,', '文冲', '', '3', '25', 'N', '0');
INSERT INTO `gzmtr_roadnet` VALUES ('144', '440100023053024', '25', '0,34,25,', '香雪', '', '3', '24', 'N', '0');
INSERT INTO `gzmtr_roadnet` VALUES ('145', '440100023053025', '25', '0,34,25,', '萝岗', '', '3', '25', 'N', '0');
INSERT INTO `gzmtr_roadnet` VALUES ('146', '440100023053026', '25', '0,34,25,', '苏元', '', '3', '26', 'N', '0');
INSERT INTO `gzmtr_roadnet` VALUES ('147', '440100023053027', '25', '0,34,25,', '暹岗', '', '3', '27', 'N', '0');
INSERT INTO `gzmtr_roadnet` VALUES ('148', '440100023053028', '25', '0,34,25,', '金峰', '', '3', '28', 'N', '0');
INSERT INTO `gzmtr_roadnet` VALUES ('149', '440100023053029', '25', '0,34,25,', '黄陂', '', '3', '29', 'N', '0');
INSERT INTO `gzmtr_roadnet` VALUES ('150', '440100023053030', '25', '0,34,25,', '高塘石', '', '3', '30', 'N', '0');
INSERT INTO `gzmtr_roadnet` VALUES ('151', '440100023053031', '25', '0,34,25,', '柯木塱', '', '3', '31', 'N', '0');
INSERT INTO `gzmtr_roadnet` VALUES ('152', '440100023053032', '25', '0,34,25,', '龙洞', '', '3', '32', 'N', '0');
INSERT INTO `gzmtr_roadnet` VALUES ('153', '440100023053033', '25', '0,34,25,', '植物园', '', '3', '33', 'N', '0');
INSERT INTO `gzmtr_roadnet` VALUES ('154', '440100023053002', '25', '0,34,25,', '长湴', '', '3', '2', 'N', '0');
INSERT INTO `gzmtr_roadnet` VALUES ('155', '440100023053005', '25', '0,34,25,', '天平架', '', '3', '5', 'N', '0');
INSERT INTO `gzmtr_roadnet` VALUES ('156', '440100023053007', '25', '0,34,25,', '沙河顶', '', '3', '7', 'N', '0');
INSERT INTO `gzmtr_roadnet` VALUES ('157', '440100023053008', '25', '0,34,25,', '黄花岗', '', '3', '8', 'N', '0');
INSERT INTO `gzmtr_roadnet` VALUES ('158', '440100023053011', '25', '0,34,25,', '东湖', '', '3', '11', 'N', '0');
INSERT INTO `gzmtr_roadnet` VALUES ('159', '440100023053012', '25', '0,34,25,', '团一大广场', '', '3', '12', 'N', '0');
INSERT INTO `gzmtr_roadnet` VALUES ('160', '440100023053013', '25', '0,34,25,', '北京路', '', '3', '13', 'N', '0');
INSERT INTO `gzmtr_roadnet` VALUES ('161', '440100023053015', '25', '0,34,25,', '一德路', '', '3', '15', 'N', '0');
INSERT INTO `gzmtr_roadnet` VALUES ('162', '440100023053016', '25', '0,34,25,', '文化公园', '', '3', '16', 'N', '0');
INSERT INTO `gzmtr_roadnet` VALUES ('163', '440100023053018', '25', '0,34,25,', '如意坊', '', '3', '18', 'N', '0');
INSERT INTO `gzmtr_roadnet` VALUES ('164', '440100023053020', '25', '0,34,25,', '河沙', '', '3', '20', 'N', '1');
INSERT INTO `gzmtr_roadnet` VALUES ('165', '440100023053021', '25', '0,34,25,', '沙贝', '', '3', '21', 'N', '0');
INSERT INTO `gzmtr_roadnet` VALUES ('166', '440100023053022', '25', '0,34,25,', '横沙', '', '3', '22', 'N', '0');
INSERT INTO `gzmtr_roadnet` VALUES ('167', '440100023053023', '25', '0,34,25,', '浔峰岗', '', '3', '23', 'N', '1');
INSERT INTO `gzmtr_roadnet` VALUES ('168', '900000043742002', '26', '0,26,', '板桥', '', '3', '2', 'N', '0');
INSERT INTO `gzmtr_roadnet` VALUES ('169', '900000043742003', '26', '0,26,', '员岗', '', '3', '3', 'N', '0');
INSERT INTO `gzmtr_roadnet` VALUES ('170', '900000043742004', '26', '0,26,', '南村万博', '', '3', '4', 'N', '0');
INSERT INTO `gzmtr_roadnet` VALUES ('171', '900000043742006', '26', '0,26,', '钟村', '', '3', '6', 'N', '0');
INSERT INTO `gzmtr_roadnet` VALUES ('172', '900000043742007', '26', '0,26,', '谢村', '', '3', '7', 'N', '0');
INSERT INTO `gzmtr_roadnet` VALUES ('173', '440100023047002', '27', '0,33,27,', '凤凰新村', '', '3', '2', 'N', '0');
INSERT INTO `gzmtr_roadnet` VALUES ('174', '440100023043019', '27', '0,33,27,', '沙园', '', '3', '19', 'N', '0');
INSERT INTO `gzmtr_roadnet` VALUES ('175', '440100023047004', '27', '0,33,27,', '宝岗大道', '', '3', '4', 'N', '0');
INSERT INTO `gzmtr_roadnet` VALUES ('176', '440100023047006', '27', '0,33,27,', '晓港', '', '3', '6', 'N', '0');
INSERT INTO `gzmtr_roadnet` VALUES ('177', '440100023047007', '27', '0,33,27,', '中大', '', '3', '7', 'N', '0');
INSERT INTO `gzmtr_roadnet` VALUES ('178', '440100023047008', '27', '0,33,27,', '鹭江', '', '3', '8', 'N', '0');
INSERT INTO `gzmtr_roadnet` VALUES ('179', '440100023047010', '27', '0,33,27,', '赤岗', '', '3', '10', 'N', '0');
INSERT INTO `gzmtr_roadnet` VALUES ('180', '440100023047011', '27', '0,33,27,', '磨碟沙', '', '3', '11', 'N', '0');
INSERT INTO `gzmtr_roadnet` VALUES ('181', '440100023047012', '27', '0,33,27,', '新港东', '', '3', '12', 'N', '0');
INSERT INTO `gzmtr_roadnet` VALUES ('182', '440100023047013', '27', '0,33,27,', '琶洲', '', '3', '13', 'N', '0');
INSERT INTO `gzmtr_roadnet` VALUES ('183', '900000043839009', '28', '0,28,', '清布', '', '3', '9', 'N', '0');
INSERT INTO `gzmtr_roadnet` VALUES ('184', '900000043839008', '28', '0,28,', '莲塘', '', '3', '8', 'N', '0');
INSERT INTO `gzmtr_roadnet` VALUES ('185', '900000043839007', '28', '0,28,', '马鞍山公园', '', '3', '7', 'N', '0');
INSERT INTO `gzmtr_roadnet` VALUES ('186', '900000043839006', '28', '0,28,', '花都广场', '', '3', '6', 'N', '0');
INSERT INTO `gzmtr_roadnet` VALUES ('187', '900000043839005', '28', '0,28,', '花果山公园', '', '3', '5', 'N', '0');
INSERT INTO `gzmtr_roadnet` VALUES ('188', '900000043839004', '28', '0,28,', '花城路', '', '3', '4', 'N', '0');
INSERT INTO `gzmtr_roadnet` VALUES ('189', '900000043839003', '28', '0,28,', '广州北站', '', '3', '3', 'N', '0');
INSERT INTO `gzmtr_roadnet` VALUES ('190', '900000043839002', '28', '0,28,', '花都汽车城', '', '3', '2', 'N', '0');
INSERT INTO `gzmtr_roadnet` VALUES ('191', '900000043839001', '28', '0,28,', '飞鹅岭', '', '3', '1', 'N', '0');
INSERT INTO `gzmtr_roadnet` VALUES ('192', '900000071578002', '29', '0,29,', '裕丰围', '', '3', '2', 'N', '0');
INSERT INTO `gzmtr_roadnet` VALUES ('193', '900000071578003', '29', '0,29,', '双岗', '', '3', '3', 'N', '0');
INSERT INTO `gzmtr_roadnet` VALUES ('194', '900000071578004', '29', '0,29,', '南海神庙', '', '3', '4', 'N', '0');
INSERT INTO `gzmtr_roadnet` VALUES ('195', '900000071578005', '29', '0,29,', '夏园', '', '3', '5', 'N', '0');
INSERT INTO `gzmtr_roadnet` VALUES ('196', '900000071578006', '29', '0,29,', '南岗', '', '3', '6', 'N', '0');
INSERT INTO `gzmtr_roadnet` VALUES ('197', '900000071578007', '29', '0,29,', '沙村', '', '3', '7', 'N', '0');
INSERT INTO `gzmtr_roadnet` VALUES ('198', '900000071578008', '29', '0,29,', '白江', '', '3', '8', 'N', '0');
INSERT INTO `gzmtr_roadnet` VALUES ('199', '900000071578009', '29', '0,29,', '新塘', '', '3', '9', 'N', '0');
INSERT INTO `gzmtr_roadnet` VALUES ('200', '900000071578010', '29', '0,29,', '官湖', '', '3', '10', 'N', '0');
INSERT INTO `gzmtr_roadnet` VALUES ('201', '900000071578011', '29', '0,29,', '新沙', '', '3', '11', 'N', '0');
INSERT INTO `gzmtr_roadnet` VALUES ('202', '900000074701008', '30', '0,30,', '新和', '', '3', '8', 'N', '0');
INSERT INTO `gzmtr_roadnet` VALUES ('203', '900000074703002', '30', '0,30,', '红卫', '', '3', '2', 'N', '0');
INSERT INTO `gzmtr_roadnet` VALUES ('204', '900000074703003', '30', '0,30,', '新南', '', '3', '3', 'N', '0');
INSERT INTO `gzmtr_roadnet` VALUES ('205', '900000074703004', '30', '0,30,', '枫下', '', '3', '4', 'N', '0');
INSERT INTO `gzmtr_roadnet` VALUES ('206', '900000074703005', '30', '0,30,', '知识城', '', '3', '5', 'N', '0');
INSERT INTO `gzmtr_roadnet` VALUES ('207', '900000074703006', '30', '0,30,', '何棠下', '', '3', '6', 'N', '0');
INSERT INTO `gzmtr_roadnet` VALUES ('208', '900000074703007', '30', '0,30,', '旺村', '', '3', '7', 'N', '0');
INSERT INTO `gzmtr_roadnet` VALUES ('209', '900000074703008', '30', '0,30,', '汤村', '', '3', '8', 'N', '0');
INSERT INTO `gzmtr_roadnet` VALUES ('210', '900000074703009', '30', '0,30,', '镇龙北', '', '3', '9', 'N', '0');
INSERT INTO `gzmtr_roadnet` VALUES ('211', '900000046003007', '30', '0,30,', '镇龙', '', '3', '7', 'N', '0');
INSERT INTO `gzmtr_roadnet` VALUES ('212', '440100023025003', '31', '0,35,31,', '海心沙', '', '3', '3', 'N', '0');
INSERT INTO `gzmtr_roadnet` VALUES ('213', '440100023025004', '31', '0,35,31,', '大剧院', '', '3', '4', 'N', '0');
INSERT INTO `gzmtr_roadnet` VALUES ('214', '440100023025005', '31', '0,35,31,', '花城大道', '', '3', '5', 'N', '0');
INSERT INTO `gzmtr_roadnet` VALUES ('215', '440100023025006', '31', '0,35,31,', '妇儿中心', '', '3', '6', 'N', '0');
INSERT INTO `gzmtr_roadnet` VALUES ('216', '440100023025007', '31', '0,35,31,', '黄埔大道', '', '3', '7', 'N', '0');
INSERT INTO `gzmtr_roadnet` VALUES ('217', '440100023025008', '31', '0,35,31,', '天河南', '', '3', '8', 'N', '0');
INSERT INTO `gzmtr_roadnet` VALUES ('218', '440100023025009', '31', '0,35,31,', '体育中心南', '', '3', '9', 'N', '0');
INSERT INTO `gzmtr_roadnet` VALUES ('219', '440100023043020', '32', '0,32,', '燕岗', '', '3', '20', 'N', '0');
INSERT INTO `gzmtr_roadnet` VALUES ('220', '440100023043018', '32', '0,32,', '沙涌', '', '3', '18', 'N', '0');
INSERT INTO `gzmtr_roadnet` VALUES ('221', '440100023043017', '32', '0,32,', '鹤洞', '', '3', '17', 'N', '0');
INSERT INTO `gzmtr_roadnet` VALUES ('222', '440100023043014', '32', '0,32,', '菊树', '', '3', '14', 'N', '0');
INSERT INTO `gzmtr_roadnet` VALUES ('223', '440100023043013', '32', '0,32,', '龙溪', '', '3', '13', 'N', '0');
INSERT INTO `gzmtr_roadnet` VALUES ('224', '440100023043012', '32', '0,32,', '金融高新区', '', '3', '12', 'N', '0');
INSERT INTO `gzmtr_roadnet` VALUES ('225', '440100023043011', '32', '0,32,', '千灯湖', '', '3', '11', 'N', '0');
INSERT INTO `gzmtr_roadnet` VALUES ('226', '440100023043016', '32', '0,32,', '礌岗', '', '3', '16', 'N', '0');
INSERT INTO `gzmtr_roadnet` VALUES ('227', '440100023043009', '32', '0,32,', '南桂路', '', '3', '9', 'N', '0');
INSERT INTO `gzmtr_roadnet` VALUES ('228', '440100023043008', '32', '0,32,', '桂城', '', '3', '8', 'N', '0');
INSERT INTO `gzmtr_roadnet` VALUES ('229', '440100023043007', '32', '0,32,', '朝安', '', '3', '7', 'N', '0');
INSERT INTO `gzmtr_roadnet` VALUES ('230', '440100023043006', '32', '0,32,', '普君北路', '', '3', '6', 'N', '0');
INSERT INTO `gzmtr_roadnet` VALUES ('231', '440100023043005', '32', '0,32,', '祖庙', '', '3', '5', 'N', '0');
INSERT INTO `gzmtr_roadnet` VALUES ('232', '440100023043004', '32', '0,32,', '同济路', '', '3', '4', 'N', '0');
INSERT INTO `gzmtr_roadnet` VALUES ('233', '440100023043003', '32', '0,32,', '季华园', '', '3', '3', 'N', '0');
INSERT INTO `gzmtr_roadnet` VALUES ('234', '440100023043002', '32', '0,32,', '魁奇路', '', '3', '2', 'N', '0');
INSERT INTO `gzmtr_roadnet` VALUES ('235', '440100023043024', '32', '0,32,', '澜石', '', '3', '24', 'N', '0');
INSERT INTO `gzmtr_roadnet` VALUES ('236', '440100023043023', '32', '0,32,', '世纪莲', '', '3', '23', 'N', '0');
INSERT INTO `gzmtr_roadnet` VALUES ('237', '440100023043022', '32', '0,32,', '东平', '', '3', '22', 'N', '0');
INSERT INTO `gzmtr_roadnet` VALUES ('238', '440100023043021', '32', '0,32,', '新城东', '', '3', '21', 'N', '0');
INSERT INTO `gzmtr_roadnet` VALUES ('239', '440100023035008', '26', '0,26,', '大学城南', '', '3', '1', 'Y', '0');
INSERT INTO `gzmtr_roadnet` VALUES ('240', '440100023308003', '26', '0,26,', '石壁', '', '3', '8', 'Y', '0');
INSERT INTO `gzmtr_roadnet` VALUES ('241', '440100023308002', '26', '0,26,', '广州南站', '', '3', '9', 'Y', '0');
INSERT INTO `gzmtr_roadnet` VALUES ('242', '440100023033004', '26', '0,26,', '汉溪长隆', '', '3', '5', 'Y', '0');
INSERT INTO `gzmtr_roadnet` VALUES ('243', '440100023031003', '28', '0,28,', '高增', '', '3', '10', 'Y', '0');
INSERT INTO `gzmtr_roadnet` VALUES ('244', '440100023037022', '29', '0,29,', '鱼珠', '', '3', '1', 'Y', '0');
INSERT INTO `gzmtr_roadnet` VALUES ('245', '440100023043015', '32', '0,32,', '西朗', '', '3', '15', 'Y', '0');
INSERT INTO `gzmtr_roadnet` VALUES ('246', '440100023043019', '32', '0,32,', '沙园', '', '3', '19', 'Y', '0');
INSERT INTO `gzmtr_roadnet` VALUES ('247', '440100023049009', '20', '0,33,20,', '公园前', '', '3', '16', 'Y', '0');
INSERT INTO `gzmtr_roadnet` VALUES ('248', '440100023047005', '27', '0,33,27,', '昌岗', '', '3', '5', 'Y', '0');
INSERT INTO `gzmtr_roadnet` VALUES ('249', '440100023033009', '27', '0,33,27,', '客村', '', '3', '9', 'Y', '0');
INSERT INTO `gzmtr_roadnet` VALUES ('250', '440100023035005', '27', '0,33,27,', '万胜围', '', '3', '14', 'Y', '0');
INSERT INTO `gzmtr_roadnet` VALUES ('251', '440100023037007', '24', '0,34,24,', '广州火车站', '', '3', '7', 'Y', '0');
INSERT INTO `gzmtr_roadnet` VALUES ('252', '440100023037012', '24', '0,34,24,', '杨箕', '', '3', '12', 'Y', '0');
INSERT INTO `gzmtr_roadnet` VALUES ('253', '440100023033011', '24', '0,34,24,', '珠江新城	', '', '3', '11', 'Y', '0');
INSERT INTO `gzmtr_roadnet` VALUES ('254', '440100023035004', '24', '0,34,24,', '车陂南', '', '3', '4', 'Y', '0');
INSERT INTO `gzmtr_roadnet` VALUES ('255', '440100023037003', '25', '0,34,25,', '坦尾', '', '3', '3', 'Y', '0');
INSERT INTO `gzmtr_roadnet` VALUES ('256', '440100023049013', '25', '0,34,25,', '黄沙站', '', '3', '13', 'Y', '0');
INSERT INTO `gzmtr_roadnet` VALUES ('257', '440100023053014', '25', '0,34,25,', '海珠广场', '', '3', '14', 'Y', '0');
INSERT INTO `gzmtr_roadnet` VALUES ('258', '440100023049006', '25', '0,34,25,', '东山口', '', '3', '6', 'Y', '0');
INSERT INTO `gzmtr_roadnet` VALUES ('259', '440100023037010', '25', '0,34,25,', '区庄', '', '3', '10', 'Y', '0');
INSERT INTO `gzmtr_roadnet` VALUES ('260', '440100023031012', '25', '0,34,25,', '燕塘', '', '3', '12', 'Y', '1');
INSERT INTO `gzmtr_roadnet` VALUES ('261', '440100023033017', '25', '0,34,25,', '天河客运站', '', '3', '17', 'Y', '0');
INSERT INTO `gzmtr_roadnet` VALUES ('262', '440100023031015', '21', '0,35,21,', '体育西路', '', '3', '15', 'Y', '1');
INSERT INTO `gzmtr_roadnet` VALUES ('263', '440100023031006', '22', '0,35,22,', '嘉禾望岗', '', '3', '6', 'Y', '0');
INSERT INTO `gzmtr_roadnet` VALUES ('264', '440100023031013', '22', '0,35,22,', '广州东', '', '3', '13', 'Y', '0');
INSERT INTO `gzmtr_roadnet` VALUES ('265', '440100023031015', '22', '0,35,22,', '体育西站', '', '3', '15', 'Y', '0');
INSERT INTO `gzmtr_roadnet` VALUES ('266', '440100023025010', '31', '0,35,31,', '林和西', '', '3', '10', 'Y', '0');
INSERT INTO `gzmtr_roadnet` VALUES ('267', '440100023025002', '31', '0,35,31,', '广州塔', '', '3', '1', 'Y', '0');




-- ----------------------------
-- Table structure for gzmtr_law
-- ----------------------------
DROP TABLE IF EXISTS `gzmtr_law`;
CREATE TABLE `gzmtr_law` (
  `id`   INT(11) NOT NULL                AUTO_INCREMENT COMMENT '主键ID',
  `name` VARCHAR(255) CHARACTER SET utf8 DEFAULT NULL COMMENT '名字',
  PRIMARY KEY (`id`)
)
  ENGINE = InnoDB
  AUTO_INCREMENT = 19
  DEFAULT CHARSET = utf8;

-- ----------------------------
-- Records of gzmtr_law
-- ----------------------------
INSERT INTO `gzmtr_law` VALUES ('19', '中国消防法');
INSERT INTO `gzmtr_law` VALUES ('23', '中华人民共和国安全生产法');
INSERT INTO `gzmtr_law` VALUES ('24', '中华人民共和国突发事件应对法');
INSERT INTO `gzmtr_law` VALUES ('25', '国家城市轨道交通运营突发事件应急预案');
INSERT INTO `gzmtr_law` VALUES ('26', '运营事业总部突发事件综合应急预案');
INSERT INTO `gzmtr_law` VALUES ('27', '运营事业总部消防安全管理办法');



-- ----------------------------
-- Table structure for gzmtr_accident
-- ----------------------------
DROP TABLE IF EXISTS `gzmtr_accident_process`;
CREATE TABLE `gzmtr_accident_process` (
  `id` int(50) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `type_id` varchar(25) DEFAULT NULL COMMENT '分组',
  `main` varchar(50) DEFAULT NULL COMMENT '主体',
  `mproperty` varchar(300) DEFAULT NULL COMMENT '主体属性',
  `mmotion` varchar(300) DEFAULT NULL COMMENT '主体动作',
  `object` varchar(300) DEFAULT NULL COMMENT '客体',
  `oproperty` varchar(300) DEFAULT NULL COMMENT '客体属性',
  `omotion` varchar(300) DEFAULT NULL COMMENT '客体动作',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=339 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of gzmtr_accident_process
-- ----------------------------
INSERT INTO `gzmtr_accident_process` VALUES ('1', '69', '7', '', '立即赶到', '', '现场', '');
INSERT INTO `gzmtr_accident_process` VALUES ('2', '69', '7', '', '协助灭火', '', '', '');
INSERT INTO `gzmtr_accident_process` VALUES ('3', '69', '7', '', '确认', '不可控制', '火灾', '');
INSERT INTO `gzmtr_accident_process` VALUES ('4', '69', '7', '', '立即关停', '非疏散方向', '扶梯', '');
INSERT INTO `gzmtr_accident_process` VALUES ('5', '69', '7', '', '组织', '站台', '乘客', '向站外疏散');
INSERT INTO `gzmtr_accident_process` VALUES ('6', '69', '7', '', '确认', '站台', '乘客', '疏散完毕');
INSERT INTO `gzmtr_accident_process` VALUES ('7', '69', '7', '', '报告', '', '车控室', '');
INSERT INTO `gzmtr_accident_process` VALUES ('8', '69', '7', '', '听从', '', '值班站长', '安排');
INSERT INTO `gzmtr_accident_process` VALUES ('9', '69', '9', '', '接收', '', '火警信息', '');
INSERT INTO `gzmtr_accident_process` VALUES ('10', '69', '9', '', '报告', '', '行调', '');
INSERT INTO `gzmtr_accident_process` VALUES ('11', '69', '9', '', '通知', '', '值班站长', '');
INSERT INTO `gzmtr_accident_process` VALUES ('12', '69', '9', '', '到报警点确认', '', '火灾', '');
INSERT INTO `gzmtr_accident_process` VALUES ('13', '69', '9', '', '确认', '', '火灾', '');
INSERT INTO `gzmtr_accident_process` VALUES ('14', '69', '9', '', '通知', '巡视岗、保洁等', '驻站人员', '协助灭火');
INSERT INTO `gzmtr_accident_process` VALUES ('15', '69', '9', '', '报告', '', '环调、119、地铁公安和120', '');
INSERT INTO `gzmtr_accident_process` VALUES ('16', '69', '9', '', '', '', '行调', '');
INSERT INTO `gzmtr_accident_process` VALUES ('17', '69', '9', '', '广播通知', '', '所有岗位', '');
INSERT INTO `gzmtr_accident_process` VALUES ('18', '69', '26', '', '执行', '设备区火灾', '应急疏散处理程序', '');
INSERT INTO `gzmtr_accident_process` VALUES ('19', '69', '9', '', '广播引导', '', '乘客', '疏散');
INSERT INTO `gzmtr_accident_process` VALUES ('20', '69', '9', '', '按压', '', 'AFC紧急按钮', '');
INSERT INTO `gzmtr_accident_process` VALUES ('21', '69', '9', '', '设置', '紧急模式', '闸机', '');
INSERT INTO `gzmtr_accident_process` VALUES ('22', '69', '9', '', '报告', '', '行调', '');
INSERT INTO `gzmtr_accident_process` VALUES ('23', '69', '9', '', '与', '', '行调、值班站长', '保持联系');
INSERT INTO `gzmtr_accident_process` VALUES ('24', '69', '9', '', '确认', '', '保洁人员', '');
INSERT INTO `gzmtr_accident_process` VALUES ('25', '69', '12', '', '到', '', '紧急出口', '');
INSERT INTO `gzmtr_accident_process` VALUES ('26', '69', '12', '', '接', '', '消防人员', '');
INSERT INTO `gzmtr_accident_process` VALUES ('27', '69', '9', '', '撤退', '', '', '');
INSERT INTO `gzmtr_accident_process` VALUES ('28', '69', '9', '', '随身携带', '', '无线电台', '联系行调');
INSERT INTO `gzmtr_accident_process` VALUES ('29', '69', '9', '', '设置', '', '相关设备区通道门门禁', '常开状态');
INSERT INTO `gzmtr_accident_process` VALUES ('30', '69', '10', '', '接到', '', '火警通知', '');
INSERT INTO `gzmtr_accident_process` VALUES ('31', '69', '10', '', '立即携带', '相应房间', '钥匙', '');
INSERT INTO `gzmtr_accident_process` VALUES ('32', '69', '10', '', '立即携带', '', '防护用品', '');
INSERT INTO `gzmtr_accident_process` VALUES ('33', '69', '10', '', '到', '', '现场', '');
INSERT INTO `gzmtr_accident_process` VALUES ('34', '69', '10', '', '确认', '', '火灾', '');
INSERT INTO `gzmtr_accident_process` VALUES ('35', '69', '10', '', '组织扑救', '', '火灾', '');
INSERT INTO `gzmtr_accident_process` VALUES ('36', '69', '10', '', '确认', '', '火灾', '不可控制');
INSERT INTO `gzmtr_accident_process` VALUES ('37', '69', '10', '', '关闭', '火灾房间', '防火门', '');
INSERT INTO `gzmtr_accident_process` VALUES ('38', '69', '10', '', '执行', '设备区火灾', '应急疏散处理程序', '');
INSERT INTO `gzmtr_accident_process` VALUES ('39', '69', '10', '', '组织疏散', '', '乘客', '');
INSERT INTO `gzmtr_accident_process` VALUES ('40', '69', '10', '', '安排', '', '值班人员', '');
INSERT INTO `gzmtr_accident_process` VALUES ('41', '69', '34', '', '在', '', '出入口', '');
INSERT INTO `gzmtr_accident_process` VALUES ('42', '69', '34', '', '拦截', '进站', '乘客', '');
INSERT INTO `gzmtr_accident_process` VALUES ('43', '69', '21', '', '到达', '', '现场', '');
INSERT INTO `gzmtr_accident_process` VALUES ('44', '69', '10', '', '报告', '', '有关信息', '');
INSERT INTO `gzmtr_accident_process` VALUES ('45', '69', '22', '', '接收', '', '有关信息', '');
INSERT INTO `gzmtr_accident_process` VALUES ('46', '69', '10', '', '组织', '', '员工', '灭火或撤退');
INSERT INTO `gzmtr_accident_process` VALUES ('47', '69', '10', '撤退时', '负责确认', '', '所有站内人员', '');
INSERT INTO `gzmtr_accident_process` VALUES ('48', '69', '35', '', '疏散完毕', '', '', '');
INSERT INTO `gzmtr_accident_process` VALUES ('49', '69', '8', '', '接到', '', '火警通知', '');
INSERT INTO `gzmtr_accident_process` VALUES ('50', '69', '8', '', '赶到', '', '现场', '');
INSERT INTO `gzmtr_accident_process` VALUES ('51', '69', '8', '', '确认并协助处理', '', '', '');
INSERT INTO `gzmtr_accident_process` VALUES ('52', '69', '8', '', '配合', '', '值班站长', '进行现场确认');
INSERT INTO `gzmtr_accident_process` VALUES ('53', '69', '10', '需要时', '进入', '', '房间', '');
INSERT INTO `gzmtr_accident_process` VALUES ('54', '69', '10', '', '确认', '', '火灾', '');
INSERT INTO `gzmtr_accident_process` VALUES ('55', '69', '8', '', '负责维持', '敞开', '房门', '');
INSERT INTO `gzmtr_accident_process` VALUES ('56', '69', '8', '及时', '传递', '', '信息', '');
INSERT INTO `gzmtr_accident_process` VALUES ('57', '69', '', '', '', '', '火灾', '不可控制');
INSERT INTO `gzmtr_accident_process` VALUES ('58', '69', '8', '立即', '赶到', '', '车控室', '');
INSERT INTO `gzmtr_accident_process` VALUES ('59', '69', '8', '', '协助', '', '行车值班员', '');
INSERT INTO `gzmtr_accident_process` VALUES ('60', '69', '9', '', '确认', '', '火灾模式', '开启');
INSERT INTO `gzmtr_accident_process` VALUES ('61', '69', '9', '', '确认', '', '疏散指示', '开启');
INSERT INTO `gzmtr_accident_process` VALUES ('62', '69', '8', '', '确认', '所有', '闸机', '紧急模式');
INSERT INTO `gzmtr_accident_process` VALUES ('63', '69', '8', '', '关闭', '', '广告照明', '');
INSERT INTO `gzmtr_accident_process` VALUES ('64', '69', '8', '', '按照', '环调', '指示', '');
INSERT INTO `gzmtr_accident_process` VALUES ('65', '69', '8', '', '操作', '有关', '设备', '');
INSERT INTO `gzmtr_accident_process` VALUES ('66', '69', '8', '', '确认', '行车值班员', '报警情况', '');
INSERT INTO `gzmtr_accident_process` VALUES ('67', '69', '13', '', '接到', '执行火灾应急疏散处理程序', '通知', '');
INSERT INTO `gzmtr_accident_process` VALUES ('68', '69', '13', '', '收好', '', '钱和票', '');
INSERT INTO `gzmtr_accident_process` VALUES ('69', '69', '13', '', '关闭', '', '票亭电源', '');
INSERT INTO `gzmtr_accident_process` VALUES ('70', '69', '13', '', '确认', '', '闸机', '进入紧急模式');
INSERT INTO `gzmtr_accident_process` VALUES ('71', '69', '13', '', '打开', '', '边门', '');
INSERT INTO `gzmtr_accident_process` VALUES ('72', '69', '13', '', '利用', '', '手提广播', '');
INSERT INTO `gzmtr_accident_process` VALUES ('73', '69', '13', '', '疏导', '', '乘客', '出站');
INSERT INTO `gzmtr_accident_process` VALUES ('74', '69', '13', '', '确认关停', '', '电扶梯', '');
INSERT INTO `gzmtr_accident_process` VALUES ('75', '69', '13', '', '到', '', '出口', '');
INSERT INTO `gzmtr_accident_process` VALUES ('76', '69', '13', '', '拦截', '', '乘客', '进站');
INSERT INTO `gzmtr_accident_process` VALUES ('77', '69', '13', '', '作好', '', '解释工作', '');
INSERT INTO `gzmtr_accident_process` VALUES ('78', '69', '14', '', '接到', '执行火灾应急疏散处理程序', '通知', '');
INSERT INTO `gzmtr_accident_process` VALUES ('79', '69', '14', '', '收好', '', '钱和票', '');
INSERT INTO `gzmtr_accident_process` VALUES ('80', '69', '14', '', '关闭', '', '票亭电源', '');
INSERT INTO `gzmtr_accident_process` VALUES ('81', '69', '14', '', '确认', '', '闸机', '');
INSERT INTO `gzmtr_accident_process` VALUES ('82', '69', '36', '', '进入', '', '紧急模式', '');
INSERT INTO `gzmtr_accident_process` VALUES ('83', '69', '14', '', '打开', '', '边门', '');
INSERT INTO `gzmtr_accident_process` VALUES ('84', '69', '14', '', '利用', '', '手提广播', '');
INSERT INTO `gzmtr_accident_process` VALUES ('85', '69', '14', '', '疏导', '', '乘客', '出站');
INSERT INTO `gzmtr_accident_process` VALUES ('86', '69', '14', '', '确认', '站厅', '乘客', '疏散出站');
INSERT INTO `gzmtr_accident_process` VALUES ('87', '69', '14', '', '确认', '垂直', '电梯', '是否困人');
INSERT INTO `gzmtr_accident_process` VALUES ('88', '69', '14', '', '关闭', '垂直', '电梯', '');
INSERT INTO `gzmtr_accident_process` VALUES ('89', '69', '14', '', '报告', '', '车控室', '');
INSERT INTO `gzmtr_accident_process` VALUES ('90', '69', '14', '', '听从', '', '值班站长', '安排');
INSERT INTO `gzmtr_accident_process` VALUES ('91', '69', '16', '', '接到', '', '通知', '');
INSERT INTO `gzmtr_accident_process` VALUES ('92', '69', '16', '立即', '赶到', '协助灭火', '现场', '');
INSERT INTO `gzmtr_accident_process` VALUES ('93', '69', '16', '', '确认', '不可控制', '火灾', '');
INSERT INTO `gzmtr_accident_process` VALUES ('94', '69', '12', '', '到', '', '车控室', '');
INSERT INTO `gzmtr_accident_process` VALUES ('95', '69', '12', '', '拿', '', '“安民告示”', '');
INSERT INTO `gzmtr_accident_process` VALUES ('96', '69', '12', '', '到', '', '出入口', '');
INSERT INTO `gzmtr_accident_process` VALUES ('97', '69', '12', '', '张贴', '', '“安民告示”', '');
INSERT INTO `gzmtr_accident_process` VALUES ('98', '69', '12', '', '关停', '出入口', '扶梯', '');
INSERT INTO `gzmtr_accident_process` VALUES ('99', '69', '21', '', '到来', '', '', '');
INSERT INTO `gzmtr_accident_process` VALUES ('100', '69', '12', '', '引导', '', '消防队', '');
INSERT INTO `gzmtr_accident_process` VALUES ('101', '69', '21', '', '到', '', '现场', '');
INSERT INTO `gzmtr_accident_process` VALUES ('102', '69', '21', '', '灭火', '', '', '');
INSERT INTO `gzmtr_accident_process` VALUES ('103', '69', '15', '', '协助疏导', '', '乘客', '出站');
INSERT INTO `gzmtr_accident_process` VALUES ('104', '69', '2', '', '通知', '', '司机', '扣车');
INSERT INTO `gzmtr_accident_process` VALUES ('105', '69', '17', '', '在', '', '火灾站的前方站', '扣车');
INSERT INTO `gzmtr_accident_process` VALUES ('106', '69', '17', '', '在', '', '站台', '');
INSERT INTO `gzmtr_accident_process` VALUES ('107', '69', '17', '', '开门待令', '', '', '');
INSERT INTO `gzmtr_accident_process` VALUES ('108', '69', '17', '', '做好', '', '乘客广播', '');
INSERT INTO `gzmtr_accident_process` VALUES ('109', '69', '2', '', '决定通过', '', '火灾站', '');
INSERT INTO `gzmtr_accident_process` VALUES ('110', '69', '17', '', '做好', '', '乘客广播', '');
INSERT INTO `gzmtr_accident_process` VALUES ('111', '69', '17', '', '加强', '', '了望', '');
INSERT INTO `gzmtr_accident_process` VALUES ('112', '69', '17', '', '确认', '', '进路', '');
INSERT INTO `gzmtr_accident_process` VALUES ('113', '69', '17', '', '接到', '车站发生火灾', '通知', '');
INSERT INTO `gzmtr_accident_process` VALUES ('114', '69', '2', '', '决定不通过', '', '火灾站', '');
INSERT INTO `gzmtr_accident_process` VALUES ('115', '69', '17', '', '做好', '', '乘客广播', '');
INSERT INTO `gzmtr_accident_process` VALUES ('116', '69', '17', '', '通知', '火灾站', '车上乘客', '不下车');
INSERT INTO `gzmtr_accident_process` VALUES ('117', '69', '33', '', '停在', '', '火灾站', '');
INSERT INTO `gzmtr_accident_process` VALUES ('118', '69', '17', '立即', '关门动车', '', '', '');
INSERT INTO `gzmtr_accident_process` VALUES ('119', '69', '7', '立即', '赶到', '', '现场', '');
INSERT INTO `gzmtr_accident_process` VALUES ('120', '69', '7', '', '协助灭火', '', '', '');
INSERT INTO `gzmtr_accident_process` VALUES ('121', '69', '7', '', '确认', '', '火灾', '不可控制');
INSERT INTO `gzmtr_accident_process` VALUES ('122', '69', '7', '立即', '关停', '非疏散方向', '扶梯', '');
INSERT INTO `gzmtr_accident_process` VALUES ('123', '69', '7', '', '组织', '站台', '乘客', '向站外疏散');
INSERT INTO `gzmtr_accident_process` VALUES ('124', '69', '7', '', '确认', '站台', '乘客', '疏散完毕');
INSERT INTO `gzmtr_accident_process` VALUES ('125', '69', '7', '', '报', '', '车控室', '');
INSERT INTO `gzmtr_accident_process` VALUES ('126', '69', '7', '', '听从', '', '值班站长', '安排');
INSERT INTO `gzmtr_accident_process` VALUES ('127', '69', '9', '', '接收', '火警', '信息', '');
INSERT INTO `gzmtr_accident_process` VALUES ('128', '69', '9', '立即', '通知', '', '值班站长', '确认');
INSERT INTO `gzmtr_accident_process` VALUES ('129', '69', '10', '', '到', '', '报警点', '');
INSERT INTO `gzmtr_accident_process` VALUES ('130', '69', '9', '', '确认', '', '火灾', '');
INSERT INTO `gzmtr_accident_process` VALUES ('131', '69', '9', '', '通知', '', '驻站人员', '协助灭火');
INSERT INTO `gzmtr_accident_process` VALUES ('132', '69', '9', '', '报', '', '环调、行调、119、地铁公安和120', '');
INSERT INTO `gzmtr_accident_process` VALUES ('133', '69', '9', '根据情况', '询问', '', '行调', '');
INSERT INTO `gzmtr_accident_process` VALUES ('134', '69', '9', '', '申请', '本站', '列车', '通过');
INSERT INTO `gzmtr_accident_process` VALUES ('135', '69', '9', '', '按压', '', 'AFC紧急按钮', '');
INSERT INTO `gzmtr_accident_process` VALUES ('136', '69', '9', '', '设置', '', '闸机', '进入紧急模式');
INSERT INTO `gzmtr_accident_process` VALUES ('137', '69', '9', '', '广播通知', '所有', '岗位', '');
INSERT INTO `gzmtr_accident_process` VALUES ('138', '69', '26', '所有', '执行', '设备区火灾', '应急疏散处理程序', '');
INSERT INTO `gzmtr_accident_process` VALUES ('139', '69', '9', '反复', '广播引导', '', '乘客', '疏散');
INSERT INTO `gzmtr_accident_process` VALUES ('140', '69', '9', '及时', '报告', '', '行调', '');
INSERT INTO `gzmtr_accident_process` VALUES ('141', '69', '2', '', '接收', '', '火灾情况', '');
INSERT INTO `gzmtr_accident_process` VALUES ('142', '69', '9', '', '与', '', '行调、值班站长', '保持联系');
INSERT INTO `gzmtr_accident_process` VALUES ('143', '69', '9', '', '安排', '', '保洁人员', '员到紧急出口外');
INSERT INTO `gzmtr_accident_process` VALUES ('144', '69', '12', '', '接', '', '消防人员', '');
INSERT INTO `gzmtr_accident_process` VALUES ('145', '69', '9', '', '设置', '', '相关设备区通道门门禁', '常开状态');
INSERT INTO `gzmtr_accident_process` VALUES ('146', '69', '9', '需撤退时', '随身携带', '', '无线电台', '联系行调');
INSERT INTO `gzmtr_accident_process` VALUES ('147', '69', '10', '', '接到', '', '火警通知', '');
INSERT INTO `gzmtr_accident_process` VALUES ('148', '69', '10', '立即', '携带', '', '钥匙', '');
INSERT INTO `gzmtr_accident_process` VALUES ('149', '69', '10', '', '与', '', '客运值班员', '到现场确认');
INSERT INTO `gzmtr_accident_process` VALUES ('150', '69', '气体控制', '报警房间外', '打为手动', '', '', '');
INSERT INTO `gzmtr_accident_process` VALUES ('151', '69', '10', '', '闻和触感', '房门', '温度', '');
INSERT INTO `gzmtr_accident_process` VALUES ('152', '69', '10', '', '判断', '', '火灾', '是否发生');
INSERT INTO `gzmtr_accident_process` VALUES ('153', '69', '10', '', '初步判断', '', '火灾', '未发生');
INSERT INTO `gzmtr_accident_process` VALUES ('154', '69', '10', '', '初步判断', '', '气体', '未喷发');
INSERT INTO `gzmtr_accident_process` VALUES ('155', '69', '10', '', '打开', '', '房间门', '');
INSERT INTO `gzmtr_accident_process` VALUES ('156', '69', '10', '', '安排', '一名', '员工', '在门外');
INSERT INTO `gzmtr_accident_process` VALUES ('157', '69', '34', '一名', '保持', '', '门', '敞开状态');
INSERT INTO `gzmtr_accident_process` VALUES ('158', '69', '10', '', '观察确认', '', '房间情况', '');
INSERT INTO `gzmtr_accident_process` VALUES ('159', '69', '10', '', '确认', '', '火灾', '');
INSERT INTO `gzmtr_accident_process` VALUES ('160', '69', '', '', '', '', '火灾', '态势较小');
INSERT INTO `gzmtr_accident_process` VALUES ('161', '69', '10', '', '利用', '', '灭火器', '灭火');
INSERT INTO `gzmtr_accident_process` VALUES ('162', '69', '', '', '', '', '火灾', '态势较大');
INSERT INTO `gzmtr_accident_process` VALUES ('163', '69', '10', '立即', '关闭', '', '房门', '');
INSERT INTO `gzmtr_accident_process` VALUES ('164', '69', '10', '手动操作', '释放', '', '气体', '灭火');
INSERT INTO `gzmtr_accident_process` VALUES ('165', '69', '', '', '', '', '火灾', '不可控制');
INSERT INTO `gzmtr_accident_process` VALUES ('166', '69', '10', '', '宣布执行', '设备区火灾', '应急处理程序', '');
INSERT INTO `gzmtr_accident_process` VALUES ('167', '69', '10', '', '组织疏散', '', '乘客', '');
INSERT INTO `gzmtr_accident_process` VALUES ('168', '69', '10', '', '安排', '', '人员', '拦截乘客');
INSERT INTO `gzmtr_accident_process` VALUES ('169', '69', '34', '', '到', '', '入口', '');
INSERT INTO `gzmtr_accident_process` VALUES ('170', '69', '21', '', '到', '', '现场', '');
INSERT INTO `gzmtr_accident_process` VALUES ('171', '69', '10', '', '通报', '', '消防负责人', '');
INSERT INTO `gzmtr_accident_process` VALUES ('172', '69', '22', '', '接收', '', '有关信息', '');
INSERT INTO `gzmtr_accident_process` VALUES ('173', '69', '10', '', '组织', '', '员工', '撤退');
INSERT INTO `gzmtr_accident_process` VALUES ('174', '69', '10', '', '负责确认', '站内', '所有人员', '疏散完毕');
INSERT INTO `gzmtr_accident_process` VALUES ('175', '69', '8', '', '接到', '火警', '通知', '');
INSERT INTO `gzmtr_accident_process` VALUES ('176', '69', '8', '立即', '与', '', '值班站长', '赶到现场');
INSERT INTO `gzmtr_accident_process` VALUES ('177', '69', '8', '', '确认', '', '火灾', '');
INSERT INTO `gzmtr_accident_process` VALUES ('178', '69', '8', '', '协助', '', '值班站长', '处理');
INSERT INTO `gzmtr_accident_process` VALUES ('179', '69', '8', '', '配合', '', '值班站长', '现场确认');
INSERT INTO `gzmtr_accident_process` VALUES ('180', '69', '10', '需要进入房间确认时', '', '', '', '');
INSERT INTO `gzmtr_accident_process` VALUES ('181', '69', '8', '', '负责维持', '敞开状态', '房门', '');
INSERT INTO `gzmtr_accident_process` VALUES ('182', '69', '8', '及时', '传递', '', '信息', '');
INSERT INTO `gzmtr_accident_process` VALUES ('183', '69', '火灾', '不可控制', '', '', '', '');
INSERT INTO `gzmtr_accident_process` VALUES ('184', '69', '8', '立即', '赶到', '', '车控室', '');
INSERT INTO `gzmtr_accident_process` VALUES ('185', '69', '8', '', '协助', '', '行车值班员', '');
INSERT INTO `gzmtr_accident_process` VALUES ('186', '69', '8', '', '确认开启', '相应的', '火灾模式', '');
INSERT INTO `gzmtr_accident_process` VALUES ('187', '69', '8', '', '确认开启', '', '疏散指示', '');
INSERT INTO `gzmtr_accident_process` VALUES ('188', '69', '8', '', '确认设置', '紧急模式', '所有闸机', '');
INSERT INTO `gzmtr_accident_process` VALUES ('189', '69', '8', '', '关闭', '', '广告照明', '');
INSERT INTO `gzmtr_accident_process` VALUES ('190', '69', '8', '', '按照', '环调的', '指示', '');
INSERT INTO `gzmtr_accident_process` VALUES ('191', '69', '8', '', '操作', '', '有关设备', '');
INSERT INTO `gzmtr_accident_process` VALUES ('192', '69', '8', '', '确认', '行车值班员', '报警情况', '');
INSERT INTO `gzmtr_accident_process` VALUES ('193', '69', '13', '', '接到', '执行火灾应急疏散处理程序', '通知', '');
INSERT INTO `gzmtr_accident_process` VALUES ('194', '69', '13', '', '收好', '', '钱和票', '');
INSERT INTO `gzmtr_accident_process` VALUES ('195', '69', '13', '', '关闭', '', '票亭电源', '');
INSERT INTO `gzmtr_accident_process` VALUES ('196', '69', '13', '', '确认', '', '闸机', '');
INSERT INTO `gzmtr_accident_process` VALUES ('197', '69', '36', '', '进入', '', '紧急模式', '');
INSERT INTO `gzmtr_accident_process` VALUES ('198', '69', '13', '', '打开', '', '边门', '');
INSERT INTO `gzmtr_accident_process` VALUES ('199', '69', '13', '', '利用', '', '手提广播', '');
INSERT INTO `gzmtr_accident_process` VALUES ('200', '69', '13', '', '疏导', '', '乘客', '出站');
INSERT INTO `gzmtr_accident_process` VALUES ('201', '69', '13', '', '确认关停', '', '电扶梯', '');
INSERT INTO `gzmtr_accident_process` VALUES ('202', '69', '13', '', '到', '', '出口', '');
INSERT INTO `gzmtr_accident_process` VALUES ('203', '69', '13', '', '拦截', '', '乘客', '进站');
INSERT INTO `gzmtr_accident_process` VALUES ('204', '69', '13', '', '作好', '', '解释工作', '');
INSERT INTO `gzmtr_accident_process` VALUES ('205', '69', '14', '', '接到', '执行火灾应急疏散处理程序', '通知', '');
INSERT INTO `gzmtr_accident_process` VALUES ('206', '69', '14', '', '收好', '', '钱和票', '');
INSERT INTO `gzmtr_accident_process` VALUES ('207', '69', '14', '', '关闭', '', '票亭电源', '');
INSERT INTO `gzmtr_accident_process` VALUES ('208', '69', '14', '', '确认', '', '闸机', '');
INSERT INTO `gzmtr_accident_process` VALUES ('209', '69', '36', '', '进入', '', '紧急模式', '');
INSERT INTO `gzmtr_accident_process` VALUES ('210', '69', '14', '', '打开', '', '边门', '');
INSERT INTO `gzmtr_accident_process` VALUES ('211', '69', '14', '', '利用', '', '手提广播', '');
INSERT INTO `gzmtr_accident_process` VALUES ('212', '69', '14', '', '疏导', '', '乘客', '出站');
INSERT INTO `gzmtr_accident_process` VALUES ('213', '69', '14', '', '确认', '站厅', '乘客', '疏散出站');
INSERT INTO `gzmtr_accident_process` VALUES ('214', '69', '14', '', '确认', '垂直', '电梯', '是否困人');
INSERT INTO `gzmtr_accident_process` VALUES ('215', '69', '14', '', '关闭', '垂直', '电梯', '');
INSERT INTO `gzmtr_accident_process` VALUES ('216', '69', '14', '', '报告', '', '车控室', '');
INSERT INTO `gzmtr_accident_process` VALUES ('217', '69', '14', '', '听从', '', '值班站长', '安排');
INSERT INTO `gzmtr_accident_process` VALUES ('218', '69', '16', '', '接到', '', '通知', '');
INSERT INTO `gzmtr_accident_process` VALUES ('219', '69', '16', '立即', '赶到', '协助灭火', '现场', '');
INSERT INTO `gzmtr_accident_process` VALUES ('220', '69', '16', '', '确认', '不可控制', '火灾', '');
INSERT INTO `gzmtr_accident_process` VALUES ('221', '69', '12', '', '到', '', '车控室', '');
INSERT INTO `gzmtr_accident_process` VALUES ('222', '69', '12', '', '拿', '', '“安民告示”', '');
INSERT INTO `gzmtr_accident_process` VALUES ('223', '69', '12', '', '到', '', '出入口', '');
INSERT INTO `gzmtr_accident_process` VALUES ('224', '69', '12', '', '张贴', '', '“安民告示”', '');
INSERT INTO `gzmtr_accident_process` VALUES ('225', '69', '12', '', '关停', '出入口', '扶梯', '');
INSERT INTO `gzmtr_accident_process` VALUES ('226', '69', '21', '', '到来', '', '', '');
INSERT INTO `gzmtr_accident_process` VALUES ('227', '69', '12', '', '引导', '', '消防队', '');
INSERT INTO `gzmtr_accident_process` VALUES ('228', '69', '21', '', '到', '', '现场', '');
INSERT INTO `gzmtr_accident_process` VALUES ('229', '69', '21', '', '灭火', '', '', '');
INSERT INTO `gzmtr_accident_process` VALUES ('230', '69', '15', '', '协助疏导', '', '乘客', '出站');
INSERT INTO `gzmtr_accident_process` VALUES ('231', '69', '2', '', '通知', '', '司机', '扣车');
INSERT INTO `gzmtr_accident_process` VALUES ('232', '69', '17', '', '在', '', '火灾站的前方站', '扣车');
INSERT INTO `gzmtr_accident_process` VALUES ('233', '69', '17', '', '在', '', '站台', '');
INSERT INTO `gzmtr_accident_process` VALUES ('234', '69', '17', '', '开门待令', '', '', '');
INSERT INTO `gzmtr_accident_process` VALUES ('235', '69', '17', '', '做好', '', '乘客广播', '');
INSERT INTO `gzmtr_accident_process` VALUES ('236', '69', '2', '', '决定通过', '', '火灾站', '');
INSERT INTO `gzmtr_accident_process` VALUES ('237', '69', '17', '', '做好', '', '乘客广播', '');
INSERT INTO `gzmtr_accident_process` VALUES ('238', '69', '17', '', '加强', '', '了望', '');
INSERT INTO `gzmtr_accident_process` VALUES ('239', '69', '17', '', '确认', '', '进路', '');
INSERT INTO `gzmtr_accident_process` VALUES ('240', '69', '17', '', '接到', '车站发生火灾', '通知', '');
INSERT INTO `gzmtr_accident_process` VALUES ('241', '69', '2', '', '决定不通过', '', '火灾站', '');
INSERT INTO `gzmtr_accident_process` VALUES ('242', '69', '17', '', '做好', '', '乘客广播', '');
INSERT INTO `gzmtr_accident_process` VALUES ('243', '69', '17', '', '通知', '火灾站', '车上乘客', '不下车');
INSERT INTO `gzmtr_accident_process` VALUES ('244', '69', '33', '', '停在', '', '火灾站', '');
INSERT INTO `gzmtr_accident_process` VALUES ('245', '69', '17', '立即', '关门动车', '', '', '');
INSERT INTO `gzmtr_accident_process` VALUES ('246', '69', '0', '', '询问', '', 'OCC', '是否已经报110、119、120、人员伤亡、现场处理情况');
INSERT INTO `gzmtr_accident_process` VALUES ('247', '69', '', '', '提醒', '', 'OCC', '及时疏散现场乘客、做好现场保护和证人、证据保留； ');
INSERT INTO `gzmtr_accident_process` VALUES ('248', '69', '', '', '调出', '', '监控视频设备CCTV、计时器、正线或车厂线路图、应急卡、OCC视频拾音系统、东软应急辅助系统及HMI，', '');
INSERT INTO `gzmtr_accident_process` VALUES ('249', '69', '', '', '关注', '', '事件', '进展');
INSERT INTO `gzmtr_accident_process` VALUES ('250', '69', '', '', '提醒', '', 'OCC', '');
INSERT INTO `gzmtr_accident_process` VALUES ('251', '69', '25', '及时', '疏散', '车站', '乘客', '');
INSERT INTO `gzmtr_accident_process` VALUES ('252', '69', '25', '视现场影响', '采取', '关站', '措施', '');
INSERT INTO `gzmtr_accident_process` VALUES ('253', '69', '25', '', '调整', '', '环控模式', '');
INSERT INTO `gzmtr_accident_process` VALUES ('254', '69', '0', '', '启动', 'Ⅰ级', '应急响应', '');
INSERT INTO `gzmtr_accident_process` VALUES ('255', '69', '', '', '跟进', '现场', '处置各小组', '成立情况');
INSERT INTO `gzmtr_accident_process` VALUES ('256', '69', '', '', '要求', '相关', '负责人', '在Glink签到');
INSERT INTO `gzmtr_accident_process` VALUES ('257', '69', '3', '', '', '火灾', '', '发生在换乘站');
INSERT INTO `gzmtr_accident_process` VALUES ('258', '69', '0', '及时', '通报', '邻线', 'OCC', '');
INSERT INTO `gzmtr_accident_process` VALUES ('259', '69', '', '视情况', '要求', '邻线', 'OCC', '采取越站措施、调整环控模式、网控措施调整环控模式、网控措施');
INSERT INTO `gzmtr_accident_process` VALUES ('260', '69', '0', '', '通报', '', '广州地铁公安指挥室或佛山地铁公安指挥室', '');
INSERT INTO `gzmtr_accident_process` VALUES ('261', '69', '', '', '请求加派', '', '警力', '到场');
INSERT INTO `gzmtr_accident_process` VALUES ('262', '69', '0', '', '通报', '', '线网各OCC', '');
INSERT INTO `gzmtr_accident_process` VALUES ('263', '69', '', '', '发布', 'Glink、企信通', '应急响应指令', '');
INSERT INTO `gzmtr_accident_process` VALUES ('264', '69', '', '同时', '通知', '其他', '运营中心OCC', '调动应急抢险队伍和物资支援');
INSERT INTO `gzmtr_accident_process` VALUES ('265', '69', '0', '', '通报', '', '当班主任/主管、线网指挥中心经理、线网管控中心值班领导、分管领导、总经理。', '');
INSERT INTO `gzmtr_accident_process` VALUES ('266', '69', '0', '', '发布', '', 'Glink、企信通信息（含事件通报、抢险指令）；编辑《事故（事件）速报表》（初报）', '');
INSERT INTO `gzmtr_accident_process` VALUES ('267', '69', '31', '', '接收', '', 'Glink、企信通信息（含事件通报、抢险指令）；编辑《事故（事件）速报表》（初报）', '');
INSERT INTO `gzmtr_accident_process` VALUES ('268', '69', '0', '', '通报', '', '基地维修中心调度', '');
INSERT INTO `gzmtr_accident_process` VALUES ('269', '69', '', '视情况', '发布', '应急抢险', '指令', '');
INSERT INTO `gzmtr_accident_process` VALUES ('270', '69', '0', '', '通报', '', '抢险信息', '');
INSERT INTO `gzmtr_accident_process` VALUES ('271', '69', '32', '', '接收', '', '抢险信息', '');
INSERT INTO `gzmtr_accident_process` VALUES ('272', '69', '0', '视情况', '发布', '', '线网PIDS信息', '');
INSERT INTO `gzmtr_accident_process` VALUES ('273', '69', '', '视情况', '要求', '其他', '线路', '采取线控、网控措施配合事发线路处置');
INSERT INTO `gzmtr_accident_process` VALUES ('274', '69', '0', '视情况', '启动', '', '应急公交接驳', '');
INSERT INTO `gzmtr_accident_process` VALUES ('275', '69', '', '', '通报', '', 'a) 总部总经理及集团公司总经理；\nb) 安全质量部总经理（视情况请求安排护卫支援）及集团公司安全监察部应急管理经理或分管应急副部长；\nc) 集团公司办公室主任；总部办公室主任（视情况请求安排交通、护卫、餐饮、医疗支援；\nd) 广佛公司总经理（广佛线）；\ne) 总部党群工作部微博管理员；\nf) 广州/羊城/佛山交通电台（造成15分钟以上晚点时的运营信息）。\ng）视情况通报区应急办。', 'COCC');
INSERT INTO `gzmtr_accident_process` VALUES ('276', '69', '0', '视情况', '发布', '', 'PRM（官方网站）、手机APP', '');
INSERT INTO `gzmtr_accident_process` VALUES ('277', '69', '', '视情况', '发布', '', '客流实况系统', '');
INSERT INTO `gzmtr_accident_process` VALUES ('278', '69', '0', '', '跟进', '', '现场处置小组', '成立情况');
INSERT INTO `gzmtr_accident_process` VALUES ('279', '69', '', '', '要求', '相关', '负责人', '在Glink签到');
INSERT INTO `gzmtr_accident_process` VALUES ('280', '69', '0', '', '询问', '', 'OCC', '是否已经报110、119、120、人员伤亡、现场处理情况');
INSERT INTO `gzmtr_accident_process` VALUES ('281', '69', '', '', '提醒', '', 'OCC', '及时疏散现场乘客、做好现场保护和证人、证据保留； ');
INSERT INTO `gzmtr_accident_process` VALUES ('282', '69', '', '', '调出', '', '监控视频设备CCTV、计时器、正线或车厂线路图、应急卡、OCC视频拾音系统、东软应急辅助系统及HMI，', '');
INSERT INTO `gzmtr_accident_process` VALUES ('283', '69', '', '', '关注', '', '事件', '进展');
INSERT INTO `gzmtr_accident_process` VALUES ('284', '69', '', '', '提醒', '', 'OCC', '');
INSERT INTO `gzmtr_accident_process` VALUES ('285', '69', '25', '及时', '疏散', '车站', '乘客', '');
INSERT INTO `gzmtr_accident_process` VALUES ('286', '69', '25', '视现场影响', '采取', '关站', '措施', '');
INSERT INTO `gzmtr_accident_process` VALUES ('287', '69', '25', '', '调整', '', '环控模式', '');
INSERT INTO `gzmtr_accident_process` VALUES ('288', '69', '0', '', '启动', 'Ⅰ级', '应急响应', '');
INSERT INTO `gzmtr_accident_process` VALUES ('289', '69', '', '', '跟进', '现场', '处置各小组', '成立情况');
INSERT INTO `gzmtr_accident_process` VALUES ('290', '69', '', '', '要求', '相关', '负责人', '在Glink签到');
INSERT INTO `gzmtr_accident_process` VALUES ('291', '69', '', '', '', '火灾', ' 发生', '发生在换乘站');
INSERT INTO `gzmtr_accident_process` VALUES ('292', '69', '0', '及时', '通报', '邻线', 'OCC', '');
INSERT INTO `gzmtr_accident_process` VALUES ('293', '69', '0', '', '跟进', '', '现场指挥部人员', '到位情况');
INSERT INTO `gzmtr_accident_process` VALUES ('294', '69', '', '', '发布', '', 'Glink、企信通信信息', '');
INSERT INTO `gzmtr_accident_process` VALUES ('295', '69', '', '', '告知', '', '现场指挥和指挥成员名单、电话', '');
INSERT INTO `gzmtr_accident_process` VALUES ('296', '69', '0', '跟进', '', '指挥部无线通讯设备', '', '到位情况');
INSERT INTO `gzmtr_accident_process` VALUES ('297', '69', '0', '视现场情况', '询问', '', '广州市消防局24小时总值班室（38766119）', '相关情况');
INSERT INTO `gzmtr_accident_process` VALUES ('298', '69', '0', '根据现场指挥部要求', '协调', '', '抢险物资', '');
INSERT INTO `gzmtr_accident_process` VALUES ('299', '69', '', '根据现场指挥部要求', '协调', '', '抢险人员', '');
INSERT INTO `gzmtr_accident_process` VALUES ('300', '69', '', '必要时', '通知', '', '建总及公司外部抢险单位', '到场支援');
INSERT INTO `gzmtr_accident_process` VALUES ('301', '69', '0', '跟进', '', '伤员', '', '救治');
INSERT INTO `gzmtr_accident_process` VALUES ('302', '69', '', '', '跟进', '', '事发列车', '退出服务情况');
INSERT INTO `gzmtr_accident_process` VALUES ('303', '69', '', '', '跟进', '', '车站', '围蔽情况');
INSERT INTO `gzmtr_accident_process` VALUES ('304', '69', '', '', '跟进', '', '公安', '调查取证工作');
INSERT INTO `gzmtr_accident_process` VALUES ('305', '69', '0', '及时', '发布', '', 'Glink、企信通', '');
INSERT INTO `gzmtr_accident_process` VALUES ('306', '69', '', '及时', '更新', '', 'PIDS信息、PRM（官方网站）、手机APP', '');
INSERT INTO `gzmtr_accident_process` VALUES ('307', '69', '', '视情况', '发布', '', '客流实况系统', '');
INSERT INTO `gzmtr_accident_process` VALUES ('308', '69', '0', '事后', '编制', '', '《事故（事件）速报表》（续报）', '');
INSERT INTO `gzmtr_accident_process` VALUES ('309', '69', '', '事后', '发送', '', '安全质量部、集团公司安全监察部。', '');


-- ----------------------------
--  Table structure for `gzmtr_ccident_history`
-- ----------------------------
DROP TABLE IF EXISTS `gzmtr_accident_history`;
CREATE TABLE `gzmtr_accident_history` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `name` varchar(100) NOT NULL COMMENT '名称',
  `roadnet_id` int(11) NOT NULL COMMENT '事故次数',
  `area_code` char(30) NOT NULL,
  `date` datetime NOT NULL,
  `rank_code` varchar(100) NOT NULL COMMENT '级别',
  `category` varchar(200) NOT NULL COMMENT '种类',
  `cause` char(30) NOT NULL COMMENT '致因',
  `detail` varchar(200) NOT NULL COMMENT '描述',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=303 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC COMMENT='事故历史信息';

-- ----------------------------
--  Table structure for `gzmtr_resource`
-- ----------------------------
DROP TABLE IF EXISTS `gzmtr_resource`;
CREATE TABLE `gzmtr_resource` (
  `id`       INT(11)      NOT NULL AUTO_INCREMENT,
  `name`     VARCHAR(100) NOT NULL COMMENT '名称',
  `large_scale` VARCHAR(100)  NOT NULL COMMENT '是否是大型物资',
  `model_num` VARCHAR(200) NOT NULL COMMENT '型号',
  `fist`    VARCHAR(200) NOT NULL COMMENT '专业用途',
  `num`    INT(11)        NOT NULL COMMENT '现有数量',
  `unit` VARCHAR(100)     NOT NULL COMMENT '单位',
  `line` VARCHAR(100)     NOT NULL COMMENT '线别',
  `location` VARCHAR(100)     NOT NULL COMMENT '放置地点',
  `remark` VARCHAR(200)     NOT NULL COMMENT '地点备注',
  `center` VARCHAR(100)     NOT NULL COMMENT '中心',
  `usedp` VARCHAR(100)     NOT NULL COMMENT '使用部门',
  `maintaindp` VARCHAR(100)     NOT NULL COMMENT '维护部门',
  `update_time` DATETIME      NOT NULL COMMENT '更新时间',
  `state` VARCHAR(100)     NOT NULL COMMENT '状态',
  PRIMARY KEY (`id`)
)
  ENGINE = InnoDB
  AUTO_INCREMENT = 2
  DEFAULT CHARSET = utf8
  ROW_FORMAT = DYNAMIC
  COMMENT ='应急资源';

-- ----------------------------
--  Records of `gzmtr_resource`
-- ----------------------------


-- ----------------------------
--  Table structure for `gzmtr_contact_group`
-- ----------------------------
DROP TABLE IF EXISTS `gzmtr_contact_group`;
CREATE TABLE `gzmtr_contact_group` (
  `id`   INT(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `num`  INT(11)                   DEFAULT NULL COMMENT '排序',
  `pid`  INT(11)                   DEFAULT NULL COMMENT '父级id',
  `pids` VARCHAR(255)              DEFAULT NULL COMMENT '父级ids',
  `name` CHAR(50)         NOT NULL DEFAULT '' COMMENT '名称',
  PRIMARY KEY (`id`)
)
  ENGINE = InnoDB
  AUTO_INCREMENT = 20
  DEFAULT CHARSET = utf8
  ROW_FORMAT = DYNAMIC
  COMMENT ='通讯录群组';

-- ----------------------------
--  Records of `gzmtr_contact_group`

-- ----------------------------
INSERT INTO `gzmtr_contact_group` VALUES ('18', '1', '0', '0/', '广州');
INSERT INTO `gzmtr_contact_group` VALUES ('20', '1', '18', '0/18/', '火警');
INSERT INTO `gzmtr_contact_group` VALUES ('21', '121', '18', '0/18/', '急救');
INSERT INTO `gzmtr_contact_group` VALUES ('22', '21', '0', '0/', '佛山');
INSERT INTO `gzmtr_contact_group` VALUES ('23', '12', '22', '0/22/', '佛山市公安局公交分局');
INSERT INTO `gzmtr_contact_group` VALUES ('24', '412', '22', '0/22/', '佛山市交通运输局');


-- ----------------------------
--  Table structure for `gzmtr_accident_type`
-- ----------------------------
DROP TABLE IF EXISTS `gzmtr_accident_type`;
CREATE TABLE `gzmtr_accident_type` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `num` int(11) DEFAULT NULL COMMENT '排序',
  `pid` int(11) DEFAULT NULL COMMENT '父级id',
  `pids` varchar(255) DEFAULT NULL COMMENT '父级ids',
  `name` varchar(255) NOT NULL COMMENT '名称',
  `code` varchar(255) NOT NULL COMMENT '编码',
  PRIMARY KEY (`id`),
  UNIQUE KEY `code` (`code`)
) ENGINE=InnoDB AUTO_INCREMENT=54 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC COMMENT='事故处理措施类型';

-- ----------------------------
-- Records of gzmtr_accident_type
-- ----------------------------
INSERT INTO `gzmtr_accident_type` VALUES ('63', '1', '0', '0/', '火灾场景', '1');
INSERT INTO `gzmtr_accident_type` VALUES ('64', '1', '63', '0/63/', '车站', '1-1');
INSERT INTO `gzmtr_accident_type` VALUES ('65', '2', '63', '0/63/', '列车', '1-2');
INSERT INTO `gzmtr_accident_type` VALUES ('66', '3', '63', '0/63/', '区间', '1-3');
INSERT INTO `gzmtr_accident_type` VALUES ('67', '1', '64', '0/63/64/', '站台', '1-1-1');
INSERT INTO `gzmtr_accident_type` VALUES ('68', '2', '64', '0/63/64/', '站厅', '1-1-2');
INSERT INTO `gzmtr_accident_type` VALUES ('69', '3', '64', '0/63/64/', '设备区', '1-1-3');
INSERT INTO `gzmtr_accident_type` VALUES ('70', '1', '65', '0/63/65/', '列车前部', '1-2-1');
INSERT INTO `gzmtr_accident_type` VALUES ('71', '2', '65', '0/63/65/', '列车中部', '1-2-2');
INSERT INTO `gzmtr_accident_type` VALUES ('72', '3', '65', '0/63/65/', '列车后部', '1-2-3');
INSERT INTO `gzmtr_accident_type` VALUES ('73', '1', '66', '0/63/66/', '地下线', '1-3-1');
INSERT INTO `gzmtr_accident_type` VALUES ('74', '2', '66', '0/63/66/', '地面线', '1-3-2');
INSERT INTO `gzmtr_accident_type` VALUES ('75', '3', '66', '0/63/66/', '高架线', '1-3-3');


-- ----------------------------
--  Table structure for `gzmtr_contact`
-- ----------------------------
DROP TABLE IF EXISTS `gzmtr_contact`;
CREATE TABLE `gzmtr_contact` (
  `id`                  INT(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name`                CHAR(50)         NOT NULL DEFAULT '' COMMENT '姓名',
  `duty`                CHAR(50)                  DEFAULT NULL COMMENT '职务/专业',
  `administration_duty` CHAR(50)                  DEFAULT NULL COMMENT '行政职务',
  `phone`               CHAR(50)         NOT NULL DEFAULT '' COMMENT '座机',
  `group_id`            INT(11)                   DEFAULT NULL COMMENT '群组',
  `mobile_phone`        CHAR(11)         NOT NULL DEFAULT '' COMMENT '手机号',
  `email`               CHAR(50)                  DEFAULT NULL COMMENT '邮箱',
  PRIMARY KEY (`id`)
)
  ENGINE = InnoDB
  AUTO_INCREMENT = 5
  DEFAULT CHARSET = utf8
  ROW_FORMAT = DYNAMIC
  COMMENT ='通讯录';

-- ----------------------------
--  Table structure for `gzmtr_contact`
-- ----------------------------
INSERT INTO `gzmtr_contact` VALUES ('6', '蔡昌俊', '组长', '运营事业总部总经理', '84218995', '20', '84218995', '13212@211');
INSERT INTO `gzmtr_contact` VALUES ('7', '张海燕', '成员', '运营事业总部党委书记', '123454', '20', '32321', '41245@1534');
INSERT INTO `gzmtr_contact` VALUES ('9', '朱士友', '组长', '运营事业总部副总经理 兼总工程师', '86060', '21', '13924239019', '735635070@11.com');
INSERT INTO `gzmtr_contact` VALUES ('10', '周大林', '副组长', '运营事业总部副总经理', '86069', '21', '13925198271', '73563555@qq.com');
INSERT INTO `gzmtr_contact` VALUES ('11', '魏爱明', '成员', '运营事业总部工会主席', '12311012', '23', '15011308846', '123121@qq.com');
INSERT INTO `gzmtr_contact` VALUES ('12', '翟蓓立', '成员', '运营事业总部副总经理', '1231231', '23', '154664133', '213212@qq.com');
INSERT INTO `gzmtr_contact` VALUES ('13', '李少璧', '成员', '运营事业总部纪委书记', '123132132', '24', '15175112337', '4654564@qq.com');
INSERT INTO `gzmtr_contact` VALUES ('14', '蓝碧海', '成员', '运营事业总部财务部总经理', '2121321', '24', '123457717', '45634@qq.com');



-- ----------------------------
--  Table structure for `gzmtr_counterplan_type_template`
-- ----------------------------

DROP TABLE IF EXISTS `gzmtr_counterplan_type_template`;
CREATE TABLE `gzmtr_counterplan_type_template` (
  `id`      INT(11)  NOT NULL AUTO_INCREMENT COMMENT '编码',
  `type`    CHAR(30) NOT NULL UNIQUE COMMENT '类别',
  `content` LONGTEXT NOT NULL COMMENT '内容',
  PRIMARY KEY (`id`)
)
  ENGINE = InnoDB
  AUTO_INCREMENT = 4
  DEFAULT CHARSET = utf8
  COMMENT ='预案类型模板';



-- ----------------------------
--  Records of `gzmtr_counterplan_type_template`
-- ----------------------------
INSERT INTO `gzmtr_counterplan_type_template` VALUES ('2', '2', '<p>1 <span style=\"font-family:宋体\">前言</span></p><p style=\"text-indent:28px\"><span style=\"font-family: 等线\">本标准起草单位：（</span><span style=\"font-family: 宋体\">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span><span style=\"font-family: 等线\">）</span></p><p style=\"text-indent:28px\"><span style=\"font-family: 等线\">本标准主要起草人：（</span><span style=\"font-family: 宋体\">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span><span style=\"font-family: 等线\">）</span></p><p style=\"text-indent:28px\"><span style=\"font-family: 等线\">本标准版本号为第（）版、第（）次修订。</span></p><p style=\"text-indent:28px\"><span style=\"font-family: 等线\">本标准（）年（）月（）日发布。</span></p><p style=\"text-indent:28px\"><span style=\"font-family: 等线\">本标准从（）年（）月（）日起实施。原（）第（）版本、第（）次修订版本，原（）同时作废。</span></p><p style=\"text-indent:28px\"><a><span style=\"font-family: 等线\">本标准由广州地铁集团有限公司（</span></a><span style=\"font-family: 宋体\">&nbsp; </span><span style=\"font-family: 等线\">）解释。</span></p><p style=\"text-indent:28px\"><a><span style=\"font-family: 等线\">本标准由广州地铁集团有限公司（</span></a><span style=\"font-family: 宋体\">&nbsp; </span><span style=\"font-family: 等线\">）提出。</span></p><p style=\"text-indent:28px\"><span style=\"font-family: 等线\">本标准由广州地铁集团有限公司（</span><span style=\"font-family: 宋体\">&nbsp; </span><span style=\"font-family: 等线\">）归口。</span></p><p>2<span style=\"font-family:宋体\">范围</span></p><p>&nbsp;&nbsp;</p><p>3<span style=\"font-family:宋体\">引用标准</span></p><p style=\"margin-right:2px;text-indent:28px;text-autospace:none\"><span style=\"font-family:宋体\">下列标准所包含的条文，通过本标准中引用而构成本标准的条文。本标准出版时，所示版本均为有效。所有标准都会被修订，使用本标准的各方应探讨使用下列标准再新版本的可能性。</span></p><p>&nbsp;</p><p>4<span style=\"font-family:宋体\">定义</span></p><p>&nbsp;</p><p>5<span style=\"font-family:宋体\">应急处理程序</span></p><p>&nbsp;</p><p>6<span style=\"font-family:宋体\">附录</span></p><p><br/></p>');
INSERT INTO `gzmtr_counterplan_type_template` VALUES ('3', '3', '');
INSERT INTO `gzmtr_counterplan_type_template` VALUES ('4', '1', '');


DROP TABLE IF EXISTS `gzmtr_counterplan`;
CREATE TABLE `gzmtr_counterplan` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `name` char(50) DEFAULT NULL COMMENT '预案名称',
  `type` char(30) DEFAULT NULL COMMENT '类别',
  `accident_type_id` INT(11) NOT NULL  COMMENT '事故类型',
  `level` char(30) DEFAULT NULL COMMENT '响应等级',
  `content` longtext COMMENT '内容',
  `station_id` int(11) DEFAULT NULL COMMENT '车站',
  `accident_main`   char(30)                  DEFAULT '' COMMENT '岗位',
  `status` int(11) DEFAULT '0' COMMENT '状态(0：未审核  2：审核通过  3：审核未通过）',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=33 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC COMMENT='预案';

-- ----------------------------
-- Records of gzmtr_counterplan
-- ----------------------------

-- ----------------------------
-- Table structure for 预案模板
-- ----------------------------
DROP TABLE IF EXISTS `gzmtr_counterplan_template`;
CREATE TABLE `gzmtr_counterplan_template` (
  `id`               INT(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name`             CHAR(50)                  DEFAULT NULL COMMENT '预案模板名称',
  `type`             CHAR(30)                  DEFAULT NULL COMMENT '类别',
  `level`            CHAR(30)                  DEFAULT '' COMMENT '响应等级',
  `content`          LONGTEXT COMMENT '内容',
  `accident_type_id` INT(11)                   DEFAULT NULL COMMENT '事故类型',
  `station_id`       INT(11)                   DEFAULT NULL COMMENT '车站',
  `accident_main`    char(30)                  DEFAULT '' COMMENT '岗位',
  PRIMARY KEY (`id`)
)
  ENGINE = InnoDB AUTO_INCREMENT = 31
  DEFAULT CHARSET = utf8
  ROW_FORMAT = DYNAMIC
  COMMENT ='预案模板';

-- ----------------------------
-- Records of gzmtr_counterplan_template
-- ----------------------------


-- ----------------------------
--  Table structure for `gzmtr_counterplan_accident`
-- ----------------------------
DROP TABLE IF EXISTS `gzmtr_counterplan_process`;
CREATE TABLE `gzmtr_counterplan_process` (
  `id`        INT(11)      NOT NULL AUTO_INCREMENT,
  `counterplan_id` INT(11) NOT NULL COMMENT '预案id',
  `main`      VARCHAR(255) NOT NULL,
  `mproperty` VARCHAR(255) NOT NULL COMMENT '主体属性',
  `mmotion`   VARCHAR(255) NOT NULL COMMENT '主题动作',
  `object`    VARCHAR(255) NOT NULL COMMENT '客体',
  `oproperty` VARCHAR(255) NOT NULL COMMENT '客体属性',
  `omotion`   VARCHAR(255) NOT NULL COMMENT '客体动作',
  `num`       INT(11)      NOT NULL COMMENT '处置步骤',
  PRIMARY KEY (`id`)
)
  ENGINE = InnoDB DEFAULT CHARSET = utf8 COMMENT ='预案处置流程表';

/*
Navicat MySQL Data Transfer

Source Server         : aaa
Source Server Version : 50634
Source Host           : 127.0.0.1:3306
Source Database       : guns

Target Server Type    : MYSQL
Target Server Version : 50634
File Encoding         : 65001

Date: 2018-05-30 17:46:47
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for `gzmtr_accident`
-- ----------------------------
DROP TABLE IF EXISTS `gzmtr_accident`;
CREATE TABLE `gzmtr_accident` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL COMMENT '事故名称',
  `accident_type_id` int(11) NOT NULL COMMENT '事故类型',
  `line_id` int(11) NOT NULL COMMENT '事故线路',
  `station_id` int(11) NOT NULL COMMENT '事故车站',
  `train_number` varchar(100) NOT NULL COMMENT '车次',
  `interruption_time` int(10) NOT NULL COMMENT '中断时间',
  `trap_num` int(11) NOT NULL COMMENT '被困人数',
  `start_time` time NOT NULL COMMENT '发生时间',
  `accident_level` varchar(100) NOT NULL COMMENT '事故级别',
  `interruption_type` char(1) NOT NULL COMMENT '中断类型',
  `reduction` double(10,0) NOT NULL COMMENT '预计行车能力降低',
  `process` text COMMENT '处理流程',
  `emergency_id` int(11) DEFAULT NULL COMMENT '突发事件类型id',
  `emergencypro_id` int(11) DEFAULT NULL COMMENT '事故突发事件属性',
  `process_json` longtext COMMENT '处置流程数据json',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8 COMMENT='事故实例';

-- ----------------------------
-- Records of gzmtr_accident
-- ----------------------------
INSERT INTO `gzmtr_accident` VALUES ('3', null, '69', '19', '43', '007车次', '20', '1', '11:00:00', '2', '1', '1', '下午12:59:48\n7立即赶到现场\n7协助灭火\n7确认不可控制火灾\n下午12:59:49\n9通知值班站长\n9到报警点确认火灾\n9确认火灾\n下午12:59:51\n10立即携带防护用品\n10到现场\n10确认火灾\n下午12:59:52\n10撤退时负责确认所有站内人员\n8接到火警通知\n', '9', '12', null);
INSERT INTO `gzmtr_accident` VALUES ('4', null, '69', '19', '43', '1', '1', '1', '00:00:00', '2', '1', '1', '下午3:01:23\n7立即赶到现场\n7协助灭火\n7确认不可控制火灾\n下午3:01:24\n9通知值班站长\n9到报警点确认火灾\n9确认火灾\n下午3:01:25\n10立即携带防护用品\n10到现场\n10确认火灾\n下午3:01:27\n10撤退时负责确认所有站内人员\n8接到火警通知\n', null, null, null);
INSERT INTO `gzmtr_accident` VALUES ('5', null, '69', '19', '46', '1', '1', '1', '00:00:00', '2', '1', '1', '下午3:03:26\n7立即赶到现场\n7协助灭火\n7确认不可控制火灾\n下午3:03:27\n9通知值班站长\n9到报警点确认火灾\n9确认火灾\n下午3:03:28\n10立即携带防护用品\n10到现场\n10确认火灾\n下午3:03:30\n10撤退时负责确认所有站内人员\n8接到火警通知\n', null, null, null);
INSERT INTO `gzmtr_accident` VALUES ('6', null, '69', '19', '44', '1', '1', '1', '00:00:00', '2', '1', '1', '下午3:07:20\n7立即赶到现场\n7协助灭火\n7确认不可控制火灾\n下午3:07:21\n9通知值班站长\n9到报警点确认火灾\n9确认火灾\n下午3:07:22\n10立即携带防护用品\n10到现场\n10确认火灾\n下午3:07:23\n10撤退时负责确认所有站内人员\n8接到火警通知\n', null, null, null);
INSERT INTO `gzmtr_accident` VALUES ('7', null, '69', '19', '41', '1', '1', '1', '00:00:00', '2', '1', '1', '下午3:34:29\n7立即赶到现场\n7协助灭火\n7确认不可控制火灾\n下午3:34:30\n9通知值班站长\n9到报警点确认火灾\n9确认火灾\n下午3:34:31\n10立即携带防护用品\n10到现场\n10确认火灾\n下午3:34:32\n10撤退时负责确认所有站内人员\n8接到火警通知\n', null, null, '{\"title\":\"处置流程编辑\",\"nodes\":{\"start_node\":{\"name\":\"开始\",\"left\":207,\"top\":20,\"type\":\"start round mix\",\"width\":28,\"height\":28,\"alt\":true},\"node_0\":{\"name\":\"\",\"left\":195,\"top\":160,\"type\":\"fork\",\"width\":104,\"height\":28,\"alt\":true},\"node_0_0\":{\"name\":\"7立即赶到现场\",\"left\":120,\"top\":220,\"type\":\"task\",\"width\":200,\"height\":28,\"alt\":true},\"node_0_1\":{\"name\":\"7协助灭火\",\"left\":360,\"top\":220,\"type\":\"task\",\"width\":200,\"height\":28,\"alt\":true},\"node_0_2\":{\"name\":\"7确认不可控制火灾\",\"left\":600,\"top\":220,\"type\":\"task\",\"width\":200,\"height\":28,\"alt\":true},\"node_1\":{\"name\":\"\",\"left\":195,\"top\":280,\"type\":\"fork\",\"width\":104,\"height\":28,\"alt\":true},\"node_1_0\":{\"name\":\"9通知值班站长\",\"left\":120,\"top\":340,\"type\":\"task\",\"width\":200,\"height\":28,\"alt\":true},\"node_1_1\":{\"name\":\"9到报警点确认火灾\",\"left\":360,\"top\":340,\"type\":\"task\",\"width\":200,\"height\":28,\"alt\":true},\"node_1_2\":{\"name\":\"9确认火灾\",\"left\":600,\"top\":340,\"type\":\"task\",\"width\":200,\"height\":28,\"alt\":true},\"node_2\":{\"name\":\"\",\"left\":195,\"top\":400,\"type\":\"fork\",\"width\":104,\"height\":28,\"alt\":true},\"node_2_0\":{\"name\":\"10立即携带防护用品\",\"left\":120,\"top\":460,\"type\":\"task\",\"width\":200,\"height\":28,\"alt\":true},\"node_2_1\":{\"name\":\"10到现场\",\"left\":360,\"top\":460,\"type\":\"task\",\"width\":200,\"height\":28,\"alt\":true},\"node_2_2\":{\"name\":\"10确认火灾\",\"left\":600,\"top\":460,\"type\":\"task\",\"width\":200,\"height\":28,\"alt\":true},\"node_3\":{\"name\":\"\",\"left\":195,\"top\":520,\"type\":\"fork\",\"width\":104,\"height\":28,\"alt\":true},\"node_3_0\":{\"name\":\"10撤退时负责确认所有站内人员\",\"left\":120,\"top\":580,\"type\":\"task\",\"width\":200,\"height\":28,\"alt\":true},\"node_3_1\":{\"name\":\"8接到火警通知\",\"left\":360,\"top\":580,\"type\":\"task\",\"width\":200,\"height\":28,\"alt\":true}},\"lines\":{\"start_nodenode_0\":{\"type\":\"sl\",\"from\":\"start_node\",\"to\":\"node_0\",\"name\":\"\",\"dash\":false,\"alt\":true},\"node_0node_0_0\":{\"type\":\"sl\",\"from\":\"node_0\",\"to\":\"node_0_0\",\"name\":\"\",\"dash\":false,\"alt\":true},\"node_0node_0_1\":{\"type\":\"sl\",\"from\":\"node_0\",\"to\":\"node_0_1\",\"name\":\"\",\"dash\":false,\"alt\":true},\"node_0node_0_2\":{\"type\":\"sl\",\"from\":\"node_0\",\"to\":\"node_0_2\",\"name\":\"\",\"dash\":false,\"alt\":true},\"node_0_0node_1\":{\"type\":\"sl\",\"from\":\"node_0_0\",\"to\":\"node_1\",\"name\":\"\",\"dash\":false,\"alt\":true},\"node_0_1node_1\":{\"type\":\"sl\",\"from\":\"node_0_1\",\"to\":\"node_1\",\"name\":\"\",\"dash\":false,\"alt\":true},\"node_0_2node_1\":{\"type\":\"sl\",\"from\":\"node_0_2\",\"to\":\"node_1\",\"name\":\"\",\"dash\":false,\"alt\":true},\"node_1node_1_0\":{\"type\":\"sl\",\"from\":\"node_1\",\"to\":\"node_1_0\",\"name\":\"\",\"dash\":false,\"alt\":true},\"node_1node_1_1\":{\"type\":\"sl\",\"from\":\"node_1\",\"to\":\"node_1_1\",\"name\":\"\",\"dash\":false,\"alt\":true},\"node_1node_1_2\":{\"type\":\"sl\",\"from\":\"node_1\",\"to\":\"node_1_2\",\"name\":\"\",\"dash\":false,\"alt\":true},\"node_1_0node_2\":{\"type\":\"sl\",\"from\":\"node_1_0\",\"to\":\"node_2\",\"name\":\"\",\"dash\":false,\"alt\":true},\"node_1_1node_2\":{\"type\":\"sl\",\"from\":\"node_1_1\",\"to\":\"node_2\",\"name\":\"\",\"dash\":false,\"alt\":true},\"node_1_2node_2\":{\"type\":\"sl\",\"from\":\"node_1_2\",\"to\":\"node_2\",\"name\":\"\",\"dash\":false,\"alt\":true},\"node_2node_2_0\":{\"type\":\"sl\",\"from\":\"node_2\",\"to\":\"node_2_0\",\"name\":\"\",\"dash\":false,\"alt\":true},\"node_2node_2_1\":{\"type\":\"sl\",\"from\":\"node_2\",\"to\":\"node_2_1\",\"name\":\"\",\"dash\":false,\"alt\":true},\"node_2node_2_2\":{\"type\":\"sl\",\"from\":\"node_2\",\"to\":\"node_2_2\",\"name\":\"\",\"dash\":false,\"alt\":true},\"node_2_0node_3\":{\"type\":\"sl\",\"from\":\"node_2_0\",\"to\":\"node_3\",\"name\":\"\",\"dash\":false,\"alt\":true},\"node_2_1node_3\":{\"type\":\"sl\",\"from\":\"node_2_1\",\"to\":\"node_3\",\"name\":\"\",\"dash\":false,\"alt\":true},\"node_2_2node_3\":{\"type\":\"sl\",\"from\":\"node_2_2\",\"to\":\"node_3\",\"name\":\"\",\"dash\":false,\"alt\":true},\"node_3node_3_0\":{\"type\":\"sl\",\"from\":\"node_3\",\"to\":\"node_3_0\",\"name\":\"\",\"dash\":false,\"alt\":true},\"node_3node_3_1\":{\"type\":\"sl\",\"from\":\"node_3\",\"to\":\"node_3_1\",\"name\":\"\",\"dash\":false,\"alt\":true}},\"areas\":{},\"initNum\":1}');
INSERT INTO `gzmtr_accident` VALUES ('8', null, '69', '19', '44', '1', '1', '1', '00:00:00', '2', '1', '1', '下午4:00:53\n7立即赶到现场\n7协助灭火\n7确认不可控制火灾\n下午4:00:54\n9通知值班站长\n9到报警点确认火灾\n9确认火灾\n下午4:00:55\n10立即携带防护用品\n10到现场\n10确认火灾\n下午4:00:57\n10撤退时负责确认所有站内人员\n8接到火警通知\n', '9', '40', '{\"title\":\"处置流程编辑\",\"nodes\":{\"start_node\":{\"name\":\"开始\",\"left\":207,\"top\":20,\"type\":\"start round mix\",\"width\":28,\"height\":28,\"alt\":true},\"node_0\":{\"name\":\"\",\"left\":195,\"top\":160,\"type\":\"fork\",\"width\":104,\"height\":28,\"alt\":true},\"node_0_0\":{\"name\":\"7立即赶到现场\",\"left\":120,\"top\":220,\"type\":\"task\",\"width\":200,\"height\":28,\"alt\":true},\"node_0_1\":{\"name\":\"7协助灭火\",\"left\":360,\"top\":220,\"type\":\"task\",\"width\":200,\"height\":28,\"alt\":true},\"node_0_2\":{\"name\":\"7确认不可控制火灾\",\"left\":600,\"top\":220,\"type\":\"task\",\"width\":200,\"height\":28,\"alt\":true},\"node_1\":{\"name\":\"\",\"left\":195,\"top\":280,\"type\":\"fork\",\"width\":104,\"height\":28,\"alt\":true},\"node_1_0\":{\"name\":\"9通知值班站长\",\"left\":120,\"top\":340,\"type\":\"task\",\"width\":200,\"height\":28,\"alt\":true},\"node_1_1\":{\"name\":\"9到报警点确认火灾\",\"left\":360,\"top\":340,\"type\":\"task\",\"width\":200,\"height\":28,\"alt\":true},\"node_1_2\":{\"name\":\"9确认火灾\",\"left\":600,\"top\":340,\"type\":\"task\",\"width\":200,\"height\":28,\"alt\":true},\"node_2\":{\"name\":\"\",\"left\":195,\"top\":400,\"type\":\"fork\",\"width\":104,\"height\":28,\"alt\":true},\"node_2_0\":{\"name\":\"10立即携带防护用品\",\"left\":120,\"top\":460,\"type\":\"task\",\"width\":200,\"height\":28,\"alt\":true},\"node_2_1\":{\"name\":\"10到现场\",\"left\":360,\"top\":460,\"type\":\"task\",\"width\":200,\"height\":28,\"alt\":true},\"node_2_2\":{\"name\":\"10确认火灾\",\"left\":600,\"top\":460,\"type\":\"task\",\"width\":200,\"height\":28,\"alt\":true},\"node_3\":{\"name\":\"\",\"left\":195,\"top\":520,\"type\":\"fork\",\"width\":104,\"height\":28,\"alt\":true},\"node_3_0\":{\"name\":\"10撤退时负责确认所有站内人员\",\"left\":120,\"top\":580,\"type\":\"task\",\"width\":200,\"height\":28,\"alt\":true},\"node_3_1\":{\"name\":\"8接到火警通知\",\"left\":360,\"top\":580,\"type\":\"task\",\"width\":200,\"height\":28,\"alt\":true}},\"lines\":{\"start_nodenode_0\":{\"type\":\"sl\",\"from\":\"start_node\",\"to\":\"node_0\",\"name\":\"\",\"dash\":false,\"alt\":true},\"node_0node_0_0\":{\"type\":\"sl\",\"from\":\"node_0\",\"to\":\"node_0_0\",\"name\":\"\",\"dash\":false,\"alt\":true},\"node_0node_0_1\":{\"type\":\"sl\",\"from\":\"node_0\",\"to\":\"node_0_1\",\"name\":\"\",\"dash\":false,\"alt\":true},\"node_0node_0_2\":{\"type\":\"sl\",\"from\":\"node_0\",\"to\":\"node_0_2\",\"name\":\"\",\"dash\":false,\"alt\":true},\"node_0_0node_1\":{\"type\":\"sl\",\"from\":\"node_0_0\",\"to\":\"node_1\",\"name\":\"\",\"dash\":false,\"alt\":true},\"node_0_1node_1\":{\"type\":\"sl\",\"from\":\"node_0_1\",\"to\":\"node_1\",\"name\":\"\",\"dash\":false,\"alt\":true},\"node_0_2node_1\":{\"type\":\"sl\",\"from\":\"node_0_2\",\"to\":\"node_1\",\"name\":\"\",\"dash\":false,\"alt\":true},\"node_1node_1_0\":{\"type\":\"sl\",\"from\":\"node_1\",\"to\":\"node_1_0\",\"name\":\"\",\"dash\":false,\"alt\":true},\"node_1node_1_1\":{\"type\":\"sl\",\"from\":\"node_1\",\"to\":\"node_1_1\",\"name\":\"\",\"dash\":false,\"alt\":true},\"node_1node_1_2\":{\"type\":\"sl\",\"from\":\"node_1\",\"to\":\"node_1_2\",\"name\":\"\",\"dash\":false,\"alt\":true},\"node_1_0node_2\":{\"type\":\"sl\",\"from\":\"node_1_0\",\"to\":\"node_2\",\"name\":\"\",\"dash\":false,\"alt\":true},\"node_1_1node_2\":{\"type\":\"sl\",\"from\":\"node_1_1\",\"to\":\"node_2\",\"name\":\"\",\"dash\":false,\"alt\":true},\"node_1_2node_2\":{\"type\":\"sl\",\"from\":\"node_1_2\",\"to\":\"node_2\",\"name\":\"\",\"dash\":false,\"alt\":true},\"node_2node_2_0\":{\"type\":\"sl\",\"from\":\"node_2\",\"to\":\"node_2_0\",\"name\":\"\",\"dash\":false,\"alt\":true},\"node_2node_2_1\":{\"type\":\"sl\",\"from\":\"node_2\",\"to\":\"node_2_1\",\"name\":\"\",\"dash\":false,\"alt\":true},\"node_2node_2_2\":{\"type\":\"sl\",\"from\":\"node_2\",\"to\":\"node_2_2\",\"name\":\"\",\"dash\":false,\"alt\":true},\"node_2_0node_3\":{\"type\":\"sl\",\"from\":\"node_2_0\",\"to\":\"node_3\",\"name\":\"\",\"dash\":false,\"alt\":true},\"node_2_1node_3\":{\"type\":\"sl\",\"from\":\"node_2_1\",\"to\":\"node_3\",\"name\":\"\",\"dash\":false,\"alt\":true},\"node_2_2node_3\":{\"type\":\"sl\",\"from\":\"node_2_2\",\"to\":\"node_3\",\"name\":\"\",\"dash\":false,\"alt\":true},\"node_3node_3_0\":{\"type\":\"sl\",\"from\":\"node_3\",\"to\":\"node_3_0\",\"name\":\"\",\"dash\":false,\"alt\":true},\"node_3node_3_1\":{\"type\":\"sl\",\"from\":\"node_3\",\"to\":\"node_3_1\",\"name\":\"\",\"dash\":false,\"alt\":true}},\"areas\":{},\"initNum\":1}');
INSERT INTO `gzmtr_accident` VALUES ('9', null, '69', '27', '177', '55', '6', '66', '00:00:00', '2', '1', '66', '下午5:29:22\n7立即赶到现场\n7协助灭火\n7确认不可控制火灾\n下午5:29:23\n9通知值班站长\n9到报警点确认火灾\n9确认火灾\n下午5:29:24\n10立即携带防护用品\n10到现场\n10确认火灾\n下午5:29:25\n10撤退时负责确认所有站内人员\n8接到火警通知\n', '9', '49', '{\"title\":\"处置流程编辑\",\"nodes\":{\"start_node\":{\"name\":\"开始\",\"left\":207,\"top\":20,\"type\":\"start round mix\",\"width\":28,\"height\":28,\"alt\":true},\"node_0\":{\"name\":\"\",\"left\":195,\"top\":160,\"type\":\"fork\",\"width\":104,\"height\":28,\"alt\":true},\"node_0_0\":{\"name\":\"7立即赶到现场\",\"left\":120,\"top\":220,\"type\":\"task\",\"width\":200,\"height\":28,\"alt\":true},\"node_0_1\":{\"name\":\"7协助灭火\",\"left\":360,\"top\":220,\"type\":\"task\",\"width\":200,\"height\":28,\"alt\":true},\"node_0_2\":{\"name\":\"7确认不可控制火灾\",\"left\":600,\"top\":220,\"type\":\"task\",\"width\":200,\"height\":28,\"alt\":true},\"node_1\":{\"name\":\"\",\"left\":195,\"top\":280,\"type\":\"fork\",\"width\":104,\"height\":28,\"alt\":true},\"node_1_0\":{\"name\":\"9通知值班站长\",\"left\":120,\"top\":340,\"type\":\"task\",\"width\":200,\"height\":28,\"alt\":true},\"node_1_1\":{\"name\":\"9到报警点确认火灾\",\"left\":360,\"top\":340,\"type\":\"task\",\"width\":200,\"height\":28,\"alt\":true},\"node_1_2\":{\"name\":\"9确认火灾\",\"left\":600,\"top\":340,\"type\":\"task\",\"width\":200,\"height\":28,\"alt\":true},\"node_2\":{\"name\":\"\",\"left\":195,\"top\":400,\"type\":\"fork\",\"width\":104,\"height\":28,\"alt\":true},\"node_2_0\":{\"name\":\"10立即携带防护用品\",\"left\":120,\"top\":460,\"type\":\"task\",\"width\":200,\"height\":28,\"alt\":true},\"node_2_1\":{\"name\":\"10到现场\",\"left\":360,\"top\":460,\"type\":\"task\",\"width\":200,\"height\":28,\"alt\":true},\"node_2_2\":{\"name\":\"10确认火灾\",\"left\":600,\"top\":460,\"type\":\"task\",\"width\":200,\"height\":28,\"alt\":true},\"node_3\":{\"name\":\"\",\"left\":195,\"top\":520,\"type\":\"fork\",\"width\":104,\"height\":28,\"alt\":true},\"node_3_0\":{\"name\":\"10撤退时负责确认所有站内人员\",\"left\":120,\"top\":580,\"type\":\"task\",\"width\":200,\"height\":28,\"alt\":true},\"node_3_1\":{\"name\":\"8接到火警通知\",\"left\":360,\"top\":580,\"type\":\"task\",\"width\":200,\"height\":28,\"alt\":true}},\"lines\":{\"start_nodenode_0\":{\"type\":\"sl\",\"from\":\"start_node\",\"to\":\"node_0\",\"name\":\"\",\"dash\":false},\"node_0node_0_0\":{\"type\":\"sl\",\"from\":\"node_0\",\"to\":\"node_0_0\",\"name\":\"\",\"dash\":false},\"node_0node_0_1\":{\"type\":\"sl\",\"from\":\"node_0\",\"to\":\"node_0_1\",\"name\":\"\",\"dash\":false},\"node_0node_0_2\":{\"type\":\"sl\",\"from\":\"node_0\",\"to\":\"node_0_2\",\"name\":\"\",\"dash\":false},\"node_0_0node_1\":{\"type\":\"sl\",\"from\":\"node_0_0\",\"to\":\"node_1\",\"name\":\"\",\"dash\":false},\"node_0_1node_1\":{\"type\":\"sl\",\"from\":\"node_0_1\",\"to\":\"node_1\",\"name\":\"\",\"dash\":false},\"node_0_2node_1\":{\"type\":\"sl\",\"from\":\"node_0_2\",\"to\":\"node_1\",\"name\":\"\",\"dash\":false},\"node_1node_1_0\":{\"type\":\"sl\",\"from\":\"node_1\",\"to\":\"node_1_0\",\"name\":\"\",\"dash\":false},\"node_1node_1_1\":{\"type\":\"sl\",\"from\":\"node_1\",\"to\":\"node_1_1\",\"name\":\"\",\"dash\":false},\"node_1node_1_2\":{\"type\":\"sl\",\"from\":\"node_1\",\"to\":\"node_1_2\",\"name\":\"\",\"dash\":false},\"node_1_0node_2\":{\"type\":\"sl\",\"from\":\"node_1_0\",\"to\":\"node_2\",\"name\":\"\",\"dash\":false},\"node_1_1node_2\":{\"type\":\"sl\",\"from\":\"node_1_1\",\"to\":\"node_2\",\"name\":\"\",\"dash\":false},\"node_1_2node_2\":{\"type\":\"sl\",\"from\":\"node_1_2\",\"to\":\"node_2\",\"name\":\"\",\"dash\":false},\"node_2node_2_0\":{\"type\":\"sl\",\"from\":\"node_2\",\"to\":\"node_2_0\",\"name\":\"\",\"dash\":false},\"node_2node_2_1\":{\"type\":\"sl\",\"from\":\"node_2\",\"to\":\"node_2_1\",\"name\":\"\",\"dash\":false},\"node_2node_2_2\":{\"type\":\"sl\",\"from\":\"node_2\",\"to\":\"node_2_2\",\"name\":\"\",\"dash\":false},\"node_2_0node_3\":{\"type\":\"sl\",\"from\":\"node_2_0\",\"to\":\"node_3\",\"name\":\"\",\"dash\":false},\"node_2_1node_3\":{\"type\":\"sl\",\"from\":\"node_2_1\",\"to\":\"node_3\",\"name\":\"\",\"dash\":false},\"node_2_2node_3\":{\"type\":\"sl\",\"from\":\"node_2_2\",\"to\":\"node_3\",\"name\":\"\",\"dash\":false},\"node_3node_3_0\":{\"type\":\"sl\",\"from\":\"node_3\",\"to\":\"node_3_0\",\"name\":\"\",\"dash\":false},\"node_3node_3_1\":{\"type\":\"sl\",\"from\":\"node_3\",\"to\":\"node_3_1\",\"name\":\"\",\"dash\":false}},\"areas\":{},\"initNum\":1}');


-- ----------------------------
--  Table structure for `gzmtr_indication`
-- ----------------------------
DROP TABLE IF EXISTS `gzmtr_indication`;
CREATE TABLE `gzmtr_indication` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  `weight` float DEFAULT NULL COMMENT '权重',
  `threshold` float DEFAULT NULL COMMENT '阀值',
  `max_val` float DEFAULT NULL COMMENT '历史最大值',
  `min_val` float DEFAULT NULL COMMENT '历史最小值',
  `num` int(11) NOT NULL COMMENT '排序',
  `pid` int(11) NOT NULL COMMENT '父节点id',
  `pids` varchar(255) NOT NULL COMMENT '父节点们id',
  `level` int(11) NOT NULL,
  `big_is_better` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=111 DEFAULT CHARSET=utf8 COMMENT='评估指标';

INSERT INTO `gzmtr_indication` VALUES ('1', '车站', null, null, '1.5', '0', '1', '0', '0/', '1', '100'),
('2', '客流', '0.2', null, '1.5', '0', '1', '1', '0/1/', '2', '100'),
('3', '进站闸机负荷度', '0.17', '1', '0.2', '0', '1', '2', '0/1/2/', '3', '0'),
('4', '出站闸机负荷度', '0.17', null, '0.2', '0', '3', '2', '0/1/2/', '3', '0'),
('5', '线路', null, null, '1.5', '0', '2', '0', '0/', '1', '100'),
('6', '客流', '0.2', null, '1.5', '0', '1', '5', '0/5/', '2', '100'),
('8', '上行区间客流风险指数', '0.5', null, '1.8', '0.8', '2', '6', '0/5/6/', '3', '0'),
('9', '区域中心', null, null, '1.5', '0', '3', '0', '0/', '1', '100'),
('11', '站台客流密度', '0.17', null, '5', '1', '2', '2', '0/1/2/', '3', '0'),
('12', '楼梯拥挤度', '0.17', null, '0.01', '0', '4', '2', '0/1/2/', '3', '0'),
('13', '通道拥挤度', '0.17', null, '0.01', '0', '5', '2', '0/1/2/', '3', '0'),
('14', '自动扶梯拥挤度', '0.17', null, '0.02', '0', '6', '2', '0/1/2/', '3', '0'),
('15', '员工素质指数', '0.33', null, '27', '3', '1', '17', '0/1/17/', '3', '1'),
('17', '员工', '0.1', null, '1.5', '0', '2', '1', '0/1/', '2', '100'),
('18', '设备设施', '0.3', null, '1.5', '0', '3', '1', '0/1/', '2', '100'),
('19', '环境', '0.2', null, '1.5', '0', '4', '1', '0/1/', '2', '100'),
('20', '管理指标', '0.1', null, '1.5', '0', '5', '1', '0/1/', '2', '100'),
('21', '事故指标', '0.1', null, '1.5', '0', '6', '1', '0/1/', '2', '100'),
('22', '员工“两纪一化”指数', '0.33', null, '0.2', '0', '3', '17', '0/1/17/', '3', '0'),
('23', '员工技术水平达标指数', '0.33', null, '1', '0.8', '4', '17', '0/1/17/', '3', '1'),
('24', '信号系统安全指数', '0.1', null, '1', '0.8', '1', '18', '0/1/18/', '3', '1'),
('25', '通信系统安全指数', '0.1', null, '1', '0.8', '2', '18', '0/1/18/', '3', '1'),
('26', '自动扶梯安全指数', '0.1', null, '1', '0.8', '3', '18', '0/1/18/', '3', '1'),
('27', '给排水系统安全指数', '0.1', null, '1', '0.8', '4', '18', '0/1/18/', '3', '1'),
('28', '消防系统安全指数', '0.1', null, '1', '0.8', '5', '18', '0/1/18/', '3', '1'),
('29', '屏蔽门安全指数', '0.1', null, '1', '0.8', '6', '18', '0/1/18/', '3', '1'),
('30', '照明系统安全指数', '0.1', null, '1', '0.8', '7', '18', '0/1/18/', '3', '1'),
('31', '车站空调系统指数', '0.1', null, '1', '0.8', '8', '18', '0/1/18/', '3', '1'),
('32', 'AFC系统影响运营风险指数', '0.1', null, '0.2', '0', '9', '18', '0/1/18/', '3', '0'),
('33', '安检系统影响运营风险指数', '0.1', null, '0.2', '0', '10', '18', '0/1/18/', '3', '0'),
('34', '车站温度指数', '0.11', null, '1', '0.5', '1', '19', '0/1/19/', '3', '1'),
('35', '车站湿度指数', '0.11', null, '1', '0.5', '2', '19', '0/1/19/', '3', '1'),
('36', '车站噪声控制指数', '0.11', null, '1', '0.8', '3', '19', '0/1/19/', '3', '1'),
('37', '车站空气质量指数', '0.11', null, '1', '0.8', '4', '19', '0/1/19/', '3', '1'),
('38', '暴雨天气安全指数', '0.11', null, '1', '0', '5', '19', '0/1/19/', '3', '1'),
('39', '雷雨大风天气安全指数', '0.11', null, '1', '0', '6', '19', '0/1/19/', '3', '1'),
('40', '台风天气安全指数', '0.11', null, '1', '0', '7', '19', '0/1/19/', '3', '1'),
('41', '车站引导标识合理指数', '0.11', null, '5', '1', '8', '19', '0/1/19/', '3', '1'),
('42', '地铁保护指数', '0.11', null, '1', '0', '9', '19', '0/1/19/', '3', '1'),
('43', '路网', null, null, '1.5', '0', '4', '0', '0/', '1', '100'),
('44', '车站安全管理指数', '0.5', null, '1000', '900', '1', '20', '0/1/20/', '3', '1'),
('45', '车站应急疏散能力指数', '0.5', null, '6', '1', '2', '20', '0/1/20/', '3', '0'),
('46', '车站事故次数', '0.33', null, '1', '0', '1', '21', '0/1/21/', '3', '0'),
('47', '车站人员死亡指数', '0.33', null, '0.0001', '0', '2', '21', '0/1/21/', '3', '0'),
('48', '车站经济损失值', '0.33', null, '100', '0', '3', '21', '0/1/21/', '3', '0'),
('49', '下行区间客流风险指数', '0.5', null, '1.8', '0.8', '2', '6', '0/5/6/', '3', '0'),
('50', '员工', '0.1', null, '1.5', '0', '2', '5', '0/5/', '2', '100'),
('51', '设备设施', '0.3', null, '1.5', '0', '3', '5', '0/5/', '2', '100'),
('52', '线路车站', '0.15', null, '1.5', '0', '4', '5', '0/5/', '2', '100'),
('53', '管理', '0.1', null, '1.5', '0', '5', '5', '0/5/', '2', '100'),
('54', '事故', '0.15', null, '1.5', '0', '6', '5', '0/5/', '2', '100'),
('55', '员工素质指数', '0.33', null, '27', '3', '1', '50', '0/5/50/', '3', '1'),
('57', '员工“两纪一化”指数', '0.33', null, '0.2', '0', '3', '50', '0/5/50/', '3', '0'),
('58', '员工技术水平达标指数', '0.33', null, '1', '0.7', '4', '50', '0/5/50/', '3', '1'),
('59', '车辆系统影响运营风险指数', '0.1', null, '1.5', '1', '1', '51', '0/5/51/', '3', '0'),
('60', '信号系统影响运营风险指数', '0.1', null, '1.5', '1', '2', '51', '0/5/51/', '3', '0'),
('61', '牵引供电系统影响运营风险指数', '0.1', null, '0.2', '0', '3', '51', '0/5/51/', '3', '0'),
('62', '低压配电系统影响运营风险指数', '0.1', null, '0.2', '0', '4', '51', '0/5/51/', '3', '0'),
('63', '通信系统影响运营风险指数', '0.1', null, '1.5', '1', '5', '51', '0/5/51/', '3', '0'),
('64', '机电系统影响运营风险指数', '0.1', null, '1.5', '1', '6', '51', '0/5/51/', '3', '0'),
('65', '土建系统影响运营风险指数', '0.1', null, '1.5', '1', '7', '51', '0/5/51/', '3', '0'),
('66', '屏蔽门系统影响运营风险指数', '0.1', null, '0.2', '0', '8', '51', '0/5/51/', '3', '0'),
('67', '线路系统影响运营风险指数', '0.1', null, '0.2', '0', '9', '51', '0/5/51/', '3', '0'),
('68', '其他因素影响运营风险指数', '0.1', null, '0.2', '0', '10', '51', '0/5/51/', '3', '0'),
('69', '线路车站客流综合指数', '0.5', null, '1.5', '0', '1', '52', '0/5/52/', '3', '0'),
('70', '线路车站环境综合指数', '0.5', null, '1.5', '0', '2', '52', '0/5/52/', '3', '1'),
('71', '线路安全管理综合指数', '1', null, '1000', '900', '1', '53', '0/5/53/', '3', '1'),
('72', '线路等效事故事件率', '1', null, '0.65', '0', '1', '54', '0/5/54/', '3', '0'),
('73', '客流', '0.3', null, '1.5', '0', '1', '9', '0/9/', '2', '100'),
('74', '设备设施', '0.3', null, '1.5', '0', '2', '9', '0/9/', '2', '100'),
('75', '环境', '0.15', null, '1.5', '0', '3', '9', '0/9/', '2', '100'),
('76', '管理', '0.1', null, '1.5', '0', '4', '9', '0/9/', '2', '100'),
('77', '事故', '0.15', null, '1.5', '0', '5', '9', '0/9/', '2', '100'),
('78', '区域中心线路间能力匹配度', '1', null, '1.5', '0', '1', '73', '0/9/73/', '3', '0'),
('79', '车辆系统影响运营风险指数', '0.11', null, '1.5', '0', '1', '74', '0/9/74/', '3', '0'),
('80', '信号系统影响运营风险指数', '0.11', null, '1.5', '0', '2', '74', '0/9/74/', '3', '0'),
('81', '牵引供电系统影响运营风险指数', '0.11', null, '1.5', '0', '3', '74', '0/9/74/', '3', '0'),
('82', '低压配电系统影响运营风险指数', '0.11', null, '1.5', '0', '4', '74', '0/9/74/', '3', '0'),
('83', '通信系统影响运营风险指数', '0.11', null, '1.5', '0', '5', '74', '0/9/74/', '3', '0'),
('84', '土建系统影响运营风险指数', '0.11', null, '1.5', '0', '6', '74', '0/9/74/', '3', '0'),
('85', '线路系统影响运营风险指数', '0.11', null, '1.5', '0', '7', '74', '0/9/74/', '3', '0'),
('86', '屏蔽门系统影响运营风险指数', '0.11', null, '1.5', '0', '8', '74', '0/9/74/', '3', '0'),
('87', '其他因素影响运营风险指数', '0.11', null, '1.5', '0', '9', '74', '0/9/74/', '3', '0'),
('88', '区域中心线路环境综合指数', '1', null, '1.5', '0', '1', '75', '0/9/75/', '3', '1'),
('89', '区域中心线路安全管理综合指数', '1', null, '1000', '900', '1', '76', '0/9/76/', '3', '1'),
('90', '区域中心等效事故率', '1', null, '0.2', '0', '1', '77', '0/9/77/', '3', '0'),
('91', '客流', '0.4', null, '1.5', '0', '1', '43', '0/43/', '2', '100'),
('92', '设备设施', '0.3', null, '1.5', '0', '2', '43', '0/43/', '2', '100'),
('93', '环境', '0.1', null, '1.5', '0', '3', '43', '0/43/', '2', '100'),
('94', '管理', '0.1', null, '1.5', '0', '4', '43', '0/43/', '2', '100'),
('95', '事故', '0.1', null, '1.5', '0', '5', '43', '0/43/', '2', '100'),
('96', '路网线路间能力匹配度', '1', null, '1.5', '0', '1', '91', '0/43/91/', '3', '0'),
('97', '车辆系统影响运营风险指数', '0.11', null, '1.5', '0', '1', '92', '0/43/92/', '3', '0'),
('98', '信号系统影响运营风险指数', '0.11', null, '1.5', '0', '2', '92', '0/43/92/', '3', '0'),
('99', '牵引供电系统影响运营风险指数', '0.11', null, '1.5', '0', '3', '92', '0/43/92/', '3', '0'),
('100', '低压配电系统影响运营风险指数', '0.11', null, '1.5', '0', '4', '92', '0/43/92/', '3', '0'),
('101', '通信系统影响运营风险指数', '0.11', null, '1.5', '0', '5', '92', '0/43/92/', '3', '0'),
('102', '土建系统影响运营风险指数', '0.11', null, '1.5', '0', '6', '92', '0/43/92/', '3', '0'),
('103', '线路系统影响运营风险指数', '0.11', null, '1.5', '0', '7', '92', '0/43/92/', '3', '0'),
('104', '屏蔽门系统影响运营风险指数', '0.11', null, '1.5', '0', '8', '92', '0/43/92/', '3', '0'),
('105', '其他因素影响运营风险指数', '0.11', null, '1.5', '0', '9', '92', '0/43/92/', '3', '0'),
('106', '路网区域中心环境综合指数', '1', null, '1.5', '0', '1', '93', '0/43/93/', '3', '1'),
('107', '路网安全管理指数', '1', null, '1000', '900', '1', '94', '0/43/94/', '3', '1'),
('108', '路网等效事故率', '1', null, '0.2', '0', '1', '95', '0/43/95/', '3', '0');



-- ----------------------------
-- Table structure for gzmtr_resource_data_type
-- ----------------------------
DROP TABLE IF EXISTS `gzmtr_resource_data_type`;
CREATE TABLE `gzmtr_resource_data_type` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL COMMENT '源数据名称',
  `instruction` varchar(255) DEFAULT NULL COMMENT '说明',
  `is_fixed` char(3) NOT NULL COMMENT '是否为可修改固值',
  `indication_id` int(11) NOT NULL COMMENT '评估指标id',
  `default_value` int(11) DEFAULT NULL COMMENT '默认值',
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`)
) ENGINE=InnoDB AUTO_INCREMENT=361 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC COMMENT='源数据类型';

-- ----------------------------
-- Records of gzmtr_resource_data_type
-- ----------------------------
INSERT INTO `gzmtr_resource_data_type` VALUES ('1', '闸机进站人数', '', 'N', '3', '0'),
('2', '单向进站闸机开放个数', '', 'Y', '3', '0'),
('3', '双向闸机开放个数(进站)', '可供出站闸机负荷度使用', '', '3', null),
('4', '每台进站闸机实际最大通过能力', '人/min', '', '3', null),
('5', '闸机出站人数', '', '', '4', null),
('6', '单向出站闸机开放个数', '', '', '4', null),
('7', '每台出站闸机实际最大通过能力', '人/min', '', '4', null),
('8', '站台密集区域平均客流数量', '', '', '11', null),
('9', '站台非密集区域平均客流数量', '', '', '11', null),
('10', '有效站台宽度', '(单位)m', '', '11', null),
('11', '车门宽度', '(单位)m', '', '11', null),
('12', '列车总长度', '(单位)m', '', '11', null),
('13', '通过监测楼梯断面的客流量', '单位:人', '', '12', null),
('14', '监测楼梯瓶颈（最小）断面宽度', '', '', '12', null),
('15', '楼梯的设计通行能力', '单位:人/（秒·米）', '', '12', null),
('16', '发车间隔(楼梯拥挤度)', '', '', '12', null),
('17', '通过监测通道断面的客流量', '单位:人', '', '13', null),
('18', '监测通道瓶颈（最小）断面宽度', '单位:米', '', '13', null),
('19', '通过自动扶梯断面客流量', '单位:人', '', '14', null),
('20', '自动扶梯瓶颈（最小）断面宽度', '单位:米', '', '14', null),
('21', '自动扶梯的设计通行能力', '单位:人/（秒·台）', '', '14', null),
('22', '30岁以下人员人数', '', '', '15', null),
('23', '31岁-39岁人员人数', '', '', '15', null),
('24', '40岁以上人员人数', '', '', '15', null),
('25', '小学文化程度人员人数', '', '', '15', null),
('26', '初中文化程度人员人数', '', '', '15', null),
('27', '高中文化程度人员人数', '', '', '15', null),
('28', '本科文化程度人员人数', '', '', '15', null),
('29', '硕士及以上文化程度人员人数', '', '', '15', null),
('30', '初级技工人员人数', '', '', '15', null),
('31', '中级技工人员人数', '', '', '15', null),
('32', '高级技工人员人数', '', '', '15', null),
('33', '技师人员人数', '', '', '15', null),
('34', '高级技师人员人数', '', '', '15', null),
('37', '问题人次数', '', '', '22', null),
('38', '工作人员总数', '', '', '22', null),
('39', '技术达标人员数', '', '', '23', null),
('40', '车站信号系统故障台数', '', '', '24', null),
('41', '车站信号系统总台数', '', '', '24', null),
('42', '车站通信系统故障台数', '', '', '25', null),
('43', '车站通信系统总台数', '', '', '25', null),
('44', '车站自动扶梯的故障时间', '', '', '26', null),
('45', '车站自动扶梯的总计划服务时间', '', '', '26', null),
('46', '车站给排水系统故障台数', '', '', '27', null),
('47', '车站给排水系统总台数', '', '', '27', null),
('48', '车站消防系统设备故障数', '', '', '28', null),
('49', '车站的消防系统设备总数', '', '', '28', null),
('50', '屏蔽门报警值', '', '', '29', null),
('51', '车站屏蔽门总数', '', '', '29', null),
('52', '照明设备故障值', '', '', '30', null),
('53', '照明设备总数', '', '', '30', null),
('54', '车站空调系统设备正常工作的时间', '', '', '31', null),
('55', '车站空调系统设备计划服务总时间', '', '', '31', null),
('56', '车站AFC系统系统故障的时间', '', '', '32', null),
('57', 'AFC系统总运行时间', '', '', '32', null),
('58', '受影响所耽误的列车正线运营里程', '', '', '32', null),
('59', '列车正线计划运营里程数', '', '', '32', null),
('60', '车站中AFC系统设备发生故障的次数', '', '', '32', null),
('61', '线路列车运营公里', '', '', '32', null),
('62', '故障的时间、耽误里程、故障的次数分别权重系数', '', '', '32', null),
('63', '车站安检系统故障的时间', '', '', '33', null),
('64', '安检系统总运行时间', '', '', '33', null),
('65', '受事件影响所耽误的列车正线运营里程', 'km', '', '33', null),
('66', '车站中安检系统设备发生故障的次数', '', '', '33', null),
('67', '温度传感器实际温度测量值', '', '', '34', null),
('68', '温度传感器测量值应达到的标准温度', '', '', '34', null),
('69', '车站温度传感器所允许的最大温度差', '', '', '34', null),
('70', '湿度传感器实际湿度测量值', '', '', '35', null),
('71', '湿度传感器测量值应达到的标准湿度', '', '', '35', null),
('72', '湿度传感器所允许的最大湿度差', '', '', '35', null),
('73', '车站噪声控制达标天数', '', '', '37', null),
('74', '车站空气质量达标天数', '', '', '36', null),
('75', '车站作业总天数', '', '', '36', null),
('76', '是否为水浸黑点', '', '', '38', null),
('77', '是否有暴雨天气', '', '', '38', null),
('78', '红色暴雨应急响应时间', '', '', '38', null),
('79', '橙色暴雨应急响应时间', '', '', '38', null),
('80', '黄色暴雨应急响应时间', '', '', '38', null),
('81', '红色暴雨等级的权重', '', '', '38', null),
('82', '橙色暴雨等级的权重', '', '', '38', null),
('83', '黄色暴雨等级的权重', '', '', '38', null),
('84', '是否为雷雨大风影响黑点', '', '', '39', null),
('85', '是否有雷雨大风天气', '', '', '39', null),
('86', '红色雷雨大风应急响应时间', '', '', '39', null),
('87', '橙色雷雨大风应急响应时间', '', '', '39', null),
('88', '黄色雷雨大风应急响应时间', '', '', '39', null),
('89', '蓝色雷雨大风应急响应时间', '', '', '39', null),
('90', '红色雷雨大风等级的权重', '', '', '39', null),
('91', '橙色雷雨大风等级的权重', '', '', '39', null),
('92', '黄色雷雨大风等级的权重', '', '', '39', null),
('93', '蓝色雷雨大风等级的权重', '', '', '39', null),
('94', '是否为台风影响黑点', '', '', '40', null),
('95', '是否有台风天气', '', '', '40', null),
('96', '红色台风应急响应时间', '', '', '40', null),
('97', '橙色台风应急响应时间', '', '', '40', null),
('98', '黄色台风应急响应时间', '', '', '40', null),
('99', '蓝色台风应急响应时间', '', '', '40', null),
('100', '白色台风应急响应时间', '', '', '40', null),
('101', '红色台风等级的权重', '', '', '40', null),
('102', '橙色台风等级的权重', '', '', '40', null),
('103', '黄色台风等级的权重', '', '', '40', null),
('104', '蓝色台风等级的权重', '', '', '40', null),
('105', '白色台风等级的权重', '', '', '40', null),
('106', '专家总数', '', '', '41', null),
('107', '认为：缺少、混乱（1）打分人数', '', '', '41', null),
('108', '认为：不满意（2）打分人数', '', '', '41', null),
('109', '认为：比较满意（3）打分人数', '', '', '41', null),
('110', '认为：基本满意（4）打分人数', '', '', '41', null),
('111', '认为：满意（5）打分人数', '', '', '41', null),
('112', '特级影响项目事件的次数', '', '', '42', null),
('113', '一级影响项目事件的次数', '', '', '42', null),
('114', '二级影响项目事件的次数', '', '', '42', null),
('115', '三级影响项目事件的次数', '', '', '42', null),
('116', '四级影响项目事件的次数', '', '', '42', null),
('117', '四级以上影响项目事件的次数', '', '', '42', null),
('118', 'A类地保事件的次数', '', '', '42', null),
('119', 'B类地保事件的次数', '', '', '42', null),
('120', 'C类地保事件的次数', '', '', '42', null),
('121', 'D类地保事件的次数', '', '', '42', null),
('122', '特级影响项目事件的权重', '', '', '42', null),
('123', '一级影响项目事件的权重', '', '', '42', null),
('124', '二级影响项目事件的权重', '', '', '42', null),
('125', '三级影响项目事件的权重', '', '', '42', null),
('126', '四级影响项目事件的权重', '', '', '42', null),
('127', '四级以上影响项目事件的权重', '', '', '42', null),
('128', 'A类地保事件的权重', '', '', '42', null),
('129', 'B类地保事件的权重', '', '', '42', null),
('130', 'C类地保事件的权重', '', '', '42', null),
('131', 'D类地保事件的权重', '', '', '42', null),
('132', '该站客流量', '', '', '42', null),
('133', '安全管理指数', '', '', '44', null),
('134', '车站达标考评分数', '', '', '44', null),
('135', '车站到站列车的额定载客人数', '', '', '45', null),
('136', '到站列车满载率', '', '', '45', null),
('137', '车站站台人数', '', '', '45', null),
('138', '电扶梯的通过能力', '单位:人/（分钟·台）', '', '45', null),
('139', '人行楼梯的通过能力', '单位:人/（分钟·米）', '', '45', null),
('140', '1台电扶梯的宽度', '', '', '45', null),
('141', '人行楼梯的总宽度', '', '', '45', null),
('142', '电扶梯正常运行的台数', '', '', '45', null),
('143', '重大事故的次数', '', '', '46', null),
('144', '较大事故的次数', '', '', '46', null),
('145', '一般事故的次数', '', '', '46', null),
('146', '险性事件的次数', '', '', '46', null),
('147', '一般事件的次数', '', '', '46', null),
('148', '事件苗头的次数', '', '', '46', null),
('149', '车站死亡人数', '', '', '47', null),
('150', '车站进出站量', '', '', '47', null),
('151', '经济损失值', '', '', '48', null),
('152', '高峰小时最大断面满载率值(上行)', '', '', '8', null),
('153', '线路断面满载率平均值(上行)', '', '', '8', null),
('154', '线路中区间满载率值>70%的区间个数(上行)', '', '', '8', null),
('155', '线路区间总个数(上行)', '', '', '8', null),
('156', '线路中存在区间满载率>100%的小时个数(上行)', '', '', '8', null),
('157', '线路运营总小时数(上行)', '', '', '8', null),
('158', '高峰小时最大断面满载率风险值权重(上行)', '', '', '8', null),
('159', '满载区间比例风险值权重(上行)', '', '', '8', null),
('160', '满载时间比例风险值权重(上行)', '', '', '8', null),
('161', '高峰小时最大断面满载率值(下行)', '', '', '49', null),
('162', '线路断面满载率平均值(下行)', '', '', '49', null),
('163', '线路中区间满载率值>70%的区间个数(下行)', '', '', '49', null),
('164', '线路区间总个数(下行)', '', '', '49', null),
('165', '线路中存在区间满载率>100%的小时个数(下行)', '', '', '49', null),
('166', '线路运营总小时数(下行)', '', '', '49', null),
('167', '高峰小时最大断面满载率风险值权重(下行)', '', '', '49', null),
('168', '满载区间比例风险值权重(下行)', '', '', '49', null),
('169', '满载时间比例风险值权重(下行)', '', '', '49', null),
('170', '30岁以下人员人数(线路)', '', '', '55', null),
('171', '31岁-39岁人员人数(线路)', '', '', '55', null),
('172', '40岁以上人员人数(线路)', '', '', '55', null),
('173', '小学文化程度人员人数(线路)', '', '', '55', null),
('174', '初中文化程度人员人数(线路)', '', '', '55', null),
('175', '高中文化程度人员人数(线路)', '', '', '55', null),
('176', '本科文化程度人员人数(线路)', '', '', '55', null),
('177', '硕士及以上文化程度人员人数(线路)', '', '', '55', null),
('178', '初级技工人员人数(线路)', '', '', '55', null),
('179', '中级技工人员人数(线路)', '', '', '55', null),
('180', '高级技工人员人数(线路)', '', '', '55', null),
('181', '技师人员人数(线路)', '', '', '55', null),
('182', '高级技师人员人数(线路)', '', '', '55', null),
('185', '问题人次数(线路)', '', '', '57', null),
('186', '工作人员总数(线路)', '', '', '57', null),
('187', '技术达标人员数(线路)', '', '', '58', null),
('188', '技术人员总数(线路)', '', '', '58', null),
('189', '车辆系统故障率', '', '', '59', null),
('190', '车辆系统平均故障修复时间', '', '', '59', null),
('191', '车辆系统故障修复时间', '', '', '59', null),
('192', '车辆故障导致的影响行车故障次数', '', '', '59', null),
('193', '受影响所耽误的列车正线运营里程(线路)', '', '', '59', null),
('194', '列车正线计划运营里程数(线路)', '', '', '59', null),
('195', '信号系统故障率', '', '', '60', null),
('196', '信号系统平均故障修复时间', '', '', '60', null),
('197', '信号系统故障修复时间', '', '', '60', null),
('198', '信号故障导致的影响行车故障次数', '', '', '60', null),
('199', '受信号影响所耽误的列车正线运营里程', '', '', '60', null),
('200', '列车正线计划运营里程数(信号)', '', '', '60', null),
('201', '区间强度', '', '', '61', null),
('202', '区间供电故障的时间', '', '', '61', null),
('203', '供电系统总运行时间', '', '', '61', null),
('204', '受供电影响所耽误的列车正线运营里程', '', '', '61', null),
('205', '列车正线计划运营里程数(供电)', '', '', '61', null),
('206', '牵引供电设备故障时间权重', '', '', '61', null),
('207', '牵引供电设备故障影响范围权重', '', '', '61', null),
('208', '低压配电系统故障的时间', '', '', '62', null),
('209', '低压配电总运行时间', '', '', '62', null),
('210', '受低压配电影响所耽误的列车正线运营里程', '', '', '62', null),
('211', '列车正线计划运营里程数(低压配电)', '', '', '62', null),
('212', '线路中低压配电设备发生故障的次数', '', '', '62', null),
('213', '线路列车运营公里(低压配电)', '', '', '62', null),
('214', '低压配电设备故障率权重', '', '', '62', null),
('215', '低压配电设备故障时间权重', '', '', '62', null),
('216', '低压配电设备故障影响范围权重', '', '', '62', null),
('217', '通信系统故障率', '', '', '63', null),
('218', '通信系统平均故障修复时间', '', '', '63', null),
('219', '通信系统故障修复时间', '', '', '63', null),
('220', '通信故障导致的影响行车故障次数', '', '', '63', null),
('221', '受通信系统影响所耽误的列车正线运营里程', '', '', '63', null),
('222', '列车正线计划运营里程数(通信系统)', '', '', '63', null),
('223', '机电系统故障率', '', '', '64', null),
('224', '机电系统平均故障修复时间', '', '', '64', null),
('225', '机电系通故障修复时间', '', '', '64', null),
('226', '机电故障导致的影响行车故障次数', '', '', '64', null),
('227', '受机电影响所耽误的列车正线运营里程', '', '', '64', null),
('228', '列车正线计划运营里程数(机电)', '', '', '64', null),
('229', '土建系统故障率', '', '', '65', null),
('230', '土建系统平均故障修复时间', '', '', '65', null),
('231', '土建故障修复时间', '', '', '65', null),
('232', '土建故障导致的影响行车故障次数', '', '', '65', null),
('233', '受土建影响所耽误的列车正线运营里程', '', '', '65', null),
('234', '列车正线计划运营里程数(土建)', '', '', '65', null),
('235', '安全门故障次数', '', '', '66', null),
('236', '安全门计划正常开启次数', '', '', '66', null),
('237', '安全门总数', '', '', '66', null),
('238', '安全门的基于故障率的权重系数', '', '', '66', null),
('239', '线路中车站总数', '', '', '66', null),
('240', '区段中伤损钢轨的数量', '', '', '67', null),
('241', '区段中钢轨的总数量', '', '', '67', null),
('242', '区段轨道指数', '', '', '67', null),
('243', '出现轨道伤损的区段个数与该线路区段总个数的比值', '', '', '67', null),
('244', '线路其他因素故障的时间', '', '', '68', null),
('245', '其他因素总运行时间', '', '', '68', null),
('246', '受其他因素影响所耽误的列车正线运营里程', '', '', '68', null),
('247', '列车正线计划运营里程数(其他因素)', '', '', '68', null),
('248', '线路中其他设备发生故障的次数', '', '', '68', null),
('249', '线路列车运营公里(其他因素)', '', '', '68', null),
('250', '其他因素故障率权重', '', '', '68', null),
('251', '其他因素故障时间权重', '', '', '68', null),
('252', '其他因素故障影响范围权重', '', '', '68', null),
('253', '车站承载客流量(客流)', '', '', '69', null),
('254', '线路总客流量(客流)', '', '', '69', null),
('255', '车站承载客流量(环境)', '', '', '70', null),
('257', '线路总客流量(环境)', '', '', '70', null),
('259', '安全管理指数(线路)', '', '', '71', null),
('260', '达标考评分数(线路)', '', '', '71', null),
('261', '特别重大事故的个数', '', '', '72', null),
('262', '重大事故的个数', '', '', '72', null),
('263', '大事故的个数', '', '', '72', null),
('264', '险性事故的个数', '', '', '72', null),
('265', '一般事故的个数', '', '', '72', null),
('266', '特别重大事故的折算因子', '', '', '72', null),
('267', '重大事故的折算因子', '', '', '72', null),
('268', '大事故的折算因子', '', '', '72', null),
('269', '险性事故的折算因子', '', '', '72', null),
('270', '一般事故的折算因子', '', '', '72', null),
('271', '百万车公里', '', '', '72', null),
('272', '双向闸机开放个数(出站)', '', '', '4', null),
('273', '车门个数', '', '', '11', null),
('274', '楼梯的设计通行能力(通道)', '单位:人/（秒·米）', '', '13', null),
('275', '发车间隔(通道)', '单位:秒', '', '13', null),
('276', '发车间隔(自动扶梯)', '单位:秒', '', '14', null),
('277', '技术人员总数', '', '', '23', null),
('278', '线路列车运营公里(安检)', '', '', '33', null),
('279', '故障的时间、耽误里程、故障的次数分别权重系数(安检)', '多个值用\",\"(半角)分割', '', '33', null),
('280', '车站作业总天数(空气质量)', '', '', '37', null),
('282', '区段断面满载率', '', '', '78', null),
('283', '区段前一区段断面满载率', '', '', '78', null),
('284', '换乘站间各换乘方向换乘量占线路换乘总量的比值', '', '', '78', null),
('285', '区段断面满载率权重', '', '', '78', null),
('286', '区段前一区段断面满载率权重', '', '', '78', null),
('288', '区域中心中各线的线路强度(车辆)', '', '', '79', null),
('290', '区域中心中各线的线路强度(信号)', '', '', '80', null),
('292', '区域中心中各线的线路强度(牵引)', '', '', '81', null),
('294', '区域中心中各线的线路强度(低压配电)', '', '', '82', null),
('296', '区域中心中各线的线路强度(通信)', '', '', '83', null),
('298', '区域中心中各线的线路强度(土建)', '', '', '84', null),
('300', '区域中心中各线的线路强度', '', '', '85', null),
('302', '区域中心中各线的线路强度(屏蔽门)', '', '', '86', null),
('304', '区域中心中各线的线路强度(其他)', '', '', '87', null),
('305', '区域中心线路的数量', '', '', '88', null),
('306', '区域中心中各线的线路强度(环境)', '', '', '88', null),
('308', '安全管理指数(区域)', '', '', '89', null),
('309', '达标考评分数', '', '', '89', null),
('310', '特别重大事故的个数(区域)', '', '', '90', null),
('311', '重大事故的个数(区域)', '', '', '90', null),
('312', '大事故的个数(区域)', '', '', '90', null),
('313', '险性事故的个数(区域)', '', '', '90', null),
('314', '一般事故的个数(区域)', '', '', '90', null),
('315', '特别重大事故的折算因子(区域)', '', '', '90', null),
('316', '重大事故的折算因子(区域)', '', '', '90', null),
('317', '大事故的折算因子(区域)', '', '', '90', null),
('318', '险性事故的折算因子(区域)', '', '', '90', null),
('319', '一般事故的折算因子(区域)', '', '', '90', null),
('320', '百万车公里(区域)', '', '', '90', null),
('321', '区段断面满载率(路网)', '', '', '96', null),
('322', '区段前一区段断面满载率(路网)', '', '', '96', null),
('323', '换乘站间各换乘方向换乘量占线路换乘总量的(路网)', '', '', '96', null),
('324', '区段断面满载率权重(路网)', '', '', '96', null),
('325', '区段前一区段断面满载率权重(路网)', '', '', '96', null),
('327', '路网中各区域中心强度(车辆)', '', '', '97', null),
('329', '路网中各区域中心强度(路网)', '', '', '98', null),
('331', '路网中各区域中心强度(牵引)', '', '', '99', null),
('333', '路网中各区域中心强度(低压)', '', '', '100', null),
('335', '路网中各区域中心强度(通信)', '', '', '101', null),
('337', '路网中各区域中心强度(土建)', '', '', '102', null),
('339', '路网中各区域中心强度(线路)', '', '', '103', null),
('341', '路网中各区域中心强度(屏蔽门)', '', '', '104', null),
('343', '路网中各区域中心强度(其他)', '', '', '105', null),
('344', '路网中区域中心的数量', '', '', '106', null),
('345', '路网中各区域中心强度', '', '', '106', null),
('347', '安全管理指数(路网)', '', '', '107', null),
('348', '达标考评分数(路网)', '', '', '107', null),
('349', '特别重大事故的个数(路网)', '', '', '108', null),
('350', '重大事故的个数(路网)', '', '', '108', null),
('351', '大事故的个数(路网)', '', '', '108', null),
('352', '险性事故的个数(路网)', '', '', '108', null),
('353', '一般事故的个数(路网)', '', '', '108', null),
('354', '特别重大事故的折算因子(路网)', '', '', '108', null),
('355', '重大事故的折算因子(路网)', '', '', '108', null),
('356', '大事故的折算因子(路网)', '', '', '108', null),
('357', '险性事故的折算因子(路网)', '', '', '108', null),
('358', '一般事故的折算因子(路网)', '', '', '108', null),
('359', '百万车公里(路网)', '', '', '108', null),
('360', '列车正线计划运营里程数(安检)', '', '', '33', null);


-- ----------------------------
--  Table structure for `gzmtr_resource_data`
-- ----------------------------
DROP TABLE IF EXISTS `gzmtr_resource_data`;
CREATE TABLE `gzmtr_resource_data` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `resource_data_type_id` int(11) NOT NULL,
  `roadnet_id` int(11) NOT NULL COMMENT '数据的来源',
  `value` varchar(255) NOT NULL DEFAULT '0',
  `start_time` datetime NOT NULL,
  `end_time` datetime NOT NULL,
  `entrance` char(10) DEFAULT NULL COMMENT '地铁站口',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=61058 DEFAULT CHARSET=utf8 COMMENT='源数据';

-- ----------------------------
--  Table structure for `gzmtr_assessment`
-- ----------------------------
DROP TABLE IF EXISTS `gzmtr_assessment`;
CREATE TABLE `gzmtr_assessment` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `indication_id` int(11) unsigned NOT NULL COMMENT '评估指标id',
  `roadnet_id` int(11) unsigned NOT NULL COMMENT '路网',
  `assessment_value` double NOT NULL COMMENT '评估结果',
  `begin_time` datetime NOT NULL COMMENT '开始时间',
  `end_time` datetime NOT NULL COMMENT '结束时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='评估结果表';



-- ----------------------------
-- Table structure for `gzmtr_public_bus_hub`
-- ----------------------------
DROP TABLE IF EXISTS `gzmtr_public_bus_hub`;
CREATE TABLE `gzmtr_public_bus_hub` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL COMMENT '枢纽站名称',
  `bus_capacity` int(11) NOT NULL COMMENT '车辆定员',
  `driver_num` varchar(100) NOT NULL COMMENT '可用司机数量',
  `distance` varchar(255) NOT NULL COMMENT '距轨道车站距离',
  `load_factor` double NOT NULL COMMENT '载客率',
  `max_num` int(11) NOT NULL COMMENT '最大载客量',
  `bus_type` varchar(255) NOT NULL COMMENT '车辆类型',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COMMENT='公交枢纽站信息';

-- ----------------------------
-- Records of gzmtr_public_bus_hub
-- ----------------------------
INSERT INTO `gzmtr_public_bus_hub` VALUES ('1', '枢纽站1', '60', '1', '200米', '0.8', '80', '燃油车');
INSERT INTO `gzmtr_public_bus_hub` VALUES ('2', '枢纽站2', '60', '1', '1000米', '0.8', '80', '燃气车');
INSERT INTO `gzmtr_public_bus_hub` VALUES ('3', '枢纽站3', '60', '1', '1500米', '0.8', '80', '电车');
INSERT INTO `gzmtr_public_bus_hub` VALUES ('4', '枢纽站4', '60', '1', '500米', '0.8', '80', '燃气车');
-- ----------------------------
--  Table structure for `gzmtr_risk_factor`
-- ----------------------------
DROP TABLE IF EXISTS `gzmtr_risk_factor`;
CREATE TABLE `gzmtr_risk_factor` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL COMMENT '名称',
  `pid` int(11) NOT NULL,
  `pids` varchar(255) NOT NULL,
  `num` int(11) NOT NULL COMMENT '排序',
  `median` float DEFAULT NULL COMMENT '中值',
  `upper` float DEFAULT NULL COMMENT '上限',
  `flo` float DEFAULT NULL COMMENT '下限',
  `status` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=44 DEFAULT CHARSET=utf8 COMMENT='事故致因';

-- ----------------------------
--  Records of `gzmtr_risk_factor`
-- ----------------------------
INSERT INTO `gzmtr_risk_factor` VALUES ('4', 'X1火源', '39', '0/39/', '1', null, null, null, '0'),
  ('5', 'X4可燃物', '39', '0/39/', '2', null, null, null, '0'),
  ('6', 'X5报警设备不健全', '39', '0/39/', '3', null, null, null, '0'),
  ('7', 'X6管理不完善', '39', '0/39/', '4', null, null, null, '0'),
  ('8', 'X7灭火设施不起作用', '39', '0/39/', '5', null, null, null, '0'),
  ('9', 'X2撞击火花', '4', '0/39/4/', '1', null, null, null, '0'),
  ('10', 'X3电火源', '4', '0/39/4/', '2', null, null, null, '0'),
  ('11', 'B1人为纵火', '4', '0/39/4/', '3', '0.01', '0.005', '0.006', '0'),
  ('12', 'B2摩擦起火', '4', '0/39/4/', '4', '0.02', '0.001', '0.002', '0'),
  ('13', 'B3乘客乱扔烟头', '4', '0/39/4/', '5', '0.35', '0.005', '0.006', '0'),
  ('14', 'B5机械碰撞', '9', '0/39/4/', '1', '0.035', '0.002', '0.003', '0'),
  ('15', 'B6列车脱轨、碰撞', '9', '0/39/4/', '2', '0.035', '0.003', '0.004', '0'),
  ('16', 'B7施工作业', '9', '0/39/4/', '3', '0.05', '0.012', '0.012', '0'),
  ('17', 'B8 小动物破坏', '10', '0/39/4/', '1', '0.02', '0.003', '0.003', '0'),
  ('18', 'B9电器短路', '10', '0/39/4/', '2', '0.095', '0.012', '0.012', '0'),
  ('19', 'B10电气设备误操作', '10', '0/39/4/', '3', '0.01', '0.003', '0.002', '0'),
  ('20', 'B11电器老化', '10', '0/39/4/', '4', '0.01', '0.003', '0.003', '0'),
  ('21', 'B12列车运行时产生电弧', '10', '0/39/4/', '5', '0.05', '0.012', '0.012', '0'),
  ('22', 'B13列车车体材料及装饰材料列车', '5', '0/39/5/', '1', '0.02', '0.003', '0.003', '0'),
  ('23', 'B15隧道内可燃物，如煤气泄漏', '5', '0/39/5/', '2', '0.05', '0.012', '0.012', '0'),
  ('24', 'B14乘客携带可燃物', '5', '0/39/5/', '3', '0.035', '0.005', '0.006', '0'),
  ('25', 'B16地铁站内可燃物', '5', '0/39/5/', '4', '0.02', '0.003', '0.003', '0'),
  ('26', 'B17监控和报警设备不规范', '6', '0/39/6/', '1', '0.007', '0.003', '0.003', '0'),
  ('27', 'B18火灾监控和报警设备失灵', '6', '0/39/6/', '2', '0.15', '0.006', '0.006', '0'),
  ('28', 'B19系统迟报', '6', '0/39/6/', '3', '0.12', '0.012', '0.012', '0'),
  ('29', 'B20未及时向有关人员报告', '6', '0/39/6/', '4', '0.04', '0.003', '0.003', '0'),
  ('30', 'B20未及时向有关人员报告', '7', '0/39/7/', '1', '0.04', '0.003', '0.003', '0'),
  ('31', 'B21负责人员不重视', '7', '0/39/7/', '2', '0.008', '0.003', '0.003', '0'),
  ('32', 'B22人员离岗', '7', '0/39/7/', '3', '0.06', '0.005', '0.006', '0'),
  ('33', 'B27设备维保不善', '7', '0/39/7/', '4', '0.06', '0.005', '0.006', '0'),
  ('34', 'B28违规动火作业', '7', '0/39/7/', '5', '0.06', '0.005', '0.006', '0'),
  ('35', 'B23灭火设备数量不足', '8', '0/39/8/', '1', '0.2', '0.012', '0.012', '0'),
  ('36', 'B24灭火设备损坏', '8', '0/39/8/', '2', '0.07', '0.012', '0.012', '0'),
  ('37', 'B25不会使用', '8', '0/39/8/', '3', '0.003', '0.003', '0.003', '0'),
  ('38', 'B26惊慌失措', '8', '0/39/8/', '4', '0.07', '0.012', '0.012', '0'),
  ('39', 'T火灾事故', '0', '0/', '1', null, null, null, '1'),
  ('42', 'T XX灾', '0', '0/', '1', null, null, null, '0'),
  ('43', 'xxx 原因', '42', '0/42/', '1', '0.2', '0.12', '0.12', '0');


-- ----------------------------
--  Table structure for `gzmtr_prediction_data`
-- ----------------------------
DROP TABLE IF EXISTS `gzmtr_prediction_data`;
CREATE TABLE `gzmtr_prediction_data` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `year` int(4) NOT NULL,
  `accident_type` char(50) NOT NULL,
  `num` int(11) NOT NULL,
  `create_time` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;

-- ----------------------------
--  Records of `gzmtr_prediction_data`
-- ----------------------------
BEGIN;
INSERT INTO `gzmtr_prediction_data` VALUES
  ('1', '2009', '1', '18', '2018-05-20 10:36:12'),
  ('2', '2010', '1', '21', '2018-05-20 10:36:22'),
  ('3', '2011', '1', '15', '2018-05-20 10:36:33'),
  ('4', '2012', '1', '10', '2018-05-20 10:36:44'),
  ('5', '2013', '1', '15', '2018-05-20 10:36:54'),
  ('6', '2014', '1', '9', '2018-05-20 10:37:05'),
  ('7', '2015', '1', '6', '2018-05-20 10:37:17');
COMMIT;

/*
Navicat MySQL Data Transfer

Source Server         : aaa
Source Server Version : 50634
Source Host           : 127.0.0.1:3306
Source Database       : guns

Target Server Type    : MYSQL
Target Server Version : 50634
File Encoding         : 65001

Date: 2018-05-30 17:44:58
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for `gzmtr_emergency`
-- ----------------------------
DROP TABLE IF EXISTS `gzmtr_emergency`;
CREATE TABLE `gzmtr_emergency` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'id',
  `name` varchar(50) CHARACTER SET utf8 NOT NULL COMMENT '突发事件名称',
  `num` int(11) DEFAULT NULL COMMENT '排序',
  `pid` int(11) DEFAULT NULL,
  `pids` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of gzmtr_emergency
-- ----------------------------
INSERT INTO `gzmtr_emergency` VALUES ('2', '火灾', '1', '0', '0/');
INSERT INTO `gzmtr_emergency` VALUES ('3', '运营期', '1', '2', '0/2/');
INSERT INTO `gzmtr_emergency` VALUES ('4', '非运营期', '2', '2', '0/2/');
INSERT INTO `gzmtr_emergency` VALUES ('5', '站厅', '1', '4', '0/2/4/');
INSERT INTO `gzmtr_emergency` VALUES ('6', '站台', '2', '3', '0/2/3/');
INSERT INTO `gzmtr_emergency` VALUES ('8', '站厅', '1', '3', '0/2/3/');
INSERT INTO `gzmtr_emergency` VALUES ('9', '设备区', '3', '3', '0/2/3/');

/*
Navicat MySQL Data Transfer

Source Server         : aaa
Source Server Version : 50634
Source Host           : 127.0.0.1:3306
Source Database       : guns

Target Server Type    : MYSQL
Target Server Version : 50634
File Encoding         : 65001

Date: 2018-05-30 17:46:10
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for `gzmtr_emergencyproperty`
-- ----------------------------
DROP TABLE IF EXISTS `gzmtr_emergencyproperty`;
CREATE TABLE `gzmtr_emergencyproperty` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `emergency_id` int(11) NOT NULL COMMENT '突发事件ID',
  `ridership` varchar(50) CHARACTER SET utf8 NOT NULL COMMENT '客流',
  `traffic_capacity` varchar(50) CHARACTER SET utf8 NOT NULL COMMENT '同行能力',
  `fault` varchar(50) CHARACTER SET utf8 NOT NULL COMMENT '故障数量',
  `gas_shield` varchar(50) CHARACTER SET utf8 NOT NULL COMMENT '气体保护',
  `tunnel_length` varchar(50) CHARACTER SET utf8 NOT NULL COMMENT '隧道长度',
  `status` varchar(50) CHARACTER SET utf8 NOT NULL COMMENT '状态',
  `distance_on` varchar(50) CHARACTER SET utf8 NOT NULL COMMENT '距离前一站距离',
  `distance_next` varchar(50) CHARACTER SET utf8 NOT NULL COMMENT '距离下一站距离',
  `trapped_num` varchar(50) CHARACTER SET utf8 NOT NULL COMMENT '被困人员数量',
  `trapped_num_power` varchar(50) CHARACTER SET utf8 NOT NULL COMMENT '被困人员行动能力',
  `physical_state` varchar(50) CHARACTER SET utf8 NOT NULL COMMENT '物理状态',
  `toxicity` varchar(50) CHARACTER SET utf8 NOT NULL COMMENT '毒性',
  `bums` varchar(50) CHARACTER SET utf8 NOT NULL COMMENT '燃爆性',
  `causticity` varchar(50) CHARACTER SET utf8 NOT NULL COMMENT '腐蚀性',
  `volatileness` varchar(50) CHARACTER SET utf8 NOT NULL COMMENT '挥发性',
  `temperature` varchar(50) CHARACTER SET utf8 NOT NULL COMMENT '温度',
  `smokescope` varchar(50) CHARACTER SET utf8 NOT NULL COMMENT '烟气浓度',
  `collapse` varchar(50) CHARACTER SET utf8 NOT NULL COMMENT '坍塌',
  `lose_efficacy` varchar(50) CHARACTER SET utf8 NOT NULL COMMENT '失效',
  `reoad` varchar(50) CHARACTER SET utf8 NOT NULL COMMENT '道路状况',
  `distance_one_one_nine` varchar(50) CHARACTER SET utf8 NOT NULL COMMENT '119距离',
  `distance_one_one_zero` varchar(50) CHARACTER SET utf8 NOT NULL COMMENT '110距离',
  `distance_one_two_zero` varchar(50) CHARACTER SET utf8 NOT NULL COMMENT '120距离',
  `fire_control` varchar(50) CHARACTER SET utf8 NOT NULL COMMENT '消防设施',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of gzmtr_emergencyproperty
-- ----------------------------
INSERT INTO `gzmtr_emergencyproperty` VALUES ('5', '9', '0.0417', '0.0417', '0.0417', '0.0417', '0.0417', '0.0417', '0.0417', '0.0417', '0.0417', '0.0417', '0.0417', '0.0417', '0.0417', '0.0417', '0.0417', '0.0417', '0.0417', '0.0417', '0.0417', '0.0417', '0.0417', '0.0417', '0.0417', '0.0417');

/*
Navicat MySQL Data Transfer

Source Server         : aaa
Source Server Version : 50634
Source Host           : 127.0.0.1:3306
Source Database       : guns

Target Server Type    : MYSQL
Target Server Version : 50634
File Encoding         : 65001

Date: 2018-05-30 17:49:32
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for `gzmtr_accident_emergencyproperty`
-- ----------------------------
DROP TABLE IF EXISTS `gzmtr_accident_emergencyproperty`;
CREATE TABLE `gzmtr_accident_emergencyproperty` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `ridership` varchar(50) CHARACTER SET utf8 DEFAULT NULL COMMENT '客流',
  `traffic_capacity` varchar(50) CHARACTER SET utf8 DEFAULT NULL COMMENT '同行能力',
  `fault` varchar(50) CHARACTER SET utf8 DEFAULT NULL COMMENT '故障数量',
  `gas_shield` varchar(50) CHARACTER SET utf8 DEFAULT NULL COMMENT '气体保护',
  `tunnel_length` varchar(50) CHARACTER SET utf8 DEFAULT NULL COMMENT '隧道长度',
  `status` varchar(50) CHARACTER SET utf8 DEFAULT NULL COMMENT '状态',
  `distance_on` varchar(50) CHARACTER SET utf8 DEFAULT NULL COMMENT '距离前一站距离',
  `distance_next` varchar(50) CHARACTER SET utf8 DEFAULT NULL COMMENT '距离下一站距离',
  `trapped_num` varchar(50) CHARACTER SET utf8 DEFAULT NULL COMMENT '被困人员数量',
  `trapped_num_power` varchar(50) CHARACTER SET utf8 DEFAULT NULL COMMENT '被困人员行动能力',
  `physical_state` varchar(50) CHARACTER SET utf8 DEFAULT NULL COMMENT '物理状态',
  `toxicity` varchar(50) CHARACTER SET utf8 DEFAULT NULL COMMENT '毒性',
  `bums` varchar(50) CHARACTER SET utf8 DEFAULT NULL COMMENT '燃爆性',
  `causticity` varchar(50) CHARACTER SET utf8 DEFAULT NULL COMMENT '腐蚀性',
  `volatileness` varchar(50) CHARACTER SET utf8 DEFAULT NULL COMMENT '挥发性',
  `temperature` varchar(50) CHARACTER SET utf8 DEFAULT NULL COMMENT '温度',
  `smokescope` varchar(50) CHARACTER SET utf8 DEFAULT NULL COMMENT '烟气浓度',
  `collapse` varchar(50) CHARACTER SET utf8 DEFAULT NULL COMMENT '坍塌',
  `lose_efficacy` varchar(50) CHARACTER SET utf8 DEFAULT NULL COMMENT '失效',
  `reoad` varchar(50) CHARACTER SET utf8 DEFAULT NULL COMMENT '道路状况',
  `distance_one_one_nine` varchar(50) CHARACTER SET utf8 DEFAULT NULL COMMENT '119距离',
  `distance_one_one_zero` varchar(50) CHARACTER SET utf8 DEFAULT NULL COMMENT '110距离',
  `distance_one_two_zero` varchar(50) CHARACTER SET utf8 DEFAULT NULL COMMENT '120距离',
  `fire_control` varchar(50) CHARACTER SET utf8 DEFAULT NULL COMMENT '消防设施',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=52 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of gzmtr_accident_emergencyproperty
-- ----------------------------
INSERT INTO `gzmtr_accident_emergencyproperty` VALUES ('12', '1', '3', '5', '7', '9', '11', '13', '15', '17', '19', '23', '24', '2', '4', '6', '8', '10', '12', '14', '16', '18', '20', '21', '22');
INSERT INTO `gzmtr_accident_emergencyproperty` VALUES ('17', '1', '2', '3', '4', '', '', '', '', '', '', '', '', '6', '6', '5', '5', '', '', '', '', '', '', '', '');
INSERT INTO `gzmtr_accident_emergencyproperty` VALUES ('18', '1', '2', '3', '5', '5', '6', '', '', '', '', '', '', '', '', '4', '4', '6', '6', '', '', '', '', '', '');
INSERT INTO `gzmtr_accident_emergencyproperty` VALUES ('19', '1', '1', '1', '1', '', '', '', '', '', '', '', '', '', '', '', '1', '1', '1', '1', '', '', '', '', '');
INSERT INTO `gzmtr_accident_emergencyproperty` VALUES ('20', '1', '1', '1', '1', '', '', '', '', '', '', '', '', '', '', '', '1', '1', '1', '1', '', '', '', '', '');
INSERT INTO `gzmtr_accident_emergencyproperty` VALUES ('21', '1', '1', '1', '', '', '1', '', '1', '', '', '', '', '', '', '1', '1', '', '', '1', '', '', '', '', '');
INSERT INTO `gzmtr_accident_emergencyproperty` VALUES ('22', '1', '1', '1', '1', '1', '', '', '', '', '', '', '', '', '1', '1', '1', '1', '', '', '', '', '', '', '');
INSERT INTO `gzmtr_accident_emergencyproperty` VALUES ('23', '1', '1', '1', '1', '', '', '', '', '', '', '', '', '', '', '', '1', '1', '', '', '', '', '', '', '');
INSERT INTO `gzmtr_accident_emergencyproperty` VALUES ('24', '1', '1', '1', '1', '1', '', '', '', '', '', '', '', '', '', '1', '1', '', '', '', '', '', '', '', '');
INSERT INTO `gzmtr_accident_emergencyproperty` VALUES ('25', '1', '1', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '');
INSERT INTO `gzmtr_accident_emergencyproperty` VALUES ('26', '1', '1', '1', '1', '', '', '', '', '', '', '', '', '', '', '', '1', '1', '1', '', '', '', '', '', '');
INSERT INTO `gzmtr_accident_emergencyproperty` VALUES ('27', '1', '1', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '');
INSERT INTO `gzmtr_accident_emergencyproperty` VALUES ('28', '2', '2', '2', '2', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '');
INSERT INTO `gzmtr_accident_emergencyproperty` VALUES ('29', '1', '1', '1', '1', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '');
INSERT INTO `gzmtr_accident_emergencyproperty` VALUES ('30', '1', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '');
INSERT INTO `gzmtr_accident_emergencyproperty` VALUES ('31', '1', '1', '1', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '');
INSERT INTO `gzmtr_accident_emergencyproperty` VALUES ('32', '1', '1', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '');
INSERT INTO `gzmtr_accident_emergencyproperty` VALUES ('33', '1', '1', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '');
INSERT INTO `gzmtr_accident_emergencyproperty` VALUES ('34', '1', '1', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '');
INSERT INTO `gzmtr_accident_emergencyproperty` VALUES ('35', '1', '1', '1', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '');
INSERT INTO `gzmtr_accident_emergencyproperty` VALUES ('36', '1', '1', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '');
INSERT INTO `gzmtr_accident_emergencyproperty` VALUES ('37', '1', '1', '1', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '');
INSERT INTO `gzmtr_accident_emergencyproperty` VALUES ('38', '1', '1', '1', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '');
INSERT INTO `gzmtr_accident_emergencyproperty` VALUES ('39', '1', '1', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '');
INSERT INTO `gzmtr_accident_emergencyproperty` VALUES ('40', '1', '1', '1', '1', '', '', '', '', '', '', '', '', '', '', '', '1', '1', '', '', '', '', '', '', '');
INSERT INTO `gzmtr_accident_emergencyproperty` VALUES ('41', '3', '4', '4', '4', '', '', '', '', '', '', '', '', '', '4', '4', '', '', '', '', '', '', '', '', '');
INSERT INTO `gzmtr_accident_emergencyproperty` VALUES ('42', '4', '4', '4', '4', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '');
INSERT INTO `gzmtr_accident_emergencyproperty` VALUES ('43', '1', '1', '1', '1', '', '', '', '', '', '', '', '', '', '', '', '1', '', '', '', '', '', '', '', '1');
INSERT INTO `gzmtr_accident_emergencyproperty` VALUES ('44', '5', '5', '5', '5', '', '', '', '', '', '', '', '', '5', '5', '5', '5', '', '', '', '', '', '', '', '');
INSERT INTO `gzmtr_accident_emergencyproperty` VALUES ('45', '1', '1', '1', '', '', '', '1', '1', '', '', '', '', '', '', '', '1', '1', '', '', '', '', '', '', '');
INSERT INTO `gzmtr_accident_emergencyproperty` VALUES ('46', '0', '0', '0', '0', '0', '', '', '', '', '', '', '', '', '', '0', '0', '', '', '', '', '', '', '', '');
INSERT INTO `gzmtr_accident_emergencyproperty` VALUES ('47', '7', '', '7', '7', '', '', '', '', '', '', '', '', '', '', '7', '7', '', '', '', '', '', '', '', '');
INSERT INTO `gzmtr_accident_emergencyproperty` VALUES ('48', '4', '4', '4', '', '', '', '', '', '', '', '', '', '', '', '4', '4', '', '', '', '', '', '', '', '');
INSERT INTO `gzmtr_accident_emergencyproperty` VALUES ('49', '6', '6', '6', '', '', '', '', '', '', '', '', '', '', '', '6', '6', '', '', '', '', '', '', '', '');
INSERT INTO `gzmtr_accident_emergencyproperty` VALUES ('50', '', '1', '1', '1', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '');
INSERT INTO `gzmtr_accident_emergencyproperty` VALUES ('51', '1', '1', '1', '', '', '', '', '', '', '', '', '1', '', '', '', '', '', '', '', '', '', '', '', '1');

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for `gzmtr_alarm`
-- ----------------------------
DROP TABLE IF EXISTS `gzmtr_alarm`;
CREATE TABLE `gzmtr_alarm` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `accident_type_id` int(20) DEFAULT NULL COMMENT '事故类型',
  `accident_level` int(10) DEFAULT NULL COMMENT '响应等级',
  `line_id` int(11) DEFAULT NULL COMMENT '事故发生线路',
  `station_id` int(11) DEFAULT NULL COMMENT '事故车站',
  `train_number` varchar(10) CHARACTER SET utf8 DEFAULT NULL COMMENT '事故车次',
  `start_time` varchar(20) CHARACTER SET utf8 DEFAULT NULL COMMENT '事故发生时间',
  `reduction` varchar(10) CHARACTER SET utf8 DEFAULT NULL COMMENT '预计行车能力降低',
  `emergency_id` int(11) DEFAULT NULL COMMENT '突发事件类型',
  `trap_num` varchar(15) CHARACTER SET utf8 DEFAULT NULL COMMENT '影响范围',
  `emergencypro_id` int(11) DEFAULT NULL COMMENT '突发事件属性',
  `interruption_type` int(11) DEFAULT NULL COMMENT '中断类型',
  `retention_time` int(11) DEFAULT NULL COMMENT '乘客滞留时间',
  `interruption_time` int(11) DEFAULT NULL COMMENT '预计中断时间',
  `expect_delays` int(11) DEFAULT NULL COMMENT '预计晚点时间',
  `loss_efficiency_retention` int(11) DEFAULT NULL COMMENT '滞留客流损失系数',
  `loss_efficiency_transportation` int(11) DEFAULT NULL COMMENT '疏运客流损失系数',
  `counterplan_id` int(11) DEFAULT NULL COMMENT '预案或历史案例的id',
  `counterplan_type` varchar(20) CHARACTER SET utf8 DEFAULT NULL COMMENT '判断是预案还是历史案例',
  `process_json` longtext CHARACTER SET utf8 COMMENT '编制处置流程获取的json数据',
  `step` int(11) DEFAULT NULL COMMENT '步骤',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;



-- ----------------------------
-- Table structure for `gzmtr_position_statement`
-- ----------------------------
DROP TABLE IF EXISTS `gzmtr_position_statement`;
CREATE TABLE `gzmtr_position_statement` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'id',
  `name` varchar(50) CHARACTER SET utf8 NOT NULL COMMENT '岗位职责名称',
  `num` int(11) DEFAULT NULL COMMENT '排序',
  `pid` int(11) DEFAULT NULL,
  `pids` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=latin1;
















































