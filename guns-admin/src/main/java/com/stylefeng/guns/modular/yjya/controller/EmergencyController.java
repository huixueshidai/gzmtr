package com.stylefeng.guns.modular.yjya.controller;

import com.stylefeng.guns.core.base.controller.BaseController;
import com.stylefeng.guns.core.node.ZTreeNode;
import com.stylefeng.guns.modular.yjya.dao.EmergencyDao;
import com.stylefeng.guns.modular.yjya.warpper.EmergencyWarpper;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.beans.factory.annotation.Autowired;
import com.stylefeng.guns.core.log.LogObjectHolder;
import org.springframework.web.bind.annotation.RequestParam;
import com.stylefeng.guns.common.persistence.model.Emergency;
import com.stylefeng.guns.modular.yjya.service.IEmergencyService;

import java.util.List;
import java.util.Map;

/**
 * 控制器
 *
 * @author wangshuaikang
 * @Date 2018-05-23 11:44:11
 */
@Controller
@RequestMapping("/emergency")
public class EmergencyController extends BaseController {

    private String PREFIX = "/yjya/emergency/";

    @Autowired
    private IEmergencyService emergencyService;

    @Autowired
    private EmergencyDao energencyDao;



    /**
     * 跳转到首页
     */
    @RequestMapping("")
    public String index() {
        return PREFIX + "emergency.html";
    }

    /**
     * 获取tree列表
     */
    @RequestMapping(value = "/tree")
    @ResponseBody
    public List<ZTreeNode> tree() {
        List<ZTreeNode> tree = this.emergencyService.tree();
        return tree;
    }

    /**
     * 跳转到添加
     */
    @RequestMapping("/emergency_add")
    public String emergencyAdd() {
        return PREFIX + "emergency_add.html";
    }

    /**
     * 跳转到修改
     */
    @RequestMapping("/emergency_update/{emergencyId}")
    public String emergencyUpdate(@PathVariable Integer emergencyId, Model model) {
        Emergency emergency = emergencyService.selectById(emergencyId);
        model.addAttribute("item",emergency);
        String pName;
        if (emergency.getPid() == 0) {
            pName = "顶级";
        } else {
            pName = emergencyService.selectById(emergency.getPid()).getName();
        }
        model.addAttribute("pName", pName);
        LogObjectHolder.me().set(emergency);
        return PREFIX + "emergency_edit.html";
    }

    /**
     * 获取列表
     */
    @RequestMapping(value = "/list")
    @ResponseBody
    public Object list(String condition) {
        List<Map<String, Object>> list = this.energencyDao.list(condition);
        return new EmergencyWarpper(list).warp();
    }

    /**
     * 新增
     */
    @RequestMapping(value = "/add")
    @ResponseBody
    public Object add(Emergency emergency) {
        return emergencyService.addNode(emergency);
    }

    /**
     * 删除
     */
    @RequestMapping(value = "/delete")
    @ResponseBody
    public Object delete(@RequestParam Integer emergencyId) {
        emergencyService.deleteById(emergencyId);
        return SUCCESS_TIP;
    }

    /**
     * 修改
     */
    @RequestMapping(value = "/update")
    @ResponseBody
    public Object update(Emergency emergency) {
        emergencyService.updateNode(emergency);
        return super.SUCCESS_TIP;
    }

    /**
     * 详情
     */
    @RequestMapping(value = "/detail/{emergencyId}")
    @ResponseBody
    public Object detail(@PathVariable("emergencyId") Integer emergencyId) {
        return emergencyService.selectById(emergencyId);
    }
}
