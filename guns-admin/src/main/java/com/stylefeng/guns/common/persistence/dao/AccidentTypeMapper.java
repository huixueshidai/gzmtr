package com.stylefeng.guns.common.persistence.dao;

import com.stylefeng.guns.common.persistence.model.AccidentType;
import com.baomidou.mybatisplus.mapper.BaseMapper;
import org.apache.ibatis.annotations.Param;

import java.io.Serializable;
import java.util.List;
import java.util.Map;

/**
 * <p>
  * 事故处理措施 Mapper 接口
 * </p>
 *
 * @author 殷佳成
 * @since 2018-01-11
 */
public interface AccidentTypeMapper extends BaseMapper<AccidentType> {
    String selectNameById(Integer id);
//    List<Map<String, Object>> list(@Param("name") String name);
}