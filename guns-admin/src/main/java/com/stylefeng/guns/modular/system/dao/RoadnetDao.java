package com.stylefeng.guns.modular.system.dao;

import com.stylefeng.guns.common.persistence.model.Roadnet;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

/**
 * Created by 60132 on 2018/1/4.
 */
public interface RoadnetDao {

    Roadnet getRoadnetByName(@Param("name") String name);

    List<Map<String, Object>> getRoadnets(@Param("name") String name);

    String getNameById(Integer id);

    String getTypeById(Integer id);

    List<Roadnet> getIsUsedChildrenById(@Param("pid") Integer pid);

    Integer getIsUsedIdByName(String name);

    Integer getStationIdByNameAndLineId(@Param("name") String name,@Param("lineId") Integer lineId);

    List<Integer> getIsUsedLineId();
}
