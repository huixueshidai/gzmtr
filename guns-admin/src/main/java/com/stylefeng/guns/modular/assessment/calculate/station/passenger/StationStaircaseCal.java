package com.stylefeng.guns.modular.assessment.calculate.station.passenger;

import com.stylefeng.guns.common.persistence.model.Assessment;
import com.stylefeng.guns.core.util.DoubleUtil;
import com.stylefeng.guns.modular.assessment.calculate.ThirdIndicationCalculator;
import com.stylefeng.guns.modular.assessment.calculate.aware.StationThirdIndicationAware;
import org.springframework.stereotype.Component;

import java.util.Date;

/**
 * 楼梯拥挤度
 */
@Component
public class StationStaircaseCal extends ThirdIndicationCalculator implements StationThirdIndicationAware {


    @Override
    protected Integer getIndicationId() {
        return 12;
    }

    @Override
    public Assessment calculate(Integer stationId, Date beginDate, Date endDate) {
        //通过监测楼梯断面的客流量
        Double c = getDoubleValue(stationId, 13, beginDate,endDate);
        //
        Double d = getDoubleValue(stationId, 14, beginDate,endDate);
        Double Cmax = getDoubleValue(stationId, 15, beginDate,endDate);
        Double T = getDoubleValue(stationId, 16, beginDate,endDate);

        Double result = DoubleUtil.safeDiv(c, DoubleUtil.mul(Cmax, T, d));

        return genResult(result, stationId, beginDate,endDate);
    }
}
