package com.stylefeng.guns.modular.trapped.service;

import com.stylefeng.guns.common.persistence.model.TrappedNumber;
import com.baomidou.mybatisplus.service.IService;

/**
 * <p>
 * 被困人员录入 服务类
 * </p>
 *
 * @author xhq
 * @since 2018-09-04
 */
public interface ITrappedNumberService extends IService<TrappedNumber> {
	
}
