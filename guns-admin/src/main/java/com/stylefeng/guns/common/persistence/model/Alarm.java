package com.stylefeng.guns.common.persistence.model;

import java.io.Serializable;

import com.baomidou.mybatisplus.enums.IdType;
import java.util.Date;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableName;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

/**
 * <p>
 * 
 * </p>
 *
 * @author wsk
 * @since 2018-07-10
 */
@TableName("gzmtr_alarm")
@Setter
@Getter
public class Alarm extends Model<Alarm> {

    private static final long serialVersionUID = 1L;

	@TableId(value="id", type= IdType.AUTO)
	private Integer id;
    /**
     * 事故类型
     */
	@TableField("accident_type_id")
	private Integer accidentTypeId;
    /**
     * 响应等级
     */
	@TableField("accident_level")
	private Integer accidentLevel;
    /**
     * 事故发生线路
     */
	@TableField("line_id")
	private Integer lineId;
    /**
     * 事故车站
     */
	@TableField("station_id")
	private String stationId;

	/**
	 * 上下行（1：上行2：下行）
	 * @return
	 */
	@TableField("stream")
	public Integer stream;
    /**
     * 事故车次
     */
	@TableField("train_number")
	private String trainNumber;
    /**
     * 事故发生时间
     */
	@TableField("start_time")
	private String startTime;

    /**
     * 突发事件类型
     */
	@TableField("emergency_id")
	private Integer emergencyId;
    /**
     * 影响范围
     */
	@TableField("trap_num")
	private String trapNum;

	/**
	 * 影响人数
	 */
	@TableField("trap_peo_num")
	private Integer trapPeoNum;

	/**
	 * 伤亡人数
	 */
	@TableField("loss_peo_num")
	private Integer lossPeoNum;

	/**
	 * 突发事件属性
	 */
	@TableField("emergencypro_id")
	private Integer emergencyproId;



	/**
	 * 中断类型
	 */
	@TableField("interruption_type")
	private String interruptionType;

	/**
	 * 车站是否封闭
	 */
	@TableField("closed_station")
	private Integer closedStation;

	/**
	 * 区间疏散
	 */
	@TableField("interval_evacuation")
	private Integer intervalEvacuation;

	/**
	 * 人员伤亡
	 */
	@TableField("personal_casualty")
	private Integer personalCasualty;


	/**
	 * 列车清人
	 */
	@TableField("train_clearing_man")
	private Integer trainClearingMan;


	/**
	 * 大客流聚集
	 */
	@TableField("large_passenger")
	private Integer largePassenger;

	/**
	 * 其它是否选中
	 */
	@TableField("other_check")
	private Integer otherCheck;


	/**
	 * 运营中断是否选中
	 */
	@TableField("operationInterruption")
	private Integer operationInterruption;
	/**
	 * 其它内容
	 */
	@TableField("other")
	private String other;

	/**
	 * 已经采取的措施或处理建议
	 */
	@TableField("measures_token")
	private String measuresToken;

	/**
	 * 滞留时间
	 */
	@TableField("retention_time")
	private Integer retentionTime;

	/**
	 * 预计中断时间
	 */
	@TableField("interruption_time")
	private Integer interruptionTime;

	/**
	 * 预计晚点时间
	 */
	@TableField("expect_delays")
	private Integer expectDelays;

	/**
	 * 滞留客流损失系数
	 */
	@TableField("loss_efficiency_retention")
	private Integer lossEfficiencyRetention;

	/**
	 * 疏运客流损失系数
	 */
	@TableField("loss_efficiency_transportation")
	private Integer lossEfficiencyRransportation;

	/**
	 * 预案或历史案例id
	 */
	@TableField("counterplan_id")
	private Integer counterplanId;

	/**
	 * 判断是预案还是历史案例
	 */
	@TableField("counterplan_type")
	private String counterplanType;

	/**
	 * 编制处置流程获取的json数据
	 */
	@TableField("process_json")
	private String processJson;

	/**
	 * 步骤
	 */
	private Integer step;

	/**
	 * 调度方式
	 * @return
	 */
	@TableField("scheduling_mode")
	public Integer schedulingMode;

	/**
	 * 行车间隔
	 * @return
	 */
    @TableField("scheduling_mode_min")
	public Integer schedulingModeMin;

	/**
	 * 公交应急步骤
	 * @return
	 */
	@TableField("bus_step")
	public Integer busStep;

	/**
	 * 接驳车站数量
	 * @return
	 */
	@TableField("station_amount")
	public Integer stationAmount;

	/**
	 * 起始站
	 * @return
	 */
	@TableField("start_station")
	public String startStation;

	/**
	 * 公交应急步骤
	 * @return
	 */
	@TableField("end_station")
	public String endStation;

	/**
	 * 事故实例id
	 * @return
	 */
	@TableField("accident_id")
	public Integer accidentId;

	/**
	 * 处置阶段每步操作的步骤id
	 * @return
	 */
	@TableField("disposition_step")
	public String dispositionStep;

	/**
	 * 处置阶段每步操作的步骤文本
	 * @return
	 */
	@TableField("disposition_step_txt")
	public String dispositionStepTxt;


	/**
	 * 处用户
	 * @return
	 */
	@TableField("user_id")
	public Integer userId;

	/**
	 * 现场情况
	 * @return
	 */
	@TableField("locale")
	public String locale;



	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getAccidentTypeId() {
		return accidentTypeId;
	}

	public void setAccidentTypeId(Integer accidentTypeId) {
		this.accidentTypeId = accidentTypeId;
	}

	public Integer getAccidentLevel() {
		return accidentLevel;
	}

	public void setAccidentLevel(Integer accidentLevel) {
		this.accidentLevel = accidentLevel;
	}

	public Integer getLineId() {
		return lineId;
	}

	public void setLineId(Integer lineId) {
		this.lineId = lineId;
	}

	public String getStationId() {
		return stationId;
	}

	public void setStationId(String stationId) {
		this.stationId = stationId;
	}

	public String getTrainNumber() {
		return trainNumber;
	}

	public void setTrainNumber(String trainNumber) {
		this.trainNumber = trainNumber;
	}

	public String getStartTime() {
		return startTime;
	}

	public void setStartTime(String startTime) {
		this.startTime = startTime;
	}


	public Integer getEmergencyId() {
		return emergencyId;
	}

	public void setEmergencyId(Integer emergencyId) {
		this.emergencyId = emergencyId;
	}

	public String getTrapNum() {
		return trapNum;
	}

	public void setTrapNum(String trapNum) {
		this.trapNum = trapNum;
	}

	@Override
	protected Serializable pkVal() {
		return this.id;
	}

	@Override
	public String toString() {
		return "Alarm{" +
			"id=" + id +
			", accidentTypeId=" + accidentTypeId +
			", accidentLevel=" + accidentLevel +
			", lineId=" + lineId +
			", stationId=" + stationId +
			", trainNumber=" + trainNumber +
			", startTime=" + startTime +
			", emergencyId=" + emergencyId +
			", trapNum=" + trapNum +
			"}";
	}
}
