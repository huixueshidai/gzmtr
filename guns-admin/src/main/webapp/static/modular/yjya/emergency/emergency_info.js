/**
 * 初始化详情对话框
 */
var EmergencyInfoDlg = {
    emergencyInfoData : {},
    zTreeInstance : null,
    validateFields: {
        name: {
            validators: {
                notEmpty: {
                    message: '突发事件名称不能为空'
                }
            }
        },
        num: {
            validators: {
                notEmpty: {
                    message: '排序不能为空'
                },
                digits:{
                    message: '排序必须为数字'
                }
            }

        },
        pName: {
            validators: {
                notEmpty: {
                    message: '请选择上级'
                }
            }
        }
    }
};

/**
 * 清除数据
 */
EmergencyInfoDlg.clearData = function() {
    this.emergencyInfoData = {};
}

/**
 * 设置对话框中的数据
 *
 * @param key 数据的名称
 * @param val 数据的具体值
 */
EmergencyInfoDlg.set = function(key, val) {
    this.emergencyInfoData[key] = (typeof val == "undefined") ? $("#" + key).val() : val;
    return this;
}

/**
 * 设置对话框中的数据
 *
 * @param key 数据的名称
 * @param val 数据的具体值
 */
EmergencyInfoDlg.get = function(key) {
    return $("#" + key).val();
}

/**
 * 关闭此对话框
 */
EmergencyInfoDlg.close = function() {
    parent.layer.close(window.parent.Emergency.layerIndex);
}

/**
 * 收集数据
 */
EmergencyInfoDlg.collectData = function() {
    this
    .set('id')
    .set('name')
    .set('num')
    .set('pid')
    ;
}

/**
 * 验证数据是否为空
 */
EmergencyInfoDlg.validate = function () {
    $('#menuInForm').data("bootstrapValidator").resetForm();
    $('#menuInForm').bootstrapValidator('validate');
    return $("#menuInForm").data('bootstrapValidator').isValid();
}

/**
 * 提交添加
 */
EmergencyInfoDlg.addSubmit = function() {

    this.clearData();
    this.collectData();
    if (!this.validate()) {
        return;
    }

    //提交信息
    var ajax = new $ax(Feng.ctxPath + "/emergency/add", function(data){
        Feng.success("添加成功!");
        window.parent.Emergency.table.refresh();
        EmergencyInfoDlg.close();
    },function(data){
        Feng.error("添加失败!" + data.responseJSON.message + "!");
    });
    ajax.set(this.emergencyInfoData);
    ajax.start();
}

/**
 * 提交修改
 */
EmergencyInfoDlg.editSubmit = function() {

    this.clearData();
    this.collectData();
    if (!this.validate()) {
        return;
    }

    //提交信息
    var ajax = new $ax(Feng.ctxPath + "/emergency/update", function(data){
        Feng.success("修改成功!");
        window.parent.Emergency.table.refresh();
        EmergencyInfoDlg.close();
    },function(data){
        Feng.error("修改失败!" + data.responseJSON.message + "!");
    });
    ajax.set(this.emergencyInfoData);
    ajax.start();
}

/**
 * 点击部门ztree列表的选项时
 *
 * @param e
 * @param treeId
 * @param treeNode
 * @returns
 * <!-- 父级部门的选择框 -->
 <div id="parentEmergencyMenu" class="menuContent"
 style="display: none; position: absolute; z-index: 200;">
 <ul id="parentEmergencyMenuTree" class="ztree tree-box" style="width: 245px !important;"></ul>
 </div>
 */
EmergencyInfoDlg.onClickEmergency = function(e, treeId, treeNode) {
    $("#pName").attr("value", EmergencyInfoDlg.zTreeInstance.getSelectedVal());
    $("#pid").attr("value", treeNode.id);
    $("#parentEmergencyMenu").fadeOut("fast");
}

/**
 * 显示tree
 */
EmergencyInfoDlg.showEmergencySelectTree = function() {
    var pName = $("#pName");
    var pNameOffset = $("#pName").offset();
    $("#parentEmergencyMenu").css({
        left : pNameOffset.left + "px",
        top : pNameOffset.top + pName.outerHeight() + "px"
    }).slideDown("fast");

    $("body").bind("mousedown", onBodyDown);
}

function onBodyDown(event) {
    if (!(event.target.id == "menuBtn" || event.target.id == "parentEmergencyMenu" || $(
            event.target).parents("#parentEmergencyMenu").length > 0)) {
        EmergencyInfoDlg.hideEmergencySelectTree();
    }
}


/**
 * 隐藏选择的树
 */
EmergencyInfoDlg.hideEmergencySelectTree = function() {
    $("#parentEmergencyMenu").fadeOut("fast");
    $("body").unbind("mousedown", onBodyDown);// mousedown当鼠标按下就可以触发，不用弹起
}

$(function() {
    Feng.initValidator("menuInForm", EmergencyInfoDlg.validateFields);

    var ztree = new $ZTree("parentEmergencyMenuTree", "/emergency/tree");
    ztree.bindOnClick(EmergencyInfoDlg.onClickEmergency);
    ztree.init();
    EmergencyInfoDlg.zTreeInstance = ztree;
});
