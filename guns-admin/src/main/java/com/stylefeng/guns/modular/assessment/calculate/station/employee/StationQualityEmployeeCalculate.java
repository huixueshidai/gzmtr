package com.stylefeng.guns.modular.assessment.calculate.station.employee;

import com.stylefeng.guns.common.persistence.model.Assessment;
import com.stylefeng.guns.core.util.DoubleUtil;
import com.stylefeng.guns.modular.assessment.calculate.ThirdIndicationCalculator;
import com.stylefeng.guns.modular.assessment.calculate.aware.StationThirdIndicationAware;
import org.springframework.stereotype.Component;

import java.util.Date;

/**
 * 员工素质指数
 */
@Component
public class StationQualityEmployeeCalculate extends ThirdIndicationCalculator implements StationThirdIndicationAware {

    @Override
    protected Integer getIndicationId() {
        return 15;
    }

    @Override
    public Assessment calculate(Integer stationId, Date beginDate, Date endDate) {

        double ageScore = scoreCalculate(stationId, beginDate, endDate, 22, 24, 9, -4);
        double eduScore = scoreCalculate(stationId, beginDate, endDate, 25, 29, 1, 2);
        double skillScore = scoreCalculate(stationId, beginDate, endDate, 30, 34, 1, 2);

        double resValue = DoubleUtil.add(ageScore, eduScore, skillScore);

        return genResult(resValue, stationId, beginDate, endDate);
    }

}
