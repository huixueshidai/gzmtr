package com.stylefeng.guns.modular.assessment.calculate.station.environment;

import com.stylefeng.guns.common.persistence.model.Assessment;
import com.stylefeng.guns.core.util.DoubleUtil;
import com.stylefeng.guns.modular.assessment.calculate.ThirdIndicationCalculator;
import com.stylefeng.guns.modular.assessment.calculate.aware.StationThirdIndicationAware;
import org.springframework.stereotype.Component;

import java.util.Date;

/**
 * 雷雨大风
 */
@Component
public class StationThunderstormCal extends ThirdIndicationCalculator implements StationThirdIndicationAware {
    @Override
    protected Integer getIndicationId() {
        return 39;
    }

    @Override
    public Assessment calculate(Integer stationId, Date beginDate, Date endDate) {
        Double isBlack = getDoubleValue(stationId, 84, beginDate,endDate);
        Double isThunderstorm = getDoubleValue(stationId, 85, beginDate,endDate);
        if (isBlack == 0d || isThunderstorm == 0d) {
            return genResult(4d, stationId, beginDate,endDate);
        }

        Double red = getDoubleValue(stationId, 86, beginDate,endDate);
        Double orange = getDoubleValue(stationId, 87, beginDate,endDate);
        Double yellow = getDoubleValue(stationId, 88, beginDate,endDate);
        Double blue = getDoubleValue(stationId, 89, beginDate,endDate);

        Double redWeight = getDoubleValue(stationId, 90, beginDate,endDate);
        Double orangeWeight = getDoubleValue(stationId, 91, beginDate,endDate);
        Double yellowWeight = getDoubleValue(stationId, 92, beginDate,endDate);
        Double blueWeight = getDoubleValue(stationId, 92, beginDate,endDate);

        Double redResult = DoubleUtil.sub(1, DoubleUtil.div(DoubleUtil.mul(red, redWeight), 24));
        Double orangeResult = DoubleUtil.sub(1, DoubleUtil.div(DoubleUtil.mul(orange, orangeWeight), 24));
        Double yellowResult = DoubleUtil.sub(1, DoubleUtil.div(DoubleUtil.mul(yellow, yellowWeight), 24));
        Double blueResult = DoubleUtil.sub(1, DoubleUtil.div(DoubleUtil.mul(blue, blueWeight), 24));
        Double result = DoubleUtil.add(redResult, orangeResult, yellowResult, blueResult);

        return genResult(result, stationId, beginDate,endDate);
    }
}
