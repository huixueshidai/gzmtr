package com.stylefeng.guns.modular.data.warpper;

import com.stylefeng.guns.common.constant.factory.ConstantFactory;
import com.stylefeng.guns.core.base.warpper.BaseControllerWarpper;
import org.apache.commons.collections.MapUtils;

import java.util.Map;

public class ResourceDataWrapper extends BaseControllerWarpper {

    public ResourceDataWrapper(Object obj) {
        super(obj);
    }

    @Override
    protected void warpTheMap(Map<String, Object> map) {
        map.put("resourceDataTypeName", ConstantFactory.me().getResourceDataTypeNameById(MapUtils.getInteger(map,"resourceDataTypeId")));
        map.put("roadnetName", ConstantFactory.me().getRoadnetNameById(MapUtils.getInteger(map,"roadnetId")));
    }
}
