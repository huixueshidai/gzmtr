package com.stylefeng.guns.common.persistence.model;

import java.io.Serializable;

import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.enums.IdType;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableName;
import com.stylefeng.guns.common.plugin.entity.BaseModel;
import com.stylefeng.guns.common.plugin.entity.Treeable;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

/**
 * <p>
 * 事故处理措施
 * </p>
 *
 * @author 殷佳成
 * @since 2018-01-11
 */
@TableName("gzmtr_accident_type")
@Getter
@Setter
public class AccidentType extends BaseModel<AccidentType> implements Treeable {

    private static final long serialVersionUID = 1L;

	@TableId(value="id", type= IdType.AUTO)
	private Integer id;
    /**
     * 排序
     */
	private Integer num;
    /**
     * 父级id
     */
	private Integer pid;
    /**
     * 父级ids
     */
	private String pids;
    /**
     * 名称
     */
	private String name;
    /**
     * 编码
     */
	private String code;

	/**
	 * 全称
	 */
	@TableField("all_name")
	private String allName;


	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getNum() {
		return num;
	}

	public void setNum(Integer num) {
		this.num = num;
	}

	public Integer getPid() {
		return pid;
	}

	public void setPid(Integer pid) {
		this.pid = pid;
	}

	public String getPids() {
		return pids;
	}

	public void setPids(String pids) {
		this.pids = pids;
	}

	@Override
	public String getSeparator() {
		return "/";
	}

	@Override
	public String makeSelfAsNewParentIds() {
		return getPids() + getId() + getSeparator();
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	@Override
	protected Serializable pkVal() {
		return this.id;
	}

	@Override
	public String toString() {
		return "AccidentType{" +
			"id=" + id +
			", num=" + num +
			", pid=" + pid +
			", pids=" + pids +
			", name=" + name +
			", code=" + code +
			"}";
	}
}
