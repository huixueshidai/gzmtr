/**
 * 初始化预案模板详情对话框
 */
var CounterplanTemplateInfoDlg = {
    counterplanTemplateInfoData: {}
    ,temporaryId:null
    ,id:$("#id").val()
    , ue: null
    ,stationId:null
    , accidentTypeTreeInstance:null
    , stationTreeInstance:null
    ,zTreepositionStatementInstance:null
    , uploadInstance:null
    ,checkCount:parseInt($("#checkCount").val())      //车站树形复选框选中次数
    ,stationdIds:[]
    , validateFields: {
        name: {
            validators: {
                notEmpty: {
                    message: '预案模板名称不能为空'
                }
            }
        },
        type: {
            validators: {
                notEmpty: {
                    message: '类别不能为空'
                }
            }
        },
        level: {
            validators: {
                notEmpty: {
                    message: '响应等级不能为空'
                }
            }
        },
        accidentTypeName: {
            validators: {
                notEmpty: {
                    message: '事故类型不能为空'
                }
            }
        },
        stationSel: {
            validators: {
                notEmpty: {
                    message: '地点不能为空'
                }
            }
        }
    }
};


/**
 * 清除数据
 */
CounterplanTemplateInfoDlg.clearData = function () {
    this.counterplanTemplateInfoData = {};
}

/**
 * 设置对话框中的数据
 *
 * @param key 数据的名称
 * @param val 数据的具体值
 */
CounterplanTemplateInfoDlg.set = function (key, val) {
    this.counterplanTemplateInfoData[key] = (typeof val == "undefined") ? $("#" + key).val() : val;
    return this;
}

/**
 * 设置对话框中的数据
 *
 * @param key 数据的名称
 * @param val 数据的具体值
 */
CounterplanTemplateInfoDlg.get = function (key) {
    return $("#" + key).val();
}

/**
 * 关闭此对话框
 */
CounterplanTemplateInfoDlg.close = function () {
    if(CounterplanTemplateInfoDlg.temporaryId!=null){
        CounterplanTemplateInfoDlg.delete();
        parent.layer.close(window.parent.CounterplanTemplate.layerIndex);
    }
    else{
        parent.layer.close(window.parent.CounterplanTemplate.layerIndex);
    }

}

/**
 * 关闭此对话框
 */
CounterplanTemplateInfoDlg.closeOldInfo = function () {

    parent.layer.close(window.parent.CounterplanTemplate.layerIndex);
}

/**
 * 收集数据
 */
CounterplanTemplateInfoDlg.collectData = function () {
    this
        .set('id')
        .set('name')
        .set('type')
        .set('level')
        .set('content', this.ue.getContent())
        .set('stationId')
        .set('accidentTypeId')
        .set('accidentMain')
    ;
}

/**
 * 验证数据是否为空
 */
CounterplanTemplateInfoDlg.validate = function () {
    $('#CounterplanTypeTemplateInform').data("bootstrapValidator").resetForm();
    $('#CounterplanTypeTemplateInform').bootstrapValidator('validate');
    return $("#CounterplanTypeTemplateInform").data('bootstrapValidator').isValid();
}

/**
 * 提交添加
 */
CounterplanTemplateInfoDlg.addSubmit = function () {
    this.clearData();
    this.collectData();
    if (!this.validate()) {
        return;
    }

    //提交信息
    var ajax = new $ax(Feng.ctxPath + "/counterplanTemplate/add", function (data) {
        Feng.success("添加成功!");
        window.parent.CounterplanTemplate.table.refresh();
        CounterplanTemplateInfoDlg.close();
    }, function (data) {
        Feng.error("添加失败!" + data.responseJSON.message + "!");
    });
    ajax.set(this.counterplanTemplateInfoData);
    ajax.start();
}

/**
 * 草稿临时提交添加
 */
CounterplanTemplateInfoDlg.addSubmitTemporary = function () {
    this.clearData();
    this.collectData();
    //提交信息
    var ajax = new $ax(Feng.ctxPath + "/counterplanTemplate/add", function (data) {
        CounterplanTemplateInfoDlg.temporaryId = data.id;
    }, function (data) {

    });
    ajax.set(this.counterplanTemplateInfoData);
    ajax.start();
}

CounterplanTemplateInfoDlg.changeType = function () {
    var type = $("#type").val();
    var ajax = new $ax(Feng.ctxPath + "/counterplanTypeTemplate/getByType", function (data) {
        var content = data.content;
        var ue = CounterplanTemplateInfoDlg.ue;
        //如果富文本编辑器中无内容
        // if(!ue.hasContents()) {
        //     ue.execCommand('cleardoc');
            ue.setContent(content);
        // }
    }, function (data) {
    });
    ajax.set("type",type);
    ajax.start();
}


/**
 * 提交修改
 */
CounterplanTemplateInfoDlg.editSubmit = function () {

    this.clearData();
    this.collectData();
    if (!this.validate()) {
        return;
    }
    //提交信息
    var ajax = new $ax(Feng.ctxPath + "/counterplanTemplate/update", function (data) {
        Feng.success("修改成功!");
        window.parent.CounterplanTemplate.table.refresh();
        CounterplanTemplateInfoDlg.close();
        // CounterplanTemplateInfoDlg.closeOldInfo();
    }, function (data) {
        Feng.error("修改失败!" + data.responseJSON.message + "!");
    });
    ajax.set(this.counterplanTemplateInfoData);
    ajax.start();
}

/**
 * 草稿箱实施提交修改
 */
CounterplanTemplateInfoDlg.editSubmitTemporary = function () {

    this.clearData();
    this.collectData();

    if(CounterplanTemplateInfoDlg.temporaryId!=null){
            //提交信息
            var ajax = new $ax(Feng.ctxPath + "/counterplanTemplate/update", function (data) {

            }, function (data) {
                Feng.error("修改失败!" + data.responseJSON.message + "!");
            });
            ajax.set(this.counterplanTemplateInfoData);
            ajax.start();
    }
    else{
        return ;
    }

}

/**
 * 取消时删除实时保存的数据
 */
CounterplanTemplateInfoDlg.delete = function () {
    var ajax = new $ax(Feng.ctxPath + "/counterplanTemplate/deleteTemporary", function () {

        CounterplanTemplate.table.refresh();
    }, function (data) {

    });
    ajax.set("id",CounterplanTemplateInfoDlg.temporaryId);
    ajax.start();

};

/**
 * 预览页面修改的跳转
 */
CounterplanTemplateInfoDlg.openCounterplanTemplateDetailUpdate = function(){
    CounterplanTemplateInfoDlg.close();
    var counterplanTemplateId = $("#id").val();

    var index = window.parent.layer.open({
        type: 2,
        title: '修改预案模板',
        area: ['1200px', '600px'], //宽高
        fix: false, //不固定
        maxmin: true,
        content: Feng.ctxPath + '/counterplanTemplate/counterplanTemplate_update/' + counterplanTemplateId
    });
    window.parent.CounterplanTemplate.layerIndex = index;
    window.parent.layer.full(index);
}


CounterplanTemplateInfoDlg.showAccidentTypeSelectTree = function () {

    Feng.showInputTree("accidentTypeName", "accidentTypeTreeDiv", 15, 34);
}

CounterplanTemplateInfoDlg.AutoMatchAccidentTypeSelectTree = function(txtObj){
    if (txtObj.value.length > 0) {
        var accidentTypeTree = new $ZTree("accidentTypeTree", "/accidentType/tree");
        accidentTypeTree.bindOnClick(CounterplanTemplateInfoDlg.onClickAccidentType);
        accidentTypeTree.init();
        accidentTypeTree.init(txtObj);
        CounterplanTemplateInfoDlg.accidentTypeTreeInstance = accidentTypeTree;
        Feng.showInputTree("accidentTypeName", "accidentTypeTreeDiv", 15, 34);
    }
    else{
        var accidentTypeTree = new $ZTree("accidentTypeTree", "/accidentType/tree");
        accidentTypeTree.bindOnClick(CounterplanTemplateInfoDlg.onClickAccidentType);
        accidentTypeTree.init();
        CounterplanTemplateInfoDlg.accidentTypeTreeInstance = accidentTypeTree;
        Feng.showInputTree("accidentTypeName", "accidentTypeTreeDiv", 15, 34);
    }
}

CounterplanTemplateInfoDlg.onClickAccidentType=function (e, treeId, treeNode) {
    $("#accidentTypeName").val(CounterplanTemplateInfoDlg.accidentTypeTreeInstance.getSelectedVals());
    $("#accidentTypeId").attr("value", treeNode.id);
    $("#accidentTypeTreeDiv").fadeOut("fast");

}

CounterplanTemplateInfoDlg.showStationSelectTree = function () {
    Feng.showInputTree("stationSel", "stationTreeDiv", 15, 34);
}

CounterplanTemplateInfoDlg.AutoMatchStationSelectTree = function(txtObj){
    if (txtObj.value.length > 0) {
        var stationTree = new $ZTree("stationTree", "/roadnet/tree/"+$("#stationId").val());
        stationTree.bindOnClick(CounterplanTemplateInfoDlg.onClickDept);
        stationTree.init();
        stationTree.init(txtObj);
        CounterplanTemplateInfoDlg.stationTreeInstance = stationTree;
        Feng.showInputTree("stationSel", "stationTreeDiv", 15, 34);
    }
    else{
        var stationTree = new $ZTree("stationTree", "/roadnet/tree/"+$("#stationId").val());
        stationTree.bindOnClick(CounterplanTemplateInfoDlg.onClickDept);
        stationTree.init();
        CounterplanTemplateInfoDlg.stationTreeInstance = stationTree;
        Feng.showInputTree("stationSel", "stationTreeDiv", 15, 34);
    }
}

CounterplanTemplateInfoDlg.removeByValue = function(arr,val){
    for(var i=0;i<arr.length;i++){
        if(arr[i] == val){
            arr.splice(i, 1);
            break;
        }
    }
}

CounterplanTemplateInfoDlg.onClickDept = function (e, treeId, treeNode) {
    if(treeNode.isParent){
        Feng.info("请选择车站");
        treeNode.checked=false;
        return;
    }
    if(treeNode.checked){
        CounterplanTemplateInfoDlg.checkCount++;
        if(CounterplanTemplateInfoDlg.checkCount==1){
            $("#stationSel").val(treeNode.getPath()[2].name+":"+treeNode.name);
            CounterplanTemplateInfoDlg.stationdIds.push(treeNode.id);
        }
        else{
            $("#stationSel").val( $("#stationSel").val()+">>"+treeNode.name);
            CounterplanTemplateInfoDlg.stationdIds.push(treeNode.id);
        }
    }
    else{
        CounterplanTemplateInfoDlg.checkCount--;
        if(CounterplanTemplateInfoDlg.checkCount==0){
            $("#stationSel").val(null);
            CounterplanTemplateInfoDlg.removeByValue(CounterplanTemplateInfoDlg.stationdIds,treeNode.id.toString())
        }
        if(CounterplanTemplateInfoDlg.checkCount==1){
            $("#stationSel").val(CounterplanTemplateInfoDlg.stationTreeInstance.getSelectedValCheck(CounterplanTemplateInfoDlg.checkCount));
            CounterplanTemplateInfoDlg.removeByValue(CounterplanTemplateInfoDlg.stationdIds,treeNode.id.toString())
        }
    }
    if( CounterplanTemplateInfoDlg.stationdIds.length>0){
        console.log(CounterplanTemplateInfoDlg.stationdIds);
        var stationId = "";
        for(var i=0;i<CounterplanTemplateInfoDlg.stationdIds.length;i++){
            stationId += CounterplanTemplateInfoDlg.stationdIds[i]+",";
        }
       $("#stationId").attr("value", stationId);
    }

    if( CounterplanTemplateInfoDlg.checkCount==2){
      $("#stationTreeDiv").fadeOut("fast");
    }

}

/**
 * 点击ztree列表的选项时(岗位)
 *
 * @param e
 * @param treeId
 * @param treeNode
 * @returns
 *
 */
CounterplanTemplateInfoDlg.onClickPositionStatement = function (e, treeId, treeNode) {
    if(treeNode.checked){
        if($("#accidentMain").val()!=""){
            $("#accidentMain").val( $("#accidentMain").val()+","+treeNode.name);
        }
        else{
            $("#accidentMain").val(treeNode.name);
        }
    }
    else{
        var accidentMain = $("#accidentMain").val().split(",");
        if(treeNode.name==accidentMain[accidentMain.length-1]){
            $("#accidentMain").val($("#accidentMain").val().replace(treeNode.name,""));
        }
        else{
            $("#accidentMain").val($("#accidentMain").val().replace(treeNode.name+",",""));
        }

    }
}

/**
 * 显示tree
 */
CounterplanTemplateInfoDlg.showPositionStatementSelectTree = function () {
    Feng.showInputTree("main", "parentTreeDiv", 15, 34);
}

CounterplanTemplateInfoDlg.upload = function () {
    CounterplanTemplateInfoDlg.uploadInstance;
}

$(function () {
    CounterplanTemplateInfoDlg.ue = UE.getEditor('content', {
        autoHeightEnabled: false
        , initialFrameHeight: 320
        , elementPathEnabled:false//不显示输入框下的路径信息
    });

    var accidentTypeTree = new $ZTree("accidentTypeTree", "/accidentType/tree");
    accidentTypeTree.bindOnClick(CounterplanTemplateInfoDlg.onClickAccidentType);
    accidentTypeTree.init();
    CounterplanTemplateInfoDlg.accidentTypeTreeInstance = accidentTypeTree;

    //初始化地点车站
    // CounterplanTemplateInfoDlg.stationdIds = new Array($("#stationId").val().substr(0, $("#stationId").val().length - 1).split("/"));
    var stationTree = new $ZTree("stationTree", "/roadnet/tree/"+$("#stationId").val());
    stationTree.setSettings({
        view : {
            dblClickExpand : true,
            selectedMulti : false
        },
        data : {simpleData : {enable : true}},
        callback : {
            onClick : this.onClick,
            onDblClick:this.ondblclick
        },
        check: {
            enable: true,
            chkStyle: "checkbox",
            chkboxType: { "Y": "", "N": "" }
        },
        callback : {
            onCheck:CounterplanTemplateInfoDlg.onClickDept
        }
    });
    stationTree.Check(CounterplanTemplateInfoDlg.onClickDept);
    stationTree.init();
    CounterplanTemplateInfoDlg.stationTreeInstance = stationTree;

    //岗位tree
    var positionStatementztree = new $ZTree("parentTree", "/positionStatement/treeMain?accidentMain="+$("#accidentMain").val());
    positionStatementztree.setSettings({
        view : {
            dblClickExpand : true,
            selectedMulti : false
        },
        data : {simpleData : {enable : true}},
        callback : {
            onClick : this.onClick,
            onDblClick:this.ondblclick
        },
        check: {
            enable: true,
            chkStyle: "checkbox",
            chkboxType: { "Y": "", "N": "" }
        },
        callback : {
            onCheck:CounterplanTemplateInfoDlg.onClickPositionStatement
        }
    });
    positionStatementztree.Check(CounterplanTemplateInfoDlg.onClickPositionStatement);
    positionStatementztree.init();
    CounterplanTemplateInfoDlg.zTreepositionStatementInstance = positionStatementztree;

    // $("#accidentMain").comboSelect();  //todo 9022修改注释

    Feng.initValidator("CounterplanTypeTemplateInform", CounterplanTemplateInfoDlg.validateFields);

    var upload = new $WebUpload({
        uploadId:"upload",
        mimeTypes:"application/msword,application/vnd.openxmlformats-officedocument.wordprocessingml.document",
        extensions:"doc,docx",
        url:"/counterplanTemplate/counterplateTemplateExport",
        multiple:true,
    });
    upload.init();
    upload.successCallBack = function(file,response){
        var ue = CounterplanTemplateInfoDlg.ue;
        ue.setContent(response);

    }
    CounterplanTemplateInfoDlg.uploadInstance = upload;

    //实时保存数据
    // CounterplanTemplateInfoDlg.ue.addListener('ready',function(){
    //     CounterplanTemplateInfoDlg.addSubmitTemporary();
    //
    //     setInterval(function() {CounterplanTemplateInfoDlg.editSubmitTemporary()},3000);
    // });


});
