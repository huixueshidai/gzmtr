package com.stylefeng.guns.modular.prediction.controller;

import com.stylefeng.guns.modular.prediction.service.impl.FishBoneService;
import com.stylefeng.guns.modular.prediction.transfer.FishBoneDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequestMapping("fishbone")
public class FishboneController {

	private String PREFIX = "/prediction/fishbone/";

	@Autowired
	private FishBoneService fishBoneService;

	@RequestMapping("")
	public String fishbone() {
		return PREFIX + "fishbone.html";
	}

	@RequestMapping("data")
	@ResponseBody
	public FishBoneDto fishboneData() {
		return fishBoneService.fishBone();
	}
}
