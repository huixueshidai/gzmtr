package com.stylefeng.guns.common.persistence.model;

import java.io.Serializable;

import com.baomidou.mybatisplus.enums.IdType;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableName;
import com.stylefeng.guns.common.plugin.entity.BaseModel;
import com.stylefeng.guns.common.plugin.entity.Treeable;
import lombok.ToString;

import java.io.Serializable;

/**
 * <p>
 * 
 * </p>
 *
 * @author wangshuaikang
 * @since 2018-05-23
 */
@TableName("gzmtr_emergency")
@ToString
public class Emergency extends BaseModel<Emergency> implements Treeable {

    private static final long serialVersionUID = 1L;

    /**
     * 突发事件名称
     */
	@TableField("name")
	private String name;
    /**
     * 排序
     */
	private Integer num;
	private Integer pid;
	private String pids;


	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Integer getNum() {
		return num;
	}

	public void setNum(Integer num) {
		this.num = num;
	}

	public Integer getPid() {
		return pid;
	}

	public void setPid(Integer pid) {
		this.pid = pid;
	}

	public String getPids() {
		return pids;
	}

	public void setPids(String pids) {
		this.pids = pids;
	}

	@Override
	public String makeSelfAsNewParentIds() {
		return getPids() + getId() + getSeparator();
	}

	@Override
	public String getSeparator() {
		return "/";
	}


}
