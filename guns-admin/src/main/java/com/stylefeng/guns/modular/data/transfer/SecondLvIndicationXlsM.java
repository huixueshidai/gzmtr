package com.stylefeng.guns.modular.data.transfer;


import cn.afterturn.easypoi.excel.annotation.Excel;
import cn.afterturn.easypoi.excel.annotation.ExcelCollection;
import cn.afterturn.easypoi.excel.annotation.ExcelIgnore;
import lombok.Getter;
import lombok.Setter;

import java.util.List;

/**
 * 二级指标Excel导出类
 */
@Getter
@Setter
public class SecondLvIndicationXlsM {

    @ExcelIgnore
    private Integer id;

    @Excel(name = "2级指标", mergeVertical = true )
    private String name;

    @ExcelCollection(name = "3级指标列表")
    private List<ThirdLvIndicationXlsM> thirdLvIndicationXlsMS;

    public SecondLvIndicationXlsM() {
    }

    public SecondLvIndicationXlsM(String name) {
        this.name = name;
    }

    public SecondLvIndicationXlsM(String name, List<ThirdLvIndicationXlsM> thirdLvIndicationXlsMS) {
        this.name = name;
        this.thirdLvIndicationXlsMS = thirdLvIndicationXlsMS;
    }
}
