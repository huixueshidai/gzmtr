package com.stylefeng.guns.modular.assessment.calculate.line.station;


import com.stylefeng.guns.common.persistence.model.Assessment;
import com.stylefeng.guns.common.persistence.model.Roadnet;
import com.stylefeng.guns.core.util.DoubleUtil;
import com.stylefeng.guns.modular.assessment.calculate.ThirdIndicationCalculator;
import com.stylefeng.guns.modular.assessment.calculate.aware.LineThirdIndicationAware;
import com.stylefeng.guns.modular.assessment.calculate.station.environment.StationEnvironmentCal;
import com.stylefeng.guns.modular.system.dao.RoadnetDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Date;
import java.util.List;

/**
 * 线路环境指数
 */
@Component
public class LineEnvironmentCal extends ThirdIndicationCalculator implements LineThirdIndicationAware {

    @Autowired
    private StationEnvironmentCal stationEnvironmentCal;

    @Autowired
    private RoadnetDao roadnetDao;


    @Override
    protected Integer getIndicationId() {
        return 70;
    }

    @Override
    public Assessment calculate(Integer lineId, final Date beginDate, Date endDate) {
        List<Double> list = getMulDoubleValue(lineId, 255, beginDate,endDate);
        Double keLiuLiang = getDoubleValue(lineId, 257, beginDate,endDate);
        Double resVal = 0D;
        for (int i = 0; i < list.size(); i++) {
            resVal = DoubleUtil.safeDiv(list.get(i), keLiuLiang);
        }
        Double xH2 = 0D;
        List<Roadnet> stationList = roadnetDao.getIsUsedChildrenById(lineId);
        for (int i = 0; i < stationList.size(); i++) {
            Double stationEnviromentSecondIndicationValue = stationEnvironmentCal.get(
                    stationList.get(i).getId(), beginDate,endDate).getAssessmentValue();

            xH2 += DoubleUtil.mul(resVal, stationEnviromentSecondIndicationValue);
        }

        return genResult(xH2, lineId, beginDate,endDate);
    }

}

