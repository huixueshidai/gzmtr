package com.stylefeng.guns.modular.assessment.calculate.station.facility;

import com.stylefeng.guns.common.persistence.model.Assessment;
import com.stylefeng.guns.core.util.DoubleUtil;
import com.stylefeng.guns.modular.assessment.calculate.ThirdIndicationCalculator;
import com.stylefeng.guns.modular.assessment.calculate.aware.StationThirdIndicationAware;
import org.springframework.stereotype.Component;

import java.util.Date;

/**
 * 车站空调系统指数
 */
@Component
public class StationKongTiaoCal extends ThirdIndicationCalculator implements StationThirdIndicationAware {

    @Override
    protected Integer getIndicationId() {
        return 31;
    }

    @Override
    public Assessment calculate(Integer stationId, Date beginDate, Date endDate) {
        Double num = this.getDoubleValue(stationId, 54, beginDate,endDate);
        Double total = this.getDoubleValue(stationId, 55, beginDate,endDate);
        Double resVal = DoubleUtil.safeDiv(num, total);
        return genResult(resVal, stationId, beginDate,endDate);
    }
}
