package com.stylefeng.guns.modular.assessment.calculate.line.station;

import com.stylefeng.guns.core.util.SpringContextHolder;
import com.stylefeng.guns.modular.assessment.calculate.SecondIndicationCalculator;
import com.stylefeng.guns.modular.assessment.calculate.ThirdIndicationCalculator;
import com.stylefeng.guns.modular.assessment.calculate.aware.LineSecondIndicationAware;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import java.util.Arrays;
import java.util.List;

/**
 * @author YinJiaCheng
 * @Description: 三级指标
 */
@Component
@Scope(ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public class LineStationCal extends SecondIndicationCalculator implements LineSecondIndicationAware {
    private List<ThirdIndicationCalculator> thirdIndicationCalculatorList;

    public LineStationCal() {
        thirdIndicationCalculatorList = Arrays.asList(
                SpringContextHolder.getBean(LineEnvironmentCal.class),
                SpringContextHolder.getBean(LineStationPassengerFlowCal.class)
        );
    }

    @Override
    public List<ThirdIndicationCalculator> getThirdIndicationCalculatorList() {
        return thirdIndicationCalculatorList;
    }


    @Override
    protected Integer getIndicationId() {
        return 52;
    }
}
