package com.stylefeng.guns.modular.yjya.warpper;

import com.stylefeng.guns.common.constant.factory.ConstantFactory;
import com.stylefeng.guns.core.base.warpper.BaseControllerWarpper;
import org.apache.commons.collections.MapUtils;

import java.util.Map;

public class AccidentHistoryInformationWrapper extends BaseControllerWarpper {
    public AccidentHistoryInformationWrapper(Object o ){
        super(o);
    }

    @Override
    protected void warpTheMap(Map<String, Object> map) {
        map.put("rankName",ConstantFactory.me().getDictNameByCode("rank",MapUtils.getString(map,"rankCode")));
        map.put("areaName",ConstantFactory.me().getDictNameByCode("accidentArea",MapUtils.getString(map,"areaCode")));
        map.put("roadnetName", ConstantFactory.me().getRoadnetNameById(MapUtils.getInteger(map, "roadnetId")));
        map.put("categoryName",ConstantFactory.me().getDictNameByCode("categoryCode",MapUtils.getString(map,"category")));
        map.put("causeName",ConstantFactory.me().getDictNameByCode("causeCode",MapUtils.getString(map,"cause")));
    }

}
