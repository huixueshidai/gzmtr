package com.stylefeng.guns.modular.assessment.calculate.station.environment;

import com.stylefeng.guns.common.persistence.model.Assessment;
import com.stylefeng.guns.core.util.DoubleUtil;
import com.stylefeng.guns.modular.assessment.calculate.ThirdIndicationCalculator;
import com.stylefeng.guns.modular.assessment.calculate.aware.StationThirdIndicationAware;
import org.springframework.stereotype.Component;

import java.util.Date;

/**
 * 暴雨天气安全指数
 */
@Component
public class
StationRainstormCal extends ThirdIndicationCalculator implements StationThirdIndicationAware {

    @Override
    protected Integer getIndicationId() {
        return 38;
    }

    @Override
    public Assessment calculate(Integer stationId, Date beginDate, Date endDate) {
        Double isBlack = getDoubleValue(stationId, 76, beginDate, endDate);
        Double isRainstorm = getDoubleValue(stationId, 77, beginDate, endDate);
        if (isBlack == 0d || isRainstorm == 0d) {
            return genResult(3d, stationId, beginDate, endDate);
        }

        Double red = getDoubleValue(stationId, 78, beginDate, endDate);
        Double orange = getDoubleValue(stationId, 79, beginDate, endDate);
        Double yellow = getDoubleValue(stationId, 80, beginDate, endDate);

        Double redWeight = getDoubleValue(stationId, 81, beginDate, endDate);
        Double orangeWeight = getDoubleValue(stationId, 82, beginDate, endDate);
        Double yellowWeight = getDoubleValue(stationId, 83, beginDate, endDate);

        Double redResult = DoubleUtil.sub(1, DoubleUtil.div(DoubleUtil.mul(red, redWeight), 24));
        Double orangeResult = DoubleUtil.sub(1, DoubleUtil.div(DoubleUtil.mul(orange, orangeWeight), 24));
        Double yellowResult = DoubleUtil.sub(1, DoubleUtil.div(DoubleUtil.mul(yellow, yellowWeight), 24));
        Double result = DoubleUtil.add(redResult, orangeResult, yellowResult);

        return genResult(result, stationId, beginDate, endDate);
    }

}
