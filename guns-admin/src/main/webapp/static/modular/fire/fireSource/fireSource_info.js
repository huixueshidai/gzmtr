/**
 * 初始化火灾情景路径选择详情对话框
 */
var FireSourceInfoDlg = {
    fireSourceInfoData: {}
};

/**
 * 清除数据
 */
FireSourceInfoDlg.clearData = function () {
    this.fireSourceInfoData = {};
}

/**
 * 设置对话框中的数据
 *
 * @param key 数据的名称
 * @param val 数据的具体值
 */
FireSourceInfoDlg.set = function (key, val) {
    this.fireSourceInfoData[key] = (typeof val == "undefined") ? $("#" + key).val() : val;
    return this;
}

/**
 * 设置对话框中的数据
 *
 * @param key 数据的名称
 * @param val 数据的具体值
 */
FireSourceInfoDlg.get = function (key) {
    return $("#" + key).val();
}

/**
 * 关闭此对话框
 */
FireSourceInfoDlg.close = function () {
    parent.layer.close(window.parent.FireSource.layerIndex);
}

/**
 * 收集数据
 */
FireSourceInfoDlg.collectData = function () {
    this
        .set('id')
        .set('material')
        .set('field')
        .set('rescuers')
        .set('expert1')
        .set('expert2')
        .set('expert3')
        .set('expert1T')
        .set('expert1F')
        .set('expert2T')
        .set('expert2F')
        .set('expert3T')
        .set('expert3F')
    ;
}

/**
 * 提交添加
 */
FireSourceInfoDlg.addSubmit = function () {

    this.clearData();
    this.collectData();

    //提交信息
    var ajax = new $ax(Feng.ctxPath + "/fireSource/add", function (data) {
        Feng.success("添加成功!");
        window.parent.FireSource.table.refresh();
        FireSourceInfoDlg.close();
    }, function (data) {
        Feng.error("添加失败!" + data.responseJSON.message + "!");
    });
    ajax.set(this.fireSourceInfoData);
    ajax.start();
}

/**
 * 提交修改
 */
FireSourceInfoDlg.editSubmit = function () {

    this.clearData();
    this.collectData();

    //提交信息
    var ajax = new $ax(Feng.ctxPath + "/fireSource/update", function (data) {
        Feng.success("修改成功!");
        window.parent.FireSource.table.refresh();
        FireSourceInfoDlg.close();
    }, function (data) {
        Feng.error("修改失败!" + data.responseJSON.message + "!");
    });
    ajax.set(this.fireSourceInfoData);
    ajax.start();
}

$(function () {

});
