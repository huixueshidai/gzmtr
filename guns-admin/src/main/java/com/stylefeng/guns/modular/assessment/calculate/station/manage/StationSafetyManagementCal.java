package com.stylefeng.guns.modular.assessment.calculate.station.manage;

import com.stylefeng.guns.common.persistence.model.Assessment;
import com.stylefeng.guns.modular.assessment.calculate.ThirdIndicationCalculator;
import com.stylefeng.guns.modular.assessment.calculate.aware.StationThirdIndicationAware;
import org.springframework.stereotype.Component;

import java.util.Date;

/**
 * 车站安全管理指数
 */
@Component
public class StationSafetyManagementCal extends ThirdIndicationCalculator implements StationThirdIndicationAware {

    @Override
    protected Integer getIndicationId() {
        return 44;
    }

    @Override
    public Assessment calculate(Integer stationId, Date beginDate, Date endDate) {
        Double resVal = getDoubleValue(stationId, 134, beginDate,endDate);
        return genResult(resVal, stationId, beginDate,endDate);
    }
}
