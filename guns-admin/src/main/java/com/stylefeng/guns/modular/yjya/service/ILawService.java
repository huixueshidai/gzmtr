package com.stylefeng.guns.modular.yjya.service;

import com.stylefeng.guns.common.persistence.model.Law;
import com.baomidou.mybatisplus.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author 殷佳成
 * @since 2018-01-18
 */
public interface ILawService extends IService<Law> {
	
}
