package com.stylefeng.guns.modular.data.service;

import com.baomidou.mybatisplus.service.IService;
import com.stylefeng.guns.common.persistence.model.ResourceData;
import com.stylefeng.guns.modular.data.transfer.ResourceDataXlsM;
import com.stylefeng.guns.modular.data.transfer.ThirdLvIndicationXlsM;

import java.util.Date;
import java.util.List;

/**
 * <p>
 * 源数据 服务类
 * </p>
 *
 * @author xuziyang
 * @since 2018-01-17
 */
public interface IResourceDataService extends IService<ResourceData> {

    List<ThirdLvIndicationXlsM> selectThirdLVIndicationXlsMList(Integer pids);

    boolean insertDtoBatch(List<ResourceDataXlsM> list);

    ResourceData selectUnique(Integer roadnetId, Integer resourceDataTypeId,Date beginDate,Date endDate);

	List<ResourceData> selectMore(Integer roadnetId, Integer resourceDataTypeId, Date beginDate, Date endDate);
}
