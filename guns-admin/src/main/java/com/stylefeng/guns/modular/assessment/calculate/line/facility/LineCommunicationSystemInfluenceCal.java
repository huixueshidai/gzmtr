package com.stylefeng.guns.modular.assessment.calculate.line.facility;

import com.stylefeng.guns.common.persistence.model.Assessment;
import com.stylefeng.guns.core.util.DoubleUtil;
import com.stylefeng.guns.modular.assessment.calculate.ThirdIndicationCalculator;
import com.stylefeng.guns.modular.assessment.calculate.aware.LineThirdIndicationAware;
import org.springframework.stereotype.Component;

import java.util.Date;

/**
 * 通信系统影响运营风险指数
 *
 * @author yinjc
 * @since 2018/05/09
 */
@Component
public class LineCommunicationSystemInfluenceCal extends ThirdIndicationCalculator implements LineThirdIndicationAware {

    @Override
    protected Integer getIndicationId() {
        return 63;
    }

    @Override
    public Assessment calculate(Integer lineId, Date beginDate, Date endDate) {
        //故障率
        Double fr = getDoubleValue(lineId, 217, beginDate,endDate);

        //通信系统平均故障修复时间
        Double avgFixDuration = getDoubleValue(lineId, 218, beginDate,endDate);

        //受通信系统影响所耽误的列车正线运营里程
        Double faultMileage = getDoubleValue(lineId, 221, beginDate,endDate);

        //列车正线计划运营里程数(通信系统)
        Double totalMileage = getDoubleValue(lineId, 222, beginDate,endDate);


        double m = DoubleUtil.sub(totalMileage, faultMileage);
        double M = DoubleUtil.safeDiv(m, totalMileage);

        double zhiShu = DoubleUtil.add(
                fr,
                M,
                DoubleUtil.safeDiv(1, avgFixDuration));
        return genResult(zhiShu, lineId, beginDate,endDate);
    }
}
