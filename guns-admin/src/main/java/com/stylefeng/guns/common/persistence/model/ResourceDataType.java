package com.stylefeng.guns.common.persistence.model;

import java.io.Serializable;

import com.baomidou.mybatisplus.enums.IdType;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableName;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

/**
 * <p>
 * 源数据类型
 * </p>
 *
 * @author xuziyang
 * @since 2018-01-15
 */
@TableName("gzmtr_resource_data_type")
@ToString
@Getter
@Setter
public class ResourceDataType extends Model<ResourceDataType> {

    private static final long serialVersionUID = 1L;

	@TableId(value="id", type= IdType.AUTO)
	private Integer id;
    /**
     * 源数据名称
     */
	private String name;
    /**
     * 说明
     */
	private String instruction;
    /**
     * 是否为可修改固值
     */
	@TableField("is_fixed")
	private String isFixed;

	/**
	 * 是否需要填写进站口
	 */
	@TableField("need_entrance")
	private boolean needEntrance;

    /**
     * 评估指标id
     */
	@TableField("indication_id")
	private Integer indicationId;

	@TableField("default_value")
	private Integer defaultValue;


	@Override
	protected Serializable pkVal() {
		return this.id;
	}

}
