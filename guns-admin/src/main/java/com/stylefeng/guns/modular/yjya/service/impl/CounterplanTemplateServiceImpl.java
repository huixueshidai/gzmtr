package com.stylefeng.guns.modular.yjya.service.impl;

import com.stylefeng.guns.common.persistence.model.CounterplanTemplate;
import com.stylefeng.guns.common.persistence.dao.CounterplanTemplateMapper;
import com.stylefeng.guns.modular.yjya.service.ICounterplanTemplateService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 预案模板 服务实现类
 * </p>
 *
 * @author xuziyang
 * @since 2018-01-04
 */
@Service
public class CounterplanTemplateServiceImpl extends ServiceImpl<CounterplanTemplateMapper, CounterplanTemplate> implements ICounterplanTemplateService {
	
}
