package com.stylefeng.guns.modular.yjya.service.impl;

import com.stylefeng.guns.common.persistence.model.Alarm;
import com.stylefeng.guns.common.persistence.dao.AlarmMapper;
import com.stylefeng.guns.modular.yjya.service.IAlarmService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author wsk
 * @since 2018-07-10
 */
@Service
public class AlarmServiceImpl extends ServiceImpl<AlarmMapper, Alarm> implements IAlarmService {
	
}
