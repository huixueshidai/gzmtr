/**
 * 初始化评价指标详情对话框
 */
var IndicationInfoDlg = {
    indicationInfoData: {}
    , validateFields: {
        name: {
            validators: {
                notEmpty: {
                    message: '指标名称不能为空'
                }
            }
        },

        weight: {
    validators: {
        notEmpty: {
            message: '权重值不能为空'
        },
        between: {
            min: 0,
            max: 1,
            message: '权重值必须0-1'
        },
        stringLength:{
            mix:0,
            max:4,
            message:"最多两位小数"
        }
    }
},
        num: {
            validators: {
                notEmpty: {
                    message: '排序不能为空'
                },
                digits: {
                    message: '排序必须为整型'
                }
            }
        },
        pName: {
            validators: {
                notEmpty: {
                    message: '请选择上一级'
                }
            }
        },
    }
};

/**
 * 清除数据
 */
IndicationInfoDlg.clearData = function () {
    this.indicationInfoData = {};
}

/**
 * 设置对话框中的数据
 *
 * @param key 数据的名称
 * @param val 数据的具体值
 */
IndicationInfoDlg.set = function (key, val) {
    this.indicationInfoData[key] = (typeof val == "undefined") ? $("#" + key).val() : val;
    return this;
}

/**
 * 设置对话框中的数据
 *
 * @param key 数据的名称
 * @param val 数据的具体值
 */
IndicationInfoDlg.get = function (key) {
    return $("#" + key).val();
}

/**
 * 关闭此对话框
 */
IndicationInfoDlg.close = function () {
    parent.layer.close(window.parent.Indication.layerIndex);
}

/**
 * 收集数据
 */
IndicationInfoDlg.collectData = function () {
    this
        .set('id')
        .set('name')
        .set('weight')
        .set('threshold')
        .set('maxVal')
        .set('minVal')
        .set('num')
        .set('pid')
        .set("bigIsBetter")
    ;
}

/**
 * 提交添加
 */
IndicationInfoDlg.addSubmit = function () {

    this.clearData();
    this.collectData();

    if (!this.validate()) {
        return;
    }

    //提交信息
    var ajax = new $ax(Feng.ctxPath + "/indication/add", function (data) {
        Feng.success("添加成功!");
        window.parent.Indication.table.refresh();
        IndicationInfoDlg.close();
    }, function (data) {
        Feng.error("添加失败!" + data.responseJSON.message + "!");
    });
    ajax.set(this.indicationInfoData);
    ajax.start();
}

/**
 * 提交修改
 */
IndicationInfoDlg.editSubmit = function () {

    this.clearData();
    this.collectData();

    //提交信息
    var ajax = new $ax(Feng.ctxPath + "/indication/update", function (data) {
        Feng.success("修改成功!");
        window.parent.Indication.table.refresh();
        IndicationInfoDlg.close();
    }, function (data) {
        Feng.error("修改失败!" + data.responseJSON.message + "!");
    });
    ajax.set(this.indicationInfoData);
    ajax.start();
}

/**
 * 显示tree
 */
IndicationInfoDlg.showIndicationSelectTree = function () {
    Feng.showInputTree("pName", "parentTreeDiv", 15, 34);
}


IndicationInfoDlg.onClickIndication = function (e, treeId, treeNode) {
    $("#pName").attr("value", treeNode.name);
    $("#pid").attr("value", treeNode.id);

    $("#parentTreeDiv").fadeOut("fast");
}

/**
 * 验证数据是否为空
 */
IndicationInfoDlg.validate = function () {
    $('#indicationInfoForm').data("bootstrapValidator").resetForm();
    $('#indicationInfoForm').bootstrapValidator('validate');
    return $("#indicationInfoForm").data('bootstrapValidator').isValid();
};

$(function () {
    Feng.initValidator("indicationInfoForm", IndicationInfoDlg.validateFields);

    var ztree = new $ZTree("parentTree", "/indication/tree");
    ztree.bindOnClick(IndicationInfoDlg.onClickIndication);
    ztree.init();

    $("#bigIsBetter").val($("#bigIsBetterValue").val());
});
