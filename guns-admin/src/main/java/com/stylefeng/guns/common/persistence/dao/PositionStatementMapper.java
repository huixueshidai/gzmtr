package com.stylefeng.guns.common.persistence.dao;

import com.stylefeng.guns.common.persistence.model.PositionStatement;
import com.baomidou.mybatisplus.mapper.BaseMapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

/**
 * <p>
  *  Mapper 接口
 * </p>
 *
 * @author 殷佳成
 * @since 2018-09-07
 */
public interface PositionStatementMapper extends BaseMapper<PositionStatement> {
      List<Map<String,Object>> list (@Param("condition") String condition);
}