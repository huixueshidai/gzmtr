package com.stylefeng.guns.modular.trapped.dao;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.stylefeng.guns.common.persistence.model.TrappedNumber;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

/**
 * <p>
  * 被困人员录入 Mapper 接口
 * </p>
 *
 * @author xhq
 * @since 2018-09-04
 */
public interface TrappedNumberMapper extends BaseMapper<TrappedNumber> {
    List<Map<String,Object>> selectMap(@Param("page") Page<TrappedNumber> page);
}