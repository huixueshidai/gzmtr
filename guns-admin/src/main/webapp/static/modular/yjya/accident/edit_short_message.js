var EditshortMessageDeg={
    accidentReport: {},
    count: $("#itemSize").val(),
    itemTemplate: $("#itemTemplate").html(),
    layerIndex:-1

};

EditshortMessageDeg.close = function () {
    parent.layer.close(window.parent.ContactMessage.layerIndex);
}

/**
 * 清除数据
 */
EditshortMessageDeg.clearData = function () {
    this.accidentInfoData = {};
}

EditshortMessageDeg.set = function (key, val) {
    this.accidentReport[key] = (typeof val == "undefined") ? $("#" + key).val() : val;
    return this;
}
/**
 * 收集数据
 */
EditshortMessageDeg.collectData = function () {
    this
        .set('accidentName')
    ;
}


/**
 * 添加空白可手动输入的联系人
 */
EditshortMessageDeg.addItem = function(){
    $("#itemsArea").append(this.itemTemplate);
    $("#dictItem").attr("id", this.newId());
    $("#name").attr("id", "name"+this.count);
    $("#duty").attr("id", "duty"+this.count);
    $("#telphone").attr("id", "telphone"+this.count);
}

/**
 * 通讯录添加
 */
EditshortMessageDeg.addItemContactSelData = function(name,duty,tel){
    $("#itemsArea").append(this.itemTemplate);
    $("#dictItem").attr("id", this.newId());
    $("#name").attr("id", "name"+this.count);
    $("#duty").attr("id", "duty"+this.count);
    $("#telphone").attr("id", "telphone"+this.count);
    $("#name"+this.count).attr("value", name);
    $("#duty"+this.count).attr("value", duty);
    $("#telphone"+this.count).attr("value", tel);

}


/**
 * item获取新的id
 */
EditshortMessageDeg.newId = function () {
    if(this.count == undefined){
        this.count = 0;
    }
    this.count = this.count + 1;
    return "dictItem" + this.count;
};
/**
 * 删除item
 */
EditshortMessageDeg.deleteItem = function (event) {
    var obj = Feng.eventParseObject(event);
    obj.parent().parent().remove();

};

/**
 * 发送消息
 */
EditshortMessageDeg.submit = function(){
    EditshortMessageDeg.close();
    Feng.info("发送消息成功！");
}

$(function () {
    var contactSelectds = window.parent.ContactMessage.contactSelects;
    for(var i=0;i<contactSelectds.length;i++){
        EditshortMessageDeg.addItemContactSelData(contactSelectds[i].name,contactSelectds[i].administrationDuty,contactSelectds[i].mobilePhone);
    }
});


