/**
 * 初始化公交联动启动详情对话框
 */
var PublicbusStarupInfoDlg = {
    publicbusStarupInfoData : {}
};

/**
 * 清除数据
 */
PublicbusStarupInfoDlg.clearData = function() {
    this.publicbusStarupInfoData = {};
}

/**
 * 设置对话框中的数据
 *
 * @param key 数据的名称
 * @param val 数据的具体值
 */
PublicbusStarupInfoDlg.set = function(key, val) {
    this.publicbusStarupInfoData[key] = (typeof val == "undefined") ? $("#" + key).val() : val;
    return this;
}

/**
 * 设置对话框中的数据
 *
 * @param key 数据的名称
 * @param val 数据的具体值
 */
PublicbusStarupInfoDlg.get = function(key) {
    return $("#" + key).val();
}

/**
 * 关闭此对话框
 */
PublicbusStarupInfoDlg.close = function() {
    parent.layer.close(window.parent.PublicbusStarup.layerIndex);
}

/**
 * 收集数据
 */
PublicbusStarupInfoDlg.collectData = function() {
    PublicbusStarupInfoDlg
    .set('id')
    .set('lineId')
    .set('stationId')
    .set('retentionTime')
    .set('interruptionTime')
    .set('expectDelays')
    .set('reduction')
    .set('interruptionType')
    .set('schedulingMode')
    .set('schedulingModeMin')
    .set('lossEfficiencyRetention')
    .set('lossEfficiencyTransportation')
    .set('startTime')
        .set('startStation')
        .set('endStation')
        .set('stationAmount')
        .set('dateStatus')
    ;
}

/**
 * 判断是否符合公交应急联动启动条件
 */
PublicbusStarupInfoDlg.starTerm = function(){
    var interruptionTime = $("#interruptionTime").val();
    var retentionTime = $("#retentionTime").val();
    var lineName = $("#lineName").val();
    var dateStatus = $("#dateStatus").val();
    if(lineName!="1号线"){
        Feng.info("目前只有1号线演示，请选择1号线！");
        return false;
    }
    else if(dateStatus==""){
        Feng.info("请选择日期类型");
        return false;
    }
    else if(retentionTime == ""){
        Feng.info("请填写滞留时间");
        return false;
    }
    else if(interruptionTime == ""){
        Feng.info("请填写预计中断时间");
        return false;
    }
    else if($("#reduction").val()>=60){ //预计行车能力降低>=60
        return true;
    }
    else if($("#expectDelays").val()>=40){ //预计晚点》=40
        return true;
    }
    else if(($("#interruptionType").val()==4)&&($("#interruptionTime").val()>=20)){ //双向中断，中断时间大于20
        return true;
    }
    else if(($("#interruptionType").val()==2||$("#interruptionType").val()==3)&&($("#schedulingMode").val()==1)&&($("#schedulingModeMin").val()>=20)&&($("#interruptionTime").val()>=20)){
        return true;
    }
    else{
        Feng.info("未满足公交应急联动启动条件");
        return false;
    }
}

/**
 * 提交添加
 */
PublicbusStarupInfoDlg.addSubmit = function() {
    PublicbusStarupInfoDlg.clearData();
    PublicbusStarupInfoDlg.collectData();
    if(PublicbusStarupInfoDlg.starTerm()){
        var operation = function(){
            //提交信息
            var ajax = new $ax(Feng.ctxPath + "/publicbusStarup/add", function(data){
                $("#PublicbusStar").attr("disabled",true);
                Feng.success("公交应急联动启动成功!");
                window.parent.document.getElementById("menu_317").click();
            },function(data){
                Feng.error("公交应急联动启动失败!" + data.responseJSON.message + "!");
            });
            ajax.set(PublicbusStarupInfoDlg.publicbusStarupInfoData);
            ajax.start();
        }
        Feng.confirm("是否启动公交应急联动?" ,operation);
    }
}

/**
 * 公交应急联动执行启动
 */
PublicbusStarupInfoDlg.submitBusExecute = function(){
    var stationAmount = PublicBusFeederRoute.stationList.length;
    var startStation = PublicBusFeederRoute.startStation;
    var endStation = PublicBusFeederRoute.endStation;
    PublicbusStarupInfoDlg.set("stationAmount",stationAmount);
    PublicbusStarupInfoDlg.set("startStation",startStation);
    PublicbusStarupInfoDlg.set("endStation",endStation);
    var operation = function(){
        //提交信息
        var ajax = new $ax(Feng.ctxPath + "/publicbusStarup/update", function(data){
            Feng.success("公交应急联动启动执行成功!");
            window.parent.document.getElementById("menu_318").click();
        },function(data){
            Feng.error("公交应急联动启动执行失败!" + data.responseJSON.message + "!");
        });
        ajax.set(PublicbusStarupInfoDlg.publicbusStarupInfoData);
        ajax.start();
    }
    Feng.confirm("是否执行公交应急联动启动?" ,operation);

}

/**
 * 重置公交应急联动
 */
PublicbusStarupInfoDlg.delete = function(){
    var operation = function(){
        var ajax = new $ax(Feng.ctxPath + "/publicbusStarup/delete", function (data) {
            Feng.success("重置成功!");
            $("#PublicbusStar").attr("disabled",false);
            location.reload();
        }, function (data) {
            Feng.error("重置失败!" + data.responseJSON.message + "!");
        });
        ajax.start();
    }
    Feng.confirm("确定重置公交应急联动信息?" ,operation);
}

/**
 * 提交修改
 */
PublicbusStarupInfoDlg.editSubmit = function() {

    this.clearData();
    this.collectData();

    //提交信息
    var ajax = new $ax(Feng.ctxPath + "/publicbusStarup/update", function(data){
        Feng.success("修改成功!");
        window.parent.PublicbusStarup.table.refresh();
        PublicbusStarupInfoDlg.close();
    },function(data){
        Feng.error("修改失败!" + data.responseJSON.message + "!");
    });
    ajax.set(this.publicbusStarupInfoData);
    ajax.start();
}

$(function() {

});
