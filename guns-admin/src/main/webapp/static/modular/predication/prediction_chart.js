
var PredicationChart = {
    searchData:{}
}

PredicationChart.set = function (key, val) {
    this.searchData[key] = (typeof val == "undefined") ? $("#" + key).val() : val;
    return this;
}

PredicationChart.get = function (key) {
    return $("#" + key).val();
}

PredicationChart.collectSearchData = function(){
    this.set("category")
        .set("year")
        .set("month")
}

PredicationChart.dateTypeOnchange = function () {
    var val =  $("#dateType").val();
    console.log(val)
    if (val == 1) {
        this.showYearSelector();
    }else if (val == 2) {
        this.showMonthSelector();
    }else{
        this.hideYearSelector();
        this.hideMonthSelector();
    }
};

PredicationChart.showYearSelector = function () {
    $("#yearDiv").show();
    this.hideMonthSelector();
}
PredicationChart.hideYearSelector = function () {
    $("#yearDiv").hide();
    $("#year").val('');
};

PredicationChart.showMonthSelector = function () {
    $("#monthDiv").show();
    this.hideYearSelector();
}
PredicationChart.hideMonthSelector = function () {
    $("#monthDiv").hide();
    $("#month").val('');
}

PredicationChart.search = function () {
    this.renderLine();
};

PredicationChart.renderLine = function () {
    this.collectSearchData();

    var ajax = new $ax(Feng.ctxPath + "/prediction/analyse", function (data) {
        data = JSON.parse(data);
        var myChart = echarts.init(document.getElementById('main'));
        myChart.setOption(data);
        $(window).resize(myChart.resize);
    }, function (data) {
        Feng.error("操作失败!" + data.responseJSON.message + "!");
    });
    ajax.set(this.searchData);
    ajax.start();
};

$(function () {

})
