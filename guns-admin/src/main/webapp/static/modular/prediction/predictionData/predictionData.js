/**
 * 风险预测数据管理管理初始化
 */
var PredictionData = {
    id: "PredictionDataTable",	//表格id
    seItem: null,		//选中的条目
    table: null,
    layerIndex: -1
};

/**
 * 初始化表格的列
 */
PredictionData.initColumn = function () {
    return [
        {field: 'selectItem', radio: true},
            {title: '年份', field: 'year', visible: true, align: 'center', valign: 'middle'},
            {title: '事故类别', field: 'accidentTypeName', visible: true, align: 'center', valign: 'middle'},
            {title: '事故次数', field: 'num', visible: true, align: 'center', valign: 'middle'},
            {title: '创建时间', field: 'createTime', visible: true, align: 'center', valign: 'middle'}
    ];
};

/**
 * 检查是否选中
 */
PredictionData.check = function () {
    var selected = $('#' + this.id).bootstrapTable('getSelections');
    if(selected.length == 0){
        Feng.info("请先选中表格中的某一记录！");
        return false;
    }else{
        PredictionData.seItem = selected[0];
        return true;
    }
};

/**
 * 点击添加风险预测数据管理
 */
PredictionData.openAddPredictionData = function () {
    var index = layer.open({
        type: 2,
        title: '添加风险预测数据管理',
        area: ['800px', '420px'], //宽高
        fix: false, //不固定
        maxmin: true,
        content: Feng.ctxPath + '/predictionData/predictionData_add'
    });
    this.layerIndex = index;
};

/**
 * 打开查看风险预测数据管理详情
 */
PredictionData.openPredictionDataDetail = function () {
    if (this.check()) {
        var index = layer.open({
            type: 2,
            title: '风险预测数据管理详情',
            area: ['800px', '420px'], //宽高
            fix: false, //不固定
            maxmin: true,
            content: Feng.ctxPath + '/predictionData/predictionData_update/' + PredictionData.seItem.id
        });
        this.layerIndex = index;
    }
};

/**
 * 删除风险预测数据管理
 */
PredictionData.delete = function () {
    if (this.check()) {
        var ajax = new $ax(Feng.ctxPath + "/predictionData/delete", function (data) {
            Feng.success("删除成功!");
            PredictionData.table.refresh();
        }, function (data) {
            Feng.error("删除失败!" + data.responseJSON.message + "!");
        });
        ajax.set("predictionDataId",this.seItem.id);
        ajax.start();
    }
};

/**
 * 查询风险预测数据管理列表
 */
PredictionData.search = function () {
    var queryData = {};
    queryData['condition'] = $("#condition").val();
    PredictionData.table.refresh({query: queryData});
};

$(function () {
    var defaultColunms = PredictionData.initColumn();
    var table = new BSTable(PredictionData.id, "/predictionData/list", defaultColunms);
    table.setPaginationType("client");
    PredictionData.table = table.init();
});
