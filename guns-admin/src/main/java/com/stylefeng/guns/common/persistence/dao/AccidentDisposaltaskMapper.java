package com.stylefeng.guns.common.persistence.dao;

import com.stylefeng.guns.common.persistence.model.AccidentDisposaltask;
import com.baomidou.mybatisplus.mapper.BaseMapper;

/**
 * <p>
  *  Mapper 接口
 * </p>
 *
 * @author wangshuaikang
 * @since 2018-09-11
 */
public interface AccidentDisposaltaskMapper extends BaseMapper<AccidentDisposaltask> {

}