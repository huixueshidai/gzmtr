package com.stylefeng.guns.common.persistence.model;

import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableName;
import com.stylefeng.guns.common.plugin.entity.BaseModel;
import com.stylefeng.guns.common.plugin.entity.Treeable;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

/**
 * <p>
 * 
 * </p>
 *
 * @author wangshuaikang
 * @since 2018-01-04
 */
@TableName("gzmtr_roadnet")
@Getter
@Setter
@ToString
public class Roadnet extends BaseModel<Roadnet> implements Treeable {

    private static final long serialVersionUID = 1L;

    /**
     * 编码，唯一
     */
	private Long code;
    /**
     * 父级
     */
	private Integer pid;
    /**
     * 父级IDS
     */
	private String pids;
    /**
     * 名称
     */
	private String name;

	/**
	 * 简称
	 */
	@TableField("short_name")
	private String shortName;

    /**
     * 类型（区域中心、线路、车站）
     */

	private String type;
    /**
     * 排序
     */
	private Integer num;

	@TableField("is_transfer")
	private String isTransfer;
	/**
	 * 是否用于安全评估 1:true 0：false
	 */
	@TableField("is_used")
    private Integer isUsed;

	@Override
	public String getSeparator() {
		return ",";
	}

	@Override
	public String makeSelfAsNewParentIds() {
		return getPids() + getId() + getSeparator();
	}
}
