package com.stylefeng.guns.modular.yjya.warpper;


import com.stylefeng.guns.common.constant.factory.ConstantFactory;
import com.stylefeng.guns.common.persistence.dao.AccidentTypeMapper;
import com.stylefeng.guns.common.persistence.dao.RoadnetMapper;
import com.stylefeng.guns.common.persistence.model.AccidentType;
import com.stylefeng.guns.common.persistence.model.Roadnet;
import com.stylefeng.guns.core.base.warpper.BaseControllerWarpper;
import com.stylefeng.guns.core.util.SpringContextHolder;
import org.apache.commons.collections.MapUtils;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

/**
 * Created by 60132 on 2018/1/22.
 */
public class CounterplanWarpper extends BaseControllerWarpper {

    public CounterplanWarpper(Object obj) {
        super(obj);
    }

    @Override
    protected void warpTheMap(Map<String, Object> map) {
        //判断是预案
        if(map.containsKey("status")){
            map.put("typeName", ConstantFactory.me().getDictNameByCode("counterplanType", MapUtils.getString(map, "type")));
            map.put("levelName", ConstantFactory.me().getDictNameByCode("responseLevel", (String) map.get("level")));
            if(map.containsKey("stationId")){
                Integer lineId =null;
                String stationIds = MapUtils.getString(map,"stationId");
                List<Integer> stationIdList = new ArrayList(Arrays.asList(stationIds.split(",")));
                List<Roadnet> roadnetList = new ArrayList<>();
                String stationName = "";
                for(int i=0;i<stationIdList.size();i++){
                    Roadnet station = SpringContextHolder.getBean(RoadnetMapper.class).selectById(stationIdList.get(i));
                    roadnetList.add(station);
                }
                for(int i=0;i<roadnetList.size();i++){
                    if(i==roadnetList.size()-1){
                        stationName += roadnetList.get(i).getName();
                        lineId = roadnetList.get(i).getPid();
                    }
                    else{
                        stationName += roadnetList.get(i).getName()+">>";
                    }
                }
                Roadnet line = SpringContextHolder.getBean(RoadnetMapper.class).selectById(lineId);
                map.put("stationName",line.getName()+":"+stationName);
            }
            AccidentType accidentType = SpringContextHolder.getBean(AccidentTypeMapper.class).selectById((Serializable) map.get("accidentTypeId"));
            if (accidentType != null) {
                String pids = accidentType.getPids();
                List<String> ids = new ArrayList<>(Arrays.asList(pids.split("/")));
                ids.add(String.valueOf(accidentType.getId()));
                List<AccidentType> accidentTypeList =  SpringContextHolder.getBean(AccidentTypeMapper.class).selectBatchIds(ids);
                String accideentTypeName = "";
                for(int i=1;i<accidentTypeList.size();i++){
                    accideentTypeName += accidentTypeList.get(i).getName()+"--";
                }
                map.put("accidentTypeName", accideentTypeName+accidentTypeList.get(0).getName());
            }
           map.put("statusName", ConstantFactory.me().getDictNameByCode("counterplanStatus", map.get("status").toString()));
            map.put("typeStatus","预案");
        }
        //历史案例
        else{
            if(map.containsKey("stationId")){
                String stationIds = MapUtils.getString(map,"stationId");
                List<Integer> stationIdList = new ArrayList(Arrays.asList(stationIds.split("/")));
                List<Roadnet> roadnetList = new ArrayList<>();
                String stationName = "";
                for(int i=0;i<stationIdList.size();i++){
                    Roadnet station = SpringContextHolder.getBean(RoadnetMapper.class).selectById(stationIdList.get(i));
                    roadnetList.add(station);
                }
                for(int i=0;i<roadnetList.size();i++){
                    if(i==roadnetList.size()-1){
                        stationName += roadnetList.get(i).getName();
                    }
                    else{
                        stationName += roadnetList.get(i).getName()+">>";
                    }
                }
                map.put("name", ConstantFactory.me().getRoadnetNameById((Integer) map.get("lineId"))+":"+stationName+"事故");
                map.put("stationName", ConstantFactory.me().getRoadnetNameById((Integer) map.get("lineId"))+":"+stationName);
            }

            map.put("levelName", ConstantFactory.me().getDictNameByCode("responseLevel", (String) map.get("accidentLevel")));
            map.put("accidentMainName", ConstantFactory.me().getDictNameByCode("accident_main", (String) map.get("accidentMain")));
            AccidentType accidentType = SpringContextHolder.getBean(AccidentTypeMapper.class).selectById((Serializable) map.get("accidentTypeId"));
            if (accidentType != null) {
                String pids = accidentType.getPids();
                List<String> ids = new ArrayList<>(Arrays.asList(pids.split("/")));
                ids.add(String.valueOf(accidentType.getId()));
                List<AccidentType> accidentTypeList =  SpringContextHolder.getBean(AccidentTypeMapper.class).selectBatchIds(ids);
                String accideentTypeName = "";
                for(int i=1;i<accidentTypeList.size();i++){
                    accideentTypeName += accidentTypeList.get(i).getName()+"--";
                }
                map.put("accidentTypeName", accideentTypeName+accidentTypeList.get(0).getName());
            }
            if(map.containsKey("addType")){
                if((Integer)map.get("addType")==1){
                    map.put("typeStatus","历史事故实例");
                }
                else{
                    map.put("typeStatus","新增事故实例");
                }
            }

        }
    }

}
