/**
 * 初始化通讯录群组详情对话框
 */
var ContactGroupInfoDlg = {
    contactGroupInfoData : {},
    zTreeInstance : null,
    validateFields: {
        name: {
            validators: {
                notEmpty: {
                    message: '群组名称不能为空'
                }
            }
        },
        num: {
            // validators: {
            //     notEmpty: true,
            //     digits: true
            // },
            // message: {
            //     notEmpty: '名字不能为空',
            //     digits: '名字必须为数字'
            //      }
            // },
            validators: {
                notEmpty: {
                    message: '排序不能为空'
                },
                digits:{
                    message: '排序必须为数字'
                }
            }

            },
        pName: {
            validators: {
                notEmpty: {
                    message: '请选择上级'
                }
            }
        }
    }
};

/**
 * 清除数据
 */
ContactGroupInfoDlg.clearData = function() {
    this.contactGroupInfoData = {};
}

/**
 * 设置对话框中的数据
 *
 * @param key 数据的名称
 * @param val 数据的具体值
 */
ContactGroupInfoDlg.set = function(key, val) {
    this.contactGroupInfoData[key] = (typeof val == "undefined") ? $("#" + key).val() : val;
    return this;
}

/**
 * 设置对话框中的数据
 *
 * @param key 数据的名称
 * @param val 数据的具体值
 */
ContactGroupInfoDlg.get = function(key) {
    return $("#" + key).val();
}

/**
 * 关闭此对话框
 */
ContactGroupInfoDlg.close = function() {
    parent.layer.close(window.parent.ContactGroup.layerIndex);
}

/**
 * 收集数据
 */
ContactGroupInfoDlg.collectData = function() {
    this
    .set('id')
    .set('num')
    .set('pid')
    .set('name');

}

/**
 * 验证数据是否为空
 */
ContactGroupInfoDlg.validate = function () {
    $('#menuInForm').data("bootstrapValidator").resetForm();
    $('#menuInForm').bootstrapValidator('validate');
    return $("#menuInForm").data('bootstrapValidator').isValid();
}


/**
 * 点击部门ztree列表的选项时
 *
 * @param e
 * @param treeId
 * @param treeNode
 * @returns
 * <!-- 父级部门的选择框 -->
 <div id="parentContactGroupMenu" class="menuContent"
 style="display: none; position: absolute; z-index: 200;">
 <ul id="parentContactGroupMenuTree" class="ztree tree-box" style="width: 245px !important;"></ul>
 </div>
 */
ContactGroupInfoDlg.onClickContactGroup = function(e, treeId, treeNode) {
    $("#pName").attr("value", ContactGroupInfoDlg.zTreeInstance.getSelectedVal());
    $("#pid").attr("value", treeNode.id);
    $("#parentContactGroupMenu").fadeOut("fast");

}

/**
 * 显示tree
 */
ContactGroupInfoDlg.showContactGroupSelectTree = function() {
    var pName = $("#pName");
    var pNameOffset = $("#pName").offset();
    $("#parentContactGroupMenu").css({
        left : pNameOffset.left + "px",
        top : pNameOffset.top + pName.outerHeight() + "px"
    }).slideDown("fast");

    $("body").bind("mousedown", onBodyDown);
}

function onBodyDown(event) {
    if (!(event.target.id == "menuBtn" || event.target.id == "parentContactGroupMenu" || $(
            event.target).parents("#parentContactGroupMenu").length > 0)) {
        ContactGroupInfoDlg.hideContactGroupSelectTree();
    }
}

/**
 * 隐藏选择的树
 */
ContactGroupInfoDlg.hideContactGroupSelectTree = function() {
    $("#parentContactGroupMenu").fadeOut("fast");
    $("body").unbind("mousedown", onBodyDown);// mousedown当鼠标按下就可以触发，不用弹起
}

/**
 * 提交添加
 */
ContactGroupInfoDlg.addSubmit = function() {

    this.clearData();
    this.collectData();
    if (!this.validate()) {
        return;
    }

    //提交信息
    var ajax = new $ax(Feng.ctxPath + "/contactGroup/add", function(data){
        Feng.success("添加成功!");
        window.parent.ContactGroup.table.refresh();
        ContactGroupInfoDlg.close();
    },function(data){
        Feng.error("添加失败!" + data.responseJSON.message + "!");
    });
    ajax.set(this.contactGroupInfoData);
    ajax.start();
}

/**
 * 提交修改
 */
ContactGroupInfoDlg.editSubmit = function() {

    this.clearData();
    this.collectData();
    if (!this.validate()) {
        return;
    }

    //提交信息
    var ajax = new $ax(Feng.ctxPath + "/contactGroup/update", function(data){
        Feng.success("修改成功!");
        window.parent.ContactGroup.table.refresh();
        ContactGroupInfoDlg.close();
    },function(data){
        Feng.error("修改失败!" + data.responseJSON.message + "!");
    });
    ajax.set(this.contactGroupInfoData);
    ajax.start();
}

$(function() {
    Feng.initValidator("menuInForm", ContactGroupInfoDlg.validateFields);

    var ztree = new $ZTree("parentContactGroupMenuTree", "/contactGroup/tree");
    ztree.bindOnClick(ContactGroupInfoDlg.onClickContactGroup);
    ztree.init();
    ContactGroupInfoDlg.zTreeInstance = ztree;

});
