package com.stylefeng.guns.modular.fire.warpper;

import com.stylefeng.guns.common.constant.factory.ConstantFactory;
import com.stylefeng.guns.core.base.warpper.BaseControllerWarpper;
import org.apache.commons.collections.MapUtils;

import java.util.Map;

public class FireSourceWarpper extends BaseControllerWarpper{


    public FireSourceWarpper(Object obj) {
        super(obj);
    }

    @Override
    protected void warpTheMap(Map<String, Object> map) {
        map.put("field", ConstantFactory.me().getDictNameByCode("fieldType", MapUtils.getString(map,"field")));
        map.put("material", ConstantFactory.me().getDictNameByCode("materialType", (String) map.get("material")));


       map.put("rescuers", ConstantFactory.me().getDictNameByCode("rescuersType", (String) map.get("rescuers")));

        return;
    }
}
