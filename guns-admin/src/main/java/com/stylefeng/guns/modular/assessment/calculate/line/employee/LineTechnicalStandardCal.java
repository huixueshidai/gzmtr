package com.stylefeng.guns.modular.assessment.calculate.line.employee;

import com.stylefeng.guns.common.persistence.model.Assessment;
import com.stylefeng.guns.core.util.DoubleUtil;
import com.stylefeng.guns.modular.assessment.calculate.ThirdIndicationCalculator;
import com.stylefeng.guns.modular.assessment.calculate.aware.LineThirdIndicationAware;
import org.springframework.stereotype.Component;

import java.util.Date;

/**
 * 员工技术水平达标指数
 *
 * @author yinjc
 * @since now
 */
@Component
public class LineTechnicalStandardCal extends ThirdIndicationCalculator implements LineThirdIndicationAware {
    @Override
    protected Integer getIndicationId() {
        return 58;
    }

    @Override
    public Assessment calculate(Integer lineId, Date beginDate, Date endDate) {
        Double daBiaoNum = getDoubleValue(lineId, 187, beginDate,endDate);
        Double totalNum = getDoubleValue(lineId, 188, beginDate,endDate);
        double resValue = DoubleUtil.safeDiv(daBiaoNum, totalNum);
        return genResult(resValue, lineId, beginDate,endDate);
    }
}
