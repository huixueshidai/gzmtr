package com.stylefeng.guns.modular.assessment.calculate.road.accident;

import com.stylefeng.guns.common.persistence.model.Assessment;
import com.stylefeng.guns.core.util.DoubleUtil;
import com.stylefeng.guns.modular.assessment.calculate.ThirdIndicationCalculator;
import com.stylefeng.guns.modular.assessment.calculate.aware.RoadnetThirdIndicationAware;
import org.springframework.stereotype.Component;

import java.util.Date;

/**
 * 路网中心等效事故率
 *
 * @author yinjc
 * @time 2018/05/11
 */
@Component
public class RoadRegionalCenterEquivalentCal extends ThirdIndicationCalculator implements RoadnetThirdIndicationAware {

    @Override
    protected Integer getIndicationId() {
        return 108;
    }

    @Override
    public Assessment calculate(Integer rodaId, Date beginDate, Date endDate) {
        double y1 = getDoubleValue(rodaId, 349, beginDate,endDate);
        double y2 = getDoubleValue(rodaId, 350, beginDate,endDate);
        double y3 = getDoubleValue(rodaId, 351, beginDate,endDate);
        double y4 = getDoubleValue(rodaId, 352, beginDate,endDate);
        double y5 = getDoubleValue(rodaId, 353, beginDate,endDate);
        double a1 = getDoubleValue(rodaId, 354, beginDate,endDate);
        double a2 = getDoubleValue(rodaId, 355, beginDate,endDate);
        double a3 = getDoubleValue(rodaId, 356, beginDate,endDate);
        double a4 = getDoubleValue(rodaId, 357, beginDate,endDate);
        double a5 = getDoubleValue(rodaId, 358, beginDate,endDate);
        double l = getDoubleValue(rodaId, 359, beginDate,endDate);

        double m1 = DoubleUtil.mul(y1, a1);
        double m2 = DoubleUtil.mul(y2, a2);
        double m3 = DoubleUtil.mul(y3, a3);
        double m4 = DoubleUtil.mul(y4, a4);
        double m5 = DoubleUtil.mul(y5, a5);

        double he = DoubleUtil.add(m1, m2, m3, m4, m5);
        double resVal = DoubleUtil.safeDiv(he, l);

        return genResult(resVal, rodaId, beginDate,endDate);
    }

}
