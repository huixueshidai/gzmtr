package com.stylefeng.guns.modular.assessment.controller;

import com.stylefeng.guns.modular.assessment.entity.Warning;
import com.stylefeng.guns.modular.assessment.service.impl.AssessmentAnalogyService;
import com.vip.vjtools.vjkit.time.DateUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 *  安全仿真
 */
@Controller
@RequestMapping("assessmentAnalogy")
public class AssessmentAnalogyController {

	private String PREFIX = "/assessmentAnalogy/";

	@Autowired
	private AssessmentAnalogyService assessmentAnalogyService;

	@RequestMapping("")
	public String index() {
		return PREFIX + "assessmentAnalogy.html";
	}

	@RequestMapping("waring")
	public String warning() {
		return PREFIX + "analogyWaring.html";
	}

	@RequestMapping("getWarning")
	@ResponseBody
	public Map<String, List<Warning>> warningInfo(@RequestParam("date") @DateTimeFormat(pattern = "yyyy-MM-dd") Date beginDate) {
		List<Warning> warningList = assessmentAnalogyService
				.getAllWarningInfo(beginDate, new Date(DateUtil.nextDate(beginDate).getTime() - 1000));

		Map<String, List<Warning>> res = warningList.stream().collect(Collectors.groupingBy(Warning::getType));
		return res;
	}

}
