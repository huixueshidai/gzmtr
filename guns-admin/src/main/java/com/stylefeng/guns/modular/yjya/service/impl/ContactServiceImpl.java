package com.stylefeng.guns.modular.yjya.service.impl;

import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.stylefeng.guns.common.persistence.dao.ContactMapper;
import com.stylefeng.guns.common.persistence.model.Contact;
import com.stylefeng.guns.modular.yjya.dao.ContactDao;
import com.stylefeng.guns.modular.yjya.service.IContactService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

/**
 * <p>
 * 通讯录 服务实现类
 * </p>
 *
 * @author xuziyang
 * @since 2017-12-26
 */
@Service
public class ContactServiceImpl extends ServiceImpl<ContactMapper, Contact> implements IContactService {

//    @Autowired
//    private ContactDao contactDao;
//
//    @Override
//    public List<Map<String, Object>> selectContacts(String name, Integer groupId) {
//        return contactDao.selectContacts(name, groupId);
//    }
}
